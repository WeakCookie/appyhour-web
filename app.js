var express = require('express');
var path = require('path');
const dotenv = require('dotenv');
const cors = require('cors');
// var enforce = require('express-sslify');
// var http = require('http');
// var favicon = require('serve-favicon');
// var busboy = require('connect-busboy');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('client-sessions');
var swig = require('swig');
var db = require('./db');
var fileUpload = require('express-fileupload');
var schedule = require('node-schedule');
var AWS = require('aws-sdk');
var passport = require('passport');
AWS.config.loadFromPath('./awsconfig.json');
var CronJob = require('cron').CronJob;
var { prefetchAnalyticsData } = require("./utils/analytics/prefetchAnalyticsData");
// Inject controllers
var index = require('./controllers/index');
var dash = require('./controllers/dash');
var home = require('./controllers/home');
var events = require('./controllers/events');
var info = require('./controllers/info');
var settings = require('./controllers/settings');
var analytics = require('./controllers/analytics');
var partners = require('./controllers/partners');
var admin = require('./controllers/admin');
var resetpassword = require('./controllers/resetpassword');
var privacypolicy = require('./controllers/privacypolicy');
var estlogin = require('./controllers/estlogin');
var metrics = require('./controllers/metrics');
var upgrade = require('./controllers/upgrade');
var supplierdashboard = require('./controllers/supplierdashboard');

var User = require('./models/user');
var Establishment = require('./models/establishment');
var Image = require('./models/image');
// var Ref = require('./models/ref');
var { requireLogin, checkForLogin } = require('./utils/common')
var router = require("./routes")
const { jwtStrategy } = require('./config/passport');
const { authLimiter } = require('./middlewares/rateLimiter');
const { updateAverageSincePublished } = require('./utils/analytics')

// Initialize firebase for the whole app
/*var firebase = require('firebase');
var config = {
  apiKey: 'AIzaSyCEpX2kAizc15BeKRq1xoNXHwK63nSP2Qw',
  authDomain: 'project-6641658176200858005.firebaseapp.com',
  databaseURL: 'https://project-6641658176200858005.firebaseio.com',
  storageBucket: 'project-6641658176200858005.appspot.com',
  messagingSenderId: "660202483975"
};
firebase.initializeApp(config);*/

dotenv.config();
var app = express();

// COMMENT OUT FOR LOCAL
/*
app.use(enforce.HTTPS({ trustProtoHeader: true }));
http.createServer(app).listen(app.get('port'), function() {
    console.log('Express server listening on port ' + app.get('port'));
});
*/
// view engine setup
var swig = new swig.Swig();
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', path.join(__dirname, 'views/pages'));

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(busboy());
app.use(fileUpload());

app.use(session({
  cookieName: 'session',
  secret: 'efdf**FSFSslds;a0000shbdf{}+Ijsli;;to8',
  duration: 7 * 24 * 60 * 60 * 1000,
  activeDuration: 30 * 60 * 1000,
  httpOnly: true,
  secure: true,
  ephemeral: false
}));

// enable cors
app.use(cors());
app.options('*', cors());

app.use(passport.initialize());
passport.use('jwt', jwtStrategy);

app.use('/v2', authLimiter);

app.use(function(req, res, next) {
  if (req.session && req.session.user) {
    User.getAllByUsername(req.session.user.username, function (err, user) {
      if (user) {
        Establishment.getAllByOwnerId(user.user_id, function (err, ests) {
          if (ests) {
            //Image.getProfilePicForEstablishmentId(ests[0].establishment_id, function(err, image) {
            //var est = ests[0];
            //est.profile_pic_id = image.image_id;
            //est.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/"+image.image_location;

            req.user = user;
            req.ests = ests;
            delete req.user.password; // delete the password from the session
            req.session.user = user;  // refresh the session value

            req.session.ests = [];
            for (var e in ests) {
              req.session.ests.push(ests[e].establishment_id);
            }
            //req.session.establishment = est;
            res.locals.user = user;
            res.locals.ests = ests;
            next(); // finishing processing the middleware and run the route
            //});
          } else {
            req.user = user;
            delete req.user.password; // delete the password from the session
            req.session.user = user;  // refresh the session value
            res.locals.user = user;
            next(); // finishing processing the middleware and run the route
          }
        });
      } else {
        next();
      }
    });
  } else {
    next();
  }
});

// schedule.scheduleJob('*/1 * * * *', function(fireDate){ // runs every minute
// job runs every day at midnight to cancel subscriptions
schedule.scheduleJob('0 0 * * *', function (fireDate) {
  console.log("Cron job run at " + fireDate);

  var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
  //var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL

  stripe.customers.list(function (err, customers) {
    User.getAll(function (err2, users) {
      var custIds = [];
      var i = 0;

      for (c in customers.data) {
        if (customers.data[c].subscriptions.total_count == 0) {
          custIds[i++] = customers.data[c].id;
        }
      }

      for (u in users) {
        if (users[u].stripe_id != null && users[u].stripe_id != "") {
          for (id in custIds) {
            if (users[u].stripe_id == custIds[id] && users[u].show_specials == 1 && users[u].username != "teamappyhour") {
              // remove specials from app
              var user = {
                USER: {
                  user_id: users[u].user_id,
                  show_specials: 0
                }
              }
              User.update(user, function (err, result) {
                console.log("USER UPDATED");
                console.log(users[u]);
              });
            }
          }
        }
      }
    });
  });
});

// ROUTES
app.get('/', checkForLogin, function (req, res) {
  //res.render('index');
  res.render('estlogin');
});

app.get('/dashboard', requireLogin, function (req, res) {
  var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
  //var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
  var cardOnFile = false;
  var hasSubscription = false;
  var hasLite = false;
  var needsToPay = false;
  // var cancelDate;
  // var dateCanceled;
  var plans;
  var showEvents = true;

  if (req.session.proxy)
    showEvents = true;

  if (req.session.establishment) {
    var est = req.session.establishment;

    if (est.topic == "denverco") {
      req.session.tz = "MST";
    }
    else if (est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
      req.session.tz = "CST";
    }
    else if (est.topic == "louisvilleky" || est.topic == "tampabayfl") {
      req.session.tz = "EST";
    }

    if (req.session.user.needs_to_pay == 1) {
      needsToPay = true;

      stripe.plans.list(function (err, plans) {
        plans = plans.data;

        if (req.session.user.stripe_id) {
          stripe.customers.retrieve(req.session.user.stripe_id, function (err, customer) {
            if (customer) {
              if (customer.subscriptions.data.length > 0) {
                hasSubscription = true;
                if (customer.subscriptions.data[0].plan.id == "price_1IYYDNJjoHPs2g2pYdsw6kXB") {
                  hasLite = true;
                }
              }
              stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function (err, card) {
                if (card) {
                  cardOnFile = true;
                }

                res.render('dashboard', {
                  needsToPay: needsToPay,
                  cardOnFile: cardOnFile,
                  hasSubscription: hasSubscription,
                  hasLite: hasLite,
                  plans: plans,
                  showEvents: showEvents,
                  est: est
                });
              });
            } else {
              res.render('dashboard', {
                needsToPay: needsToPay,
                cardOnFile: cardOnFile,
                hasSubscription: hasSubscription,
                hasLite: hasLite,
                plans: plans,
                showEvents: showEvents,
                est: est
              });
            }
          });
        } else {
          res.render('dashboard', {
            needsToPay, needsToPay,
            cardOnFile: cardOnFile,
            hasSubscription: hasSubscription,
            hasLite: hasLite,
            plans: plans,
            showEvents: showEvents,
            est: est
          });
        }
      });
    } else {
      res.render('dashboard', {
        needsToPay, needsToPay,
        cardOnFile: cardOnFile,
        hasSubscription: hasSubscription,
        hasLite: hasLite,
        plans: plans,
        showEvents: showEvents,
        est: est
      });
    }
  }
  else {
    Establishment.getAllByOwnerId(req.session.user.user_id, function (err, ests) {
      if (ests) {
        Image.getProfilePicForEstablishmentId(ests[0].establishment_id, function (err, image) {
          var est = ests[0];
          est.profile_pic_id = image.image_id;
          est.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/" + image.image_location;

          if (est.topic == "denverco") {
            req.session.tz = "MST";
          }
          else if (est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
            req.session.tz = "CST";
          }
          else if (est.topic == "louisvilleky" || est.topic == "tampabayfl") {
            req.session.tz = "EST";
          }

          req.session.establishment = est;

          if (req.session.user.needs_to_pay == 1) {
            needsToPay = true;

            stripe.plans.list(function (err, plans) {
              plans = plans.data;

              if (req.session.user.stripe_id) {
                stripe.customers.retrieve(req.session.user.stripe_id, function (err, customer) {
                  if (customer) {
                    if (customer.subscriptions.data.length > 0) {
                      hasSubscription = true;
                      if (customer.subscriptions.data[0].plan.id == "price_1IYYDNJjoHPs2g2pYdsw6kXB") {
                        hasLite = true;
                      }
                    }
                    stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function (err, card) {
                      if (card) {
                        cardOnFile = true;
                      }

                      res.render('dashboard', {
                        needsToPay: needsToPay,
                        cardOnFile: cardOnFile,
                        hasSubscription: hasSubscription,
                        hasLite: hasLite,
                        plans: plans,
                        showEvents: showEvents,
                        est: est
                      });
                    });
                  } else {
                    res.render('dashboard', {
                      needsToPay: needsToPay,
                      cardOnFile: cardOnFile,
                      hasSubscription: hasSubscription,
                      hasLite: hasLite,
                      plans: plans,
                      showEvents: showEvents,
                      est: est
                    });
                  }
                });
              } else {
                res.render('dashboard', {
                  needsToPay, needsToPay,
                  cardOnFile: cardOnFile,
                  hasSubscription: hasSubscription,
                  hasLite: hasLite,
                  plans: plans,
                  showEvents: showEvents,
                  est: est
                });
              }
            });
          } else {
            res.render('dashboard', {
              needsToPay, needsToPay,
              cardOnFile: cardOnFile,
              hasSubscription: hasSubscription,
              hasLite: hasLite,
              plans: plans,
              showEvents: showEvents,
              est: est
            });
          }
        });
      }
    });
  }
});

app.get('/changeest', function (req, res) {
  Establishment.getAllByEstablishmentId(req.query.selectedEst, function (err, est) {
    if (est) {
      var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
      //var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
      var cardOnFile = false;
      var hasSubscription = false;
      var hasLite = false;
      var needsToPay = false;
      var cancelDate;
      var dateCanceled;
      var plans;
      var showEvents = true;

      if (req.session.proxy)
        showEvents = true;

      Image.getProfilePicForEstablishmentId(est.establishment_id, function (err, image) {
        est.profile_pic_id = image.image_id;
        est.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/" + image.image_location;

        if (est.topic == "denverco") {
          req.session.tz = "MST";
        }
        else if (est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
          req.session.tz = "CST";
        }
        else if (est.topic == "louisvilleky" || est.topic == "tampabayfl") {
          req.session.tz = "EST";
        }

        delete req.session.establishment;
        req.session.establishment = est;

        if (req.session.user.needs_to_pay == 1) {
          needsToPay = true;

          stripe.plans.list(function (err, plans) {
            plans = plans.data;

            if (req.session.user.stripe_id) {
              stripe.customers.retrieve(req.session.user.stripe_id, function (err, customer) {
                if (customer) {
                  if (customer.subscriptions.data.length > 0) {
                    hasSubscription = true;
                    if (customer.subscriptions.data[0].plan.id == "price_1IYYDNJjoHPs2g2pYdsw6kXB") {
                      hasLite = true;
                    }
                  }
                  stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function (err, card) {
                    if (card) {
                      cardOnFile = true;
                    }

                    res.render('dashboard', {
                      needsToPay: needsToPay,
                      cardOnFile: cardOnFile,
                      hasSubscription: hasSubscription,
                      hasLite: hasLite,
                      plans: plans,
                      showEvents: showEvents,
                      est: est
                    });
                  });
                } else {
                  res.render('dashboard', {
                    needsToPay: needsToPay,
                    cardOnFile: cardOnFile,
                    hasSubscription: hasSubscription,
                    hasLite: hasLite,
                    plans: plans,
                    showEvents: showEvents,
                    est: est
                  });
                }
              });
            } else {
              res.render('dashboard', {
                needsToPay, needsToPay,
                cardOnFile: cardOnFile,
                hasSubscription: hasSubscription,
                hasLite: hasLite,
                plans: plans,
                showEvents: showEvents,
                est: est
              });
            }
          });
        }
        else {
          res.render('dashboard', {
            needsToPay, needsToPay,
            cardOnFile: cardOnFile,
            hasSubscription: hasSubscription,
            hasLite: hasLite,
            plans: plans,
            showEvents: showEvents,
            est: est
          });
        }
      });
    }
  });
});

app.get('/logout', function (req, res) {
  req.session.reset();
  //res.redirect('/');
  res.redirect('/estlogin');
});

app.get('/admin', admin.show);
// TODO: make sure we can't access these and AJAX routes directly
app.get('/dash', dash.show);
app.get('/home', home.show);
app.get('/events', events.show);
app.get('/info', info.show);
app.get('/analytics', analytics.analyticsIntercept, analytics.show);
app.get('/partners', partners.show);
app.get('/resetpassword', resetpassword.show);
app.get('/privacypolicy', privacypolicy.show);
app.get('/estlogin', estlogin.show);
app.get('/metrics', metrics.show);
app.get('/upgrade', upgrade.show);
app.get('/supplierdashboard', supplierdashboard.show);

app.post('/login', index.login);
app.post('/supplierlogin', index.supplierLogin);
app.post('/updateuser', index.updateUser);
app.post('/updatesupplier', index.updateSupplier);

app.get('/verifyfpkey', resetpassword.verifyFPKey);
app.post('/resetpasswordsubmit', resetpassword.resetPassword);

app.post('/saveinfo', info.save);
app.get('/savetoken', info.saveToken);
app.get('/cancelsubscription', info.cancelSubscription);
app.get('/reactivatesubscription', info.reactivateSubscription);

app.post('/savesettings', settings.saveSettings);
app.post('/updatepassword', settings.updatePassword);
app.post('/addmanager', settings.addManager);
app.post('/deletemanager', settings.deleteManager);

app.post('/addDeal', home.addDeal);
app.post('/addDailyDeal', home.addDailyDeal);
app.post('/deletespecial', home.del);
app.post('/removeDailyDeal', home.del);
app.get('/loadspecial', home.loadSpecial);
app.post('/editspecial', home.editSpecial);
app.post('/deleteeditspecial', home.deleteEditSpecial);

app.get('/scheduleevent', events.scheduleEvent);
app.post('/createevent', events.createEvent);
app.post('/edittodaysevent', events.editTodaysEvent);
app.post('/deletetodaysevent', events.deleteTodaysEvent);
app.get('/addspecialtotodaysevent', events.addSpecialToTodaysEvent);
app.get('/deletespecialfromevent', events.deleteSpecialFromEvent);
app.get('/loadeventsforweek', events.loadEventsForWeek);
app.get('/loadevent', events.loadEvent);
app.get('/validateevent', events.validateEvent);
app.post('/editevent', events.editEvent);
app.post('/deleteevent', events.deleteEvent);

app.get('/upcomingsoefilter', dash.upcomingSoeFilter);

app.post('/auth', admin.auth);
app.post('/adminlogout', admin.adminLogout);
app.post('/saveowner', admin.saveOwner);
app.post('/deleteowner', admin.deleteOwner);
app.post('/saveest', admin.saveEst);
app.post('/deleteest', admin.deleteEst);
app.post('/adminaddspecial', admin.addSpecial);
app.post('/adminadddailyspecial', admin.addDailySpecial);
app.post('/admindeletespecial', admin.deleteSpecial);
app.post('/admindeletedailyspecial', admin.deleteDailySpecial);
app.post('/adminaddevent', admin.addEvent);
app.post('/admindeleteevent', admin.deleteEvent);
app.post('/admineditevent', admin.saveEvent);
app.get('/adminaddspecialtoevent', admin.addSpecialToEvent);
app.get('/promos', admin.getPromos);
app.post('/savepromo', admin.savePromo);
app.post('/savesupplier', admin.saveSupplier);
app.post('/adminapprovepromo', admin.approvePromo);
app.post('/admindenypromo', admin.denyPromo);
app.get('/suppliersendclaimemail', admin.supplierSendClaimEmail);
app.get('/adminvalidatesupplier', admin.validateSupplier);
app.post('/archivesupplier', admin.archiveSupplier);
app.get('/importests', admin.showImportEsts);
app.post('/saveimportests', admin.importEsts);
app.post('/addtoah', admin.addToAH);
app.post('/addtoqueue', admin.addToQueue);
app.post('/reviewest', admin.reviewEst);
app.get('/validatereviewest', admin.validateReviewEst);

app.get('/audit', admin.showAudit);
app.post('/auditaddeditspecial', admin.auditAddEditSpecial);
app.post('/auditdeletespecial', admin.auditDeleteSpecial);
app.post('/auditaddeditevent', admin.auditAddEditEvent);
app.post('/auditdeleteevent', admin.auditDeleteEvent);
app.get('/auditvalidateest', admin.auditValidateEst);
app.post('/auditsaveest', admin.auditSaveEst);
app.post('/auditreviewest', admin.auditReviewEst);

app.post('/metricsauth', metrics.metricsAuth);
app.post('/metricslogout', metrics.metricsLogout);

app.post('/supplierlogout', supplierdashboard.supplierLogout);
app.post('/suppliersavepromo', supplierdashboard.supplierSavePromo);
app.get('/supplierpromodetails', supplierdashboard.supplierPromoDetails);
app.get('/supplieraccountinfo', supplierdashboard.supplierAccountInfo);
app.post('/supplierupdatepassword', supplierdashboard.supplierUpdatePassword);
app.post('/suppliervalidateinfo', supplierdashboard.supplierValidateInfo);
app.post('/suppliersaveinfo', supplierdashboard.supplierSaveInfo);
app.get('/suppliereditmanagers', supplierdashboard.supplierEditManagers);
app.post('/supplieraddmanager', supplierdashboard.supplierAddManager);
app.post('/supplierdeletemanager', supplierdashboard.supplierDeleteManager);
app.post('/supplierresendclaimemail', supplierdashboard.supplierResendClaimEmail);
app.post('/suppliersaveuserlocation', supplierdashboard.supplierSaveUserLocation);
app.get('/supplieraddcampaign', supplierdashboard.supplierAddCampaign);
app.post('/supplierpersistlogistics', supplierdashboard.supplierPersistLogistics);
app.post('/supplierpersistdesign', supplierdashboard.supplierPersistDesign);
app.post('/suppliersavecampaign', supplierdashboard.supplierSaveCampaign);
app.post('/supplieractivatecampaign', supplierdashboard.supplierActivateCampaign);

// AJAX ROUTES
app.get('/contactus', index.contactUs);
app.get('/userexists', index.userExists);
app.get('/userhaschangedpassword', index.userHasChangedPassword);
app.get('/secretpasswordmatches', index.secretPasswordMatches);
app.get('/passwordmatches', index.passwordMatches);
app.get('/getemailforforgotpassword', index.getEmailForForgotPassword);
app.get('/forgotpassword', index.forgotPassword);
app.get('/supplierexists', index.supplierExists);
app.get('/supplierhaschangedpassword', index.supplierHasChangedPassword);
app.get('/supplierpasswordmatches', index.supplierPasswordMatches);

app.get('/selectowner', admin.selectOwner);
app.get('/validateowner', admin.validateOwner);
app.get('/sendclaimemail', admin.sendClaimEmail);

app.get('/selectest', admin.selectEst);
app.get('/adminanalytics', admin.analytics);
app.get('/validateest', admin.validateEst);
app.post('/proxytouser', admin.proxyToUser);
app.post('/deletepromo', admin.deletePromo);
app.get('/promodetails', admin.getPromoDetails);
app.post('/promolocation', admin.getPromosForLocation);
app.post('/savepromopriority', admin.savePromoPriority);

//* INFO: api v2
app.use('/v2', router);
app.post('/analytics1', prefetchAnalyticsData); // TODO remove



// TODO: do we want a catch-all?
//app.get('*', checkForLogin, function(req, res) {
//  res.render('index');
//});

// catch 404 and forward to error handler
app.use(function (_, _, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
// CRON JOB
var job = new CronJob('*/40 * * * *', function () {
  prefetchAnalyticsData()
}, null, true, 'America/Los_Angeles');
job.start();

var updateAverageViewJob = new CronJob('0 0 * * 0', function () {
  // turn on when v2 publish
  // updateAverageSincePublished()
}, null, true, 'America/Los_Angeles');
updateAverageViewJob.start();

// error handler
app.use(function (err, req, res) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Connect to MySQL on start
db.connect(function (err) {
  if (err) {
    console.log('Unable to connect to MySQL.');
    process.exit(1);
  } else {
    console.log('Connected to MySQL')
  }
});

module.exports = app;
