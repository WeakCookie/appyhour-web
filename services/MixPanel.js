const MixpanelExport = require('mixpanel-data-export')
const panel = new MixpanelExport({
    api_key: "4f98c92e0ecf95d97d76d6eca2cb0298",
    api_secret: "3e9796ed74b2cdda13edc3bbe904f073"
})
const isEmpty = require('lodash/isEmpty')
const get = require('lodash/get')
const mergeWith = require('lodash/mergeWith')
const isNumber = require('lodash/isNumber')
const { subDays, isWithinInterval, parse } = require('date-fns')
const { DATE_FORMAT } = require('../constants/mixpanel')

// return to utils
function getYYYYMMDD(d) {
	var year = d.getYear() + 1900;
	var month = d.getMonth() + 1;
	if (month < 10)
		month = "0" + month;
	var day = d.getDate();
	if (day < 10)
		day = "0" + day;
	return year + "-" + month + "-" + day;
}

function getEventNames(data, type) {
	const SPECIAL_IMPRESSION_PREFIX = 'Impression-'
	const EVENT_IMPRESSION_PREFIX = 'EventImpression-'

	if (type === 'profile_view') {
		return data
	}

	return data.map(object => {
		if (object) {
			const isSpecial = type === 'special'
			const vendorId = isSpecial ? 'special_id' : 'event_id'
			const prefix = isSpecial ? SPECIAL_IMPRESSION_PREFIX : EVENT_IMPRESSION_PREFIX
			return `${prefix}${object[vendorId]}`
		}
	})
}

function betweenInterval(key, interval) {
	const date = key.split(' ')[0]
	let start = undefined
	let end = undefined
	let today = new Date()
	today.setHours(0, 0, 0, 0)

	if (interval === 0) {
		start = today
		end = today
	}
	if (interval === 1) {
		start = subDays(today, 1)
		end = subDays(today, 1)
	}
	if (interval > 1) {
		start = subDays(today, interval)
		end = subDays(today, 1)
	}

	return isWithinInterval(
		parse(date, DATE_FORMAT, new Date()),
		{ start, end }
	)
}

function betweenPreviousInterval(key, interval) {
	const date = key.split(' ')[0]
    let today = new Date()
	today.setHours(0, 0, 0, 0)
    const endThisInterval = subDays(today, 1)
    const startThisInterval = subDays(endThisInterval, interval)
    let endPreviousInterval
    let startPreviousInterval

    if (interval === 0) {
        startPreviousInterval = subDays(today, 1)
        endPreviousInterval = today
    }
    if (interval >= 1 ) {
        endPreviousInterval = subDays(startThisInterval, 1)
        startPreviousInterval = subDays(endPreviousInterval, interval)
    }

    return isWithinInterval(
		parse(date, DATE_FORMAT, new Date()),
		{ start: startPreviousInterval, end: endPreviousInterval }
	)
}

function getPercentageForDisplay(countPast, countPrevious) {
	let change = 0
	if (countPast > 0 && countPrevious > 0) {
		change = ((countPast - countPrevious) / countPrevious) * 100
		change = change.toFixed(0)
	}
	else if (countPast > 0 && countPrevious === 0) {
		change = 100
	}
	else if (countPast === 0 && countPrevious > 0) {
		change = -100
	}
	return Number(change)
}


/**
  * @param events events to get data from, string[]
  * @param unit month, week, day,...
  * @param from from date (Date)
  * @param to to date (Date)
  */
async function getEvents({ events, unit, from, to, type }) {
    const params = {
        event: events,
        type: type ? type : 'general',
        unit,
        from_date: getYYYYMMDD(from),
        to_date: getYYYYMMDD(to)
    }
    const data = await panel.events(params)
    if (data.error) {
        console.log('MixPanel Error : ', data.error)
    }

    return data
}

function getTotalView(values, interval) {
    if(interval === undefined) {
        return {}
    }

    let totalThisInterval = 0
    let totalPreviousInterval = 0

    for (const key in values) {
        if (betweenInterval(key, interval)) {
            totalThisInterval += values[key]
        } else if (betweenPreviousInterval(key, interval)) {
            totalPreviousInterval += values[key]
        }
    }

    return { totalViews: totalThisInterval, changes: getPercentageForDisplay(totalThisInterval, totalPreviousInterval) }
}

function getChartData(values, interval) {
    if (interval === undefined) {
        return {}
    }
    const chartDataArray = []
    for (const key in values) {
        if (betweenInterval(key, interval)) {
            chartDataArray.push({x: key, y: values[key]})
        }
    }

    return chartDataArray
}

function mergeDaysOfEvent(data) {
    const values = Object.values(data)

    const mergedDates = mergeWith({}, ...values, (objValue, srcValue) =>
        isNumber(objValue) ? objValue + srcValue : srcValue)

    return mergedDates
}

function extractInformationFromValues (values, interval) {
    if (isEmpty(values)) {
        return { totalViews: 0, chartData: {}, changes: 0 }
    }

    const mergedDays = mergeDaysOfEvent(values)
    const { totalViews, changes } = getTotalView(mergedDays, interval)
    const chartData = getChartData(mergedDays, interval)
    return { chartData, totalViews, changes}
}

exports.extractInformationFromValues = extractInformationFromValues

/**
  * @param data special + specials_archived | event + event_archived
  * @param type 'special' | 'event'
  * @param params { unit, from, to, type }
  */
exports.getViews = async function (data, type, params, interval) {
    if (isEmpty(data)) {
        return { totalViews: 0, details: {}, originalData: null }
    }

    const eventNames = getEventNames(data, type)
    if (eventNames.length > 100) {
        eventNames.length = 100 // mixpanel can only handle 100 events ?
    }
    const result = await getEvents({ ...params, events: eventNames })
    const values = get(result, 'data.values')
    const { chartData, totalViews, changes} = extractInformationFromValues(values, interval)
    return { details: chartData, originalData: values, totalViews, changes }
}