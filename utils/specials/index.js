const isEmpty = require('lodash/isEmpty')
const Common = require('../../models/common')
const Special = require('../../models/special')
const XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest
schedule = require('node-schedule')

const STARTING_HOUR_OF_WORKING_DAY = 4
const GOOGLE_FCM_ENDPOINT = 'https://fcm.googleapis.com/fcm/send'
const GOOGLE_FCM_KEY = 'key=AAAAmbcicQc:APA91bGb3m6HgUtAJmIQe8nCzQLfQffQiqicAYo5CicKGa-1YEhRqMnj_nXiRpMZR2fGyvXTHBI4KaACrIG5JgSFAfFoTjFFq0DDOqqhuLgIlnneMVzRbyU33ECIjF0EFk7gDX-uPch0KW7uIzmaWVBWYqP0ChJScA'

function insertSpecial(special, establishmentId, time) {
	var response = { sErr: null, sResult: null, esErr: null, esResult: null };
	Special.insert(special, function (err, result) {
		response.sErr = err;
		response.sResult = result;

		console.log(err);
		console.log(result);

		if (result.affectedRows > 0) {
			const establishment_special = {
				ESTABLISHMENT_SPECIAL: {
					establishment_id: establishmentId,
					special_id: result.insertId,
					created_at: time,
					updated_at: time
				}
			};
			EstablishmentSpecial.insert(establishment_special, function (err, result) {
				response.esErr = err;
				response.esResult = result;
				if (result.affectedRows > 0) {
					return response
				} else {
					return response
				}
			});
		} else {
			return response
		}
	})
}

function updateEditSpecial(special, specialIds, start) {
    if (start < specialIds.length) {
        Special.getAllBySpecialId(specialIds[start], function (err, result) {
            if (special.day == result.day) {
                const specialToUpdate = {
                    SPECIAL: {
                        special_id: result.special_id,
                        price: special.price,
                        price_type_ref_id: special.price_type_ref_id,
                        details: special.details,
                        deal_type_ref_id: special.deal_type_ref_id,
                        day: special.day,
                        start_time: special.start_time,
                        end_time: special.end_time,
                        dinein: special.dinein,
                        carryout: special.carryout,
                        updated_at: special.updated_at
                    }
                };
                Special.update(specialToUpdate, function (err, res) {
                    return
                })
            }
            else {
                updateEditSpecial(special, specialIds, ++start)
            }
        })
    }
    else {
        return
    }
}

function deleteSpecial(id, timezone) {
    var now = Common.newDate(timezone)
    var deleted = new Date(now)

    Special.getAllBySpecialId(id, function (err, special) {
        const special1 = {
            SPECIAL_ARCHIVE: {
                special_id: special1.special_id,
                price: special1.price,
                price_type_ref_id: special1.price_type_ref_id,
                details: special1.details,
                deal_type_ref_id: special1.deal_type_ref_id,
                day: special1.day,
                start_time: special1.start_time,
                end_time: special1.end_time,
                deleted_at: deleted,
                created_at: special1.created_at,
                updated_at: special1.updated_at
            }
        };

        if (special1.day == -1) {
            var job = schedule.scheduledJobs["special" + special1.special_id];
            if (job)
                job.cancel();
        }

        SpecialArchive.insert(special1, function (err, result1) {
            if (result1 != null && result1 != undefined && result1.affectedRows > 0) {
                Special.delete(id, function (err, result2) {
                    if (result2 != null && result2 != undefined && result2.affectedRows > 0) {
                        return;
                    } else {
                        //res.redirect('dashboard?page=info');
                    }
                });
            }
            else {
                //res.redirect('dashboard?page=info');
            }
        });
    });
}

function deleteEditSpecial(specialIds, day, timezone, start) {
    if (start < specialIds.length) {
        Special.getAllBySpecialId(specialIds[start], function (err, special) {
            if (special.day == day) {
                deleteSpecial(special.special_id, timezone)
            }
            else {
                deleteEditSpecial(specialIds, day, timezone, ++start)
            }
        });
    }
    else {
        return;
    }
}

function getNotificationDuration(hour, min) {
    if (hour == 0)
        return min + " minutes!"
    else if (hour == 1 && min == 0)
        return "hour!";
    else if (hour == 1 && min > 0)
        return hour + " hour " + min + " minutes!";
    else if (hour > 0 && min == 0)
        return hour + " hours!";
    else
        return hour + " hours " + min + " minutes!";
}

function getNotificationTitleOnPrice(price, priceType) {
    let title = ''
    if (priceType == 300)
        title = "$" + price
    else if (priceType == 301)
        title = price + "% Off"
    else if (priceType == 302)
        title = "BOGO"
    else if (priceType == 307)
        title = "Free"
    return title
}

function sendNotification({ price, priceType, details, topic, isScheduledSpecial, timezone, start, newSpecial }) {
    const request = new XMLHttpRequest()
    const title = getNotificationTitleOnPrice(price, priceType)

    const data = {
        "notification": {
            "title": `${title} ${details} for the next ${getNotificationDuration(hours, minutes)}`,
            "body": req.session.establishment.name
        },
        "to": `/topics/${topic}`
    };

    request.open("POST", GOOGLE_FCM_ENDPOINT, true)
    request.setRequestHeader("Content-Type", "application/json")
    request.setRequestHeader("Authorization", GOOGLE_FCM_KEY)

    if (isScheduledSpecial == "false") {
        request.send(JSON.stringify(data))
    }
    else {
        start.addHoursDST(timezone)
        start.setHours(start.getHours() - 1)
        //start.setHours(start.getHours()+6); // UTC issue (comment out locally) (Daylight Savings Time - Fall)
        //start.setHours(start.getHours()+5); // 5 hours for central time (Daylight Savings Time - Spring)
        schedule.scheduleJob("special" + newSpecial.insertId, start, function () {
            request.send(JSON.stringify(data))
        })
    }
}

function getTodaysSpecials(specials, timezone) {
    const today = Common.newDate(timezone)
    const currentWorkingDay = getCurrentWorkingDay(timezone) // get day of yesterday if before 4am, else get day of today

    // need today at 4am, so that we can see if exclusive special was created today
    const beginningOfWorkingDate = getBeginningOfWorkingDay(timezone, STARTING_HOUR_OF_WORKING_DAY)

    // need tomorrow date so we can assign to daily specials that run past midnight
    const tomorrow = Common.newDate(timezone)
    tomorrow.setDate(tomorrow.getDate() + 1)

    const expired = []
    const current = []
    const upcoming = []

    if (isEmpty(specials)) {
        return []
    }

    specials.forEach(special => {
        if (special.day == -1 || special.day == currentWorkingDay) {
            const context = {}
            context.dealType = special.deal_type_ref_id
            context.name = special.details
            context.price = {
                amount: special.price,
                type: special.price_type_ref_id
            }
            context.categories = {
                dineIn: !!special.dinein,
                takeOut: !!special.carryout
            }

            if (special.day == currentWorkingDay) {
                context.start = special.start_time.getHours() <= STARTING_HOUR_OF_WORKING_DAY ? tomorrow : today
                context.end = special.end_time.getHours() <= STARTING_HOUR_OF_WORKING_DAY ? tomorrow : today
            }

            // expired
            if (context.start > beginningOfWorkingDate && context.end < today) {
                expired.push(context)
            }
            // current
            else if (context.start <= today && context.end >= today) {
                current.push(context)
            }
            // upcoming weekly
            else if (special.day == currentWorkingDay && context.start > today) {
                upcoming.push(context)
            }
            // upcoming one-time
            else if (context.start > today && context.start.getDate() == today.getDate() && context.start.getMonth() == today.getMonth() && context.start.getYear() == today.getYear()) {
                upcoming.push(context)
            }
        }
    })

    return {
        expired: sortByStartTimeThenEndTime(expired),
        current: sortByEndTime(current),
        upcoming: sortByStartTimeThenEndTime(upcoming)
    }
}

function getDealTypeRefIdByOGId(id) {
    if (id == 0) {
        return 200; //beer
    } else if (id == 1) {
        return 202; //shot
    } else if (id == 2) {
        return 206; //mixed drink
    } else if (id == 3) {
        return 208; //margarita
    } else if (id == 4) {
        return 210; //martini
    } else if (id == 5) {
        return 212; //tumbler
    } else if (id == 6) {
        return 204; //wine
    } else if (id == 7) {
        return 251; //burger
    } else if (id == 8) {
        return 253; //appetizer
    } else if (id == 9) {
        return 255; //pizza
    } else if (id == 10) {
        return 257; //taco
    } else if (id == 11) {
        return 259; //sushi
    } else if (id == 12) {
        return 261; //bowl
    } else if (id == 13) {
        return 263; //wing
    } else if (id == 14) {
        return 214; //bottle
    } else if (id == 15) {
        return 216; //can
    }

    return 200;
}

function getCurrentWorkingDay(timezone) {
    const day = Common.newDate(timezone)

    // show yesterday'special date if before 4am
    if (day.getHours() < 4) {
        day.setDate(day.getDate() - 1)
    }

    return day.getDay()
}

function getBeginningOfWorkingDay(timezone, startingHour) {
    const workingDay = Common.newDate(timezone);
    if (workingDay.getHours() < startingHour) {
        workingDay.setDate(workingDay.getDate() - 1)
    }
    workingDay.setHours(startingHour)
    workingDay.setMinutes(0)
    workingDay.setSeconds(0)
    return workingDay
}

// use at FE
function getDealTypeImageByRefId(refId) {
    switch (refId) {
        case 200: return 'beer-icon.png'
        case 202: return 'shot-icon.png'
        case 204: return 'wine-icon.png'
        case 206: return 'mixed-drink-icon.png'
        case 208: return 'margarita-icon.png'
        case 210: return 'martini-icon.png'
        case 212: return 'tumbler-icon.png'
        case 214: return 'beerbottle.png'
        case 216: return 'beercan.png'
        case 251: return 'burger-icon.png'
        case 253: return 'appetizer-icon.png'
        case 255: return 'pizza-icon.png'
        case 257: return 'taco-icon.png'
        case 259: return 'sushi.png'
        case 261: return 'bowl.png'
        case 263: return 'chickenwing.png'
        default: return ''
    }
}

function sortByEndTime(specials) {
    var sorted = false;

    while (!sorted) {
        sorted = true;
        for (var index = 1; index < specials.length; index++) {
            var endA = new Date(specials[index - 1].end_time);
            var endB = new Date(specials[index].end_time);

            if (endA > endB) {
                var special = specials[index - 1];
                specials[index - 1] = specials[index];
                specials[index] = special;
                sorted = false;
            }
        }

        if (sorted)
            break;
    }

    return specials;
}

function sortByStartTimeThenEndTime(specials) {
    var sorted = false;

    while (!sorted) {
        sorted = true;
        for (var index = 1; index < specials.length; index++) {
            var startA = new Date(specials[index - 1].start_time);
            var startB = new Date(specials[index].start_time);
            var endA = new Date(specials[index - 1].end_time);
            var endB = new Date(specials[index].end_time);

            if (startA > startB || (startA.getTime() === startB.getTime() && endA > endB)) {
                var special = specials[index - 1];
                specials[index - 1] = specials[index];
                specials[index] = special;
                sorted = false;
            }
        }

        if (sorted)
            break;
    }

    return specials;
}

function getSpecialsRemainingForDay(specials, day, { needsToPay, cardOnFile, hasSubscription }) {
    const paying = !needsToPay || (cardOnFile && hasSubscription)
    let remaining = paying == true ? 5 : 1

    specials.forEach(special => {
        if (special.day == day && special.event_id == null) {
            remaining -= 1
        }
    })

    if (remaining < 0) {
        remaining = 0
    }

    return remaining
}

function getWeeklySpecials(specials) {
    // Each list in weeklySpecials corresponds to a day (item 0 in list is Sunday, 6 is Saturday)
    const weeklySpecials = [[], [], [], [], [], [], []]

    if (isEmpty(specials)) {
        return []
    }

    specials.filter(special => special.day !== -1).map(special => {
        const context = {}
        context.day = special.day
        context.dealType = special.deal_type_ref_id
        context.start_time = special.start_time
        context.end_time = special.end_time
        context.name = special.details
        context.price = {
            amount: special.price,
            type: special.price_type_ref_id
        }
        context.categories = {
            dineIn: !!special.dinein,
            takeOut: !!special.carryout
        }

        if (context.day > 0)
            context.day = context.day - 1
        else
            context.day = 6

        weeklySpecials[context.day].push(context)
    })

    return weeklySpecials.map(weeklySpecial => sortByStartTimeThenEndTime(weeklySpecial))
}

function getExclusiveSpecials(specials, week, timezone) {
    let addDays = 1
    if (week === 0)
        addDays = 1
    else if (week === 1)
        addDays = 8
    else
        addDays = 15

    const currFirst = Common.newDate(timezone)
    currFirst.setHours(0, 0, 0, 0)
    const currLast = Common.newDate(timezone)
    currLast.setHours(0, 0, 0, 0)
    let first = 0
    const now = Common.newDate(timezone)

    if (currFirst.getDay() != 0) {
        first = currFirst.getDate() - currFirst.getDay() + addDays
    }
    else {
        first = currFirst.getDate() - 7 + addDays
    }

    const last = first + 7
    const firstDayOfWeek = new Date(currFirst.setDate(first))
    const lastDayOfWeek = new Date(currLast.setDate(last))

    const expired = []
    const current = []
    const upcoming = []

    specials.map(special => {
        if (special.day == -1 && special.event_id == null) {
            if (special.start_time > firstDayOfWeek && special.end_time < lastDayOfWeek) {
                const context = {}
                context.start_time = special.start_time
                context.end_time = special.end_time
                context.dealType = special.deal_type_ref_id
                context.name = special.details
                context.price = {
                    amount: special.price,
                    type: special.price_type_ref_id
                }
                context.categories = {
                    dineIn: !!special.dinein,
                    takeOut: !!special.carryout
                }

                if (context.end_time < now) {
                    expired.push(context)
                }
                else if (context.start_time < now && context.end_time > now) {
                    current.push(context)
                }
                else if (context.start_time > now) {
                    upcoming.push(context)
                }
            }
        }
    })

    const remaining = Math.max(1 - current.length, 0)

    return {
        expired: sortByStartTimeThenEndTime(expired),
        current: sortByEndTime(current),
        upcoming: sortByStartTimeThenEndTime(upcoming),
        remaining
    }
}

module.exports = {
    getTodaysSpecials,
    getWeeklySpecials,
    getExclusiveSpecials,
    getSpecialsRemainingForDay,
    sendNotification,

    getDealTypeRefIdByOGId,
    updateEditSpecial,
    deleteEditSpecial,
    insertSpecial,
    deleteSpecial
}