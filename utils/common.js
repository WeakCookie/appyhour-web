
const jwt = require('jsonwebtoken')

exports.checkForLogin = function checkForLogin(req, res, next) {
  if(req.user) {
    res.redirect('/dashboard');
  }
  else if(req.session.supplier) {
    res.redirect('/supplierdashboard');
  }
  else {
    next();
  }
}

exports.requireLogin = function requireLogin(req, res, next) {
  if (!req.user) {
    res.redirect('/estlogin');
  } else {
    next();
  }
}

exports.authenticateToken = function authenticateToken(req, res, next) {
  const authHeader = req.headers['authorization']
  const token = authHeader && authHeader.split(' ')[1]

  if (token == null) return res.sendStatus(401)

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    console.log(err)

    if (err) return res.sendStatus(403)

    req.user = user

    next()
  })
}