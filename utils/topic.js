const { Timezones } = require('../constants')

function convertTopicToTimezone(topic) {
    if (topic== "denverco") {
        return Timezones.MST
    }
    else if (topic== "desmoinesia" || topic== "iowacityia" || topic== "minneapolismn") {
        return Timezones.CST
    }
    else if (topic== "louisvilleky" || topic== "tampabayfl") {
        return Timezones.EST
    }
    else {
        return undefined
    }
}

module.exports = {
    convertTopicToTimezone
}