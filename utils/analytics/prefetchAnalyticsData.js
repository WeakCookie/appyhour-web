const Establishment = require('../../models/establishment.js');
const AnalyticsCache = require('../../models/analytics_cache.js');
const User = require('../../models/user')
const { convertAnalyticsCacheJsonToDbObject, getPrefetchBody } = require('./data')
const { convertTopicToTimezone } = require('../topic')
const precalculateAnalytics = require('../../utils/analytics/precalculateAnalyticsData');

const BATCH_SIZE = 100
var numberOfBatches = 0
var currentBatch = -1

async function getCurrentBatch(currentBatch) {
  try {
    const currentEstablishmentBatch = await Establishment.getAllEstablishmentsAsync([currentBatch * BATCH_SIZE, BATCH_SIZE])
    return currentEstablishmentBatch
  } catch (error) {
    console.log('getCurrentBatch -> Error : ', error)
    return []
  }
}

exports.prefetchAnalyticsData = async () => {
  currentBatch = currentBatch + 1
  if (currentBatch === numberOfBatches) {
    currentBatch = 0
    const establishmentsCount = await Establishment.countAllEstablishmentsAsync()
    numberOfBatches = Math.ceil(establishmentsCount / BATCH_SIZE)
  }
  const currentEstablishmentBatch = await getCurrentBatch(currentBatch)

  setImmediate(async () => {
    for (const index in currentEstablishmentBatch) {
      try {
        const timezone = convertTopicToTimezone(currentEstablishmentBatch[index].topic)
        const user = await User.getAllByEstablishmentIdAsync(currentEstablishmentBatch[index].establishment_id)
        const body = getPrefetchBody(currentEstablishmentBatch[index].establishment_id, timezone, user)
        const result = await precalculateAnalytics.getAnalyticsCalculator(body)
        const dbObject = convertAnalyticsCacheJsonToDbObject(currentEstablishmentBatch[index].establishment_id, timezone, result)
        console.log('check')
        AnalyticsCache.upsert(dbObject)
      } catch (error) {
        console.log(error)
        continue
      }
    }
  })

  return
}