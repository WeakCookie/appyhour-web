const { subDays, isWithinInterval, parse, addDays, format, isThisWeek } = require('date-fns')
var Common = require('../../models/common.js')
const Special = require('../../models/special')
const SpecialArchive = require('../../models/special_archive')
const Event = require('../../models/event')
const EventArchive = require('../../models/event_archive')
const Establishment = require('../../models/establishment')
const AnalyticsCache = require('../../models/analytics_cache_v2')
const MixPanel = require('../../services/MixPanel')
const isEmpty = require('lodash/isEmpty')
const countBy = require('lodash/countBy')
const get = require('lodash/get')
const { DATE_FORMAT, DataTypes } = require('../../constants/mixpanel')
const { DB_DATE_FORMAT } = require('../../constants/date')

function getLastWeek(timezone) {
	const today = Common.newDate(timezone)
	const weekStart = new Date(today.getFullYear(), today.getMonth(), today.getDate() - today.getDay() + 1)
	return { start: subDays(weekStart, 7), end: subDays(weekStart, 1) }
}

function getPreviousWeek(startLastWeek) {
	return { start: subDays(startLastWeek, 7), end: subDays(startLastWeek, 1) }
}

// d = dateOfView, start_time, day
// if day = -1, get string date of start_time, else get string date of dateOfView
function getDateStr(d, sStart, day) {
	var month;
	var date;
	var year;

	if (day == -1) {
		month = sStart.getMonth() + 1;
		if (month < 10)
			month = "0" + month;
		date = sStart.getDate();
		if (date < 10)
			date = "0" + date;
		year = sStart.getYear() + 1900;
	}
	else {
		if (day == 0)
			day = 7;
		var newDate = new Date(d);
		newDate.setDate(newDate.getDate() + day);

		month = newDate.getMonth() + 1;
		if (month < 10)
			month = "0" + month;
		date = newDate.getDate();
		if (date < 10)
			date = "0" + date;
		year = newDate.getYear() + 1900;
	}

	return month + "/" + date + "/" + year;
}

function getTimeStr(datetime) {
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}

	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function getYYYYMMDD(d) {
	var year = d.getYear() + 1900;
	var month = d.getMonth() + 1;
	if (month < 10)
		month = "0" + month;
	var day = d.getDate();
	if (day < 10)
		day = "0" + day;
	return year + "-" + month + "-" + day;
}

function intDayToString(day) {
	var weekdays = new Array(7);
	weekdays[0] = "Sunday";
	weekdays[1] = "Monday";
	weekdays[2] = "Tuesday";
	weekdays[3] = "Wednesday";
	weekdays[4] = "Thursday";
	weekdays[5] = "Friday";
	weekdays[6] = "Saturday";
	if (day >= 0 && day <= 6)
		return weekdays[day];
	else
		return "";
}

function dateToStr(d) {
	var month;
	var date;
	var year;
	var hours;
	var minutes;
	var ampm;

	month = d.getMonth() + 1;
	if (month < 10)
		month = "0" + month;

	date = d.getDate();
	if (date < 10)
		date = "0" + date;

	year = d.getYear() + 1900;

	hours = d.getHours();
	if (hours > 12) {
		hours = hours - 12;
		ampm = "PM";
	}
	else if (hours == 12) {
		ampm = "PM";
	}
	else if (hours < 12 && hours != 0) {
		ampm = "AM";
	}
	else if (hours == 0) {
		hours = 12;
		ampm = "AM";
	}

	minutes = d.getMinutes();
	if (minutes < 10) {
		minutes = "0" + minutes;
	}

	return month + "/" + date + "/" + year + " " + hours + ":" + minutes + " " + ampm;
}

function calculateDistance(lat1, lon1, lat2, lon2) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1 / 180;
		var radlat2 = Math.PI * lat2 / 180;
		var theta = lon1 - lon2;
		var radtheta = Math.PI * theta / 180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180 / Math.PI;
		dist = dist * 60 * 1.1515;
		return dist;
	}
}

function setEstablishmentStatisticBasedOnLocation(latitude, longitude, location, profiles, establishment, establishmentStatistic, timezone) {
	const distance = calculateDistance(establishment.latitude, establishment.longitude, latitude, longitude)
	if (distance <= 30) {
		establishmentStatistic.location = location

		for (let index in profiles) {
			const profileDistance = calculateDistance(profiles[index].location_lat, profiles[index].location_lon, latitude, longitude)
			if (profileDistance <= 30) {
				establishmentStatistic.profs += 1
				const today = Common.newDate(timezone)
				const lastWeek = today.setDate(today.getDate() - 7)
				if (profiles[index].created_at <= lastWeek) {
					establishmentStatistic.profsLastWeek += 1
				}
			}
		}
	}
}

function getNumberOfDayUntilWeekEnd(today) {
	let weekEnd = today.getDay()
	if (weekEnd === 0)
		weekEnd = 7
	weekEnd = 7 - weekEnd;
	weekEnd = "+" + weekEnd + "d"
	return weekEnd
}

// interval = 0 is today, 1 is yesterday, 7 for last 7 days
function getParamsBasedOnInterval() {
	// const unit = interval > 1 ? 'day' : 'hour'
	// let intervalMapping = interval

	// if (interval === 0 || interval === 1) {
	// 	intervalMapping = interval + 1
	// } else {
	// 	intervalMapping = interval * 2
	// }

	return {
		unit: 'day',
		from: subDays(new Date(), 56),
		to: new Date()
	}
}

/**
  * @param data special + specials_archived | event + event_archived
  * @param type 'special' | 'event' | 'profile_view'
  */
function getEventNames(data, type) {
	const SPECIAL_IMPRESSION_PREFIX = 'Impression-'
	const EVENT_IMPRESSION_PREFIX = 'EventImpression-'

	if (type === 'profile_view') {
		return data
	}

	return data.map(object => {
		if (object) {
			const isSpecial = type === 'special'
			const vendorId = isSpecial ? 'special_id' : 'event_id'
			const prefix = isSpecial ? SPECIAL_IMPRESSION_PREFIX : EVENT_IMPRESSION_PREFIX
			return `${prefix}${object[vendorId]}`
		}
	})
}

/*
	EG : today is 3/6 -> start = 27/5, end = 2/6
*/
function getStartAndEndThisInterval(timezone, interval) {
	const daysAgo = interval > 1 ? interval - 1 : interval
	let endThisInterval = Common.newDate(timezone)
	endThisInterval = subDays(endThisInterval, 1)
	endThisInterval.setHours(0, 0, 0, 0)
	const startThisInterval = new Date(endThisInterval)
	endThisInterval = daysAgo === 0 ? Common.newDate(timezone) : endThisInterval
	startThisInterval.setDate(startThisInterval.getDate() - daysAgo)
	return { startThisInterval, endThisInterval }
}

/*
	EG : today is 3/6 -> start = 20/5, end = 27/5
*/
function getPreviousStartAndEndInterval(startThisInterval, interval) {
	const endPreviousInterval = new Date(startThisInterval)
	const startPreviousInterval = new Date(endPreviousInterval)
	startPreviousInterval.setDate(startPreviousInterval.getDate() - interval)
	return { startPreviousInterval, endPreviousInterval }
}

function mapSpecialDetailForDisplay(special) {
	const { price_type_ref_id, price, details } = special
	if (price_type_ref_id === 300)
		return `$ ${price.toFixed(2)} ${details}`
	else if (price_type_ref_id === 301)
		return `${price}% Off ${details}`
	else if (price_type_ref_id === 302)
		return `BOGO ${details}`
	else if (price_type_ref_id === 307)
		return `Free ${details}`
	else
		return details
}

function calculateAverageSincePublishedForAllEvents(values) {
	const mapping = {}
	for (const event in values) {
		const totalOfAllWeeks = Object.values(values[event]).filter(total => total > 0)
		let averageOfAllWeeks = 0
		if (totalOfAllWeeks.length > 0) {
			averageOfAllWeeks = totalOfAllWeeks.reduce((previous, total) => previous + total, 0) / totalOfAllWeeks.length
		}
		mapping[event] = averageOfAllWeeks
	}

	return mapping
}

function betweenInterval(key, interval) {
	const date = key.split(' ')[0]
	let start = undefined
	let end = undefined
	let today = new Date()
	today.setHours(0, 0, 0, 0)

	if (interval === 0) {
		start = today
		end = today
	}
	if (interval === 1) {
		start = subDays(today, 1)
		end = subDays(today, 1)
	}
	if (interval > 1) {
		start = subDays(today, interval + 1)
		end = subDays(today, 1)
	}

	return isWithinInterval(
		parse(date, DATE_FORMAT, new Date()),
		{ start, end }
	)
}

function isWithinWeek(date, week) {
	const result = isWithinInterval(
		date,
		{ start: week.start, end: week.end }
	)

	return result
}

function getPercentageForDisplay(countPast, countPrevious) {
	let percentage = 0
	if (countPast > 0 && countPrevious > 0) {
		percentage = ((countPast - countPrevious) / countPrevious) * 100
		percentage = parseInt(percentage.toFixed(0))
	}
	else if (countPast > 0 && countPrevious === 0) {
		percentage = 100
	}
	else if (countPast === 0 && countPrevious > 0) {
		percentage = -100
	}
	return Number(percentage)
}

function getSpecialWeeklyImpressions(values, specials, timezone, interval) {
	const weeklyImpressions = []
	const lastWeek = getLastWeek(timezone)
	const previousWeek = getPreviousWeek(lastWeek.start)

	for (const impression in values) {
		const dates = Object.keys(values[impression])
		const views = Object.values(values[impression])
		const specialId = Number(impression.split('-')[1])
		const correspondSpecial = specials.find(special => special.special_id === specialId)

		const context = {}
		context.specialId = specialId
		context.start = correspondSpecial.start_time
		context.end = correspondSpecial.end_time
		context.day = correspondSpecial.day
		context.event = correspondSpecial.event
		context.views = {
			thisWeek: 0,
			lastWeek: 0,
			changes: {
				thisWeek: 0,
				lastWeek: 0
			}
		}
		let previousWeekViewCount = 0
		context.createdAt = dateToStr(correspondSpecial.created_at)
		context.details = mapSpecialDetailForDisplay(correspondSpecial)
		if (correspondSpecial.deleted_at != undefined) {
			context.deleted = correspondSpecial.deleted_at
		}

		for (const v in views) {
			const dateOfView = new Date(dates[v])

			dateOfView.subtractHoursDST(timezone)
			context.date = getDateStr(dateOfView, correspondSpecial.start_time, correspondSpecial.day)

			const contextDate = new Date(context.date)
			const objCreated = new Date(correspondSpecial.created_at)

			const nextContextDate = addDays(new Date(contextDate), 1)

			const intervalOfView = dateOfView
			const nextIntervalOfView = addDays(new Date(intervalOfView), interval)

			const isActive = context.day > -1
			const startInNextIntervalOfView = intervalOfView <= context.start && nextIntervalOfView > context.start
			const createdBeforeContextDate = objCreated < nextContextDate
			const deletedAfterContextDate = (context.deleted != undefined && context.deleted > contextDate)
			const isDeleted = context.deleted != undefined

			if ((!isActive && startInNextIntervalOfView) || (isActive && createdBeforeContextDate)) {
				if (!isDeleted || deletedAfterContextDate) {
					// if ((!isActive && startInThisInterval) || (isActive && contextDateInThisInterval)) {
					// 	viewCount += views[v]
					// }
					// else if ((!isActive && startPreviousInterval <= context.start && endPreviousInterval > context.start) ||
					// 	(isActive && startPreviousInterval <= contextDate && endPreviousInterval > contextDate)) {
					// 	viewCountPrevious += views[v]
					// }

					if (isThisWeek(dateOfView, { weekStartsOn: 1 })) {
						context.views.thisWeek += views[v]
						context.return = true
					}

					if (isWithinWeek(dateOfView, lastWeek)) {
						context.views.lastWeek += views[v]
						context.return = true
					}

					if (isWithinWeek(dateOfView, previousWeek)) {
						previousWeekViewCount += views[v]
						context.return = true
					}
				}
			}
		}

		if (context.return) {
			context.views.changes.thisWeek = getPercentageForDisplay(context.views.thisWeek, context.views.lastWeek)
			context.views.changes.lastWeek = getPercentageForDisplay(context.views.lastWeek, previousWeekViewCount)
			weeklyImpressions.push(context)
		}
	}

	return weeklyImpressions
}

function getEventWeeklyImpressions(values, events, timezone, interval) {
	if (!values) {
		return {}
	}
	const weeklyImpressions = []
	const lastWeek = getLastWeek(timezone)
	const previousWeek = getPreviousWeek(lastWeek.start)
	let previousWeekViewCount = 0
	Object.keys(values).map(impression => {
		const dates = Object.keys(values[impression])
		const views = Object.values(values[impression])

		const context = {}
		context.views = {
			thisWeek: 0,
			lastWeek: 0,
			changes: {
				thisWeek: 0,
				lastWeek: 0
			}
		}
		const specialId = Number(impression.split('-')[1])
		const correspondingEvent = events.find(event => event.event_id === specialId)
		context.createdAt = dateToStr(correspondingEvent.created_at)
		context.specialId = specialId
		context.start = correspondingEvent.start_time
		context.end = correspondingEvent.end_time
		context.title = correspondingEvent.title
		context.day = correspondingEvent.day

		if (correspondingEvent.deleted_at) {
			context.deleted = dateToStr(correspondingEvent.deleted_at)
		}

		for (let v in views) {
			const dateOfView = new Date(dates[v])
			dateOfView.addHoursDST(timezone)

			if (isThisWeek(dateOfView, { weekStartsOn: 1 })) {
				context.views.thisWeek += views[v]
				context.return = true
			}

			if (isWithinWeek(dateOfView, lastWeek)) {
				context.views.lastWeek += views[v]
				context.return = true
			}

			if (isWithinWeek(dateOfView, previousWeek)) {
				previousWeekViewCount += views[v]
				context.return = true
			}
		}

		if (context.return) {
			context.views.changes.thisWeek = getPercentageForDisplay(context.views.thisWeek, context.views.lastWeek)
			context.views.changes.lastWeek = getPercentageForDisplay(context.views.lastWeek, previousWeekViewCount)
			weeklyImpressions.push(context)
		}
	})

	return weeklyImpressions
}

function calculatePercentageChangeForProfileViews(values, timezone, interval) {
	if (!values) {
		return {}
	}
	let viewCount = 0
	let viewCountPrevious = 0
	const chartData = {}
	const { startThisInterval, endThisInterval } = getStartAndEndThisInterval(timezone, interval)
	const { startPreviousInterval, endPreviousInterval } = getPreviousStartAndEndInterval(startThisInterval, interval)
	for (var d in values) {
		var dates = Object.keys(values[d])
		var views = Object.values(values[d])

		for (var v in views) {
			var context = {}
			var objKeyDt = new Date(dates[v])
			objKeyDt.addHoursDST(timezone)
			context.date = dateToStr(objKeyDt)

			var contextDate = new Date(context.date)
			if (contextDate >= startThisInterval && contextDate <= endThisInterval) {
				viewCount += parseInt(views[v])
				if (!chartData[dates[v]]) {
					chartData[dates[v]] = 0
				}
				chartData[dates[v]] += views[v]
			}
			else if (contextDate >= startPreviousInterval && contextDate <= endPreviousInterval) {
				viewCountPrevious += parseInt(views[v])
			}
		}
	}

	return {
		chartData,
		totalView: viewCount,
		changes: getPercentageForDisplay(viewCount, viewCountPrevious)
	}
}

function generateEmptyChartData(timezone, interval) {
	const chartData = []
	const { startThisInterval, endThisInterval } = getStartAndEndThisInterval(timezone, interval)
	let start = startThisInterval
	while(start <= endThisInterval) {
		const formattedDate = format(start, DATE_FORMAT)
		chartData.push({x: formattedDate, y: 0})
		start = addDays(start, 1)
	}

	return chartData
}

function inRange(value, start, end) {
	return !!(value >= start && value <= end)
}

function calculatePercentageChangeForFavorites(favorites, timezone, interval) {
	const chartData = generateEmptyChartData(timezone, interval)
	const { startThisInterval, endThisInterval } = getStartAndEndThisInterval(timezone, interval)
	const { startPreviousInterval, endPreviousInterval } = getPreviousStartAndEndInterval(startThisInterval, interval)
	chartData.map((chartItem) => ({
		x: chartItem.x,
		y: countBy(favorites, (favorite) => format(favorite.created_at, DATE_FORMAT) === chartItem.x)
	}))
	
	function checkCreatedAtInRange(item) {
		return inRange(item.created_at, startThisInterval, endThisInterval)
	}

	function checkCreatedAtInPreviousRange(item) {
		return inRange(item.created_at, startPreviousInterval, endPreviousInterval)
	}

	const count = countBy(favorites, checkCreatedAtInRange);
  const countPrevious = countBy(favorites, checkCreatedAtInPreviousRange);
	const viewCount = isEmpty(get(count, 'true', 0)) ? 0 : count.true
	const viewCountPrevious = isEmpty(get(countPrevious, 'true', 0)) ? 0 : countPrevious.true

	return {
		chartData,
		totalViews: viewCount,
		changes: getPercentageForDisplay(viewCount, viewCountPrevious)
	}
}

async function getAverageMapping(establishment, fetchedData) {
	const establishmentId = establishment.establishment_id
	const { created_at } = establishment
	const yearAgo = format(subDays(new Date(), 365), DB_DATE_FORMAT)
	const params = [establishmentId, yearAgo]
	const mixPanelParams = {
		unit: 'week',
		type: 'general',
		from: created_at,
		to: new Date()
	}
	let allSpecials = []
	let allEvents = []

	if (isEmpty(fetchedData)) {
		let [
			specials,
			specialArchives,
			events,
			eventArchives
		] = await Promise.all([
			Special.getAllByEstablishmentIdForYearAsync(params),
			SpecialArchive.getAllByEstablishmentIdForYearAsync([...params, yearAgo]),
			Event.getAllByEstablishmentIdForYearAsync(params),
			EventArchive.getAllByEstablishmentIdForYearAsync(params)
		])
		allSpecials = [...specials, ...specialArchives]
		allEvents = [...events, ...eventArchives]
	} else {
		allSpecials = fetchedData.special
		allEvents = fetchedData.event
	}

	let [
		specialData,
		eventData
	] = await Promise.all([
		MixPanel.getViews(allSpecials, DataTypes.SPECIAL, mixPanelParams),
		MixPanel.getViews(allEvents, DataTypes.EVENT, mixPanelParams)
	])

	const specialAverageMapping = calculateAverageSincePublishedForAllEvents(specialData.originalData)
	const eventAverageMapping = calculateAverageSincePublishedForAllEvents(eventData.originalData)

	return {
		special: specialAverageMapping,
		event: eventAverageMapping,
		updatedAt: new Date()
	}
}

function mapAverageSincePublishedToImpressions(averageMappings, weeklyImpressions) {
	return Object.keys(averageMappings).map(event => {
		const specialId = event.split('-')[1]
		const weeklyImpression = weeklyImpressions.find(weeklyImpression => weeklyImpression.specialId == specialId)

		if (isEmpty(weeklyImpression)) {
			return false
		}

		weeklyImpression.views.averageSincePublished = averageMappings[event]
		return weeklyImpression
	}).filter(Boolean)
}

async function updateAverageSincePublished() {
	const caches = await AnalyticsCache.getAllAsync()

	setImmediate(async () => {
		for (const index in caches) {
			const { establishmentId } = caches[index]
			const existingEstablishment = await Establishment.getByIdAsync(establishmentId)
			const averageMapping = await getAverageMapping(existingEstablishment)
			averageMapping.updatedAt = new Date()
			AnalyticsCache.upsert({ ...caches[index], averageMapping })
		}
	})

	return
}

module.exports = {
	getDateStr,
	getTimeStr,
	getYYYYMMDD,
	intDayToString,
	dateToStr,
	setEstablishmentStatisticBasedOnLocation,
	getNumberOfDayUntilWeekEnd,
	getParamsBasedOnInterval,
	getEventNames,
	betweenInterval,
	calculatePercentageChangeForFavorites,
	getSpecialWeeklyImpressions,
	getEventWeeklyImpressions,
	calculatePercentageChangeForProfileViews,
	getPercentageForDisplay,
	getAverageMapping,
	mapAverageSincePublishedToImpressions,
	updateAverageSincePublished
}