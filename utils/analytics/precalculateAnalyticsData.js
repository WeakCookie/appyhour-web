var Common = require('../../models/common.js')
var Establishment = require('../../models/establishment.js')
var Special = require('../../models/special.js')
var SpecialArchive = require('../../models/special_archive.js')
var Event = require('../../models/event.js');
var EventArchive = require('../../models/event_archive.js')
var Favorites = require('../../models/favorites.js');
var MixpanelExport = require('mixpanel-data-export')
var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
// var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
var { getDateStr, getTimeStr, getYYYYMMDD, intDayToString, dateToStr, setEstablishmentStatisticBasedOnLocation, getNumberOfDayUntilWeekEnd } = require('.')
var { checkRateLimitError } = require('./data')
const { EstablishmentName, Locations } = require('../../constants')
const { PanelEventTypes, PanelEventUnits } = require('../../constants/mixpanel.js')
const get = require('lodash/get')

var panel = new MixpanelExport({
	api_key: "4f98c92e0ecf95d97d76d6eca2cb0298",
	api_secret: "3e9796ed74b2cdda13edc3bbe904f073"
})

exports.getAnalyticsCalculator = function (prefetchData) {
	const currentDate = Common.newDate(prefetchData.tz) // Get current date base on timezone
	const toDate = getYYYYMMDD(currentDate) // convert current date to another format for mixpanel
	const weekEnd = getNumberOfDayUntilWeekEnd(currentDate)
	const weekStart = getYYYYMMDD(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay() + 1))
	currentDate.setDate(currentDate.getDate() - 365)


	const yearAgo = getYYYYMMDD(new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() - currentDate.getDay())) + " 00:00:00";
	const establishmentId = prefetchData.establishment.establishment_id

	return new Promise((resolve, reject) => {
		Establishment.getEverythingByEstablishmentId(establishmentId, function (_, establishment) {
			if (!establishment) {
				reject('Establishment not found')
			}
			var params = [establishment.establishment_id, yearAgo];
			Special.getAllByEstablishmentIdForYear(params, function (_, specials) {
				Event.getAllByEstablishmentIdForYear(params, function (_, events) {

					EventArchive.getAllByEstablishmentIdForYear(params, function (_, eventsArc) {
						var params = [establishment.establishment_id, yearAgo, yearAgo];

						SpecialArchive.getAllByEstablishmentIdForYear(params, function (_, specialsArc) {

							Favorites.getAllByEstablishmentId(establishment.establishment_id, function (_, favs) {
								for (var f in favs) {
									favs[f].createdAt = dateToStr(favs[f].created_at);
								}

								Favorites.getAllProfiles(function (_, profiles) {
									const profileStatistic = {
										location: 0,
										profs: 0,
										profsLastWeek: 0
									}

									var cardOnFile = false;
									var hasSubscription = false;
									var needsToPay = false;
									var favorites = 0;
									var favoritesLastWeek = 0;
									var imp = [];
									var impPastWeek = 0;
									var impPrevWeek = 0;
									var impChange = 0;
									var eventImp = [];
									var eventImpPastWeek = 0;
									var eventImpPrevWeek = 0;
									var eventImpChange = 0;
									var isAppyHour = false;
									var endWeek = Common.newDate(prefetchData.tz);
									endWeek.setDate(endWeek.getDate() - 1);
									endWeek.setHours(0, 0, 0, 0);
									var startWeek = new Date(endWeek);
									startWeek.setDate(startWeek.getDate() - 6);
									var endWeekPrev = new Date(startWeek);
									var startWeekPrev = new Date(endWeekPrev);
									startWeekPrev.setDate(startWeekPrev.getDate() - 7);

									if (establishment.name == EstablishmentName.APPY_HOUR)
										isAppyHour = true;

									// ACTIVE USERS
									const { MS, IC, DEN, LOU } = Locations
									setEstablishmentStatisticBasedOnLocation(MS.latitude, MS.longitude, MS.name, profiles, establishment, profileStatistic, prefetchData.tz)
									setEstablishmentStatisticBasedOnLocation(IC.latitude, IC.longitude, IC.name, profiles, establishment, profileStatistic, prefetchData.tz)
									setEstablishmentStatisticBasedOnLocation(DEN.latitude, DEN.longitude, DEN.name, profiles, establishment, profileStatistic, prefetchData.tz)
									setEstablishmentStatisticBasedOnLocation(LOU.latitude, LOU.longitude, LOU.name, profiles, establishment, profileStatistic, prefetchData.tz)

									const { location, profs, profsLastWeek } = profileStatistic

									var profsChange = 0;
									if (profs != profsLastWeek && profsLastWeek > 0) {
										profsChange = ((profs - profsLastWeek) / profsLastWeek) * 100;
										profsChange = profsChange.toFixed(0);
									}
									else if (profs > profsLastWeek && profsLastWeek == 0) {
										profsChange = 100;
									}

									// FAVORITES
									favorites = 0;

									for (var f in favs) {
										if (favs[f].created_at >= startWeek && favs[f].created_at <= endWeek) {
											favorites += 1;
										}
										else if (favs[f].created_at >= startWeekPrev && favs[f].created_at <= endWeekPrev) {
											favoritesLastWeek += 1;
										}
									}

									var favoritesChange = 0;
									if (favorites != favoritesLastWeek && favoritesLastWeek > 0) {
										favoritesChange = ((favorites - favoritesLastWeek) / favoritesLastWeek) * 100;
										favoritesChange = favoritesChange.toFixed(0);
									}
									else if (favorites > favoritesLastWeek && favoritesLastWeek == 0) {
										favoritesChange = 100;
									}

									// SPECIAL IMPRESSIONS
									for (var x in specialsArc) {
										specials.push(specialsArc[x]);
									}

									var impressions = [];
									// mixpanel can only handle 100 events
									for (var s = 0; s < 100; s++) {
										if (specials[s] !== undefined) {
											impressions.push("Impression-" + specials[s].special_id);
										}
										else {
											break;
										}
									}
									panel.events({
										event: impressions,
										type: PanelEventTypes.GENERAL,
										unit: PanelEventUnits.WEEK,
										from_date: "2019-01-01",
										to_date: toDate
									}).then(function (data) {
										if (data.error) {
											if (checkRateLimitError(data.error, establishmentId))
												reject('rate limited')
										}
										if (get(data, 'data.values')) { // done
											var vals = get(data, 'data.values')

											for (var d in vals) {
												var objKeys = Object.keys(vals[d]); // eg Impression-11101
												var objVals = Object.values(vals[d]); // eg [1,2,3,4]

												for (var v in objVals) {
													var impObj = {};
													impObj.specialId = d.substring(d.indexOf("-") + 1);
													impObj.impressions = objVals[v]; // imp count

													var objDate;
													var objWeek;
													var objCreated;

													for (var sp in specials) {
														if (specials[sp].special_id == impObj.specialId) {
															if (specials[sp].price_type_ref_id == 300)
																impObj.details = "$" + specials[sp].price.toFixed(2) + " " + specials[sp].details;
															else if (specials[sp].price_type_ref_id == 301)
																impObj.details = specials[sp].price + "% Off " + specials[sp].details;
															else if (specials[sp].price_type_ref_id == 302)
																impObj.details = "BOGO " + specials[sp].details;
															else if (specials[sp].price_type_ref_id == 307)
																impObj.details = "Free " + specials[sp].details;

															impObj.dealType = specials[sp].deal_type_ref_id;

															var newDate = new Date(objKeys[v]);
															//newDate.setHours(newDate.getHours()-5); // UTC issue
															newDate.subtractHoursDST(prefetchData.tz);

															impObj.start = specials[sp].start_time;
															impObj.date = getDateStr(newDate, specials[sp].start_time, specials[sp].day);
															impObj.time = getTimeStr(specials[sp].start_time) + " - " + getTimeStr(specials[sp].end_time);
															impObj.day = specials[sp].day;
															impObj.createdAt = dateToStr(specials[sp].created_at);
															var impDate = new Date(objKeys[v] + " 00:00:00");
															impObj.impDate = dateToStr(impDate);

															if (specials[sp].deleted_at != undefined) {
																impObj.deleted = specials[sp].deleted_at;
																impObj.status = "Deleted";
																impObj.status2 = dateToStr(impObj.deleted);
															}
															else if (impObj.day > -1) {
																impObj.status = "Active every " + intDayToString(impObj.day);
															}

															if (specials[sp].event)
																impObj.event = specials[sp].event;
															else if (specials[sp].eventArc)
																impObj.event = specials[sp].eventArc;

															objDate = new Date(impObj.date);
															objWeek = newDate;
															objCreated = new Date(specials[sp].created_at);
															break;
														}
													}

													var nextDay = new Date(objDate);
													nextDay.setDate(nextDay.getDate() + 1);

													var nextWeek = new Date(objWeek);
													nextWeek.setDate(nextWeek.getDate() + 7);

													if ((impObj.day == -1 && objWeek <= impObj.start && nextWeek > impObj.start) ||
														(impObj.day > -1 && objCreated < nextDay)) {
														if (impObj.deleted == undefined || (impObj.deleted != undefined && impObj.deleted > objDate)) {
															if ((impObj.day == -1 && startWeek <= impObj.start && endWeek > impObj.start) ||
																(impObj.day > -1 && startWeek <= objDate && endWeek > objDate)) {
																impPastWeek += impObj.impressions;
															}
															else if ((impObj.day == -1 && startWeekPrev <= impObj.start && endWeekPrev > impObj.start) ||
																(impObj.day > -1 && startWeekPrev <= objDate && endWeekPrev > objDate)) {
																impPrevWeek += impObj.impressions;
															}
															imp.push(impObj);
														}
													}
												}
											}
										}

										if (impPastWeek > 0 && impPrevWeek > 0) {
											impChange = ((impPastWeek - impPrevWeek) / impPrevWeek) * 100;
											impChange = impChange.toFixed(0);
										}
										else if (impPastWeek > 0 && impPrevWeek === 0) {
											impChange = 100;
										}
										else if (impPastWeek === 0 && impPrevWeek > 0) {
											impChange = -100;
										}

										// EVENT IMPRESSIONS
										for (var x in eventsArc) {
											events.push(eventsArc[x]);
										}

										var eimpressions = [];
										// mixpanel can only handle 100 events
										for (var e = 0; e < 100; e++) {
											if (events[e] !== undefined) {
												eimpressions.push("EventImpression-" + events[e].event_id);
											}
											else {
												break;
											}
										}
										panel.events({
											event: eimpressions,
											type: PanelEventTypes.GENERAL,
											unit: PanelEventUnits.DAY,
											from_date: "2020-10-01",
											to_date: toDate
										}).then(function (data) {
											if (data.error) {
												if (checkRateLimitError(data.error, establishmentId))
													reject('rate limited')
											}
											if (get(data, 'data.values')) {
												var vals = get(data, 'data.values')

												for (var d in vals) {
													var objKeys = Object.keys(vals[d]);
													var objVals = Object.values(vals[d]);

													for (var v in objVals) {
														var impObj = {};
														impObj.eventId = d.substring(d.indexOf("-") + 1);
														impObj.impressions = objVals[v];

														for (var e in events) {
															if (events[e].event_id === impObj.eventId) {
																impObj.startTime = dateToStr(events[e].start_time);
																impObj.endTime = dateToStr(events[e].end_time);
																impObj.day = events[e].day;
																var impDate = new Date(objKeys[v]);
																impDate.addHoursDST(prefetchData.tz);
																impObj.impDate = dateToStr(impDate);

																impObj.createdAt = dateToStr(events[e].created_at);
																impObj.duration = getTimeStr(events[e].start_time) + " - " + getTimeStr(events[e].end_time);
																impObj.title = events[e].title;
																if (events[e].deleted_at !== undefined)
																	impObj.arcDate = dateToStr(events[e].deleted_at);

																break;
															}
														}

														if (impDate >= startWeek && impDate <= endWeek)
															eventImpPastWeek += parseInt(impObj.impressions);
														else if (impDate >= startWeekPrev && impDate <= endWeekPrev)
															eventImpPrevWeek += parseInt(impObj.impressions);

														eventImp.push(impObj);
													}
												}
											}

											if (eventImpPastWeek > 0 && eventImpPrevWeek > 0) {
												eventImpChange = ((eventImpPastWeek - eventImpPrevWeek) / eventImpPrevWeek) * 100;
												eventImpChange = eventImpChange.toFixed(0);
											}
											else if (eventImpPastWeek > 0 && eventImpPrevWeek == 0) {
												eventImpChange = 100;
											}
											else if (eventImpPastWeek == 0 && eventImpPrevWeek > 0) {
												eventImpChange = -100;
											}

											// PROFILE VIEWS
											var profViews = [];
											var profViewsCount = 0;
											var profViewsCountPast = 0;

											panel.events({
												event: ["Est loaded " + establishment.establishment_id],
												type: PanelEventTypes.GENERAL,
												unit: PanelEventUnits.DAY,
												from_date: "2019-01-01",
												to_date: toDate
											}).then(function (data) {
												if (data.error) {
													if (checkRateLimitError(data.error, establishmentId))
														reject('rate limited')
												}
												if (get(data, 'data.values')) {
													var vals = get(data, 'data.values')

													for (var d in vals) {
														var objKeys = Object.keys(vals[d]);
														var objVals = Object.values(vals[d]);

														for (var v in objVals) {
															var loadObj = {};
															var objKeyDt = new Date(objKeys[v]);
															objKeyDt.addHoursDST(prefetchData.tz);
															loadObj.date = dateToStr(objKeyDt);
															loadObj.count = objVals[v];
															profViews.push(loadObj);


															var objDate = new Date(loadObj.date);
															if (objDate >= startWeek && objDate <= endWeek) {
																profViewsCount += parseInt(loadObj.count);
															}
															else if (objDate >= startWeekPrev && objDate <= endWeekPrev) {
																profViewsCountPast += parseInt(loadObj.count);
															}
														}
													}

													var profViewsChange = 0;
													if (profViewsCount != profViewsCountPast && profViewsCountPast > 0) {
														profViewsChange = ((profViewsCount - profViewsCountPast) / profViewsCountPast) * 100;
														profViewsChange = profViewsChange.toFixed(0);
													}
													else if (profViewsCount > profViewsCountPast && profViewsCountPast == 0) {
														profViewsChange = 100;
													}
												}

												const result = {
													needsToPay,
													cardOnFile,
													hasSubscription,
													allFavs: favs,
													favorites,
													favoritesChange,
													impPastWeek,
													impChange,
													location,
													profs,
													profsChange,
													profViews,
													profViewsCount,
													profViewsChange,
													impressions: imp,
													eventImp,
													eventImpPastWeek,
													eventImpChange,
													isAppyHour,
													toDate,
													weekStart,
													weekEnd
												}

												if (prefetchData.user.needs_to_pay == 1) {
													result.needsToPay = true;
													if (prefetchData.user.stripe_id) {
														stripe.customers.retrieve(prefetchData.user, stripe_id, function (_, customer) {
															if (customer) {
																if (customer.subscriptions.data.length > 0) {
																	result.hasSubscription = true;
																}
																stripe.customers.retrieveCard(prefetchData.user.stripe_id, customer.default_source, function (_, card) {
																	if (card) {
																		result.cardOnFile = true;
																	}
																	resolve(result)
																});
															} else {
																resolve(result)
															}
														});
													} else {
														resolve(result)
													}
												} else {
													resolve(result)
												}
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	})
};