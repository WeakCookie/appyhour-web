function convertAnalyticsCacheJsonToDbObject(establishmentId, timezone, json) {
    const newJson = assignJsonArrays(json)
    return {
        ANALYTICS_CACHE: {
            timezone,
            establishment_id: establishmentId,
            analytics_cache_json: JSON.stringify(newJson)
        }
    }
}

//INFO : assign arrays to objects to prevent lost data
function assignJsonArrays (json) {
    const { allFavs, eventImp, profViews, impressions } = json
    return {
        ...json,
        allFavs: Object.assign({}, allFavs),
        eventImp: Object.assign({}, eventImp),
        profViews: Object.assign({}, profViews),
        impressions: Object.assign({}, impressions)
    }
}

function parseJsonArrays(json) {
    const { allFavs, eventImp, profViews, impressions } = json
    return {
        ...json,
        allFavs: convertObjectToArray(allFavs),
        eventImp: convertObjectToArray(eventImp),
        profViews: convertObjectToArray(profViews),
        impressions: convertObjectToArray(impressions)
    }
}

function convertObjectToArray(object) {
    const newArray = []
    Object.keys(object).map(key => {
        newArray[key] = object[key]
    })
    return newArray
}

function getPrefetchBody(establishmentId, tz, user) {
    return {
        "establishment": {
            "establishment_id": establishmentId
        },
        "tz": tz,
        "user": user
    }
}

function checkRateLimitError(error, establishmentId) {
    const isRateLimitError = error.includes('Query rate limit exceeded')
    isRateLimitError && console.log('Establishment ', establishmentId, ' rate limited')
    return isRateLimitError
}

module.exports = {
    convertAnalyticsCacheJsonToDbObject,
    getPrefetchBody,
    parseJsonArrays,
    checkRateLimitError
}