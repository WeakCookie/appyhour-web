const fetch = require('node-fetch')
const get = require('lodash/get')

async function fetchData(url, method, body) {
    let payload = undefined
    if(body) {
        payload = { method, body: JSON.stringify(body), headers: { 'Content-Type': 'application/json' } }
    }
    const response = await fetch(url, payload)
    const statusCode = String(get(response, 'status'))
    if(statusCode[0] !== '2' && statusCode[0] !== '3') {
        throw new Error()
    }
    const result = await response.json()
    return result
}

module.exports = {
    fetchData
}