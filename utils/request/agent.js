const https = require('https')
const http = require('http')

const httpAgent = new http.Agent({
    keepAlive: true
})
const httpsAgent = new https.Agent({
    keepAlive: true
})

const options = {
    agent: function (_parsedURL) {
        if (_parsedURL.protocol == 'http:') {
            return httpAgent;
        } else {
            return httpsAgent;
        }
    }
}

module.exports = {
    options
}