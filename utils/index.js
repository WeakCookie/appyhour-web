function secondsDifferent(createdDate) {
    const currentDate = new Date()
    return (currentDate - createdDate) / 1000
}

module.exports = {
    secondsDifferent
}