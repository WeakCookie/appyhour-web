var Establishment = require('../models/establishment.js');
var EstablishmentSpecial = require('../models/establishment_special.js');
var Address = require('../models/address.js');
var Hours = require('../models/hours.js');
var Ref = require('../models/ref.js');
var Special = require('../models/special.js');
var SpecialArchive = require('../models/special_archive.js');
var Common = require('../models/common.js');
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
schedule = require('node-schedule');

exports.show = function(req, res) {
	var establishment = req.session.establishment;

	Special.getAllByEstablishmentId(establishment.establishment_id, function(err, specials) {
		var cardOnFile = false;
		var hasSubscription = false;
		var needsToPay = false;
		var hasLite = false;
		var dateCanceled;
		var cancelDate;

		if(req.session.user.needs_to_pay == 1) {
			needsToPay = true;
			if(req.session.user.stripe_id) {
			    var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
					//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
			    stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
			      	if(customer) {
			      		if(customer.subscriptions.data.length > 0) {
			          		hasSubscription = true;

			          		if(customer.subscriptions.data[0].plan.id == "price_1IYYDNJjoHPs2g2pYdsw6kXB") {
			          			hasLite = true;
			          		}

			          		// customer's subscription will cancel
				            if(customer.subscriptions.data[0].cancel_at_period_end == true) {
				                dateCanceled = epochToDate(customer.subscriptions.data[0].canceled_at);
				                cancelDate = epochToDate(customer.subscriptions.data[0].current_period_end);
				            } 
			          	}
			          	
			        	stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
			          		if(card) {
			            		cardOnFile = true;
			          		}

			          		res.render('home', {
			          			needsToPay: needsToPay,
						    	cardOnFile: cardOnFile,
						    	hasSubscription: hasSubscription,
						    	hasLite: hasLite,
      							dateCanceled: dateCanceled, 
      							cancelDate: cancelDate,
				  				name: establishment.name,
				  				todaysDate: getTodaysDate(req.session.tz),
				  				day: getTodaysDay(req.session.tz),
				  				todaysSpecials: setupTodaysSpecialsTable(specials, req.session.tz),
				  				instantSpecials: setupExclusiveSpecialsTable(specials, 0, req.session.tz),
				  				nextWeekSpecials: setupExclusiveSpecialsTable(specials, 1, req.session.tz),
				  				twoWeeksOutSpecials: setupExclusiveSpecialsTable(specials, 2, req.session.tz),
				  				dailySpecials: setupDailySpecialsTable(specials),
				  				price: null,
				  				details: null,
				  				durationHours: null,
				  				durationMinutes: null,
				  				foodOrDrink: null,
				  				dealType: null,
				  				specialsRemaining: getSpecialsRemainingForWeek(specials, 0, req.session.tz),
				  				specialsRemainingNextWeek: getSpecialsRemainingForWeek(specials, 1, req.session.tz),
				  				specialsRemainingTwoWeeksOut: getSpecialsRemainingForWeek(specials, 2, req.session.tz),
				  				monSpecialsRemaining: getSpecialsRemainingForDay(specials, 0, !needsToPay || (cardOnFile && hasSubscription)),
				  				tueSpecialsRemaining: getSpecialsRemainingForDay(specials, 1, !needsToPay || (cardOnFile && hasSubscription)),
				  				wedSpecialsRemaining: getSpecialsRemainingForDay(specials, 2, !needsToPay || (cardOnFile && hasSubscription)),
				  				thuSpecialsRemaining: getSpecialsRemainingForDay(specials, 3, !needsToPay || (cardOnFile && hasSubscription)),
				  				friSpecialsRemaining: getSpecialsRemainingForDay(specials, 4, !needsToPay || (cardOnFile && hasSubscription)),
				  				satSpecialsRemaining: getSpecialsRemainingForDay(specials, 5, !needsToPay || (cardOnFile && hasSubscription)),
				  				sunSpecialsRemaining: getSpecialsRemainingForDay(specials, 6, !needsToPay || (cardOnFile && hasSubscription)),
				  				currentWeek: getWeek(0, req.session.tz),
				  				nextWeek: getWeek(1, req.session.tz),
				  				twoWeeksOut: getWeek(2, req.session.tz)
							});
			        	});
			      	} else {
			        	res.render('home', {
			        		needsToPay: needsToPay,
					    	cardOnFile: cardOnFile,
					    	hasSubscription: hasSubscription,
					    	hasLite: hasLite,
					    	dateCanceled: dateCanceled, 
      						cancelDate: cancelDate,
			  				name: establishment.name,
			  				todaysDate: getTodaysDate(req.session.tz),
			  				day: getTodaysDay(req.session.tz),
			  				todaysSpecials: setupTodaysSpecialsTable(specials, req.session.tz),
			  				instantSpecials: setupExclusiveSpecialsTable(specials, 0, req.session.tz),
				  			nextWeekSpecials: setupExclusiveSpecialsTable(specials, 1, req.session.tz),
				  			twoWeeksOutSpecials: setupExclusiveSpecialsTable(specials, 2, req.session.tz),
			  				dailySpecials: setupDailySpecialsTable(specials),
			  				price: null,
			  				details: null,
			  				durationHours: null,
			  				durationMinutes: null,
			  				foodOrDrink: null,
			  				dealType: null,
			  				specialsRemaining: getSpecialsRemainingForWeek(specials, 0, req.session.tz),
				  			specialsRemainingNextWeek: getSpecialsRemainingForWeek(specials, 1, req.session.tz),
				  			specialsRemainingTwoWeeksOut: getSpecialsRemainingForWeek(specials, 2, req.session.tz),
				  			monSpecialsRemaining: getSpecialsRemainingForDay(specials, 0, !needsToPay || (cardOnFile && hasSubscription)),
				  			tueSpecialsRemaining: getSpecialsRemainingForDay(specials, 1, !needsToPay || (cardOnFile && hasSubscription)),
				  			wedSpecialsRemaining: getSpecialsRemainingForDay(specials, 2, !needsToPay || (cardOnFile && hasSubscription)),
				  			thuSpecialsRemaining: getSpecialsRemainingForDay(specials, 3, !needsToPay || (cardOnFile && hasSubscription)),
				  			friSpecialsRemaining: getSpecialsRemainingForDay(specials, 4, !needsToPay || (cardOnFile && hasSubscription)),
				  			satSpecialsRemaining: getSpecialsRemainingForDay(specials, 5, !needsToPay || (cardOnFile && hasSubscription)),
				  			sunSpecialsRemaining: getSpecialsRemainingForDay(specials, 6, !needsToPay || (cardOnFile && hasSubscription)),
			  				currentWeek: getWeek(0, req.session.tz),
				  			nextWeek: getWeek(1, req.session.tz),
				  			twoWeeksOut: getWeek(2, req.session.tz)
						});
			      	}
			    });
			} else {
			    res.render('home', {
			    	needsToPay: needsToPay,
			    	cardOnFile: cardOnFile,
			    	hasSubscription: hasSubscription,
			    	hasLite: hasLite,
			    	dateCanceled: dateCanceled, 
      				cancelDate: cancelDate,
	  				name: establishment.name,
	  				todaysDate: getTodaysDate(req.session.tz),
	  				day: getTodaysDay(req.session.tz),
	  				todaysSpecials: setupTodaysSpecialsTable(specials, req.session.tz),
	  				instantSpecials: setupExclusiveSpecialsTable(specials, 0, req.session.tz),
				  	nextWeekSpecials: setupExclusiveSpecialsTable(specials, 1, req.session.tz),
				  	twoWeeksOutSpecials: setupExclusiveSpecialsTable(specials, 2, req.session.tz),
	  				dailySpecials: setupDailySpecialsTable(specials),
	  				price: null,
	  				details: null,
	  				durationHours: null,
	  				durationMinutes: null,
	  				foodOrDrink: null,
	  				dealType: null,
	  				specialsRemaining: getSpecialsRemainingForWeek(specials, 0, req.session.tz),
				  	specialsRemainingNextWeek: getSpecialsRemainingForWeek(specials, 1, req.session.tz),
				  	specialsRemainingTwoWeeksOut: getSpecialsRemainingForWeek(specials, 2, req.session.tz),
				  	monSpecialsRemaining: getSpecialsRemainingForDay(specials, 0, !needsToPay || (cardOnFile && hasSubscription)),
				    tueSpecialsRemaining: getSpecialsRemainingForDay(specials, 1, !needsToPay || (cardOnFile && hasSubscription)),
				  	wedSpecialsRemaining: getSpecialsRemainingForDay(specials, 2, !needsToPay || (cardOnFile && hasSubscription)),
				  	thuSpecialsRemaining: getSpecialsRemainingForDay(specials, 3, !needsToPay || (cardOnFile && hasSubscription)),
				  	friSpecialsRemaining: getSpecialsRemainingForDay(specials, 4, !needsToPay || (cardOnFile && hasSubscription)),
				  	satSpecialsRemaining: getSpecialsRemainingForDay(specials, 5, !needsToPay || (cardOnFile && hasSubscription)),
				  	sunSpecialsRemaining: getSpecialsRemainingForDay(specials, 6, !needsToPay || (cardOnFile && hasSubscription)),
	  				currentWeek: getWeek(0, req.session.tz),
				  	nextWeek: getWeek(1, req.session.tz),
				  	twoWeeksOut: getWeek(2, req.session.tz)
				});
			}
		} else {
			res.render('home', {
				needsToPay: needsToPay,
		    	cardOnFile: cardOnFile,
		    	hasSubscription: hasSubscription,
		    	hasLite: hasLite,
		    	dateCanceled: dateCanceled, 
      			cancelDate: cancelDate,
  				name: establishment.name,
  				todaysDate: getTodaysDate(req.session.tz),
  				day: getTodaysDay(req.session.tz),
  				todaysSpecials: setupTodaysSpecialsTable(specials, req.session.tz),
  				instantSpecials: setupExclusiveSpecialsTable(specials, 0, req.session.tz),
				nextWeekSpecials: setupExclusiveSpecialsTable(specials, 1, req.session.tz),
				twoWeeksOutSpecials: setupExclusiveSpecialsTable(specials, 2, req.session.tz),
  				dailySpecials: setupDailySpecialsTable(specials),
  				price: null,
  				details: null,
  				durationHours: null,
  				durationMinutes: null,
  				foodOrDrink: null,
  				dealType: null,
  				specialsRemaining: getSpecialsRemainingForWeek(specials, 0, req.session.tz),
				specialsRemainingNextWeek: getSpecialsRemainingForWeek(specials, 1, req.session.tz),
				specialsRemainingTwoWeeksOut: getSpecialsRemainingForWeek(specials, 2, req.session.tz),
				monSpecialsRemaining: getSpecialsRemainingForDay(specials, 0, !needsToPay || (cardOnFile && hasSubscription)),
				tueSpecialsRemaining: getSpecialsRemainingForDay(specials, 1, !needsToPay || (cardOnFile && hasSubscription)),
				wedSpecialsRemaining: getSpecialsRemainingForDay(specials, 2, !needsToPay || (cardOnFile && hasSubscription)),
				thuSpecialsRemaining: getSpecialsRemainingForDay(specials, 3, !needsToPay || (cardOnFile && hasSubscription)),
				friSpecialsRemaining: getSpecialsRemainingForDay(specials, 4, !needsToPay || (cardOnFile && hasSubscription)),
				satSpecialsRemaining: getSpecialsRemainingForDay(specials, 5, !needsToPay || (cardOnFile && hasSubscription)),
				sunSpecialsRemaining: getSpecialsRemainingForDay(specials, 6, !needsToPay || (cardOnFile && hasSubscription)),
  				currentWeek: getWeek(0, req.session.tz),
				nextWeek: getWeek(1, req.session.tz),
				twoWeeksOut: getWeek(2, req.session.tz)
			});
		}
	});
};

exports.addDeal = function(req, res) {
	var price = req.body.price;
	var priceTypeId = req.body.priceType;
	var details = req.body.details;
	var dinein = req.body.dinein == "true" ? 1 : 0;
	var carryout = req.body.carryout == "true" ? 1 : 0;
	var hours = req.body.hours;
	var minutes = req.body.minutes;
	var type = getDealTypeRefIdByOGId(req.body.type);
	var startDate = req.body.specialStartDate;
	var startTime = req.body.specialStartTime;
	var isScheduledSpecial = req.body.isScheduledSpecial;
	var establishment_id = req.session.establishment.establishment_id;

	var now = Common.newDate(req.session.tz);
	var created = new Date(now);
	var start;
	var end;

	if(isScheduledSpecial == "false") {
		start = new Date(now);
		end = new Date(now);
	}
	else {
		start = new Date(startDate);
		start.setHours(parseInt(startTime.substr(0, startTime.indexOf(":"))));
		start.setMinutes(parseInt(startTime.substr(startTime.indexOf(":")+1)));
		start.setSeconds(0);
		end = new Date(start);
	}

	end.addHourss(hours);
	end.addMinutess(minutes);

	const special = { 
	    SPECIAL: {
	      price: price,
	      price_type_ref_id: priceTypeId,
	      details: details,
	      deal_type_ref_id: type,
	      day: -1,
	      start_time: start,
	      end_time: end,
	      dinein: dinein,
	      carryout: carryout,
	      created_at: created,
	      updated_at: created
	    }
	};

	Special.insert(special, function(err, result) {
		if(result.affectedRows > 0) {
			const establishment_special = { 
			    ESTABLISHMENT_SPECIAL: {
			      establishment_id: establishment_id,
			      special_id: result.insertId,
			      created_at: created,
			      updated_at: created
			    }
			};
			EstablishmentSpecial.insert(establishment_special, function(err, result2) {
				if(result2.affectedRows > 0) {
					var request = new XMLHttpRequest();
				  	var title = "";

				  	if (priceTypeId == 300)
	                    title = "$" + price;
	                else if (priceTypeId == 301)
	                    title = price + "% Off";
	                else if (priceTypeId == 302)
	                    title = "BOGO";
	                else if (priceTypeId == 307)
	                    title = "Free";

				  	var data = {
				    	"notification": {
				      		"title": title + " " + details + " for the next " + getNotiDuration(hours, minutes),
				      		"body": req.session.establishment.name
				    	},
				    	"to": "/topics/"+req.session.establishment.topic
				  	};

				  	request.onload = function () {
				    	var status = request.status; // HTTP response status, e.g., 200 for "200 OK"
				     	var data = request.responseText; // Returned data, e.g., an HTML document.
				  	}

				  	request.open("POST", "https://fcm.googleapis.com/fcm/send", true);
				  	request.setRequestHeader("Content-Type", "application/json");
				  	request.setRequestHeader("Authorization", "key=AAAAmbcicQc:APA91bGb3m6HgUtAJmIQe8nCzQLfQffQiqicAYo5CicKGa-1YEhRqMnj_nXiRpMZR2fGyvXTHBI4KaACrIG5JgSFAfFoTjFFq0DDOqqhuLgIlnneMVzRbyU33ECIjF0EFk7gDX-uPch0KW7uIzmaWVBWYqP0ChJScA");
				  	
					// send push notification
					if(isScheduledSpecial == "false") {
					  	request.send(JSON.stringify(data));
					  	console.log("PUSH NOTIFICATION SENT");
				  	} 
				  	else {
				  		start.addHoursDST(req.session.tz);
				  		start.setHours(start.getHours()-1);
				  		//start.setHours(start.getHours()+6); // UTC issue (comment out locally) (Daylight Savings Time - Fall)
				  		//start.setHours(start.getHours()+5); // 5 hours for central time (Daylight Savings Time - Spring)
				  		console.log("PUSH NOTIFICATION SCHEDULED for " + start);
						schedule.scheduleJob("special"+result.insertId, start, function(fireDate) {
							request.send(JSON.stringify(data));
						  	console.log("PUSH NOTIFICATION SENT at " + fireDate);
						});
				  	}

					res.redirect('dashboard?page=home');
				} else {
					//res.redirect('dashboard?page=info');
				}
			});
			
		} else {
			//res.redirect('dashboard?page=info');
		}
	});
};

exports.addDailyDeal = function(req, res) {
	var price = req.body.dailyPrice;
	var priceTypeId = req.body.dailyPriceType;
	var details = req.body.dailyDetails;

	// BOGO and Free deals set price to 0
	if (priceTypeId == 302 || priceTypeId == 307)
		price = 0;

	// for BOTH start/end times: if hours < 4, set to 1970-01-02
	var start = req.body.dailyStartTime.replace(':', '');
	var end = req.body.dailyEndTime.replace(':', '');
	start = parseInt(start);
	end = parseInt(end);

	var startTime, endTime;

	if(start >= 400)
		startTime = "1970-01-01 " + req.body.dailyStartTime + ":00";
	else
		startTime = "1970-01-02 " + req.body.dailyStartTime + ":00";

	if(end > 400)
		endTime = "1970-01-01 " + req.body.dailyEndTime + ":00";
	else
		endTime = "1970-01-02 " + req.body.dailyEndTime + ":00";

	var type = req.body.type;
	var now = Common.newDate(req.session.tz);
	var time = new Date(now);
	var establishmentId = req.session.establishment.establishment_id;

	//console.log(req.body);
	var dinein = req.body.dailyDinein == "true" ? 1 : 0;
	var carryout = req.body.dailyTakeout == "true" ? 1 : 0;

	for(var i = 0; i < 7; i++){
		if(req.body.dailyDayMon != null && req.body.dailyDayMon == 'true' && i == 1){
			const monSpecial = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: 1,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: time,
			      updated_at: time
			    }
			};
			insertSpecial(monSpecial, establishmentId, time);
		}
		if(req.body.dailyDayTue != null && req.body.dailyDayTue == 'true' && i == 2){
			const tueSpecial = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: 2,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: time,
			      updated_at: time
			    }
			};
			insertSpecial(tueSpecial, establishmentId, time);
		}
		if(req.body.dailyDayWed != null && req.body.dailyDayWed == 'true' && i == 3){
			const wedSpecial = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: 3,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: time,
			      updated_at: time
			    }
			};
			insertSpecial(wedSpecial, establishmentId, time);
		}
		if(req.body.dailyDayThu != null && req.body.dailyDayThu == 'true' && i == 4){
			const thuSpecial = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: 4,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: time,
			      updated_at: time
			    }
			};
			insertSpecial(thuSpecial, establishmentId, time);
		}
		if(req.body.dailyDayFri != null && req.body.dailyDayFri == 'true' && i == 5){
			const friSpecial = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: 5,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: time,
			      updated_at: time
			    }
			};
			insertSpecial(friSpecial, establishmentId, time);
		}
		if(req.body.dailyDaySat != null && req.body.dailyDaySat == 'true' && i == 6){
			const satSpecial = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: 6,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: time,
			      updated_at: time
			    }
			};
			insertSpecial(satSpecial, establishmentId, time);
		}
		if(req.body.dailyDaySun != null && req.body.dailyDaySun == 'true' && i == 0){
			const sunSpecial = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: 0,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: time,
			      updated_at: time
			    }
			};
			insertSpecial(sunSpecial, establishmentId, time);
		}
	}
	
	res.redirect('dashboard?page=home');
};

function insertSpecial(special, establishmentId, time){
	var response = {sErr: null, sResult: null, esErr: null, esResult: null};
	Special.insert(special, function(err, result) {
		response.sErr = err;
		response.sResult = result;

		console.log(err);
		console.log(result);

		if(result.affectedRows > 0) {
			const establishment_special = { 
			    ESTABLISHMENT_SPECIAL: {
			      establishment_id: establishmentId,
			      special_id: result.insertId,
			      created_at: time,
			      updated_at: time
			    }
			};
			EstablishmentSpecial.insert(establishment_special, function(err, result) {
				response.esErr = err;
				response.esResult = result;
				if(result.affectedRows > 0) {
					return response;
				} else {
					return response;
				}
			});
		} else {
			return response;
		}
	});
}

exports.del = function(req, res){
	var id = req.body.delSpecialId;
	deleteSpecial(id, req.session.tz);
	res.redirect('dashboard?page=home');
};

function deleteSpecial(id, tz) {
	var now = Common.newDate(tz);
	var deleted = new Date(now);

	Special.getAllBySpecialId(id, function(err, s) {
		const special = { 
		    SPECIAL_ARCHIVE: {
		    	special_id: s.special_id,
		      	price: s.price,
		      	price_type_ref_id: s.price_type_ref_id,
		      	details: s.details,
		      	deal_type_ref_id: s.deal_type_ref_id,
		      	day: s.day,
		      	start_time: s.start_time,
		      	end_time: s.end_time,
		      	deleted_at: deleted,
		      	created_at: s.created_at,
		      	updated_at: s.updated_at
		    }
		};

		if(s.day == -1) {
			var job = schedule.scheduledJobs["special"+s.special_id];
			if(job)
				job.cancel();
		}

		SpecialArchive.insert(special, function(err, result1) {
			if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
				Special.delete(id, function(err, result2) {
					if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
						return;
					} else {
						//res.redirect('dashboard?page=info');
					}
				});
			}
			else {
				//res.redirect('dashboard?page=info');
			}
		});
	});

	/*
	EstablishmentSpecial.deleteBySpecialId(id, function(err, result){
		if(result.affectedRows > 0){
			Special.delete(id, function(err, result){
				if(result.affectedRows > 0){
					return;
				} else {
					//res.redirect('dashboard?page=info');
				}
			});
		} else {
			//res.redirect('dashboard?page=info');
		}
	});	
	*/
}






exports.loadSpecial = function(req, res) {
	var sid = req.query.sid;
	// var s;

	Special.getAllBySpecialIdGrouped(sid, function(err, result) {
		if(result) {
			result.start_time.addHoursDST(req.session.tz);
			result.end_time.addHoursDST(req.session.tz);
			res.json(result);
		}
		else {
			res.json(err);
		}
	});
}

exports.editSpecial = function(req, res) {
	//console.log(req.body);

	var now = Common.newDate(req.session.tz);
	var establishmentId = req.session.establishment.establishment_id;
	var sids = req.body.editSpecialIds;
	// var sid = req.body.editSpecialId;
	var priceTypeId = req.body.editPriceTypeId;
	var price = req.body.editPrice;
	var details = req.body.editDetails;
	var type = req.body.editType;
	var dinein = req.body.editDinein == "true" ? 1 : 0;
	var carryout = req.body.editTakeout == "true" ? 1 : 0;

	// BOGO and Free deals set price to 0
	if (priceTypeId == 302 || priceTypeId == 307)
		price = 0;

	// for BOTH start/end times: if hours < 4, set to 1970-01-02
	var start = req.body.editStartTime.replace(':', '');
	var end = req.body.editEndTime.replace(':', '');
	start = parseInt(start);
	end = parseInt(end);

	var startTime, endTime;

	if(start >= 400)
		startTime = "1970-01-01 " + req.body.editStartTime + ":00";
	else
		startTime = "1970-01-02 " + req.body.editStartTime + ":00";

	if(end > 400)
		endTime = "1970-01-01 " + req.body.editEndTime + ":00";
	else
		endTime = "1970-01-02 " + req.body.editEndTime + ":00";

	var daysInsert = [], daysUpdate = [], daysDelete = [];

	if(req.body.editDaySun == "true" && req.body.editDaySunOrig == "true")
		daysUpdate.push(0);
	else if(req.body.editDaySun == "true" && req.body.editDaySunOrig == "false")
		daysInsert.push(0);
	else if(req.body.editDaySun == "false" && req.body.editDaySunOrig == "true")
		daysDelete.push(0);

	if(req.body.editDayMon == "true" && req.body.editDayMonOrig == "true")
		daysUpdate.push(1);
	else if(req.body.editDayMon == "true" && req.body.editDayMonOrig == "false")
		daysInsert.push(1);
	else if(req.body.editDayMon == "false" && req.body.editDayMonOrig == "true")
		daysDelete.push(1);

	if(req.body.editDayTue == "true" && req.body.editDayTueOrig == "true")
		daysUpdate.push(2);
	else if(req.body.editDayTue == "true" && req.body.editDayTueOrig == "false")
		daysInsert.push(2);
	else if(req.body.editDayTue == "false" && req.body.editDayTueOrig == "true")
		daysDelete.push(2);

	if(req.body.editDayWed == "true" && req.body.editDayWedOrig == "true")
		daysUpdate.push(3);
	else if(req.body.editDayWed == "true" && req.body.editDayWedOrig == "false")
		daysInsert.push(3);
	else if(req.body.editDayWed == "false" && req.body.editDayWedOrig == "true")
		daysDelete.push(3);

	if(req.body.editDayThu == "true" && req.body.editDayThuOrig == "true")
		daysUpdate.push(4);
	else if(req.body.editDayThu == "true" && req.body.editDayThuOrig == "false")
		daysInsert.push(4);
	else if(req.body.editDayThu == "false" && req.body.editDayThuOrig == "true")
		daysDelete.push(4);

	if(req.body.editDayFri == "true" && req.body.editDayFriOrig == "true")
		daysUpdate.push(5);
	else if(req.body.editDayFri == "true" && req.body.editDayFriOrig == "false")
		daysInsert.push(5);
	else if(req.body.editDayFri == "false" && req.body.editDayFriOrig == "true")
		daysDelete.push(5);

	if(req.body.editDaySat == "true" && req.body.editDaySatOrig == "true")
		daysUpdate.push(6);
	else if(req.body.editDaySat == "true" && req.body.editDaySatOrig == "false")
		daysInsert.push(6);
	else if(req.body.editDaySat == "false" && req.body.editDaySatOrig == "true")
		daysDelete.push(6);

	//console.log("insert " + daysInsert);
	//console.log("update " + daysUpdate);
	//console.log("delete " + daysDelete);

	for(var i = 0; i < 7; i++) {
		// insert
		if(daysInsert.indexOf(i) > -1) {
			const special = { 
			    SPECIAL: {
			      price: price,
			      price_type_ref_id: priceTypeId,
			      details: details,
			      deal_type_ref_id: type,
			      day: i,
			      start_time: startTime,
			      end_time: endTime,
			      dinein: dinein,
			      carryout: carryout,
			      created_at: now,
			      updated_at: now
			    }
			};
			insertSpecial(special, establishmentId, now);
		}
		// update
		else if(daysUpdate.indexOf(i) > -1) {
			var special = { 
			    price: price,
			    price_type_ref_id: priceTypeId,
			    details: details,
			    deal_type_ref_id: type,
			    day: i,
			    start_time: startTime,
			    end_time: endTime,
			    dinein: dinein,
			    carryout: carryout,
			    updated_at: now
			};
			updateEditSpecial(special, sids.split(","), 0);
		}
		// delete
		else if(daysDelete.indexOf(i) > -1) {
			deleteEditSpecial(sids.split(","), i, req.session.tz, 0);
		}
	}

	res.redirect('dashboard?page=home');
}

function updateEditSpecial(special, sids, start) {
	if(start < sids.length) {
		Special.getAllBySpecialId(sids[start], function(err, s) {
			if(special.day == s.day) {
				//console.log("update special " + s.special_id);
				var specialToUpdate = { 
					SPECIAL: {
						special_id: s.special_id,
				    	price: special.price,
				    	price_type_ref_id: special.price_type_ref_id,
				    	details: special.details,
				    	deal_type_ref_id: special.deal_type_ref_id,
				    	day: special.day,
				    	start_time: special.start_time,
				    	end_time: special.end_time,
				    	dinein: special.dinein,
				    	carryout: special.carryout,
				    	updated_at: special.updated_at
					}
				};
				Special.update(specialToUpdate, function(err, res) {
					return;
				});
			}
			else {
				updateEditSpecial(special, sids, ++start);
			}
		});
	}
	else {
		return;
	}
}

function deleteEditSpecial(sids, day, tz, start) {
	if(start < sids.length) {
		Special.getAllBySpecialId(sids[start], function(err, special) {
			if(special.day == day) {
				deleteSpecial(special.special_id, tz);
			}
			else {
				deleteEditSpecial(sids, day, tz, ++start);
			}
		});
	}
	else {
		return;
	}
}

exports.deleteEditSpecial = function(req, res) {
	var sids = req.body.sids;
	sids = sids.split(",");

	for(var s in sids) {
		deleteSpecial(sids[s], req.session.tz);
	}

	res.send(true);
}



function setupTodaysSpecialsTable(specials, tz){
	var specialsResponse = [];
	var now = Common.newDate(tz);
	var n = getTodaysDayInt(tz);

	// we need today at 4am, so that we can see if exclusive special was created today
	var todayAt4am = Common.newDate(tz);
	if (todayAt4am.getHours() < 4) {
		todayAt4am.setDate(todayAt4am.getDate()-1)
	}
	todayAt4am.setHours(4);
	todayAt4am.setMinutes(0);
	todayAt4am.setSeconds(0);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);
  	
  	var expired = [];
  	var current = [];
  	var upcoming = [];

  	if(specials != undefined && specials != null) {
  		for(var i in specials) {
  			s = specials[i];
  			if(s.day == -1 || s.day == n) {
				s.deal_type_ref_id_TS = getDealTypeImageByRefId(s.deal_type_ref_id);

  				if(s.day == n) {
  					if(s.start_time.getHours() <= 4) {
  						s.start_time.setMonth(tmw.getMonth());
  						s.start_time.setDate(tmw.getDate());
  						s.start_time.setYear(1900+tmw.getYear());
  					}
  					else {
  						s.start_time.setMonth(now.getMonth());
  						s.start_time.setDate(now.getDate());
  						s.start_time.setYear(1900+now.getYear());
  					}

  					if(s.end_time.getHours() <= 4) {
  						s.end_time.setMonth(tmw.getMonth());
  						s.end_time.setDate(tmw.getDate());
  						s.end_time.setYear(1900+tmw.getYear());
  					}
  					else {
  						s.end_time.setMonth(now.getMonth());
  						s.end_time.setDate(now.getDate());
  						s.end_time.setYear(1900+now.getYear());
  					}
  				}
  				
				s.duration = formatStartEndTime(s.start_time) + " - " + formatStartEndTime(s.end_time);

  				// expired 
  				if(s.start_time > todayAt4am && s.end_time < now) {
  					s.expired = true;
  					expired.push(s);
  				}
  				// current 
  				else if(s.start_time <= now && s.end_time >= now) {
  					s.timeremaining = dateDiff(s.end_time, tz);
  					s.current = true;
  					current.push(s);
  				}
  				// upcoming weekly
  				else if(s.day == n && s.start_time > now) {
  					s.upcoming = true;
  					upcoming.push(s);
  				}
  				// upcoming one-time
  				else if(s.start_time > now && s.start_time.getDate() == now.getDate() && s.start_time.getMonth() == now.getMonth() && s.start_time.getYear() == now.getYear()) {
  					s.upcoming = true;
  					upcoming.push(s);
  				}
  			}
		}

		expired = sortByStartTimeThenEndTime(expired);
		current = sortByEndTime(current);
		upcoming = sortByStartTimeThenEndTime(upcoming);

		for (e in expired)
			specialsResponse.push(expired[e]);

		for (c in current)
			specialsResponse.push(current[c]);

		for (u in upcoming)
			specialsResponse.push(upcoming[u]);
  	}

  	return specialsResponse;
}

function sortByStartTimeThenEndTime(specials) {
  	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var startA = new Date(specials[i-1].start_time);
    		var startB = new Date(specials[i].start_time);
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if (startA > startB || (startA.getTime() === startB.getTime() && endA > endB)) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}
		
		if(sorted)
		    break;
  	}

  	return specials;
}

function sortByEndTime(specials) {
	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if (endA > endB) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}
		
		if(sorted)
		    break;
  	}

  	return specials;
}

function setupExclusiveSpecialsTable(specials, week, tz) {
	var addDays = 1;
	if(week == 0)
		addDays = 1;
	else if(week == 1)
		addDays = 8;
	else
		addDays = 15;

	var specialsResponse = [];

	var currFirst = Common.newDate(tz); 
	currFirst.setHours(0,0,0,0);
	var currLast = Common.newDate(tz);
	currLast.setHours(0,0,0,0);
	var first = 0;
	if(currFirst.getDay() != 0)
		first = currFirst.getDate() - currFirst.getDay() + addDays; 
	else
		first = currFirst.getDate() - 7 + addDays;
	var last = first + 7;
	var firstday = new Date(currFirst.setDate(first));
	var lastday = new Date(currLast.setDate(last));

	var expired = [];
  	var current = [];
  	var upcoming = [];

	for(s in specials) {
		if(specials[s].day == -1 && specials[s].event_id == null) {
			if(specials[s].start_time > firstday && specials[s].end_time < lastday) {
				var special = specials[s];

				var st = special.start_time;
				special.startDate = intDayToStringAbbr(st.getDay()) + " " + intMonthToStringAbbr(st.getMonth()) + " " + st.getDate() + getDaySuperscript(st.getDate());
				
				var now = Common.newDate(tz);
				// expired 
  				if(special.end_time < now) {
  					special.expired = true;
  					expired.push(special);
  				}
  				// current 
  				else if(special.start_time < now && special.end_time > now) {
  					special.current = true;
  					current.push(special);
  				}
  				// upcoming
  				else if(special.start_time > now) {
  					special.upcoming = true;
  					upcoming.push(special);
  				}
			}
		}
	}

	expired = sortByStartTimeThenEndTime(expired);
	current = sortByEndTime(current);
	upcoming = sortByStartTimeThenEndTime(upcoming);

	for (e in expired)
		specialsResponse.push(expired[e]);

	for (c in current)
		specialsResponse.push(current[c]);

	for (u in upcoming)
		specialsResponse.push(upcoming[u]);

	return specialsResponse;
}

function setupDailySpecialsTable(specials){
	// Each list in specialsResponse corresponds to a day (item 0 in list is Sunday, 6 is Saturday)
	var specialsResponse = [[], [], [], [], [], [], []];

  	if(specials != undefined && specials != null) {
  		for(var i in specials) {
  			if(specials[i].day != -1) {
  				if(specials[i].day > 0)
  					specials[i].day = specials[i].day - 1;
  				else
  					specials[i].day = 6;

	  			specials[i].start_time_DS = formatStartEndTime(specials[i].start_time);
				specials[i].end_time_DS = formatStartEndTime(specials[i].end_time);
				specials[i].deal_type_ref_id_DS = getDealTypeImageByRefId(specials[i].deal_type_ref_id);
				specialsResponse[specials[i].day].push(specials[i]);
			}
  		}
  	}
  	
  	specialsResponse[0] = sortByStartTimeThenEndTime(specialsResponse[0]);
  	specialsResponse[1] = sortByStartTimeThenEndTime(specialsResponse[1]);
  	specialsResponse[2] = sortByStartTimeThenEndTime(specialsResponse[2]);
  	specialsResponse[3] = sortByStartTimeThenEndTime(specialsResponse[3]);
  	specialsResponse[4] = sortByStartTimeThenEndTime(specialsResponse[4]);
  	specialsResponse[5] = sortByStartTimeThenEndTime(specialsResponse[5]);
  	specialsResponse[6] = sortByStartTimeThenEndTime(specialsResponse[6]);

  	var noSpecials = new Object();

  	for(var x = 0; x <= 6; x++) {
  		if(specialsResponse[x].length == 0) {
  			noSpecials = new Object();
	  		noSpecials.day = x;
	  		noSpecials.empty = true;
	  		specialsResponse[x].push(noSpecials);
	  	}
  	}
  	
	return specialsResponse;
}

function formatStartEndTime(datetime){
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}
		
	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}


//Common Function

function getTodaysDate(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	var month = intMonthToString(t.getMonth());
	var day = t.getDate();
	var year = 1900+t.getYear();
	return month + " " + day + getDaySuperscript(day) + ", " + year; //test
}

function getTodaysDay(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	var dayOfWeek = intDayToString(t.getDay());
	
	return dayOfWeek;
}

function getTodaysDayInt(tz) {
	var t = Common.newDate(tz);
	
	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	return t.getDay();
}

function intDayToString(day) {
	var weekday = new Array(7);
	weekday[0] = "Sunday";
	weekday[1] = "Monday";
	weekday[2] = "Tuesday";
	weekday[3] = "Wednesday";
	weekday[4] = "Thursday";
	weekday[5] = "Friday";
	weekday[6] = "Saturday";
	return weekday[day];
}

function intDayToStringAbbr(day) {
	var weekday = new Array(7);
	weekday[0] = "Sun";
	weekday[1] = "Mon";
	weekday[2] = "Tue";
	weekday[3] = "Wed";
	weekday[4] = "Thu";
	weekday[5] = "Fri";
	weekday[6] = "Sat";
	return weekday[day];
}

function intMonthToString(month) {
	var m = new Array(7);
	m[0] = "January";
	m[1] = "February";
	m[2] = "March";
	m[3] = "April";
	m[4] = "May";
	m[5] = "June";
	m[6] = "July";
	m[7] = "August";
	m[8] = "September";
	m[9] = "October";
	m[10] = "November";
	m[11] = "December";
	return m[month];
}

function intMonthToStringAbbr(month) {
	var m = new Array(7);
	m[0] = "Jan";
	m[1] = "Feb";
	m[2] = "Mar";
	m[3] = "Apr";
	m[4] = "May";
	m[5] = "Jun";
	m[6] = "Jul";
	m[7] = "Aug";
	m[8] = "Sep";
	m[9] = "Oct";
	m[10] = "Nov";
	m[11] = "Dec";
	return m[month];
}

function intMonthToString(month) {
	var m = new Array(7);
	m[0] = "January";
	m[1] = "February";
	m[2] = "March";
	m[3] = "April";
	m[4] = "May";
	m[5] = "June";
	m[6] = "July";
	m[7] = "August";
	m[8] = "September";
	m[9] = "October";
	m[10] = "November";
	m[11] = "December";
	return m[month];
}

function getDealTypeImageByRefId(refId) {
	switch(refId) {
		case 200: return 'beer-icon.png';
		case 202: return 'shot-icon.png';
    	case 204: return 'wine-icon.png';
    	case 206: return 'mixed-drink-icon.png';
    	case 208: return 'margarita-icon.png';
    	case 210: return 'martini-icon.png';
    	case 212: return 'tumbler-icon.png';
    	case 214: return 'beerbottle.png';
    	case 216: return 'beercan.png';
		case 251: return 'burger-icon.png';
		case 253: return 'appetizer-icon.png';
		case 255: return 'pizza-icon.png';
    	case 257: return 'taco-icon.png';
    	case 259: return 'sushi.png';
    	case 261: return 'bowl.png';
    	case 263: return 'chickenwing.png';
		default: return '';
	}
}

function getDealTypeRefIdByOGId(id){
	if(id == 0) {
		return 200; //beer
	} else if (id == 1) {
		return 202; //shot
	} else if (id == 2) {
		return 206; //mixed drink
	} else if (id == 3) {
		return 208; //margarita
	} else if (id == 4) {
		return 210; //martini
	} else if (id == 5) {
		return 212; //tumbler
	} else if (id == 6) {
		return 204; //wine
	} else if (id == 7) {
		return 251; //burger
	} else if (id == 8) {
		return 253; //appetizer
	} else if (id == 9) {
		return 255; //pizza
	} else if (id == 10) {
		return 257; //taco
	} else if (id == 11) {
		return 259; //sushi
	} else if (id == 12) {
		return 261; //bowl
	} else if (id == 13) {
		return 263; //wing
	} else if (id == 14) {
		return 214; //bottle
	} else if (id == 15) {
		return 216; //can
	}

	return 200;
}

Date.prototype.addHourss = function(h) {    
	this.setTime(this.getTime() + (h*60*60*1000)); 
   	return this;   
}

Date.prototype.addMinutess = function(m) {
	this.setTime(this.getTime() + (m*60*1000)); 
   	return this; 
}

function dateDiff(a, tz) {
	var today = Common.newDate(tz);
  	var dealDate = new Date(a);

  	if (dealDate < today) {
    	dealDate.setDate(dealDate.getDate()+1);
    	a = dealDate.getTime();
  	}

	var diffMs = (a - today); // milliseconds between now & a
	var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
	var diffMins = Math.ceil(((diffMs % 86400000) % 3600000) / 60000); // minutes

	if (diffMins == 60) {
		diffHrs++;
		diffMins = 0;
	}
	
	if (diffHrs == 0) {
		return(diffMins + "m");
	} else {
		return(diffHrs + "h " + diffMins + "m");
	}
}

function getNotiDuration(hr, min) {
  if(hr == 0)
    return min + " minutes!";
  else if(hr == 1 && min == 0)
    return "hour!";
  else if(hr == 1 && min > 0)
    return hr + " hour " + min + " minutes!";
  else if(hr > 0 && min == 0)
    return hr + " hours!";
  else
    return hr + " hours " + min + " minutes!";
}

function epochToDate(epoch) {
  var d = new Date(0);
  d.setUTCSeconds(epoch);

  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = d.getDate();
  var monthIndex = d.getMonth();
  var year = d.getFullYear();

  return monthNames[monthIndex] + ' ' + day + getDaySuperscript(day) + ', ' + year;
}

function getDaySuperscript(day) {
  if(day == 1 || day == 21 || day == 31)
    return "st";
  else if(day == 2 || day == 22)
    return "nd";
  else if(day == 3 || day == 23)
    return "rd";
  else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
    return "th";
  else 
    return "";
}

function getWeek(week, tz) {
	var addDays = 1;
	if(week == 0)
		addDays = 1;
	else if(week == 1)
		addDays = 8;
	else
		addDays = 15;

	var currFirst = Common.newDate(tz); 
	var currLast = Common.newDate(tz);
	var first = 0;
	if(currFirst.getDay() != 0)
		first = currFirst.getDate() - currFirst.getDay() + addDays; 
	else
		first = currFirst.getDate() - 7 + addDays;
	var last = first + 6;
	var firstday = new Date(currFirst.setDate(first));
	var lastday = new Date(currLast.setDate(last));

	firstday = intMonthToStringAbbr(firstday.getMonth()) + " " + firstday.getDate() + getDaySuperscript(firstday.getDate());
	lastday = intMonthToStringAbbr(lastday.getMonth()) + " " + lastday.getDate() + getDaySuperscript(lastday.getDate());

	return firstday + " - " + lastday;
}

function getSpecialsRemainingForWeek(specials, week, tz) {
	var addDays = 1;
	if(week == 0)
		addDays = 1;
	else if(week == 1)
		addDays = 8;
	else
		addDays = 15;

	var specialsThisWeek = 0;
	var currFirst = Common.newDate(tz); 
	currFirst.setHours(0,0,0,0);
	var currLast = Common.newDate(tz);
	currLast.setHours(0,0,0,0);
	var first = 0;
	if(currFirst.getDay() != 0)
		first = currFirst.getDate() - currFirst.getDay() + addDays; 
	else
		first = currFirst.getDate() - 7 + addDays;
	var last = first + 7;
	var firstday = new Date(currFirst.setDate(first));
	var lastday = new Date(currLast.setDate(last));

	for(s in specials) {
		if(specials[s].day == -1 && specials[s].title == undefined) {
			if(specials[s].start_time > firstday && specials[s].end_time < lastday) {
				specialsThisWeek = specialsThisWeek + 1;
			}
		}
	}

	var remaining = 1 - specialsThisWeek;
	if(remaining < 0)
		remaining = 0;

	return remaining;
}

function getSpecialsRemainingForDay(specials, day, paying) {
	var remaining = paying == true ? 5 : 1;

	for(s in specials) {
		if(specials[s].day == day && specials[s].event_id == null) {
			remaining -= 1;
		}
	}

	if(remaining < 0)
		remaining = 0;

	return remaining;
}


