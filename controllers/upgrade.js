exports.show = function(req, res) {
  var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
  //var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
  var cardOnFile = false;
  var hasSubscription = false;
  var hasLite = false;
  var needsToPay = false;
  var cancelDate;
  var dateCanceled;
  var plans;
  var showEvents = true;

  if(req.session.proxy)
    showEvents = true;

  if(req.session.establishment) {
    var est = req.session.establishment;

    if(est.topic == "denverco") {
      req.session.tz = "MST";
    }
    else if(est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
      req.session.tz = "CST";
    }
    else if(est.topic == "louisvilleky" || est.topic == "tampabayfl") {
      req.session.tz = "EST";
    }

    if(req.session.user.needs_to_pay == 1) {
      needsToPay = true;

      stripe.plans.list(function(err, plans) {
        plans = plans.data;

        if(req.session.user.stripe_id) {
          stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
            if(customer) {
              if(customer.subscriptions.data.length > 0) {
                hasSubscription = true;
                if(customer.subscriptions.data[0].plan.id == "price_1IYYDNJjoHPs2g2pYdsw6kXB") {
                  hasLite = true;
                }
              }
              stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
                if(card) {
                  cardOnFile = true;
                }

                res.render('upgrade', {
                  needsToPay: needsToPay,
                  cardOnFile: cardOnFile,
                  hasSubscription: hasSubscription,
                  hasLite: hasLite,
                  plans: plans,
                  showEvents: showEvents,
                  est: est
                });
              });
            } else {
              res.render('upgrade', {
                needsToPay: needsToPay,
                cardOnFile: cardOnFile,
                hasSubscription: hasSubscription,
                hasLite: hasLite,
                plans: plans,
                showEvents: showEvents,
                est: est
              });
            }
          });
        } else {
          res.render('upgrade', {
            needsToPay, needsToPay,
            cardOnFile: cardOnFile,
            hasSubscription: hasSubscription,
            hasLite: hasLite,
            plans: plans,
            showEvents: showEvents,
            est: est
          });
        }
      });
    } else {
      res.render('upgrade', {
        needsToPay, needsToPay,
        cardOnFile: cardOnFile,
        hasSubscription: hasSubscription,
        hasLite: hasLite,
        plans: plans,
        showEvents: showEvents,
        est: est
      });
    }
  }
  else {
    Establishment.getAllByOwnerId(req.session.user.user_id, function(err, ests) {
      if(ests) {
        Image.getProfilePicForEstablishmentId(ests[0].establishment_id, function(err, image) {
          var est = ests[0];
          est.profile_pic_id = image.image_id;
          est.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/"+image.image_location;

          if(est.topic == "denverco") {
            req.session.tz = "MST";
          }
          else if(est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
            req.session.tz = "CST";
          }
          else if(est.topic == "louisvilleky" || est.topic == "tampabayfl") {
            req.session.tz = "EST";
          }

          req.session.establishment = est;

          if(req.session.user.needs_to_pay == 1) {
            needsToPay = true;

            stripe.plans.list(function(err, plans) {
              plans = plans.data;

              if(req.session.user.stripe_id) {
                stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
                  if(customer) {
                    if(customer.subscriptions.data.length > 0) {
                      hasSubscription = true;
                      if(customer.subscriptions.data[0].plan.id == "price_1IYYDNJjoHPs2g2pYdsw6kXB") {
                        hasLite = true;
                      }
                    }
                    stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
                      if(card) {
                        cardOnFile = true;
                      }

                      res.render('upgrade', {
                        needsToPay: needsToPay,
                        cardOnFile: cardOnFile,
                        hasSubscription: hasSubscription,
                        hasLite: hasLite,
                        plans: plans,
                        showEvents: showEvents,
                        est: est
                      });
                    });
                  } else {
                    res.render('upgrade', {
                      needsToPay: needsToPay,
                      cardOnFile: cardOnFile,
                      hasSubscription: hasSubscription,
                      hasLite: hasLite,
                      plans: plans,
                      showEvents: showEvents,
                      est: est
                    });
                  }
                });
              } else {
                res.render('upgrade', {
                  needsToPay, needsToPay,
                  cardOnFile: cardOnFile,
                  hasSubscription: hasSubscription,
                  hasLite: hasLite,
                  plans: plans,
                  showEvents: showEvents,
                  est: est
                });
              }
            });
          } else {
            res.render('upgrade', {
              needsToPay, needsToPay,
              cardOnFile: cardOnFile,
              hasSubscription: hasSubscription,
              hasLite: hasLite,
              plans: plans,
              showEvents: showEvents,
              est: est
            });
          }
        });
      }
    });
  }
};