var Promo = require('../models/promo.js');
var PromoPriority = require('../models/promo_priority.js');
var Supplier = require('../models/supplier.js');
var SupplierPromo = require('../models/supplier_promo.js');
var SupplierManager = require('../models/supplier_manager.js');
var Location = require('../models/location.js');
var Establishment = require('../models/establishment.js');
var Special = require('../models/special.js');
var Event = require('../models/event.js');
var Ref = require('../models/ref.js');
var Common = require('../models/common.js');
var fs = require('fs');
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./awsconfig.json');
var MixpanelExport = require('mixpanel-data-export');
var panel = new MixpanelExport({
	api_key: "4f98c92e0ecf95d97d76d6eca2cb0298",
	api_secret: "3e9796ed74b2cdda13edc3bbe904f073"
});
var geoTz = require('geo-tz');
var cookie = require('cookie');

var supPromos;
var supPromoImps;
var supPromoPresses;

exports.show = function(req, res) {
	var supplier = req.session.supplier;

	if(supplier) {
		init(req, res);
	}
	else {
		res.redirect('/estlogin');
	}  	
}

exports.supplierSaveUserLocation = function(req, res) {
	var userLocationRequested = req.body.userLocationRequested;

  	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	if(tz == '' || userLocationRequested == "true") {
		var lat = req.body.lat;
		var lon = req.body.lon;

		if(lat != "" && lon != "") {
			req.session.userLocationAllowed = true;
			tz = getTimezone(lat, lon);
		}
		else {
			req.session.userLocationAllowed = false;
			tz = 'EST';
		}

		//res.setHeader('Set-Cookie', cookie.serialize('tz', tz, {
	    //  httpOnly: true,
	    //  maxAge: 60 * 60 * 24 * 7 // 1 week
	    //}));
	}

	res.send(tz);
}

function getTimezone(lat, lon) {
	var timezone = geoTz(lat, lon);
	var dst = (new Date()).isDstObserved();
	var tz = 'EST';

	if(dst) {
		if(timezone[0] == 'America/Los_Angeles')
			tz = 'PDT';
		else if(timezone[0] == 'America/Denver')
			tz = 'MDT';
		else if(timezone[0] == 'America/Chicago')
			tz = 'CDT';
		else if(timezone[0] == 'America/New_York')
			tz = 'EDT';
	}
	else {
		if(timezone[0] == 'America/Los_Angeles')
			tz = 'PST';
		else if(timezone[0] == 'America/Denver')
			tz = 'MST';
		else if(timezone[0] == 'America/Chicago')
			tz = 'CST';
		else if(timezone[0] == 'America/New_York')
			tz = 'EST';
	}

	return tz;
}

function init(req, res) {
	var supplier = req.session.supplier;
	var impressions = [];

	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	Promo.getAllBySupplierId(supplier.supplier_id, function(err, result) {
		Location.getAll(function(errLoc, locations) {
			for(var r in result) {
				impressions.push("PromoImpression-"+result[r].promo_id);

				if(result[r].active == 0)
					result[r].activeYN = "No";
				else
					result[r].activeYN = "Yes";

				result[r].featuresStr = "-";
				if(result[r].features != null && result[r].features != "") {
					var features = result[r].features.split(";");
					var fStr = "";
					for(var f = 0; f < features.length; f++) {
						features[f] = features[f].replace("dinein", "Dine-in");
						features[f] = features[f].replace("dog_friendly", "Dog-friendly");
						features[f] = features[f].charAt(0).toUpperCase() + features[f].slice(1);
						
						if(f < features.length - 1)
							fStr += features[f] + ", ";
						else
							fStr += features[f];
					}
					result[r].featuresStr = fStr;
				}

				result[r].keywordStr = "-";
				if(result[r].keywords != null && result[r].keywords != "") {
					var keywords = result[r].keywords.split(";");
					var kStr = "";
					for(var k = 0; k < keywords.length; k++) {
						if(k < keywords.length - 1)
							kStr += keywords[k] + ", ";
						else
							kStr += keywords[k];
					}
					result[r].keywordStr = kStr;
				}

				result[r].dayStr = dayStrToString(result[r].day);

				for(var l in locations) {
					if(locations[l].latitude == (result[r].latitude != '' ? result[r].latitude : null)) {
						result[r].location = locations[l].location_id;
						result[r].locationStr = locations[l].location;
						break;
					}
				}

				result[r].created = dtToStr(result[r].created_at);
				result[r].createdAt = result[r].created_at.toString();

				if(result[r].approved_at != null)
					result[r].approved = dtToStr(result[r].approved_at);
				else
					result[r].approved = null;

				if(result[r].archive_dt != null)
					result[r].archived = dtToStr(result[r].archive_dt);
				else
					result[r].archived = null;

				result[r].thisWeek = 0;
				result[r].lastWeek = 0;
				result[r].prevWeek = 0;
				result[r].allWeeks = 0;
				result[r].intChangeThisWeek = 0;
				result[r].intChangeLastWeek = 0;

				result[r].thisWeekImp = 0;
				result[r].lastWeekImp = 0;
				result[r].prevWeekImp = 0;
				result[r].allWeeksImp = 0;
				result[r].intChangeThisWeekImp = 0;
				result[r].intChangeLastWeekImp = 0;

				if(result[r].approved_at == null) {
					result[r].thisWeek = "-";
					result[r].lastWeek = "-";
					result[r].prevWeek = "-";
					result[r].allWeeks = "-";
					result[r].intChangeThisWeek = "-";
					result[r].intChangeLastWeek = "-";

					result[r].thisWeekImp = "-";
					result[r].lastWeekImp = "-";
					result[r].prevWeekImp = "-";
					result[r].allWeeksImp = "-";
					result[r].intChangeThisWeekImp = "-";
					result[r].intChangeLastWeekImp = "-";
				}
			}

			// sort promos
			result.sort((a, b) => (a.approved_at < b.approved_at) ? 1 : -1);

			var cookies = cookie.parse(req.headers.cookie || '');
  			var tz = cookies.tz;

			var toDate = getYYYYMMDD(Common.newDate(tz));

			var curr = Common.newDate(tz);
			curr.setHours(0,0,0,0);

			// last 7 days
			curr.setDate(curr.getDate()-1);
			var bb = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var aa = new Date(curr.getTime());

			// 2 weeks ago
			curr.setDate(curr.getDate()-1);
			var dd = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var cc = new Date(curr.getTime());

			// 3 weeks ago
			curr.setDate(curr.getDate()-1);
			var ff = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var ee = new Date(curr.getTime());

			var thisWeekStart = intMonthToStringAbbr(aa.getMonth()) + " " + aa.getDate();
			var thisWeekStartSuper = getSuperscript(aa.getDate());
			var thisWeekEnd = intMonthToStringAbbr(bb.getMonth()) + " " + bb.getDate();
			var thisWeekEndSuper = getSuperscript(bb.getDate());

			var lastWeekStart = intMonthToStringAbbr(cc.getMonth()) + " " + cc.getDate();
			var lastWeekStartSuper = getSuperscript(cc.getDate());
			var lastWeekEnd = intMonthToStringAbbr(dd.getMonth()) + " " + dd.getDate();
			var lastWeekEndSuper = getSuperscript(dd.getDate());

			panel.eventProperties({
			  	event: "Promo pressed",
			  	name: "promo_id",
			  	type: "general",
			  	unit: "day",
			  	from_date: "2020-08-01",
			  	to_date: toDate
			}).then(function(data) {

				var vals = data.data.values;
				supPromoPresses = vals;

				for(var d in vals) {
					//console.log(d);
					var objKeys = Object.keys(vals[d]);
					var objVals = Object.values(vals[d]);
					//console.log(objKeys);
					//console.log(objVals);

					//var impVals = Object.values(imps[d]);
					//console.log(impVals);

					for(var k in objKeys) {
						var dayDt = new Date(objKeys[k] + " 00:00:00");
						for(var promo in result) {
							if(result[promo].approved != null) {
								if(result[promo].thisWeek == undefined || result[promo].thisWeek == "-") 
									result[promo].thisWeek = 0;
								if(result[promo].lastWeek == undefined || result[promo].lastWeek == "-") 
									result[promo].lastWeek = 0;
								if(result[promo].prevWeek == undefined || result[promo].prevWeek == "-") 
									result[promo].prevWeek = 0;
								if(result[promo].allWeeks == undefined || result[promo].allWeeks == "-") 
									result[promo].allWeeks = 0;

								if(result[promo].promo_id == d) {
									result[promo].allWeeks += parseInt(objVals[k]);
									
									if(dayDt.getTime() >= aa.getTime() && dayDt.getTime() <= bb.getTime()) {
										result[promo].thisWeek += parseInt(objVals[k]);
									}
								 	else if(dayDt.getTime() >= cc.getTime() && dayDt.getTime() <= dd.getTime()) {
										result[promo].lastWeek += parseInt(objVals[k]);
									}
									else if(dayDt.getTime() >= ee.getTime() && dayDt.getTime() <= ff.getTime()) {
										result[promo].prevWeek += parseInt(objVals[k]);
									}
								}
							}
						}
					}
				}

				for(var promo in result) {
					if(result[promo].approved != null) {
						if(result[promo].lastWeek > 0 && result[promo].prevWeek > 0) {
							result[promo].intChangeLastWeek = ((result[promo].lastWeek - result[promo].prevWeek) / result[promo].prevWeek) * 100;
							result[promo].intChangeLastWeek = result[promo].intChangeLastWeek.toFixed(0);
						}
						else if(result[promo].lastWeek > 0 && result[promo].prevWeek == 0) {
							result[promo].intChangeLastWeek = 100;
						}
						else if(result[promo].lastWeek == 0 && result[promo].prevWeek > 0) {
							result[promo].intChangeLastWeek = -100;
						}

						if(result[promo].thisWeek > 0 && result[promo].lastWeek > 0) {
							result[promo].intChangeThisWeek = ((result[promo].thisWeek - result[promo].lastWeek) / result[promo].lastWeek) * 100;
							result[promo].intChangeThisWeek = result[promo].intChangeThisWeek.toFixed(0);
						}
						else if(result[promo].thisWeek > 0 && result[promo].lastWeek == 0) {
							result[promo].intChangeThisWeek = 100;
						}
						else if(result[promo].thisWeek == 0 && result[promo].lastWeek > 0) {
							result[promo].intChangeThisWeek = -100;
						}
					}
				}

				panel.events({
				  	event: impressions,
				  	type: "general",
				  	unit: "day",
				  	from_date: "2020-08-01",
				  	to_date: toDate
				}).then(function(impData) {
					if(impData !== undefined && impData.data !== undefined && impData.data.values !== undefined) {
						var impVals = impData.data.values;
						supPromoImps = impVals;
						
						for(var i in impVals) {	
							var objImpKeys = Object.keys(impVals[i]);
							var objImpVals = Object.values(impVals[i]);

							for(var iv in objImpVals) {
								var promo_id = i.substring(i.indexOf("-")+1);
								var dayDt = new Date(objImpKeys[iv] + " 00:00:00");

								for(var promo in result) {
									if(result[promo].approved != null) {
										if(result[promo].thisWeekImp == undefined || result[promo].thisWeekImp == "-") 
											result[promo].thisWeekImp = 0;
										if(result[promo].lastWeekImp == undefined || result[promo].lastWeekImp == "-") 
											result[promo].lastWeekImp = 0;
										if(result[promo].prevWeekImp == undefined || result[promo].prevWeekImp == "-") 
											result[promo].prevWeekImp = 0;
										if(result[promo].allWeeksImp == undefined || result[promo].allWeeksImp == "-") 
											result[promo].allWeeksImp = 0;

										if(result[promo].promo_id == promo_id) {
											result[promo].allWeeksImp += parseInt(objImpVals[iv]);
											
											if(dayDt.getTime() >= aa.getTime() && dayDt.getTime() <= bb.getTime()) {
												result[promo].thisWeekImp += parseInt(objImpVals[iv]);
											}
										 	else if(dayDt.getTime() >= cc.getTime() && dayDt.getTime() <= dd.getTime()) {
												result[promo].lastWeekImp += parseInt(objImpVals[iv]);
											}
											else if(dayDt.getTime() >= ee.getTime() && dayDt.getTime() <= ff.getTime()) {
												result[promo].prevWeekImp += parseInt(objImpVals[iv]);
											}
										}
									}
								}
							}
						}

						for(var promo in result) {
							if(result[promo].approved != null) {
								if(result[promo].lastWeekImp > 0 && result[promo].prevWeekImp > 0) {
									result[promo].intChangeLastWeekImp = ((result[promo].lastWeekImp - result[promo].prevWeekImp) / result[promo].prevWeekImp) * 100;
									result[promo].intChangeLastWeekImp = result[promo].intChangeLastWeekImp.toFixed(0);
								}
								else if(result[promo].lastWeekImp > 0 && result[promo].prevWeekImp == 0) {
									result[promo].intChangeLastWeekImp = 100;
								}
								else if(result[promo].lastWeekImp == 0 && result[promo].prevWeekImp > 0) {
									result[promo].intChangeLastWeekImp = -100;
								}

								if(result[promo].thisWeekImp > 0 && result[promo].lastWeekImp > 0) {
									result[promo].intChangeThisWeekImp = ((result[promo].thisWeekImp - result[promo].lastWeekImp) / result[promo].lastWeekImp) * 100;
									result[promo].intChangeThisWeekImp = result[promo].intChangeThisWeekImp.toFixed(0);
								}
								else if(result[promo].thisWeekImp > 0 && result[promo].lastWeekImp == 0) {
									result[promo].intChangeThisWeekImp = 100;
								}
								else if(result[promo].thisWeekImp == 0 && result[promo].lastWeekImp > 0) {
									result[promo].intChangeThisWeekImp = -100;
								}
							}
						}

						supPromos = result;

						res.render('supplierdashboard', {
							currPage: 'dashboard',
							supplier: req.session.supplier,
							promos: result,
							promoLocations: locations,
							thisWeekStart: thisWeekStart,
							thisWeekStartSuper: thisWeekStartSuper,
							thisWeekEnd: thisWeekEnd,
							thisWeekEndSuper: thisWeekEndSuper,
							lastWeekStart: lastWeekStart,
							lastWeekStartSuper: lastWeekStartSuper,
							lastWeekEnd: lastWeekEnd,
							lastWeekEndSuper: lastWeekEndSuper,
							tz: tz,
							userLocationAllowed: req.session.userLocationAllowed
						});
					}
					else {
						supPromos = result;

						res.render('supplierdashboard', {
							currPage: 'dashboard',
							supplier: req.session.supplier,
							promos: result,
							promoLocations: locations,
							thisWeekStart: thisWeekStart,
							thisWeekStartSuper: thisWeekStartSuper,
							thisWeekEnd: thisWeekEnd,
							thisWeekEndSuper: thisWeekEndSuper,
							lastWeekStart: lastWeekStart,
							lastWeekStartSuper: lastWeekStartSuper,
							lastWeekEnd: lastWeekEnd,
							lastWeekEndSuper: lastWeekEndSuper,
							tz: tz,
							userLocationAllowed: req.session.userLocationAllowed
						});
					}
				});
			});
		});
	});
}

function supplierGetData(supplierId, req, _callback) {
	var impressions = [];

	Promo.getAllBySupplierId(supplierId, function(err, result) {
		Location.getAll(function(errLoc, locations) {
			for(var r in result) {
				impressions.push("PromoImpression-"+result[r].promo_id);

				if(result[r].active == 0)
					result[r].activeYN = "No";
				else
					result[r].activeYN = "Yes";

				result[r].featuresStr = "-";
				if(result[r].features != null && result[r].features != "") {
					var features = result[r].features.split(";");
					var fStr = "";
					for(var f = 0; f < features.length; f++) {
						features[f] = features[f].replace("dinein", "Dine-in");
						features[f] = features[f].replace("dog_friendly", "Dog-friendly");
						features[f] = features[f].charAt(0).toUpperCase() + features[f].slice(1);

						if(f < features.length - 1)
							fStr += features[f] + ", ";
						else
							fStr += features[f];
					}
					result[r].featuresStr = fStr;
				}

				result[r].keywordStr = "-";
				if(result[r].keywords != null && result[r].keywords != "") {
					var keywords = result[r].keywords.split(";");
					var kStr = "";
					for(var k = 0; k < keywords.length; k++) {
						if(k < keywords.length - 1)
							kStr += keywords[k] + ", ";
						else
							kStr += keywords[k];
					}
					result[r].keywordStr = kStr;
				}

				result[r].dayStr = dayStrToString(result[r].day);

				for(var l in locations) {
					if(locations[l].latitude == (result[r].latitude != '' ? result[r].latitude : null)) {
						result[r].location = locations[l].location_id;
						result[r].locationStr = locations[l].location;
						break;
					}
				}

				result[r].created = dtToStr(result[r].created_at);
				result[r].createdAt = result[r].created_at.toString();

				if(result[r].approved_at != null)
					result[r].approved = dtToStr(result[r].approved_at);
				else
					result[r].approved = null;

				if(result[r].archive_dt != null)
					result[r].archived = dtToStr(result[r].archive_dt);
				else
					result[r].archived = null;

				result[r].thisWeek = 0;
				result[r].lastWeek = 0;
				result[r].prevWeek = 0;
				result[r].allWeeks = 0;
				result[r].intChangeThisWeek = 0;
				result[r].intChangeLastWeek = 0;

				result[r].thisWeekImp = 0;
				result[r].lastWeekImp = 0;
				result[r].prevWeekImp = 0;
				result[r].allWeeksImp = 0;
				result[r].intChangeThisWeekImp = 0;
				result[r].intChangeLastWeekImp = 0;

				if(result[r].approved_at == null) {
					result[r].thisWeek = "-";
					result[r].lastWeek = "-";
					result[r].prevWeek = "-";
					result[r].allWeeks = "-";
					result[r].intChangeThisWeek = "-";
					result[r].intChangeLastWeek = "-";

					result[r].thisWeekImp = "-";
					result[r].lastWeekImp = "-";
					result[r].prevWeekImp = "-";
					result[r].allWeeksImp = "-";
					result[r].intChangeThisWeekImp = "-";
					result[r].intChangeLastWeekImp = "-";
				}
			}

			// sort promos
			result.sort((a, b) => (a.approved_at < b.approved_at) ? 1 : -1);

			var cookies = cookie.parse(req.headers.cookie || '');
  			var tz = cookies.tz;

			var toDate = getYYYYMMDD(Common.newDate(tz));

			var curr = Common.newDate(tz);
			curr.setHours(0,0,0,0);

			// last 7 days
			curr.setDate(curr.getDate()-1);
			var bb = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var aa = new Date(curr.getTime());

			// 2 weeks ago
			curr.setDate(curr.getDate()-1);
			var dd = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var cc = new Date(curr.getTime());

			// 3 weeks ago
			curr.setDate(curr.getDate()-1);
			var ff = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var ee = new Date(curr.getTime());

			var thisWeekStart = intMonthToStringAbbr(aa.getMonth()) + " " + aa.getDate();
			var thisWeekStartSuper = getSuperscript(aa.getDate());
			var thisWeekEnd = intMonthToStringAbbr(bb.getMonth()) + " " + bb.getDate();
			var thisWeekEndSuper = getSuperscript(bb.getDate());

			var lastWeekStart = intMonthToStringAbbr(cc.getMonth()) + " " + cc.getDate();
			var lastWeekStartSuper = getSuperscript(cc.getDate());
			var lastWeekEnd = intMonthToStringAbbr(dd.getMonth()) + " " + dd.getDate();
			var lastWeekEndSuper = getSuperscript(dd.getDate());

			panel.eventProperties({
			  	event: "Promo pressed",
			  	name: "promo_id",
			  	type: "general",
			  	unit: "day",
			  	from_date: "2020-08-01",
			  	to_date: toDate
			}).then(function(data) {

				var vals = data.data.values;
				supPromoPresses = vals;

				for(var d in vals) {
					var objKeys = Object.keys(vals[d]);
					var objVals = Object.values(vals[d]);

					for(var k in objKeys) {
						var dayDt = new Date(objKeys[k] + " 00:00:00");
						for(var promo in result) {
							if(result[promo].approved != null) {
								if(result[promo].thisWeek == undefined || result[promo].thisWeek == "-") 
									result[promo].thisWeek = 0;
								if(result[promo].lastWeek == undefined || result[promo].lastWeek == "-") 
									result[promo].lastWeek = 0;
								if(result[promo].prevWeek == undefined || result[promo].prevWeek == "-") 
									result[promo].prevWeek = 0;
								if(result[promo].allWeeks == undefined || result[promo].allWeeks == "-") 
									result[promo].allWeeks = 0;

								if(result[promo].promo_id == d) {
									result[promo].allWeeks += parseInt(objVals[k]);
									
									if(dayDt.getTime() >= aa.getTime() && dayDt.getTime() <= bb.getTime()) {
										result[promo].thisWeek += parseInt(objVals[k]);
									}
								 	else if(dayDt.getTime() >= cc.getTime() && dayDt.getTime() <= dd.getTime()) {
										result[promo].lastWeek += parseInt(objVals[k]);
									}
									else if(dayDt.getTime() >= ee.getTime() && dayDt.getTime() <= ff.getTime()) {
										result[promo].prevWeek += parseInt(objVals[k]);
									}
								}
							}
						}
					}
				}

				for(var promo in result) {
					if(result[promo].approved != null) {
						if(result[promo].lastWeek > 0 && result[promo].prevWeek > 0) {
							result[promo].intChangeLastWeek = ((result[promo].lastWeek - result[promo].prevWeek) / result[promo].prevWeek) * 100;
							result[promo].intChangeLastWeek = result[promo].intChangeLastWeek.toFixed(0);
						}
						else if(result[promo].lastWeek > 0 && result[promo].prevWeek == 0) {
							result[promo].intChangeLastWeek = 100;
						}
						else if(result[promo].lastWeek == 0 && result[promo].prevWeek > 0) {
							result[promo].intChangeLastWeek = -100;
						}

						if(result[promo].thisWeek > 0 && result[promo].lastWeek > 0) {
							result[promo].intChangeThisWeek = ((result[promo].thisWeek - result[promo].lastWeek) / result[promo].lastWeek) * 100;
							result[promo].intChangeThisWeek = result[promo].intChangeThisWeek.toFixed(0);
						}
						else if(result[promo].thisWeek > 0 && result[promo].lastWeek == 0) {
							result[promo].intChangeThisWeek = 100;
						}
						else if(result[promo].thisWeek == 0 && result[promo].lastWeek > 0) {
							result[promo].intChangeThisWeek = -100;
						}
					}
				}

				supPromos = result;

				panel.events({
				  	event: impressions,
				  	type: "general",
				  	unit: "day",
				  	from_date: "2020-08-01",
				  	to_date: toDate
				}).then(function(impData) {
					if(impData !== undefined && impData.data !== undefined && impData.data.values !== undefined) {
						var impVals = impData.data.values;
						supPromoImps = impVals;
						
						for(var i in impVals) {	
							var objImpKeys = Object.keys(impVals[i]);
							var objImpVals = Object.values(impVals[i]);

							for(var iv in objImpVals) {
								var promo_id = i.substring(i.indexOf("-")+1);
								var dayDt = new Date(objImpKeys[iv] + " 00:00:00");

								for(var promo in result) {
									if(result[promo].approved != null) {
										if(result[promo].thisWeekImp == undefined || result[promo].thisWeekImp == "-") 
											result[promo].thisWeekImp = 0;
										if(result[promo].lastWeekImp == undefined || result[promo].lastWeekImp == "-") 
											result[promo].lastWeekImp = 0;
										if(result[promo].prevWeekImp == undefined || result[promo].prevWeekImp == "-") 
											result[promo].prevWeekImp = 0;
										if(result[promo].allWeeksImp == undefined || result[promo].allWeeksImp == "-") 
											result[promo].allWeeksImp = 0;

										if(result[promo].promo_id == promo_id) {
											result[promo].allWeeksImp += parseInt(objImpVals[iv]);
											
											if(dayDt.getTime() >= aa.getTime() && dayDt.getTime() <= bb.getTime()) {
												result[promo].thisWeekImp += parseInt(objImpVals[iv]);
											}
										 	else if(dayDt.getTime() >= cc.getTime() && dayDt.getTime() <= dd.getTime()) {
												result[promo].lastWeekImp += parseInt(objImpVals[iv]);
											}
											else if(dayDt.getTime() >= ee.getTime() && dayDt.getTime() <= ff.getTime()) {
												result[promo].prevWeekImp += parseInt(objImpVals[iv]);
											}
										}
									}
								}
							}
						}

						for(var promo in result) {
							if(result[promo].approved != null) {
								if(result[promo].lastWeekImp > 0 && result[promo].prevWeekImp > 0) {
									result[promo].intChangeLastWeekImp = ((result[promo].lastWeekImp - result[promo].prevWeekImp) / result[promo].prevWeekImp) * 100;
									result[promo].intChangeLastWeekImp = result[promo].intChangeLastWeekImp.toFixed(0);
								}
								else if(result[promo].lastWeekImp > 0 && result[promo].prevWeekImp == 0) {
									result[promo].intChangeLastWeekImp = 100;
								}
								else if(result[promo].lastWeekImp == 0 && result[promo].prevWeekImp > 0) {
									result[promo].intChangeLastWeekImp = -100;
								}

								if(result[promo].thisWeekImp > 0 && result[promo].lastWeekImp > 0) {
									result[promo].intChangeThisWeekImp = ((result[promo].thisWeekImp - result[promo].lastWeekImp) / result[promo].lastWeekImp) * 100;
									result[promo].intChangeThisWeekImp = result[promo].intChangeThisWeekImp.toFixed(0);
								}
								else if(result[promo].thisWeekImp > 0 && result[promo].lastWeekImp == 0) {
									result[promo].intChangeThisWeekImp = 100;
								}
								else if(result[promo].thisWeekImp == 0 && result[promo].lastWeekImp > 0) {
									result[promo].intChangeThisWeekImp = -100;
								}
							}
						}

						supPromos = result;
					}

					_callback();
				});
			});
		});
	});
}

exports.supplierLogout = function(req, res) {
	req.session.reset();
  	res.redirect('/estlogin');
}


/* CAMPAIGN ACTIONS */

exports.supplierActivateCampaign = function(req, res) {
	var promoId = req.body.promoId;

	const promo = {
	    PROMO: {
	      promo_id: promoId,
	      active: 1
	    }
	};

	Promo.update(promo, function(err1, result1) {
		res.send("true");
	});
}


/* ADD CAMPAIGN */

exports.supplierAddCampaign = function(req, res) {
	var supplier = req.session.supplier;
	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

  	req.session.addCampaign = null;

	if(supplier) {
		Location.getAll(function(errLoc, locations) {
			res.render('supplierdashboard', {
				currPage: 'addcampaign',
				supplier: supplier,
				promoLocations: locations,
				//addCampaignStep: 1,
				tz: tz,
				todayDay: getTodaysDayInt(tz),
				userLocationAllowed: req.session.userLocationAllowed
			});
		});
	}
	else {
		res.redirect('/estlogin');
	}
}

exports.supplierPersistLogistics = function(req, res) {
	var logistics = {};

	var features = "";
	if(req.body.campaignType == "Specials" || req.body.campaignType == "Events") {
		features = req.body.dineinTakeout != "Carryout" ? req.body.dineinTakeout : "Takeout";
	}
	else {
	    if(req.body.patio == "true")
	      	features += "Patio, ";
	    if(req.body.rooftop == "true")
	      	features += "Rooftop, ";
	    if(req.body.dog == "true")
	      	features += "Dog-friendly, ";
	    if(req.body.brunch == "true")
	      	features += "Brunch, ";
	    if(req.body.delivery == "true")
	      	features += "Delivery, ";
	    if(req.body.takeout == "true")
	      	features += "Takeout, ";

	  	// remove last comma
	    if(features != "")
	      	features = features.substring(0, features.length-2);
  	}

  	logistics.features = features;

  	var day = "";
	var dayStr = "";
	if(req.body.everyday == "true" || (req.body.mon == "true" && req.body.tue == "true" && req.body.wed == "true" && req.body.thu == "true" && req.body.fri == "true" && req.body.sat == "true" && req.body.sun == "true")) {
		day = "-1";
		dayStr = "Everyday";
	}
	else {
	  	if(req.body.mon == "true") {
	  		day += "1,";
	    	dayStr += "Monday, ";
	  	}
	  	if(req.body.tue == "true") {
	  		day += "2,";
	   	 	dayStr += "Tuesday, ";
	  	}
	  	if(req.body.wed == "true") {
	  		day += "3,";
	    	dayStr += "Wednesday, ";
	  	}
	  	if(req.body.thu == "true") {
	  		day += "4,";
	    	dayStr += "Thursday, ";
	  	}
	  	if(req.body.fri == "true") {
	  		day += "5,";
	    	dayStr += "Friday, ";
	  	}
	  	if(req.body.sat == "true") {
	  		day += "6,";
	    	dayStr += "Saturday, ";
	  	}
	  	if(req.body.sun == "true") {
	  		day += "0,";
	    	dayStr += "Sunday, ";
	  	}

	  	day = day.substring(0, day.length-1);
	  	dayStr = dayStr.substring(0, dayStr.length-2);
  	}
  	logistics.day = day;
    logistics.dayStr = dayStr;

  	Location.getAll(function(errLoc, locations) {
  		var loc;
		for(var l in locations) {
			if(locations[l].location_id == req.body.location) {
				loc = locations[l];
			}
		}
		logistics.locationStr = loc.location;
		logistics.latitude = loc.latitude;
		logistics.longitude = loc.longitude;

		req.session.addCampaign = {
			...req.body,
			...logistics
		}
		res.json(req.session.addCampaign);
	});
}

exports.supplierPersistDesign = function(req, res) {
	var imgPaths = {};

	if(req.files && req.files.promoImg && req.files.promoDetailsImg) {
		var promoImg = req.files.promoImg;
		var promoDetailsImg = req.files.promoDetailsImg;

		var path = __dirname + '/../public/img/appy-promos/' + promoImg.name;
		imgPaths.campaignImgPath = path;
		promoImg.mv(path, function(err) {
		    if(err)
		      	return res.status(500).send(err);
		    // Don't send to AWS S3 yet
		});

		if(promoImg.name != promoDetailsImg.name) {
			path = __dirname + '/../public/img/appy-promos/' + promoDetailsImg.name;
			imgPaths.campaignDetailsImgPath = path;
			promoDetailsImg.mv(path, function(err) {
			    if(err)
			      	return res.status(500).send(err);
			    // Don't send to AWS S3 yet
			});
		}
	}

	var designParams = req.body.json;
	try {
		designParams = JSON.parse(req.body.json);
	}
	catch(e) {
		designParams = req.body.json;
	}
	
	req.session.addCampaign = {
		...req.session.addCampaign,
		...designParams,
		...imgPaths
	}

	res.json(req.session.addCampaign);
}

exports.supplierSaveCampaign = function(req, res) {
	if(req.body.background && req.body.details_background) {
	    // Create S3 service object
		s3 = new AWS.S3({apiVersion: '2006-03-01'});

		// Configure the file stream and obtain the upload parameters
		var fileStream = fs.createReadStream(req.body.background);
		fileStream.on('error', function(err) {
		  console.log('File Error', err);
		});

		// call S3 to retrieve upload file to specified bucket
		var uploadParams = {Bucket: 'appy-promos', Key: req.body.backgroundName, Body: fileStream, ACL: 'public-read'};

		// call S3 to retrieve upload file to specified bucket
		s3.upload(uploadParams, function (err, data) {
		  if (err) {
		    console.log("Error", err);
		  } if (data) {
		    //console.log("Upload Success", data.Location);
		  }
		});

		// Create S3 service object
		s3 = new AWS.S3({apiVersion: '2006-03-01'});

		// Configure the file stream and obtain the upload parameters
		var fileStream = fs.createReadStream(req.body.details_background);
		fileStream.on('error', function(err) {
		  console.log('File Error', err);
		});

		// call S3 to retrieve upload file to specified bucket
		var uploadParams = {Bucket: 'appy-promos', Key: req.body.details_backgroundName, Body: fileStream, ACL: 'public-read'};

		// call S3 to retrieve upload file to specified bucket
		s3.upload(uploadParams, function (err, data) {
		  if (err) {
		    console.log("Error", err);
		  } if (data) {
		    //console.log("Upload Success", data.Location);
		  }
		});
	}

	var cookies = cookie.parse(req.headers.cookie || '');
	var tz = cookies.tz;
	var now = Common.newDate(tz);

	//console.log(req.body);

	const promo = {
	    PROMO: {
	      title: req.body.title,
	      label: req.body.label,
	      description: req.body.description,
	      day: req.body.day,
	      latitude: req.body.latitude,
	      longitude: req.body.longitude,
	      promo_type: req.body.campaignType,
	      keywords: req.body.keywords,
	      features: req.body.features,
	      background: req.body.backgroundName,
	      fontColor: req.body.fontColor,
	      textAlign: req.body.textAlign,
	      verticalAlign: req.body.verticalAlign,
	      details_background: req.body.details_backgroundName,
	      details_fontColor: req.body.details_fontColor,
	      link: req.body.link != "undefined" ? req.body.link : "",
	      link_text: req.body.link_text != "undefined" ? req.body.link_text : "",
	      link_text_color: req.body.link_text_color != "undefined" ? req.body.link_text_color : "",
	      link_bg_color: req.body.link_bg_color != "undefined" ? req.body.link_bg_color : "",
	      link_btn_text: req.body.link_btn_text != "undefined" ? req.body.link_btn_text : "",
	      link_btn_text_color: req.body.link_btn_text_color != "undefined" ? req.body.link_btn_text_color : "",
	      link_btn_bg_color: req.body.link_btn_bg_color != "undefined" ? req.body.link_btn_bg_color : "",
	      active: 0,
	      archive_dt: null,
	      created_at: now,
	      updated_at: now
	    }
	};

	//console.log(promo);
	
	Promo.insert(promo, function(err1, result1) {
		//console.log(err1);
		//console.log(result1);
		//console.log(" ");

		var promoId = result1.insertId;

		const supplierpromo = {
			SUPPLIER_PROMO: {
				supplier_id: req.session.supplier.supplier_id,
				promo_id: promoId,
				approved_at: null,
				approved_by: null,
				created_at: now,
				updated_at: now
			}
		}

		SupplierPromo.insert(supplierpromo, function(err3, result3) {
			//console.log(err3);
			//console.log(result3);
			//console.log(" ");

			fs.readFile(__dirname + '/../public/emails/ah_approve_campaign_email.html', function (err, html) {
			    if (!err) {
			        var msg = ""+html;
		        	msg = msg.replace("{{ supplier }}", req.session.supplier.name);

		        	var transporter = nodemailer.createTransport({
				      service: 'Gmail',
				      auth: {
				        user: 'appy-support@appyhourmobile.com',
				        pass: 'gpzkftethvbstvfn'
				      }
				    });

				    var mailOptions = {
				      from: 'AppyHour <appy-support@appyhourmobile.com>',
				      to: 'appy-support@appyhourmobile.com',
				      subject: 'Approve/Deny Campaign',
				      html: msg
				    };

				    transporter.sendMail(mailOptions, function(error, info){
				      if (error) {
				        console.log(error);
				        //res.send("Sorry, we're having some issues sending your message. Please try again later.");
				      } else {
				        console.log('Email sent: ' + info.response);
				        //res.send("true");
				      }
				    });
				}
			});


			init(req, res);
		});
	});
}



/* EDIT CAMPAIGN */

exports.supplierSavePromo = function(req, res) {
	var supplierId = req.session.supplier.supplier_id;
	var img = "";
	var textY = req.body.addTextY;
	var textN = req.body.addTextN;
	var title = "";
	var label = "";
	var textColor = "white";
	var textAlign = "left";
	var verticalAlign = "top";
	if(textY == "true" || textY == true) {
		title = req.body.title;
		label = req.body.label;
		textColor = req.body.textColor;
		textAlign = req.body.textAlign;
		verticalAlign = req.body.vertAlign;
	}
	//var description = req.body.description;
	var day = "";
	var location = req.body.location;
	//var lat = "";
	//var lon = "";
	var promoType = req.body.promoType;
	var keywords = req.body.keywords;
	var features = "";
	//var active = req.body.active;

	var detailsImg = "";
	var detailsText = "";
	var detailsTextColor = "white";
	var detailsTextY = req.body.addTextYDetails;
	var detailsTextN = req.body.addTextNDetails;
	if(detailsTextY == "true" || detailsTextY == true) {
		detailsText = req.body.text;
		detailsTextColor = req.body.textColorDetails;
	}

	// cta
	var detailsActionBtn = req.body.detailsActionBtn;
	var link = req.body.link;
	var linkText = req.body.linkText;
	var linkTextColor = req.body.linkTextColor;
	var linkBgColor = req.body.linkBgColor;
	var ctaLabel = req.body.ctaLabel;
	var ctaLabelTextColor = req.body.ctaLabelTextColor;
	var ctaLabelBgColor = req.body.ctaLabelBgColor;

	if(detailsActionBtn == "N") {
		link = null;
		linkText = null;
		linkTextColor = null;
		linkBgColor = null;
		ctaLabel = null;
		ctaLabelTextColor = null;
		ctaLabelBgColor = null;
	}

	// day(s)
	if(req.body.everyday == "true") {
		day = "-1";
	}
	else  {
		if(req.body.sun == "true")
			day += "0,";
		if(req.body.mon == "true")
			day += "1,";
		if(req.body.tue == "true")
			day += "2,";
		if(req.body.wed == "true")
			day += "3,";
		if(req.body.thu == "true")
			day += "4,";
		if(req.body.fri == "true")
			day += "5,";
		if(req.body.sat == "true")
			day += "6,";

		day = day.substring(0, day.length-1);
	}

	// establishment features
	if(req.body.promoType == "establishments") {
		if(req.body.patio != undefined)
			features = features + "patio;";
		if(req.body.rooftop != undefined)
			features = features + "rooftop;";
		if(req.body.dogs != undefined)
			features = features + "dog_friendly;";
		if(req.body.brunch != undefined)
			features = features + "brunch;";
		if(req.body.delivery != undefined)
			features = features + "delivery;";
		if(req.body.takeout != undefined)
			features = features + "takeout;";
		if(req.body.blackowned != undefined)
			features = features + "black_owned;";
		if(req.body.naoptions != undefined)
			features = features + "na_options;";
		if(req.body.paying != undefined)
			features = features + "paying;";
	}

	// special features
	if(req.body.promoType == "specials") {
		if(req.body.carryout != undefined)
			features = features + "takeout;";
	}

	//remove last semi-colon
	if(features != "")
		features = features.substring(0, features.length-1); 

	var keywordsStr = "";
	keywords = keywords.split(",");
	for(var k = 0; k < keywords.length; k++) {
		if(k < keywords.length - 1)
			keywordsStr += keywords[k].trim() + ";";
		else
			keywordsStr += keywords[k].trim();
	}

	if(req.files && req.files.promoImg) {
		var promoImg = req.files.promoImg;
		img = promoImg.name;

		var path = __dirname + '/../public/img/appy-promos/' + img;
		promoImg.mv(path, function(err) {
		    if (err)
		      	return res.status(500).send(err);

		    // Create S3 service object
			s3 = new AWS.S3({apiVersion: '2006-03-01'});

			// Configure the file stream and obtain the upload parameters
			var fileStream = fs.createReadStream(path);
			fileStream.on('error', function(err) {
			  console.log('File Error', err);
			});

			// call S3 to retrieve upload file to specified bucket
			var uploadParams = {Bucket: 'appy-promos', Key: img, Body: fileStream, ACL: 'public-read'};

			// call S3 to retrieve upload file to specified bucket
			s3.upload(uploadParams, function (err, data) {
			  if (err) {
			    console.log("Error", err);
			  } if (data) {
			    console.log("Upload Success", data.Location);
			  }
			});
		});
	}
	else {
		img = req.body.promoImgOriginal;
	}

	if(req.files && req.files.campaignDetailsImg) {
		var campaignDetailsImg = req.files.campaignDetailsImg;
		detailsImg = campaignDetailsImg.name;

		var path = __dirname + '/../public/img/appy-promos/' + detailsImg;
		campaignDetailsImg.mv(path, function(err) {
		    if (err)
		      	return res.status(500).send(err);

		    // Create S3 service object
			s3 = new AWS.S3({apiVersion: '2006-03-01'});

			// Configure the file stream and obtain the upload parameters
			var fileStream = fs.createReadStream(path);
			fileStream.on('error', function(err) {
			  console.log('File Error', err);
			});

			// call S3 to retrieve upload file to specified bucket
			var uploadParams = {Bucket: 'appy-promos', Key: detailsImg, Body: fileStream, ACL: 'public-read'};

			// call S3 to retrieve upload file to specified bucket
			s3.upload(uploadParams, function (err, data) {
			  if (err) {
			    console.log("Error", err);
			  } if (data) {
			    console.log("Upload Success", data.Location);
			  }
			});
		});
	}
	else {
		detailsImg = req.body.campaignDetailsImgOriginal;
	}

	Location.getAllByLocationId(location, function(err, loc) {
		var cookies = cookie.parse(req.headers.cookie || '');
  		var tz = cookies.tz;
  		var now = Common.newDate(tz);

		const promo = {
		    PROMO: {
		      promo_id: req.body.promo_id,
		      title: title,
		      label: label,
		      description: detailsText,
		      day: day,
		      latitude: loc.latitude,
		      longitude: loc.longitude,
		      promo_type: promoType,
		      keywords: keywordsStr,
		      features: features,
		      background: img,
		      fontColor: textColor,
		      textAlign: textAlign,
		      verticalAlign: verticalAlign,
		      details_background: detailsImg,
		      details_fontColor: detailsTextColor,
		      link: link,
		      link_text: ctaLabel,
		      link_text_color: ctaLabelTextColor,
		      link_bg_color: ctaLabelBgColor,
		      link_btn_text: linkText,
		      link_btn_text_color: linkTextColor,
		      link_btn_bg_color: linkBgColor
		    }
		};

		Promo.update(promo, function(err1, result1) {
			init(req, res);
		});
	});
}



/* PROMO DETAILS */

exports.supplierPromoDetails = function(req, res) {
	var promoId = req.query.promoId;
	var supplier = req.session.supplier;

	if(supplier) {
		//if(supPromos !== undefined) {
		//	supplierGetDetails(promoId, req, res);
		//}
		//else {
			supplierGetData(supplier.supplier_id, req, function() {
				supplierGetDetails(promoId, req, res);
			});
		//}
	}
	else {
		res.redirect('/estlogin');
	}
}

function supplierGetDetails(promoId, req, res) {
	var promo;

	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	for(var p in supPromos) {
		if(supPromos[p].promo_id == promoId) {
			promo = supPromos[p];
		}
	}

	if(promo !== undefined) {
		supplierPromoItems(promo, req, function() {
			var tableData = [];
		
			for(var d in supPromoPresses) {
				if(d == promo.promo_id) {
					var objKeys = Object.keys(supPromoPresses[d]);
					var objVals = Object.values(supPromoPresses[d]);
					var tableData = [];

					for(var k in objKeys) {
						var dt = new Date(objKeys[k] + " 00:00:00");
						var p = parseInt(objVals[k]);

						if(promo.archived == null || promo.archived == "") {
							if(dt.getTime() >= promo.created_at.getTime()) {
								tableData.push({ date: objKeys[k], presses: p, imps: 0 });
							}
						}
						else {
							if(dt.getTime() >= promo.created_at.getTime() && dt.getTime() <= promo.archive_dt.getTime()) {
								tableData.push({ date: objKeys[k], presses: p, imps: 0 });
							}
						}
					}

					var objImpKeys = Object.keys(supPromoImps["PromoImpression-"+d]);
					var objImpVals = Object.values(supPromoImps["PromoImpression-"+d]);
					var imps = [];

					for(var ik in objImpKeys) {
						var dt = new Date(objImpKeys[ik] + " 00:00:00");
						var i = parseInt(objImpVals[ik]);

						for(var t in tableData) {
							if(objImpKeys[ik] == tableData[t].date) {
								tableData[t].imps = i;
							}
						}
					}
				}
			}						
		
			tableData.sort((a, b) => (a.date < b.date) ? 1 : -1);

			res.render('supplierdashboard', {
				currPage: 'promodetails',
				supplier: req.session.supplier,
				presses: tableData,
				promo: promo,
				tz: tz,
				todayDay: getTodaysDayInt(tz),
				userLocationAllowed: req.session.userLocationAllowed
			});
		});
	}
	else {
		res.render('supplierdashboard', {
			currPage: 'notauthorized',
			supplier: req.session.supplier,
			tz: tz,
			todayDay: getTodaysDayInt(tz),
			userLocationAllowed: req.session.userLocationAllowed
		});
	}
}

function supplierPromoItems(promo, req, _callback) {
	promo.items = [];
	promo.itemsMN = 0;
	promo.itemsKY = 0;
	promo.itemsIA = 0;
	promo.itemsDen = 0;
	promo.itemsTampa = 0;

	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	var key = promo.keywords;
	var keys = "";

  	if(key != null && key != "") {
    	key = key.split(";");
    	for(k in key) {
			keys += key[k] + ",";
		}
		keys = keys.substring(0, keys.length-1);
  	}
  	else {
    	key = [];
  	}

	var feat = promo.features;
  	if(feat != null && feat != "")
    	feat = feat.split(";");
  	else
    	feat = [];
	
	var now = Common.newDate(tz);
	var n = getTodaysDayInt(tz);

	// we need today at 4am, so that we can see if exclusive special was created today
	var todayAt4am = Common.newDate(tz);
	if (todayAt4am.getHours() < 4) {
		todayAt4am.setDate(todayAt4am.getDate()-1)
	}
	todayAt4am.setHours(4);
	todayAt4am.setMinutes(0);
	todayAt4am.setSeconds(0);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);

	var expired = [];
	var current = [];
	var upcoming = [];

	if(promo.promo_type == "establishments") {
		Establishment.getAll(function(err, ests) {
			for(var e in ests) {
				var hasFeats = true;
	        	for(var f in feat) {
	          		if((feat[f] == "patio" && ests[e].patio == 1) ||
	             	 (feat[f] == "rooftop" && ests[e].rooftop == 1) ||
	             	 (feat[f] == "brunch" && ests[e].brunch == 1) ||
	             	 (feat[f] == "dog_friendly" && ests[e].dog_friendly == 1) ||
	             	 (feat[f] == "delivery" && ests[e].delivery == 1) ||
	             	 (feat[f] == "takeout" && ests[e].takeout == 1) ||
	             	 (feat[f] == "black_owned" && ests[e].black_owned == 1) ||
	             	 (feat[f] == "na_options" && ests[e].na_options == 1) ||
	             	 (feat[f] == "paying" && ests[e].stripe_id != null)) {
	            		hasFeats = true;
	          		}
	          		else {
	          			hasFeats = false;
	          			break;
	          		}
	        	}
	        	if(hasFeats) {
	        		ests[e].isEstablishment = true;

            		ests[e].fullAddress = ests[e].address_1;
            		if(ests[e].address_2 != null && ests[e].address_2 != "")
            			ests[e].fullAddress += " " + ests[e].address_2;

            		if(getDistanceFromLatLng(ests[e].latitude, ests[e].longitude, 44.9778, -93.2650) <= 30) // Minne/STP
			      		promo.itemsMN++;
			      	else if(getDistanceFromLatLng(ests[e].latitude, ests[e].longitude, 41.6611, -91.5302) <= 30) // Iowa City
			      		promo.itemsIA++;
			      	else if(getDistanceFromLatLng(ests[e].latitude, ests[e].longitude, 38.2527, -85.7585) <= 30) // Louisville
			      		promo.itemsKY++;
			      	else if(getDistanceFromLatLng(ests[e].latitude, ests[e].longitude, 39.7392, -104.9903) <= 30) // Denver
			      		promo.itemsDen++;
			      	else if(getDistanceFromLatLng(ests[e].latitude, ests[e].longitude, 27.7634, -82.5437) <= 30) // Tampa
		                promo.itemsTampa++;

            		promo.items.push(ests[e]);
	        	}
		    }

		    promo.items.sort((a, b) => (a.name > b.name) ? 1 : -1);

		    _callback();
		});
    }
    else if(promo.promo_type == "specials") {
    	Special.search(keys, function(err, result) {
    		//Special.getAll(function(err2, specials) {
    			for(var r in result) {
	    			//if(promo.day == -1 || (promo.day != -1 && promo.day == n)) {

	    			var addSpecial = false;

	    			if(feat.length > 0) {
		    			for(var f in feat) {
		          			if(feat[f] == "takeout" && result[r].carryout == 1) {
		          				addSpecial = true;
		          			}
		          		}
	          		}
	          		else {
	          			addSpecial = true;
	          		}

	          		if(addSpecial) {
		    			result[r].isSpecial = true;
				      	result[r].tz = getTimezone(result[r].latitude, result[r].longitude);
				      	result[r].deal_type_ref_id_TS = getDealTypeImageByRefId(result[r].deal_type_ref_id);

				      	//if(result[r].day == n) {
				      	if(result[r].day > -1) {
		  					if(result[r].start_time.getHours() <= 4) {
		  						result[r].start_time.setMonth(tmw.getMonth());
		  						result[r].start_time.setDate(tmw.getDate());
		  						result[r].start_time.setYear(1900+tmw.getYear());
		  					}
		  					else {
		  						result[r].start_time.setMonth(now.getMonth());
		  						result[r].start_time.setDate(now.getDate());
		  						result[r].start_time.setYear(1900+now.getYear());
		  					}

		  					if(result[r].end_time.getHours() <= 4) {
		  						result[r].end_time.setMonth(tmw.getMonth());
		  						result[r].end_time.setDate(tmw.getDate());
		  						result[r].end_time.setYear(1900+tmw.getYear());
		  					}
		  					else {
		  						result[r].end_time.setMonth(now.getMonth());
		  						result[r].end_time.setDate(now.getDate());
		  						result[r].end_time.setYear(1900+now.getYear());
		  					}
		  				}
		  				
		  				result[r].dayStr = dayOrDateToString(result[r].day, result[r].start_time, result[r].end_time, tz);
						result[r].duration = formatStartEndTime(result[r].start_time, result[r].tz, tz) + " - " + formatStartEndTime(result[r].end_time, result[r].tz, tz);

						if(promo.day == -1 || (promo.day > -1 && promo.day == result[r].day)) {
							if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 44.9778, -93.2650) <= 30) { // Minne/STP
					      		result[r].location = "minneapolis";
					      		promo.itemsMN++;
					      	}
					      	else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 41.6611, -91.5302) <= 30) { // Iowa City
					      		result[r].location = "iowacity";
					      		promo.itemsIA++;
					      	}
					      	else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 38.2527, -85.7585) <= 30) { // Louisville
					      		result[r].location = "louisville";
					      		promo.itemsKY++;
					      	}
					      	else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 39.7392, -104.9903) <= 30) { // Denver
					      		result[r].location = "denver";
			      				promo.itemsDen++;
			      			}
			      			else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 27.7634, -82.5437) <= 30) { // Tampa
			      				result[r].location = "tampabay";
		                		promo.itemsTampa++;
		                	}

			  				// expired 
			  				if(result[r].start_time > todayAt4am && result[r].end_time < now) {
			  					result[r].expired = true;
			  					expired.push(result[r]);
			  				}
			  				// current 
			  				else if(result[r].start_time <= now && result[r].end_time >= now && (result[r].day == n || result[r].day == -1)) {
			  					result[r].timeremaining = dateDiff(result[r].end_time, result[r].tz, tz);
			  					result[r].current = true;
			  					current.push(result[r]);
			  				}
			  				else {
			  					result[r].upcoming = true;
			  					upcoming.push(result[r]);
			  				}
			  				/* // upcoming weekly
			  				else if(result[r].day == n && result[r].start_time > now) {
			  					result[r].upcoming = true;
			  					upcoming.push(result[r]);
			  				}
			  				// upcoming one-time
			  				else if(result[r].start_time > now && result[r].start_time.getDate() == now.getDate() && result[r].start_time.getMonth() == now.getMonth() && result[r].start_time.getYear() == now.getYear()) {
			  					result[r].upcoming = true;
			  					upcoming.push(result[r]);
			  				}*/
		  				}
	  				}
	    		}

	    		/*
	    		for(var s in specials) {
	    			for(var f in feat) {
	          			if(feat[f] == "takeout" && specials[s].carryout == 1) {
			    			specials[s].isSpecial = true;

			    			if(getDistanceFromLatLng(specials[s].latitude, specials[s].longitude, 44.9778, -93.2650) <= 30) // Minne/STP
					      		specials[s].location = "minneapolis";
					      	else if(getDistanceFromLatLng(specials[s].latitude, specials[s].longitude, 41.6611, -91.5302) <= 30) // Iowa City
					      		specials[s].location = "iowacity";
					      	else if(getDistanceFromLatLng(specials[s].latitude, specials[s].longitude, 38.2527, -85.7585) <= 30) // Louisville
					      		specials[s].location = "louisville";
					      	//else if(getDistanceFromLatLng(specials[s].latitude, specials[s].longitude, 41.5868, -93.6250) <= 30) // Des Moines
					      	//	specials[s].location = "desmoinesia";
					      	//else if(getDistanceFromLatLng(specials[s].latitude, specials[s].longitude, 39.7392, -104.9903) <= 30) // Denver
					      	//	specials[s].location = "denverco";

					      	specials[s].tz = getTimezone(specials[s].latitude, specials[s].longitude);

					      	specials[s].deal_type_ref_id_TS = getDealTypeImageByRefId(specials[s].deal_type_ref_id);

					      	//if(specials[s].day == n) {
					      	if(specials[s].day > -1) {
			  					if(specials[s].start_time.getHours() <= 4) {
			  						specials[s].start_time.setMonth(tmw.getMonth());
			  						specials[s].start_time.setDate(tmw.getDate());
			  						specials[s].start_time.setYear(1900+tmw.getYear());
			  					}
			  					else {
			  						specials[s].start_time.setMonth(now.getMonth());
			  						specials[s].start_time.setDate(now.getDate());
			  						specials[s].start_time.setYear(1900+now.getYear());
			  					}

			  					if(specials[s].end_time.getHours() <= 4) {
			  						specials[s].end_time.setMonth(tmw.getMonth());
			  						specials[s].end_time.setDate(tmw.getDate());
			  						specials[s].end_time.setYear(1900+tmw.getYear());
			  					}
			  					else {
			  						specials[s].end_time.setMonth(now.getMonth());
			  						specials[s].end_time.setDate(now.getDate());
			  						specials[s].end_time.setYear(1900+now.getYear());
			  					}
			  				}
			  				
			  				specials[s].dayStr = dayOrDateToString(specials[s].day, specials[s].start_time, specials[s].end_time, tz);
							specials[s].duration = formatStartEndTime(specials[s].start_time, specials[s].tz, tz) + " - " + formatStartEndTime(specials[s].end_time, specials[s].tz, tz);

							if(promo.day == -1 || (promo.day > -1 && promo.day == specials[s].day)) {
				  				// expired 
				  				if(specials[s].start_time > todayAt4am && specials[s].end_time < now) {
				  					specials[s].expired = true;
				  					expired.push(specials[s]);
				  				}
				  				// current 
				  				else if(specials[s].start_time <= now && specials[s].end_time >= now && (specials[s].day == n || specials[s].day == -1)) {
				  					specials[s].timeremaining = dateDiff(specials[s].end_time, specials[s].tz, tz);
				  					specials[s].current = true;
				  					current.push(specials[s]);
				  				}
				  				else {
				  					specials[s].upcoming = true;
				  					upcoming.push(specials[s]);
				  				}
				  				<!--
				  				// upcoming weekly
				  				else if(specials[s].day == n && specials[s].start_time > now) {
				  					specials[s].upcoming = true;
				  					upcoming.push(specials[s]);
				  				}
				  				// upcoming one-time
				  				else if(specials[s].start_time > now && specials[s].start_time.getDate() == now.getDate() && specials[s].start_time.getMonth() == now.getMonth() && specials[s].start_time.getYear() == now.getYear()) {
				  					specials[s].upcoming = true;
				  					upcoming.push(specials[s]);
				  				}
				  				-->
			  				}
		  				}
	  				}
	    		}
	    		*/

	    		current = sortByEndTime(current);
				upcoming = sortByStartTimeThenEndTime(upcoming);
				expired = sortByStartTimeThenEndTime(expired);

	    		var allPromos = [];

				for(var c in current)
					allPromos.push(current[c]);

				for(var u in upcoming)
					allPromos.push(upcoming[u]);

				for(var e in expired)
					allPromos.push(expired[e]);

	    		promo.items = allPromos;
	    		_callback();
    		});
    	//});
    }
    else if(promo.promo_type == "events") {
    	Event.search(keys, function(err, result) {
    		var events = [];

    		for(var r in result) {
    			var inArr = false;

    			for(var e in events) {
	    			if(events[e].event_id == result[r].event_id) {
	    				inArr = true;
	    				break;
		  			}
    			}

    			if(!inArr) {
    				result[r].isEvent = true;
    				result[r].specials = [];

    				if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 44.9778, -93.2650) <= 30) { // Minne/STP
			      		result[r].location = "minneapolis";
			      		promo.itemsMN++;
			      	}
			      	else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 41.6611, -91.5302) <= 30) { // Iowa City
			      		result[r].location = "iowacity";
			      		promo.itemsIA++;
			      	}
			      	else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 38.2527, -85.7585) <= 30) { // Louisville
			      		result[r].location = "louisville";
			      		promo.itemsKY++;
			      	}
			      	else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 39.7392, -104.9903) <= 30) { // Denver
			      		result[r].location = "denver";
	      				promo.itemsDen++;
	      			}
	      			else if(getDistanceFromLatLng(result[r].latitude, result[r].longitude, 27.7634, -82.5437) <= 30) { // Tampa
	      				result[r].location = "tampabay";
                		promo.itemsTampa++;
                	}

			      	result[r].tz = getTimezone(result[r].latitude, result[r].longitude);

    				if(result[r].day == n) {
	  					if(result[r].start_time.getHours() <= 4) {
	  						result[r].start_time.setMonth(tmw.getMonth());
	  						result[r].start_time.setDate(tmw.getDate());
	  						result[r].start_time.setYear(1900+tmw.getYear());
	  					}
	  					else {
	  						result[r].start_time.setMonth(now.getMonth());
	  						result[r].start_time.setDate(now.getDate());
	  						result[r].start_time.setYear(1900+now.getYear());
	  					}

	  					if(result[r].end_time.getHours() <= 4) {
	  						result[r].end_time.setMonth(tmw.getMonth());
	  						result[r].end_time.setDate(tmw.getDate());
	  						result[r].end_time.setYear(1900+tmw.getYear());
	  					}
	  					else {
	  						result[r].end_time.setMonth(now.getMonth());
	  						result[r].end_time.setDate(now.getDate());
	  						result[r].end_time.setYear(1900+now.getYear());
	  					}
	  				}

	  				result[r].dayStr = dayOrDateToString(result[r].day, result[r].start_time, result[r].end_time, tz);
					result[r].duration = formatStartEndTime(result[r].start_time, result[r].tz, tz) + " - " + formatStartEndTime(result[r].end_time, result[r].tz, tz);

	  				// expired 
	  				if(result[r].start_time > todayAt4am && result[r].end_time < now) {
	  					result[r].expired = true;
	  					expired.push(result[r]);
	  					events.push(result[r]);
	  				}
	  				// current 
	  				else if(result[r].start_time <= now && result[r].end_time >= now) {
	  					result[r].timeremaining = dateDiff(result[r].end_time, result[r].tz, tz);
	  					result[r].current = true;
	  					current.push(result[r]);
	  					events.push(result[r]);
	  				}
	  				else {
	  					result[r].upcoming = true;
	  					upcoming.push(result[r]);
	  					events.push(result[r]);
	  				}
	  				/*
	  				// upcoming weekly
	  				else if(result[r].day != -1) {
	  					result[r].upcoming = true;
	  					upcoming.push(result[r]);
	  					events.push(result[r]);
	  				}
	  				// upcoming one-time
	  				else if(result[r].start_time > now && result[r].start_time.getDate() == now.getDate() && result[r].start_time.getMonth() == now.getMonth() && result[r].start_time.getYear() == now.getYear()) {
	  					result[r].upcoming = true;
	  					upcoming.push(result[r]);
	  					events.push(result[r]);
	  				}*/
    			}
    		}	

    		current = sortByEndTime(current);
			upcoming = sortByStartTimeThenEndTime(upcoming);
			expired = sortByStartTimeThenEndTime(expired);

			var allEvents = [];

			for(var c in current)
				allEvents.push(current[c]);

			for(var u in upcoming)
				allEvents.push(upcoming[u]);

			for(var e in expired)
				allEvents.push(expired[e]);

    		for(var r in result) {
    			for(var e in allEvents) {
    				if(result[r].event_id == allEvents[e].event_id && result[r].special_start_time != null) {
				      	//if(result[r].day == n) {
		  					if(result[r].special_start_time.getHours() <= 4) {
		  						result[r].special_start_time.setMonth(tmw.getMonth());
		  						result[r].special_start_time.setDate(tmw.getDate());
		  						result[r].special_start_time.setYear(1900+tmw.getYear());
		  					}
		  					else {
		  						result[r].special_start_time.setMonth(now.getMonth());
		  						result[r].special_start_time.setDate(now.getDate());
		  						result[r].special_start_time.setYear(1900+now.getYear());
		  					}

		  					if(result[r].special_end_time.getHours() <= 4) {
		  						result[r].special_end_time.setMonth(tmw.getMonth());
		  						result[r].special_end_time.setDate(tmw.getDate());
		  						result[r].special_end_time.setYear(1900+tmw.getYear());
		  					}
		  					else {
		  						result[r].special_end_time.setMonth(now.getMonth());
		  						result[r].special_end_time.setDate(now.getDate());
		  						result[r].special_end_time.setYear(1900+now.getYear());
		  					}
		  				//}

		  				result[r].tz = getTimezone(result[r].latitude, result[r].longitude);
		  				
						result[r].duration = formatStartEndTime(result[r].special_start_time, result[r].tz, tz) + " - " + formatStartEndTime(result[r].special_end_time, result[r].tz, tz);

		  				// expired 
		  				if(result[r].special_start_time > todayAt4am && result[r].special_end_time < now) {
		  					result[r].expired = true;
		  				}
		  				// current 
		  				else if(result[r].special_start_time <= now && result[r].special_end_time >= now && (result[r].day == n || result[r].day == -1)) {
		  					result[r].timeremaining = dateDiff(result[r].special_end_time, result[r].tz, tz);
		  					result[r].current = true;
		  				}
		  				else {
		  					result[r].upcoming = true;
		  					//upcoming.push(result[r]);
		  				}
		  				/*
		  				// upcoming weekly
		  				else if(result[r].day == n && result[r].special_start_time > now) {
		  					result[r].upcoming = true;
		  				}
		  				// upcoming one-time
		  				else if(result[r].special_start_time > now && result[r].special_start_time.getDate() == now.getDate() && result[r].special_start_time.getMonth() == now.getMonth() && result[r].special_start_time.getYear() == now.getYear()) {
		  					result[r].upcoming = true;
		  				}
		  				*/

		  				allEvents[e].specials.push(result[r]);
						break;
    				}
    			}
    		}

    		promo.items = allEvents;

    		_callback();
    	});
    }
}

function getDistanceFromLatLng(userLat, userLon, lat, lon) {
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }

    if(userLat && userLon && lat && lon) {
        var lat2 = parseFloat(userLat);
        var lon2 = parseFloat(userLon);
        var lat1 = parseFloat(lat);
        var lon1 = parseFloat(lon);

        var R = 3959; // Radius in miles has a problem with the .toRad() method below.
        var x1 = lat2 - lat1;
        var dLat = x1.toRad();
        var x2 = lon2 - lon1;
        var dLon = x2.toRad();
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;

        d = Math.round(d * 100)/100;

        return d;
    } else {
        return "";
    }
}

function getDealTypeImageByRefId(refId) {
	switch(refId) {
		case 200: return 'beer-icon.png';
		case 202: return 'shot-icon.png';
    	case 204: return 'wine-icon.png';
    	case 206: return 'mixed-drink-icon.png';
    	case 208: return 'margarita-icon.png';
    	case 210: return 'martini-icon.png';
    	case 212: return 'tumbler-icon.png';
    	case 214: return 'beerbottle.png';
    	case 216: return 'beercan.png';
		case 251: return 'burger-icon.png';
		case 253: return 'appetizer-icon.png';
		case 255: return 'pizza-icon.png';
    	case 257: return 'taco-icon.png';
    	case 259: return 'sushi.png';
    	case 261: return 'bowl.png';
    	case 263: return 'chickenwing.png';
		default: return '';
	}
}




/* ACCOUNT INFO */

exports.supplierAccountInfo = function(req, res) {
	var supplier = req.session.supplier;

	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	if(supplier) {
		res.render('supplierdashboard', {
			currPage: 'info',
			supplier: supplier,
			tz: tz,
			userLocationAllowed: req.session.userLocationAllowed
		});
	}
	else {
		res.redirect('/estlogin');
	} 
}

exports.supplierUpdatePassword = function(req, res) {
	var currPw = req.body.currPw;
	var newPw = req.body.newPw;

	Supplier.getAllBySupplierId(req.session.supplier.supplier_id, function(err, supplier) {
		bcrypt.compare(currPw, supplier.password, function(err, match) {
	    	if(match) {
	    		bcrypt.hash(newPw, 10, function(err, hash) {
		          if(!err) {
		            const s = { 
		              SUPPLIER: {
		                supplier_id: req.session.supplier.supplier_id, 
		                password: hash
		              }
		            };
		            Supplier.update(s, function(err, result) {
		            	Supplier.getAllBySupplierId(req.session.supplier.supplier_id, function(err2, result2) {
			              	// owner
				            if(result2.user_type_ref_id != 306) {
				              result2.password = null;
				              result2.phone_num_formatted = formatPhone(result2.phone_num);
				              req.session.supplier = result2;
				              res.send("true");
				            }
				            // manager
				            else {
				              Supplier.getOwner(result2.supplier_id, function(errOwner, owner) {
				                result2.name = owner.name;
				                result2.profile_pic = owner.profile_pic;
				                result2.password = null;
				                result2.phone_num_formatted = formatPhone(result2.phone_num);
				                req.session.supplier = result2;
				                res.send("true");
				              });
				            }
			        	});	
		            });
		          }
		        });
	    	}
	    	else {
	    		res.send("Current Password is invalid. Please try again.")
	    	}
	    });
	});
}

exports.supplierValidateInfo = function(req, res) {
	var err = [];
	var name = req.body.name;
	var username = req.body.username;
	var email = req.body.email;

	if(req.session.supplier.user_type_ref_id != 306) {
		Supplier.getAllByName(name, function(errName, nameExists) {
			//console.log(errName);
			//console.log(nameExists);
			if(errName != null)
				err.push("Error validating name. Please try again later or contact appy-support@appyhourmobile.com for support.");
			else if(nameExists && nameExists.supplier_id != req.session.supplier.supplier_id)
				err.push("Name already exists. Please choose a different name.");

			Supplier.getAllByUsername(username, function(errUsername, usernameExists) {
				//console.log(errUsername);
				//console.log(usernameExists);
				if(errUsername != null)
					err.push("Error validating username. Please try again later or contact appy-support@appyhourmobile.com for support.");
				else if(usernameExists && usernameExists.supplier_id != req.session.supplier.supplier_id)
					err.push("Username already exists. Please choose a different username.");

				res.send(err);
			});
		});
	}
	else {
		Supplier.getAllByUsername(username, function(errUsername, usernameExists) {
			//console.log(errUsername);
			//console.log(usernameExists);
			if(errUsername != null)
				err.push("Error validating username. Please try again later or contact appy-support@appyhourmobile.com for support.");
			else if(usernameExists && usernameExists.supplier_id != req.session.supplier.supplier_id)
				err.push("Username already exists. Please choose a different username.");

			res.send(err);
		});
	}
}

exports.supplierSaveInfo = function(req, res) {
	var supplier = {};
	var name = req.body.name;
	var username = req.body.username;
	var email = req.body.email;
	var phone = req.body.phone;

	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	phone = phone.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");

	if(req.session.supplier.user_type_ref_id != 306) {
		if(req.files && req.files.profilePic) {
			var profPic = req.files.profilePic;
			var profile_pic_name = profPic.name;
			 
			var path = __dirname + '/../public/img/est-img/' + profile_pic_name;
			profPic.mv(path, function(err) {
			    if (err)
			      	return res.status(500).send(err);

			    // Create S3 service object
				s3 = new AWS.S3({apiVersion: '2006-03-01'});

				// Configure the file stream and obtain the upload parameters
				var fileStream = fs.createReadStream(path);
				fileStream.on('error', function(err) {
				  console.log('File Error', err);
				});

				// call S3 to retrieve upload file to specified bucket
				var uploadParams = {Bucket: 'est-img', Key: profile_pic_name, Body: fileStream, ACL: 'public-read'};

				// call S3 to retrieve upload file to specified bucket
				s3.upload(uploadParams, function (err, data) {
				  if (err) {
				    console.log("Error", err);
				  } if (data) {
				    console.log("Upload Success", data.Location);
				  }
				});
			});

			supplier = {
				SUPPLIER: {
					supplier_id: req.session.supplier.supplier_id,
					name: name,
					username: username,
					email: email,
					phone_num: phone,
					profile_pic: profile_pic_name,
					updated_at: Common.newDate(tz)
				}
			}
		}
		else {
			supplier = {
				SUPPLIER: {
					supplier_id: req.session.supplier.supplier_id,
					name: name,
					username: username,
					email: email,
					phone_num: phone,
					updated_at: Common.newDate(tz)
				}
			}
		}
	}
	else {
		supplier = {
			SUPPLIER: {
				supplier_id: req.session.supplier.supplier_id,
				username: username,
				email: email,
				phone_num: phone,
				updated_at: Common.newDate(tz)
			}
		}
	}

	Supplier.update(supplier, function(err, result) {
		Supplier.getAllBySupplierId(req.session.supplier.supplier_id, function(err2, result2) {
			// owner
            if(result2.user_type_ref_id != 306) {
              result2.password = null;
              result2.phone_num_formatted = formatPhone(result2.phone_num);
              req.session.supplier = result2;
              
              res.render('supplierdashboard', {
				currPage: 'info',
				supplier: req.session.supplier,
				tz: tz,
				userLocationAllowed: req.session.userLocationAllowed
			  });
            }
            // manager
            else {
              Supplier.getOwner(result2.supplier_id, function(errOwner, owner) {
                result2.name = owner.name;
                result2.profile_pic = owner.profile_pic;
                result2.password = null;
                result2.phone_num_formatted = formatPhone(result2.phone_num);
                req.session.supplier = result2;

                res.render('supplierdashboard', {
				  currPage: 'info',
				  supplier: req.session.supplier,
				  tz: tz,
				  userLocationAllowed: req.session.userLocationAllowed
				});
              });
            }
		});
	});
}





/* EDIT MANAGERS */

exports.supplierEditManagers = function(req, res) {
	var supplier = req.session.supplier;

	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	if(supplier) {
		if(supplier.user_type_ref_id != 306) {
			Supplier.getManagers(supplier.supplier_id, function(err, managers) {
				for(var m in managers) {
					managers[m].password = null;
					managers[m].phone_num_formatted = formatPhone(managers[m].phone_num);

					if(managers[m].claimed_at == null && managers[m].claim_email_sent_at != null) {
						managers[m].status = "Pending Activation";
						managers[m].claimEmailSent = dtToStr(managers[m].claim_email_sent_at);
					}
					else if(managers[m].claimed_at != null) {
						managers[m].status = "Active";
					}
				}

				res.render('supplierdashboard', {
					currPage: 'managers',
					supplier: supplier,
					managers: managers,
					tz: tz,
					userLocationAllowed: req.session.userLocationAllowed
				});
			});
		}
		else {
			res.render('supplierdashboard', {
				currPage: 'notauthorized',
				supplier: supplier,
				tz: tz,
				userLocationAllowed: req.session.userLocationAllowed
			});
		}
	}
	else {
		res.redirect('/estlogin');
	} 
}

exports.supplierAddManager = function(req, res) {
	var username = req.body.username;
	var email = req.body.email;
	var phone = req.body.phone;

	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;
	
	Supplier.getAllByUsername(username, function(errUsername, foundUsername) {
		if(!foundUsername) {
			Supplier.getAllByEmail(email, function(errEmail, foundEmail) {
				if(!foundEmail) {
					var password = "AHgenPW$";

			  		bcrypt.hash(password, 10, function(errPw, hash) {
			  			if(!errPw) {
			  				var ceKey = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);

			  				const supplier = {
					      		SUPPLIER: {
					      		  username: username,
					      		  email: email,
					      		  password: hash,
					      		  phone_num: phone,
								  user_type_ref_id: 306, 
					      		  is_active: 1,
					      		  needs_to_pay: 0,
					      		  claim_email_key: ceKey,
					      		  claim_email_sent_at: Common.newDate(tz),
					      		  created_at: Common.newDate(tz),
					      		  updated_at: Common.newDate(tz)
					      		}
					      	}

					      	Supplier.insert(supplier, function(errInsert, userInserted) {
					      		const suppliermanager = {
						      		SUPPLIER_MANAGER: {
						      		  supplier_id: req.session.supplier.supplier_id,
						      		  manager_id: userInserted.insertId,
						      		  created_at: Common.newDate(tz),
						      		  updated_at: Common.newDate(tz)
						      		}
						      	}

					      		SupplierManager.insert(suppliermanager, function(errInsert2, managerInserted) {

									Ref.getAllByRefId(1000, function(errRef, secretPw) {
							        	
										fs.readFile(__dirname + '/../public/emails/sup_new_manager_email.html', function (err, html) {
										    if (!err) {
										        var msg = ""+html;
									        	msg = msg.replace("{{ req.session.supplier.email }}", req.session.supplier.email);
									        	msg = msg.replace("{{ req.session.supplier.name }}", req.session.supplier.name);

									        	while(msg.indexOf("{{ claimUrl }}") > -1) {
													msg = msg.replace("{{ claimUrl }}", "app.appyhourmobile.com/estlogin?email="+email+"&ceKey="+ceKey);
												}

									        	var transporter = nodemailer.createTransport({
											      service: 'Gmail',
											      auth: {
											        user: 'appy-support@appyhourmobile.com',
											        pass: 'gpzkftethvbstvfn'
											      }
											    });

											    var mailOptions = {
											      from: 'AppyHour <appy-support@appyhourmobile.com>',
											      to: email,
											      subject: 'Manager Login Instructions',
											      html: msg
											    };

											    transporter.sendMail(mailOptions, function(error, info){
											      if (error) {
											        console.log(error);
											        res.send("Sorry, we're having some issues sending your message. Please try again later.");
											      } else {
											        console.log('Email sent: ' + info.response);
											        res.send("true");
											      }
											    });
											}
										});
									});
					      		});
					      	});
					    }
					});
				}
				else {
					res.send("Email already exists. Please use a different email.");
				}
			});
		}
		else {
			res.send("Username already exists. Please select a different username.");
		}
	});
}

exports.supplierDeleteManager = function(req, res) {
	var managerId = req.body.managerId;
	
	SupplierManager.deleteByManagerId(managerId, function(errSupplierManager, supplierManagerDeleted) {
		Supplier.delete(managerId, function(errSupplier, supplierDeleted) {
			res.send("true");
		});
	});
}

exports.supplierResendClaimEmail = function(req, res) {
	var ceKey = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	var cookies = cookie.parse(req.headers.cookie || '');
  	var tz = cookies.tz;

	const supplier = {
  		SUPPLIER: {
  		  supplier_id: req.body.supplierId,
  		  claim_email_key: ceKey,
  		  claim_email_sent_at: Common.newDate(tz)
  		}
  	}

  	Supplier.update(supplier, function(errUpdate, supplierUpdated) {
    	fs.readFile(__dirname + '/../public/emails/sup_new_manager_email.html', function (err, html) {
		    if (!err) {
		        var msg = ""+html;

		    	msg = msg.replace("{{ req.session.supplier.email }}", req.session.supplier.email);
		    	msg = msg.replace("{{ req.session.supplier.name }}", req.session.supplier.name);

		    	while(msg.indexOf("{{ claimUrl }}") > -1) {
					msg = msg.replace("{{ claimUrl }}", "app.appyhourmobile.com/estlogin?email="+req.body.email+"&ceKey="+ceKey);
				}

		    	var transporter = nodemailer.createTransport({
			      service: 'Gmail',
			      auth: {
			        user: 'appy-support@appyhourmobile.com',
			        pass: 'gpzkftethvbstvfn'
			      }
			    });

			    var mailOptions = {
			      from: 'AppyHour <appy-support@appyhourmobile.com>',
			      to: req.body.email,
			      subject: 'Manager Login Instructions',
			      html: msg
			    };

			    transporter.sendMail(mailOptions, function(error, info){
			      if (error) {
			        console.log(error);
			        res.send("Sorry, we're having some issues sending your message. Please try again later.");
			      } else {
			        console.log('Email sent: ' + info.response);
			        res.send("true");
			      }
			    });
			}
		});
	});
}





// Common functions

function intDayToString(day) {
	if(day == -1)
		return "Everyday";
	else if(day == 0)
		return "Sunday";
	else if(day == 1)
		return "Monday";
	else if(day == 2)
		return "Tuesday";
	else if(day == 3)
		return "Wednesday";
	else if(day == 4)
		return "Thursday";
	else if(day == 5)
		return "Friday";
	else if(day == 6)
		return "Saturday";
	else
		return "Everyday";
}

function dayStrToString(days) {
	var str = "";

	try {
		if(days.indexOf(",") > -1) {
			var temp = days.split(",");
			for(var t = 0; t < temp.length; t++) {
				if(t < temp.length - 1) 
					str += intDayToString(parseInt(temp[t])) + ", ";
				else 
					str += intDayToString(parseInt(temp[t]));
			}
		}
		else {
			str = intDayToString(parseInt(days));
		}
	}
	catch(e) {
		// eat it
	}

	return str;
}

function dayOrDateToString(day, start, end, tz) {
	var now = Common.newDate(tz);
	var n = now.getDay();

	if(day == -1) {
	    var tmw = Common.newDate(tz);
	    tmw.setDate(now.getDate()+1);

	    if (start < now) {
	        return "";
	        //return null;
	    }
	    else if(start.getDate() == now.getDate() && start.getMonth() == now.getMonth() && start.getFullYear() == now.getFullYear()) {
	        return "Today";
	        //return null;
	    }
	    else {
	        return timestampToDayMonDate(start);
	    }		
	}
	else if(day == n) {
		return "Today";
		//return null;
	}
	else if(day == 0) {
		return "Sunday";
	}
	else if(day == 1) {
		return "Monday";
	}
	else if(day == 2) {
		return "Tuesday";
	}
	else if(day == 3) {
		return "Wednesday";
	}
	else if(day == 4) {
		return "Thursday";
	}
	else if(day == 5) {
		return "Friday";
	}
	else if(day == 6) {
		return "Saturday";
	}
	else {
		return "";
		//return null;
	}
}

function timestampToDayMonDate(timestamp) {
    var d = new Date(timestamp);
    var day = intDayToStringAbbr(d.getDay());
    var month = intMonthToStringAbbr(d.getMonth());
    var date = d.getDate();
    return day + ", " + month + " " + date;
}

function intDayToStringAbbr(day) {
	var weekday = new Array(7);
	weekday[0] = "Sun";
	weekday[1] = "Mon";
	weekday[2] = "Tue";
	weekday[3] = "Wed";
	weekday[4] = "Thu";
	weekday[5] = "Fri";
	weekday[6] = "Sat";
	return weekday[day];
}

function intMonthToStringAbbr(month) {
	var m = new Array(7);
	m[0] = "Jan";
	m[1] = "Feb";
	m[2] = "Mar";
	m[3] = "Apr";
	m[4] = "May";
	m[5] = "Jun";
	m[6] = "Jul";
	m[7] = "Aug";
	m[8] = "Sep";
	m[9] = "Oct";
	m[10] = "Nov";
	m[11] = "Dec";
	return m[month];
}

function dtToStr(dt) {
	var m = dt.getMonth() + 1;
	var d = dt.getDate();
	var y = dt.getFullYear();
	var hr = dt.getHours();
	var min = dt.getMinutes();
	var ampm = "am";

	if(m < 10)
		m = "0"+m;

	if(d < 10)
		d = "0"+d;

	if(hr > 12) {
		hr -= 12;
		ampm = "pm";
	}
	else if(hr == 0) {
		hr = 12;
	}

	if(min < 10)
		min = "0"+min;

	return m+"/"+d+"/"+y+" "+hr+":"+min+ampm;
}

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
   return year + "-" + month + "-" + day;
}

function getSuperscript(day) {
  if(day == 1 || day == 21 || day == 31)
    return "st";
  else if(day == 2 || day == 22)
    return "nd";
  else if(day == 3 || day == 23)
    return "rd";
  else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
    return "th";
  else 
    return "";
}

function formatPhone(phone) {
  if(phone === undefined || phone == null || phone == "")
    return "";

  return "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6, 10);
}

function formatStartEndTime(datetime, tzSpecial, tzUser) {
	var timezones = {};
	timezones["PST"] = 0;
	timezones["PDT"] = 1;
	timezones["MST"] = 1;
	timezones["MDT"] = 2;
	timezones["CST"] = 2;
	timezones["CDT"] = 3;
	timezones["EST"] = 3;
	timezones["EDT"] = 4;

	var tzDiff = timezones[tzSpecial] - timezones[tzUser];
	var h = datetime.getHours() - tzDiff;
	var m = datetime.getMinutes();
	var ampm = "AM";

	if(h > 24) {
		h -= 24;
	}
	else if(h == 24) {
		h = 12;
	}
	else if(h > 12) {
		h -= 12;
		ampm = "PM";
	}
	else if(h == 12) {
		ampm = "PM";
	}
	else if(h == 0) {
		h = 12;
	}
	else if(h < 0) {
		h += 12;
		ampm = "PM";
	}

		
	if(m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function getTodaysDayInt(tz) {
	var t = Common.newDate(tz);
	
	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	return t.getDay();
}

function dateDiff(a, tzSpecial, tzUser) {
	var today = Common.newDate(tzSpecial);
  	var dealDate = new Date(a);

  	if (dealDate < today) {
    	dealDate.setDate(dealDate.getDate()+1);
    	a = dealDate.getTime();
  	}

	var diffMs = (a - today); // milliseconds between now & a
	var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
	var diffMins = Math.ceil(((diffMs % 86400000) % 3600000) / 60000); // minutes

	if (diffMins == 60) {
		diffHrs++;
		diffMins = 0;
	}
	
	if (diffHrs == 0) {
		return(diffMins + "m");
	} else {
		return(diffHrs + "h " + diffMins + "m");
	}
}

function sortByStartTimeThenEndTime(specials, tz) {
  	var sorted = false;
  	var n = getTodaysDayInt(tz);

  	while(!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var startA = new Date(specials[i-1].start_time);
    		var startB = new Date(specials[i].start_time);
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if(specials[i].day >= n)
				specials[i].newDay = specials[i].day - n;
			else
				specials[i].newDay = specials[i].day + (7 - n);

			if(specials[i-1].day >= n)
				specials[i-1].newDay = specials[i-1].day - n;
			else
				specials[i-1].newDay = specials[i-1].day + (7 - n);

    		if(specials[i-1].newDay > specials[i].newDay || (specials[i-1].newDay == specials[i].newDay && (startA > startB || (startA.getTime() === startB.getTime() && endA > endB)))) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}
  	}

  	return specials;
}

function sortByEndTime(specials) {
	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if (endA > endB) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}

		if(sorted)
		    break;
  	}

  	return specials;
}

Date.prototype.stdTimezoneOffset = function () {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.isDstObserved = function () {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}




