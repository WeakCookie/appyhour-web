var User = require('../models/user.js');
var Establishment = require('../models/establishment.js');
var Address = require('../models/address.js');
var Hours = require('../models/hours.js');
var Ref = require('../models/ref.js');
var Image = require('../models/image.js');
var NodeGeocoder = require('node-geocoder');
var Common = require('../models/common.js');
var Geocoder = NodeGeocoder({ provider: 'google', apiKey: 'AIzaSyA8ObhYNLNSbaaHrF1B7dC1a10t9iF50II' });
var fs = require('fs');
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./awsconfig.json');

var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL

exports.show = function(req, res) {
	var establishment = req.session.establishment;

	Address.getAllByAddressId(establishment.address_id, function(err, address) {
		req.session.address = address;
		Hours.getAllByHoursId(establishment.hours_id, function(err, hours) {
			req.session.hours = hours;

			var allStates = [];
			Ref.getAllByRefTypeId(24, function(err2, states) {
				for(var s in states) {
					allStates.push(states[s].ref_code);
				}

				var needsToPay = false;
				var cardOnFile = false;
				var cancelDate;
				var dateCanceled;

				if(req.session.user.needs_to_pay == 1) {
					needsToPay = true;

					// customer is in our db
					if(req.session.user.stripe_id) {
						stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
							stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
								// customer has card on file
								if(card) {
									cardOnFile = true;
								}

								// customer has a subscription
								if(customer.subscriptions.data.length > 0) {

									// customer's subscription will cancel
									if(customer.subscriptions.data[0].cancel_at_period_end == true) {
										dateCanceled = epochToDate(customer.subscriptions.data[0].canceled_at);
										cancelDate = epochToDate(customer.subscriptions.data[0].current_period_end);
									} 

									var custBillAmount = centsToDollars(customer.subscriptions.data[0].plan.amount);
									var custPlanPrice = custBillAmount + "/" + customer.subscriptions.data[0].plan.interval;
										
									res.render('info', {
										needsToPay: needsToPay,
										profile_pic: establishment.profile_pic,
										name: establishment.name,
										address_1: address.address_1,
										address_2: address.address_2,
										city: address.city,
										state: address.state,
										state_code_ref_id: address.state_code_ref_id,
										allStates: allStates,
										zip: address.zip,
										phone_num: establishment.phone_num,
										phone_num_formatted: formatPhone(establishment.phone_num),
										website: establishment.website,
										resWebsite: establishment.resWebsite,
										menu_link: establishment.menu_link,
										//instagram: establishment.instagram,
										//facebook: establishment.facebook,
										//twitter: establishment.twitter,
										//snapchat: establishment.snapchat,
										mon_open: formatHours(hours.mon_open),
										mon_close: formatHours(hours.mon_close),
										tue_open: formatHours(hours.tue_open),
										tue_close: formatHours(hours.tue_close),
										wed_open: formatHours(hours.wed_open),
										wed_close: formatHours(hours.wed_close),
										thu_open: formatHours(hours.thu_open),
										thu_close: formatHours(hours.thu_close),
										fri_open: formatHours(hours.fri_open),
										fri_close: formatHours(hours.fri_close),
										sat_open: formatHours(hours.sat_open),
										sat_close: formatHours(hours.sat_close),
										sun_open: formatHours(hours.sun_open),
										sun_close: formatHours(hours.sun_close),
										mon_hours: getHours(hours.mon_open, hours.mon_close),
										tue_hours: getHours(hours.tue_open, hours.tue_close),
										wed_hours: getHours(hours.wed_open, hours.wed_close),
										thu_hours: getHours(hours.thu_open, hours.thu_close),
										fri_hours: getHours(hours.fri_open, hours.fri_close),
										sat_hours: getHours(hours.sat_open, hours.sat_close),
										sun_hours: getHours(hours.sun_open, hours.sun_close),
										description: establishment.description,
										delivery: (establishment.delivery == 1 ? "Yes" : "No"),
										takeout: (establishment.takeout == 1 ? "Yes" : "No"),
										patio: (establishment.patio == 1 ? "Yes" : "No"),
										rooftop: (establishment.rooftop == 1 ? "Yes" : "No"),
										brunch: (establishment.brunch == 1 ? "Yes" : "No"),
										dog: (establishment.dog_friendly == 1 ? "Yes" : "No"),
										cardOnFile: cardOnFile,
										custCardBrand: card.brand,
										custCardLastFour: card.last4,
										plan: customer.subscriptions.data[0].plan,
										custNextBillDate: epochToDate(customer.subscriptions.data[0].current_period_end),
										custBillAmount: custBillAmount,
										dateCanceled: dateCanceled,
										cancelDate: cancelDate,
										hasSubscription: true
									});
								} 
								// customer does not have a subscription
								else {
									res.render('info', {
										needsToPay: needsToPay,
										profile_pic: establishment.profile_pic,
										name: establishment.name,
										address_1: address.address_1,
										address_2: address.address_2,
										city: address.city,
										state: address.state,
										state_code_ref_id: address.state_code_ref_id,
										allStates: allStates,
										zip: address.zip,
										phone_num: establishment.phone_num,
										phone_num_formatted: formatPhone(establishment.phone_num),
										website: establishment.website,
										resWebsite: establishment.resWebsite,
										menu_link: establishment.menu_link,
										//instagram: establishment.instagram,
										//facebook: establishment.facebook,
										//twitter: establishment.twitter,
										//snapchat: establishment.snapchat,
										mon_open: formatHours(hours.mon_open),
										mon_close: formatHours(hours.mon_close),
										tue_open: formatHours(hours.tue_open),
										tue_close: formatHours(hours.tue_close),
										wed_open: formatHours(hours.wed_open),
										wed_close: formatHours(hours.wed_close),
										thu_open: formatHours(hours.thu_open),
										thu_close: formatHours(hours.thu_close),
										fri_open: formatHours(hours.fri_open),
										fri_close: formatHours(hours.fri_close),
										sat_open: formatHours(hours.sat_open),
										sat_close: formatHours(hours.sat_close),
										sun_open: formatHours(hours.sun_open),
										sun_close: formatHours(hours.sun_close),
										mon_hours: getHours(hours.mon_open, hours.mon_close),
										tue_hours: getHours(hours.tue_open, hours.tue_close),
										wed_hours: getHours(hours.wed_open, hours.wed_close),
										thu_hours: getHours(hours.thu_open, hours.thu_close),
										fri_hours: getHours(hours.fri_open, hours.fri_close),
										sat_hours: getHours(hours.sat_open, hours.sat_close),
										sun_hours: getHours(hours.sun_open, hours.sun_close),
										description: establishment.description,
										delivery: (establishment.delivery == 1 ? "Yes" : "No"),
										takeout: (establishment.takeout == 1 ? "Yes" : "No"),
										patio: (establishment.patio == 1 ? "Yes" : "No"),
										rooftop: (establishment.rooftop == 1 ? "Yes" : "No"),
										brunch: (establishment.brunch == 1 ? "Yes" : "No"),
										dog: (establishment.dog_friendly == 1 ? "Yes" : "No"),
										cardOnFile: cardOnFile,
										hasSubscription: false
									});
								}
							});
						});
					} 
					// customer is not in our db
					else {
						res.render('info', {
							needsToPay: needsToPay,
							profile_pic: establishment.profile_pic,
							name: establishment.name,
							address_1: address.address_1,
							address_2: address.address_2,
							city: address.city,
							state: address.state,
							state_code_ref_id: address.state_code_ref_id,
							allStates: allStates,
							zip: address.zip,
							phone_num: establishment.phone_num,
							phone_num_formatted: formatPhone(establishment.phone_num),
							website: establishment.website,
							resWebsite: establishment.resWebsite,
							menu_link: establishment.menu_link,
							//instagram: establishment.instagram,
							//facebook: establishment.facebook,
							//twitter: establishment.twitter,
							//snapchat: establishment.snapchat,
							mon_open: formatHours(hours.mon_open),
							mon_close: formatHours(hours.mon_close),
							tue_open: formatHours(hours.tue_open),
							tue_close: formatHours(hours.tue_close),
							wed_open: formatHours(hours.wed_open),
							wed_close: formatHours(hours.wed_close),
							thu_open: formatHours(hours.thu_open),
							thu_close: formatHours(hours.thu_close),
							fri_open: formatHours(hours.fri_open),
							fri_close: formatHours(hours.fri_close),
							sat_open: formatHours(hours.sat_open),
							sat_close: formatHours(hours.sat_close),
							sun_open: formatHours(hours.sun_open),
							sun_close: formatHours(hours.sun_close),
							mon_hours: getHours(hours.mon_open, hours.mon_close),
							tue_hours: getHours(hours.tue_open, hours.tue_close),
							wed_hours: getHours(hours.wed_open, hours.wed_close),
							thu_hours: getHours(hours.thu_open, hours.thu_close),
							fri_hours: getHours(hours.fri_open, hours.fri_close),
							sat_hours: getHours(hours.sat_open, hours.sat_close),
							sun_hours: getHours(hours.sun_open, hours.sun_close),
							description: establishment.description,
							delivery: (establishment.delivery == 1 ? "Yes" : "No"),
							takeout: (establishment.takeout == 1 ? "Yes" : "No"),
							patio: (establishment.patio == 1 ? "Yes" : "No"),
							rooftop: (establishment.rooftop == 1 ? "Yes" : "No"),
							brunch: (establishment.brunch == 1 ? "Yes" : "No"),
							dog: (establishment.dog_friendly == 1 ? "Yes" : "No")
						});
					}
				} else {
					res.render('info', {
						needsToPay: needsToPay,
						profile_pic: establishment.profile_pic,
						name: establishment.name,
						address_1: address.address_1,
						address_2: address.address_2,
						city: address.city,
						state: address.state,
						state_code_ref_id: address.state_code_ref_id,
						allStates: allStates,
						zip: address.zip,
						phone_num: establishment.phone_num,
						phone_num_formatted: formatPhone(establishment.phone_num),
						website: establishment.website,
						resWebsite: establishment.resWebsite,
						menu_link: establishment.menu_link,
						//instagram: establishment.instagram,
						//facebook: establishment.facebook,
						//twitter: establishment.twitter,
						//snapchat: establishment.snapchat,
						mon_open: formatHours(hours.mon_open),
						mon_close: formatHours(hours.mon_close),
						tue_open: formatHours(hours.tue_open),
						tue_close: formatHours(hours.tue_close),
						wed_open: formatHours(hours.wed_open),
						wed_close: formatHours(hours.wed_close),
						thu_open: formatHours(hours.thu_open),
						thu_close: formatHours(hours.thu_close),
						fri_open: formatHours(hours.fri_open),
						fri_close: formatHours(hours.fri_close),
						sat_open: formatHours(hours.sat_open),
						sat_close: formatHours(hours.sat_close),
						sun_open: formatHours(hours.sun_open),
						sun_close: formatHours(hours.sun_close),
						mon_hours: getHours(hours.mon_open, hours.mon_close),
						tue_hours: getHours(hours.tue_open, hours.tue_close),
						wed_hours: getHours(hours.wed_open, hours.wed_close),
						thu_hours: getHours(hours.thu_open, hours.thu_close),
						fri_hours: getHours(hours.fri_open, hours.fri_close),
						sat_hours: getHours(hours.sat_open, hours.sat_close),
						sun_hours: getHours(hours.sun_open, hours.sun_close),
						description: establishment.description,
						delivery: (establishment.delivery == 1 ? "Yes" : "No"),
						takeout: (establishment.takeout == 1 ? "Yes" : "No"),
						patio: (establishment.patio == 1 ? "Yes" : "No"),
						rooftop: (establishment.rooftop == 1 ? "Yes" : "No"),
						brunch: (establishment.brunch == 1 ? "Yes" : "No"),
						dog: (establishment.dog_friendly == 1 ? "Yes" : "No")
					});
				}
			});
		});
	});
};

exports.save = function(req, res) {
	var name = req.body.name;
	var address_1 = req.body.address_1;
	var address_2 = req.body.address_2;
	var city = req.body.city;
	var state = req.body.state;
	var zip = req.body.zip;
	var phone_num = req.body.phone_num.replace(/\D/g,'');
	var website = req.body.website;
	var resWebsite = req.body.resWebsite;
	var menu_link = req.body.menu_link;
	//var instagram = req.body.instagram;
	//var facebook = req.body.facebook;
	//var twitter = req.body.twitter;
	//var snapchat = req.body.snapchat;
	var mon_open = deFormatHours(req.body.mon_open);
	var mon_close = deFormatHours(req.body.mon_close);
	var tue_open = deFormatHours(req.body.tue_open);
	var tue_close = deFormatHours(req.body.tue_close);
	var wed_open = deFormatHours(req.body.wed_open);
	var wed_close = deFormatHours(req.body.wed_close);
	var thu_open = deFormatHours(req.body.thu_open);
	var thu_close = deFormatHours(req.body.thu_close);
	var fri_open = deFormatHours(req.body.fri_open);
	var fri_close = deFormatHours(req.body.fri_close);
	var sat_open = deFormatHours(req.body.sat_open);
	var sat_close = deFormatHours(req.body.sat_close);
	var sun_open = deFormatHours(req.body.sun_open);
	var sun_close = deFormatHours(req.body.sun_close);

	var description = req.body.description;
	var delivery = (req.body.delivery == "Yes" ? 1 : 0);
	var takeout = (req.body.takeout == "Yes" ? 1 : 0);
	var patio = (req.body.patio == "Yes" ? 1 : 0);
	var rooftop = (req.body.rooftop == "Yes" ? 1 : 0);
	var brunch = (req.body.brunch == "Yes" ? 1 : 0);
	var dog = (req.body.dog == "Yes" ? 1 : 0);
	
	Ref.getAllByRefCode(state, function(err, ref) {
		if (err || ref == undefined) {
  			//res.render('dashboard', {
	        //    error: "We're having some trouble locating the address you've entered. Please update the address fields and try again."
	        //});
			res.redirect('dashboard?page=info');
			return;
		}

		var state_code_ref_id = ref.ref_id;
		var address = address_1 + " " + address_2 + ", " + city + ", " + state;

	  	Geocoder.geocode(address, function(err, geo) {
	  		if (err || geo[0] == undefined) {
	  			//res.render('dashboard?page=info', {
		        //    error: "We're having some trouble locating the address you've entered. Please update the address fields and try again."
		        //});
	  			res.redirect('dashboard?page=info');
				return;
	  		}

	  		if (geo[0].city != city || geo[0].zipcode != zip) {
	  			//res.render('dashboard?page=info', {
		        //    error: "We're having some trouble locating the address you've entered. Please update the address fields and try again."
		        //});
	  			res.redirect('dashboard?page=info');
				return;
	  		}

	  		var latitude = geo[0].latitude;
	      	var longitude = geo[0].longitude;

	      	var topic = "";

	      	if(getDistanceFromLatLng(latitude, longitude, 44.9778, -93.2650) <= 30) // Minne/STP
	      		topic = "minneapolismn";
	      	else if(getDistanceFromLatLng(latitude, longitude, 41.6611, -91.5302) <= 30) // Iowa City
	      		topic = "iowacityia";
	      	else if(getDistanceFromLatLng(latitude, longitude, 41.5868, -93.6250) <= 30) // Des Moines
	      		topic = "desmoinesia";
	      	else if(getDistanceFromLatLng(latitude, longitude, 39.7392, -104.9903) <= 30) // Denver
	      		topic = "denverco";
	      	else if(getDistanceFromLatLng(latitude, longitude, 38.2527, -85.7585) <= 30) // Louisville
	      		topic = "louisvilleky";
	      	else if(getDistanceFromLatLng(latitude, longitude, 27.7634, -82.5437) <= 30) // Tampa
                topic = "tampabayfl";

	      	const establishment = {
				ESTABLISHMENT: {
			      establishment_id: req.session.establishment.establishment_id,
			      name: name,
			      phone_num: phone_num,
			      website: website,
			      resWebsite: resWebsite,
			      menu_link: menu_link,
			      description: description,
			      delivery: delivery,
			      takeout: takeout,
			      patio: patio,
			      rooftop: rooftop,
			      brunch: brunch,
			      dog_friendly: dog,
			      //instagram: instagram,
			      //facebook: facebook,
			      //twitter: twitter,
			      //snapchat: snapchat,
			      topic: topic
			    }
			};

			const address = {
				ADDRESS: {
			      address_id: req.session.address.address_id,
			      address_1: address_1,
			      address_2: address_2,
			      city: city,
			      state_code_ref_id: state_code_ref_id,
			      zip: zip,
			      latitude: latitude,
			      longitude: longitude
			    }
			};

			const hours = { 
			    HOURS: {
			      hours_id: req.session.hours.hours_id,
			      mon_open: mon_open,
			      mon_close: mon_close,
			      tue_open: tue_open,
			      tue_close: tue_close,
			      wed_open: wed_open,
			      wed_close: wed_close,
			      thu_open: thu_open,
			      thu_close: thu_close,
			      fri_open: fri_open,
			      fri_close: fri_close,
			      sat_open: sat_open,
			      sat_close: sat_close,
			      sun_open: sun_open,
			      sun_close: sun_close
			    }
		  	};

		  	var profile_pic_name = "";

			if (req.files && req.files.profilePic) {
				profPic = req.files.profilePic;
				//profile_pic_name = profPic.name;
				var ext = profPic.name.substring(profPic.name.indexOf("."));
				profile_pic_name = "estimg"+req.session.establishment.establishment_id+ext;

				var path = __dirname + '/../public/img/est-img/' + profPic.name;
				profPic.mv(path, function(err) {
				    if (err)
				      	return res.status(500).send(err);

				    // Create S3 service object
					s3 = new AWS.S3({apiVersion: '2006-03-01'});

					// Configure the file stream and obtain the upload parameters
					var fileStream = fs.createReadStream(path);
					fileStream.on('error', function(err) {
					  console.log('File Error', err);
					});

					// call S3 to retrieve upload file to specified bucket
					var uploadParams = {Bucket: 'est-img', Key: profile_pic_name, Body: fileStream, ACL: 'public-read'};

					// call S3 to retrieve upload file to specified bucket
					s3.upload(uploadParams, function (err, data) {
					  if (err) {
					    console.log("Error", err);
					  } if (data) {
					    console.log("Upload Success", data.Location);
					  }
					});
				});
			}

		  	Hours.update(hours, function(err, result) {
		  		if(result.affectedRows > 0) {
		  			Address.update(address, function(err, result) {
				  		if(result.affectedRows > 0) {
				  			Establishment.update(establishment, function(err, result) {
				  				if(result.affectedRows > 0) {
				  					if(profile_pic_name && profile_pic_name != "") {
										// save the new pic
					  					const image = {
									  		IMAGE: {
									  		  image_id: req.session.establishment.profile_pic_id,
									  		  image_location: profile_pic_name
									  		}
									  	}

					  					Image.update(image, function(err, result) {
					  						if(result.affectedRows > 0) {
					  							/*Establishment.getAllByOwnerId(req.session.user.user_id, function(err, ests) {
											     	if(ests) {
											        	var est = ests[0];
											          	est.profile_pic_id = req.session.establishment.profile_pic_id;
											          	est.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/"+profile_pic_name;
											          	if(est.topic == "denverco") {
												            req.session.tz = "MST";
												        }
												        else if(est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
												            req.session.tz = "CST";
												        }
												        else if(est.topic == "louisvilleky") {
												            req.session.tz = "EST";
          												}
											        	req.session.establishment = est;
											        }
											        res.redirect('dashboard?page=info');
											    });*/
											    req.session.establishment.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/"+profile_pic_name;
											    res.redirect('dashboard?page=info');
					  						}
					  					});
				  					}
				  					else {
				  						/*
				  						Establishment.getAllByOwnerId(req.session.user.user_id, function(err, ests) {
									     	if(ests) {
									        	Image.getProfilePicForEstablishmentId(ests[0].establishment_id, function(err, image) {
										          	var est = ests[0];
										          	est.profile_pic_id = image.image_id;
										          	est.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/"+image.image_location;
										          	req.session.establishment = est;
										          	if(est.topic == "denverco") {
											            req.session.tz = "MST";
											        }
											        else if(est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
											            req.session.tz = "CST";
											        }
											        else if(est.topic == "louisvilleky") {
											            req.session.tz = "EST";
      												}
										          	res.redirect('dashboard?page=info');
										        });
									        }
									    });
									    */
									    res.redirect('dashboard?page=info');
				  					}
							    } 
				  			});
				  		}
		  			});
		  		}
		  	});
	  	});
	});
};

exports.saveToken = function(req, res) {
	var trial = 0;
	if(req.query.trial != 'undefined') {
		trial = req.query.trial;
	}

	var estCount = 1;
	try {
		estCount = req.session.ests.length;
	}
	catch(e) {}

	// customer not saved to our db
	if(req.session.user.stripe_id == null) {
		stripe.customers.list({ email: req.session.user.email }, function(err, customers) {
			// stripe customer, but not in our db
	    	if(customers.data.length > 0) {
	    		stripe.customers.update(customers.data[0].id, {
					source: req.query.token,
				});

				stripe.subscriptions.create({
					customer: customers.data[0].id,
					items: [{ plan: req.query.planId, quantity: estCount }],
					trial_period_days: trial
				}, function(err, subscription) {
					console.log(err);
				 	console.log(subscription);
				});

				var user = {
		      		USER: {
		      		  user_id: req.session.user.user_id,
		      		  stripe_id: customers.data[0].id,
		      		  show_specials: 1
		      		}
		      	}

		      	User.update(user, function(err, result) {
		      		res.send(true);
		      	});
	    	}
	    	// not a stripe customer, and not in our db (first time adding payment) 
	    	else {
	    		stripe.customers.create({
					source: req.query.token,
					email: req.session.user.email
				}, function(err, customer) {
					stripe.subscriptions.create({
						customer: customer.id,
						items: [{ plan: req.query.planId, quantity: estCount }],
						trial_period_days: trial
					}, function(err, subscription) {
						console.log(err);
					 	console.log(subscription);
					});

					var user = {
			      		USER: {
			      		  user_id: req.session.user.user_id,
			      		  stripe_id: customer.id,
			      		  show_specials: 1
			      		}
			      	}

			      	User.update(user, function(err, result) {
			      		res.send(true);
		      		});
				});
	    	}
	  	});
	}
	// customer saved in db
	else {
		stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
			if(customer.subscriptions.data.length == 0) {
				stripe.subscriptions.create({
					customer: req.session.user.stripe_id,
					items: [{ plan: req.query.planId, quantity: estCount }],
					trial_period_days: trial
				}, function(err, subscription) {
					console.log(err);
					console.log(subscription);
					stripe.customers.update(req.session.user.stripe_id, {
						source: req.query.token,
					});
					var user = {
			      		USER: {
			      		  user_id: req.session.user.user_id,
			      		  show_specials: 1
			      		}
			      	}
			      	User.update(user, function(err, result) {
			      		res.send(true);
		      		});
				});
			}
			else {
				//var subStatus = customer.subscriptions.data[0].status;
				//if(subStatus == "trialing") {
					var subEnd = customer.subscriptions.data[0].current_period_end;
					var subEndDt = new Date(subEnd * 1000);

					var diffMs = Math.abs(subEndDt - Common.newDate(req.session.tz)); 
					var diffDays = Math.floor(diffMs / (1000 * 60 * 60 * 24)); 

					trial = diffDays;
				//}
				
				stripe.subscriptions.del(customer.subscriptions.data[0].id, function(errDel, deleted) {
					console.log(errDel);
					console.log(deleted);
					
					stripe.subscriptions.create({
						customer: req.session.user.stripe_id,
						items: [{ plan: req.query.planId, quantity: estCount }],
						trial_period_days: trial
					}, function(err, subscription) {
						console.log(err);
						console.log(subscription);
						stripe.customers.update(req.session.user.stripe_id, {
							source: req.query.token,
						});
						var user = {
				      		USER: {
				      		  user_id: req.session.user.user_id,
				      		  show_specials: 1
				      		}
				      	}
				      	User.update(user, function(errUser, result) {
				      		res.send(true);
			      		});
					});	
				});
			}
		});
	}
};

exports.cancelSubscription = function(req, res) {
	stripe.subscriptions.list({ customer: req.session.user.stripe_id }, function(err, subs) {
		stripe.subscriptions.update(subs.data[0].id, { cancel_at_period_end: true });
		res.send(true);
	});
};

exports.reactivateSubscription = function(req, res) {
	stripe.subscriptions.list({ customer: req.session.user.stripe_id }, function(err, subs) {
		stripe.subscriptions.update(subs.data[0].id, { cancel_at_period_end: false });
		res.send(true);
	});
};

function formatPhone(phone) {
	if(phone.length == 10)
		return '(' + phone.slice(0,3) + ') ' + phone.slice(3,6) + '-' + phone.slice(6);
	else if(phone.length == 11)
		return phone.slice(0,1) + '(' + phone.slice(1,4) + ') ' + phone.slice(4,7) + '-' + phone.slice(7);
	else
		return phone;
}

function formatHours(hours) {
	if(hours.length == 4)
		return hours.slice(0,2) + ':' + hours.slice(2);
	else
		return hours;
}

function deFormatHours(hours) {
	if(hours)
		return hours.replace(':','');
	else
		return '';
}

function getHours(open, close) {
	if(open == '') 
		return 'Closed';

	var hours = '';
	var openHr = parseInt(open.slice(0,2));
	var openMin = parseInt(open.slice(2));
	var closeHr = parseInt(close.slice(0,2));
	var closeMin = parseInt(close.slice(2));

	if(openMin < 10)
		openMin = '0' + openMin;
	if(closeMin < 10)
		closeMin = '0' + closeMin;
	
	if (openHr > 12)
		hours = (openHr - 12) + ':' + openMin + ' PM';
	else if (openHr == 12)
		hours = openHr + ':' + openMin + ' PM';
	else if (openHr == 0)
		hours = '12:' + openMin + ' AM';
	else
		hours = openHr + ':' + openMin + ' AM';

	if (closeHr > 12)
		hours = hours + ' - ' + (closeHr - 12) + ':' + closeMin + ' PM';
	else if (closeHr == 12)
		hours = hours + ' - ' + closeHr + ':' + closeMin + ' PM';
	else if (closeHr == 0)
		hours = hours + ' - ' + '12:' + closeMin + ' AM';
	else
		hours = hours + ' - ' + closeHr + ':' + closeMin + ' AM';

	return hours;
}

function getDistanceFromLatLng(userLat, userLon, lat, lon) {
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }

    if(userLat && userLon && lat && lon) {
        var lat2 = userLat;
        var lon2 = userLon;
        var lat1 = parseFloat(lat);
        var lon1 = parseFloat(lon);

        var R = 3959; // Radius in miles has a problem with the .toRad() method below.
        var x1 = lat2 - lat1;
        var dLat = x1.toRad();
        var x2 = lon2 - lon1;
        var dLon = x2.toRad();
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;

        d = Math.round(d * 100)/100;

        return d;
    } else {
        return "";
    }
}

function epochToDate(epoch) {
	var d = new Date(0);
	d.setUTCSeconds(epoch);

  	var monthNames = [
    	"January", "February", "March",
    	"April", "May", "June", "July",
    	"August", "September", "October",
    	"November", "December"
  	];

  	var day = d.getDate();
  	var monthIndex = d.getMonth();
  	var year = d.getFullYear();

  	return monthNames[monthIndex] + ' ' + day + getDaySuperscript(day) + ', ' + year;
}

function getDaySuperscript(day) {
	if(day == 1 || day == 21 || day == 31)
		return "st";
	else if(day == 2 || day == 22)
		return "nd";
	else if(day == 3 || day == 23)
		return "rd";
	else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
		return "th";
	else 
		return "";
}

function centsToDollars(cents) {
	var dollars = cents / 100;
	return dollars.toLocaleString("en-US", {style:"currency", currency:"USD"});
}