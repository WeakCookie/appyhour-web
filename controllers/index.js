var User = require('../models/user.js');
var Supplier = require('../models/supplier.js');
var Ref = require('../models/ref.js');
var Common = require('../models/common.js');

var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');

exports.show = function(req, res) {
  res.render('index');
};

exports.login = function(req, res) {
  User.getAllByUsername(req.body.username, function(err, user) {
    if(user) {
      bcrypt.compare(req.body.password, user.password, function(err, match) {
        if(match) {
          req.user = user;
          req.session.user = user;
          res.redirect('dashboard?page=home');
        } else {
          res.render('index', {
            message: "password incorrect"
          });
        } 
      });
    } else {
      res.render('index', {
        message: "user not found"
      });
    }
  });
}

exports.updateUser = function(req, res) {
  bcrypt.hash(req.body.newPassword, 10, function(err, hash) {
    if(!err) {
      console.log(req.body);
      User.getAllByUsername(req.body.username, function(err, u) {
        const user = { 
          USER: {
            user_id: u.user_id, 
            password: hash, 
            email: req.body.email,
            claimed_at: Common.newDate(req.session.tz),
            claim_email_key: null
          }
        };
        User.update(user, function(err, result) {
          req.user = u;
          req.session.user = u;
          res.redirect('dashboard?page=home');
        });
      });
    } else {
      res.render('index', {
        message: "error updating user"
      });
    }
  });
}

exports.updateSupplier = function(req, res) {
  bcrypt.hash(req.body.newPassword, 10, function(err, hash) {
    if(!err) {
      Supplier.getAllByUsername(req.body.username, function(err, s) {
        const supplier = { 
          SUPPLIER: {
            supplier_id: s.supplier_id, 
            password: hash, 
            email: req.body.email,
            is_active: 1,
            claimed_at: new Date(),
            claim_email_key: null
          }
        };
        Supplier.update(supplier, function(err, result) {
          Supplier.getAllBySupplierId(s.supplier_id, function(err2, sup) {
            // owner
            if(sup.user_type_ref_id != 306) {
              sup.password = null;
              sup.phone_num_formatted = formatPhone(sup.phone_num);
              req.session.supplier = sup;
              res.redirect('supplierdashboard');
            }
            // manager
            else {
              Supplier.getOwner(sup.supplier_id, function(errOwner, owner) {
                sup.name = owner.name;
                sup.profile_pic = owner.profile_pic;
                sup.password = null;
                sup.phone_num_formatted = formatPhone(sup.phone_num);
                req.session.supplier = sup;
                res.redirect('supplierdashboard');
              });
            }
          });
        });
      });
    } else {
      console.log(err);
      res.render('index', {
        message: "error updating user"
      });
    }
  });
}

// AJAX
exports.contactUs = function(req, res) {
  var first = req.query.first;
  var last = req.query.last;
  var email = req.query.email;
  var phone = req.query.phone;
  var bizName = req.query.bizName;
  var message = req.query.message;

  if(email == undefined || email == "" || email == "undefined")
    res.send("Please enter a valid email address");
  else if(message == undefined || message == "" || message == "undefined")
    res.send("Please enter a valid message");

  var msg = "";

  if(first != undefined && first != "" && first != "undefined")
    msg = msg + "First Name: " + first + "<br/>";
  
  if(last != undefined && last != "" && last != "undefined") 
    msg = msg + "Last Name: " + last + "<br/>";

  msg = msg + "Email: " + email + "<br/>";

  if(phone != undefined && phone != "" && phone != "undefined")
    msg = msg + "Phone: " + phone + "<br/>";

  if(bizName != undefined && bizName != "" && bizName != "undefined")
    msg = msg + "Business: " + bizName + "<br/>";

  msg = msg + "Message: " + message;

  if(email != undefined && email != "" && email != "undefined" && 
    message != undefined && message != "" && message != "undefined") {

    var transporter = nodemailer.createTransport({
      service: 'Gmail',
      auth: {
        user: 'appy-support@appyhourmobile.com',
        pass: 'gpzkftethvbstvfn'
      }
    });

    var mailOptions = {
      from: 'appy-support@appyhourmobile.com',
      to: 'appy-support@appyhourmobile.com',
      subject: 'Contact Us - ' + email,
      html: msg
    };

    transporter.sendMail(mailOptions, function(error, info){
      if (error) {
        console.log(error);
        res.send("Sorry, we're having some issues sending your message. Please try again later.");
      } else {
        console.log('Email sent: ' + info.response);
        res.send("");
      }
    });
  }
}

exports.userExists = function(req, res) {
  User.getAllByUsername(req.query.username, function(err, user) {
    if(user && user.username == req.query.username) {
      res.send(true);
    }
    else {
      res.send(false);
    }
  });
}

exports.userHasChangedPassword = function(req, res) {
  User.getAllByUsername(req.query.username, function(err, user) {
    if(user) {
      var updatedEmail = true;

      if(user.email) {
        //console.log("EMAIL: " + user.email);
        if(user.email.indexOf("@email.com") > -1) {
          //console.log("has @email.com - need to change pw");
          updatedEmail = false;
          res.send(false);
        }
        else {
          //console.log("email updated - continue");
        }
      }
      else {
        //console.log("has no email - need to change pw");
        updatedEmail = false;
        res.send(false);
      }

      if(updatedEmail) {
        bcrypt.compare("AHgenPW$", user.password, function(err, match) {
          if(match) {
            console.log("pw is AHgenPW$ - need to change pw");
            res.send(false);
          }
          else {
            console.log("pw is not AHgenPW$ - pw has been changed");
            res.send(true);
          }
        });
      }
    }
    else {
      console.log("could not find user");
      res.send(false);
    }
  });
}

exports.secretPasswordMatches = function(req, res) {
  Ref.getAllByRefId(1000, function(err, pw) {
    var secretpw = pw.ref_code;
    if(req.query.secretpw == secretpw)
      res.send(true);
    else
      res.send(false);
  });
}

exports.passwordMatches = function(req, res) {
  User.getAllByUsername(req.query.username, function(err, user) {
    if(user) {
      bcrypt.compare(req.query.password, user.password, function(err, match) {
        if(match) {
          res.send(true);
        }
        else {
          res.send(false);
        }
      });
    }
  });
}

exports.getEmailForForgotPassword = function(req, res) {
  User.getAllByUsername(req.query.username, function(err, user) {
    if(user) {
      res.send(user.email);
    }
    else {
      res.send("");
    }
  });
}

exports.forgotPassword = function(req, res) {
  var msg = req.query.resetPasswordEmail;
  var fpKey = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
  console.log(fpKey);
  while(msg.indexOf("{{ resetPwUrl }}") > -1) {
    msg = msg.replace("{{ resetPwUrl }}", "https://app.appyhourmobile.com/resetpassword?email="+req.query.email+"&fpKey="+fpKey);
  }
  
  var transporter = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
      user: 'appy-support@appyhourmobile.com',
      pass: 'gpzkftethvbstvfn'
    }
  });

  var mailOptions = {
    from: 'AppyHour <appy-support@appyhourmobile.com>',
    to: req.query.email,
    subject: 'Reset Password',
    html: msg
  };

  transporter.sendMail(mailOptions, function(error, info){
    if (error) {
      console.log(error);
      res.send("Sorry, we're having some issues sending your message. Please try again later.");
    } else {
      console.log('Email sent: ' + info.response);
      User.getAllByEmail(req.query.email, function(errUser, user) {
        if(!errUser && user) {
          const u = { 
            USER: {
              user_id: user.user_id, 
              forgot_password: fpKey
            }
          };
          User.update(u, function(errUpdateUser, updatedUser) {
            if(!errUpdateUser && updatedUser) {
              res.send("true");
            }
            else {
              res.send("Sorry, we're having some issues sending your message. Please try again later.");
            }
          });
        }
      });
    }
  });
}


// Supplier login

exports.supplierExists = function(req, res) {
  Supplier.getAllByUsername(req.query.username, function(err, supplier) {
    if(supplier && supplier.username == req.query.username) {
      res.send(true);
    }
    else {
      res.send(false);
    }
  });
}

exports.supplierHasChangedPassword = function(req, res) {
  Supplier.getAllByUsername(req.query.username, function(err, supplier) {
    if(supplier) {
      var updatedEmail = true;

      if(supplier.email) {
        //console.log("EMAIL: " + user.email);
        if(supplier.email.indexOf("@email.com") > -1) {
          //console.log("has @email.com - need to change pw");
          updatedEmail = false;
          res.send(false);
        }
        else {
          //console.log("email updated - continue");
        }
      }
      else {
        //console.log("has no email - need to change pw");
        updatedEmail = false;
        res.send(false);
      }

      if(updatedEmail) {
        bcrypt.compare("AHgenPW$", supplier.password, function(err, match) {
          if(match) {
            //console.log("pw is AH generated - need to change pw");
            res.send(false);
          }
          else {
            //console.log("pw is not AH generated - pw has been changed");
            res.send(true);
          }
        });
      }
    }
    else {
      //console.log("could not find user");
      res.send(false);
    }
  });
}

exports.supplierLogin = function(req, res) {
  Supplier.getAllByUsername(req.body.username, function(err, supplier) {
    if(supplier) {
      bcrypt.compare(req.body.password, supplier.password, function(err1, match) {
        if(match) {
          // owner
          if(supplier.user_type_ref_id != 306) {
            supplier.password = null;
            supplier.phone_num_formatted = formatPhone(supplier.phone_num);
            req.session.supplier = supplier;
            res.redirect('supplierdashboard');
          }
          // manager
          else {
            Supplier.getOwner(supplier.supplier_id, function(errOwner, owner) {
              supplier.name = owner.name;
              supplier.profile_pic = owner.profile_pic;
              supplier.password = null;
              supplier.phone_num_formatted = formatPhone(supplier.phone_num);
              req.session.supplier = supplier;
              res.redirect('supplierdashboard');
            });
          }
        } else {
          res.render('index', {
            message: "password incorrect"
          });
        } 
      });
    } else {
      res.render('index', {
        message: "user not found"
      });
    }
  });
}

exports.supplierPasswordMatches = function(req, res) {
  Supplier.getAllByUsername(req.query.username, function(err, supplier) {
    if(supplier) {
      bcrypt.compare(req.query.password, supplier.password, function(err, match) {
        if(match) {
          res.send(true);
        }
        else {
          res.send(false);
        }
      });
    }
  });
}


function formatPhone(phone) {
  if(phone === undefined || phone == null || phone == "")
    return "";

  return "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6, 10);
}





/* GET USER ****
  User.getAllByUserId(1, function(err, user) {
    res.render('index', {
      user: user.username
    });
  });
*/


/* GET ALL USERS ****
  User.getAll(function(err, users) {
    res.render('index', {
      user: users[0].username
    });
  }); 
*/


/* INSERT USER ****
  const user = { 
    USER: {
      user_type_ref_id: 1, 
      username: "jake", 
      password: "pwjake", 
      email: "jake123@test.com", 
      phone_num: "1231231234", 
      is_active: 1
    }
  };

  User.insert(user, function(err, result) {
    res.render('index', {
      rows: result.affectedRows
    });
  });
*/


/* UPDATE USER ****
  const user = { 
    USER: {
      user_id: 1,
      user_type_ref_id: 1, 
      username: "jake", 
      password: "pwjake", 
      email: "jake123@test.com", 
      phone_num: "1231231234", 
      is_active: 1
    }
  };

  User.update(user, function(err, result) {
    res.render('index', {
      rows: result.affectedRows
    });
  }); 
*/


/* DELETE USER ****
  User.delete(2, function(err, result) {
    res.render('index', {
      rows: result.affectedRows
    });
  });
*/