var Establishment = require('../models/establishment.js');
var Special = require('../models/special.js');
var Event = require('../models/event.js');
var Common = require('../models/common.js');

var upcomingSoeStartDate;
var upcomingSoeEndDate;
var upcomingSoeShowSpecials = true;
var upcomingSoeShowEvents = true;
var upcomingSoeSearch = "";

exports.show = function(req, res) {
	var establishment = req.session.establishment;
	Special.getAllByEstablishmentId(establishment.establishment_id, function(err, specials) {
		Event.getAllByEstablishmentIdGroupRecurring(req.session.establishment.establishment_id, function(err, events) {
			var eventIds = "(0)";

			if(events.length > 0) {
				eventIds = "(";
				for(var i in events) {
					eventIds = eventIds + events[i].event_id;
					if(i < events.length-1) {
						eventIds = eventIds + ",";
					}
					else if(i == events.length-1) {
						eventIds = eventIds + ")";
					}
				}
			}

			Special.getTodaysEventsByEventIds(eventIds, function(err, eSpecials) {
				upcomingSoeStartDate = Common.newDate(req.session.tz);
				upcomingSoeStartDate.setHours(0,0,0,0);
				upcomingSoeEndDate = Common.newDate(req.session.tz);
				upcomingSoeEndDate.setHours(23,59,59);
				upcomingSoeEndDate.setDate(upcomingSoeEndDate.getDate()+7);

				res.render('dash', {
					todaysDate: getTodaysDate(req.session.tz),
					day: getTodaysDay(req.session.tz),
					todaysSpecials: setupTodaysSpecialsTable(specials, req.session.tz),
					todaysEvents: setupTodaysEventsTable(events, eSpecials, req.session.tz),
					upcomingSoe: setupUpcomingSoeTable(specials, events, req.session.tz)
					//dailySoe: setupDailySoeTable(specials, events),
				});
			});
		});
	});
}

exports.upcomingSoeFilter = function(req, res) {
	if(req.query.startDate != undefined) {
		upcomingSoeStartDate = new Date(req.query.startDate);
		upcomingSoeStartDate.setHours(0,0,0,0);
	}
	if(req.query.endDate != undefined) {
		upcomingSoeEndDate = new Date(req.query.endDate);
		upcomingSoeEndDate.setHours(23,59,59);
	}
	if(req.query.showSpecials != undefined) {
		upcomingSoeShowSpecials = (req.query.showSpecials == 'true');
	}
	if(req.query.showEvents != undefined) {
		upcomingSoeShowEvents = (req.query.showEvents == 'true');
	}
	if(req.query.search != undefined) {
		upcomingSoeSearch = req.query.search;
	}

	Special.getAllByEstablishmentId(req.session.establishment.establishment_id, function(err, specials) {
		Event.getAllByEstablishmentIdGroupRecurring(req.session.establishment.establishment_id, function(err, events) {
			res.json(setupUpcomingSoeTable(specials, events));
		});
	});
}

function setupTodaysSpecialsTable(specials, tz) {
	var specialsResponse = [];
	var now = Common.newDate(tz);
	var n = getTodaysDayInt(tz);

	// we need today at 4am, so that we can see if exclusive special was created today
	var todayAt4am = Common.newDate(tz);
	if (todayAt4am.getHours() < 4) {
		todayAt4am.setDate(todayAt4am.getDate()-1)
	}
	todayAt4am.setHours(4);
	todayAt4am.setMinutes(0);
	todayAt4am.setSeconds(0);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);
  	
  	var expired = [];
  	var current = [];
  	var upcoming = [];

  	if(specials != undefined && specials != null) {
  		for(var i in specials) {
  			s = specials[i];
  			if(s.day == -1 || s.day == n) {
				s.deal_type_ref_id_TS = getDealTypeImageByRefId(s.deal_type_ref_id);

  				if(s.day == n) {
  					if(s.start_time.getHours() <= 4) {
  						s.start_time.setMonth(tmw.getMonth());
  						s.start_time.setDate(tmw.getDate());
  						s.start_time.setYear(1900+tmw.getYear());
  					}
  					else {
  						s.start_time.setMonth(now.getMonth());
  						s.start_time.setDate(now.getDate());
  						s.start_time.setYear(1900+now.getYear());
  					}

  					if(s.end_time.getHours() <= 4) {
  						s.end_time.setMonth(tmw.getMonth());
  						s.end_time.setDate(tmw.getDate());
  						s.end_time.setYear(1900+tmw.getYear());
  					}
  					else {
  						s.end_time.setMonth(now.getMonth());
  						s.end_time.setDate(now.getDate());
  						s.end_time.setYear(1900+now.getYear());
  					}

  					s.reoccurringStr = getReoccurringStr(s.day.toString());
  				}
  				
				s.duration = formatStartEndTime(s.start_time) + " - " + formatStartEndTime(s.end_time);

  				// expired 
  				if(s.start_time > todayAt4am && s.end_time < now) {
  					s.expired = true;
  					expired.push(s);
  				}
  				// current 
  				else if(s.start_time < now && s.end_time > now) {
  					s.timeremaining = dateDiff(s.end_time, tz);
  					s.current = true;
  					current.push(s);
  				}
  				// upcoming
  				else if(s.day == n && s.start_time > now) {
  					s.upcoming = true;
  					upcoming.push(s);
  				}
  			}
		}

		expired = sortByStartTimeThenEndTime(expired);
		current = sortByEndTime(current);
		upcoming = sortByStartTimeThenEndTime(upcoming);

		for (e in expired)
			specialsResponse.push(expired[e]);

		for (c in current)
			specialsResponse.push(current[c]);

		for (u in upcoming)
			specialsResponse.push(upcoming[u]);
  	}

  	return specialsResponse;
}

function setupTodaysEventsTable(events, specials, tz) {
	var eventsResponse = [];
	var n = getTodaysDayInt(tz);
	var now = Common.newDate(tz);
	if(now.getHours() < 4)
		now.setDate(now.getDate()+1);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);

	var expired = [];
  	var current = [];
  	var upcoming = [];

	for(var i in events) {
		var e = events[i];
		e.specials = [];

		if(isToday(e.days, e.start_time, tz)) {
			if(isToday(e.days, null, tz)) {
				if(e.start_time.getHours() <= 4) {
					e.start_time.setMonth(tmw.getMonth());
					e.start_time.setDate(tmw.getDate());
					e.start_time.setYear(1900+tmw.getYear());
				}
				else {
					e.start_time.setMonth(now.getMonth());
					e.start_time.setDate(now.getDate());
					e.start_time.setYear(1900+now.getYear());
				}

				if(e.end_time.getHours() <= 4) {
					e.end_time.setMonth(tmw.getMonth());
					e.end_time.setDate(tmw.getDate());
					e.end_time.setYear(1900+tmw.getYear());
				}
				else {
					e.end_time.setMonth(now.getMonth());
					e.end_time.setDate(now.getDate());
					e.end_time.setYear(1900+now.getYear());
				}

				e.reoccurringStr = getReoccurringStr(e.days);

				e.editEventDayMon = false;
				e.editEventDayTue = false;
				e.editEventDayWed = false;
				e.editEventDayThu = false;
				e.editEventDayFri = false;
				e.editEventDaySat = false;
				e.editEventDaySun = false;

				var days = e.days.split(",");
				for(var x = 0; x < 7; x++) {
					for(var d in days) {
						if(days[d] == x && x == 0) {
							e.editEventDaySun = true;
						}
						else if(days[d] == x && x == 1) {
							e.editEventDayMon = true;
						}
						else if(days[d] == x && x == 2) {
							e.editEventDayTue = true;
						}
						else if(days[d] == x && x == 3) {
							e.editEventDayWed = true;
						}
						else if(days[d] == x && x == 4) {
							e.editEventDayThu = true;
						}
						else if(days[d] == x && x == 5) {
							e.editEventDayFri = true;
						}
						else if(days[d] == x && x == 6) {
							e.editEventDaySat = true;
						}
					}
				}
			}

			//e.eventDate = getEventDate(e.start_time);
			e.duration = formatStartEndTime(e.start_time) + " - " + formatStartEndTime(e.end_time);

			e.start = getISOString(e.start_time);
			e.end = getISOString(e.end_time);

			e.startDate = e.start.substring(0, e.start.indexOf("T"));
			e.startTime = e.start.substring(e.start.indexOf("T")+1);
			e.endTime = e.end.substring(e.end.indexOf("T")+1);

			for(var s in specials) {
				var special = specials[s];
				if(special.event_id == e.event_id) {
					special.deal_type_ref_id_TS = getDealTypeImageByRefId(special.deal_type_ref_id);
					e.specials.push(special);
				}
			}

			// expired 
			if(e.end_time < now) {
				e.expired = true;
				e.current = false;
				expired.push(e);
			}
			// current 
			else if(e.start_time < now && e.end_time > now) {
				e.timeremaining = dateDiff(e.end_time);
				e.current = true;
				current.push(e);
			}
			// upcoming
			else if(e.start_time > now) {
				e.upcoming = true;
				e.current = false;
				upcoming.push(e);
			}
		}
	}

	expired = sortByStartTimeThenEndTime(expired);
	current = sortByEndTime(current);
	upcoming = sortByStartTimeThenEndTime(upcoming);

	for (ex in expired)
		eventsResponse.push(expired[ex]);

	for (c in current)
		eventsResponse.push(current[c]);

	for (u in upcoming)
		eventsResponse.push(upcoming[u]);

	return eventsResponse;
}

function setupDailySoeTable(specials, events) {
	// Each list in specialsResponse corresponds to a day (item 0 in list is Sunday, 6 is Saturday)
	var specialsResponse = [[], [], [], [], [], [], []];

  	if(specials != undefined && specials != null) {
  		for(var i in specials) {
  			if(specials[i].day != -1) {
  				if(specials[i].day > 0)
  					specials[i].day = specials[i].day - 1;
  				else
  					specials[i].day = 6;

	  			specials[i].start_time_DS = formatStartEndTime(specials[i].start_time);
				specials[i].end_time_DS = formatStartEndTime(specials[i].end_time);
				specials[i].deal_type_ref_id_DS = getDealTypeImageByRefId(specials[i].deal_type_ref_id);
				specialsResponse[specials[i].day].push(specials[i]);
			}
  		}
  	}
  	
  	specialsResponse[0] = sortByStartTimeThenEndTime(specialsResponse[0]);
  	specialsResponse[1] = sortByStartTimeThenEndTime(specialsResponse[1]);
  	specialsResponse[2] = sortByStartTimeThenEndTime(specialsResponse[2]);
  	specialsResponse[3] = sortByStartTimeThenEndTime(specialsResponse[3]);
  	specialsResponse[4] = sortByStartTimeThenEndTime(specialsResponse[4]);
  	specialsResponse[5] = sortByStartTimeThenEndTime(specialsResponse[5]);
  	specialsResponse[6] = sortByStartTimeThenEndTime(specialsResponse[6]);

  	var noSpecials = new Object();

  	for(var x = 0; x <= 6; x++) {
  		if(specialsResponse[x].length == 0) {
  			noSpecials = new Object();
	  		noSpecials.day = x;
	  		noSpecials.empty = true;
	  		specialsResponse[x].push(noSpecials);
	  	}
  	}
  	
	return specialsResponse;
}

function setupUpcomingSoeTable(specials, events, tz) {
	var specialsResponse = [];
	var now = Common.newDate(tz);
	var n = getTodaysDayInt(tz);
  	
  	var current = [];
  	var upcoming = [];

  	if(upcomingSoeShowSpecials) {
		for(var i in specials) {
			var s = specials[i];
			s.deal_type_ref_id_TS = getDealTypeImageByRefId(s.deal_type_ref_id);

			if(s.day > -1) {
				var soeDate = Common.newDate(tz);
				var addDays = s.day - n;
				if(addDays < 0) {
					addDays = 7 + addDays;
				}
				soeDate.setDate(soeDate.getDate()+addDays);
				s.soeDate = (soeDate.getMonth()+1) + "/" + soeDate.getDate();
				var soeTmw = new Date(soeDate.getDate());
				soeTmw.setDate(soeTmw.getDate()+1);

				if(s.start_time.getHours() <= 4) {
					s.start_time.setMonth(soeTmw.getMonth());
					s.start_time.setDate(soeTmw.getDate());
					s.start_time.setYear(1900+soeTmw.getYear());
				}
				else {
					s.start_time.setMonth(soeDate.getMonth());
					s.start_time.setDate(soeDate.getDate());
					s.start_time.setYear(1900+soeDate.getYear());
				}

				if(s.end_time.getHours() <= 4) {
					s.end_time.setMonth(soeTmw.getMonth());
					s.end_time.setDate(soeTmw.getDate());
					s.end_time.setYear(1900+soeTmw.getYear());
				}
				else {
					s.end_time.setMonth(soeDate.getMonth());
					s.end_time.setDate(soeDate.getDate());
					s.end_time.setYear(1900+soeDate.getYear());
				}

				s.reoccurringStr = getReoccurringStr(s.day.toString());
			}
			if(s.start_time >= upcomingSoeStartDate && s.start_time <= upcomingSoeEndDate) {
				// current 
				if(s.start_time < now && s.end_time > now) {
					s.timeremaining = dateDiff(s.end_time, tz);
					s.current = true;
					current.push(s);
				}
				// upcoming
				else if(s.start_time > now) {
					s.duration = formatStartEndTime(s.start_time) + " - " + formatStartEndTime(s.end_time);
					s.upcoming = true;
					upcoming.push(s);
				}
			}
		}
	}

	if(upcomingSoeShowEvents) {
		for(var j in events) {
			var e = JSON.parse(JSON.stringify(events[j]));

			var eDays = e.days.split(",");

			if(parseInt(eDays[0]) > -1) {
				for(var x in eDays) {
					var ev = JSON.parse(JSON.stringify(e));
					// dates are converted to string with stringify
					ev.start_time = new Date(ev.start_time); 
					ev.end_time = new Date(ev.end_time);

					var eventDay = parseInt(eDays[x]);

					var soeDate = Common.newDate(tz);
					var addDays = eventDay - n;
					if(addDays < 0) {
						addDays = 7 + addDays;
					}
					soeDate.setDate(soeDate.getDate()+addDays);
					ev.soeDate = (soeDate.getMonth()+1) + "/" + soeDate.getDate();
					var soeTmw = new Date(soeDate.getDate());
					soeTmw.setDate(soeTmw.getDate()+1);

					if(ev.start_time.getHours() <= 4) {
						ev.start_time.setMonth(soeTmw.getMonth());
						ev.start_time.setDate(soeTmw.getDate());
						ev.start_time.setYear(1900+soeTmw.getYear());
					}
					else {
						ev.start_time.setMonth(soeDate.getMonth());
						ev.start_time.setDate(soeDate.getDate());
						ev.start_time.setYear(1900+soeDate.getYear());
					}

					if(ev.end_time.getHours() <= 4) {
						ev.end_time.setMonth(soeTmw.getMonth());
						ev.end_time.setDate(soeTmw.getDate());
						ev.end_time.setYear(1900+soeTmw.getYear());
					}
					else {
						ev.end_time.setMonth(soeDate.getMonth());
						ev.end_time.setDate(soeDate.getDate());
						ev.end_time.setYear(1900+soeDate.getYear());
					}

					ev.reoccurringStr = getReoccurringStr(ev.days);

					if(ev.start_time >= upcomingSoeStartDate && ev.start_time <= upcomingSoeEndDate) {
						// current 
						if(ev.start_time < now && ev.end_time > now) {
							ev.timeremaining = dateDiff(ev.end_time, tz);
							ev.duration = null;
							ev.current = true;
							current.push(ev);
						}
						// upcoming
						else if(ev.start_time > now) {
							ev.duration = formatStartEndTime(ev.start_time) + " - " + formatStartEndTime(ev.end_time);
							ev.timeremaining = null;
							ev.upcoming = true;
							upcoming.push(ev);
						}
					}
				}
			}
			else {
				if(e.start_time > upcomingSoeStartDate && e.start_time < upcomingSoeEndDate) {
					// current 
					if(e.start_time < now && e.end_time > now) {
						e.timeremaining = dateDiff(e.end_time, tz);
						e.current = true;
						current.push(e);
					}
					// upcoming
					else if(e.start_time > now) {
						e.duration = formatStartEndTime(e.start_time) + " - " + formatStartEndTime(e.end_time);
						e.upcoming = true;
						upcoming.push(e);
					}
				}
			}
		}
	}

	current = sortByEndTime(current);
	upcoming = sortByStartTimeThenEndTime(upcoming);

	for (c in current) {
		if(inSearch(current[c])) {
			specialsResponse.push(current[c]);
		}
	}

	for (u in upcoming) {
		if(inSearch(upcoming[u])) {
			specialsResponse.push(upcoming[u]);
		}
	}

  	return specialsResponse;
}

function sortByStartTimeThenEndTime(specials) {
  	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var startA = new Date(specials[i-1].start_time);
    		var startB = new Date(specials[i].start_time);
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if (startA > startB || (startA.getTime() === startB.getTime() && endA > endB)) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}
		
		if(sorted)
		    break;
  	}

  	return specials;
}

function sortByEndTime(specials) {
	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if (endA > endB) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}
		
		if(sorted)
		    break;
  	}

  	return specials;
}

function inSearch(soe) {
	if(upcomingSoeSearch.trim() != "") {
		if(soe.details != undefined && soe.details.toLowerCase().indexOf(upcomingSoeSearch.toLowerCase()) > -1) {
			return true;
		}
		if(soe.title != undefined && soe.title.toLowerCase().indexOf(upcomingSoeSearch.toLowerCase()) > -1) {
			return true;
		}
	}
	else {
		return true;
	}

	return false;
}


// Specials

function dateDiff(a, tz) {
	var today = Common.newDate(tz);
  	var dealDate = new Date(a);

  	if (dealDate < today) {
    	dealDate.setDate(dealDate.getDate()+1);
    	a = dealDate.getTime();
  	}

	var diffMs = (a - today); // milliseconds between now & a
	var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
	var diffMins = Math.ceil(((diffMs % 86400000) % 3600000) / 60000); // minutes

	if (diffMins == 60) {
		diffHrs++;
		diffMins = 00;
	}
	
	if (diffHrs == 0) {
		return(diffMins + "m");
	} else {
		return(diffHrs + "h " + diffMins + "m");
	}
}

function formatStartEndTime(datetime) {
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}
		
	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function getDealTypeImageByRefId(refId) {
	switch(refId) {
		case 200: return 'beer-icon.png';
		case 202: return 'shot-icon.png';
    	case 204: return 'wine-icon.png';
    	case 206: return 'mixed-drink-icon.png';
    	case 208: return 'margarita-icon.png';
    	case 210: return 'martini-icon.png';
    	case 212: return 'tumbler-icon.png';
    	case 214: return 'beerbottle.png';
    	case 216: return 'beercan.png';
		case 251: return 'burger-icon.png';
		case 253: return 'appetizer-icon.png';
		case 255: return 'pizza-icon.png';
    	case 257: return 'taco-icon.png';
    	case 259: return 'sushi.png';
    	case 261: return 'bowl.png';
    	case 263: return 'chickenwing.png';
		default: return '';
	}
}

function getISOString(dt) {
	var current_date = dt.getDate();
	var current_month = dt.getMonth() + 1;
	var current_year = dt.getFullYear();
	var current_hrs = dt.getHours();
	var current_mins = dt.getMinutes();
	var current_secs = dt.getSeconds();
	var current_datetime;

	// Add 0 before date, month, hrs, mins or secs if they are less than 0
	current_date = current_date < 10 ? '0' + current_date : current_date;
	current_month = current_month < 10 ? '0' + current_month : current_month;
	current_hrs = current_hrs < 10 ? '0' + current_hrs : current_hrs;
	current_mins = current_mins < 10 ? '0' + current_mins : current_mins;
	current_secs = current_secs < 10 ? '0' + current_secs : current_secs;

	// Current datetime
	// String such as 2016-07-16T19:20:30
	current_datetime = current_year + '-' + current_month + '-' + current_date + 'T' + current_hrs + ':' + current_mins + ':' + current_secs;
	return current_datetime;
}


// Events

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
    return year + "-" + month + "-" + day;
}

function isToday(days, d, tz) {
	var n = getTodaysDayInt(tz);

	var dayArr = days.split(",");
	for(var i in dayArr) {
		if(dayArr[i] == n) {
			return true;
		}
	}

	if(d) {
		var t = Common.newDate(tz);
		if(t.getHours() < 4)
			t.setDate(t.getDate()-1);
		return t.getDate() == d.getDate() && t.getMonth() == d.getMonth() && t.getFullYear() == d.getFullYear();
	}

	return false;
}

function getReoccurringStr(days) {
	var str = "";
	var dayArr = days.split(",");

	//sort day array 1, 2, 3, 4, 5, 6, 0
	if(dayArr.length > 1) {
		var sorted = false;
		while(!sorted) {
			sorted = true;
			for(var d = 0; d < dayArr.length - 1; d++) {
				var d1 = parseInt(dayArr[d+1]);
				var d2 = parseInt(dayArr[d]);
				if((d1 < d2 && d1 != 0) || d2 == 0) {
					var temp = dayArr[d];
					dayArr[d] = dayArr[d+1];
					dayArr[d+1] = temp;
					sorted = false;
				}
			}
		}
	}

	if(dayArr.length == 1) {
		str = str + intDayToString(dayArr[0]);
	}
	else if(dayArr.length == 2) {
		str = str + intDayToString(dayArr[0]) + " and " + intDayToString(dayArr[1]);
	}
	else {
		for(var i in dayArr) {
			if(i < dayArr.length - 2) {
				str = str + intDayToString(dayArr[i]) + ", ";
			}
			else if(i < dayArr.length - 1) {
				str = str + intDayToString(dayArr[i]) + ", and ";
			}
			else {
				str = str + intDayToString(dayArr[i]);
			}
		}
	}

	return str;
}


// Common functions

function getTodaysDate(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	var month = intMonthToString(t.getMonth());
	var day = t.getDate();
	var daySuperscript = getDaySuperscript(day);
	var year = 1900+t.getYear();
	return month + " " + day + daySuperscript + ", " + year;
}

function getTodaysDay(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	var dayOfWeek = intDayToString(t.getDay());
	
	return dayOfWeek;
}

function getTodaysDayInt(tz) {
	var t = Common.newDate(tz);
	
	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	return t.getDay();
}

function intMonthToString(month) {
	var m = new Array(7);
	m[0] = "January";
	m[1] = "February";
	m[2] = "March";
	m[3] = "April";
	m[4] = "May";
	m[5] = "June";
	m[6] = "July";
	m[7] = "August";
	m[8] = "September";
	m[9] = "October";
	m[10] = "November";
	m[11] = "December";
	return m[month];
}

function intDayToString(day) {
	var weekday = new Array(7);
	weekday[0] = "Sunday";
	weekday[1] = "Monday";
	weekday[2] = "Tuesday";
	weekday[3] = "Wednesday";
	weekday[4] = "Thursday";
	weekday[5] = "Friday";
	weekday[6] = "Saturday";
	return weekday[day];
}

function getDaySuperscript(day) {
  if(day == 1 || day == 21 || day == 31)
    return "st";
  else if(day == 2 || day == 22)
    return "nd";
  else if(day == 3 || day == 23)
    return "rd";
  else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
    return "th";
  else 
    return "";
}