const { subDays, format } = require('date-fns')
const get = require('lodash/get')
const isEmpty = require('lodash/isEmpty')

const MixPanel = require('../../../services/MixPanel')
const Establishment = require('../../../models/establishment.js')
const Special = require('../../../models/special.js')
const SpecialArchive = require('../../../models/special_archive.js')
const Event = require('../../../models/event.js')
const EventArchive = require('../../../models/event_archive.js')
const AnalyticsCache = require('../../../models/analytics_cache_v2')
const Favorites = require('../../../models/favorites.js')
const { Errors } = require('../../../constants/cache')
const { DB_DATE_FORMAT } = require('../../../constants/date')
const { secondsDifferent } = require('../../../utils')
const {
    getParamsBasedOnInterval,
    calculatePercentageChangeForFavorites,
    getSpecialWeeklyImpressions,
    getEventWeeklyImpressions,
    getAverageMapping,
    mapAverageSincePublishedToImpressions
} = require('../../../utils/analytics')

exports.getChartData = async function (req, res) {
    const { timezone, establishmentId, daysAgo } = req.body
    const existingEstablishment = await Establishment.getByIdAsync(establishmentId)
    if (!existingEstablishment) {
        res.status(403).end('Establishment not found')
    }
    const mixPanelParams = getParamsBasedOnInterval(daysAgo)
    const yearAgo = format(subDays(new Date(), 365), DB_DATE_FORMAT)
    const params = [establishmentId, yearAgo]

    try {

        let [
            specials,
            specialsArchives,
            events,
            eventsArchives,
            favorites
        ] = await Promise.all([
            Special.getAllByEstablishmentIdForYearAsync(params),
            SpecialArchive.getAllByEstablishmentIdForYearAsync([...params, yearAgo]),
            Event.getAllByEstablishmentIdForYearAsync(params),
            EventArchive.getAllByEstablishmentIdForYearAsync(params),
            Favorites.getAllByEstablishmentIdAsync(establishmentId) // TODO : only get favorites based on daysAgo
        ])
        specials = [...specials, ...specialsArchives]
        events = [...events, ...eventsArchives]

        const needRefetchAverageMapping = get(req, 'needRefetch.averageMapping') || !get(req, 'cache.averageMapping')
        const [
            specialImpressionData,
            eventImpressionsData,
            profileViewsData,
        ] = await Promise.all([
            MixPanel.getViews(specials, 'special', { ...mixPanelParams }, daysAgo),
            MixPanel.getViews(events, 'event', mixPanelParams, daysAgo),
            MixPanel.getViews(["Est loaded " + establishmentId], 'profile_view', mixPanelParams, daysAgo),
        ])

        const specialWeeklyImpressions = getSpecialWeeklyImpressions(specialImpressionData.originalData, specials, timezone, daysAgo)

        const eventWeeklyImpressions = getEventWeeklyImpressions(eventImpressionsData.originalData, events, timezone, daysAgo)

        const favoritesData = calculatePercentageChangeForFavorites(favorites, timezone, daysAgo)

        const fetchedAverageMapping = needRefetchAverageMapping && await getAverageMapping(existingEstablishment, { special: specials, event: events })
        const averageMapping = needRefetchAverageMapping ? fetchedAverageMapping : req.cache.averageMapping

        if (req.isMissCache) {
            const cacheData = {
                establishmentId,
                timezone,
                averageMapping,
                vendorData: {
                    special: specialImpressionData.originalData,
                    event: eventImpressionsData.originalData,
                    profileView: profileViewsData.originalData
                },
                weeklyImpressions: {
                    special: specialWeeklyImpressions,
                    event: eventWeeklyImpressions
                }
            }
            AnalyticsCache.upsert(cacheData, (error) => console.log(error))
        }

        res.json({
            special: {
                chartData: specialImpressionData.details,
                totalViews: specialImpressionData.totalViews,
                changes: specialImpressionData.changes,
            },
            event: {
                chartData: eventImpressionsData.details,
                totalViews: eventImpressionsData.totalViews,
                changes: eventImpressionsData.changes,
            },
            profileView: {
                chartData: profileViewsData.details,
                totalViews: profileViewsData.totalViews,
                changes: profileViewsData.changes,
            },
            favorite: favoritesData,
            weeklyImpressions: {
                special: mapAverageSincePublishedToImpressions(averageMapping.special, specialWeeklyImpressions),
                event: mapAverageSincePublishedToImpressions(averageMapping.event, eventWeeklyImpressions)
            }
        })

    } catch (error) {
        res.status(500).end(error.message)
    }
}

exports.cacheMiddleware = async function (req, res, next) {
    const { establishmentId, timezone, daysAgo } = req.body
    const CACHE_TIMEOUT = 24 * 60 * 60 // INFO: invalidate cache after 1 day
    const AVG_MAPPING_CACHE_TIMEOUT = 7 * 24 * 60 * 60 // INFO: invalidate cache after 1 day
    const needRefetch = {}
    let cachedData

    try {
        const cache = await AnalyticsCache.getAllByEstablishmentIdAsync([establishmentId, timezone])
        const favorites = await Favorites.getAllByEstablishmentIdAsync(establishmentId) // TODO : only get favorites based on daysAgo
        cachedData = cache
        const { vendorData, updatedAt, weeklyImpressions, averageMapping } = cache

        if (isEmpty(vendorData)) {
            needRefetch.vendorData = true
        }
        if (isEmpty(weeklyImpressions)) {
            needRefetch.weeklyImpressions = true
        }
        if (isEmpty(averageMapping) || secondsDifferent(averageMapping.updatedAt) >= AVG_MAPPING_CACHE_TIMEOUT) {
            needRefetch.averageMapping = true
        }
        if (!isEmpty(needRefetch)) {
            throw new Error()
        }
        if (secondsDifferent(updatedAt) >= CACHE_TIMEOUT) {
            throw new Error(Errors.TIME_OUT)
        }

        console.log('cache hit')
        return res.json({
            special: MixPanel.extractInformationFromValues(get(vendorData, 'special'), daysAgo),
            event: MixPanel.extractInformationFromValues(get(vendorData, 'event'), daysAgo),
            profileView: MixPanel.extractInformationFromValues(get(vendorData, 'profileView'), daysAgo),
            favorite: calculatePercentageChangeForFavorites(favorites, timezone, daysAgo),
            weeklyImpressions: {
                special: mapAverageSincePublishedToImpressions(averageMapping.special, weeklyImpressions.special),
                event: mapAverageSincePublishedToImpressions(averageMapping.event, weeklyImpressions.event)
            }
        })

    } catch (error) {
        console.log('cache miss')
        req.isMissCache = true
        req.needRefetch = needRefetch
        req.cache = cachedData
        return next()
    }
}