const Supplier = require('../../../models/supplier')
const bcrypt = require('bcrypt');

exports.updateSupplier = function(req, res) {
  bcrypt.hash(req.body.password, 10, function(err, hash) {
    if(!err) {
      Supplier.getAllByUsername(req.body.username, function(err, supplier) {
        const newSupplier = { 
          SUPPLIER: {
            supplier_id: supplier.supplier_id, 
            password: hash, 
            email: req.body.email,
            is_active: 1,
            claimed_at: new Date(),
            claim_email_key: null
          }
        };
        Supplier.update(newSupplier, function(err, result) {
          Supplier.getAllBySupplierId(supplier.supplier_id, function(err2, result) {
            // owner
            if(result.user_type_ref_id != 306) {
              result.password = null;
              result.phone_num_formatted = formatPhone(result.phone_num);
              req.session.supplier = result;
              res.send(true);
            }
            // manager
            else {
              Supplier.getOwner(result.supplier_id, function(errOwner, owner) {
                result.name = owner.name;
                result.profile_pic = owner.profile_pic;
                result.password = null;
                result.phone_num_formatted = formatPhone(result.phone_num);
                req.session.supplier = result;
                res.send(true);
              });
            }
          });
        });
      });
    } else {
      console.log(err);
      res.send({
        message: "error updating supplier"
      });
    }
  });
}

function formatPhone(phone) {
  if(!phone) {
    return "";
  }
  return "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6, 10);
}