const User = require('../../../models/user');
const bcrypt = require('bcrypt');
const Common = require('../../../models/common.js');

exports.updateUser = function(req, res) {
  bcrypt.hash(req.body.password, 10, function(err, hash) {
    if(!err) {
      console.log(req.body);
      User.getAllByUsername(req.body.username, function(err, user) {
        const newUser = { 
          USER: {
            user_id: user.user_id, 
            password: hash, 
            email: req.body.email,
            claimed_at: Common.newDate(req.session.tz),
            claim_email_key: null
          }
        };
        User.update(newUser, function(err, result) {
          req.user = user;
          req.session.user = user;
          res.send(true);
        });
      });
    } else {
      res.send({
        message: "error updating user"
      });
    }
  });
}
