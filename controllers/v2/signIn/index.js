const User = require('../../../models/user');
const Establishment = require('../../../models/establishment');
const Ref = require('../../../models/ref.js');
const Supplier = require('../../../models/supplier')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken')
const set = require('lodash/set')

const DEFAULT_SECRET_PASSWORD = 'AHgenPW$'

exports.login = function (req, res) {
  User.getAllByUsername(req.body.username, function (err, user) {
    if (user) {
      bcrypt.compare(req.body.password, user.password, function (err, match) {
        if (match) {
          delete user.password
          let token = null
          Establishment.getAllByOwnerId(user.user_id, function (err, est) {
            if (Array.isArray(est)) {
              token = generateAccessToken(user.username)
              set(user, 'ests', est.map(e => e.establishment_id))
              res.json({ token, ests: user.ests })
            }})
        } else {
          res.send({
            message: "password incorrect",
            error: err
          });
        }
      });
    } else {
      res.send({
        message: "user not found",
        error: err
      });
    }
  });
}

exports.userExists = function (req, res) {
  User.getAllByUsername(req.body.username, function (err, user) {
    if (user && user.username == req.body.username) {
      res.send(true);
    }
    else {
      res.send(false);
    }
  });
}

// Supplier login

exports.supplierExists = function (req, res) {
  Supplier.getAllByUsername(req.body.username, function (err, supplier) {
    if (supplier && supplier.username == req.body.username) {
      res.send(true);
    }
    else {
      res.send(false);
    }
  });
}

exports.supplierLogin = function (req, res) {
  Supplier.getAllByUsername(req.body.username, function (err, supplier) {
    if (supplier) {
      bcrypt.compare(req.body.password, supplier.password, function (err1, match) {
        if (match) {
          const token = generateAccessToken(supplier)
          res.json({ token })
        } else {
          res.render('index', {
            message: "password incorrect"
          });
        }
      });
    } else {
      res.render('index', {
        message: "user not found"
      });
    }
  });
}

exports.userHasChangedPassword = function (req, res) {
  User.getAllByUsername(req.body.username, function (err, user) {
    if (user && user.email && !user.email.includes("@email")) {
      bcrypt.compare(DEFAULT_SECRET_PASSWORD, user.password, function (err, match) {
        if (match) {
          console.log("pw is DEFAULT_SECRET_PASSWORD - need to change pw");
          res.send(false);
        }
        else {
          console.log("pw is not DEFAULT_SECRET_PASSWORD - pw has been changed");
          res.send(true);
        }
      });
    } else {
      res.send(false);
    }
  });
}

exports.supplierHasChangedPassword = function(req, res) {
  Supplier.getAllByUsername(req.body.username, function (err, supplier) {
    if (supplier && supplier.email && !supplier.email.includes("@email")) {
      bcrypt.compare(DEFAULT_SECRET_PASSWORD, supplier.password, function (err, match) {
        if (match) {
          console.log("pw is DEFAULT_SECRET_PASSWORD - need to change pw");
          res.send(false);
        }
        else {
          console.log("pw is not DEFAULT_SECRET_PASSWORD - pw has been changed");
          res.send(false);
        }
      });
    } else {
      res.send(false);
    }
  });
}

exports.secretPasswordMatches = function(req, res) {
  Ref.getAllByRefId(1000, function(err, pw) {
    var secretPassword = pw.ref_code;
    if(req.body.secretPassword == secretPassword)
      res.send(true);
    else
      res.send(true);
  });
}

function generateAccessToken(date) {
  return jwt.sign({ user: JSON.stringify(date) }, process.env.TOKEN_SECRET, { expiresIn: '86400s' });
}
