var Address = require('../models/address.js');
var Common = require('../models/common.js');
var Establishment = require('../models/establishment.js');
var EstablishmentImage = require('../models/establishment_image.js');
var EstablishmentSpecial = require('../models/establishment_special.js');
var EstablishmentEvent = require('../models/establishment_event.js');
var EstablishmentQueue = require('../models/establishment_queue.js');
var EstablishmentQueueImage = require('../models/establishment_queue_image.js');
var Hours = require('../models/hours.js');
var Image = require('../models/image.js');
var Ref = require('../models/ref.js');
var Special = require('../models/special.js');
var SpecialArchive = require('../models/special_archive.js');
var Event = require('../models/event.js');
var EventArchive = require('../models/event_archive.js');
var EventSpecial = require('../models/event_special.js');
var User = require('../models/user.js');
var UserEstablishment = require('../models/user_establishment.js');
var UserQueue = require('../models/user_queue.js');
var UserEstablishmentQueue = require('../models/user_establishment_queue.js');
var Supplier = require('../models/supplier.js');
var SupplierPromo = require('../models/supplier_promo.js');
var SupplierArchive = require('../models/supplier_archive.js');
var Promo = require('../models/promo.js');
var Location = require('../models/location.js');
var PromoPriority = require('../models/promo_priority.js');

var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var NodeGeocoder = require('node-geocoder');
var Geocoder = NodeGeocoder({ provider: 'google', apiKey: 'AIzaSyA8ObhYNLNSbaaHrF1B7dC1a10t9iF50II' });
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
var fs = require('fs');
//var parser = require('xml2json');
var MixpanelExport = require('mixpanel-data-export');
var panel = new MixpanelExport({
	api_key: "4f98c92e0ecf95d97d76d6eca2cb0298",
	api_secret: "3e9796ed74b2cdda13edc3bbe904f073"
});
var AWS = require('aws-sdk');
AWS.config.loadFromPath('./awsconfig.json');

var establishments = [];
var owners = [];
var allPromos = [];
var currentPromos = [];
var promoLocations = [];
var promoAnalytics = [];
var chosenLocation = -1;

var allSuppliers = [];

exports.show = function(req, res) {
	if(req.session.authenticated === undefined)
		req.session.authenticated = false;

	if(req.session.authenticated) {
		//resetSelectedValues();

		User.getOwners(function(err, owns) {
			owners = owns;

			owners.sort(OwnerComparator);

			Establishment.getAll(function(err, ests) {
				establishments = ests;
				res.render('admin', {
					showLogin: !req.session.authenticated,
					adminUsername: req.session.adminUsername,
					owners: owners,
					ests: ests
				});
			});
		});
	}
	else {
		res.render('admin', {
	  		showLogin: !req.session.authenticated,
			adminUsername: req.session.adminUsername,
	  		owners: [],
	  		ests: []
		});
	}
};

exports.auth = function(req, res) {
	/* KEEP: use this to create a new password
	var pass = "XXX";
	bcrypt.hash(pass, 10, function(err, hash) {
		console.log(hash);
	});
	*/

	fs.readFile( './auth.xml', 'utf8', function(err, pw) {
    	bcrypt.compare(req.body.adminPW, pw, function(err, match) {
			if(match) {
				req.session.authenticated = true;
				req.session.adminUsername = req.body.adminUsername;
				//resetSelectedValues();

				User.getOwners(function(err, owns) {
					owners = owns

					owners.sort(OwnerComparator);

					Establishment.getAll(function(err, ests) {
						establishments = ests;
						res.render('admin', {
							adminUsername: req.body.adminUsername,
							showLogin: !req.session.authenticated,
							owners: owners,
							ests: ests
						});
					});
				});
			}
			else {
				res.render('admin', {
					showLogin: !req.session.authenticated,
					status: "wrong password"
				});
			}
		});
 	});
};

exports.adminLogout = function(req, res) {
	req.session.authenticated = false;
	req.session.adminUsername = undefined;

	res.render('admin', {
		showLogin: !req.session.authenticated,
		status: "successfully logged out"
	});
}



/* IMPORT ESTABLISHMENTS */

exports.showImportEsts = function(req, res) {
	//var city = "";
	//if(req.query && req.query.city)
	//	city = req.query.city;

	var zip = "";
	if(req.query && req.query.zip)
		zip = req.query.zip;

	var allStates = [];
	Ref.getAllByRefTypeId(24, function(err2, states) {
		for(var s in states) {
			allStates.push(states[s].ref_code);
		}

		Establishment.getAll(function(errE, ahEstsRes) {
			var ahEsts = [];
			for(var ae in ahEstsRes) {
				//if(ahEstsRes[ae].topic == city) {
					//if(allEsts.filter(e => e.name === ahEstsRes[ae].name && e.ah_status == "Added to AH").length > 0) {
						//console.log(ahEstsRes[ae]);
					//	ahEstsRes[ae].isFromEstQueue = true;
					//}

					//ahEsts.push(ahEstsRes[ae]);
				//}
				//if(ahEstsRes[ae].zip == zip) {
				//if(ahEstsRes[ae].zip.substring(0, 2) == "80") {
					ahEsts.push(ahEstsRes[ae]);
				//}
			}

			ahEsts.sort((a, b) => a.name > b.name ? 1 : -1);

			User.getOwners(function(err, owns) {
				UserQueue.getOwners(function(errUQ, ownsUQ) {
					owners = [...owns, ...ownsUQ];

					owners.sort(OwnerComparator);

					if(zip != "") {
						EstablishmentQueue.getAll(function(errEQ, allEsts) {
							var ests = [];
							for(var e in allEsts) {
								//if(allEsts[e].topic == city) {
								//if(allEsts[e].zip.substring(0, 2) == "80") {
								//if(allEsts[e].ah_status == 'Reviewed') {
								if(allEsts[e].zip == zip) {
									if(allEsts[e].imageUrl != null)
										allEsts[e].profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/" + allEsts[e].imageUrl;
									else
										allEsts[e].profile_pic = "/img/est-img/establishment_no_image.png";

									allEsts[e].formatted_phone_num = formatPhone(allEsts[e].phone_num);

									allEsts[e].mon_open_formatted = timeStrToTime(allEsts[e].mon_open);
									allEsts[e].mon_close_formatted = timeStrToTime(allEsts[e].mon_close);
									allEsts[e].tue_open_formatted = timeStrToTime(allEsts[e].tue_open);
									allEsts[e].tue_close_formatted = timeStrToTime(allEsts[e].tue_close);
									allEsts[e].wed_open_formatted = timeStrToTime(allEsts[e].wed_open);
									allEsts[e].wed_close_formatted = timeStrToTime(allEsts[e].wed_close);
									allEsts[e].thu_open_formatted = timeStrToTime(allEsts[e].thu_open);
									allEsts[e].thu_close_formatted = timeStrToTime(allEsts[e].thu_close);
									allEsts[e].fri_open_formatted = timeStrToTime(allEsts[e].fri_open);
									allEsts[e].fri_close_formatted = timeStrToTime(allEsts[e].fri_close);
									allEsts[e].sat_open_formatted = timeStrToTime(allEsts[e].sat_open);
									allEsts[e].sat_close_formatted = timeStrToTime(allEsts[e].sat_close);
									allEsts[e].sun_open_formatted = timeStrToTime(allEsts[e].sun_open);
									allEsts[e].sun_close_formatted = timeStrToTime(allEsts[e].sun_close);

									allEsts[e].created_at_formatted = dtToStr(allEsts[e].eq_created_at);
									allEsts[e].updated_at_formatted = dtToStr(allEsts[e].eq_updated_at);
									if(allEsts[e].created_at_formatted == allEsts[e].updated_at_formatted) {
										allEsts[e].updated_at_formatted = "-";
									}

									ests.push(allEsts[e]);
								}
							}

							ests.sort((a, b) => (a.is_active < b.is_active || (a.is_active == b.is_active && a.name > b.name)) ? 1 : -1);

							//ests = shuffle(ests);

							for(var x in ahEsts) {
								if(ests.filter(e => e.name === ahEsts[x].name && e.ah_status == "Added to AH").length > 0) {
									ahEsts[x].isFromEstQueue = true;
								}
							}

							res.render('admin', {
								showLogin: !req.session.authenticated,
								adminUsername: req.session.adminUsername,
								showImportEsts: true,
								estQueue: ests,
								ahEsts: ahEsts,
								//city: city,
								zip: zip,
								allStates: allStates,
								allOwners: owners
							});
						});
					}
					else {
						res.render('admin', {
							showLogin: !req.session.authenticated,
							adminUsername: req.session.adminUsername,
							showImportEsts: true,
							estQueue: [],
							ahEsts: ahEsts,
							//city: city,
							zip: zip,
							allStates: allStates,
							allOwners: owners
						});
					}
				});
			});
		});
	});
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

exports.importEsts = function(req, res) {
	var ests = JSON.parse(req.body.ests);
	//var city = req.body.city;
	var zip = req.body.zip;

	var allStates = [];
	Ref.getAllByRefTypeId(24, function(err2, states) {
		for(var s in states) {
			allStates.push(states[s].ref_code);
		}

		EstablishmentQueue.getAll(function(errEQ, allEsts) {
			insertEstToQueue(ests, 0, allEsts, 0, function(inserted) {
				EstablishmentQueue.getAll(function(errEQ2, allEsts2) {
					var ests2 = [];
					for(var e in allEsts2) {
						//if(allEsts2[e].topic == city) {
						if(allEsts2[e].zip == zip) {
							if(allEsts2[e].imageUrl != null)
								allEsts2[e].profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/" + allEsts2[e].imageUrl;
							else
								allEsts2[e].profile_pic = "/img/est-img/establishment_no_image.png";

							allEsts2[e].formatted_phone_num = formatPhone(allEsts2[e].phone_num);

							allEsts2[e].mon_open_formatted = timeStrToTime(allEsts2[e].mon_open);
							allEsts2[e].mon_close_formatted = timeStrToTime(allEsts2[e].mon_close);
							allEsts2[e].tue_open_formatted = timeStrToTime(allEsts2[e].tue_open);
							allEsts2[e].tue_close_formatted = timeStrToTime(allEsts2[e].tue_close);
							allEsts2[e].wed_open_formatted = timeStrToTime(allEsts2[e].wed_open);
							allEsts2[e].wed_close_formatted = timeStrToTime(allEsts2[e].wed_close);
							allEsts2[e].thu_open_formatted = timeStrToTime(allEsts2[e].thu_open);
							allEsts2[e].thu_close_formatted = timeStrToTime(allEsts2[e].thu_close);
							allEsts2[e].fri_open_formatted = timeStrToTime(allEsts2[e].fri_open);
							allEsts2[e].fri_close_formatted = timeStrToTime(allEsts2[e].fri_close);
							allEsts2[e].sat_open_formatted = timeStrToTime(allEsts2[e].sat_open);
							allEsts2[e].sat_close_formatted = timeStrToTime(allEsts2[e].sat_close);
							allEsts2[e].sun_open_formatted = timeStrToTime(allEsts2[e].sun_open);
							allEsts2[e].sun_close_formatted = timeStrToTime(allEsts2[e].sun_close);

							allEsts2[e].created_at_formatted = dtToStr(allEsts2[e].created_at);
							allEsts2[e].updated_at_formatted = dtToStr(allEsts2[e].updated_at);
							if(allEsts2[e].created_at_formatted == allEsts2[e].updated_at_formatted) {
								allEsts2[e].updated_at_formatted = "-";
							}

							ests2.push(allEsts2[e]);
						}
					}

					ests2.sort((a, b) => (a.is_active < b.is_active || (a.is_active == b.is_active && a.name > b.name)) ? 1 : -1);

					Establishment.getAll(function(errE, ahEstsRes) {
						var ahEsts = [];
						for(var ae in ahEstsRes) {
							if(allEsts2.filter(e => e.name === ahEstsRes[ae].name && e.ah_status == "Added to AH").length > 0) {
								ahEstsRes[ae].isFromEstQueue = true;
							}

							//if(ahEstsRes[ae].zip == zip) {
								ahEsts.push(ahEstsRes[ae]);
							//}
						}

						ahEsts.sort((a, b) => a.name > b.name ? 1 : -1);

						User.getOwners(function(errU, owns) {
							UserQueue.getOwners(function(errUQ, ownsUQ) {
								owners = [...owns, ...ownsUQ];

								owners.sort(OwnerComparator);

								res.render('admin', {
									showLogin: !req.session.authenticated,
									adminUsername: req.session.adminUsername,
									showImportEsts: true,
									ahEsts: ahEsts,
									estQueue: ests2,
									//city: city,
									zip: zip,
									allStates: allStates,
									allOwners: owners,
									adminImportStatus: inserted,
									adminErrStatus: (ests.length - inserted)
								});
							});
						});
					});
				});
			});
		});
	});
}

function insertEstToQueue(ests, i, allEsts, inserted, _callback) {
	var dontWant = ["7-Eleven", 
					"A&W Restaurant",
					"Applebee's Grill + Bar",
					"Arby's", 
					"Boston Market",
					"Buffalo Wild Wings",
					"Burger King", 
					"Caribou Coffee", 
					"Carl's Jr.",
					"Chick-fil-A", 
					"Chili's Grill & Bar",
					"Chipotle Mexican Grill",
					"Chuck E. Cheese",
					"Church's Chicken",
					"Circle K", 
					"Culver's",
					"Dairy Queen",
					"Dairy Queen Grill & Chill",
					"Del Taco",
					"Denny's", 
					"Dominos",
					"Domino's Pizza", 
					"Dunkin'",
					"Einstein Bros. Bagels", 
					"Firehouse Subs",
					"Five Guys", 
					"Godfather's Pizza",
					"Golden Corral Buffet & Grill",
					"Good Times Burgers & Frozen Custard", 
					"Hooters",
					"HuHot Mongolian Grill",
					"IHOP",
					"Jack in the Box",
					"Jersey Mike's Subs", 
					"Jimmy John's", 
					"Joe's Crab Shack",
					"KFC",
					"Little Caesars Pizza", 
					"McDonald's", 
					"Noodles and Company",
					"Olive Garden Italian Restaurant", 
					"Outback Steakhouse",
					"P.F. Chang's", 
					"Panda Express", 
					"Panera Bread", 
					"Papa John's Pizza", 
					"Papa Murphy's | Take 'N' Bake Pizza", 
					"Pizza Hut", 
					"Popeyes Louisiana Kitchen",
					"Potbelly Sandwich Shop",
					"QDOBA Mexican Eats",
					"Quiznos",
					"Red Lobster", 
					"Red Robin Gourmet Burgers and Brews",
					"Smashburger",
					"Sonic", 
					"Sonic Drive-In",
					"Starbucks", 
					"Steak 'n Shake",
					"Subway", 
					"Taco Bell", 
					"Taco John's",
					"Texas Roadhouse",
					"Village Inn",
					"Waffle House",
					"Wendy's", 
					"Which Wich Superior Sandwiches",
					"Wing Stop",
					"Wingstop",
					"Wing Street"];

	if(i < ests.length) {
		if(allEsts.filter(e => e.name === ests[i].name && e.address_1 === ests[i].address_1 && e.city === ests[i].city).length == 0 && ests[i].address_1 != null && ests[i].address_1 != "" && !dontWant.includes(ests[i].name)) {
			Ref.getAllByRefCode(ests[i].state, function(errState, resState) {

				if(errState)
					console.log("ERR STATE: " + errState);

				var address = {
					ADDRESS: {
						address_1: ests[i].address_1,
						address_2: ests[i].address_2 !== undefined ? ests[i].address_2 : null,
						city: ests[i].city,
						state_code_ref_id: resState.ref_id,
						zip: ests[i].zip,
						latitude: ests[i].latitude,
						longitude: ests[i].longitude
					}
				}
				//console.log(address);
				Address.insert(address, function(errAddress, resAddress) {

					if(errAddress)
						console.log("ERR ADDRESS: " + errAddress);

					var hours = {
						HOURS: {
							sun_open: ests[i].sun_open,
							sun_close: ests[i].sun_close,
							mon_open: ests[i].mon_open,
							mon_close: ests[i].mon_close,
							tue_open: ests[i].tue_open,
							tue_close: ests[i].tue_close,
							wed_open: ests[i].wed_open,
							wed_close: ests[i].wed_close,
							thu_open: ests[i].thu_open,
							thu_close: ests[i].thu_close,
							fri_open: ests[i].fri_open,
							fri_close: ests[i].fri_close,
							sat_open: ests[i].sat_open,
							sat_close: ests[i].sat_close,
						}
					}
					//console.log(hours);
					Hours.insert(hours, function(errHours, resHours) {

						if(errHours)
							console.log("ERR HOURS: " + errHours);

						var active = 0;
						if(ests[i].status == "OPERATIONAL")
							active = 1;
						
						var eq = {
							ESTABLISHMENT_QUEUE: {
								google_id: ests[i].google_id,
								name: ests[i].name,
								phone_num: ests[i].phone_num,
								address_id: resAddress.insertId,
								hours_id: resHours.insertId,
								topic: ests[i].topic,
								is_active: active,
								website: ests[i].website
							}
						}
						//console.log(eq);
						EstablishmentQueue.insert(eq, function(errEQ, resEQ) {
							
							if(errEQ)
								console.log("ERR EQ: " + errEQ);

							insertEstToQueue(ests, ++i, allEsts, ++inserted, _callback);
						});
					});
				});
			});
		}
		else if(allEsts.filter(e => e.google_id == ests[i].google_id || (e.name === ests[i].name && e.address_1 === ests[i].address_1 && e.city == ests[i].city)).length > 0) {
			var eqId = 0;
			var hoursId = 0;

			for(var a in allEsts) {
				if(allEsts[a].google_id == ests[i].google_id || (allEsts[a].name == ests[i].name && allEsts[a].address_1 == ests[i].address_1 && allEsts[a].city == ests[i].city)) {
					eqId = allEsts[a].establishment_id;
					hoursId = allEsts[a].hours_id;
					break;
				}
			}

			if(eqId != 0 && hoursId != 0) {
				var hours = {
					HOURS: {
						hours_id: hoursId,
						sun_open: ests[i].sun_open,
						sun_close: ests[i].sun_close,
						mon_open: ests[i].mon_open,
						mon_close: ests[i].mon_close,
						tue_open: ests[i].tue_open,
						tue_close: ests[i].tue_close,
						wed_open: ests[i].wed_open,
						wed_close: ests[i].wed_close,
						thu_open: ests[i].thu_open,
						thu_close: ests[i].thu_close,
						fri_open: ests[i].fri_open,
						fri_close: ests[i].fri_close,
						sat_open: ests[i].sat_open,
						sat_close: ests[i].sat_close,
					}
				}
				//console.log(hours);
				Hours.update(hours, function(errHours, resHours) {
					if(errHours)
						console.log("ERR HOURS: " + errHours);

					var active = 0;
					if(ests[i].status == "OPERATIONAL")
						active = 1;
					
					var eq = {
						ESTABLISHMENT_QUEUE: {
							establishment_id: eqId,
							google_id: ests[i].google_id,
							name: ests[i].name,
							phone_num: ests[i].phone_num,
							topic: ests[i].topic,
							is_active: active,
							website: ests[i].website
						}
					}
					//console.log(eq);
					EstablishmentQueue.update(eq, function(errEQ, resEQ) {
						insertEstToQueue(ests, ++i, allEsts, inserted, _callback);
					});
				});
			}
			else {
				insertEstToQueue(ests, ++i, allEsts, inserted, _callback);
			}
		}
		else {
			insertEstToQueue(ests, ++i, allEsts, inserted, _callback);
		}
	}
	else {
		//console.log(inserted);
		_callback(inserted);
	}
}

function timeStrToTime(time) {
	try {
		if(time && time.length == 4) {
			var h = time.substring(0, 2);
			var m = time.substring(2);
			var ampm = "am";

			h = parseInt(h);

			if(h == 0) {
				h = 12;
			}
			else if(h == 12) {
				ampm = "pm";
			}
			else if(h > 12) {
				h = h - 12;
				ampm = "pm";
			}

			return h + ":" + m + ampm;
		}
		else {
			return time;
		}
	}
	catch(e) {
		return time;
	}
}

exports.addToAH = function(req, res) {
	var estId = req.body.estId;
	var add = req.body.add;
	var reason = req.body.reason;

	if(add == "true") {
		UserEstablishmentQueue.getUserIdByEstablishmentId(estId, function(errUEQ, resUEQ) {
			getAndUpdateUserQueue(resUEQ, estId, function(userId) {
				EstablishmentQueue.getAllByEstablishmentId(estId, function(errEQ, resEQ) {
					const est = {
						ESTABLISHMENT: {
							name: resEQ.name,
							description: resEQ.description,
							phone_num: resEQ.phone_num,
							address_id: resEQ.address_id,
							hours_id: resEQ.hours_id,
							topic: resEQ.topic,
							is_active: resEQ.is_active,
							show_in_app: resEQ.is_active,
							website: resEQ.website,
							resWebsite: resEQ.resWebsite,
							menu_link: resEQ.menu_link,
							dog_friendly: resEQ.dog_friendly,
							patio: resEQ.patio,
							rooftop: resEQ.rooftop,
							brunch: resEQ.brunch,
							delivery: resEQ.delivery,
							takeout: resEQ.takeout
						}
					}
					//console.log(est);
					Establishment.insert(est, function(errInsertEst, resInsertEst) {
						//console.log(errInsertEst);
						//console.log(resInsertEst);
						const ei = {
							ESTABLISHMENT_IMAGE: {
								establishment_id: resInsertEst.insertId,
								image_id: resEQ.image_id
							}
						}
						//console.log(ei);
						EstablishmentImage.insert(ei, function(errInsertEI, resInsertEI) {
							//console.log(errInsertEI);
							//console.log(resInsertEI);
							const user_establishment = {
								USER_ESTABLISHMENT: {
									user_id: userId,
									establishment_id: resInsertEst.insertId
								}
							}
							//console.log(user_establishment);
							UserEstablishment.insert(user_establishment, function(errInsertUE, resInsertUE) {
								//console.log(errInsertUE);
								//console.log(resInsertUE);
								const eq = {
									ESTABLISHMENT_QUEUE: {
										establishment_id: estId,
										ah_status: "Added to AH"
									}
								}
								//console.log(eq);
								EstablishmentQueue.update(eq, function(errUpdate, resUpdate) {
									var address = resEQ.address_1;
									if(resEQ.address_2 != null && resEQ.address_2 != "")
										address += " " + resEQ.address_2;
									address += " | " + resEQ.city + ", " + resEQ.state + " " + resEQ.zip;

									res.send({ "name": resEQ.name, "address": address});
								});
							});
						});
					});
				});
			});
		});
	}
	else if(add == "false") {
		const eq = {
			ESTABLISHMENT_QUEUE: {
				establishment_id: estId,
				ah_status: reason == "willnotinsert" ? "Will Not Insert" : "Already in AH"
			}
		}
		EstablishmentQueue.update(eq, function(errUpdate, resUpdate) {
			res.send(true);
		});
	}
}

exports.addToQueue = function(req, res) {
	var estId = req.body.estId;

	var eq = {
		ESTABLISHMENT_QUEUE: {
			establishment_id: estId,
			ah_status: null
		}
	}
	EstablishmentQueue.update(eq, function(errUpdate, resUpdate) {
		res.send(true);
	});
}

exports.reviewEst = function(req, res) {
	var owner_id = req.body.owner_id;
	var owner_username = req.body.owner_username;
	var owner_email = req.body.owner_email;
	var owner_phoneNumber = req.body.owner_phoneNumber;
	if(owner_phoneNumber !== undefined)
		owner_phoneNumber = owner_phoneNumber.replace(/\D/g,'');
	var owner_needsToPay = req.body.owner_needsToPay;

	var establishment_id = req.body.establishment_id;
	var name = req.body.name;
	var address_id = req.body.address_id;
	var address_1 = req.body.address_1;
	var address_2 = req.body.address_2;
	var city = req.body.city;
	var state = req.body.state;
	var zip = req.body.zip;
	var phone_num = req.body.phone_num.replace(/\D/g,'');
	var website = req.body.website;
	var resWebsite = req.body.resWebsite;
	var menu_link = req.body.menu_link;
	//var instagram = req.body.instagram;
	//var facebook = req.body.facebook;
	//var twitter = req.body.twitter;
	//var snapchat = req.body.snapchat;
	var hours_id = req.body.hours_id;
	var mon_open = deFormatHours(req.body.mon_open);
	var mon_close = deFormatHours(req.body.mon_close);
	var tue_open = deFormatHours(req.body.tue_open);
	var tue_close = deFormatHours(req.body.tue_close);
	var wed_open = deFormatHours(req.body.wed_open);
	var wed_close = deFormatHours(req.body.wed_close);
	var thu_open = deFormatHours(req.body.thu_open);
	var thu_close = deFormatHours(req.body.thu_close);
	var fri_open = deFormatHours(req.body.fri_open);
	var fri_close = deFormatHours(req.body.fri_close);
	var sat_open = deFormatHours(req.body.sat_open);
	var sat_close = deFormatHours(req.body.sat_close);
	var sun_open = deFormatHours(req.body.sun_open);
	var sun_close = deFormatHours(req.body.sun_close);

	var description = req.body.description;
	var delivery = (req.body.delivery == "Yes" ? 1 : 0);
	var takeout = (req.body.takeout == "Yes" ? 1 : 0);
	var patio = (req.body.patio == "Yes" ? 1 : 0);
	var rooftop = (req.body.rooftop == "Yes" ? 1 : 0);
	var brunch = (req.body.brunch == "Yes" ? 1 : 0);
	var dog = (req.body.dog == "Yes" ? 1 : 0);

	Ref.getAllByRefCode(state, function(errRef, ref) {
		if (errRef || ref == undefined) {
			res.redirect('/importests');
			return;
		}

		var state_code_ref_id = ref.ref_id;
		var address = address_1 + " " + address_2 + ", " + city + ", " + state;

	  	Geocoder.geocode(address, function(errGeo, geo) {
	  		if (errGeo || geo[0] == undefined) {
	  			res.redirect('/importests');
				return;
	  		}

	  		if (geo[0].city != city) {
	  			res.redirect('/importests');
				return;
	  		}

	  		//if (geo[0].zipcode != zip) {
	  		//	res.redirect('/importests');
			//	return;
	  		//}

	  		var latitude = geo[0].latitude;
	      	var longitude = geo[0].longitude;

	      	var topic = "";

	      	if(getDistanceFromLatLng(latitude, longitude, 44.9778, -93.2650) <= 30) // Minne/STP
	      		topic = "minneapolismn";
	      	else if(getDistanceFromLatLng(latitude, longitude, 41.6611, -91.5302) <= 30) // Iowa City
	      		topic = "iowacityia";
	      	else if(getDistanceFromLatLng(latitude, longitude, 41.5868, -93.6250) <= 30) // Des Moines
	      		topic = "desmoinesia";
	      	else if(getDistanceFromLatLng(latitude, longitude, 39.7392, -104.9903) <= 30) // Denver
	      		topic = "denverco";
	      	else if(getDistanceFromLatLng(latitude, longitude, 38.2527, -85.7585) <= 30) // Louisville
	      		topic = "louisvilleky";
	      	else if(getDistanceFromLatLng(latitude, longitude, 27.7634, -82.5437) <= 30) // Tampa
                topic = "tampabayfl";

	      	var profile_pic_name = "";

	      	var params = {
	      		establishment_id: establishment_id,
	      		owner_id: owner_id,
	      		owner_username: owner_username,
	      		owner_email: owner_email,
	      		owner_phoneNumber: owner_phoneNumber,
	      		owner_needsToPay: owner_needsToPay,
	      	}

	      	updateEstQueueOwner(params, function(resUpdateOwner) {
	      		//console.log(resUpdateOwner);

		      	updateEstQueueImg(req.files, function(profPicName) {
		      		profile_pic_name = profPicName;

		      		const establishment = {
						ESTABLISHMENT_QUEUE: {
					      establishment_id: establishment_id,
					      name: name,
					      phone_num: phone_num,
					      website: website,
					      resWebsite: resWebsite,
					      menu_link: menu_link,
					      description: description,
					      delivery: delivery,
					      takeout: takeout,
					      patio: patio,
					      rooftop: rooftop,
					      brunch: brunch,
					      dog_friendly: dog,
					      //instagram: instagram,
					      //facebook: facebook,
					      //twitter: twitter,
					      //snapchat: snapchat,
					      topic: topic,
					      ah_status: "Reviewed"
					    }
					};

					const address = {
						ADDRESS: {
					      address_id: address_id,
					      address_1: address_1,
					      address_2: address_2,
					      city: city,
					      state_code_ref_id: state_code_ref_id,
					      zip: zip,
					      latitude: latitude,
					      longitude: longitude
					    }
					};

					const hours = { 
					    HOURS: {
					      hours_id: hours_id,
					      mon_open: mon_open,
					      mon_close: mon_close,
					      tue_open: tue_open,
					      tue_close: tue_close,
					      wed_open: wed_open,
					      wed_close: wed_close,
					      thu_open: thu_open,
					      thu_close: thu_close,
					      fri_open: fri_open,
					      fri_close: fri_close,
					      sat_open: sat_open,
					      sat_close: sat_close,
					      sun_open: sun_open,
					      sun_close: sun_close
					    }
				  	};

				  	//console.log(establishment);
				  	//console.log(address);
				  	//console.log(hours);

					Hours.update(hours, function(err, result) {
				  		if(result.affectedRows > 0) {
				  			Address.update(address, function(err, result) {
						  		if(result.affectedRows > 0) {
						  			EstablishmentQueue.update(establishment, function(err, result) {
						  				if(result.affectedRows > 0) {
						  					// save the new pic
						  					if(profile_pic_name && profile_pic_name != "") {

						  						EstablishmentQueueImage.getAllByEstablishmentId(establishment_id, function(errEQI, resEQI) {
						  							//console.log(errEQI);
						  							//console.log(resEQI);
						  							
						  							// update image
						  							if(errEQI == null && resEQI) {
						  								console.log('update image');

						  								const image = {
													  		IMAGE: {
													  		  image_id: resEQI.image_id,
													  		  image_location: profile_pic_name
													  		}
													  	}

									  					Image.update(image, function(err, result) {
									  						if(result.affectedRows > 0) {
									  							res.redirect('/importests?zip='+zip);
									  						}
									  						else {
									  							res.send(false);
									  						}
									  					});
						  							}
						  							// add image
						  							else if(errEQI == null) {
						  								console.log('add image');

						  								const image = {
													  		IMAGE: {
													  		  image_type_ref_id: 303,
													  		  image_location: profile_pic_name
													  		}
													  	}

									  					Image.insert(image, function(err, result) {
									  						if(result.affectedRows > 0) {
									  							//console.log(result);
									  							const estImg = {
									  								ESTABLISHMENT_QUEUE_IMAGE: {
									  									establishment_id: establishment_id,
									  									image_id: result.insertId
									  								}
									  							}
									  							EstablishmentQueueImage.insert(estImg, function(err, result) {
									  								if(result.affectedRows > 0) {
									  									//res.send(true);
									  									res.redirect('/importests?zip='+zip);
									  								}
									  								else {
									  									res.send(false);
									  								}
									  							});
									  						}
									  						else {
									  							res.send(false);
									  						}
									  					});
						  							}
						  						});
						  					}
						  					else {
											    //res.send(true);
											    res.redirect('/importests?zip='+zip);
						  					}
									    }
									    else {
				  							res.send(false);
				  						} 
						  			});
						  		}
						  		else {
		  							res.send(false);
		  						}
				  			});
				  		}
				  		else {
							res.send(false);
						}
				  	});
		      	});

	      	});
		});
	});
}

exports.validateReviewEst = function(req, res) {
	var errs = [];

	// owner
	var selOwnerId 	= req.query.ownerId;
	var email 		= req.query.email;
	var username 	= req.query.username;
	var phone 		= req.query.phone;
	var needsToPay  = req.query.needsToPay;

	if(username == "")
		errs.push("Enter an owner username");
	if(email == "")
		errs.push("Enter an owner email");
	if(needsToPay == "")
		errs.push("Decide if the owner needs to pay");

	// est - basic
	var estId 		= req.query.estId;
	var img 		= req.query.img;
	var name 		= req.query.name;
	var address_id 	= req.query.address_id;
	var address_1 	= req.query.address_1;
	var address_2 	= req.query.address_2;
	var city 		= req.query.city;
	var state 		= req.query.state;
	var zip 		= req.query.zip;
	var phone_num 	= req.query.phone_num.replace(/\D/g,'');
	var website 	= req.query.website;
	var resWebsite 	= req.query.resWebsite;
	var menu_link 	= req.query.menu_link;
	var hours_id 	= req.query.hours_id;
	var mon_open 	= deFormatHours(req.query.mon_open);
	var mon_close 	= deFormatHours(req.query.mon_close);
	var tue_open 	= deFormatHours(req.query.tue_open);
	var tue_close 	= deFormatHours(req.query.tue_close);
	var wed_open 	= deFormatHours(req.query.wed_open);
	var wed_close 	= deFormatHours(req.query.wed_close);
	var thu_open 	= deFormatHours(req.query.thu_open);
	var thu_close 	= deFormatHours(req.query.thu_close);
	var fri_open 	= deFormatHours(req.query.fri_open);
	var fri_close 	= deFormatHours(req.query.fri_close);
	var sat_open 	= deFormatHours(req.query.sat_open);
	var sat_close 	= deFormatHours(req.query.sat_close);
	var sun_open 	= deFormatHours(req.query.sun_open);
	var sun_close 	= deFormatHours(req.query.sun_close);

	if(img == "")
		errs.push("Choose a profile picture");
	if(name == "")
		errs.push("Enter a business name");
	if(address_1 == "" || city == "" || state == "" || zip == "")
		errs.push("Enter a valid address");
	if((mon_open == "" && mon_close != "") || (mon_open != "" && mon_close == ""))
		errs.push("Set valid hours for Monday");
	if((tue_open == "" && tue_close != "") || (tue_open != "" && tue_close == ""))
		errs.push("Set valid hours for Tuesday");
	if((wed_open == "" && wed_close != "") || (wed_open != "" && wed_close == ""))
		errs.push("Set valid hours for Wednesday");
	if((thu_open == "" && thu_close != "") || (thu_open != "" && thu_close == ""))
		errs.push("Set valid hours for Thursday");
	if((fri_open == "" && fri_close != "") || (fri_open != "" && fri_close == ""))
		errs.push("Set valid hours for Friday");
	if((sat_open == "" && sat_close != "") || (sat_open != "" && sat_close == ""))
		errs.push("Set valid hours for Saturday");
	if((sun_open == "" && sun_close != "") || (sun_open != "" && sun_close == ""))
		errs.push("Set valid hours for Sunday");

	// est - features
	var description = req.query.description;
	var delivery 	= (req.query.delivery == "Yes" ? 1 : 0);
	var takeout 	= (req.query.takeout == "Yes" ? 1 : 0);
	var patio 		= (req.query.patio == "Yes" ? 1 : 0);
	var rooftop 	= (req.query.rooftop == "Yes" ? 1 : 0);
	var brunch 		= (req.query.brunch == "Yes" ? 1 : 0);
	var dog 		= (req.query.dog == "Yes" ? 1 : 0);

	if(address_1 != "" && city != "" && state != "" && zip != "") {
		Ref.getAllByRefCode(state, function(errRef, ref) {
			if (errRef || ref == undefined) {
				errs.push("Internal error: " + errRef);
			}

			var state_code_ref_id = ref.ref_id;
			var address = address_1;
			if(address_2 != "")
				address += " " + address_2;
			address += ", " + city + ", " + state;

		  	Geocoder.geocode(address, function(errGeo, geo) {
		  		if (errGeo || geo[0] == undefined) {
		  			errs.push("Geocoder error: " + errGeo);
		  		}

		  		if(geo[0].city != city) {
		  			console.log("City does not match what Google found: " + geo[0].city);
			        //errs.push("City does not match what Google found: " + geo[0].city);
		  		}

		  		if(geo[0].zipcode != zip) {
		  			console.log("Zip does not match what Google found: " + geo[0].zipcode);
		  			//errs.push("Zip does not match what Google found: " + geo[0].zipcode);
		  		}

		  		// check if username or email already exist
				if(username != "" && email != "") {
					User.getAllByUsername(username, function(err, user) {
						UserQueue.getAllByUsername(username, function(errUQ, userQ) {
							if((user && user.user_id != selOwnerId) || (userQ && userQ.user_id != selOwnerId)) {
								errs.push("Owner username already exists");
							}
							User.getAllByEmail(email, function(err2, user2) {
								UserQueue.getAllByEmail(email, function(errUQ2, userQ2) {
									if((user2 && user2.user_id != selOwnerId) || (userQ2 && userQ2.user_id != selOwnerId)) {
										errs.push("Owner email already exists");
									}
									res.send(errs);
								});
							});
						});
					});
				}
				else {
					res.send(errs);
				}
		  	});
		});
	}
	else {
		// check if username or email already exist
		if(username != "" && email != "") {
			User.getAllByUsername(username, function(err, user) {
				UserQueue.getAllByUsername(username, function(errUQ, userQ) {
					if((user && user.user_id != selOwnerId) || (userQ && userQ.user_id != selOwnerId)) {
						errs.push("Owner username already exists");
					}
					User.getAllByEmail(email, function(err2, user2) {
						UserQueue.getAllByEmail(email, function(errUQ2, userQ2) {
							if((user2 && user2.user_id != selOwnerId) || (userQ2 && userQ2.user_id != selOwnerId)) {
								errs.push("Owner email already exists");
							}
							res.send(errs);
						});
					});
				});
			});
		}
		else {
			res.send(errs);
		}
	}
};

function updateEstQueueOwner(params, _callback) {
	var establishment_id = params.establishment_id;
	var owner_id = params.owner_id;
	var owner_username = params.owner_username;
	var owner_email = params.owner_email;
	var owner_phoneNumber = params.owner_phoneNumber;
	var owner_needsToPay = params.owner_needsToPay;

	// add owner
  	if(owner_id == "") {
  		// need to set username
  		if (owner_username == null || owner_username == "") {
  			console.log("Need to set username");
  			return;
  		}

  		var password = "AHgenPW$";

  		bcrypt.hash(password, 10, function(err, hash) {
  			if(!err) {
  				const user = {
		      		USER_QUEUE: {
		      		  email: owner_email,
		      		  username: owner_username,
		      		  password: hash,
		      		  phone_num: owner_phoneNumber,
					  user_type_ref_id: 305,
		      		  is_active: 1,
		      		  needs_to_pay: owner_needsToPay
		      		}
		      	}

				UserQueue.insert(user, function(err1, res1) {
					const user_establishment = {
						USER_ESTABLISHMENT_QUEUE: {
							user_id: res1.insertId,
							establishment_id: establishment_id
						}
					}

					UserEstablishmentQueue.insert(user_establishment, function(err2, res2) {
						_callback(true);
					});
				});
		    }
		    else {
		    	_callback(false);
		    }
		});
  	}
  	// existing owner
  	else {
  		UserEstablishmentQueue.getAllByEstablishmentId(establishment_id, function(errUEQ, resUEQ) {
  			// add user/est connection
  			if(resUEQ == null) {
  				const user_establishment = {
					USER_ESTABLISHMENT_QUEUE: {
						user_id: owner_id,
						establishment_id: establishment_id
					}
				}

				UserEstablishmentQueue.insert(user_establishment, function(err2, res2) {
					_callback(true);
				});
  			}
  			// user/est connection exists, do nothing
  			else {
  				_callback(true);
  			}
  		});
  	}
}

function updateEstQueueImg(files, _callback) {
	var profile_pic_name = "";

	if(files && files.profilePic) {
		profPic = files.profilePic;
		var ext = profPic.name.substring(profPic.name.indexOf("."));
		var rand = Math.floor(Math.random() * 1000000000);
		profile_pic_name = "estimg"+rand+ext;

		var path = __dirname + '/../public/img/est-img/' + profPic.name;
		profPic.mv(path, function(err) {
		    if (err)
		      	return res.status(500).send(err);

		    // Create S3 service object
			s3 = new AWS.S3({apiVersion: '2006-03-01'});

			// Configure the file stream and obtain the upload parameters
			var fileStream = fs.createReadStream(path);
			fileStream.on('error', function(err) {
			  console.log('File Error', err);
			});

			// call S3 to retrieve upload file to specified bucket
			var uploadParams = {Bucket: 'est-img', Key: profile_pic_name, Body: fileStream, ACL: 'public-read'};

			// call S3 to retrieve upload file to specified bucket
			s3.upload(uploadParams, function (err, data) {
			  if (err) {
			    console.log("Error", err);
			  } if (data) {
			    console.log("Upload Success", data.Location);
			    _callback(profile_pic_name);
			  }
			});
		});
	}
	else {
		_callback(profile_pic_name);
	}
}

function getAndUpdateUserQueue(resUEQ, estId, _callback) {
	if(resUEQ.queue_user_id != null) {
		//console.log("queue_user_id");
		UserQueue.getAllByEstablishmentId(estId, function(errUQ, resUQ) {
			//console.log(errUQ);
			//console.log(resUQ);
			const user = {
				USER: {
					user_type_ref_id: resUQ.user_type_ref_id,
					username: resUQ.username,
					password: resUQ.password,
					email: resUQ.email,
					stripe_id: resUQ.stripe_id,
					phone_num: resUQ.phone_num,
					is_active: resUQ.is_active,
					needs_to_pay: resUQ.needs_to_pay,
					show_specials: resUQ.show_specials,
					forgot_password: resUQ.forgot_password,
					claimed_at: resUQ.claimed_at,
					claim_email_key: resUQ.claim_email_key,
					claim_email_sent_at: resUQ.claim_email_sent_at
				}
			}
			//console.log(user);
			User.insert(user, function(errInsertUser, resInsertUser) {
				//console.log(errInsertUser);
				//console.log(resInsertUser);
				UserQueue.delete(resUQ.user_id, function(errDeleteUQ, resDeleteUQ) {
					//console.log(errDeleteUQ);
					//console.log(resDeleteUQ);
					UserEstablishmentQueue.deleteByUserId(resUQ.user_id, function(errDeleteUEQ, resDeleteUEQ) {
						//console.log(errDeleteUEQ);
						//console.log(resDeleteUEQ);
						_callback(resInsertUser.insertId);
					});
				});
			});
		});
	}
	else {
		//console.log("user_id");
		UserEstablishmentQueue.deleteByUserId(resUEQ.user_id, function(errDeleteUEQ, resDeleteUEQ) {
			//console.log(errDeleteUEQ);
			//console.log(resDeleteUEQ);
			_callback(resUEQ.user_id);
		});
	}
}



/* OWNER */

exports.selectOwner = function(req, res) {
	var selOwnerId = req.query.owner;
	if(selOwnerId != "" && selOwnerId != 0) {
		User.getAllByUserId(selOwnerId, function(errUser, user) {
			Establishment.getAllByOwnerId(selOwnerId, function(errEsts, ests) {
				establishments = ests;

				var allStates = [];
				Ref.getAllByRefTypeId(24, function(errRef, states) {
					for(var s in states) {
						allStates.push(states[s].ref_code);
					}

					var claimedAt = user.claimed_at;
					if(claimedAt) {
						claimedAt = new Date(claimedAt);
						claimedAt = getStartDate(claimedAt) + ", " + claimedAt.getFullYear() + " " + getTimeStr(claimedAt);
					}

					var claimEmailSentAt = user.claim_email_sent_at;
					if(claimEmailSentAt) {
						claimEmailSentAt = new Date(claimEmailSentAt);
						claimEmailSentAt = getStartDate(claimEmailSentAt) + ", " + claimEmailSentAt.getFullYear() + " " + getTimeStr(claimEmailSentAt);
					}

					res.render('admin', {
						showLogin: !req.session.authenticated,
						adminUsername: req.session.adminUsername,
						owners: owners,
						selOwnerId: selOwnerId,
						username: user.username,
						email: user.email,
						phoneNumber: user.phone_num,
						needsToPay: user.needs_to_pay,
						claimDate: claimedAt,
						claimEmailSentAt: claimEmailSentAt,
						ests: ests,
						allStates: allStates
					});
				});
			});
		});
	}
	else {
		Establishment.getAll(function(err, ests) {
			establishments = ests;
			res.render('admin', {
				showLogin: !req.session.authenticated,
				adminUsername: req.session.adminUsername,
				owners: owners,
				ests: ests
			});
		});
	}
};

exports.saveOwner = function(req, res) {
	var selOwnerId = req.body.ownerId;
	var email = req.body.email;
	var username = req.body.username;
	var phone_num = req.body.phoneNumber.replace(/\D/g,'');
	var needsToPay = req.body.needsToPay;

	// add owner
	if(selOwnerId == "" || selOwnerId == 0) {
		// need to set username
  		if (username == null || username == "") {
  			console.log("Need to set username");
  			res.redirect('admin');
  			return;
  		}

  		var password = "AHgenPW$";

  		bcrypt.hash(password, 10, function(err, hash) {
  			if(!err) {
  				const user = {
		      		USER: {
		      		  email: email,
		      		  username: username,
		      		  password: hash,
		      		  phone_num: phone_num,
					  		user_type_ref_id: 305,
		      		  is_active: 1,
		      		  needs_to_pay: needsToPay
		      		}
		      	}

					User.insert(user, function(err1, res1) {
						User.getOwners(function(err, owns) {
							owners = owns
							res.redirect('selectowner?owner='+res1.insertId+'&ownerHidden=');
						});
					});
		    }
		});
	}
	else {
		const user = {
      		USER: {
      		  user_id: selOwnerId,
      		  email: email,
      		  username: username,
      		  phone_num: phone_num,
      		  needs_to_pay: needsToPay
      		}
      	}

    User.update(user, function(err, result) {
			User.getOwners(function(err, owns) {
				owners = owns
				res.redirect('selectowner?owner='+selOwnerId+'&ownerHidden=');
			});
		});
	}
};

exports.validateOwner = function(req, res) {
	if(!req.query.delete || req.query.delete == "false") {
		var selOwnerId 	= req.query.ownerId;
		var email 		= req.query.email;
		var username 	= req.query.username;
		var phone 		= req.query.phone;

		var errs = [];

		if(username == "") {
			errs.push("Enter a username");
			res.send(errs);
		}
		else if(email == "") {
			errs.push("Enter an email");
			res.send(errs);
		}
		else {
			User.getAllByUsername(username, function(err, user) {
				if(user && user.user_id != selOwnerId) {
					errs.push("Username already exists");
				}
				User.getAllByEmail(email, function(err2, user2) {
					if(user2 && user2.user_id != selOwnerId) {
						errs.push("Email already exists");
					}
					res.send(errs);
				});
			});
		}
	}
	else {
		var selOwnerId 	= req.query.ownerId;
		var errs = [];

		if(establishments.length > 0) {
			errs.push("Delete all of the owner's establishments before deleting the owner");
		}
		res.send(errs);
	}
};

exports.proxyToUser = function(req, res) {
	delete req.session.establishment;
	User.getAllByUsername(req.body.user, function(err, user) {
    	if(user) {
    		Establishment.getAllByOwnerId(user.user_id, function(err, ests) {
		      if(ests) {
		        Image.getProfilePicForEstablishmentId(ests[0].establishment_id, function(err, image) {
		          var est = ests[0];
		          est.profile_pic_id = image.image_id;
		          est.profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/"+image.image_location;

		          if(est.topic == "denverco") {
		            req.session.tz = "MST";
		          }
		          else if(est.topic == "desmoinesia" || est.topic == "iowacityia" || est.topic == "minneapolismn") {
		            req.session.tz = "CST";
		          }
		          else if(est.topic == "louisvilleky" || est.topic == "tampabayfl") {
		            req.session.tz = "EST";
		          }

		          req.session.establishment = est;

	              req.user = user;
	              req.session.user = user;
	              req.session.proxy = true;
	              res.redirect('dashboard?page=home');
	           	});
	          }
	        });
        }
    });
};

exports.deleteOwner = function(req, res) {
	var selOwnerId = req.body.ownerId;

	User.delete(selOwnerId, function(err, result) {
		//console.log("error " + err);
		//console.log("result " + result);

		User.getOwners(function(err1, owns) {
			owners = owns;

			owners.sort(OwnerComparator);

			Establishment.getAll(function(err2, ests) {
				establishments = ests;

				res.render('admin', {
						showLogin: !req.session.authenticated,
						adminUsername: req.session.adminUsername,
						owners: owners,
						selOwnerId: null,
						ests: establishments
				});
			});
		});
	});
}

exports.sendClaimEmail = function(req, res) {
	var ceKey = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	
	fs.readFile(__dirname + '/../public/emails/claim_email.html', function (err, html) {
	    if (!err) {
	        var msg = ""+html;

	        while(msg.indexOf("{{ claimUrl }}") > -1) {
				msg = msg.replace("{{ claimUrl }}", "https://app.appyhourmobile.com/estlogin?email="+req.query.email+"&ceKey="+ceKey);
			}
		  
			var transporter = nodemailer.createTransport({
			    service: 'Gmail',
			    auth: {
			      user: 'appy-support@appyhourmobile.com',
			      pass: 'gpzkftethvbstvfn'
			      //pass: 'AppyHour2021!'
			    }
			});

			var mailOptions = {
			    from: 'AppyHour <appy-support@appyhourmobile.com>',
			    to: req.query.email,
			    subject: 'Claim Your Account',
			    html: msg
			};

			transporter.sendMail(mailOptions, function(error, info){
			    if (error) {
			      console.log(error);
			      res.send("Sorry, we're having some issues sending your message. Please try again later.");
			    } 
			    else {
			      console.log('Email sent: ' + info.response);
			      User.getAllByEmail(req.query.email, function(errUser, user) {
			        if(!errUser && user) {
			          const u = { 
			            USER: {
			              user_id: user.user_id, 
			              claim_email_key: ceKey,
			              claim_email_sent_at: Common.newDate(req.session.tz)
			            }
			          };
			          User.update(u, function(errUpdateUser, updatedUser) {
			            if(!errUpdateUser && updatedUser) {
			              //res.send("true");
			              res.redirect('selectowner?owner=' + user.user_id + '&ownerHidden=');
			            }
			            else {
			              res.send("Sorry, we're having some issues sending your message. Please try again later.");
			            }
			          });
			        }
			      });
			    }
			});



	    }       
	});
}



/* ESTABLISHMENT */

exports.selectEst = function(req, res) {
	var selEstId = req.query.est;
	var selOwnerId = req.query.ownerId;

	var dsSuccess = "";
	if(req.query.dsSuccess)
		dsSuccess = req.query.dsSuccess;

	var dsErr = "";
	if(req.query.dsErr)
		dsErr = req.query.dsErr;

	if(selEstId != "" && selEstId != 0) {
		Establishment.getAllByEstablishmentId(selEstId, function(errEst, est) {
			User.getAllByEstablishmentId(selEstId, function(errUser, user) {
				var selUserId = user.user_id;
				Address.getAllByAddressId(est.address_id, function(errAddress, address) {
					var selAddressId = address.address_id;
					Hours.getAllByHoursId(est.hours_id, function(errHours, hours) {
						var selHoursId = hours.hours_id;
						Image.getProfilePicForEstablishmentId(selEstId, function(errImage, image) {
							var selProfPicId = image.image_id;
							var selProfPicLocation = "https://s3.us-east-2.amazonaws.com/est-img/"+image.image_location;
							//var selProfPicLocation = 'img/est-img/'+image.image_location;

							Special.getAllByEstablishmentId(selEstId, function(errSpecial, specials) {

								Event.getAllByEstablishmentIdGroupRecurring(selEstId, function(errEvent, events) {

									var allStates = [];
									Ref.getAllByRefTypeId(24, function(errRef, states) {
										for(var s in states) {
											allStates.push(states[s].ref_code);
										}

										res.render('admin', {
											showLogin: !req.session.authenticated,
											adminUsername: req.session.adminUsername,
											owners: owners,
											selOwnerId: selUserId,
											ests: establishments,
											selEstId: selEstId,
											selUserId: selUserId,
											selProfPicId: selProfPicId,
											adminProfilePic: selProfPicLocation,
											establishment: est.name,
											todaysSpecials: getTodaysSpecials(specials, req.session.tz),
											exclusiveSpecials: getExclusiveSpecials(specials, req.session.tz),
											dailySpecials: getDailySpecials(specials),
											events: getActiveEvents(events, specials, req.session.tz),
											dsSuccess: dsSuccess,
											dsErr: dsErr,
											username: user.username,
											email: user.email,
											phoneNumber: user.phone_num,
											estPhoneNumber: est.phone_num,
											selAddressId: selAddressId,
											line1: address.address_1,
											line2: address.address_2,
											city: address.city,
											state: address.state,
											allStates: allStates,
											zip: address.zip,
											topic: est.topic,
											needsToPay: user.needs_to_pay,
											showInApp: est.show_in_app,
											website: est.website,
											resWebsite: est.resWebsite,
											menu_link: est.menu_link,
											//twitter: est.twitter,
											//facebook: est.facebook,
											//instagram: est.instagram,
											//snapchat: est.snapchat,
											description: est.description,
											delivery: est.delivery,
											takeout: est.takeout,
											patio: est.patio,
											rooftop: est.rooftop,
											brunch: est.brunch,
											dog: est.dog_friendly,
											selHoursId: selHoursId,
											monOpen: addColon(hours.mon_open),
											monClose: addColon(hours.mon_close),
											tueOpen: addColon(hours.tue_open),
											tueClose: addColon(hours.tue_close),
											wedOpen: addColon(hours.wed_open),
											wedClose: addColon(hours.wed_close),
											thuOpen: addColon(hours.thu_open),
											thuClose: addColon(hours.thu_close),
											friOpen: addColon(hours.fri_open),
											friClose: addColon(hours.fri_close),
											satOpen: addColon(hours.sat_open),
											satClose: addColon(hours.sat_close),
											sunOpen: addColon(hours.sun_open),
											sunClose: addColon(hours.sun_close)
										});

										/*
										SpecialArchive.getAllByEstablishmentId(est.establishment_id, function(err, specialsArc) {
											var imp = [];
											var impressions = getImpressionsEvent([...specials], [...specialsArc]);
											var toDate = getYYYYMMDD(Common.newDate(req.session.tz));

											panel.events({
											  event: impressions,
											  type: "general",
											  unit: "week",
											  from_date: "2019-01-01",
											  to_date: toDate
											}).then(function(data) {
												imp = getImpressions(data, [...specials], [...specialsArc]);

												res.render('admin', {
													showLogin: !authenticated,
													adminUsername: req.session.adminUsername,
													ests: establishments,
													selEstId: selEstId,
													adminProfilePic: selProfPicLocation,
													establishment: est.name,
													todaysSpecials: getTodaysSpecials(specials, req.session.tz),
													dailySpecials: getDailySpecials(specials),
													dsSuccess: dsSuccess,
													dsErr: dsErr,
													username: user.username,
													email: user.email,
													phoneNumber: user.phone_num,
													line1: address.address_1,
													line2: address.address_2,
													city: address.city,
													state: address.state,
													zip: address.zip,
													topic: est.topic,
													needsToPay: user.needs_to_pay,
													showInApp: est.show_in_app,
													website: est.website,
													twitter: est.twitter,
													facebook: est.facebook,
													instagram: est.instagram,
													snapchat: est.snapchat,
													monOpen: addColon(hours.mon_open),
													monClose: addColon(hours.mon_close),
													tueOpen: addColon(hours.tue_open),
													tueClose: addColon(hours.tue_close),
													wedOpen: addColon(hours.wed_open),
													wedClose: addColon(hours.wed_close),
													thuOpen: addColon(hours.thu_open),
													thuClose: addColon(hours.thu_close),
													friOpen: addColon(hours.fri_open),
													friClose: addColon(hours.fri_close),
													satOpen: addColon(hours.sat_open),
													satClose: addColon(hours.sat_close),
													sunOpen: addColon(hours.sun_open),
													sunClose: addColon(hours.sun_close),
													impressions: imp,
											    	toDate: toDate
												});
											});
										});
										*/
									});
								});
							});
						});
					});
				});
			});
		});
	}
	else {
		//resetSelectedValues();

		User.getAllByUserId(selOwnerId, function(errUser, user) {
			res.render('admin', {
				showLogin: !req.session.authenticated,
				adminUsername: req.session.adminUsername,
				owners: owners,
				selOwnerId: selOwnerId,
				username: user.username,
				email: user.email,
				phoneNumber: user.phone_num,
				needsToPay: user.needs_to_pay,
				ests: establishments
			});
		});
	}
};

exports.saveEst = function(req, res) {
	var selEstId = req.body.estId;
	var selOwnerId = req.body.ownerId;

	var profile_pic_name = "";
	var name = req.body.establishment;
	var phone_num = req.body.estPhoneNumber;
	var address_1 = req.body.line1;
	var address_2 = req.body.line2;
	var city = req.body.city;
	var state = req.body.state;
	var zip = req.body.zip;
	var topic = req.body.topic;
	var showInApp = req.body.showInApp;
	var website = req.body.website;
	var resWebsite = req.body.resWebsite;
	var menu_link = req.body.menu_link;
	//var twitter = req.body.twitter;
	//var facebook = req.body.facebook;
	//var instagram = req.body.instagram;
	//var snapchat = req.body.snapchat;
	var description = req.body.adminDescription;
	var delivery = req.body.adminDelivery;
	var takeout = req.body.adminTakeout;
	var patio = req.body.adminPatio;
	var rooftop = req.body.adminRooftop;
	var brunch = req.body.adminBrunch;
	var dog_friendly = req.body.adminDog;
	var mon_open = deFormatHours(req.body.mon_open);
	var mon_close = deFormatHours(req.body.mon_close);
	var tue_open = deFormatHours(req.body.tue_open);
	var tue_close = deFormatHours(req.body.tue_close);
	var wed_open = deFormatHours(req.body.wed_open);
	var wed_close = deFormatHours(req.body.wed_close);
	var thu_open = deFormatHours(req.body.thu_open);
	var thu_close = deFormatHours(req.body.thu_close);
	var fri_open = deFormatHours(req.body.fri_open);
	var fri_close = deFormatHours(req.body.fri_close);
	var sat_open = deFormatHours(req.body.sat_open);
	var sat_close = deFormatHours(req.body.sat_close);
	var sun_open = deFormatHours(req.body.sun_open);
	var sun_close = deFormatHours(req.body.sun_close);

	Ref.getAllByRefCode(state, function(err, ref) {
		if (err || ref == undefined) {
			res.redirect('admin');
			return;
		}

		var state_code_ref_id = ref.ref_id;
		var address = address_1 + ", " + city + ", " + state;

	  	Geocoder.geocode(address, function(err, geo) {
	  		if(err || geo[0] == undefined) {
	  			console.log("GEO err");
	  			res.redirect('admin');
				return;
	  		}

	  		if(geo[0].zipcode != zip) {
	  			console.log(geo[0].zipcode + " != " + zip);
	  			console.log("GEO zip error");
	  			res.redirect('admin');
				return;
	  		}

	  		var latitude = geo[0].latitude;
	      	var longitude = geo[0].longitude;

	      	// add establishment
	      	if(selEstId == 0 || selEstId == "") {
	      		//console.log("add establishment");

	      		// need to choose profile pic
	      		if(!req.files || !req.files.adminProfilePic) {
	      			console.log("Need to choose profile pic");
	      			res.redirect('admin');
	      			return;
	      		}

	      		// need to set address fields
	      		if(address_1 == null || address_1 == "") {
	      			console.log("Need to set address");
	      			res.redirect('admin');
	      			return;
	      		}
	      		if(city == null || city == "") {
	      			console.log("Need to set city");
	      			res.redirect('admin');
	      			return;
	      		}
	      		if(state == null || state == "") {
	      			console.log("Need to set state");
	      			res.redirect('admin');
	      			return;
	      		}
	      		if(zip == null || zip == "") {
	      			console.log("Need to set zip");
	      			res.redirect('admin');
	      			return;
	      		}

	      		var password = "AHgenPW$";

	      		bcrypt.hash(password, 10, function(err, hash) {
          			if(!err) {
						const address = {
							ADDRESS: {
						      address_1: address_1,
						      address_2: address_2,
						      city: city,
						      state_code_ref_id: state_code_ref_id,
						      zip: zip,
						      latitude: latitude,
						      longitude: longitude
						    }
						};

						const hours = {
						    HOURS: {
						      mon_open: mon_open,
						      mon_close: mon_close,
						      tue_open: tue_open,
						      tue_close: tue_close,
						      wed_open: wed_open,
						      wed_close: wed_close,
						      thu_open: thu_open,
						      thu_close: thu_close,
						      fri_open: fri_open,
						      fri_close: fri_close,
						      sat_open: sat_open,
						      sat_close: sat_close,
						      sun_open: sun_open,
						      sun_close: sun_close
						    }
					  	};

						var address_id, hours_id, establishment_id, image_id;

				    	Address.insert(address, function(err2, res2) {
				    		address_id = res2.insertId;
				    		Hours.insert(hours, function(err3, res3) {
				    			hours_id = res3.insertId;

				    			const establishment = {
									ESTABLISHMENT: {
								      name: name,
								      phone_num: phone_num,
								      address_id: address_id,
								  	  hours_id: hours_id,
								  	  topic: topic,
								  	  website: website,
								  	  resWebsite: resWebsite,
								  	  menu_link: menu_link,
								      //twitter: twitter,
								      //facebook: facebook,
								      //instagram: instagram,
								      //snapchat: snapchat,
								      description: description,
								      delivery: delivery,
								      takeout: takeout,
								      patio: patio,
								      rooftop: rooftop,
								      brunch: brunch,
								      dog_friendly: dog_friendly,
								      is_active: 1,
								      show_in_app: showInApp
								    }
								};

								Establishment.insert(establishment, function(err4, res4) {
								  	establishment_id = res4.insertId;

								  	const user_establishment = {
									  	USER_ESTABLISHMENT: {
									  	  user_id: selOwnerId,
									  	  establishment_id: establishment_id
									  	}
									}

									UserEstablishment.insert(user_establishment, function(err5, res5) {

										// insert profile pic
										if (req.files && req.files.adminProfilePic) {
											var profPic = req.files.adminProfilePic;
											var ext = profPic.name.substring(profPic.name.indexOf("."));
											profile_pic_name = "estimg"+establishment_id+ext;

											var path = __dirname + '/../public/img/est-img/' + profPic.name;
											profPic.mv(path, function(err) {
											    if (err)
											      	return res.status(500).send(err);

											    // Create S3 service object
												s3 = new AWS.S3({apiVersion: '2006-03-01'});

												// Configure the file stream and obtain the upload parameters
												var fileStream = fs.createReadStream(path);
												fileStream.on('error', function(err) {
												  console.log('File Error', err);
												});

												// call S3 to retrieve upload file to specified bucket
												var uploadParams = {Bucket: 'est-img', Key: profile_pic_name, Body: fileStream, ACL: 'public-read'};

												// call S3 to retrieve upload file to specified bucket
												s3.upload(uploadParams, function (err, data) {
												  if (err) {
												    console.log("Error", err);
												  } if (data) {
												    console.log("Upload Success", data.Location);
												  }
												});
											});
										}

									  	const image = {
									  		IMAGE: {
									  	      image_type_ref_id: 303,
									  	      image_location: profile_pic_name
									  		}
									  	}

									  	Image.insert(image, function(err6, res6) {
									  		image_id = res6.insertId;

									  		const establishment_image = {
									  			ESTABLISHMENT_IMAGE: {
									  			  establishment_id: establishment_id,
									  		      image_id: image_id
									  			}
									  		}

									  		EstablishmentImage.insert(establishment_image, function(err7, res7) {
									  			Establishment.getAllByOwnerId(selOwnerId, function(err, ests) {
													establishments = ests;
									  				res.redirect('selectest?est='+establishment_id+'&estHidden=');
									  			});
									  		});
									  	});
									});
								});
							});
						});
          			}
          		});
	      	}
	      	// edit establishment
	      	else {
	      		// edit profile pic if needed
				if (req.files && req.files.adminProfilePic) {
					var profPic = req.files.adminProfilePic;
					var ext = profPic.name.substring(profPic.name.indexOf("."));
					profile_pic_name = "estimg"+selEstId+ext;

					var path = __dirname + '/../public/img/est-img/' + profPic.name;
					profPic.mv(path, function(err) {
					    if (err)
					      	return res.status(500).send(err);

					    // Create S3 service object
						s3 = new AWS.S3({apiVersion: '2006-03-01'});

						// Configure the file stream and obtain the upload parameters
						var fileStream = fs.createReadStream(path);
						fileStream.on('error', function(err) {
						  console.log('File Error', err);
						});

						// call S3 to retrieve upload file to specified bucket
						var uploadParams = {Bucket: 'est-img', Key: profile_pic_name, Body: fileStream, ACL: 'public-read'};

						// call S3 to retrieve upload file to specified bucket
						s3.upload(uploadParams, function (err, data) {
						  if (err) {
						    console.log("Error", err);
						  } if (data) {
						    console.log("Upload Success", data.Location);
						  }
						});
					});
				}

		      	const establishment = {
					ESTABLISHMENT: {
				      establishment_id: selEstId,
				      name: name,
				      phone_num: phone_num,
				      topic: topic,
				      website: website,
				      resWebsite: resWebsite,
				      menu_link: menu_link,
				      //twitter: twitter,
				      //facebook: facebook,
				      //instagram: instagram,
				      //snapchat: snapchat,
				      description: description,
				      delivery: delivery,
				      takeout: takeout,
				      patio: patio,
				      rooftop: rooftop,
				      brunch: brunch,
				      dog_friendly: dog_friendly,
				      show_in_app: showInApp
				    }
				};

				const address = {
					ADDRESS: {
				      address_id: req.body.addressId,
				      address_1: address_1,
				      address_2: address_2,
				      city: city,
				      state_code_ref_id: state_code_ref_id,
				      zip: zip,
				      latitude: latitude,
				      longitude: longitude
				    }
				};

				const hours = {
				    HOURS: {
				      hours_id: req.body.hoursId,
				      mon_open: mon_open,
				      mon_close: mon_close,
				      tue_open: tue_open,
				      tue_close: tue_close,
				      wed_open: wed_open,
				      wed_close: wed_close,
				      thu_open: thu_open,
				      thu_close: thu_close,
				      fri_open: fri_open,
				      fri_close: fri_close,
				      sat_open: sat_open,
				      sat_close: sat_close,
				      sun_open: sun_open,
				      sun_close: sun_close
				    }
			  	};

			  	Hours.update(hours, function(err, result) {
			  		if(result.affectedRows > 0) {
			  			Address.update(address, function(err, result) {
					  		if(result.affectedRows > 0) {
					  			Establishment.update(establishment, function(err, result) {
					  				if(result.affectedRows > 0) {
					  					if(profile_pic_name && profile_pic_name != "") {
											// save the new pic
						  					const image = {
										  		IMAGE: {
										  		  image_id: req.body.profPicId,
										  		  image_location: profile_pic_name
										  		}
										  	}
						  					Image.update(image, function(err, result) {
						  						//if(result.affectedRows > 0) {
						  						Establishment.getAllByOwnerId(selOwnerId, function(err, ests) {
													establishments = ests;
						  							res.redirect('selectest?est='+selEstId+'&estHidden=');
						  						});
						  					});
					  					}
					  					else {
					  						Establishment.getAllByOwnerId(selOwnerId, function(err, ests) {
												establishments = ests;
					  							res.redirect('selectest?est='+selEstId+'&estHidden=');
					  						});
					  					}
								    }
					  			});
					  		}
			  			});
			  		}
			  	});
	      	}
	  	});
	});
};

exports.validateEst = function(req, res) {
	var validImg 	= (req.query.validImg == 'true');
	var name 		= req.query.name;
	var email 		= req.query.email;
	var username 	= req.query.username;
	var phone 		= req.query.phone;
	var address1 	= req.query.address1;
	var address2 	= req.query.address2;
	var city 		= req.query.city;
	var state 		= req.query.state;
	var zip 		= req.query.zip;
	var topic 		= req.query.topic;
	var needsToPay 	= req.query.needsToPay;
	var showInApp 	= req.query.showInApp;
	var website		= req.query.website;
	var resWebsite  = req.query.resWebsite;
	var menu_link    = req.query.menu_link;
	//var twitter 	= req.query.twitter;
	//var facebook 	= req.query.facebook;
	//var instagram 	= req.query.instagram;
	//var snapchat 	= req.query.snapchat;
	var monOpen 	= req.query.monOpen;
	var monClose 	= req.query.monClose;
	var tueOpen 	= req.query.tueOpen;
	var tueClose 	= req.query.tueClose;
	var wedOpen 	= req.query.wedOpen;
	var wedClose 	= req.query.wedClose;
	var thuOpen 	= req.query.thuOpen;
	var thuClose 	= req.query.thuClose;
	var friOpen 	= req.query.friOpen;
	var friClose 	= req.query.friClose;
	var satOpen 	= req.query.satOpen;
	var satClose 	= req.query.satClose;
	var sunOpen 	= req.query.sunOpen;
	var sunClose 	= req.query.sunClose;

	var errs = [];

	if(!validImg) {
		errs.push("Select a profile pic");
	}

	if(name == "") {
		errs.push("Enter an establishment name");
	}

	if(username == "") {
		errs.push("Enter a username");
	}

	if(address1 == "") {
		errs.push("Enter an address");
	}

	if(city == "") {
		errs.push("Enter a city");
	}

	if(state == "") {
		errs.push("Enter a state");
	}

	if(zip == "") {
		errs.push("Enter a zip");
	}

	if(topic == "") {
		errs.push("Select a topic for push notifications");
	}

	if((monOpen == "" && monClose != "") || (monOpen != "" && monClose == "")) {
		errs.push("Enter open and close time for Monday");
	}

	if((tueOpen == "" && tueClose != "") || (tueOpen != "" && tueClose == "")) {
		errs.push("Enter open and close time for Tuesday");
	}

	if((wedOpen == "" && wedClose != "") || (wedOpen != "" && wedClose == "")) {
		errs.push("Enter open and close time for Wednesday");
	}

	if((thuOpen == "" && thuClose != "") || (thuOpen != "" && thuClose == "")) {
		errs.push("Enter open and close time for Thursday");
	}

	if((friOpen == "" && friClose != "") || (friOpen != "" && friClose == "")) {
		errs.push("Enter open and close time for Friday");
	}

	if((satOpen == "" && satClose != "") || (satOpen != "" && satClose == "")) {
		errs.push("Enter open and close time for Saturday");
	}

	if((sunOpen == "" && sunClose != "") || (sunOpen != "" && sunClose == "")) {
		errs.push("Enter open and close time for Sunday");
	}

	if(monOpen == "" && monClose == "" && tueOpen == "" && tueClose == "" &&
	   wedOpen == "" && wedClose == "" && thuOpen == "" && thuClose == "" &&
	   friOpen == "" && friClose == "" && satOpen == "" && satClose == "" &&
	   sunOpen == "" && sunClose == "") {
		errs.push("Enter hours of operation");
	}

	if(state != "") {
		Ref.getAllByRefCode(state, function(err, ref) {
			if (err || ref == undefined) {
				errs.push("Enter a valid state");
				res.send(errs);
			}
			else {
				var address = address1 + ", " + city + ", " + state;
			  	Geocoder.geocode(address, function(err, geo) {
			  		if (err || geo[0] == undefined) {
			  			errs.push("Error getting geocode");
			  		}
					else if (geo[0].zipcode != zip) {
			  			errs.push("Zipcode does not match address - should be " + geo[0].zipcode);
			  		}

			  		res.send(errs);
			  	});
			}
		});
	}
	else {
		res.send(errs);
	}
};

exports.deleteEst = function(req, res) {
	var selOwnerId = req.body.ownerId;
	var selEstId = req.body.estId;
	var selUserId = req.body.userId;
	var selAddressId = req.body.addressId;
	var selHoursId = req.body.hoursId;

	if (selEstId != 0) {
		User.getAllByUserId(selOwnerId, function(err, user) {
			EstablishmentImage.getAllByEstablishmentId(selEstId, function(err2, estImage) {
				EstablishmentImage.deleteByEstablishmentId(selEstId, function(err3, res3) {
					Image.delete(estImage.image_id, function(err4, res4) {
						User.getManagersForEstablishmentId(selEstId, function(err13, managers) {
							UserEstablishment.deleteByEstablishmentId(selEstId, function(err5, res5) {
								EstablishmentSpecial.deleteByEstablishmentId(selEstId, function(err10, res10) {
									Special.deleteByEstablishmentId(selEstId, function(err11, res11) {
										Establishment.delete(selEstId, function(err7, res7) {
											Hours.delete(selHoursId, function(err8, res8) {
												Address.delete(selAddressId, function(err9, res9) {
													//var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
													//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
													//stripe.subscriptions.list({ customer: user.stripe_id }, function(err, subs) {
													//	if(subs) {
													//		stripe.subscriptions.update(subs.data[0].id, { cancel_at_period_end: true });
													//	}
													//});

													var userIds = [];
													for(var m in managers) {
														userIds.push(managers[m].user_id);
													}

													User.deleteAll(userIds, function(err12, res12) {

														Establishment.getAllByOwnerId(selOwnerId, function(err, ests) {
															establishments = ests;
															//resetSelectedValues();

															res.render('admin', {
														  		showLogin: !req.session.authenticated,
																	adminUsername: req.session.adminUsername,
														  		owners: owners,
														  		selOwnerId: selOwnerId,
														  		username: user.username,
														  		email: user.email,
														  		phoneNumber: user.phone_num,
														  		needsToPay: user.needs_to_pay,
														  		ests: establishments
															});
														});
													});
												});
											});
										});
									});
								});
							});
						});
					});
				});
			});
		});
	}
};



/* SPECIALS */

exports.addSpecial = function(req, res) {
	var establishment_id = req.body.estId;
	var priceType = req.body.priceType;
	var price = priceType != 302 && priceType != 307 ? req.body.price : 0; // BOGO and Free deals set price to 0
	var details = req.body.details;
	var specialType = getDealTypeRefIdByOGId(req.body.specialType);
	var startTime;
	var endTime;
	var days = [];

	// weekly
	if(req.body.weeklyOrExclusive == 'weekly') {
		var start = req.body.dailyStartTime.replace(':', '');
		var end = req.body.dailyEndTime.replace(':', '');
		start = parseInt(start);
		end = parseInt(end);

		if(start >= 400)
			startTime = "1970-01-01 " + req.body.dailyStartTime + ":00";
		else
			startTime = "1970-01-02 " + req.body.dailyStartTime + ":00";

		if(end > 400)
			endTime = "1970-01-01 " + req.body.dailyEndTime + ":00";
		else
			endTime = "1970-01-02 " + req.body.dailyEndTime + ":00";

		if(req.body.dailyDayMon == "true")
			days.push(1);
		if(req.body.dailyDayTue == "true")
			days.push(2);
		if(req.body.dailyDayWed == "true")
			days.push(3);
		if(req.body.dailyDayThu == "true")
			days.push(4);
		if(req.body.dailyDayFri == "true")
			days.push(5);
		if(req.body.dailyDaySat == "true")
			days.push(6);
		if(req.body.dailyDaySun == "true")
			days.push(0);
	}
	// exclusive
	else {
		var hours = req.body.hours;
		var minutes = req.body.minutes;
		
		if(req.body.exclusiveStart)
			startTime = new Date(req.body.exclusiveStart);
		else
			startTime = Common.newDate(req.session.tz);

		endTime = new Date(startTime.getTime());
		endTime.addHourss(hours);
		endTime.addMinutess(minutes);

		days.push(-1);
	}

	var special = {
		establishment_id: establishment_id,
		price: price,
      	price_type_ref_id: priceType,
      	details: details,
      	deal_type_ref_id: specialType,
      	days: days,
     	start_time: startTime,
      	end_time: endTime,
      	created_at: Common.newDate(req.session.tz),
      	updated_at: Common.newDate(req.session.tz)
	}


	insertSpecial(special, 0, {sErr: null, sResult: null, esErr: null, esResult: null});

	res.redirect('selectest?est='+establishment_id+'&estHidden=&dsSuccess=&dsErr=');
};

function insertSpecial(special, start, response){
	if(start < special.days.length) {
		const s = {
		    SPECIAL: {
		      price: special.price,
		      price_type_ref_id: special.price_type_ref_id,
		      details: special.details,
		      deal_type_ref_id: special.deal_type_ref_id,
		      day: special.days[start],
		      start_time: special.start_time,
		      end_time: special.end_time,
		      created_at: special.created_at,
		      updated_at: special.updated_at
		    }
		};

		Special.insert(s, function(err, result) {
			response.sErr = err;
			response.sResult = result;

			if(result.affectedRows > 0) {
				const establishment_special = {
				    ESTABLISHMENT_SPECIAL: {
				      establishment_id: special.establishment_id,
				      special_id: result.insertId,
				      created_at: special.created_at,
				      updated_at: special.updated_at
				    }
				};
				EstablishmentSpecial.insert(establishment_special, function(err, result) {
					response.esErr = err;
					response.esResult = result;
					if(result.affectedRows > 0) {
						insertSpecial(special, ++start, response);
					} else {
						return response;
					}
				});
			} else {
				return response;
			}
		});
	}
	else {
		return response;
	}
}

exports.addDailySpecial = function(req, res) {
	var priceType = req.body.dailyPriceType;
	var price = req.body.dailyPrice;
	// BOGO and Free deals set price to 0
	if (priceType == 302 || priceType == 307)
		price = 0;
	var details = req.body.dailyDetails;
	// for BOTH start/end times: if hours < 4, set to 1970-01-02
	var start = req.body.dailyStartTime.replace(':', '');
	var end = req.body.dailyEndTime.replace(':', '');
	start = parseInt(start);
	end = parseInt(end);

	var startTime, endTime;

	if(start >= 400)
		startTime = "1970-01-01 " + req.body.dailyStartTime + ":00";
	else
		startTime = "1970-01-02 " + req.body.dailyStartTime + ":00";

	if(end > 400)
		endTime = "1970-01-01 " + req.body.dailyEndTime + ":00";
	else
		endTime = "1970-01-02 " + req.body.dailyEndTime + ":00";

	var type = getDealTypeRefIdByOGId(req.body.dailyType);
	var now = Common.newDate(req.session.tz);
	var time = new Date(now);

	var establishmentId = req.body.estId;

	Special.getAllByEstablishmentId(establishmentId, function(errSpecial, specials) {
		var ds = getDailySpecials(specials);
		var dsSuccess = "";
		var dsErr = "";

		for(var i = 0; i < 7; i++){
			if(req.body.dailyDayMon != null && req.body.dailyDayMon == 'on' && i == 1){
				if(ds[0].length < 5) {
					const monSpecial = {
					    SPECIAL: {
					      price: price,
					      price_type_ref_id: priceType,
					      details: details,
					      deal_type_ref_id: type,
					      day: 1,
					      start_time: startTime,
					      end_time: endTime,
					      created_at: time,
					      updated_at: time
					    }
					};
					insertSpecial(monSpecial, establishmentId, time);
					if(dsSuccess == "")
						dsSuccess = "Special added for Monday";
					else
						dsSuccess = dsSuccess + ", Monday";
				}
				else {
					if(dsErr == "")
						dsErr = "Special not added for Monday";
					else
						dsErr = dsErr + ", Monday";
				}
			}
			if(req.body.dailyDayTue != null && req.body.dailyDayTue == 'on' && i == 2){
				if(ds[1].length < 5) {
					const tueSpecial = {
					    SPECIAL: {
					      price: price,
					      price_type_ref_id: priceType,
					      details: details,
					      deal_type_ref_id: type,
					      day: 2,
					      start_time: startTime,
					      end_time: endTime,
					      created_at: time,
					      updated_at: time
					    }
					};
					insertSpecial(tueSpecial, establishmentId, time);
					if(dsSuccess == "")
						dsSuccess = "Special added for Tuesday";
					else
						dsSuccess = dsSuccess + ", Tuesday";
				}
				else {
					if(dsErr == "")
						dsErr = "Special not added for Tuesday";
					else
						dsErr = dsErr + ", Tuesday";
				}
			}
			if(req.body.dailyDayWed != null && req.body.dailyDayWed == 'on' && i == 3){
				if(ds[2].length < 5) {
					const wedSpecial = {
					    SPECIAL: {
					      price: price,
					      price_type_ref_id: priceType,
					      details: details,
					      deal_type_ref_id: type,
					      day: 3,
					      start_time: startTime,
					      end_time: endTime,
					      created_at: time,
					      updated_at: time
					    }
					};
					insertSpecial(wedSpecial, establishmentId, time);
					if(dsSuccess == "")
						dsSuccess = "Special added for Wednesday";
					else
						dsSuccess = dsSuccess + ", Wednesday";
				}
				else {
					if(dsErr == "")
						dsErr = "Special not added for Wednesday";
					else
						dsErr = dsErr + ", Wednesday";
				}
			}
			if(req.body.dailyDayThu != null && req.body.dailyDayThu == 'on' && i == 4){
				if(ds[3].length < 5) {
					const thuSpecial = {
					    SPECIAL: {
					      price: price,
					      price_type_ref_id: priceType,
					      details: details,
					      deal_type_ref_id: type,
					      day: 4,
					      start_time: startTime,
					      end_time: endTime,
					      created_at: time,
					      updated_at: time
					    }
					};
					insertSpecial(thuSpecial, establishmentId, time);
					if(dsSuccess == "")
						dsSuccess = "Special added for Thursday";
					else
						dsSuccess = dsSuccess + ", Thursday";
				}
				else {
					if(dsErr == "")
						dsErr = "Special not added for Thursday";
					else
						dsErr = dsErr + ", Thursday";
				}
			}
			if(req.body.dailyDayFri != null && req.body.dailyDayFri == 'on' && i == 5){
				if(ds[4].length < 5) {
					const friSpecial = {
					    SPECIAL: {
					      price: price,
					      price_type_ref_id: priceType,
					      details: details,
					      deal_type_ref_id: type,
					      day: 5,
					      start_time: startTime,
					      end_time: endTime,
					      created_at: time,
					      updated_at: time
					    }
					};
					insertSpecial(friSpecial, establishmentId, time);
					if(dsSuccess == "")
						dsSuccess = "Special added for Friday";
					else
						dsSuccess = dsSuccess + ", Friday";
				}
				else {
					if(dsErr == "")
						dsErr = "Special not added for Friday";
					else
						dsErr = dsErr + ", Friday";
				}
			}
			if(req.body.dailyDaySat != null && req.body.dailyDaySat == 'on' && i == 6){
				if(ds[5].length < 5) {
					const satSpecial = {
					    SPECIAL: {
					      price: price,
					      price_type_ref_id: priceType,
					      details: details,
					      deal_type_ref_id: type,
					      day: 6,
					      start_time: startTime,
					      end_time: endTime,
					      created_at: time,
					      updated_at: time
					    }
					};
					insertSpecial(satSpecial, establishmentId, time);
					if(dsSuccess == "")
						dsSuccess = "Special added for Saturday";
					else
						dsSuccess = dsSuccess + ", Saturday";
				}
				else {
					if(dsErr == "")
						dsErr = "Special not added for Saturday";
					else
						dsErr = dsErr + ", Saturday";
				}
			}
			if(req.body.dailyDaySun != null && req.body.dailyDaySun == 'on' && i == 0){
				if(ds[6].length < 5) {
					const sunSpecial = {
					    SPECIAL: {
					      price: price,
					      price_type_ref_id: priceType,
					      details: details,
					      deal_type_ref_id: type,
					      day: 0,
					      start_time: startTime,
					      end_time: endTime,
					      created_at: time,
					      updated_at: time
					    }
					};
					insertSpecial(sunSpecial, establishmentId, time);
					if(dsSuccess == "")
						dsSuccess = "Special added for Sunday";
					else
						dsSuccess = dsSuccess + ", Sunday";
				}
				else {
					if(dsErr == "")
						dsErr = "Special not added for Sunday";
					else
						dsErr = dsErr + ", Sunday";
				}
			}
		}

		if(dsErr != "") {
			dsErr = dsErr + " (max 5 specials)";
		}
		if(dsSuccess != "") {
			dsSuccess = dsSuccess + " successfully";
		}

		res.redirect('selectest?est='+establishmentId+'&estHidden=&dsSuccess='+dsSuccess+'&dsErr='+dsErr);
	});
};

exports.deleteSpecial = function(req, res) {
	EstablishmentSpecial.deleteBySpecialId(req.body.delSpecialId, function(err, result){
		if(result.affectedRows > 0){
			Special.delete(req.body.delSpecialId, function(err, result){
				if(result.affectedRows > 0){
					var selEstId = req.body.estId;
					res.redirect('selectest?est='+selEstId+'&estHidden=');
				} else {

				}
			});
		} else {

		}
	});
};

exports.deleteDailySpecial = function(req, res) {
	EstablishmentSpecial.deleteBySpecialId(req.body.delDailySpecialId, function(err, result){
		if(result.affectedRows > 0){
			Special.delete(req.body.delDailySpecialId, function(err, result){
				if(result.affectedRows > 0){
					var selEstId = req.body.estId;
					res.redirect('selectest?est='+selEstId+'&estHidden=');
				} else {

				}
			});
		} else {

		}
	});
};



/* EVENTS */

exports.addEvent = function(req, res) {
	var establishment_id = req.body.estId;
	var title = req.body.title;
	var details = req.body.details;
	var startTime;
	var endTime;
	var days = [];

	// weekly
	if(req.body.weeklyOrExclusive == 'weekly') {
		var start = req.body.weeklyEventStartTime.replace(':', '');
		var end = req.body.weeklyEventEndTime.replace(':', '');
		start = parseInt(start);
		end = parseInt(end);

		if(start >= 400)
			startTime = "1970-01-01 " + req.body.weeklyEventStartTime + ":00";
		else
			startTime = "1970-01-02 " + req.body.weeklyEventStartTime + ":00";

		if(end > 400)
			endTime = "1970-01-01 " + req.body.weeklyEventEndTime + ":00";
		else
			endTime = "1970-01-02 " + req.body.weeklyEventEndTime + ":00";

		if(req.body.weeklyEventDayMon == "true")
			days.push(1);
		if(req.body.weeklyEventDayTue == "true")
			days.push(2);
		if(req.body.weeklyEventDayWed == "true")
			days.push(3);
		if(req.body.weeklyEventDayThu == "true")
			days.push(4);
		if(req.body.weeklyEventDayFri == "true")
			days.push(5);
		if(req.body.weeklyEventDaySat == "true")
			days.push(6);
		if(req.body.weeklyEventDaySun == "true")
			days.push(0);
	}
	// exclusive
	else {
		startTime = new Date(req.body.oneTimeEventStart);
		endTime = new Date(req.body.oneTimeEventEnd);
		days.push(-1);
	}

	var event = {
		establishment_id: establishment_id,
		title: title,
		details: details,
      	days: days,
     	start_time: startTime,
      	end_time: endTime,
      	created_at: Common.newDate(req.session.tz),
      	updated_at: Common.newDate(req.session.tz)
	}

	insertEvent(event, 0, {sErr: null, sResult: null, esErr: null, esResult: null});

	res.redirect('selectest?est='+establishment_id+'&estHidden=&dsSuccess=&dsErr=');
};

function insertEvent(event, start, response){
	if(start < event.days.length) {
		const e = {
		    EVENT: {
		      title: event.title,
		      details: event.details,
		      day: event.days[start],
		      start_time: event.start_time,
		      end_time: event.end_time,
		      created_at: event.created_at,
		      updated_at: event.updated_at
		    }
		};

		Event.insert(e, function(err, result) {
			response.sErr = err;
			response.sResult = result;

			if(result.affectedRows > 0) {
				const establishment_event = {
				    ESTABLISHMENT_EVENT: {
				      establishment_id: event.establishment_id,
				      event_id: result.insertId,
				      created_at: event.created_at,
				      updated_at: event.updated_at
				    }
				};
				EstablishmentEvent.insert(establishment_event, function(err, result) {
					response.esErr = err;
					response.esResult = result;
					if(result.affectedRows > 0) {
						insertEvent(event, ++start, response);
					} else {
						return response;
					}
				});
			} else {
				return response;
			}
		});
	}
	else {
		return response;
	}
}


exports.saveEvent = function(req, res) {
	var ids = req.body.eventIds;
	var days = req.body.days;
	var title = req.body.title;
	var details = req.body.details;
	var origDetails = req.body.origDetails;
	var startDate = req.body.startDate;
	//var startDateTime = req.body.startDateTime;
	//var endDateTime = req.body.endDateTime;
	var startTime = req.body.startTime;
	var endDate = req.body.endDate;
	var endTime = req.body.endTime;
	var mon = req.body.eventDayMon;
	var tue = req.body.eventDayTue;
	var wed = req.body.eventDayWed;
	var thu = req.body.eventDayThu;
	var fri = req.body.eventDayFri;
	var sat = req.body.eventDaySat;
	var sund = req.body.eventDaySun;
	var updated = Common.newDate(req.session.tz);

	// one-time
	if(days == "-1") {
		var s = getDateTime(startTime, startDate);
		var e = getDateTime(endTime, endDate);

		const ev = {
		    EVENT: {
		      event_id: ids,
		      details: details,
		      day: -1,
		      start_time: s,
		      end_time: e,
	      	  updated_at: updated
		    }
		};

		//console.log(s + " - " + e);
		//console.log(daysBetween(s, e));

		//console.log("EVENT START");
		//console.log(s);
		//console.log("EVENT END");
		//console.log(e);

		Event.update(ev, function(err, result) {
			Special.getAllByEventId(ids, function(err2, result2) {
				if(daysBetween(s, e) == 0)
					updateSpecials(result2, 0, s, e, req, res);
				else
					updateSpecialsMultiday(result2, 0, s, e, req, res);
			});
		});
	}
	// recurring
	else {
		var start = new Date("01 Jan 1970 "+startTime+" UTC");
		var end = new Date("01 Jan 1970 "+endTime+" UTC");

		//console.log("tz: " + req.session.tz);
		//console.log("-");
		//console.log(start);
		//console.log(end);
		//console.log("-");

		//start.subtractHoursDST(req.session.tz);
		//end.subtractHoursDST(req.session.tz);
		//start.setHours(start.getHours()-6); // UTC issue
		//end.setHours(end.getHours()-6); // UTC issue

		//console.log(start);
		//console.log(end);
		

		if(start.getHours() < 4) {
			start.setDate(2);
		} else {
			start.setDate(1);
		}
		if(end.getHours() < 4) {
			end.setDate(2);
		} else {
			end.setDate(1);
		}

		// fix for weird bug that sets date to 1969-12-02
		if(start.getYear() == 69 && start.getMonth() == 11) {
			start.setYear(70);
			start.setMonth(0);
		}
		if(end.getYear() == 69 && end.getMonth() == 11) {
			end.setYear(70);
			end.setMonth(0);
		}

		var parameters = [
			title,
			details,
			days.split(",")[0],
			start,
			end,
			mon,
			tue,
			wed,
			thu,
			fri,
			sat,
			sund,
			origDetails
		];

		//console.log(parameters);

		var idsArr = ids.split(",");

		Special.getAllByEventId(idsArr[0], function(err, result) {
			insertRecurringEvent(parameters, 0, result, req, res);
		});
	}
};

function insertRecurringEvent(parameters, start, specials, req, res) {
	if(start < 7) {
		var now = Common.newDate(req.session.tz);
		var updated = new Date(now);
		var editMon = parameters[5];
		var editTue = parameters[6];
		var editWed = parameters[7];
		var editThu = parameters[8];
		var editFri = parameters[9];
		var editSat = parameters[10];
		var editSun = parameters[11];

		var newParameters = [
			parameters[0],
			parameters[12], //origDetails
			start
		];

		Event.getEventIdForRecurring(newParameters, function(err, results) {
			var inDayArr = false;
			if(start == 0 && editSun == 'true')
				inDayArr = true;
			else if(start == 1 && editMon == 'true')
				inDayArr = true;
			else if(start == 2 && editTue == 'true')
				inDayArr = true;
			else if(start == 3 && editWed == 'true')
				inDayArr = true;
			else if(start == 4 && editThu == 'true')
				inDayArr = true;
			else if(start == 5 && editFri == 'true')
				inDayArr = true;
			else if(start == 6 && editSat == 'true')
				inDayArr = true;

			// in database
			if(results && results.length > 0) {
				// in database and selected on form
				if(inDayArr) {
					const ev = {
					    EVENT: {
					      event_id: results[0].event_id,
					      details: parameters[1], // new details
					      day: start,
					      start_time: parameters[3],
					      end_time: parameters[4],
				      	  updated_at: updated
					    }
					};

					Event.update(ev, function(err2, result2) {
						Special.getAllByEventId(results[0].event_id, function(err3, result3) {
							updateSpecialsRecurring(result3, 0, parameters, start, specials, req, res);
						});
					});
				}
				// in database but not selected on form
				else {
					var id = results[0].event_id;
					Special.deleteByEventId(id, function(err3, result3) {
						EventSpecial.deleteByEventId(id, function(err2, result2) {
							EstablishmentEvent.deleteByEventId(id, function(err4, result4) {
								if(result4.affectedRows > 0){
									Event.delete(id, function(err5, result5) {
										//if(result5.affectedRows > 0){
											insertRecurringEvent(parameters, ++start, specials, req, res);
										//}
									});
								} else {
									insertRecurringEvent(parameters, ++start, specials, req, res);
								}
							});
						});
					});
				}
			}
			// not in database
			else {
				// not in database but selected on form
				if(inDayArr) {
					const ev = {
					    EVENT: {
					      title: parameters[0],
					      details: parameters[1],
					      day: start,
					      start_time: parameters[3],
					      end_time: parameters[4],
				      	  updated_at: updated
					    }
					};

					Event.insert(ev, function(err2, result2) {
						if(result2.affectedRows > 0) {
							const establishment_event = {
							    ESTABLISHMENT_EVENT: {
							      establishment_id: req.body.estId,
							      event_id: result2.insertId,
							      created_at: updated,
							      updated_at: updated
							    }
							};
							EstablishmentEvent.insert(establishment_event, function(err3, result3) {
								addSpecialsRecurring(result2.insertId, 0, parameters, start, specials, req, res);
							});
						}
						else {
							insertRecurringEvent(parameters, ++start, specials, req, res);
						}
					});
				}
				// not in database and not selected on form
				else {
					insertRecurringEvent(parameters, ++start, specials, req, res);
				}
			}
		});
	}
	else {
		res.redirect('selectest?est='+req.body.estId+'&estHidden=');
	}
}

function addSpecialsRecurring(eventId, start, parameters, eventStart, eventSpecials, req, res) {
	if(start < eventSpecials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	price: eventSpecials[start].price,
		    	price_type_ref_id: eventSpecials[start].price_type_ref_id,
		    	details: eventSpecials[start].details,
		    	deal_type_ref_id: eventSpecials[start].deal_type_ref_id,
		    	day: eventStart,
		      	start_time: parameters[3],
		      	end_time: parameters[4],
		      	created_at: now,
		      	updated_at: now
		    }
		};

		Special.insert(special, function(err, result) {
			if(result.affectedRows > 0) {
				var specialId = result.insertId;
				const establishment_special = {
				    ESTABLISHMENT_SPECIAL: {
				      establishment_id: req.session.establishment.establishment_id,
				      special_id: specialId,
				      created_at: now,
				      updated_at: now
				    }
				};

				EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
					const event_special = {
					    EVENT_SPECIAL: {
					      event_id: eventId,
					      special_id: specialId,
					      created_at: now,
					      updated_at: now
					    }
					};
					EventSpecial.insert(event_special, function(err3, result3) {
						addSpecialsRecurring(eventId, ++start, parameters, eventStart, eventSpecials, req, res);
					});
				});
			}
		});
	}
	else {
		insertRecurringEvent(parameters, ++eventStart, eventSpecials, req, res);
	}
}

function updateSpecials(specials, start, startTime, endTime, req, res) {
	if(start < specials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	special_id: specials[start].special_id,
		      	start_time: startTime,
		      	end_time: endTime,
		      	updated_at: now
		    }
		};

		Special.update(special, function(err, result) {
			updateSpecials(specials, ++start, startTime, endTime, req, res);
		});
	}
	else {
		res.redirect('selectest?est='+req.body.estId+'&estHidden=');
	}
}

function updateSpecialsMultiday(specials, start, startTime, endTime, req, res) {
	if(start < specials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	special_id: specials[start].special_id,
		      	start_time: startTime,
		      	end_time: endTime,
		      	updated_at: now
		    }
		};

		if(specials[start].start_time < startTime || specials[start].end_time > endTime) {
			EstablishmentSpecial.deleteBySpecialId(specials[start].special_id, function(err1, result1) {
				if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
					Special.delete(specials[start].special_id, function(err2, result2) {
						if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
							EventSpecial.deleteBySpecialId(specials[start].special_id, function(err3, result3) {
								if(result3 != null && result3 != undefined && result3.affectedRows > 0) {
									updateSpecialsMultiday(specials, ++start, startTime, endTime, req, res);
								}
							});
						} else {
							res.send(false);
						}
					});
				}
				else {
					res.send(false);
				}
			});
		}
		else {
			updateSpecialsMultiday(specials, ++start, startTime, endTime, req, res);
		}
	}
	else {
		res.redirect('selectest?est='+req.body.estId+'&estHidden=');
	}
}

function updateSpecialsRecurring(specials, start, parameters, eventStart, eventSpecials, req, res) {
	if(start < specials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	special_id: specials[start].special_id,
		      	start_time: parameters[3],
		      	end_time: parameters[4],
		      	updated_at: now
		    }
		};

		Special.update(special, function(err, result) {
			updateSpecialsRecurring(specials, ++start, parameters, eventStart, eventSpecials, req, res);
		});
	}
	else {
		insertRecurringEvent(parameters, ++eventStart, eventSpecials, req, res);
	}
}

function getDateTime(start, startDate) {
	var startH = parseInt(start.substring(0, start.indexOf(":")));
	var startM = parseInt(start.substring(start.indexOf(":")+1));

	var s = new Date(startDate);
	s.setHours(s.getHours() + startH);
	s.setMinutes(s.getMinutes() + startM);

	//if(startH < 4) {
	//	s.setDate(s.getDate() + 1);
	//}

	return s;
}

exports.addSpecialToEvent = function(req, res) {
	var estId = req.query.estId;
	var now = Common.newDate(req.session.tz);
	var created = new Date(now);
	var eventId = req.query.eventId;
	var days = req.query.days;
	var startTime = req.query.startTime;
	var endTime = req.query.endTime;
	var priceType = req.query.priceType;
	var price = req.query.price;
	var details = req.query.details;
	var specialType = req.query.specialType;

	Event.getAllByEventId(eventId, function(err, result) {
		if(days == undefined) {
			days = [result.start_time];
			startTime = result.start_time.getHours() + ":" + result.start_time.getMinutes();
			endTime = result.end_time.getHours() + ":" + result.end_time.getMinutes();
		}

		if(result.day == -1) {
			const parameters = {
			  estId: estId,
			  eventId: eventId,
		      price: price,
		      price_type_ref_id: priceType,
		      details: details,
		      deal_type_ref_id: specialType,
		      day: -1,
		      startTime: startTime,
		      endTime: endTime,
		      created_at: created,
		      updated_at: created
			};

			insertSpecialToMultidayEvent(parameters, days, [], 0, res);
		}
		else {
			const parameters = {
			  estId: estId,
		      price: price,
		      price_type_ref_id: priceType,
		      details: details,
		      deal_type_ref_id: specialType,
		      start_time: result.start_time,
		      end_time: result.end_time,
		      created_at: created,
		      updated_at: created
			};

			insertSpecialToRecurringEvent(result.title, result.details, parameters, 0, 0, res);
		}
	});
};

function insertSpecialToMultidayEvent(parameters, days, specialIds, start, res) {
	if(start < days.length) {
		var startTime = new Date(days[start]);
		var startH = parameters.startTime.substring(0, parameters.startTime.indexOf(":"));
		var startM = parameters.startTime.substring(parameters.startTime.indexOf(":")+1);

		startTime.setHours(startH);
		startTime.setMinutes(startM);

		var endTime = new Date(days[start]);
		var endH = parameters.endTime.substring(0, parameters.endTime.indexOf(":"));
		var endM = parameters.endTime.substring(parameters.endTime.indexOf(":")+1);

		endTime.setHours(endH);
		endTime.setMinutes(endM);

		const special = {
		    SPECIAL: {
		      price: parameters.price,
		      price_type_ref_id: parameters.price_type_ref_id,
		      details: parameters.details,
		      deal_type_ref_id: parameters.deal_type_ref_id,
		      day: parameters.day,
		      start_time: startTime,
		      end_time: endTime,
		      created_at: parameters.created_at,
		      updated_at: parameters.updated_at
		    }
		};

		Special.insert(special, function(err1, result1) {
			if(result1.affectedRows > 0) {
				var specialId = result1.insertId;
				specialIds.push(specialId);

				const establishment_special = {
				    ESTABLISHMENT_SPECIAL: {
				      establishment_id: parameters.estId,
				      special_id: specialId,
				      created_at: parameters.created_at,
				      updated_at: parameters.created_at
				    }
				};

				EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
					
					if(result2.affectedRows > 0) {
						
						const event_special = {
						    EVENT_SPECIAL: {
						      event_id: parameters.eventId,
						      special_id: specialId,
						      created_at: parameters.created_at,
						      updated_at: parameters.created_at
						    }
						};

						EventSpecial.insert(event_special, function(err3, result3) {
							if(result3.affectedRows > 0) {
								//res.send({ specialId: specialId });
								insertSpecialToMultidayEvent(parameters, days, specialIds, ++start, res);
							} else {
								res.send(false);
							}
						});
					} else {
						res.send(false);
					}
				});
			} else {
				res.send(false);
			}
		});
	}
	else {
		res.send({ specialIds: specialIds });
	}
}

function insertSpecialToRecurringEvent(title, details, parameters, start, specialId, res) {
	if(start < 7) {
		var newParameters = [ title, details, start ];
		Event.getEventIdForRecurring(newParameters, function(err, result) {
			if(result.length > 0) {
				const special = {
				    SPECIAL: {
				      price: parameters.price,
				      price_type_ref_id: parameters.price_type_ref_id,
				      details: parameters.details,
				      deal_type_ref_id: parameters.deal_type_ref_id,
				      day: start,
				      start_time: parameters.start_time,
				      end_time: parameters.end_time,
				      created_at: parameters.created_at,
				      updated_at: parameters.updated_at
				    }
				};

				Special.insert(special, function(err1, result1) {
					if(result1.affectedRows > 0) {
						var specialId = result1.insertId;
						const establishment_special = {
						    ESTABLISHMENT_SPECIAL: {
						      establishment_id: parameters.estId,
						      special_id: specialId,
						      created_at: parameters.created_at,
						      updated_at: parameters.updated_at
						    }
						};
						EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
							if(result2.affectedRows > 0) {
								const event_special = {
								    EVENT_SPECIAL: {
								      event_id: result[0].event_id,
								      special_id: specialId,
								      created_at: parameters.created_at,
						      		  updated_at: parameters.updated_at
								    }
								};
								EventSpecial.insert(event_special, function(err3, result3) {
									if(result3.affectedRows > 0) {
										insertSpecialToRecurringEvent(title, details, parameters, ++start, specialId, res);
									} else {
										res.send(false);
									}
								});
							} else {
								res.send(false);
							}
						});
					} else {
						res.send(false);
					}
				});
			}
			else {
				insertSpecialToRecurringEvent(title, details, parameters, ++start, specialId, res);
			}
		});
	}
	else {
		res.send({ specialId: specialId });
	}
}


exports.deleteEvent = function(req, res) {
	var id = req.body.eventId;
	var estId = req.body.estId;

	Event.getAllByEventId(id, function(err, result) {
		if(result.day == -1) {
			SpecialArchive.archiveByEventId(id, function(err5, result5) {
				Special.deleteByEventId(id, function(err2, result2) {
					//EventSpecial.deleteByEventId(id, function(err1, result1) {
						//EstablishmentEvent.deleteByEventId(id, function(err3, result3) {
							//if(result3.affectedRows > 0){
								Event.delete(id, function(err4, result4) {

									const ev = {
									    EVENT_ARCHIVE: {
									      event_id: result.event_id,
									      title: result.title,
									      details: result.details,
									      day: result.day,
									      start_time: result.start_time,
									      end_time: result.end_time,
									      deleted_at: Common.newDate(req.session.tz),
									      created_at: result.created_at,
								      	  updated_at: Common.newDate(req.session.tz)
									    }
									};

									EventArchive.insert(ev, function(err6, result6) {
									//if(result4.affectedRows > 0){
										res.send(true);
									//}
									});
								});
							//} else {
							//	res.redirect('dashboard?page=events');
							//}
						//});
					//});
				});
			});
		}
		else {
			deleteRecurringEvent(result.title, 0, res, req.session.tz, estId);
		}
	});
};

function deleteRecurringEvent(title, start, res, tz, estId) {
	if(start < 7) {
		var newParameters = [ title, start ];

		Event.getAllForRecurring(newParameters, function(err, result) {
			if(result.length > 0) {
				SpecialArchive.archiveByEventId(result[0].event_id, function(err5, result5) {
					Special.deleteByEventId(result[0].event_id, function(err2, result2) {
						//EventSpecial.deleteByEventId(result[0].event_id, function(err1, result1) {
							//EstablishmentEvent.deleteByEventId(result[0].event_id, function(err3, result3) {
								//if(result3.affectedRows > 0){
									Event.delete(result[0].event_id, function(err4, result4) {
										var now = Common.newDate(tz);
										const ev = {
										    EVENT_ARCHIVE: {
										      event_id: result[0].event_id,
										      title: result[0].title,
										      details: result[0].details,
										      day: result[0].day,
										      start_time: result[0].start_time,
										      end_time: result[0].end_time,
										      deleted_at: Common.newDate(tz),
										      created_at: result[0].created_at,
									      	  updated_at: Common.newDate(tz)
										    }
										};

										EventArchive.insert(ev, function(err6, result6) {


										//if(result4.affectedRows > 0){
											deleteRecurringEvent(title, ++start, res, tz, estId);
										//}
										});
									});
								//} else {
								//	deleteRecurringEvent(title, ++start, res, tz);
								//}
							//});
						//});
					});
				});
			}
			else {
				deleteRecurringEvent(title, ++start, res, tz, estId);
			}
		});
	}
	else {
		res.send(true);
	}
}

function getActiveEvents(events, specials, tz) {
	var evs = [];

	/*
	var eventIds = "(0)";

	if(events.length > 0) {
		eventIds = "(";
		for(var i in events) {
			eventIds = eventIds + events[i].event_id;
			if(i < events.length-1) {
				eventIds = eventIds + ",";
			}
			else if(i == events.length-1) {
				eventIds = eventIds + ")";
			}
		}
	}
	*/

	for(var e in events) {
		var end = new Date(events[e].end_time);
		var curr = Common.newDate(tz);
		if(events[e].days != '-1' || (events[e].days == '-1' && end.getTime() >= curr.getTime())) {
			if(events[e].days != '-1') 
				events[e].date = getRecurringStr(events[e].days);
			else if(daysBetween(events[e].start_time, events[e].end_time) > 0)
				events[e].date = (parseInt(events[e].start_time.getMonth())+1) + "/" + events[e].start_time.getDate() + "/" + (parseInt(events[e].start_time.getYear())+1900) + " - " + (parseInt(events[e].end_time.getMonth())+1) + "/" + events[e].end_time.getDate() + "/" + (parseInt(events[e].end_time.getYear())+1900);
			else
				events[e].date = (parseInt(events[e].start_time.getMonth())+1) + "/" + events[e].start_time.getDate() + "/" + (parseInt(events[e].start_time.getYear())+1900);

			events[e].time = formatStartEndTime(events[e].start_time) + " - " + formatStartEndTime(events[e].end_time);

			events[e].specials = [];
			for(var s in specials) {
				if(specials[s].event_id && specials[s].event_id == events[e].event_id) {
					var now = Common.newDate(tz);
					var n = getTodaysDayInt(tz);

					// we need today at 4am, so that we can see if exclusive special was created today
					var todayAt4am = Common.newDate(tz);
					if (todayAt4am.getHours() < 4) {
						todayAt4am.setDate(todayAt4am.getDate()-1)
					}
					todayAt4am.setHours(4);
					todayAt4am.setMinutes(0);
					todayAt4am.setSeconds(0);

					specials[s].dealType = getDealTypeImageByRefId(specials[s].deal_type_ref_id);
					specials[s].duration = formatStartEndTime(specials[s].start_time) + " - " + formatStartEndTime(specials[s].end_time);

                    if(specials[s].price_type_ref_id == 300)
                    	specials[s].priceAndDetails = "$" + specials[s].price.toFixed(2) + " " + specials[s].details;
                    else if(specials[s].price_type_ref_id == 301)
                    	specials[s].priceAndDetails = specials[s].price.toFixed(0) + "% Off " + specials[s].details;
                    else if(specials[s].price_type_ref_id == 302)
                    	specials[s].priceAndDetails = "BOGO " + specials[s].details;
                    else if(specials[s].price_type_ref_id == 307)
                    	specials[s].priceAndDetails = "Free " + specials[s].details;

	  				// expired
	  				if(specials[s].start_time > todayAt4am && specials[s].end_time < now) {
	  					specials[s].expired = true;
	  				}
	  				// current
	  				else if(specials[s].start_time < now && specials[s].end_time > now) {
	  					specials[s].timeremaining = dateDiff(specials[s].end_time, tz);
	  					specials[s].current = true;
	  				}
	  				// upcoming
	  				else {
	  					specials[s].upcoming = true;
	  				}

					events[e].specials.push(specials[s]);
				}
			}


			if(events[e].days == '-1') {
				var es = {...events[e].specials};
				var especials = [];
				for(var i in es) {
					if(especials.length > 0) {
						var inArr = false;
						for(var x in especials) {
							if(especials[x].dealType == es[i].dealType && especials[x].priceAndDetails == es[i].priceAndDetails &&
							   especials[x].start_time.getHours() == es[i].start_time.getHours() && especials[x].start_time.getMinutes() == es[i].start_time.getMinutes() &&
							   especials[x].end_time.getHours() == es[i].end_time.getHours() && especials[x].end_time.getMinutes() == es[i].end_time.getMinutes()) {
								especials[x].date += ", " + (parseInt(es[i].start_time.getMonth())+1) + "/" + es[i].start_time.getDate();
								inArr = true;
							}
						}
						if(!inArr) {
							es[i].date = (parseInt(es[i].start_time.getMonth())+1) + "/" + es[i].start_time.getDate();
							especials.push(es[i]);
						}
					}
					else {
						es[i].date = (parseInt(es[i].start_time.getMonth())+1) + "/" + es[i].start_time.getDate();
						especials.push(es[i]);
					}
				}

				especials.sort((a, b) => (a.start_time > b.start_time) ? 1 : -1);

				events[e].specials = especials;
			}

			evs.push(events[e]);
		}
	}
	return evs;
}

function getRecurringStr(days) {
	var str = "";
	var dayArr = days.split(",");

	//sort day array 1, 2, 3, 4, 5, 6, 0
	if(dayArr.length > 1) {
		var sorted = false;
		while(!sorted) {
			sorted = true;
			for(var d = 0; d < dayArr.length - 1; d++) {
				var d1 = parseInt(dayArr[d+1]);
				var d2 = parseInt(dayArr[d]);
				if((d1 < d2 && d1 != 0) || d2 == 0) {
					var temp = dayArr[d];
					dayArr[d] = dayArr[d+1];
					dayArr[d+1] = temp;
					sorted = false;
				}
			}
		}
	}

	if(dayArr.length == 1) {
		str = str + intDayToString(dayArr[0]);
	}
	else if(dayArr.length == 2) {
		str = str + intDayToString(dayArr[0]) + " and " + intDayToString(dayArr[1]);
	}
	else {
		for(var i in dayArr) {
			if(i < dayArr.length - 2) {
				str = str + intDayToString(dayArr[i]) + ", ";
			}
			else if(i < dayArr.length - 1) {
				str = str + intDayToString(dayArr[i]) + ", and ";
			}
			else {
				str = str + intDayToString(dayArr[i]);
			}
		}
	}

	return str;
}

function daysBetween(a, b) {
  	var aa = new Date(a);
  	var bb = new Date(b);

	var diffMs = Math.abs(bb - aa); 
	var diffDays = Math.floor(diffMs / (1000 * 60 * 60 * 24)); 

	return diffDays;
}



/* ANALYTICS */

exports.analytics = function(req, res) {
	//resetSelectedValues();

	Special.getAllFromDate('2015-01-01', function(err, specials) {
		SpecialArchive.getAllFromDate('2015-01-01', function(err, specialsArc) {
			var imp = [];
			var impressions = getImpressionsEvent([...specials], [...specialsArc]);
			var toDate = getYYYYMMDD(Common.newDate(req.session.tz));

			panel.eventNames({
				type: "general"
			}).then(function(names) {
				impressions = checkImpressions(names, impressions);

				panel.events({
				  	event: impressions,
				  	type: "general",
				  	unit: "week",
				  	from_date: "2019-06-03",
				  	to_date: toDate
				}).then(function(data) {
					imp = getImpressions(data, [...specials], [...specialsArc]);

					res.render('admin', {
						showLogin: !req.session.authenticated,
						adminUsername: req.session.adminUsername,
						ests: establishments,
						impressionsForAll: imp
					});
				});
			});

		});
	});
};



/* SUPPLIERS/PROMOS */

exports.getPromos = function(req, res) {
	initSuppliersPromos(req, res);
};

function initSuppliersPromos(req, res) {
	if(req.query && req.query.location)
		chosenLocation = req.query.location;
	else
		chosenLocation = -1;

	Supplier.getOwners(function(errSup, suppliers) {
		for(var s in suppliers) {
			suppliers[s].phone_num_formatted = formatPhone(suppliers[s].phone_num);
			suppliers[s].created = dtToStr(suppliers[s].created_at);

			if(suppliers[s].claimed_at == null && suppliers[s].claim_email_sent_at == null) {
				suppliers[s].status = "Claim Email Not Sent";
			}
			else if(suppliers[s].claimed_at == null && suppliers[s].claim_email_sent_at != null) {
				suppliers[s].status = "Pending Activation";
				suppliers[s].claimEmailSent = dtToStr(suppliers[s].claim_email_sent_at);
			}
			else if(suppliers[s].claimed_at != null && suppliers[s].is_active == 1) {
				suppliers[s].status = "Active";
			}
			else if(suppliers[s].claimed_at != null && suppliers[s].is_active == 0) {
				suppliers[s].status = "Inactive";
			}
		}

		SupplierArchive.getOwners(function(errSupArchive, supArchived) {
			for(var a in supArchived) {
				supArchived[a].phone_num_formatted = formatPhone(supArchived[a].phone_num);
				supArchived[a].created = dtToStr(supArchived[a].created_at);
				supArchived[a].status = "Archived";
				suppliers.push(supArchived[a]);
			}

			allSuppliers = suppliers;

			var toDate = getYYYYMMDD(Common.newDate(req.session.tz));

			var curr = Common.newDate(req.session.tz);
			curr.setHours(0,0,0,0);

			// last 7 days
			curr.setDate(curr.getDate()-1);
			var bb = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var aa = new Date(curr.getTime());

			// 2 weeks ago
			curr.setDate(curr.getDate()-1);
			var dd = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var cc = new Date(curr.getTime());

			// 3 weeks ago
			curr.setDate(curr.getDate()-1);
			var ff = new Date(curr.getTime());
			curr.setDate(curr.getDate()-6);
			var ee = new Date(curr.getTime());

			thisWeek = intMonthToStringAbbr(aa.getMonth()) + " " + aa.getDate() + " - " + intMonthToStringAbbr(bb.getMonth()) + " " + bb.getDate();
			lastWeek = intMonthToStringAbbr(cc.getMonth()) + " " + cc.getDate() + " - " + intMonthToStringAbbr(dd.getMonth()) + " " + dd.getDate();

			if(chosenLocation == -1 || allPromos.length == 0) {
				Promo.getAll(function(err, result) {
					Location.getAll(function(errLoc, locations) {
						promoLocations = locations;

						for(var r in result) {
							if(result[r].supplier_name == null)
								result[r].supplier_name = "-";

							if(result[r].active == 0)
								result[r].activeYN = "No";
							else
								result[r].activeYN = "Yes";

							if(result[r].active == 1 && (result[r].supplier_promo_id == null || (result[r].supplier_promo_id != null && result[r].approved_at != null))) {
								result[r].status = "Active";
								result[r].place = 1;
							}
							else if(result[r].active == 0 && (result[r].supplier_promo_id == null || (result[r].supplier_promo_id != null && result[r].approved_at != null))) {
								result[r].status = "Inactive";
								result[r].place = 2;
							}
							else if(result[r].supplier_promo_id != null && result[r].approved_at == null && result[r].denied_at == null) {
								result[r].status = "Pending Review";
								result[r].place = 0;
							}
							else if(result[r].supplier_promo_id != null && result[r].approved_at == null && result[r].denied_at != null) {
								result[r].status = "Denied";
								result[r].place = 3;
							}
							else {
								result[r].status = "";
								result[r].place = 4;
							}

							result[r].dayStr = intDayToString(result[r].day);

							//result[r].location = getLocationFromCoords(result[r].latitude, result[r].longitude);
							for(var l in locations) {
								if(locations[l].latitude == result[r].latitude) {
									result[r].location = locations[l].location_id;
									result[r].locationStr = locations[l].location;
									break;
								}
							}

							result[r].createdAt = result[r].created_at.toString();

							if(result[r].archive_dt != null) {
								result[r].archived = result[r].archive_dt.toString();
								result[r].place = 5;
							}
							else {
								result[r].archived = null;
							}

							//console.log(result[r].archived);

							result[r].thisWeek = 0;
							result[r].lastWeek = 0;
							result[r].prevWeek = 0;
							result[r].allWeeks = 0;
							result[r].intChangeThisWeek = 0;
							result[r].intChangeLastWeek = 0;

							result[r].priority = "-";
						}

						// sort promos
						result.sort((a, b) => (a.place > b.place) ? 1 : -1);

						panel.eventProperties({
						  	event: "Promo pressed",
						  	name: "promo_id",
						  	type: "general",
						  	unit: "day",
						  	from_date: "2020-08-01",
						  	to_date: toDate
						}).then(function(data) {
							var vals = data.data.values;
							promoAnalytics = vals;

							//console.log(vals);

							for(var d in vals) {
								//console.log(d);
								var objKeys = Object.keys(vals[d]);
								var objVals = Object.values(vals[d]);
								//console.log(objKeys);
								//console.log(objVals);

								for(var k in objKeys) {
									var dayDt = new Date(objKeys[k] + " 00:00:00");
									for(var promo in result) {
										if(result[promo].promo_id == d) {
											result[promo].allWeeks += parseInt(objVals[k]);
											
											if(dayDt.getTime() >= aa.getTime() && dayDt.getTime() <= bb.getTime()) {
												result[promo].thisWeek += parseInt(objVals[k]);
											}
										 	else if(dayDt.getTime() >= cc.getTime() && dayDt.getTime() <= dd.getTime()) {
												result[promo].lastWeek += parseInt(objVals[k]);
											}
											else if(dayDt.getTime() >= ee.getTime() && dayDt.getTime() <= ff.getTime()) {
												result[promo].prevWeek += parseInt(objVals[k]);
											}
										}
									}
								}
							}

							for(var promo in result) {
								if(result[promo].lastWeek > 0 && result[promo].prevWeek > 0) {
									result[promo].intChangeLastWeek = ((result[promo].lastWeek - result[promo].prevWeek) / result[promo].prevWeek) * 100;
									result[promo].intChangeLastWeek = result[promo].intChangeLastWeek.toFixed(0);
								}
								else if(result[promo].lastWeek > 0 && result[promo].prevWeek == 0) {
									result[promo].intChangeLastWeek = 100;
								}
								else if(result[promo].lastWeek == 0 && result[promo].prevWeek > 0) {
									result[promo].intChangeLastWeek = -100;
								}


								if(result[promo].thisWeek > 0 && result[promo].lastWeek > 0) {
									result[promo].intChangeThisWeek = ((result[promo].thisWeek - result[promo].lastWeek) / result[promo].lastWeek) * 100;
									result[promo].intChangeThisWeek = result[promo].intChangeThisWeek.toFixed(0);
								}
								else if(result[promo].thisWeek > 0 && result[promo].lastWeek == 0) {
									result[promo].intChangeThisWeek = 100;
								}
								else if(result[promo].thisWeek == 0 && result[promo].lastWeek > 0) {
									result[promo].intChangeThisWeek = -100;
								}
							}

							allPromos = result;

							if(chosenLocation != -1) {
								var lat = "";
								var lon = "";
								for(var pl in promoLocations) {
									if(promoLocations[pl].location_id == chosenLocation) {
										lat = promoLocations[pl].latitude;
										lon = promoLocations[pl].longitude;
										break;
									}
								}

								PromoPriority.getAllByLocationId(chosenLocation, function(err, promoPriority) {
									var pp = [];
									var promos = [];

									for(var a in allPromos) {
										for(var p in promoPriority) {
											if(promoPriority[p].promo_id == allPromos[a].promo_id) {
												var temp = { ...allPromos[a] };
												temp.priority = promoPriority[p].priority;
												pp.push(temp);
											}
										}

										if((allPromos[a].latitude == null || (allPromos[a].latitude == lat && allPromos[a].longitude == lon)) && !promos.some(el => el.promo_id === allPromos[a].promo_id) && !pp.some(el => el.promo_id === allPromos[a].promo_id)) {
											var temp = { ...allPromos[a] };
											temp.priority = "-";
											promos.push(temp);
										}
									}

									pp.sort((a, b) => (a.priority > b.priority) ? 1 : -1);
									promos.sort((a, b) => (a.place > b.place) ? 1 : -1);

									for(var x in promos) {
										pp.push(promos[x]);
									}

									currentPromos = pp;
									
									res.render('admin', {
										showLogin: !req.session.authenticated,
										adminUsername: req.session.adminUsername,
										suppliers: allSuppliers,
										showPromos: true,
										promos: pp,
										thisWeek: thisWeek,
										lastWeek: lastWeek,
										promoAnalytics: JSON.stringify(promoAnalytics),
										promoLocations: promoLocations,
										chosenLocation: chosenLocation
									});
								});
							}
							else {
								currentPromos = result;

								res.render('admin', {
									showLogin: !req.session.authenticated,
									adminUsername: req.session.adminUsername,
									suppliers: allSuppliers,
									showPromos: true,
									promos: allPromos,
									thisWeek: thisWeek,
									lastWeek: lastWeek,
									promoAnalytics: JSON.stringify(promoAnalytics),
									promoLocations: promoLocations,
									chosenLocation: chosenLocation
								});
							}
						});
					});
				});
			}
			else {
				var lat = "";
				var lon = "";
				for(var pl in promoLocations) {
					if(promoLocations[pl].location_id == chosenLocation) {
						lat = promoLocations[pl].latitude;
						lon = promoLocations[pl].longitude;
						break;
					}
				}

				PromoPriority.getAllByLocationId(chosenLocation, function(err, promoPriority) {
					var pp = [];
					var promos = [];

					for(var a in allPromos) {
						for(var p in promoPriority) {
							if(promoPriority[p].promo_id == allPromos[a].promo_id) {
								var temp = { ...allPromos[a] };
								temp.priority = promoPriority[p].priority;
								pp.push(temp);
							}
						}
						

						if((allPromos[a].latitude == null || (allPromos[a].latitude == lat && allPromos[a].longitude == lon)) && !promos.some(el => el.promo_id === allPromos[a].promo_id) && !pp.some(el => el.promo_id === allPromos[a].promo_id)) {
							var temp = { ...allPromos[a] };
							temp.priority = "-";
							promos.push(temp);
						}
					}

					pp.sort((a, b) => (a.priority > b.priority) ? 1 : -1);
					promos.sort((a, b) => (a.place > b.place) ? 1 : -1);

					for(var x in promos) {
						pp.push(promos[x]);
					}

					currentPromos = pp;

					res.render('admin', {
						showLogin: !req.session.authenticated,
						adminUsername: req.session.adminUsername,
						suppliers: allSuppliers,
						showPromos: true,
						promos: pp,
						thisWeek: thisWeek,
						lastWeek: lastWeek,
						promoAnalytics: JSON.stringify(promoAnalytics),
						promoLocations: promoLocations,
						chosenLocation: chosenLocation
					});
				});
			}
		});
	});
}



/* SUPPLIERS */

exports.validateSupplier = function(req, res) {
	var err = [];
	var supplierId = req.query.supplierId;
	var img = req.query.img;
	var name = req.query.name;
	var username = req.query.username;
	var email = req.query.email;
	var phone = req.query.phone;

	Supplier.getAllByUsername(username, function(errUsername, supUsername) {
		if((supUsername && supplierId == "") || (supUsername && supUsername.supplier_id != supplierId))
			err.push("Username already exists");

		Supplier.getAllByEmail(email, function(errEmail, supEmail) {
			if((supEmail && supplierId == "") || (supEmail && supEmail.supplier_id != supplierId))
				err.push("Email already exists");

			res.send(err);
		});
	});
}

exports.saveSupplier = function(req, res) {
	var img = "";
	var supplier_id = req.body.supplier_id;
	var name = req.body.name;
	var username = req.body.username;
	var email = req.body.email;
	var phone = req.body.phone;

	phone = phone.replace("(", "").replace(")", "").replace(" ", "").replace("-", "").replace("_", "");
	
	if(req.files && req.files.supplierImg) {
		var supplierImg = req.files.supplierImg;
		//img = supplierImg.name;
		var supId = supplier_id == null || supplier_id == "" ? Math.floor(Math.random() * 1000000000) : supplier_id;
		var ext = supplierImg.name.substring(supplierImg.name.indexOf("."));
		img = "supplierimg"+supId+ext;

		var path = __dirname + '/../public/img/est-img/' + supplierImg.name;
		supplierImg.mv(path, function(err) {
		    if (err)
		      	return res.status(500).send(err);

		    // Create S3 service object
			s3 = new AWS.S3({apiVersion: '2006-03-01'});

			// Configure the file stream and obtain the upload parameters
			var fileStream = fs.createReadStream(path);
			fileStream.on('error', function(err) {
			  console.log('File Error', err);
			});

			// call S3 to retrieve upload file to specified bucket
			var uploadParams = {Bucket: 'est-img', Key: img, Body: fileStream, ACL: 'public-read'};

			// call S3 to retrieve upload file to specified bucket
			s3.upload(uploadParams, function (err, data) {
			  if (err) {
			    console.log("Error", err);
			  } if (data) {
			    console.log("Upload Success", data.Location);
			  }
			});
		});
	}
	else {
		img = req.body.supplierImgOriginal;
	}

	if(supplier_id == null || supplier_id == "") {
		const supplier = {
			SUPPLIER: {
				user_type_ref_id: 305,
				name: name,
				username: username,
				email: email,
				phone_num: phone,
				profile_pic: img,
				is_active: 0,
				needs_to_pay: 1,
				created_at: Common.newDate(req.session.tz),
				updated_at: Common.newDate(req.session.tz)
			}
		}

		Supplier.insert(supplier, function(errInsert, userInserted) {
			initSuppliersPromos(req, res);
		});
	}
	else {
		const supplier = {
			SUPPLIER: {
				supplier_id: supplier_id,
				name: name,
				username: username,
				email: email,
				phone_num: phone,
				profile_pic: img,
				updated_at: Common.newDate(req.session.tz)
			}
		}

		Supplier.update(supplier, function(errUpdate, userUpdated) {
			initSuppliersPromos(req, res);
		});
	}
}

exports.archiveSupplier = function(req, res) {
	Promo.getAllBySupplierId(req.body.supplierId, function(errPromos, promos) {
		// archive all associated promos
		archivePromos(promos, 0, req.session.tz);

		Supplier.getAllBySupplierId(req.body.supplierId, function(errSupplier, s) {
			const sa = {
				SUPPLIER_ARCHIVE: {
					supplier_id: s.supplier_id,
					user_type_ref_id: s.user_type_ref_id,
					name: s.name,
					username: s.username,
					email: s.email,
					phone_num: s.phone_num,
					profile_pic: s.profile_pic,
					stripe_id: s.stripe_id,
					claimed_at: s.claimed_at,
					archived_at: new Date(),
					created_at: s.created_at,
					updated_at: s.updated_at
				}
			}

			// add supplier to archives
			SupplierArchive.insert(sa, function(errInsert, resInsert) {
				//console.log(errInsert);
				//console.log(resInsert);

				// delete supplier from active suppliers
				Supplier.delete(req.body.supplierId, function(errDelete, resDelete) {
					//console.log(errDelete);
					//console.log(resDelete);
					res.send(true);
				});
			});
		});
	});
}

function archivePromos(promos, start, tz) {
	if(start < promos.length) {
		var promo = {
			PROMO: {
				promo_id: promos[start].promo_id,
				active: 0,
				archive_dt: Common.newDate(tz)
			}
		}

		Promo.update(promo, function(err, res) {
			//console.log(err);
			//console.log(res);
			archivePromos(promos, ++start);
		});
	}
	else {
		return;
	}
}

exports.supplierSendClaimEmail = function(req, res) {
	var ceKey = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
	
	fs.readFile(__dirname + '/../public/emails/sup_claim_email.html', function (err, html) {
	    if (!err) {
	        var msg = ""+html;

	        msg = msg.replace("{{ username }}", req.query.username);

	        while(msg.indexOf("{{ claimUrl }}") > -1) {
				msg = msg.replace("{{ claimUrl }}", "https://app.appyhourmobile.com/estlogin?email="+req.query.email+"&ceKey="+ceKey);
			}
		  
			var transporter = nodemailer.createTransport({
			    service: 'Gmail',
			    auth: {
			      user: 'appy-support@appyhourmobile.com',
			      pass: 'gpzkftethvbstvfn'
			    }
			});

			var mailOptions = {
			    from: 'AppyHour <appy-support@appyhourmobile.com>',
			    to: req.query.email,
			    subject: 'Claim Your Account',
			    html: msg
			};

			transporter.sendMail(mailOptions, function(error, info){
			    if (error) {
			      console.log(error);
			      res.send("Sorry, we're having some issues sending your message. Please try again later.");
			    } 
			    else {
			      console.log('Email sent: ' + info.response);
			      Supplier.getAllByEmail(req.query.email, function(errSupplier, supplier) {
			        if(!errSupplier && supplier) {
			          const s = { 
			            SUPPLIER: {
			              supplier_id: supplier.supplier_id, 
			              claim_email_key: ceKey,
			              claim_email_sent_at: Common.newDate(req.session.tz)
			            }
			          };
			          Supplier.update(s, function(errUpdateSupplier, updatedSupplier) {
			            if(!errUpdateSupplier && updatedSupplier) {
			              res.send("true");
			              //res.redirect('selectowner?owner=' + user.user_id + '&ownerHidden=');
			            }
			            else {
			              res.send("Sorry, we're having some issues sending your message. Please try again later.");
			            }
			          });
			        }
			      });
			    }
			});
	    }       
	});
}




/* PROMOS */

exports.getPromoDetails = function(req, res) {
	var promoId = req.query.promoId;
	var promo;

	if(allPromos.length > 0) {
		for(var p in allPromos) {
			if(allPromos[p].promo_id == promoId) {
				promo = allPromos[p];
				break;
			}
		}

		var presses = [];
		for(var d in promoAnalytics) {
			if(promoId == d) {
				var objKeys = Object.keys(promoAnalytics[d]);
				var objVals = Object.values(promoAnalytics[d]);
				for(var k in objKeys) {
					var impDate = new Date(objKeys[k]);
					impDate.setHours(0,0,0,0);

					if(promo.archive_dt == null || promo.archive_dt == "") {
						if(impDate.getTime() >= promo.created_at.getTime()) {
							presses.push({ date: objKeys[k], presses: objVals[k] });
						}
					}
					else {
						if(impDate.getTime() >= promo.created_at.getTime() && impDate.getTime() <= promo.archive_dt.getTime()) {
							presses.push({ date: objKeys[k], presses: objVals[k] });
						}
					}
				}
			}
		}

		res.render('admin', {
			showLogin: !req.session.authenticated,
			adminUsername: req.session.adminUsername,
			showPromoDetails: true,
			promoAnalytics: promoAnalytics,
			presses: presses,
			promo: promo
		});
	}
	else {
		getPromo(promoId, req.session.tz, function(p) {
			if(p != null) {
				promo = p;

				res.render('admin', {
					showLogin: !req.session.authenticated,
					adminUsername: req.session.adminUsername,
					showPromoDetails: true,
					promoAnalytics: promoAnalytics,
					presses: promo.presses,
					promo: promo
				});
			}
			else {
				res.render('admin', {
					showLogin: !req.session.authenticated,
					adminUsername: req.session.adminUsername,
					showPromoDetails: true,
					promoAnalytics: [],
					presses: [],
					promo: {}
				});
			}
		});
	}
};

function getPromo(promoId, tz, _callback) {
	Promo.getAllByPromoId(promoId, function(err, promo) {
		Location.getAll(function(errLoc, locations) {
			if(promo) {
				if(promo.supplier_name == null)
					promo.supplier_name = "-";

				if(promo.active == 0)
					promo.activeYN = "No";
				else
					promo.activeYN = "Yes";

				if(promo.active == 1 && (promo.supplier_promo_id == null || (promo.supplier_promo_id != null && promo.approved_at != null))) {
					promo.status = "Active";
					promo.place = 1;
				}
				else if(promo.active == 0 && (promo.supplier_promo_id == null || (promo.supplier_promo_id != null && promo.approved_at != null))) {
					promo.status = "Inactive";
					promo.place = 2;
				}
				else if(promo.supplier_promo_id != null && promo.approved_at == null && promo.denied_at == null) {
					promo.status = "Pending Review";
					promo.place = 0;
				}
				else if(promo.supplier_promo_id != null && promo.approved_at == null && promo.denied_at != null) {
					promo.status = "Denied";
					promo.place = 3;
				}
				else {
					promo.status = "";
					promo.place = 4;
				}

				promo.dayStr = intDayToString(promo.day);

				//promo.location = getLocationFromCoords(promo.latitude, promo.longitude);
				for(var l in locations) {
					if(locations[l].latitude == promo.latitude) {
						promo.location = locations[l].location_id;
						promo.locationStr = locations[l].location;
						break;
					}
				}

				promo.createdAt = promo.created_at.toString();

				if(promo.archive_dt != null) {
					promo.archived = promo.archive_dt.toString();
					promo.place = 5;
				}
				else {
					promo.archived = null;
				}

				//console.log(promo.archived);

				promo.thisWeek = 0;
				promo.lastWeek = 0;
				promo.prevWeek = 0;
				promo.allWeeks = 0;
				promo.intChangeThisWeek = 0;
				promo.intChangeLastWeek = 0;

				promo.priority = "-";
			
				var toDate = getYYYYMMDD(Common.newDate(tz));

				var curr = Common.newDate(tz);
				curr.setHours(0,0,0,0);

				// last 7 days
				curr.setDate(curr.getDate()-1);
				var bb = new Date(curr.getTime());
				curr.setDate(curr.getDate()-6);
				var aa = new Date(curr.getTime());

				// 2 weeks ago
				curr.setDate(curr.getDate()-1);
				var dd = new Date(curr.getTime());
				curr.setDate(curr.getDate()-6);
				var cc = new Date(curr.getTime());

				// 3 weeks ago
				curr.setDate(curr.getDate()-1);
				var ff = new Date(curr.getTime());
				curr.setDate(curr.getDate()-6);
				var ee = new Date(curr.getTime());

				thisWeek = intMonthToStringAbbr(aa.getMonth()) + " " + aa.getDate() + " - " + intMonthToStringAbbr(bb.getMonth()) + " " + bb.getDate();
				lastWeek = intMonthToStringAbbr(cc.getMonth()) + " " + cc.getDate() + " - " + intMonthToStringAbbr(dd.getMonth()) + " " + dd.getDate();

				panel.eventProperties({
				  	event: "Promo pressed",
				  	name: "promo_id",
				  	type: "general",
				  	unit: "day",
				  	from_date: "2020-08-01",
				  	to_date: toDate
				}).then(function(data) {
					var vals = data.data.values;
					//console.log(vals);

					promo.presses = [];
					for(var d in vals) {
						if(promoId == d) {
							var objKeys = Object.keys(vals[d]);
							var objVals = Object.values(vals[d]);
							for(var k in objKeys) {
								var impDate = new Date(objKeys[k]);
								impDate.setHours(0,0,0,0);

								if(promo.archive_dt == null || promo.archive_dt == "") {
									if(impDate.getTime() >= promo.created_at.getTime()) {
										promo.presses.push({ date: objKeys[k], presses: objVals[k] });
									}
								}
								else {
									if(impDate.getTime() >= promo.created_at.getTime() && impDate.getTime() <= promo.archive_dt.getTime()) {
										promo.presses.push({ date: objKeys[k], presses: objVals[k] });
									}
								}
							}
						}
					}

					/*
					for(var d in vals) {
						//console.log(d);
						var objKeys = Object.keys(vals[d]);
						var objVals = Object.values(vals[d]);
						//console.log(objKeys);
						//console.log(objVals);

						for(var k in objKeys) {
							var dayDt = new Date(objKeys[k] + " 00:00:00");
							
							if(promo.promo_id == d) {
								promo.allWeeks += parseInt(objVals[k]);
								
								if(dayDt.getTime() >= aa.getTime() && dayDt.getTime() <= bb.getTime()) {
									promo.thisWeek += parseInt(objVals[k]);
								}
							 	else if(dayDt.getTime() >= cc.getTime() && dayDt.getTime() <= dd.getTime()) {
									promo.lastWeek += parseInt(objVals[k]);
								}
								else if(dayDt.getTime() >= ee.getTime() && dayDt.getTime() <= ff.getTime()) {
									promo.prevWeek += parseInt(objVals[k]);
								}
							}
						}
					}

					if(promo.lastWeek > 0 && promo.prevWeek > 0) {
						promo.intChangeLastWeek = ((promo.lastWeek - promo.prevWeek) / promo.prevWeek) * 100;
						promo.intChangeLastWeek = promo.intChangeLastWeek.toFixed(0);
					}
					else if(promo.lastWeek > 0 && promo.prevWeek == 0) {
						result[promo].intChangeLastWeek = 100;
					}
					else if(promo.lastWeek == 0 && promo.prevWeek > 0) {
						result[promo].intChangeLastWeek = -100;
					}

					if(promo.thisWeek > 0 && promo.lastWeek > 0) {
						promo.intChangeThisWeek = ((promo.thisWeek - promo.lastWeek) / promo.lastWeek) * 100;
						promo.intChangeThisWeek = promo.intChangeThisWeek.toFixed(0);
					}
					else if(promo.thisWeek > 0 && promo.lastWeek == 0) {
						promo.intChangeThisWeek = 100;
					}
					else if(promo.thisWeek == 0 && promo.lastWeek > 0) {
						promo.intChangeThisWeek = -100;
					}*/
					
					_callback(promo);
				});
			}
			else {
				_callback(null)
			}
		});
	});
}

exports.savePromo = function(req, res) {
	/* LOGISTICS */

	var campaignType = req.body.campaignType;
	var keywords = req.body.keywords;
	var features = "";
	var day = "";
	var location = req.body.location;
	var active = req.body.active;

	// establishment features
	if(req.body.patio != undefined)
		features = features + "patio;";
	if(req.body.rooftop != undefined)
		features = features + "rooftop;";
	if(req.body.dogs != undefined)
		features = features + "dog_friendly;";
	if(req.body.brunch != undefined)
		features = features + "brunch;";
	if(req.body.delivery != undefined)
		features = features + "delivery;";
	if(req.body.takeout != undefined)
		features = features + "takeout;";
	if(req.body.blackowned != undefined)
		features = features + "black_owned;";
	if(req.body.naoptions != undefined)
		features = features + "na_options;";
	if(req.body.paying != undefined)
		features = features + "paying;";

	// special features
	if(req.body.carryout != undefined)
		features = features + "takeout;";

	// remove last semi-colon
	if(features != "")
		features = features.substring(0, features.length-1); 

	// keywords
	var keywordsArr = [];
	if(keywords != "") {
		keywordsArr = keywords.split(",");
		keywords = "";
		for(var k in keywordsArr)
			keywords += keywordsArr[k] + ";";
		keywords = keywords.substring(0, keywords.length-1); 
	}

	// day(s)
	if(req.body.everyday != "true") {
		if(req.body.sun == "true")
			day += "0,";
		if(req.body.mon == "true")
			day += "1,";
		if(req.body.tue == "true")
			day += "2,";
		if(req.body.wed == "true")
			day += "3,";
		if(req.body.thu == "true")
			day += "4,";
		if(req.body.fri == "true")
			day += "5,";
		if(req.body.sat == "true")
			day += "6,";
		
		day = day.substring(0, day.length-1);
	}
	else {
		day = -1;
	}

	
	/* DESIGN - DEAL FEED */

	var img = "";
	var textOverlay = req.body.textOverlay;
	var title = req.body.title;
	var label = req.body.label;
	var textColor = req.body.textColor;
	var textAlign = req.body.textAlign;
	var vertAlign = req.body.vertAlign;

	if(textOverlay == "N") {
		title = "";
		label = "";
		textColor = "white";
		textAlign = "left";
		vertAlign = "top";
	}


	/* DESIGN - DETAILS VIEW */

	var detailsImg = "";
	var detailsTextOverlay = req.body.detailsTextOverlay;
	var description = req.body.description;
	var detailsTextColor = req.body.detailsTextColor;

	if(detailsTextOverlay == "N") {
		description = "";
		detailsTextColor = "white";
	}

	var detailsActionBtn = req.body.detailsActionBtn;
	var link = req.body.link;
	var linkText = req.body.linkText;
	var linkTextColor = req.body.linkTextColor;
	var linkBgColor = req.body.linkBgColor;

	var ctaLabel = req.body.ctaLabel;
	var ctaLabelTextColor = req.body.ctaLabelTextColor;
	var ctaLabelBgColor = req.body.ctaLabelBgColor;

	if(detailsActionBtn == "N") {
		link = null;
		linkText = null;
		linkTextColor = null;
		linkBgColor = null;
		ctaLabel = null;
		ctaLabelTextColor = null;
		ctaLabelBgColor = null;
	}

	uploadCampaignImages(req.files, req.body.promo_id, function(imgNames) {
		img = imgNames.img != "" ? imgNames.img : req.body.campaignImgOriginal;
		detailsImg = imgNames.detailsImg != "" ? imgNames.detailsImg : req.body.campaignDetailsImgOriginal;

		Location.getAllByLocationId(location, function(err, loc) {
			// add promo
			if(req.body.promo_id == '') {
				const promo = {
				    PROMO: {
				      title: title,
				      label: label,
				      description: description,
				      day: day,
				      latitude: loc.latitude,
				      longitude: loc.longitude,
				      promo_type: campaignType,
				      keywords: keywords,
				      features: features,
				      background: img,
				      fontColor: textColor,
				      textAlign: textAlign,
				      verticalAlign: vertAlign,
				      details_background: detailsImg,
				      details_fontColor: detailsTextColor,
				      link: link,
				      link_text: ctaLabel,
				      link_text_color: ctaLabelTextColor,
				      link_bg_color: ctaLabelBgColor,
				      link_btn_text: linkText,
				      link_btn_text_color: linkTextColor,
				      link_btn_bg_color: linkBgColor,
				      active: active
				    }
				};

				Promo.insert(promo, function(err1, result1) {
					Promo.getAllByPromoId(result1.insertId, function(err2, result) {
						if(result.supplier_name == null)
							result.supplier_name = "-";

						if(result.active == 0)
							result.activeYN = "No";
						else
							result.activeYN = "Yes";

						if(result.active == 1 && (result.supplier_promo_id == null || (result.supplier_promo_id != null && result.approved_at != null)))
							result.status = "Active";
						else if(result.active == 0 && (result.supplier_promo_id == null || (result.supplier_promo_id != null && result.approved_at != null)))
							result.status = "Inactive";
						else if(result.supplier_promo_id != null && result.approved_at == null && result.denied_at == null)
							result.status = "Pending Review";
						else if(result.supplier_promo_id != null && result.approved_at == null && result.denied_at != null)
							result.status = "Denied";
						else
							result.status = "";

						result.dayStr = intDayToString(result.day);
						result.location = location;
						
						result.thisWeek = 0;
						result.lastWeek = 0;
						result.prevWeek = 0;
						result.allWeeks = 0;
						result.intChangeThisWeek = 0;
						result.intChangeLastWeek = 0;
						result.priority = "-";

						if(result.active == 1) {
							if(location > 0) {
								PromoPriority.getNextPriorityForLocationId(location, function(err3, nextPriority) {
									const promoPriority = {
										PROMO_PRIORITY: {
											location_id: location,
											priority: nextPriority.max,
											promo_id: result1.insertId
										}
									}

									PromoPriority.insert(promoPriority, function(err4, result4) {
										allPromos.push(result);
									
										initSuppliersPromos(req, res);
										/*
										res.render('admin', {
											showLogin: !req.session.authenticated,
											adminUsername: req.session.adminUsername,
											suppliers: allSuppliers,
											showPromos: true,
											promos: allPromos,
											thisWeek: thisWeek,
											lastWeek: lastWeek,
											promoAnalytics: JSON.stringify(promoAnalytics),
											promoLocations: promoLocations,
											chosenLocation: -1
										});
										*/
									});
								});
							}
							else {
								insertPriorityForAllLocations(result1.insertId, 0, req, res);
							}
						}
						else {
							allPromos.push(result);
							initSuppliersPromos(req, res);
							/*
							res.render('admin', {
								showLogin: !req.session.authenticated,
								adminUsername: req.session.adminUsername,
								suppliers: allSuppliers,
								showPromos: true,
								promos: allPromos,
								thisWeek: thisWeek,
								lastWeek: lastWeek,
								promoAnalytics: JSON.stringify(promoAnalytics),
								promoLocations: promoLocations,
								chosenLocation: -1
							});
							*/
						}
					});
				});
			}
			// edit promo
			else {
				const promo = {
				    PROMO: {
				      promo_id: req.body.promo_id,
				      title: title,
				      label: label,
				      description: description,
				      day: day,
				      latitude: loc.latitude,
				      longitude: loc.longitude,
				      promo_type: campaignType,
				      keywords: keywords,
				      features: features,
				      background: img,
				      fontColor: textColor,
				      textAlign: textAlign,
				      verticalAlign: vertAlign,
				      details_background: detailsImg,
				      details_fontColor: detailsTextColor,
				      link: link,
				      link_text: ctaLabel,
				      link_text_color: ctaLabelTextColor,
				      link_bg_color: ctaLabelBgColor,
				      link_btn_text: linkText,
				      link_btn_text_color: linkTextColor,
				      link_btn_bg_color: linkBgColor,
				      active: active
				    }
				};

				Promo.update(promo, function(err1, result1) {
					Promo.getAllByPromoId(req.body.promo_id, function(err2, newPromo) {
						for(var a in allPromos) {
							if(allPromos[a].promo_id == newPromo.promo_id) {
								// persist analytics
								newPromo.thisWeek = allPromos[a].thisWeek;
								newPromo.lastWeek = allPromos[a].lastWeek;
								newPromo.prevWeek = allPromos[a].prevWeek;
								newPromo.allWeeks = allPromos[a].allWeeks;
								newPromo.intChangeThisWeek = allPromos[a].intChangeThisWeek;
								newPromo.intChangeLastWeek = allPromos[a].intChangeLastWeek;
								newPromo.createdAt = allPromos[a].createdAt;

								allPromos[a] = newPromo;

						      	if(allPromos[a].supplier_name == null)
						      		allPromos[a].supplier_name = "-";

						      	if(allPromos[a].active == 0)
									allPromos[a].activeYN = "No";
								else
									allPromos[a].activeYN = "Yes";

								if(allPromos[a].active == 1 && (allPromos[a].supplier_promo_id == null || (allPromos[a].supplier_promo_id != null && allPromos[a].approved_at != null)))
									allPromos[a].status = "Active";
								else if(allPromos[a].active == 0 && (allPromos[a].supplier_promo_id == null || (allPromos[a].supplier_promo_id != null && allPromos[a].approved_at != null)))
									allPromos[a].status = "Inactive";
								else if(allPromos[a].supplier_promo_id != null && allPromos[a].approved_at == null && allPromos[a].denied_at == null)
									allPromos[a].status = "Pending Review";
								else if(allPromos[a].supplier_promo_id != null && allPromos[a].approved_at == null && allPromos[a].denied_at != null)
									allPromos[a].status = "Denied";
								else
									allPromos[a].status = "";

								allPromos[a].dayStr = intDayToString(allPromos[a].day);
								allPromos[a].location = location;

						      	if(allPromos[a].active == 1) {
						      		PromoPriority.getAllByPromoId(allPromos[a].promo_id, function(errPP, pp) {
						      			if(pp.length == 0) {
						      				if(location > 0) {
									      		PromoPriority.getNextPriorityForLocationId(location, function(err3, nextPriority) {
													const promoPriority = {
														PROMO_PRIORITY: {
															location_id: location,
															priority: nextPriority.max,
															promo_id: req.body.promo_id
														}
													}

													PromoPriority.insert(promoPriority, function(err4, result4) {
														req.query.location = chosenLocation;
														initSuppliersPromos(req, res);
													});
												});
								      		}
								      		else {
								      			insertPriorityForAllLocations(req.body.promo_id, 0, req, res);
								      		}
						      			}
						      			else {
						      				req.query.location = chosenLocation;
						      				initSuppliersPromos(req, res);
						      			}
						      		});
						      		
						      	}
						      	else if(allPromos[a].active == 0) {
						      		allPromos[a].priority = "-";

						      		PromoPriority.deleteAllByPromoId(req.body.promo_id, function(err3, result3) {
						      			req.query.location = chosenLocation;
						      			initSuppliersPromos(req, res);
						      		});
						      	}

						      	break;
							}
						}
					});
				});
			}
		});
	});
};

function uploadCampaignImages(files, promoId, _callback) {
	var img = "";
	var detailsImg = "";

	if(Object.keys(files).length !== 0 && files.constructor === Object) {
		if(files.campaignImg && files.campaignDetailsImg) {
			uploadCampaignImage(files.campaignImg, promoId, function(campaignImgName) {
				img = campaignImgName;
				uploadCampaignImage(files.campaignDetailsImg, "details"+promoId, function(campaignDetailsImgName) {
					detailsImg = campaignDetailsImgName;
					_callback({ "img": img, "detailsImg": detailsImg });
				});
			});
		}
		else if(files.campaignImg) {
			uploadCampaignImage(files.campaignImg, promoId, function(name) {
				img = name;
				_callback({ "img": img, "detailsImg": detailsImg });
			});
		}
		else if(files.campaignDetailsImg) {
			uploadCampaignImage(files.campaignDetailsImg, "details"+promoId, function(name) {
				detailsImg = name;
				_callback({ "img": img, "detailsImg": detailsImg });
			});
		}
	}
	else {
		_callback({ "img": img, "detailsImg": detailsImg });
	}
}

function uploadCampaignImage(img, promoId, _callback) {
	var rand = promoId == "" ? Math.floor(Math.random() * 1000000000) : promoId;
	var ext = img.name.substring(img.name.indexOf("."));
	var imgname = "campaignimg"+rand+ext;

	var path = __dirname + '/../public/img/appy-promos/' + img.name;
	img.mv(path, function(err) {
	    if (err)
	      	return res.status(500).send(err);

	    // Create S3 service object
		s3 = new AWS.S3({apiVersion: '2006-03-01'});

		// Configure the file stream and obtain the upload parameters
		var fileStream = fs.createReadStream(path);
		fileStream.on('error', function(err) {
		  console.log('File Error', err);
		});

		// call S3 to retrieve upload file to specified bucket
		var uploadParams = {Bucket: 'appy-promos', Key: imgname, Body: fileStream, ACL: 'public-read'};

		// call S3 to retrieve upload file to specified bucket
		s3.upload(uploadParams, function (err, data) {
		  if (err) {
		    console.log("Error", err);
		  } 
		  if (data) {
		    console.log("Upload Success", data.Location);
		    _callback(imgname);
		  }
		});
	});
}

exports.deletePromo = function(req, res) {
	//Promo.delete(req.body.promoId, function(err, result) {

	var arcDt = new Date();

	const promo = {
  		PROMO: {
  		  promo_id: req.body.promoId,
  		  active: 0,
  		  archive_dt: arcDt
  		}
  	};

  	Promo.update(promo, function(err, result) {
		PromoPriority.deleteAllByPromoId(req.body.promoId, function(err2, result2) {
			for(var a in allPromos) {
				//console.log(allPromos[a].promo_id + " == " + req.body.promoId);
				if(allPromos[a].promo_id == req.body.promoId) {
					//console.log("HERE");
					allPromos[a].archived = arcDt.toString();
					res.send(true);
					break;
				}
			}
  		});
  	});
};

exports.approvePromo = function(req, res) {
	var promoId = req.body.promoId;
	var now = Common.newDate(req.session.tz);

	SupplierPromo.getAllByPromoId(promoId, function(err, supPromo) {
		if(err == null && supPromo) {
			const supplier_promo = {
				SUPPLIER_PROMO: {
					supplier_promo_id: supPromo.supplier_promo_id,
					approved_at: now,
					approved_by: "admin"
				}
			}

			Supplier.getAllBySupplierId(supPromo.supplier_id, function(errSupplier, supplier) {
				if(errSupplier == null && supplier) {
					SupplierPromo.update(supplier_promo, function(errUpdate, resUpdate) {
						if(errUpdate == null && resUpdate) {
							const promo = {
								PROMO: {
									promo_id: promoId,
									active: 0 //0 - owner must activate, 1 - auto set to active
								}
							}

							Promo.update(promo, function(errPromo, resPromo) {
								fs.readFile(__dirname + '/../public/emails/campaign_approved_email.html', function (err, html) {
								    if (!err) {
								        var msg = ""+html;
							        	
							        	var transporter = nodemailer.createTransport({
									      service: 'Gmail',
									      auth: {
									        user: 'appy-support@appyhourmobile.com',
									        pass: 'gpzkftethvbstvfn'
									      }
									    });

									    var mailOptions = {
									      from: 'AppyHour <appy-support@appyhourmobile.com>',
									      to: supplier.email,
									      subject: 'Campaign Approved',
									      html: msg
									    };

									    transporter.sendMail(mailOptions, function(error, info){
									      if (error) {
									        console.log(error);
									        //res.send("Sorry, we're having some issues sending your message. Please try again later.");
									      } else {
									        console.log('Email sent: ' + info.response);
									        //res.send("true");
									      }
									    });
									}
								});

								res.send(errPromo == null && resPromo);
							});
						}
						else {
							res.send(false);
						}
					});
				}
				else {
					res.send(false);
				}
			});
		}
		else {
			res.send(false);
		}
	});
}

exports.denyPromo = function(req, res) {
	var promoId = req.body.promoId;
	var reason = req.body.reason;
	var now = Common.newDate(req.session.tz);

	SupplierPromo.getAllByPromoId(promoId, function(err, supPromo) {
		if(err == null && supPromo) {
			const supplier_promo = {
				SUPPLIER_PROMO: {
					supplier_promo_id: supPromo.supplier_promo_id,
					denied_at: now,
					denied_by: "admin",
					denied_reason: reason
				}
			}

			Supplier.getAllBySupplierId(supPromo.supplier_id, function(errSupplier, supplier) {
				if(errSupplier == null && supplier) {

					SupplierPromo.update(supplier_promo, function(errUpdate, resUpdate) {
						if(errUpdate == null && resUpdate) {
							const promo = {
								PROMO: {
									promo_id: promoId,
									active: 0 
								}
							}

							Promo.update(promo, function(errPromo, resPromo) {
								fs.readFile(__dirname + '/../public/emails/campaign_denied_email.html', function (err, html) {
								    if (!err) {
								        var msg = ""+html;
								        msg = msg.replace('{{ denied_reason }}', reason);
							        	
							        	var transporter = nodemailer.createTransport({
									      service: 'Gmail',
									      auth: {
									        user: 'appy-support@appyhourmobile.com',
									        pass: 'gpzkftethvbstvfn'
									      }
									    });

									    var mailOptions = {
									      from: 'AppyHour <appy-support@appyhourmobile.com>',
									      to: supplier.email,
									      subject: 'Campaign Denied',
									      html: msg
									    };

									    transporter.sendMail(mailOptions, function(error, info){
									      if (error) {
									        console.log(error);
									        //res.send("Sorry, we're having some issues sending your message. Please try again later.");
									      } else {
									        console.log('Email sent: ' + info.response);
									        //res.send("true");
									      }
									    });
									}
								});

								res.send(errPromo == null && resPromo);
							});
						}
						else {
							res.send(false);
						}
					});
				}
				else {
					res.send(false);
				}
			});
		}
		else {
			res.send(false);
		}
	});
}

exports.getPromosForLocation = function(req, res) {
	chosenLocation = req.body.locationId;
	getPromosForLocationId(chosenLocation, req, res);
};

exports.savePromoPriority = function(req, res) {
	var locationId = req.body.locationId;
	var priorities = JSON.parse(req.body.priorities);
	var promoIds = JSON.parse(req.body.promoIds);

	PromoPriority.deleteAllByLocationId(locationId, function(err, result) {
		if(result.affectedRows > 0) {
			var promoPriorityArr = [];

			for(var i = 0; i < promoIds.length; i++) {
				var promoPriority = {
					PROMO_PRIORITY: {
						location_id: locationId,
						promo_id: promoIds[i],
						priority: priorities[i]
					}
				};
				promoPriorityArr.push(promoPriority);
			}

			insertPromoPriority(promoPriorityArr, 0);

			res.send(true);
		}
		else {
			res.send(false);
		}
	});
};

function insertPromoPriority(promoPriorityArr, start) {
	if(start < promoPriorityArr.length) {
		PromoPriority.insert(promoPriorityArr[start], function(err, result) {
			insertPromoPriority(promoPriorityArr, ++start);
		});
	}
	else {
		return true;
	}
}

function getPromosForLocationId(locationId, req, res) {
	if(locationId != -1) {
		var lat = "";
		var lon = "";
		for(var pl in promoLocations) {
			if(promoLocations[pl].location_id == locationId) {
				lat = promoLocations[pl].latitude;
				lon = promoLocations[pl].longitude;
				break;
			}
		}

		PromoPriority.getAllByLocationId(locationId, function(err, promoPriority) {
			var pp = [];
			var promos = [];

			for(var a in allPromos) {
				for(var p in promoPriority) {
					if(promoPriority[p].promo_id == allPromos[a].promo_id) {
						var temp = { ...allPromos[a] };
						temp.priority = promoPriority[p].priority;
						pp.push(temp);
					}
				}
				

				if((allPromos[a].latitude == null || (allPromos[a].latitude == lat && allPromos[a].longitude == lon)) && !promos.some(el => el.promo_id === allPromos[a].promo_id) && !pp.some(el => el.promo_id === allPromos[a].promo_id)) {
					var temp = { ...allPromos[a] };
					temp.priority = "-";
					promos.push(temp);
				}
			}

			pp.sort((a, b) => (a.priority > b.priority) ? 1 : -1);
			promos.sort((a, b) => (a.place > b.place) ? 1 : -1);

			for(var x in promos) {
				pp.push(promos[x]);
			}

			currentPromos = pp;

			res.render('admin', {
				showLogin: !req.session.authenticated,
				adminUsername: req.session.adminUsername,
				suppliers: allSuppliers,
				showPromos: true,
				promos: pp,
				thisWeek: thisWeek,
				lastWeek: lastWeek,
				promoAnalytics: JSON.stringify(promoAnalytics),
				promoLocations: promoLocations,
				chosenLocation: chosenLocation
			});
		});
	}
	else {
		currentPromos = allPromos;

		res.render('admin', {
			showLogin: !req.session.authenticated,
			adminUsername: req.session.adminUsername,
			suppliers: allSuppliers,
			showPromos: true,
			promos: allPromos,
			thisWeek: thisWeek,
			lastWeek: lastWeek,
			promoAnalytics: JSON.stringify(promoAnalytics),
			promoLocations: promoLocations,
			chosenLocation: chosenLocation
		});
	}
}

function insertPriorityForAllLocations(promoId, start, req, res) {
	if(start < promoLocations.length) {
		PromoPriority.getNextPriorityForLocationId(promoLocations[start].location_id, function(err3, nextPriority) {
			const promoPriority = {
				PROMO_PRIORITY: {
					location_id: promoLocations[start].location_id,
					priority: nextPriority.max,
					promo_id: promoId
				}
			}

			PromoPriority.insert(promoPriority, function(err4, result4) {
				insertPriorityForAllLocations(promoId, ++start, req, res);
			});
		});
	}
	else {
		req.query.location = chosenLocation;
		initSuppliersPromos(req, res);
	}
}





/* AUDIT */

selectedState = -1;
selectedZip = -1;

exports.showAudit = function(req, res) {
	selectedState = -1;
	if(req.query && req.query.state)
		selectedState = req.query.state;

	selectedZip = -1;
	if(req.query && req.query.zip)
		selectedZip = req.query.zip;

	Establishment.getAll(function(errEst, allEsts) {
		Special.getAllGrouped(function(errSpecial, allSpecials) {	
			Event.getAllGrouped(function(errEvent, allEvents) {
				var ests = [];
				var states = [];
				var zips = [];

				for(var e in allEsts) {
					if((selectedState == -1 || (selectedState != -1 && selectedState == allEsts[e].state)) &&
					   (selectedZip == -1 || (selectedZip != -1 && selectedZip == allEsts[e].zip))) {
						if(allEsts[e].imageUrl != null)
							allEsts[e].profile_pic = "https://s3.us-east-2.amazonaws.com/est-img/" + allEsts[e].imageUrl;
						else
							allEsts[e].profile_pic = "/img/est-img/establishment_no_image.png";

						allEsts[e].formatted_phone_num = formatPhone(allEsts[e].phone_num);

						allEsts[e].mon_open_formatted = timeStrToTime(allEsts[e].mon_open);
						allEsts[e].mon_close_formatted = timeStrToTime(allEsts[e].mon_close);
						allEsts[e].tue_open_formatted = timeStrToTime(allEsts[e].tue_open);
						allEsts[e].tue_close_formatted = timeStrToTime(allEsts[e].tue_close);
						allEsts[e].wed_open_formatted = timeStrToTime(allEsts[e].wed_open);
						allEsts[e].wed_close_formatted = timeStrToTime(allEsts[e].wed_close);
						allEsts[e].thu_open_formatted = timeStrToTime(allEsts[e].thu_open);
						allEsts[e].thu_close_formatted = timeStrToTime(allEsts[e].thu_close);
						allEsts[e].fri_open_formatted = timeStrToTime(allEsts[e].fri_open);
						allEsts[e].fri_close_formatted = timeStrToTime(allEsts[e].fri_close);
						allEsts[e].sat_open_formatted = timeStrToTime(allEsts[e].sat_open);
						allEsts[e].sat_close_formatted = timeStrToTime(allEsts[e].sat_close);
						allEsts[e].sun_open_formatted = timeStrToTime(allEsts[e].sun_open);
						allEsts[e].sun_close_formatted = timeStrToTime(allEsts[e].sun_close);

						var created = allEsts[e].est_created_at;
						created.subtractHoursDST(req.session.tz);
						var updated = allEsts[e].est_updated_at;
						updated.subtractHoursDST(req.session.tz);

						allEsts[e].created_at_formatted = dtToStr(created);
						allEsts[e].updated_at_formatted = dtToStr(updated);
						if(allEsts[e].created_at_formatted == allEsts[e].updated_at_formatted) {
							allEsts[e].updated_at_formatted = "-";
						}

						var claimedAt = allEsts[e].claimed_at;
						if(claimedAt) {
							claimedAt.subtractHoursDST(req.session.tz);
							allEsts[e].claimed_at_formatted = dtToStr(claimedAt);
						}
						else {
							allEsts[e].claimed_at_formatted = "-";
						}

						var claimEmailSent = allEsts[e].claim_email_sent_at;
						if(claimEmailSent) {
							claimEmailSent.subtractHoursDST(req.session.tz);
							allEsts[e].claim_email_sent_formatted = dtToStr(claimEmailSent);
						}
						else {
							allEsts[e].claim_email_sent_formatted = "-";
						}

						allEsts[e].specials = [];

						for(var s in allSpecials) {
							if(allSpecials[s].establishment_id == allEsts[e].establishment_id && allSpecials[s].event_id == null) {
								allSpecials[s].dealType = getDealTypeImageByRefId(allSpecials[s].deal_type_ref_id);
								allSpecials[s].startTimeStr = formatStartEndTime(allSpecials[s].start_time);
								allSpecials[s].endTimeStr = formatStartEndTime(allSpecials[s].end_time);
								allSpecials[s].duration = allSpecials[s].startTimeStr + " - " + allSpecials[s].endTimeStr;
								allSpecials[s].dayStr = getDaysString(allSpecials[s].days.split(","));
								allEsts[e].specials.push(allSpecials[s]);
							}
						}

						allEsts[e].events = [];

						for(var ev in allEvents) {
							if(allEvents[ev].establishment_id == allEsts[e].establishment_id) {
								var end = new Date(allEvents[ev].end_time);
								var curr = Common.newDate(req.session.tz);
								if(allEvents[ev].days != '-1' || (allEvents[ev].days == '-1' && end.getTime() >= curr.getTime())) {
									if(allEvents[ev].days != '-1') 
										allEvents[ev].date = getDaysString(allEvents[ev].days.split(","));
									else if(daysBetween(allEvents[ev].start_time, allEvents[ev].end_time) > 0)
										allEvents[ev].date = (parseInt(allEvents[ev].start_time.getMonth())+1) + "/" + allEvents[ev].start_time.getDate() + "/" + (parseInt(allEvents[ev].start_time.getYear())+1900) + " - " + (parseInt(allEvents[ev].end_time.getMonth())+1) + "/" + allEvents[ev].end_time.getDate() + "/" + (parseInt(allEvents[ev].end_time.getYear())+1900);
									else
										allEvents[ev].date = (parseInt(allEvents[ev].start_time.getMonth())+1) + "/" + allEvents[ev].start_time.getDate() + "/" + (parseInt(allEvents[ev].start_time.getYear())+1900);

									allEvents[ev].time = formatStartEndTime(allEvents[ev].start_time) + " - " + formatStartEndTime(allEvents[ev].end_time);

									var startMonth = parseInt(allEvents[ev].start_time.getMonth()) + 1;
									var startDt = parseInt(allEvents[ev].start_time.getDate());

									if(startMonth < 10)
										startMonth = "0" + startMonth;

									if(startDt < 10)
										startDt = "0" + startDt;

									var endMonth = parseInt(allEvents[ev].end_time.getMonth()) + 1;
									var endDt = parseInt(allEvents[ev].end_time.getDate());

									if(endMonth < 10)
										endMonth = "0" + endMonth;

									if(endDt < 10)
										endDt = "0" + endDt;

									allEvents[ev].start_date = startMonth + "/" + startDt + "/" + (parseInt(allEvents[ev].start_time.getYear())+1900);
									allEvents[ev].end_date = endMonth + "/" + endDt + "/" + (parseInt(allEvents[ev].end_time.getYear())+1900);
									allEvents[ev].startTimeStr = formatStartEndTime(allEvents[ev].start_time);
									allEvents[ev].endTimeStr = formatStartEndTime(allEvents[ev].end_time);

									allEvents[ev].specials = [];
									for(var s in allSpecials) {
										var eventIds = allEvents[ev].event_ids.split(",");

										if(allSpecials[s].event_id && eventIds.indexOf(""+allSpecials[s].event_id) > -1) {
											allSpecials[s].dealType = getDealTypeImageByRefId(allSpecials[s].deal_type_ref_id);
											allSpecials[s].duration = formatStartEndTime(allSpecials[s].start_time) + " - " + formatStartEndTime(allSpecials[s].end_time);

						                    if(allSpecials[s].price_type_ref_id == 300)
						                    	allSpecials[s].priceAndDetails = "$" + allSpecials[s].price.toFixed(2) + " " + allSpecials[s].details;
						                    else if(allSpecials[s].price_type_ref_id == 301)
						                    	allSpecials[s].priceAndDetails = allSpecials[s].price.toFixed(0) + "% Off " + allSpecials[s].details;
						                    else if(allSpecials[s].price_type_ref_id == 302)
						                    	allSpecials[s].priceAndDetails = "BOGO " + allSpecials[s].details;
						                    else if(allSpecials[s].price_type_ref_id == 307)
						                    	allSpecials[s].priceAndDetails = "Free " + allSpecials[s].details;

											allEvents[ev].specials.push(allSpecials[s]);
										}
									}

									if(allEvents[ev].days == '-1') {
										var es = {...allEvents[ev].specials};
										var especials = [];
										for(var i in es) {
											if(especials.length > 0) {
												var inArr = false;
												for(var x in especials) {
													if(especials[x].dealType == es[i].dealType && especials[x].priceAndDetails == es[i].priceAndDetails &&
													   especials[x].start_time.getHours() == es[i].start_time.getHours() && especials[x].start_time.getMinutes() == es[i].start_time.getMinutes() &&
													   especials[x].end_time.getHours() == es[i].end_time.getHours() && especials[x].end_time.getMinutes() == es[i].end_time.getMinutes()) {
														especials[x].date += ", " + (parseInt(es[i].start_time.getMonth())+1) + "/" + es[i].start_time.getDate();
														inArr = true;
													}
												}
												if(!inArr) {
													es[i].date = (parseInt(es[i].start_time.getMonth())+1) + "/" + es[i].start_time.getDate();
													especials.push(es[i]);
												}
											}
											else {
												es[i].date = (parseInt(es[i].start_time.getMonth())+1) + "/" + es[i].start_time.getDate();
												especials.push(es[i]);
											}
										}

										especials.sort((a, b) => (a.start_time > b.start_time) ? 1 : -1);

										for(var es in especials) {
											var dateLength = especials[es].date.split(",").length;
											var db = daysBetween(allEvents[ev].start_time, allEvents[ev].end_time) + 1;
											if(dateLength == 1) {
												especials[es].date = null;
											}
											else if(dateLength == db) {
												especials[es].date = "All Days";
											}
										}

										allEvents[ev].specials = especials;
									}

									allEsts[e].events.push(allEvents[ev]);
								}
							}
						}

						ests.push(allEsts[e]);
					}

					if(states.indexOf(allEsts[e].state) == -1)
						states.push(allEsts[e].state);

					if(selectedState == -1 || (selectedState != -1 && selectedState == allEsts[e].state && zips.indexOf(allEsts[e].zip) == -1))
						zips.push(allEsts[e].zip);
				}

				states.sort();
				zips.sort();

				//ests.sort((a, b) => (a.is_active < b.is_active || (a.is_active == b.is_active && a.name > b.name)) ? 1 : -1);

				var allStates = [];
				Ref.getAllByRefTypeId(24, function(err2, states50) {
					for(var s in states50) {
						allStates.push(states50[s].ref_code);
					}

					res.render('admin', {
						showLogin: !req.session.authenticated,
						adminUsername: req.session.adminUsername,
						showAudit: true,
						ests: ests,
						auditStates: states,
						auditZips: zips,
						selectedState: selectedState,
						selectedZip: selectedZip,
						allStates: allStates
					});
				});
			});
		});
	});
};

// VALIDATE EST
exports.auditValidateEst = function(req, res) {
	var errs = [];

	// owner
	/*var selOwnerId 	= req.query.ownerId;
	var email 		= req.query.email;
	var username 	= req.query.username;
	var phone 		= req.query.phone;
	var needsToPay  = req.query.needsToPay;

	if(username == "")
		errs.push("Enter an owner username");
	if(email == "")
		errs.push("Enter an owner email");
	if(needsToPay == "")
		errs.push("Decide if the owner needs to pay");*/

	// est - basic
	var estId 		= req.query.estId;
	var img 		= req.query.img;
	var name 		= req.query.name;
	var address_id 	= req.query.address_id;
	var address_1 	= req.query.address_1;
	var address_2 	= req.query.address_2;
	var city 		= req.query.city;
	var state 		= req.query.state;
	var zip 		= req.query.zip;
	var phone_num 	= req.query.phone_num.replace(/\D/g,'');
	var website 	= req.query.website;
	var resWebsite 	= req.query.resWebsite;
	var menu_link 	= req.query.menu_link;
	var hours_id 	= req.query.hours_id;
	var mon_open 	= deFormatHours(req.query.mon_open);
	var mon_close 	= deFormatHours(req.query.mon_close);
	var tue_open 	= deFormatHours(req.query.tue_open);
	var tue_close 	= deFormatHours(req.query.tue_close);
	var wed_open 	= deFormatHours(req.query.wed_open);
	var wed_close 	= deFormatHours(req.query.wed_close);
	var thu_open 	= deFormatHours(req.query.thu_open);
	var thu_close 	= deFormatHours(req.query.thu_close);
	var fri_open 	= deFormatHours(req.query.fri_open);
	var fri_close 	= deFormatHours(req.query.fri_close);
	var sat_open 	= deFormatHours(req.query.sat_open);
	var sat_close 	= deFormatHours(req.query.sat_close);
	var sun_open 	= deFormatHours(req.query.sun_open);
	var sun_close 	= deFormatHours(req.query.sun_close);

	if(img == "")
		errs.push("Choose a profile picture");
	if(name == "")
		errs.push("Enter a business name");
	if(address_1 == "" || city == "" || state == "" || zip == "")
		errs.push("Enter a valid address");
	if((mon_open == "" && mon_close != "") || (mon_open != "" && mon_close == ""))
		errs.push("Set valid hours for Monday");
	if((tue_open == "" && tue_close != "") || (tue_open != "" && tue_close == ""))
		errs.push("Set valid hours for Tuesday");
	if((wed_open == "" && wed_close != "") || (wed_open != "" && wed_close == ""))
		errs.push("Set valid hours for Wednesday");
	if((thu_open == "" && thu_close != "") || (thu_open != "" && thu_close == ""))
		errs.push("Set valid hours for Thursday");
	if((fri_open == "" && fri_close != "") || (fri_open != "" && fri_close == ""))
		errs.push("Set valid hours for Friday");
	if((sat_open == "" && sat_close != "") || (sat_open != "" && sat_close == ""))
		errs.push("Set valid hours for Saturday");
	if((sun_open == "" && sun_close != "") || (sun_open != "" && sun_close == ""))
		errs.push("Set valid hours for Sunday");

	// est - features
	var description = req.query.description;
	var delivery 	= (req.query.delivery == "Yes" ? 1 : 0);
	var takeout 	= (req.query.takeout == "Yes" ? 1 : 0);
	var patio 		= (req.query.patio == "Yes" ? 1 : 0);
	var rooftop 	= (req.query.rooftop == "Yes" ? 1 : 0);
	var brunch 		= (req.query.brunch == "Yes" ? 1 : 0);
	var dog 		= (req.query.dog == "Yes" ? 1 : 0);

	if(address_1 != "" && city != "" && state != "" && zip != "") {
		Ref.getAllByRefCode(state, function(errRef, ref) {
			if (errRef || ref == undefined) {
				errs.push("Internal error: " + errRef);
			}

			var state_code_ref_id = ref.ref_id;
			var address = address_1;
			if(address_2 != "")
				address += " " + address_2;
			address += ", " + city + ", " + state;

		  	Geocoder.geocode(address, function(errGeo, geo) {
		  		if (errGeo || geo[0] == undefined) {
		  			errs.push("Geocoder error: " + errGeo);
		  		}

		  		if(geo[0].city != city) {
		  			console.log("City does not match what Google found: " + geo[0].city);
			        //errs.push("City does not match what Google found: " + geo[0].city);
		  		}

		  		if(geo[0].zipcode != zip) {
		  			console.log("Zip does not match what Google found: " + geo[0].zipcode);
		  			//errs.push("Zip does not match what Google found: " + geo[0].zipcode);
		  		}

		  		res.send(errs);
		  		// check if username or email already exist
				/*if(username != "" && email != "") {
					User.getAllByUsername(username, function(err, user) {
						UserQueue.getAllByUsername(username, function(errUQ, userQ) {
							if((user && user.user_id != selOwnerId) || (userQ && userQ.user_id != selOwnerId)) {
								errs.push("Owner username already exists");
							}
							User.getAllByEmail(email, function(err2, user2) {
								UserQueue.getAllByEmail(email, function(errUQ2, userQ2) {
									if((user2 && user2.user_id != selOwnerId) || (userQ2 && userQ2.user_id != selOwnerId)) {
										errs.push("Owner email already exists");
									}
									res.send(errs);
								});
							});
						});
					});
				}
				else {
					res.send(errs);
				}*/
		  	});
		});
	}
	else {
		// check if username or email already exist
		/*if(username != "" && email != "") {
			User.getAllByUsername(username, function(err, user) {
				UserQueue.getAllByUsername(username, function(errUQ, userQ) {
					if((user && user.user_id != selOwnerId) || (userQ && userQ.user_id != selOwnerId)) {
						errs.push("Owner username already exists");
					}
					User.getAllByEmail(email, function(err2, user2) {
						UserQueue.getAllByEmail(email, function(errUQ2, userQ2) {
							if((user2 && user2.user_id != selOwnerId) || (userQ2 && userQ2.user_id != selOwnerId)) {
								errs.push("Owner email already exists");
							}
							res.send(errs);
						});
					});
				});
			});
		}
		else {
			res.send(errs);
		}*/

		res.send(errs);
	}
};

// EDIT EST
exports.auditSaveEst = function(req, res) {
	/*
	var owner_id = req.body.owner_id;
	var owner_username = req.body.owner_username;
	var owner_email = req.body.owner_email;
	var owner_phoneNumber = req.body.owner_phoneNumber;
	if(owner_phoneNumber !== undefined)
		owner_phoneNumber = owner_phoneNumber.replace(/\D/g,'');
	var owner_needsToPay = req.body.owner_needsToPay;
	*/

	var establishment_id = req.body.establishment_id;
	var name = req.body.name;
	var address_id = req.body.address_id;
	var address_1 = req.body.address_1.trim();
	var address_2 = req.body.address_2;
	var city = req.body.city.trim();
	var state = req.body.state;
	var zip = req.body.zip;
	var phone_num = req.body.phone_num.replace(/\D/g,'');
	var website = req.body.website;
	var resWebsite = req.body.resWebsite;
	var menu_link = req.body.menu_link;
	//var instagram = req.body.instagram;
	//var facebook = req.body.facebook;
	//var twitter = req.body.twitter;
	//var snapchat = req.body.snapchat;
	var hours_id = req.body.hours_id;
	var mon_open = deFormatHours(req.body.mon_open);
	var mon_close = deFormatHours(req.body.mon_close);
	var tue_open = deFormatHours(req.body.tue_open);
	var tue_close = deFormatHours(req.body.tue_close);
	var wed_open = deFormatHours(req.body.wed_open);
	var wed_close = deFormatHours(req.body.wed_close);
	var thu_open = deFormatHours(req.body.thu_open);
	var thu_close = deFormatHours(req.body.thu_close);
	var fri_open = deFormatHours(req.body.fri_open);
	var fri_close = deFormatHours(req.body.fri_close);
	var sat_open = deFormatHours(req.body.sat_open);
	var sat_close = deFormatHours(req.body.sat_close);
	var sun_open = deFormatHours(req.body.sun_open);
	var sun_close = deFormatHours(req.body.sun_close);

	var description = req.body.description;
	var delivery = (req.body.delivery == "Yes" ? 1 : 0);
	var takeout = (req.body.takeout == "Yes" ? 1 : 0);
	var patio = (req.body.patio == "Yes" ? 1 : 0);
	var rooftop = (req.body.rooftop == "Yes" ? 1 : 0);
	var brunch = (req.body.brunch == "Yes" ? 1 : 0);
	var dog = (req.body.dog == "Yes" ? 1 : 0);

	Ref.getAllByRefCode(state, function(errRef, ref) {
		if (errRef || ref == undefined) {
			res.redirect('audit?state='+selectedState+'&zip='+selectedZip);
		}

		var state_code_ref_id = ref.ref_id;
		var address = address_1 + " " + address_2 + ", " + city + ", " + state;

	  	Geocoder.geocode(address, function(errGeo, geo) {
	  		if (errGeo || geo[0] == undefined) {
	  			//res.redirect('audit?state='+selectedState+'&zip='+selectedZip);
	  		}

	  		if (geo[0].city != city) {
	  			//res.redirect('audit?state='+selectedState+'&zip='+selectedZip);
	  		}

	  		if (geo[0].zipcode != zip) {
	  		//	res.redirect('/importests');
			//	return;
	  		}

	  		var latitude = geo[0].latitude;
	      	var longitude = geo[0].longitude;

	      	var topic = "";

	      	if(getDistanceFromLatLng(latitude, longitude, 44.9778, -93.2650) <= 30) // Minne/STP
	      		topic = "minneapolismn";
	      	else if(getDistanceFromLatLng(latitude, longitude, 41.6611, -91.5302) <= 30) // Iowa City
	      		topic = "iowacityia";
	      	else if(getDistanceFromLatLng(latitude, longitude, 41.5868, -93.6250) <= 30) // Des Moines
	      		topic = "desmoinesia";
	      	else if(getDistanceFromLatLng(latitude, longitude, 39.7392, -104.9903) <= 30) // Denver
	      		topic = "denverco";
	      	else if(getDistanceFromLatLng(latitude, longitude, 38.2527, -85.7585) <= 30) // Louisville
	      		topic = "louisvilleky";
	      	else if(getDistanceFromLatLng(latitude, longitude, 27.7634, -82.5437) <= 30) // Tampa
                topic = "tampabayfl";

	      	var profile_pic_name = "";

	      	
	      	//var params = {
	      	//	establishment_id: establishment_id,
	      	//	owner_id: owner_id,
	      	//	owner_username: owner_username,
	      	//	owner_email: owner_email,
	      	//	owner_phoneNumber: owner_phoneNumber,
	      	//	owner_needsToPay: owner_needsToPay,
	      	//}
	      	
	      	//updateEstQueueOwner(params, function(resUpdateOwner) {
	      		//console.log(resUpdateOwner);

		      	updateEstQueueImg(req.files, function(profPicName) {
		      		profile_pic_name = profPicName;

		      		const establishment = {
						ESTABLISHMENT: {
					      establishment_id: establishment_id,
					      name: name,
					      phone_num: phone_num,
					      website: website,
					      resWebsite: resWebsite,
					      menu_link: menu_link,
					      description: description,
					      delivery: delivery,
					      takeout: takeout,
					      patio: patio,
					      rooftop: rooftop,
					      brunch: brunch,
					      dog_friendly: dog,
					      //instagram: instagram,
					      //facebook: facebook,
					      //twitter: twitter,
					      //snapchat: snapchat,
					      topic: topic
					    }
					};

					const address = {
						ADDRESS: {
					      address_id: address_id,
					      address_1: address_1,
					      address_2: address_2,
					      city: city,
					      state_code_ref_id: state_code_ref_id,
					      zip: zip,
					      latitude: latitude,
					      longitude: longitude
					    }
					};

					const hours = { 
					    HOURS: {
					      hours_id: hours_id,
					      mon_open: mon_open,
					      mon_close: mon_close,
					      tue_open: tue_open,
					      tue_close: tue_close,
					      wed_open: wed_open,
					      wed_close: wed_close,
					      thu_open: thu_open,
					      thu_close: thu_close,
					      fri_open: fri_open,
					      fri_close: fri_close,
					      sat_open: sat_open,
					      sat_close: sat_close,
					      sun_open: sun_open,
					      sun_close: sun_close
					    }
				  	};

				  	//console.log(establishment);
				  	//console.log(address);
				  	//console.log(hours);

					Hours.update(hours, function(err, result) {
				  		if(result.affectedRows > 0) {
				  			Address.update(address, function(err2, result2) {
						  		if(result2.affectedRows > 0) {
						  			Establishment.update(establishment, function(err3, result3) {
						  				if(result3.affectedRows > 0) {
						  					// save the new pic
						  					if(profile_pic_name && profile_pic_name != "") {
						  						EstablishmentImage.getAllByEstablishmentId(establishment_id, function(errEQI, resEQI) {
						  							//console.log(errEQI);
						  							//console.log(resEQI);
						  							
						  							// update image
						  							if(errEQI == null && resEQI) {
						  								//console.log('update image');

						  								const image = {
													  		IMAGE: {
													  		  image_id: resEQI.image_id,
													  		  image_location: profile_pic_name
													  		}
													  	}

									  					Image.update(image, function(err, result4) {
									  						if(result4.affectedRows > 0) {
									  							//res.redirect('audit?state='+selectedState+'&zip='+selectedZip);
									  							var dt = Common.newDate(req.session.tz);
																dt = dtToStr(dt);
																res.send({ newDate: dt, imgUrl: "https://s3.us-east-2.amazonaws.com/est-img/"+profile_pic_name });
									  						}
									  						else {
									  							res.send(false);
									  						}
									  					});
						  							}
						  							// add image
						  							else if(errEQI == null) {
						  								//console.log('add image');

						  								const image = {
													  		IMAGE: {
													  		  image_type_ref_id: 303,
													  		  image_location: profile_pic_name
													  		}
													  	}

									  					Image.insert(image, function(err, result4) {
									  						if(result4.affectedRows > 0) {
									  							//console.log(result);
									  							const estImg = {
									  								ESTABLISHMENT_IMAGE: {
									  									establishment_id: establishment_id,
									  									image_id: result4.insertId
									  								}
									  							}
									  							EstablishmentImage.insert(estImg, function(err, result5) {
									  								if(result5.affectedRows > 0) {
									  									//res.redirect('audit?state='+selectedState+'&zip='+selectedZip);
									  									var dt = Common.newDate(req.session.tz);
																		dt = dtToStr(dt);
																		res.send({ newDate: dt, imgUrl: "https://s3.us-east-2.amazonaws.com/est-img/"+profile_pic_name });
									  								}
									  								else {
									  									res.send(false);
									  								}
									  							});
									  						}
									  						else {
									  							res.send(false);
									  						}
									  					});
						  							}
						  						});
						  					}
						  					else {
											    //res.redirect('audit?state='+selectedState+'&zip='+selectedZip);
											    var dt = Common.newDate(req.session.tz);
												dt = dtToStr(dt);
												res.send({ newDate: dt, imgUrl: "" });
						  					}
									    }
									    else {
				  							res.send(false);
				  						} 
						  			});
						  		}
						  		else {
		  							res.send(false);
		  						}
				  			});
				  		}
				  		else {
							res.send(false);
						}
				  	});
		      	});

	      	//});
		});
	});
};

// REVIEW EST
exports.auditReviewEst = function(req, res) {
	var establishment_id = req.body.estId;
	const establishment = {
		ESTABLISHMENT: {
			establishment_id: establishment_id
		}
	}
	Establishment.update(establishment, function(err, result) {
		//res.redirect('audit?state='+selectedState+'&zip='+selectedZip);
		var dt = Common.newDate(req.session.tz);
		dt = dtToStr(dt);
		res.send(dt);
	});
};


// AUDIT EST SPECIALS

// audit add/update special

exports.auditAddEditSpecial = function(req, res) {
	var special_id = req.body.specialId;
	var sids = req.body.specialIds;
	var establishment_id = req.body.estId;
	var priceType = req.body.priceType;
	var price = priceType != 302 && priceType != 307 ? req.body.price : 0; // BOGO and Free deals set price to 0
	var details = req.body.details;
	var specialType = getDealTypeRefIdByOGId(req.body.specialType);
	var startTime;
	var endTime;
	var days = [];

	var start = req.body.dailyStartTime.replace(':', '');
	var end = req.body.dailyEndTime.replace(':', '');
	start = parseInt(start);
	end = parseInt(end);

	const establishment = {
		ESTABLISHMENT: {
			establishment_id: establishment_id
			//updated_at: Common.newDate(req.session.tz)
		}
	}

	if(start >= 400)
		startTime = "1970-01-01 " + req.body.dailyStartTime + ":00";
	else
		startTime = "1970-01-02 " + req.body.dailyStartTime + ":00";

	if(end > 400)
		endTime = "1970-01-01 " + req.body.dailyEndTime + ":00";
	else
		endTime = "1970-01-02 " + req.body.dailyEndTime + ":00";

	if(req.body.dailyDayMon == "true")
		days.push(1);
	if(req.body.dailyDayTue == "true")
		days.push(2);
	if(req.body.dailyDayWed == "true")
		days.push(3);
	if(req.body.dailyDayThu == "true")
		days.push(4);
	if(req.body.dailyDayFri == "true")
		days.push(5);
	if(req.body.dailyDaySat == "true")
		days.push(6);
	if(req.body.dailyDaySun == "true")
		days.push(0);

	// add special
	if(special_id == null || special_id == "") {
		var special = {
			establishment_id: establishment_id,
			price: price,
	      	price_type_ref_id: priceType,
	      	details: details,
	      	deal_type_ref_id: specialType,
	      	days: days,
	     	start_time: startTime,
	      	end_time: endTime
	      	//created_at: Common.newDate(req.session.tz),
	      	//updated_at: Common.newDate(req.session.tz)
		}

		//console.log(special);

		insertSpecialAudit(special, 0, [], function(newSpecialIds) {
			Establishment.update(establishment, function(errEst, resEst) {
				var dt = Common.newDate(req.session.tz);
				dt = dtToStr(dt);
				res.send({ specialIds: newSpecialIds, newDate: dt });
			});
		});
	}
	// edit special 
	else {
		var daysInsert = [], daysUpdate = [], daysDelete = [];

		if(req.body.dailyDaySun == "true" && req.body.dailyDaySunOrig == "true")
			daysUpdate.push(0);
		else if(req.body.dailyDaySun == "true" && req.body.dailyDaySunOrig == "false")
			daysInsert.push(0);
		else if(req.body.dailyDaySun == "false" && req.body.dailyDaySunOrig == "true")
			daysDelete.push(0);

		if(req.body.dailyDayMon == "true" && req.body.dailyDayMonOrig == "true")
			daysUpdate.push(1);
		else if(req.body.dailyDayMon == "true" && req.body.dailyDayMonOrig == "false")
			daysInsert.push(1);
		else if(req.body.dailyDayMon == "false" && req.body.dailyDayMonOrig == "true")
			daysDelete.push(1);

		if(req.body.dailyDayTue == "true" && req.body.dailyDayTueOrig == "true")
			daysUpdate.push(2);
		else if(req.body.dailyDayTue == "true" && req.body.dailyDayTueOrig == "false")
			daysInsert.push(2);
		else if(req.body.dailyDayTue == "false" && req.body.dailyDayTueOrig == "true")
			daysDelete.push(2);

		if(req.body.dailyDayWed == "true" && req.body.dailyDayWedOrig == "true")
			daysUpdate.push(3);
		else if(req.body.dailyDayWed == "true" && req.body.dailyDayWedOrig == "false")
			daysInsert.push(3);
		else if(req.body.dailyDayWed == "false" && req.body.dailyDayWedOrig == "true")
			daysDelete.push(3);

		if(req.body.dailyDayThu == "true" && req.body.dailyDayThuOrig == "true")
			daysUpdate.push(4);
		else if(req.body.dailyDayThu == "true" && req.body.dailyDayThuOrig == "false")
			daysInsert.push(4);
		else if(req.body.dailyDayThu == "false" && req.body.dailyDayThuOrig == "true")
			daysDelete.push(4);

		if(req.body.dailyDayFri == "true" && req.body.dailyDayFriOrig == "true")
			daysUpdate.push(5);
		else if(req.body.dailyDayFri == "true" && req.body.dailyDayFriOrig == "false")
			daysInsert.push(5);
		else if(req.body.dailyDayFri == "false" && req.body.dailyDayFriOrig == "true")
			daysDelete.push(5);

		if(req.body.dailyDaySat == "true" && req.body.dailyDaySatOrig == "true")
			daysUpdate.push(6);
		else if(req.body.dailyDaySat == "true" && req.body.dailyDaySatOrig == "false")
			daysInsert.push(6);
		else if(req.body.dailyDaySat == "false" && req.body.dailyDaySatOrig == "true")
			daysDelete.push(6);

		var special = { 
	      sids: sids,
	      establishment_id: establishment_id,
	      price: price,
	      price_type_ref_id: priceType,
	      details: details,
	      deal_type_ref_id: specialType,
	      start_time: startTime,
	      end_time: endTime
	      //dinein: dinein,
	      //carryout: carryout,
		};

		//console.log(special);

		auditUpdateSpecials(daysUpdate, daysInsert, daysDelete, special, req.session.tz, 0, [], function(newSpecialIds) {
			Establishment.update(establishment, function(errEst, resEst) {
				//console.log(errEst);
				//console.log(resEst);
				var dt = Common.newDate(req.session.tz);
				dt = dtToStr(dt);
				res.send({ specialIds: newSpecialIds, newDate: dt });
			});
		});
	}
};

// add special

function insertSpecialAudit(special, start, newSpecialIds, _callback) {
	if(start < special.days.length) {
		const s = {
		    SPECIAL: {
		      price: special.price,
		      price_type_ref_id: special.price_type_ref_id,
		      details: special.details,
		      deal_type_ref_id: special.deal_type_ref_id,
		      day: special.days[start],
		      start_time: special.start_time,
		      end_time: special.end_time,
		      created_at: special.created_at,
		      updated_at: special.updated_at
		    }
		};

		Special.insert(s, function(err, result) {
			//response.sErr = err;
			//response.sResult = result;

			if(result.affectedRows > 0) {
				const establishment_special = {
				    ESTABLISHMENT_SPECIAL: {
				      establishment_id: special.establishment_id,
				      special_id: result.insertId,
				      created_at: special.created_at,
				      updated_at: special.updated_at
				    }
				};
				EstablishmentSpecial.insert(establishment_special, function(err, result2) {
					//response.esErr = err;
					//response.esResult = result;
					if(result2.affectedRows > 0) {
						newSpecialIds.push(result.insertId);
						insertSpecialAudit(special, ++start, newSpecialIds, _callback);
					} 
					else {
						_callback(newSpecialIds);
					}
				});
			} 
			else {
				_callback(newSpecialIds);
			}
		});
	}
	else {
		_callback(newSpecialIds);
	}
}

// update special

function auditUpdateSpecials(daysUpdate, daysInsert, daysDelete, special, tz, start, newSpecialIds, _callback) {
	if(start < 7) {
		// insert
		if(daysInsert.indexOf(start) > -1) {
			var special_to_insert = { 
			    SPECIAL: {
			      price: special.price,
			      price_type_ref_id: special.price_type_ref_id,
			      details: special.details,
			      deal_type_ref_id: special.deal_type_ref_id,
			      day: start,
			      start_time: special.start_time,
			      end_time: special.end_time
			      //dinein: dinein,
			      //carryout: carryout,
			      //created_at: now,
			      //updated_at: now
			    }
			};
			
			insertEditSpecialAudit(special_to_insert, special.establishment_id, function(newSpecialId) {
				//console.log("INSERT");
				//console.log(start);
				newSpecialIds.push(newSpecialId);
				auditUpdateSpecials(daysUpdate, daysInsert, daysDelete, special, tz, ++start, newSpecialIds, _callback);
			});
		}
		// update
		else if(daysUpdate.indexOf(start) > -1) {
			var special_to_update = { 
		      	price: special.price,
		      	price_type_ref_id: special.price_type_ref_id,
		      	details: special.details,
		      	deal_type_ref_id: special.deal_type_ref_id,
		      	day: start,
		      	start_time: special.start_time,
		      	end_time: special.end_time
		      	//dinein: dinein,
		      	//carryout: carryout,
		      	//created_at: now,
		      	//updated_at: now
			};
			
			updateEditSpecialAudit(special_to_update, special.sids.split(","), 0, function(newSpecialId) {
				//console.log("UPDATE");
				//console.log(start);

				newSpecialIds.push(newSpecialId);
				auditUpdateSpecials(daysUpdate, daysInsert, daysDelete, special, tz, ++start, newSpecialIds, _callback);
			});
		}
		// delete
		else if(daysDelete.indexOf(start) > -1) {
			deleteEditSpecialAudit(special.sids.split(","), start, 0, tz, function() {
				//console.log("DELETE");
				//console.log(start);

				auditUpdateSpecials(daysUpdate, daysInsert, daysDelete, special, tz, ++start, newSpecialIds, _callback);
			});
		}
		else {
			auditUpdateSpecials(daysUpdate, daysInsert, daysDelete, special, tz, ++start, newSpecialIds, _callback);
		}
	}
	else {
		_callback(newSpecialIds);
	}
}

function insertEditSpecialAudit(special, establishmentId, _callback){
	var response = {sErr: null, sResult: null, esErr: null, esResult: null};
	Special.insert(special, function(err, result) {
		response.sErr = err;
		response.sResult = result;

		if(result && result.affectedRows > 0) {
			const establishment_special = { 
			    ESTABLISHMENT_SPECIAL: {
			      establishment_id: establishmentId,
			      special_id: result.insertId
			    }
			};
			EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
				response.esErr = err2;
				response.esResult = result2;

				if(result2.affectedRows > 0) {
					_callback(result.insertId);
				} else {
					_callback(false)
				}
			});
		} else {
			_callback(false);
		}
	});
}

function updateEditSpecialAudit(special, sids, start, _callback) {
	if(start < sids.length) {
		Special.getAllBySpecialId(sids[start], function(err, s) {
			if(s && special.day == s.day) {
				var specialToUpdate = { 
					SPECIAL: {
						special_id: s.special_id,
				    	price: special.price,
				    	price_type_ref_id: special.price_type_ref_id,
				    	details: special.details,
				    	deal_type_ref_id: special.deal_type_ref_id,
				    	day: special.day,
				    	start_time: special.start_time,
				    	end_time: special.end_time
				    	//dinein: special.dinein,
				    	//carryout: special.carryout
					}
				};

				Special.update(specialToUpdate, function(err, res) {
					_callback(s.special_id);
				});
			}
			else {
				updateEditSpecialAudit(special, sids, ++start, _callback);
			}
		});
	}
	else {
		_callback(false);
	}
}

function deleteEditSpecialAudit(sids, day, start, tz, _callback) {
	if(start < sids.length) {
		Special.getAllBySpecialId(sids[start], function(err, special) {
			if(special && special.day == day) {
				var now = Common.newDate(tz);
				var deleted = new Date(now);
				
				const specialArc = { 
				    SPECIAL_ARCHIVE: {
				    	special_id: sids[start],
				      	price: special.price,
				      	price_type_ref_id: special.price_type_ref_id,
				      	details: special.details,
				      	deal_type_ref_id: special.deal_type_ref_id,
				      	day: special.day,
				      	dinein: special.dinein,
				      	carryout: special.carryout,
				      	start_time: special.start_time,
				      	end_time: special.end_time,
				      	deleted_at: deleted,
				      	created_at: special.created_at,
				      	updated_at: special.updated_at
				    }
				};

				if(special.day == -1) {
					var job = schedule.scheduledJobs["special"+special.special_id];
					if(job)
						job.cancel();
				}

				SpecialArchive.insert(specialArc, function(err, result1) {
					if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
						Special.delete(sids[start], function(err, result2) {
							if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
								_callback();
							} else {
								//res.redirect('dashboard?page=info');
								_callback();
							}
						});
					}
					else {
						//res.redirect('dashboard?page=info');
						_callback();
					}
				});
			}
			else {
				deleteEditSpecialAudit(sids, day, ++start, tz, _callback);
			}
		});
	}
	else {
		_callback();
	}
}

// delete special

exports.auditDeleteSpecial = function(req, res) {
	var specialIds = req.body.specialIds;
	specialIds = specialIds.split(",");

	auditDeleteSpecialIds(specialIds, 0, function() {
		res.send(true);
	});
};

function auditDeleteSpecialIds(specialIds, start, _callback) {
	if(start < specialIds.length) {
		EstablishmentSpecial.deleteBySpecialId(specialIds[start], function(err, result){
		//if(result.affectedRows > 0){
			Special.delete(specialIds[start], function(err, result){
				//if(result.affectedRows > 0){
					auditDeleteSpecialIds(specialIds, ++start, _callback);
				//} else { }
			});
		//} else { }
		});
	}	
	else {
		_callback();
	}
}


// AUDIT EST EVENTS

// audit add/update event

exports.auditAddEditEvent = function(req, res) {
	var establishment_id = req.body.estId;
	var event_id = req.body.eventId;
	var eventIds = req.body.eventIds;
	var title = req.body.title;
	var details = req.body.details;
	var startTime;
	var endTime;
	var days = [];

	// weekly
	if(req.body.weeklyOrExclusive == 'weekly') {
		var start = req.body.startTime.replace(':', '');
		var end = req.body.endTime.replace(':', '');
		start = parseInt(start);
		end = parseInt(end);

		if(start >= 400)
			startTime = "1970-01-01 " + req.body.startTime + ":00";
		else
			startTime = "1970-01-02 " + req.body.startTime + ":00";

		if(end > 400)
			endTime = "1970-01-01 " + req.body.endTime + ":00";
		else
			endTime = "1970-01-02 " + req.body.endTime + ":00";

		if(req.body.eventDayMon == "true")
			days.push(1);
		if(req.body.eventDayTue == "true")
			days.push(2);
		if(req.body.eventDayWed == "true")
			days.push(3);
		if(req.body.eventDayThu == "true")
			days.push(4);
		if(req.body.eventDayFri == "true")
			days.push(5);
		if(req.body.eventDaySat == "true")
			days.push(6);
		if(req.body.eventDaySun == "true")
			days.push(0);
	}
	// exclusive
	else {
		startTime = new Date(req.body.startDate + " " + req.body.startTime);
		endTime = new Date(req.body.endDate + " " + req.body.endTime);
		days.push(-1);
	}

	// add event
	if(event_id == null || event_id == "") {
		var event = {
			establishment_id: establishment_id,
			title: title,
			details: details,
	      	days: days,
	     	start_time: startTime,
	      	end_time: endTime,
	      	created_at: Common.newDate(req.session.tz),
	      	updated_at: Common.newDate(req.session.tz)
		}

		//console.log(event);

		insertEventAudit(event, 0, [], function(newEventIds) {
			res.send({ eventIds: newEventIds });
		});
	}
	// edit event
	else {
		if(req.body.weeklyOrExclusive == 'weekly') {
			var daysInsert = [], daysUpdate = [], daysDelete = [];

			if(req.body.eventDaySun == "true" && req.body.eventDaySunOrig == "true")
				daysUpdate.push(0);
			else if(req.body.eventDaySun == "true" && req.body.eventDaySunOrig == "false")
				daysInsert.push(0);
			else if(req.body.eventDaySun == "false" && req.body.eventDaySunOrig == "true")
				daysDelete.push(0);

			if(req.body.eventDayMon == "true" && req.body.eventDayMonOrig == "true")
				daysUpdate.push(1);
			else if(req.body.eventDayMon == "true" && req.body.eventDayMonOrig == "false")
				daysInsert.push(1);
			else if(req.body.eventDayMon == "false" && req.body.eventDayMonOrig == "true")
				daysDelete.push(1);

			if(req.body.eventDayTue == "true" && req.body.eventDayTueOrig == "true")
				daysUpdate.push(2);
			else if(req.body.eventDayTue == "true" && req.body.eventDayTueOrig == "false")
				daysInsert.push(2);
			else if(req.body.eventDayTue == "false" && req.body.eventDayTueOrig == "true")
				daysDelete.push(2);

			if(req.body.eventDayWed == "true" && req.body.eventDayWedOrig == "true")
				daysUpdate.push(3);
			else if(req.body.eventDayWed == "true" && req.body.eventDayWedOrig == "false")
				daysInsert.push(3);
			else if(req.body.eventDayWed == "false" && req.body.eventDayWedOrig == "true")
				daysDelete.push(3);

			if(req.body.eventDayThu == "true" && req.body.eventDayThuOrig == "true")
				daysUpdate.push(4);
			else if(req.body.eventDayThu == "true" && req.body.eventDayThuOrig == "false")
				daysInsert.push(4);
			else if(req.body.eventDayThu == "false" && req.body.eventDayThuOrig == "true")
				daysDelete.push(4);

			if(req.body.eventDayFri == "true" && req.body.eventDayFriOrig == "true")
				daysUpdate.push(5);
			else if(req.body.eventDayFri == "true" && req.body.eventDayFriOrig == "false")
				daysInsert.push(5);
			else if(req.body.eventDayFri == "false" && req.body.eventDayFriOrig == "true")
				daysDelete.push(5);

			if(req.body.eventDaySat == "true" && req.body.eventDaySatOrig == "true")
				daysUpdate.push(6);
			else if(req.body.eventDaySat == "true" && req.body.eventDaySatOrig == "false")
				daysInsert.push(6);
			else if(req.body.eventDaySat == "false" && req.body.eventDaySatOrig == "true")
				daysDelete.push(6);

			var event = { 
		      eventIds: eventIds,
		      establishment_id: establishment_id,
		      title: title,
		      details: details,
		      start_time: startTime,
		      end_time: endTime
			};

			//console.log(event);

			auditUpdateEvents(daysUpdate, daysInsert, daysDelete, event, req.session.tz, 0, [], function(newEventIds) {
				res.send({ eventIds: newEventIds });
			});
		}
		else {
			var event = { 
			    EVENT: {
			      event_id: event_id,
			      title: title,
			      details: details,
			      day: -1,
			      start_time: startTime,
			      end_time: endTime
			    }
			};

			//console.log(event);
			
			Event.update(event, function(err, result) {
				Special.getAllByEventId(event_id, function(errSpecial, specials) {
					updateOneTimeEventSpecialsAudit(specials, startTime, endTime, 0, function() {
						res.send({ eventIds: [event_id] });
					});
				});
			});
		}
	}
};

// add event

function insertEventAudit(event, start, newEventIds, _callback){
	if(start < event.days.length) {
		const e = {
		    EVENT: {
		      title: event.title,
		      details: event.details,
		      day: event.days[start],
		      start_time: event.start_time,
		      end_time: event.end_time,
		      created_at: event.created_at,
		      updated_at: event.updated_at
		    }
		};

		Event.insert(e, function(err, result) {
			if(result.affectedRows > 0) {
				const establishment_event = {
				    ESTABLISHMENT_EVENT: {
				      establishment_id: event.establishment_id,
				      event_id: result.insertId,
				      created_at: event.created_at,
				      updated_at: event.updated_at
				    }
				};
				EstablishmentEvent.insert(establishment_event, function(err2, result2) {
					if(result2.affectedRows > 0) {
						newEventIds.push(result.insertId);
						insertEventAudit(event, ++start, newEventIds, _callback);
					} else {
						_callback(newEventIds);
					}
				});
			} else {
				_callback(newEventIds);
			}
		});
	}
	else {
		_callback(newEventIds);
	}
}

// update event weekly

function auditUpdateEvents(daysUpdate, daysInsert, daysDelete, event, tz, start, newEventIds, _callback) {
	if(start < 7) {
		// insert
		if(daysInsert.indexOf(start) > -1) {
			var event_to_insert = { 
			    EVENT: {
			      title: event.title,
			      details: event.details,
			      day: start,
			      start_time: event.start_time,
			      end_time: event.end_time
			    }
			};
			
			insertEditEventAudit(event_to_insert, event.establishment_id, function(newEventId) {
				//console.log("INSERT");
				//console.log(start);

				if(newEventId) {
					var evId = event.eventIds.split(",")[0];
					Special.getAllByEventId(evId, function(errSpecials, specials) {
						insertEventSpecialsAudit(specials, 0, event, start, newEventId, function() {
							newEventIds.push(newEventId);
							auditUpdateEvents(daysUpdate, daysInsert, daysDelete, event, tz, ++start, newEventIds, _callback);
						});
					});
				}
				else {
					_callback(newEventIds);
				}
			});
		}
		// update
		else if(daysUpdate.indexOf(start) > -1) {
			var event_to_update = { 
		      	title: event.title,
			    details: event.details,
			    day: start,
			    start_time: event.start_time,
			    end_time: event.end_time
			};
			
			updateEditEventAudit(event_to_update, event.eventIds.split(","), 0, function(newEventId) {
				//console.log("UPDATE");
				//console.log(start);

				newEventIds.push(newEventId);
				auditUpdateEvents(daysUpdate, daysInsert, daysDelete, event, tz, ++start, newEventIds, _callback);
			});
		}
		// delete
		else if(daysDelete.indexOf(start) > -1) {
			deleteEditEventAudit(event.eventIds.split(","), start, 0, tz, function() {
				//console.log("DELETE");
				//console.log(start);

				auditUpdateEvents(daysUpdate, daysInsert, daysDelete, event, tz, ++start, newEventIds, _callback);
			});
		}
		else {
			auditUpdateEvents(daysUpdate, daysInsert, daysDelete, event, tz, ++start, newEventIds, _callback);
		}
	}
	else {
		_callback(newEventIds);
	}
}

function insertEditEventAudit(event, establishmentId, _callback){
	var response = {sErr: null, sResult: null, esErr: null, esResult: null};
	Event.insert(event, function(err, result) {
		response.sErr = err;
		response.sResult = result;

		if(result && result.affectedRows > 0) {
			const establishment_event = { 
			    ESTABLISHMENT_EVENT: {
			      establishment_id: establishmentId,
			      event_id: result.insertId
			    }
			};
			EstablishmentEvent.insert(establishment_event, function(err2, result2) {
				response.esErr = err2;
				response.esResult = result2;

				if(result2.affectedRows > 0) {
					_callback(result.insertId);
				} 
				else {
					_callback(false)
				}
			});
		} 
		else {
			_callback(false);
		}
	});
}

function insertEventSpecialsAudit(specials, start, event, day, newEventId, _callback) {
	if(start < specials.length) {
		const special = {
		    SPECIAL: {
		    	price: specials[start].price,
		    	price_type_ref_id: specials[start].price_type_ref_id,
	      		details: specials[start].details,
	      		deal_type_ref_id: specials[start].deal_type_ref_id,
	      		dinein: specials[start].dinein,
	      		carryout: specials[start].carryout,
		    	day: day,
		      	start_time: event.start_time,
		      	end_time: event.end_time
		    }
		};

		Special.insert(special, function(err, result) {
			if(result.affectedRows > 0) {
				var specialId = result.insertId;
				const establishment_special = {
				    ESTABLISHMENT_SPECIAL: {
				      establishment_id: event.establishment_id,
				      special_id: specialId
				    }
				};

				EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
					const event_special = {
					    EVENT_SPECIAL: {
					      event_id: newEventId,
					      special_id: specialId
					    }
					};
					EventSpecial.insert(event_special, function(err3, result3) {
						insertEventSpecialsAudit(specials, ++start, event, day, newEventId, _callback);
					});
				});
			}
			else {
				_callback();
			}
		});
	}
	else {
		_callback();
	}
}

function updateEditEventAudit(event, eventIds, start, _callback) {
	if(start < eventIds.length) {
		Event.getAllByEventId(eventIds[start], function(err, e) {
			if(e && event.day == e.day) {
				var eventToUpdate = { 
					EVENT: {
						event_id: e.event_id,
						title: event.title,
						details: event.details,
						day: event.day,
				    	start_time: event.start_time,
				    	end_time: event.end_time
					}
				};

				Event.update(eventToUpdate, function(err2, res) {
					Special.getAllByEventId(e.event_id, function(errSpecials, specials) {
						updateEventSpecialsAudit(specials, 0, event, function() {
							_callback(e.event_id);
						});
					});
				});
			}
			else {
				updateEditEventAudit(event, eventIds, ++start, _callback);
			}
		});
	}
	else {
		_callback(false);
	}
}

function updateEventSpecialsAudit(specials, start, event, _callback) {
	if(start < specials.length) {
		const special = {
		    SPECIAL: {
		    	special_id: specials[start].special_id,
		    	day: event.day,
		      	start_time: event.start_time,
		      	end_time: event.end_time
		    }
		};

		Special.update(special, function(err, result) {
			updateEventSpecialsAudit(specials, ++start, event, _callback);
		});
	}
	else {
		_callback();
	}
}

function deleteEditEventAudit(eventIds, day, start, tz, _callback) {
	if(start < eventIds.length) {
		Event.getAllByEventId(eventIds[start], function(err, event) {
			if(event && event.day == day) {
				var now = Common.newDate(tz);
				var deleted = new Date(now);
				
				const eventArc = { 
				    EVENT_ARCHIVE: {
				    	event_id: eventIds[start],
				      	title: event.title,
				      	details: event.details,
				      	day: event.day,
				      	start_time: event.start_time,
				      	end_time: event.end_time,
				      	deleted_at: deleted,
				      	created_at: event.created_at,
				      	updated_at: event.updated_at
				    }
				};

				EventArchive.insert(eventArc, function(err, result1) {
					if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
						Special.getAllByEventId(eventIds[start], function(errSpecials, specials) {
							deleteEventSpecialsAudit(specials, 0, function() {
								Event.delete(eventIds[start], function(err, result2) {
									if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
										_callback();
									} 
									else {
										_callback();
									}
								});
							});
						});
					}
					else {
						_callback();
					}
				});
				
			}
			else {
				deleteEditEventAudit(eventIds, day, ++start, tz, _callback);
			}
		});
	}
	else {
		_callback();
	}
}

function deleteEventSpecialsAudit(specials, start, _callback) {
	if(start < specials.length) {
		EstablishmentSpecial.deleteBySpecialId(specials[start].special_id, function(err1, result1) {
			EventSpecial.deleteBySpecialId(specials[start].special_id, function(err2, result2) {
				Special.delete(specials[start].special_id, function(err3, result3) {
					deleteEventSpecialsAudit(specials, ++start, _callback);
				});
			});
		});
	}
	else {
		_callback();
	}
}

// update event one-time

function updateOneTimeEventSpecialsAudit(specials, startTime, endTime, start, _callback) {
	if(start < specials.length) {
		if(specials[start].start_time < startTime || specials[start].end_time > endTime) {
			EstablishmentSpecial.deleteBySpecialId(specials[start].special_id, function(err1, result1) {
				if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
					Special.delete(specials[start].special_id, function(err2, result2) {
						if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
							EventSpecial.deleteBySpecialId(specials[start].special_id, function(err3, result3) {
								if(result3 != null && result3 != undefined && result3.affectedRows > 0) {
									updateOneTimeEventSpecialsAudit(specials, startTime, endTime, ++start, _callback);
								}
							});
						} 
						else {
							_callback();
						}
					});
				}
				else {
					_callback();
				}
			});
		}
		else {
			updateOneTimeEventSpecialsAudit(specials, startTime, endTime, ++start, _callback)
		}
	}
	else {
		_callback();
	}
}

// delete event

exports.auditDeleteEvent = function(req, res) {
	var eventIds = req.body.eventIds;
	eventIds = eventIds.split(",");

	auditDeleteEventIds(eventIds, 0, function() {
		res.send(true);
	});
};

function auditDeleteEventIds(eventIds, start, _callback) {
	if(start < eventIds.length) {
		var id = eventIds[start];
		EstablishmentSpecial.deleteByEventId(id, function(err, result) {
			Special.deleteByEventId(id, function(err1, result1) {
				EventSpecial.deleteByEventId(id, function(err2, result2) {
					EstablishmentEvent.deleteByEventId(id, function(err3, result3) {
						Event.delete(id, function(err4, result4) {
							auditDeleteEventIds(eventIds, ++start, _callback);
						});
					});
				});
			});
		});
	}	
	else {
		_callback();
	}
}





/* FUNCTIONS */

/*
function insertSpecial(special, establishmentId, time){
	var response = {sErr: null, sResult: null, esErr: null, esResult: null};
	Special.insert(special, function(err, result) {
		response.sErr = err;
		response.sResult = result;

		if(result.affectedRows > 0) {
			const establishment_special = {
			    ESTABLISHMENT_SPECIAL: {
			      establishment_id: establishmentId,
			      special_id: result.insertId,
			      created_at: time,
			      updated_at: time
			    }
			};
			EstablishmentSpecial.insert(establishment_special, function(err, result) {
				response.esErr = err;
				response.esResult = result;
				if(result.affectedRows > 0) {
					return response;
				} else {
					return response;
				}
			});
		} else {
			return response;
		}
	});
}
*/

function getExclusiveSpecials(specials, tz) {
	var specialsResponse = [];
	var now = Common.newDate(tz);

	// we need today at 4am, so that we can see if exclusive special was created today
	var todayAt4am = Common.newDate(tz);
	if (todayAt4am.getHours() < 4) {
		todayAt4am.setDate(todayAt4am.getDate()-1)
	}
	todayAt4am.setHours(4);
	todayAt4am.setMinutes(0);
	todayAt4am.setSeconds(0);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);

  	var expired = [];
  	var current = [];
  	var upcoming = [];

  	if(specials != undefined && specials != null) {
  		for(var i in specials) {
  			s = specials[i];
  			if(s.day == -1) {
				s.dealType = getDealTypeImageByRefId(s.deal_type_ref_id);
				s.duration = formatStartEndTime(s.start_time) + " - " + formatStartEndTime(s.end_time);

  				// expired
  				if(s.start_time > todayAt4am && s.end_time < now) {
  					s.expired = true;
  					expired.push(s);
  				}
  				// current
  				else if(s.start_time < now && s.end_time > now) {
  					s.timeremaining = dateDiff(s.end_time, tz);
  					s.current = true;
  					current.push(s);
  				}
  				// upcoming
  				else if(s.start_time > now) {
  					if(s.start_time.getDate() == now.getDate())
  						s.startDate = "Today";
  					else
  						s.startDate = getStartDate(s.start_time);
  					s.upcoming = true;
  					upcoming.push(s);
  				}
  			}
		}

		current = sortByEndTime(current);
		upcoming = sortByStartTimeThenEndTime(upcoming);
		expired = sortByStartTimeThenEndTime(expired);

		for (c in current)
			specialsResponse.push(current[c]);

		for (u in upcoming)
			specialsResponse.push(upcoming[u]);

		for (e in expired)
			specialsResponse.push(expired[e]);
  	}

  	return specialsResponse;
}

function getTodaysSpecials(specials, tz) {
	var specialsResponse = [];
	var now = Common.newDate(tz);
	var n = getTodaysDayInt(tz);

	// we need today at 4am, so that we can see if exclusive special was created today
	var todayAt4am = Common.newDate(tz);
	if (todayAt4am.getHours() < 4) {
		todayAt4am.setDate(todayAt4am.getDate()-1)
	}
	todayAt4am.setHours(4);
	todayAt4am.setMinutes(0);
	todayAt4am.setSeconds(0);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);

  	var expired = [];
  	var current = [];
  	var upcoming = [];

  	if(specials != undefined && specials != null) {
  		for(var i in specials) {
  			s = specials[i];
  			if(s.day == -1 || s.day == n) {
				s.dealType = getDealTypeImageByRefId(s.deal_type_ref_id);

  				if(s.day == n) {
  					if(s.start_time.getHours() <= 4) {
  						s.start_time.setMonth(tmw.getMonth());
  						s.start_time.setDate(tmw.getDate());
  						s.start_time.setYear(1900+tmw.getYear());
  					}
  					else {
  						s.start_time.setMonth(now.getMonth());
  						s.start_time.setDate(now.getDate());
  						s.start_time.setYear(1900+now.getYear());
  					}

  					if(s.end_time.getHours() <= 4) {
  						s.end_time.setMonth(tmw.getMonth());
  						s.end_time.setDate(tmw.getDate());
  						s.end_time.setYear(1900+tmw.getYear());
  					}
  					else {
  						s.end_time.setMonth(now.getMonth());
  						s.end_time.setDate(now.getDate());
  						s.end_time.setYear(1900+now.getYear());
  					}
  				}

				s.duration = formatStartEndTime(s.start_time) + " - " + formatStartEndTime(s.end_time);

  				// expired
  				if(s.start_time > todayAt4am && s.end_time < now) {
  					s.expired = true;
  					expired.push(s);
  				}
  				// current
  				else if(s.start_time < now && s.end_time > now) {
  					s.timeremaining = dateDiff(s.end_time, tz);
  					s.current = true;
  					current.push(s);
  				}
  				// upcoming
  				else if((s.day == n || s.day == -1) && s.start_time > now) {
  					if(s.day == n)
  						s.startDate = "Today";
  					else
  						s.startDate = getStartDate(s.start_time);
  					s.upcoming = true;
  					upcoming.push(s);
  				}
  			}
		}

		current = sortByEndTime(current);
		upcoming = sortByStartTimeThenEndTime(upcoming);
		expired = sortByStartTimeThenEndTime(expired);

		for (c in current)
			specialsResponse.push(current[c]);

		for (u in upcoming)
			specialsResponse.push(upcoming[u]);

		for (e in expired)
			specialsResponse.push(expired[e]);
  	}

  	return specialsResponse;
}

function getDailySpecials(specials) {
	// Each list in specialsResponse corresponds to a day (item 0 in list is Sunday, 6 is Saturday)
	var specialsResponse = [[], [], [], [], [], [], []];

  	if(specials != undefined && specials != null) {
  		for(var i in specials) {
  			if(specials[i].day != -1) {
  				if(specials[i].day > 0)
  					specials[i].day = specials[i].day - 1;
  				else
  					specials[i].day = 6;

	  			specials[i].start_time_DS = formatStartEndTime(specials[i].start_time);
				specials[i].end_time_DS = formatStartEndTime(specials[i].end_time);
				specials[i].deal_type_ref_id_DS = getDealTypeImageByRefId(specials[i].deal_type_ref_id);
				specialsResponse[specials[i].day].push(specials[i]);
			}
  		}
  	}

  	specialsResponse[0] = sortByStartTimeThenEndTime(specialsResponse[0]);
  	specialsResponse[1] = sortByStartTimeThenEndTime(specialsResponse[1]);
  	specialsResponse[2] = sortByStartTimeThenEndTime(specialsResponse[2]);
  	specialsResponse[3] = sortByStartTimeThenEndTime(specialsResponse[3]);
  	specialsResponse[4] = sortByStartTimeThenEndTime(specialsResponse[4]);
  	specialsResponse[5] = sortByStartTimeThenEndTime(specialsResponse[5]);
  	specialsResponse[6] = sortByStartTimeThenEndTime(specialsResponse[6]);

  	var noSpecials = new Object();

  	for(var x = 0; x <= 6; x++) {
  		if(specialsResponse[x].length == 0) {
  			noSpecials = new Object();
	  		noSpecials.day = x;
	  		noSpecials.empty = true;
	  		specialsResponse[x].push(noSpecials);
	  	}
  	}

	return specialsResponse;
}

function sortByStartTimeThenEndTime(specials) {
  	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var startA = new Date(specials[i-1].start_time);
    		var startB = new Date(specials[i].start_time);
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if (startA > startB || (startA.getTime() === startB.getTime() && endA > endB)) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}

		if(sorted)
		    break;
  	}

  	return specials;
}

function sortByEndTime(specials) {
	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < specials.length; i++) {
    		var endA = new Date(specials[i-1].end_time);
    		var endB = new Date(specials[i].end_time);

    		if (endA > endB) {
				var s = specials[i-1];
		        specials[i-1] = specials[i];
		        specials[i] = s;
		        sorted = false;
		    }
		}

		if(sorted)
		    break;
  	}

  	return specials;
}

function dtToStr(dt) {
	var m = dt.getMonth() + 1;
	var d = dt.getDate();
	var y = dt.getFullYear();
	var hr = dt.getHours();
	var min = dt.getMinutes();
	var ampm = "am";

	if(m < 10)
		m = "0"+m;

	if(d < 10)
		d = "0"+d;

	if(hr > 12) {
		hr -= 12;
		ampm = "pm";
	}
	else if(hr == 0) {
		hr = 12;
	}

	if(min < 10)
		min = "0"+min;

	return m+"/"+d+"/"+y+" "+hr+":"+min+ampm;
}

function formatStartEndTime(datetime){
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}

	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function formatPhone(phone) {
  if(phone === undefined || phone == null || phone == "")
    return "";

  return "(" + phone.substring(0, 3) + ") " + phone.substring(3, 6) + "-" + phone.substring(6, 10);
}

function getDealTypeImageByRefId(refId) {
	switch(refId) {
		case 200: return 'beer-icon.png';
		case 202: return 'shot-icon.png';
    	case 204: return 'wine-icon.png';
    	case 206: return 'mixed-drink-icon.png';
    	case 208: return 'margarita-icon.png';
    	case 210: return 'martini-icon.png';
    	case 212: return 'tumbler-icon.png';
    	case 214: return 'beerbottle.png';
    	case 216: return 'beercan.png';
		case 251: return 'burger-icon.png';
		case 253: return 'appetizer-icon.png';
		case 255: return 'pizza-icon.png';
    	case 257: return 'taco-icon.png';
    	case 259: return 'sushi.png';
    	case 261: return 'bowl.png';
    	case 263: return 'chickenwing.png';
		default: return '';
	}
}

function getDealTypeRefIdByOGId(id){
	if (id == 0) {
		return 200; //beer
	} else if (id == 1) {
		return 202; //shot
	} else if (id == 2) {
		return 206; //mixed drink
	} else if (id == 3) {
		return 208; //margarita
	} else if (id == 4) {
		return 210; //martini
	} else if (id == 5) {
		return 212; //tumbler
	} else if (id == 6) {
		return 204; //wine
	} else if (id == 7) {
		return 251; //burger
	} else if (id == 8) {
		return 253; //appetizer
	} else if (id == 9) {
		return 255; //pizza
	} else if (id == 10) {
		return 257; //taco
	} else if (id == 11) {
		return 259; //sushi
	} else if (id == 12) {
		return 261; //bowl
	} else if (id == 13) {
		return 263; //wing
	} else if (id == 14) {
		return 214; //bottle
	} else if (id == 15) {
		return 216; //can
	}

	return 200;
}

function getTodaysDayInt(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	return t.getDay();
}

function getNotiDuration(hr, min) {
  if(hr == 0)
    return min + " minutes!";
  else if(hr == 1 && min == 0)
    return "hour!";
  else if(hr == 1 && min > 0)
    return hr + " hour " + min + " minutes!";
  else if(hr > 0 && min == 0)
    return hr + " hours!";
  else
    return hr + " hours " + min + " minutes!";
}

function dateDiff(a, tz) {
	var today = Common.newDate(tz);
  	var dealDate = new Date(a);

  	if (dealDate < today) {
    	dealDate.setDate(dealDate.getDate()+1);
    	a = dealDate.getTime();
  	}

	var diffMs = (a - today); // milliseconds between now & a
	var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
	var diffMins = Math.ceil(((diffMs % 86400000) % 3600000) / 60000); // minutes

	if (diffMins == 60) {
		diffHrs++;
		diffMins = 00;
	}

	if (diffHrs == 0) {
		return(diffMins + "m");
	} else {
		return(diffHrs + "h " + diffMins + "m");
	}
}

function addColon(hours) {
	if (hours && hours != "")
		return hours.substring(0, 2) + ":" + hours.substring(2, 4);
	else
		return hours;
}

function deFormatHours(hours) {
	if(hours)
		return hours.replace(':','');
	else
		return '';
}

Date.prototype.addHourss = function(h) {
   this.setTime(this.getTime() + (h*60*60*1000));
   return this;
}

Date.prototype.addMinutess = function(m) {
    this.setTime(this.getTime() + (m*60*1000));
   return this;
}

function getStartDate(d) {
	var startDate = intMonthToStringAbbr(d.getMonth()) + " " + d.getDate() + getDaySuperscript(d.getDate());
	return startDate;
}

function getDaysString(days) {
	days = days.sort();
	var dayStr = "";

	if(days.length == 7) {
		dayStr = "All Days";
	}
	else if(days.length == 6) {
		/*if(days[0] == 0) {
			for(var d = 0; d < days.length - 1; d++) {
				var temp = days[d];
				days[d] = days[d+1];
				days[d+1] = temp;
			}
		}*/
		dayStr = intDayToStringAbbr(days[0]) + " - " + intDayToStringAbbr(days[5]);
	}
	else if(days.length >= 3) {
		var inARow = true;
		for(var d = 0; d < days.length - 1; d++) {
			if(Math.abs(days[d+1] - days[d]) != 1) {
				inARow = false;
			}
		}
		if(inARow) {
			if(days[0] == 0) {
				for(var d = 0; d < days.length - 1; d++) {
					var temp = days[d];
					days[d] = days[d+1];
					days[d+1] = temp;
				}
			}
			dayStr = intDayToStringAbbr(days[0]) + " - " + intDayToStringAbbr(days[days.length-1]);
		}
		else {
			if(days[0] == 0) {
				for(var d = 0; d < days.length - 1; d++) {
					var temp = days[d];
					days[d] = days[d+1];
					days[d+1] = temp;
				}
			}
			for(var d = 0; d < days.length; d++) {
				dayStr += intDayToStringAbbr(days[d]);
				if(d < days.length - 2) {
					dayStr += ", ";
				}
				else if(d == days.length - 2) {
					dayStr += " & ";
				}
			}
		}
	}
	else {
		if(days[0] == 0) {
			for(var d = 0; d < days.length - 1; d++) {
				var temp = days[d];
				days[d] = days[d+1];
				days[d+1] = temp;
			}
		}
		for(var d = 0; d < days.length; d++) {
			dayStr += intDayToStringAbbr(days[d]);
			if(d < days.length - 2) {
				dayStr += ", ";
			}
			else if(d == days.length - 2) {
				dayStr += " & ";
			}
		}
	}
	return dayStr;
}

function intDayToString(day) {
	if(day == -1)
		return "Everyday";
	else if(day == 0)
		return "Sunday";
	else if(day == 1)
		return "Monday";
	else if(day == 2)
		return "Tuesday";
	else if(day == 3)
		return "Wednesday";
	else if(day == 4)
		return "Thursday";
	else if(day == 5)
		return "Friday";
	else if(day == 6)
		return "Saturday";
	else
		return "";
}

function intDayToStringAbbr(day) {
	if(day == -1)
		return "Everyday";
	else if(day == 0)
		return "Sun";
	else if(day == 1)
		return "Mon";
	else if(day == 2)
		return "Tue";
	else if(day == 3)
		return "Wed";
	else if(day == 4)
		return "Thu";
	else if(day == 5)
		return "Fri";
	else if(day == 6)
		return "Sat";
	else
		return "";
}

function intMonthToStringAbbr(month) {
	var m = new Array(7);
	m[0] = "Jan";
	m[1] = "Feb";
	m[2] = "Mar";
	m[3] = "Apr";
	m[4] = "May";
	m[5] = "Jun";
	m[6] = "Jul";
	m[7] = "Aug";
	m[8] = "Sep";
	m[9] = "Oct";
	m[10] = "Nov";
	m[11] = "Dec";
	return m[month];
}

function getDaySuperscript(day) {
  if(day == 1 || day == 21 || day == 31)
    return "st";
  else if(day == 2 || day == 22)
    return "nd";
  else if(day == 3 || day == 23)
    return "rd";
  else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
    return "th";
  else
    return "";
}

function getDateStr(d, sStart, day) {
	var month;
	var date;
	var year;

	if(day == -1) {
		month = sStart.getMonth()+1;
		if(month < 10)
			month = "0" + month;
		date = sStart.getDate();
		if(date < 10)
			date = "0"+date;
		year = sStart.getYear()+1900;
	}
	else {
		if(day == 0)
			day = 7;
		var newDate = new Date(d);
		newDate.setDate(newDate.getDate() + day);

		month = newDate.getMonth()+1;
		if(month < 10)
			month = "0" + month;
		date = newDate.getDate();
		if(date < 10)
			date = "0"+date;
		year = newDate.getYear()+1900;
	}

	return month + "/" + date + "/" + year;
}

function getTimeStr(datetime) {
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}

	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
   return year + "-" + month + "-" + day;
}

function getImpressionsEvent(specials, specialsArc) {
	var s = specials;
	for(var x = 0; x < specialsArc.length; x++)
		s.push(specialsArc[x]);

	var sorted = false;
  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < s.length; i++) {
    		var endA = new Date(s[i-1].created_at);
    		var endB = new Date(s[i].created_at);

    		if (endA < endB) {
				var s1 = s[i-1];
		        s[i-1] = s[i];
		        s[i] = s1;
		        sorted = false;
		    }
		}
  	}


	var impressions = [];
	// mixpanel can only handle 100 events
	for(var y = 0; y < 100; y++) {
		if(s[y] !== undefined)
			impressions.push("Impression-"+s[y].special_id);
		else
			break;
	}

	return impressions;
}

function checkImpressions(names, impressions) {
	var imp = [];
	for(var i in impressions) {
		for(var n in names) {
			if(impressions[i] == names[n]) {
				imp.push(impressions[i]);
				break;
			}
		}
	}
	return imp;
}

function getImpressions(data, specials, specialsArc) {
	var imp = [];

	if(data !== undefined && data.data !== undefined && data.data.values !== undefined) {
		var vals = data.data.values;

		for(var d in vals) {
			//console.log(vals[d]);
			var objKeys = Object.keys(vals[d]);
			var objVals = Object.values(vals[d]);
			//console.log(objKeys);
			//console.log(objVals);

			for(var v in objVals) {
				var impObj = {};
				impObj.specialId = d.substring(d.indexOf("-")+1);
				impObj.impressions = objVals[v];

				var objDate;
				var objWeek;
				var objCreated;
				var isArchive = true;

				for(var sp in specials) {
					if(specials[sp].special_id == impObj.specialId) {
						if(specials[sp].price_type_ref_id == 300)
                            impObj.details = "$" + specials[sp].price + " " + specials[sp].details;
                        else if(specials[sp].price_type_ref_id == 301)
                            impObj.details = specials[sp].price + "% Off " + specials[sp].details;
                        else if(specials[sp].price_type_ref_id == 302)
                            impObj.details = "BOGO " + specials[sp].details;
                        else if(specials[sp].price_type_ref_id == 307)
                            impObj.details = "Free " + specials[sp].details;

                        impObj.establishment = specials[sp].establishment;
						impObj.date = getDateStr(objKeys[v], specials[sp].start_time, specials[sp].day);
						impObj.time = getTimeStr(specials[sp].start_time) + " - " + getTimeStr(specials[sp].end_time);
						impObj.day = specials[sp].day;

						//if(impObj.impressions > 30) {
                        //	console.log(impObj);
                        //	console.log(objKeys);
						//	console.log(objVals);
                        //}

						objDate = new Date(impObj.date);
						objWeek = new Date(objKeys[v]);
						objCreated = new Date(specials[sp].created_at);
						isArchive = false;
						break;
					}
				}

				if(isArchive) {
					for(var spa in specialsArc) {
						if(specialsArc[spa].special_id == impObj.specialId) {
							if(specialsArc[spa].price_type_ref_id == 300)
	                            impObj.details = "$" + specialsArc[spa].price + " " + specialsArc[spa].details;
	                        else if(specialsArc[spa].price_type_ref_id == 301)
	                            impObj.details = specialsArc[spa].price + "% Off " + specialsArc[spa].details;
	                        else if(specialsArc[spa].price_type_ref_id == 302)
	                            impObj.details = "BOGO " + specialsArc[spa].details;
	                        else if(specialsArc[spa].price_type_ref_id == 307)
	                            impObj.details = "Free " + specialsArc[spa].details;

	                        impObj.establishment = specialsArc[spa].establishment;
							impObj.date = getDateStr(objKeys[v], specialsArc[spa].start_time, specialsArc[spa].day);
							impObj.time = getTimeStr(specialsArc[spa].start_time) + " - " + getTimeStr(specialsArc[spa].end_time);
							impObj.day = specialsArc[spa].day;

							objDate = new Date(impObj.date);
							objWeek = new Date(objKeys[v]);
							objCreated = new Date(specialsArc[spa].created_at);
							break;
						}
					}
				}

				if((impObj.day == -1 && objWeek <= objDate && objWeek.setDate(objWeek.getDate()+6) >= objDate) ||
				   (impObj.day > -1 && objCreated < objDate.setDate(objDate.getDate()+1))) {
					imp.push(impObj);
				}
			}
		}
	}

	return imp;
}

function OwnerComparator(a, b) {
   	if (a.username < b.username) return -1;
   	if (a.username > b.username) return 1;
   	return 0;
}

function getLocationFromCoords(lat, lon) {
	if(lat == 44.9838 && lon == -93.2650)
		return "Minne/St Paul";
	else if(lat == 38.2577 && lon == -85.7585)
		return "Louisville";
	else if(lat == 41.6661 && lon == -91.5320)
		return "Iowa City";
	else if(lat == 34.0522 && lon == -118.2437)
		return "Los Angeles";
	else
		return "";
}

function getMonday(d) {
	d = new Date(d);
  	var day = d.getDay();
    var diff = d.getDate() - day + (day == 0 ? -6 : 1); // adjust when day is sunday
  	return new Date(d.setDate(diff));
}

function getDistanceFromLatLng(userLat, userLon, lat, lon) {
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }

    if(userLat && userLon && lat && lon) {
        var lat2 = userLat;
        var lon2 = userLon;
        var lat1 = parseFloat(lat);
        var lon1 = parseFloat(lon);

        var R = 3959; // Radius in miles has a problem with the .toRad() method below.
        var x1 = lat2 - lat1;
        var dLat = x1.toRad();
        var x2 = lon2 - lon1;
        var dLon = x2.toRad();
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;

        d = Math.round(d * 100)/100;

        return d;
    } else {
        return "";
    }
}
