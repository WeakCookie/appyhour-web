var User = require('../models/user.js');
var UserEstablishment = require('../models/user_establishment.js');
var Establishment = require('../models/establishment.js');
var Ref = require('../models/ref.js');
var bcrypt = require('bcrypt');
var nodemailer = require('nodemailer');
var fs = require('fs');

var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL

exports.show = function(req, res) {
	var isOwner = false;

	//if(req.session.user.user_type_ref_id == 305) {
	if(req.session.ests.length > 1) {
		isOwner = true;
	}

	User.getManagersForOwnerId(req.session.user.user_id, function(err, managers) {
		var needsToPay = false;
		var cardOnFile = false;
		var cancelDate;
		var dateCanceled;

		if(req.session.user.needs_to_pay == 1) {
			needsToPay = true;

			// customer is in our db
			if(req.session.user.stripe_id) {
				stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
					stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
						// customer has card on file
						if(card) {
							cardOnFile = true;
						}

						// customer has a subscription
						if(customer.subscriptions.data.length > 0) {

							// customer's subscription will cancel
							if(customer.subscriptions.data[0].cancel_at_period_end == true) {
								dateCanceled = epochToDate(customer.subscriptions.data[0].canceled_at);
								cancelDate = epochToDate(customer.subscriptions.data[0].current_period_end);
							} 

							//var custBillAmount = centsToDollars(customer.subscriptions.data[0].plan.amount);
							//var custPlanPrice = custBillAmount + "/" + customer.subscriptions.data[0].plan.interval;

							var planOnePrice = 9999;
							var planElsePrice = 8999;
							if(customer.subscriptions.data[0].plan.tiers) {
								planOnePrice = customer.subscriptions.data[0].plan.tiers[0].unit_amount;
								planElsePrice = customer.subscriptions.data[0].plan.tiers[1].unit_amount;
							}
							else if(customer.subscriptions.data[0].plan.amount) {
								planOnePrice = customer.subscriptions.data[0].plan.amount;
								planElsePrice = customer.subscriptions.data[0].plan.amount;
							}

							var custBillAmount = centsToDollars(planOnePrice + ((req.session.ests.length - 1) * planElsePrice));

							var pauseCollection = false;
							if(customer.subscriptions.data[0].pause_collection != null)
								pauseCollection = true;

							res.render('settings', {
								isOwner: isOwner,
								managers: managers,
								needsToPay: needsToPay,
								cardOnFile: cardOnFile,
								custCardBrand: card.brand,
								custCardLastFour: card.last4,
								plan: customer.subscriptions.data[0].plan,
								custNextBillDate: epochToDate(customer.subscriptions.data[0].current_period_end),
								custBillAmount: custBillAmount,
								dateCanceled: dateCanceled,
								cancelDate: cancelDate,
								hasSubscription: true,
								pauseCollection: pauseCollection
							});
						}
						else {
							res.render('settings', {
								isOwner: isOwner,
								managers: managers,
								needsToPay: needsToPay,
								cardOnFile: cardOnFile,
								hasSubscription: false
							});
						}
					});
				});
			}
			else {
				res.render('settings', {
					isOwner: isOwner,
					managers: managers,
					needsToPay: needsToPay
				});
			}
		}
		else {
			res.render('settings', {
				isOwner: isOwner,
				managers: managers,
				needsToPay: needsToPay
			});
		}
	});
}

exports.saveSettings = function(req, res) {
	var email = req.body.email;
	var phone = req.body.phone;
	var username = req.body.username;

	User.getAllByUsername(username, function(errUsername, foundUsername) {
		if(!foundUsername || (foundUsername && username == req.session.user.username)) {
			User.getAllByEmail(email, function(errEmail, foundEmail) {
				if(!foundEmail || (foundEmail && email == req.session.user.email)) {
					const user = {
			      		USER: {
			      		  user_id: req.session.user.user_id,
			      		  email: email,
			      		  username: username,
			      		  phone_num: phone
			      		}
			      	}

			      	User.update(user, function(errUpdate, update) {
			      		User.getAllByUserId(req.session.user.user_id, function(errUser, sessionUser) {
				      		req.user = sessionUser;
			              	req.session.user = sessionUser;
			              	res.send("true");
			      		});
			      	});
				}
				else {
					res.send("Email already exists.");
				}
			});
		}
		else {
			res.send("Username already exists. Please select a different username.");
		}
	});
}

exports.updatePassword = function(req, res) {
	var currPw = req.body.currPw;
	var newPw = req.body.newPw;

	User.getAllByUserId(req.session.user.user_id, function(err, user) {
		bcrypt.compare(currPw, user.password, function(err, match) {
	    	if(match) {
	    		bcrypt.hash(newPw, 10, function(err, hash) {
		          if(!err) {
		            const u = { 
		              USER: {
		                user_id: req.session.user.user_id, 
		                password: hash
		              }
		            };
		            User.update(u, function(err, result) {
		              req.user = user;
		              req.session.user = user;
		              res.send("true");
		            });
		          }
		        });
	    	}
	    	else {
	    		res.send("Current Password is invalid. Please try again.")
	    	}
	    });
	});
}

exports.addManager = function(req, res) {
	var estId = req.body.managerEst;
	var email = req.body.managerEmail;
	var phone = req.body.managerPhone;
	var username = req.body.managerUsername;

	User.getAllByUsername(username, function(errUsername, foundUsername) {
		if(!foundUsername) {
			User.getAllByEmail(email, function(errEmail, foundEmail) {
				if(!foundEmail) {
					var password = "AHgenPW$";

			  		bcrypt.hash(password, 10, function(errPw, hash) {
			  			if(!errPw) {
			  				const user = {
					      		USER: {
					      		  email: email,
					      		  username: username,
					      		  password: hash,
					      		  phone_num: phone,
								  user_type_ref_id: 306, 
					      		  is_active: 1,
					      		  needs_to_pay: 0
					      		}
					      	}

					      	User.insert(user, function(errInsert, userInserted) {
					      		const userEst = {
						      		USER_ESTABLISHMENT: {
						      		  user_id: userInserted.insertId,
						      		  establishment_id: estId
						      		}
						      	}

						      	UserEstablishment.insert(userEst, function(errEstInserted, userEstInserted) {

									Establishment.getAllByEstablishmentId(estId, function(errEst, est) {

										Ref.getAllByRefId(1000, function(errRef, secretPw) {
											
								        	fs.readFile(__dirname + '/../public/emails/new_manager_email.html', function (err, html) {
											    if (!err) {
											        var msg = ""+html;
										        	msg = msg.replace("{{ req.session.user.email }}", req.session.user.email);
										        	msg = msg.replace("{{ est.name }}", est.name);
										        	msg = msg.replace("{{ username }}", username);
										        	msg = msg.replace("{{ secretPw.ref_code }}", secretPw.ref_code);

										        	var transporter = nodemailer.createTransport({
												      service: 'Gmail',
												      auth: {
												        user: 'appy-support@appyhourmobile.com',
												        pass: 'gpzkftethvbstvfn'
												      }
												    });

												    var mailOptions = {
												      from: 'appy-support@appyhourmobile.com',
												      to: email,
												      subject: 'AppyHour Login Instructions',
												      html: msg
												    };

												    transporter.sendMail(mailOptions, function(error, info){
												      if (error) {
												        console.log(error);
												        res.send("Sorry, we're having some issues sending your message. Please try again later.");
												      } else {
												        console.log('Email sent: ' + info.response);
												        res.send("true");
												      }
												    });
												}
											});
										});
									});
						        	

						      		//res.send("true");
						      	});
					      	});
					    }
					});
				}
				else {
					res.send("Email already exists.");
				}
			});
		}
		else {
			res.send("Username already exists. Please select a different username.");
		}
	});
}

exports.deleteManager = function(req, res) {
	var managerId = req.body.managerId;
	
	UserEstablishment.deleteByUserId(managerId, function(errUserEst, userEstDeleted) {
		User.delete(managerId, function(errUser, userDeleted) {
			res.send("true");
		});
	});
}

function epochToDate(epoch) {
	var d = new Date(0);
	d.setUTCSeconds(epoch);

  	var monthNames = [
    	"January", "February", "March",
    	"April", "May", "June", "July",
    	"August", "September", "October",
    	"November", "December"
  	];

  	var day = d.getDate();
  	var monthIndex = d.getMonth();
  	var year = d.getFullYear();

  	return monthNames[monthIndex] + ' ' + day + getDaySuperscript(day) + ', ' + year;
}

function getDaySuperscript(day) {
	if(day == 1 || day == 21 || day == 31)
		return "st";
	else if(day == 2 || day == 22)
		return "nd";
	else if(day == 3 || day == 23)
		return "rd";
	else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
		return "th";
	else 
		return "";
}

function centsToDollars(cents) {
	var dollars = cents / 100;
	return dollars.toLocaleString("en-US", {style:"currency", currency:"USD"});
}