var Common = require('../models/common.js');
var Establishment = require('../models/establishment.js');
var Special = require('../models/special.js');
var SpecialArchive = require('../models/special_archive.js');
var Favorites = require('../models/favorites.js');
var MixpanelExport = require('mixpanel-data-export');
var panel = new MixpanelExport({
	api_key: "4f98c92e0ecf95d97d76d6eca2cb0298",
	api_secret: "3e9796ed74b2cdda13edc3bbe904f073"
});
var NodeGeocoder = require('node-geocoder');
var Geocoder = NodeGeocoder({ provider: 'google', apiKey: 'AIzaSyA8ObhYNLNSbaaHrF1B7dC1a10t9iF50II' });

exports.show = function(req, res) {
	var curr = Common.newDate(req.session.tz);
	var toDate = getYYYYMMDD(curr);
	var weekStart = getYYYYMMDD(new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() - curr.getDay()+1));
	var weekEnd = curr.getDay();
	if(weekEnd == 0)
		weekEnd = 7;
	weekEnd = 7 - weekEnd;
	weekEnd = "+" + weekEnd + "d";

	curr.setDate(curr.getDate()-365);
	var yearAgo = getYYYYMMDD(new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() - curr.getDay())) + " 00:00:00";

	Establishment.getEverythingByEstablishmentId(req.session.establishment.establishment_id, function(err, establishment) {
		var params = [ establishment.establishment_id, yearAgo ];

		Special.getAllByEstablishmentIdForYear(params, function(err, specials) {

			var params = [ establishment.establishment_id, yearAgo, yearAgo ];

  			SpecialArchive.getAllByEstablishmentIdForYear(params, function(err, specialsArc) {

  				Favorites.getAllByEstablishmentId(establishment.establishment_id, function(err, favs) {

  					Favorites.getAllProfiles(function(err, profiles) {
  						var mspLat = 44.9778;
  						var mspLon = -93.2650;
  						var icLat = 41.6611;
  						var icLon = -91.5302;
  						var denLat = 39.7392;
  						var denLon = -104.9903;
  						var louLat = 38.2527;
  						var louLon = -85.7585;
  						var location = "";
  						var profs = 0;
  						var profsLastWeek = 0;

  						var dist = distance(establishment.latitude, establishment.longitude, mspLat, mspLon);
  						if(dist <= 30) {
  							location = "msp";
	  						for(var p in profiles) {
	  							var d = distance(profiles[p].location_lat, profiles[p].location_lon, mspLat, mspLon);
	  							if(d <= 30) {
	  								profs += 1;
	  								var lw = Common.newDate(req.session.tz);
	  								lw.setDate(lw.getDate() - 7);
	  								if(profiles[p].created_at <= lw) {
	  									profsLastWeek += 1;
	  								}
	  							}
	  						}
  						}
  						dist = distance(establishment.latitude, establishment.longitude, icLat, icLon);
  						if(dist <= 30) {
  							location = "ic";
	  						for(var p in profiles) {
	  							var d = distance(profiles[p].location_lat, profiles[p].location_lon, icLat, icLon);
	  							if(d <= 30) {
	  								profs += 1;
	  								var lw = Common.newDate(req.session.tz);
	  								lw.setDate(lw.getDate() - 7);
	  								if(profiles[p].created_at <= lw) {
	  									profsLastWeek += 1;
	  								}
	  							}
	  						}
  						}
  						dist = distance(establishment.latitude, establishment.longitude, denLat, denLon);
  						if(dist <= 30) {
  							location = "den";
	  						for(var p in profiles) {
	  							var d = distance(profiles[p].location_lat, profiles[p].location_lon, denLat, denLon);
	  							if(d <= 30) {
	  								profs += 1;
	  								var lw = Common.newDate(req.session.tz);
	  								lw.setDate(lw.getDate() - 7);
	  								if(profiles[p].created_at <= lw) {
	  									profsLastWeek += 1;
	  								}
	  							}
	  						}
  						}
  						dist = distance(establishment.latitude, establishment.longitude, louLat, louLon);
  						if(dist <= 30) {
  							location = "lou";
	  						for(var p in profiles) {
	  							var d = distance(profiles[p].location_lat, profiles[p].location_lon, louLat, louLon);
	  							if(d <= 30) {
	  								profs += 1;
	  								var lw = Common.newDate(req.session.tz);
	  								lw.setDate(lw.getDate() - 7);
	  								if(profiles[p].created_at <= lw) {
	  									profsLastWeek += 1;
	  								}
	  							}
	  						}
  						}

  						var profsChange = 0;
  						if(profs != profsLastWeek && profsLastWeek > 0) {
  							profsChange = ((profs - profsLastWeek) / profsLastWeek) * 100;
  							profsChange = profsChange.toFixed(2);
  						}
  						else if(profs > profsLastWeek && profsLastWeek == 0) {
  							profsChange = 100;
  						}

  						var cardOnFile = false;
						var hasSubscription = false;
						var needsToPay = false;
						var favorites = 0;
						var favoritesLastWeek = 0;
						var imp = [];
						var impPastWeek = 0;
						var impPrevWeek = 0;
						var impChange = 0;
						var pastWeek = "";
						var isAppyHour = false;

						var endWeek = Common.newDate(req.session.tz);
						endWeek.setDate(endWeek.getDate()+1);
						endWeek.setHours(0,0,0,0);
						var startWeek = new Date(endWeek);
						startWeek.setDate(startWeek.getDate()-7);

						var endWeekPrev = new Date(startWeek);
						var startWeekPrev = new Date(endWeekPrev);
						startWeekPrev.setDate(startWeekPrev.getDate()-7);

						var sw = new Date(startWeek);
						sw = (sw.getMonth()+1) + "/" + sw.getDate() + "/" + (sw.getYear()+1900);
						var ew = new Date(endWeek);
						ew.setDate(ew.getDate()-1);
						ew = (ew.getMonth()+1) + "/" + ew.getDate() + "/" + (ew.getYear()+1900);
						pastWeek = sw + " - " + ew;

						if(favs) {
							favorites = favs.length;
							for(var f in favs) {
								var lw = Common.newDate(req.session.tz);
	  							lw.setDate(lw.getDate() - 7);
								if(favs[f].created_at <= lw) {
									favoritesLastWeek += 1;
								}
							}
						}

						var favoritesChange = 0;
  						if(favorites != favoritesLastWeek && favoritesLastWeek > 0) {
  							favoritesChange = ((favorites - favoritesLastWeek) / favoritesLastWeek) * 100;
  							favoritesChange = favoritesChange.toFixed(2);
  						}
  						else if(favorites > favoritesLastWeek && favoritesLastWeek == 0) {
  							favoritesChange = 100;
  						}

						if(establishment.name == "AppyHour")
							isAppyHour = true;

						for(var x in specialsArc) {
							specials.push(specialsArc[x]);
						}

						var impressions = [];
						// mixpanel can only handle 100 events
						for(var s = 0; s < 100; s++) {
							if(specials[s] !== undefined) {
								impressions.push("Impression-"+specials[s].special_id);
							}
							else {
								break;
							}
						}
						
						panel.events({
						  event: impressions,
						  type: "general",
						  unit: "week",
						  from_date: "2019-01-01",
						  to_date: toDate
						}).then(function(data) {
							if(data !== undefined && data.data !== undefined && data.data.values !== undefined) {
								var vals = data.data.values;
								
								for(var d in vals) {	
									var objKeys = Object.keys(vals[d]);
									var objVals = Object.values(vals[d]);

									for(var v in objVals) {
										var impObj = {};
										impObj.specialId = d.substring(d.indexOf("-")+1);
										impObj.impressions = objVals[v];

										var objDate;
										var objWeek;
										var objCreated;
										var isArchive = true;

										for(var sp in specials) {
											if(specials[sp].special_id == impObj.specialId) {
												if(specials[sp].price_type_ref_id == 300)
						                            impObj.details = "$" + specials[sp].price.toFixed(2) + " " + specials[sp].details;
						                        else if(specials[sp].price_type_ref_id == 301)
						                            impObj.details = specials[sp].price + "% Off " + specials[sp].details;
						                        else if(specials[sp].price_type_ref_id == 302)
						                            impObj.details = "BOGO " + specials[sp].details;
						                        else if(specials[sp].price_type_ref_id == 307) 
						                            impObj.details = "Free " + specials[sp].details;

						                        var newDate = new Date(objKeys[v]);
		                        				//newDate.setHours(newDate.getHours()-5); // UTC issue
		                        				newDate.subtractHoursDST(req.session.tz);

						                        impObj.start = specials[sp].start_time;
												impObj.date = getDateStr(newDate, specials[sp].start_time, specials[sp].day);
												impObj.time = getTimeStr(specials[sp].start_time) + " - " + getTimeStr(specials[sp].end_time);
												impObj.day = specials[sp].day;

												if(specials[sp].deleted_at != undefined) {
													impObj.deleted = specials[sp].deleted_at;
													impObj.status = "Deleted";
													impObj.status2 = dateToStr(impObj.deleted);
												}
												else if(impObj.day > -1) {
													impObj.status = "Active every " + intDayToString(impObj.day);
												}

												if(specials[sp].event)
													impObj.event = specials[sp].event;
												else if(specials[sp].eventArc)
													impObj.event = specials[sp].eventArc;
												
												objDate = new Date(impObj.date);
												objWeek = newDate;
												objCreated = new Date(specials[sp].created_at);
												break;
											}
										}

										var nextDay = new Date(objDate);
										nextDay.setDate(nextDay.getDate()+1);
										
										var nextWeek = new Date(objWeek);
										nextWeek.setDate(nextWeek.getDate()+7);

										if((impObj.day == -1 && objWeek <= impObj.start && nextWeek > impObj.start) ||
										   (impObj.day > -1 && objCreated < nextDay)) {
											if(impObj.deleted == undefined || (impObj.deleted != undefined && impObj.deleted > objDate)) {
												if((impObj.day == -1 && startWeek <= impObj.start && endWeek > impObj.start) ||
												   (impObj.day > -1 && startWeek <= objDate && endWeek > objDate)) {
													impPastWeek += impObj.impressions;
												}
												else if((impObj.day == -1 && startWeekPrev <= impObj.start && endWeekPrev > impObj.start) ||
												   		(impObj.day > -1 && startWeekPrev <= objDate && endWeekPrev > objDate)) {
													impPrevWeek += impObj.impressions;
												}
												imp.push(impObj);
											}
										}
									}
								}
							}

							if(impPastWeek > 0 && impPrevWeek > 0) {
								impChange = ((impPastWeek - impPrevWeek) / impPrevWeek) * 100;
								impChange = impChange.toFixed(2);
							}
							else if(impPastWeek > 0 && impPrevWeek == 0) {
								impChange = 100;
							}
							else if(impPastWeek == 0 && impPrevWeek > 0) {
								impChange = -100;
							}
						
							if(req.session.user.needs_to_pay == 1) {
								needsToPay = true;
								if(req.session.user.stripe_id) {
						  			var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
						  			//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
								    stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
								      	if(customer) {
								      		if(customer.subscriptions.data.length > 0) {
										        hasSubscription = true;
										    }
								        	stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
								          		if(card) {
								            		cardOnFile = true;
								          		}
								          		res.render('analytics', {
											    	needsToPay: needsToPay,
											    	cardOnFile: cardOnFile,
											    	hasSubscription: hasSubscription,
											    	favorites: favorites,
											    	favoritesChange: favoritesChange,
											    	impPastWeek: impPastWeek,
											    	impChange: impChange,
											    	pastWeek: pastWeek,
											    	location: location,
											    	profs: profs,
											    	profsChange: profsChange,
											    	impressions: imp,
											    	isAppyHour: isAppyHour,
											    	toDate: toDate,
											    	weekStart: weekStart,
											    	weekEnd: weekEnd
												});
								        	});
								      	} else {
								      		res.render('analytics', {
										    	needsToPay: needsToPay,
										    	cardOnFile: cardOnFile,
										    	hasSubscription: hasSubscription,
										    	favorites: favorites,
										    	favoritesChange: favoritesChange,
										    	impPastWeek: impPastWeek,
										    	impChange: impChange,
										    	pastWeek: pastWeek,
										    	location: location,
											    profs: profs,
											    profsChange: profsChange,
										    	impressions: imp,
											    isAppyHour: isAppyHour,
											    toDate: toDate,
											    weekStart: weekStart,
											    weekEnd: weekEnd
											});
								      	}
								    });
								} else {
									res.render('analytics', {
								    	needsToPay: needsToPay,
								    	cardOnFile: cardOnFile,
								    	hasSubscription: hasSubscription,
								    	favorites: favorites,
								    	favoritesChange: favoritesChange,
								    	impPastWeek: impPastWeek,
								    	impChange: impChange,
								    	pastWeek: pastWeek,
								    	location: location,
										profs: profs,
										profsChange: profsChange,
								    	impressions: imp,
										isAppyHour: isAppyHour,
										toDate: toDate,
										weekStart: weekStart,
										weekEnd: weekEnd
									});
								}
							} else {
								res.render('analytics', {
							    	needsToPay: needsToPay,
							    	cardOnFile: cardOnFile,
							    	hasSubscription: hasSubscription,
							    	favorites: favorites,
							    	favoritesChange: favoritesChange,
							    	impPastWeek: impPastWeek,
							    	impChange: impChange,
							    	pastWeek: pastWeek,
							    	location: location,
									profs: profs,
									profsChange: profsChange,
							    	impressions: imp,
									isAppyHour: isAppyHour,
									toDate: toDate,
									weekStart: weekStart,
									weekEnd: weekEnd
								});
							}
						});
  					});
				});
			});
		});
	});
};

function getImpressions(specialIds, specials, s, imp, res, req) {
	var cardOnFile = false;
	var hasSubscription = false;
	var needsToPay = false;
	var isAppyHour = false;
	var curr = Common.newDate(req.session.tz);
	var toDate = getYYYYMMDD(curr);
	var weekStart = getYYYYMMDD(new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() - curr.getDay()+1));
	var weekEnd = curr.getDay();
	if(weekEnd == 0)
		weekEnd = 7;
	weekEnd = 7 - weekEnd;
	weekEnd = "+" + weekEnd + "d";

	curr.setDate(curr.getDate()-365);
	var yearAgo = getYYYYMMDD(new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() - curr.getDay())) + " 00:00:00";
	
	if(s < specials.length) {
		
		panel.eventProperties({
		  	event: "Impression-"+specialIds[s],
		  	name: "feature",
		  	type: "general",
		  	unit: "week",
		  	from_date: "2020-01-01",
		  	to_date: toDate
		}).then(function(data) {
			if(data !== undefined && data.data !== undefined && data.data.values !== undefined) {
				var vals = data.data.values;
				
				for(var d in vals) {	
					var objKeys = Object.keys(vals[d]);
					var objVals = Object.values(vals[d]);
					
					for(var v in objKeys) {
						var impObj = {};
						impObj.specialId = specialIds[s];
						impObj.impressions = objVals[v];

						var objDate;
						var objWeek;
						var objCreated;

						for(var sp in specials) {
							if(specials[sp].special_id == impObj.specialId) {
								if(specials[sp].price_type_ref_id == 300)
		                            impObj.details = "$" + specials[sp].price.toFixed(2) + " " + specials[sp].details;
		                        else if(specials[sp].price_type_ref_id == 301)
		                            impObj.details = specials[sp].price + "% Off " + specials[sp].details;
		                        else if(specials[sp].price_type_ref_id == 302)
		                            impObj.details = "BOGO " + specials[sp].details;
		                        else if(specials[sp].price_type_ref_id == 307) 
		                            impObj.details = "Free " + specials[sp].details;

		                        var newDate = new Date(objKeys[v]);
		                        //newDate.setHours(newDate.getHours()-5); // UTC issue
		                        newDate.subtractHoursDST(req.session.tz);
		                        
								impObj.date = getDateStr(newDate, specials[sp].start_time, specials[sp].day);
								impObj.time = getTimeStr(specials[sp].start_time) + " - " + getTimeStr(specials[sp].end_time);
								impObj.day = specials[sp].day;

								if(specials[sp].deleted_at != undefined) {
									impObj.deleted = specials[sp].deleted_at;
									impObj.status = "Deleted";
									impObj.status2 = dateToStr(impObj.deleted);
								}
								else if(impObj.day > -1) {
									impObj.status = "Active every " + intDayToString(impObj.day);
								}
								
								objDate = new Date(impObj.date);
								objWeek = newDate;
								objCreated = new Date(specials[sp].created_at);
								break;
							}
						}

						var nextDay = new Date(objDate);
						nextDay.setDate(nextDay.getDate()+1);
						
						if((impObj.day == -1 && objWeek <= objDate && objWeek.setDate(objWeek.getDate()+6) >= objDate) ||
						   (impObj.day > -1 && objCreated < nextDay)) {
							if(impObj.deleted == undefined || (impObj.deleted != undefined && impObj.deleted > objDate)) {
								imp.push(impObj);
							}
						}
					}
				}
			}
			getImpressions(specialIds, specials, ++s, imp, res, req);
		});
		
		//getImpressions(specialIds, ++s, imp, res, req);
	}
	else {
		if(req.session.user.needs_to_pay == 1) {
			needsToPay = true;
			if(req.session.user.stripe_id) {
	  			var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
	  			//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
			    stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
			      	if(customer) {
			      		if(customer.subscriptions.data.length > 0) {
					        hasSubscription = true;
					    }
			        	stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
			          		if(card) {
			            		cardOnFile = true;
			          		}
			          		res.render('analytics', {
						    	needsToPay: needsToPay,
						    	cardOnFile: cardOnFile,
						    	hasSubscription: hasSubscription,
						    	impressions: imp,
						    	isAppyHour: isAppyHour,
						    	toDate: toDate,
						    	weekStart: weekStart,
						    	weekEnd: weekEnd
							});
			        	});
			      	} else {
			      		res.render('analytics', {
					    	needsToPay: needsToPay,
					    	cardOnFile: cardOnFile,
					    	hasSubscription: hasSubscription,
					    	impressions: imp,
						    isAppyHour: isAppyHour,
						    toDate: toDate,
						    weekStart: weekStart,
						    weekEnd: weekEnd
						});
			      	}
			    });
			} else {
				res.render('analytics', {
			    	needsToPay: needsToPay,
			    	cardOnFile: cardOnFile,
			    	hasSubscription: hasSubscription,
			    	impressions: imp,
					isAppyHour: isAppyHour,
					toDate: toDate,
					weekStart: weekStart,
					weekEnd: weekEnd
				});
			}
		} else {
			res.render('analytics', {
		    	needsToPay: needsToPay,
		    	cardOnFile: cardOnFile,
		    	hasSubscription: hasSubscription,
		    	impressions: imp,
				isAppyHour: isAppyHour,
				toDate: toDate,
				weekStart: weekStart,
				weekEnd: weekEnd
			});
		}
	}
}

function specialsThisWeek(specials, tz) {
	var thisWeek = [];

	var currFirst = Common.newDate(tz);
    currFirst.setHours(0,0,0,0);
    var currLast = Common.newDate(tz);
    currLast.setHours(4,0,0,0);

    var first = currFirst.getDate() - currFirst.getDay() + 1; 
    var last = first + 7;

    first = new Date(currFirst.setDate(first));
    last = new Date(currLast.setDate(last));

	for(var s in specials) {
		if(specials[s].day > -1) {
			thisWeek.push(specials[s]);
		}
		else {
			if(specials[s].start_time > first && specials[s].end_time < last) {
				thisWeek.push(specials[s]);
			}
		}
	}

	return thisWeek;
}

function orderSpecials(specials) {
	return specials;
}

function getDateStr(d, sStart, day) {
	var month;
	var date;
	var year;

	if(day == -1) {
		month = sStart.getMonth()+1;
		if(month < 10)
			month = "0" + month;
		date = sStart.getDate();
		if(date < 10)
			date = "0"+date;
		year = sStart.getYear()+1900;
	}
	else {
		if(day == 0)
			day = 7;
		var newDate = new Date(d);
		newDate.setDate(newDate.getDate() + day);

		month = newDate.getMonth()+1;
		if(month < 10)
			month = "0" + month;
		date = newDate.getDate();
		if(date < 10)
			date = "0"+date;
		year = newDate.getYear()+1900;
	}

	return month + "/" + date + "/" + year;
}

function getTimeStr(datetime) {
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}
		
	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
   return year + "-" + month + "-" + day;
}

function intDayToString(day) {
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    if (day >= 0 && day <= 6)
        return weekdays[day];
    else
        return "";
}

function dateToStr(d) {
	var month;
	var date;
	var year;
	var hours;
	var minutes;
	var ampm;

	month = d.getMonth()+1;
	if(month < 10)
		month = "0" + month;

	date = d.getDate();
	if(date < 10)
		date = "0"+date;

	year = d.getYear()+1900;

	hours = d.getHours();
	if(hours > 12) {
		hours = hours - 12;
		ampm = "PM";
	}
	else if(hours == 12) {
		ampm = "PM";
	}
	else if(hours < 12 && hours != 0) {
		ampm = "AM";
	}
	else if(hours == 0) {
		hours = 12;
		ampm = "AM";
	}

	minutes = d.getMinutes();
	if(minutes < 10) {
		minutes = "0" + minutes;
	}

	return month + "/" + date + "/" + year + " " + hours + ":" + minutes + " " + ampm;
}

function distance(lat1, lon1, lat2, lon2) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1/180;
		var radlat2 = Math.PI * lat2/180;
		var theta = lon1-lon2;
		var radtheta = Math.PI * theta/180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180/Math.PI;
		dist = dist * 60 * 1.1515;
		return dist;
	}
}
