var User = require('../models/user.js');
var bcrypt = require('bcrypt');

exports.show = function(req, res) {
	res.render('resetpassword');
};

exports.verifyFPKey = function(req, res) {
	var email = req.query.email;
	var fpKey = req.query.fpKey;

	User.getAllByEmail(email, function(errUser, user) {
		if(user.forgot_password == fpKey) {
			res.send("true");
		}
		else {
			res.send("false");
		}
	});
};

exports.resetPassword = function(req, res) {
	var email = req.body.email;
	var fpKey = req.body.fpKey;
	var pw = req.body.pw;

	User.getAllByEmail(email, function(errUser, user) {
		bcrypt.compare(pw, user.password, function(err, match) {
	    	if(match) {
	    		res.send("New password can't be your old password");
	    	}
	    	else {  
	    		if(user.forgot_password != fpKey) {	
	    			res.send("Your password has already been updated. If you need to update your password again, please click Forgot Password on the AppyHour login page.")
	    		}	
	    		else {
		    		bcrypt.hash(pw, 10, function(err2, hash) {
			          if(!err2) {
			            const u = { 
			              USER: {
			                user_id: user.user_id, 
			                password: hash,
			                forgot_password: null
			              }
			            };
			            User.update(u, function(err3, result) {
			              res.send("true");
			            });
			          }
			          else {
			          	res.send("false");
			          }
			        });
	    		}
	    	}
	    });
	});
}