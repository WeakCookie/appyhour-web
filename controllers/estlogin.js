var User = require('../models/user.js');
var Supplier = require('../models/supplier.js');

exports.show = function(req, res) {
	var email = req.query.email;
	var ceKey = req.query.ceKey;

	if(email != undefined && ceKey != undefined) {
		User.getAllByEmail(email, function(errUser, user) {
			if(user) {
				if(user.claim_email_key == ceKey) {
					res.render('estlogin', {
						supplierLogin: false,
						username: user.username,
						email: email,
						ceKeyMatch: true
					});
				}
				else {
					Supplier.getAllByEmail(email, function(errSupplier, supplier) {
						if(supplier) {
							if(supplier.claim_email_key == ceKey) {
								res.render('estlogin', {
									supplierLogin: true,
									username: supplier.username,
									email: email,
									supplierCEKeyMatch: true
								});
							}
							else {
								res.render('estlogin');
							}
						}
						else {
							res.render('estlogin');
						}
					});
				}
			}
			else {
				Supplier.getAllByEmail(email, function(errSupplier, supplier) {
					if(supplier) {
						if(supplier.claim_email_key == ceKey) {
							res.render('estlogin', {
								supplierLogin: true,
								username: supplier.username,
								email: email,
								supplierCEKeyMatch: true
							});
						}
						else {
							res.render('estlogin');
						}
					}
					else {
						res.render('estlogin');
					}
				});
			}
		});
	}
	else {
		res.render('estlogin');
	}
};