var Event = require('../models/event.js');
var EventArchive = require('../models/event_archive.js');
var EstablishmentEvent = require('../models/establishment_event.js');
var EventSpecial = require('../models/event_special.js');
var Special = require('../models/special.js');
var SpecialArchive = require('../models/special_archive.js');
var Establishment = require('../models/establishment.js');
var EstablishmentSpecial = require('../models/establishment_special.js');
var Common = require('../models/common.js');

var cardOnFile = false;
var hasSubscription = false;
var needsToPay = false;
var dateCanceled;
var cancelDate;
var weeklyEvents = 0;
var weeklyEventsRemaining = 0;

exports.show = function(req, res) {
	cardOnFile = false;
	hasSubscription = false;
	needsToPay = false;
	dateCanceled;
	cancelDate;
	weeklyEvents = 0;
	weeklyEventsRemaining = 0;

	Event.getAllByEstablishmentIdGroupRecurring(req.session.establishment.establishment_id, function(err, events) {
		var eventIds = "(0)";

		if(events && events.length > 0) {
			eventIds = "(";
			for(var i in events) {
				eventIds = eventIds + events[i].event_id;
				if(i < events.length-1) {
					eventIds = eventIds + ",";
				}
				else if(i == events.length-1) {
					eventIds = eventIds + ")";
				}

				var days = events[i].days.split(",");
				if(!days.includes("-1")) {
					weeklyEvents += days.length;
				}
			}
		}

		var curr = Common.newDate(req.session.tz);
		var weekStart = getYYYYMMDD(new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() - curr.getDay()+1));
		var eventStartDate = getYYYYMMDD(curr);
		var eventEndDate = getYYYYMMDD(curr);

		Special.getTodaysEventsByEventIds(eventIds, function(err, specials) {

			if(req.session.user.needs_to_pay == 1) {
				needsToPay = true;
				if(req.session.user.stripe_id) {
				    var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
  					//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
				    stripe.customers.retrieve(req.session.user.stripe_id, function(err, customer) {
				      	if(customer) {
				      		if(customer.subscriptions.data.length > 0) {
				          		hasSubscription = true;
				          		// customer's subscription will cancel
					            if(customer.subscriptions.data[0].cancel_at_period_end == true) {
					                dateCanceled = epochToDate(customer.subscriptions.data[0].canceled_at);
					                cancelDate = epochToDate(customer.subscriptions.data[0].current_period_end);
					            }
				          	}

				        	stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function(err, card) {
				          		if(card) {
				            		cardOnFile = true;
				          		}

				          		if(!needsToPay || (cardOnFile && hasSubscription))
				          			weeklyEventsRemaining = 3 - weeklyEvents;
				          		else
				          			weeklyEventsRemaining = 1 - weeklyEvents;

								res.render('events', {
									todaysDate: getTodaysDate(req.session.tz),
									todaysTime: getTodaysTime(req.session.tz),
									day: getTodaysDay(req.session.tz),
									todaysEvents: setupTodaysEventsTable(events, specials, req.session.tz),
									allEvents: setupManageEventsTable(events, specials, req.session.tz),
									specials: formatSpecials(specials),
									weeklyEvents: setupManageWeeklyEventsTable(events, specials, req.session.tz),
									weekStartEvents: weekStart,
									weekEndEvents: "+1y",
									eventStartDate: eventStartDate,
									eventEndDate: eventEndDate,
									needsToPay: needsToPay,
							    	cardOnFile: cardOnFile,
							    	hasSubscription: hasSubscription,
          							dateCanceled: dateCanceled,
          							cancelDate: cancelDate,
          							weeklyEventsRemaining: weeklyEventsRemaining
								});
							});
						}
						else {
							if(!needsToPay || (cardOnFile && hasSubscription))
				          		weeklyEventsRemaining = 3 - weeklyEvents;
				          	else
				          		weeklyEventsRemaining = 1 - weeklyEvents;

							res.render('events', {
								todaysDate: getTodaysDate(req.session.tz),
								todaysTime: getTodaysTime(req.session.tz),
								day: getTodaysDay(req.session.tz),
								todaysEvents: setupTodaysEventsTable(events, specials, req.session.tz),
								allEvents: setupManageEventsTable(events, specials, req.session.tz),
								specials: formatSpecials(specials),
								weeklyEvents: setupManageWeeklyEventsTable(events, specials, req.session.tz),
								weekStartEvents: weekStart,
								weekEndEvents: "+1y",
								eventStartDate: eventStartDate,
								eventEndDate: eventEndDate,
								needsToPay: needsToPay,
						    	cardOnFile: cardOnFile,
						    	hasSubscription: hasSubscription,
      							dateCanceled: dateCanceled,
      							cancelDate: cancelDate,
      							weeklyEventsRemaining: weeklyEventsRemaining
							});
						}
					});
				}
				else {
					if(!needsToPay || (cardOnFile && hasSubscription))
				        weeklyEventsRemaining = 3 - weeklyEvents;
				    else
				        weeklyEventsRemaining = 1 - weeklyEvents;

					res.render('events', {
						todaysDate: getTodaysDate(req.session.tz),
						todaysTime: getTodaysTime(req.session.tz),
						day: getTodaysDay(req.session.tz),
						todaysEvents: setupTodaysEventsTable(events, specials, req.session.tz),
						allEvents: setupManageEventsTable(events, specials, req.session.tz),
						specials: formatSpecials(specials),
						weeklyEvents: setupManageWeeklyEventsTable(events, specials, req.session.tz),
						weekStartEvents: weekStart,
						weekEndEvents: "+1y",
						eventStartDate: eventStartDate,
						eventEndDate: eventEndDate,
						needsToPay: needsToPay,
				    	cardOnFile: cardOnFile,
				    	hasSubscription: hasSubscription,
						dateCanceled: dateCanceled,
						cancelDate: cancelDate,
						weeklyEventsRemaining: weeklyEventsRemaining
					});
				}
			}
			else {
				if(!needsToPay || (cardOnFile && hasSubscription))
				    weeklyEventsRemaining = 3 - weeklyEvents;
				else
				    weeklyEventsRemaining = 1 - weeklyEvents;

				res.render('events', {
					todaysDate: getTodaysDate(req.session.tz),
					todaysTime: getTodaysTime(req.session.tz),
					day: getTodaysDay(req.session.tz),
					todaysEvents: setupTodaysEventsTable(events, specials, req.session.tz),
					allEvents: setupManageEventsTable(events, specials, req.session.tz),
					specials: formatSpecials(specials),
					weeklyEvents: setupManageWeeklyEventsTable(events, specials, req.session.tz),
					weekStartEvents: weekStart,
					weekEndEvents: "+1y",
					eventStartDate: eventStartDate,
					eventEndDate: eventEndDate,
					needsToPay: needsToPay,
			    	cardOnFile: cardOnFile,
			    	hasSubscription: hasSubscription,
					dateCanceled: dateCanceled,
					cancelDate: cancelDate,
					weeklyEventsRemaining: weeklyEventsRemaining
				});
			}
		});
	});
}



/* Schedule an event */

exports.scheduleEvent = function(req, res) {
	var estId = req.session.establishment.establishment_id;
	var now = Common.newDate(req.session.tz);
	var created = new Date(now);
	var reoccurring = req.query.recurring;
	var title = req.query.title;
	var details = req.query.details;
	var ev;
	var eventId = 0;

	if(reoccurring == "true") {
		var startTimeReoccurring = req.query.startTimeRecur.replace(':', '');
		var endTimeReoccurring = req.query.endTimeRecur.replace(':','');

		startTimeReoccurring = parseInt(startTimeReoccurring);
		endTimeReoccurring = parseInt(endTimeReoccurring);

		var startTime;
		var endTime;

		if(startTimeReoccurring >= 400)
			startTime = "1970-01-01 " + req.query.startTimeRecur + ":00";
		else
			startTime = "1970-01-02 " + req.query.startTimeRecur + ":00";

		if(endTimeReoccurring > 400)
			endTime = "1970-01-01 " + req.query.endTimeRecur + ":00";
		else
			endTime = "1970-01-02 " + req.query.endTimeRecur + ":00";

		var mon = req.query.mon;
		var tue = req.query.tue;
		var wed = req.query.wed;
		var thu = req.query.thu;
		var fri = req.query.fri;
		var sat = req.query.sat;
		var sund = req.query.sund;

		var days = "";
		for(var i = 0; i < 7; i++) {
			var day = -1;
			if(mon == "true" && i == 1)
				day = 1;
			if(tue == "true" && i == 2)
				day = 2;
			if(wed == "true" && i == 3)
				day = 3;
			if(thu == "true" && i == 4)
				day = 4;
			if(fri == "true" && i == 5)
				day = 5;
			if(sat == "true" && i == 6)
				day = 6;
			if(sund == "true" && i == 0)
				day = 0;

			if(day != -1) {
				days = days + day + ",";
			}
		}
		days = days.substring(0, days.length-1);

		var eventCount = days.split(",").length;

		Event.getRecurringEventsForEstId(estId, function(err, events) {
			if(events.length + eventCount <= 3) {
				ev = {
				    EVENT: {
				      title: title,
				      details: details,
				      days: days,
				      start_time: startTime,
				      end_time: endTime,
				      created_at: created,
			      	  updated_at: created
				    }
				};

				insertEvent(ev, estId, 0, 0, res, req.session.tz);
			}
			else {
				res.send({
	    			eventId: null,
	    			success: false,
	    			oneTimeEventLimit: false,
	    			recurringEventLimit: true,
	    			err: ""
	    		});
			}
		});
	}
	else {
		var dStr = req.query.startDate + " " + req.query.startTime;
		var sd = new Date(dStr);
		sd.setHours(0,0,0,0);

		//console.log(sd);
		//console.log(sd.getDay());

	    var first = 0;
	    if(sd.getDay() != 0)
	    	first = sd.getDate() - sd.getDay() + 1;
	    else
	    	first = sd.getDate() - 6;

	    //console.log(first);

	    var weekStart = new Date(sd.setDate(first));
	 	first = first + 7;
	    var weekEnd = new Date(sd.setDate(first));

	    //console.log(weekStart);
	    //console.log(weekEnd);

	    var parameters = [ weekStart, weekEnd ];

	    Event.getOneTimeEventsForWeek(parameters, function(err, events) {
	    	if(err) {
	    		res.send({
	    			eventId: null,
	    			success: false,
	    			oneTimeEventLimit: false,
	    			recurringEventLimit: false,
	    			err: err
	    		});
	    	}

	    	/*if(events.length > 0) {
	    		res.send({
	    			eventId: null,
	    			success: false,
	    			oneTimeEventLimit: true,
	    			recurringEventLimit: false,
	    			err: ""
	    		});
	    	}*/
	    	//else {
	    		var startDate = req.query.startDate;
				var startTime = req.query.startTime;
				var endDate = req.query.endDate;
				var endTime = req.query.endTime;

				var start = getDateTime(startTime, startDate);
				var end = getDateTime(endTime, endDate);

				ev = {
					EVENT: {
						title: title,
						details: details,
						days: -1,
						start_time: start,
						end_time: end,
						created_at: created,
						updated_at: created
					}
				}

				insertEvent(ev, estId, 0, 0, res, req.session.tz);
	    	//}
	    });
	}
}

exports.createEvent = function(req, res) {
	var estId = req.session.establishment.establishment_id;
	var now = Common.newDate(req.session.tz);
	var created = new Date(now);
	var reoccurring = req.body.eventReoccurring;
	var title = req.body.eventTitle;
	var details = req.body.eventDetails;
	var ev;

	if(reoccurring == "true") {
		var startTimeReoccurring = req.body.eventStartTimeReoccurring.replace(':', '');
		var endTimeReoccurring = req.body.eventEndTimeReoccurring.replace(':','');

		startTimeReoccurring = parseInt(startTimeReoccurring);
		endTimeReoccurring = parseInt(endTimeReoccurring);

		var startTime;
		var endTime;

		if(startTimeReoccurring >= 400)
			startTime = "1970-01-01 " + req.body.eventStartTimeReoccurring + ":00";
		else
			startTime = "1970-01-02 " + req.body.eventStartTimeReoccurring + ":00";

		if(endTimeReoccurring > 400)
			endTime = "1970-01-01 " + req.body.eventEndTimeReoccurring + ":00";
		else
			endTime = "1970-01-02 " + req.body.eventEndTimeReoccurring + ":00";

		var mon = req.body.createEventDayMon;
		var tue = req.body.createEventDayTue;
		var wed = req.body.createEventDayWed;
		var thu = req.body.createEventDayThu;
		var fri = req.body.createEventDayFri;
		var sat = req.body.createEventDaySat;
		var sund = req.body.createEventDaySun;

		for(var i = 0; i < 7; i++) {
			var day = -1;
			if(mon == "true" && i == 1)
				day = 1;
			if(tue == "true" && i == 2)
				day = 2;
			if(wed == "true" && i == 3)
				day = 3;
			if(thu == "true" && i == 4)
				day = 4;
			if(fri == "true" && i == 5)
				day = 5;
			if(sat == "true" && i == 6)
				day = 6;
			if(sund == "true" && i == 0)
				day = 0;

			if(day != -1) {
				ev = {
				    EVENT: {
				      title: title,
				      details: details,
				      day: day,
				      start_time: startTime,
				      end_time: endTime,
				      created_at: created,
			      	  updated_at: created
				    }
				};

				insertEvent(ev, estId, null, null, null, req.session.tz);
			}
		}
	}
	else {
		var startDate = req.body.eventStartDate;
		var startTime = req.body.eventStartTime;
		var endDate = req.body.eventEndDate;
		var endTime = req.body.eventEndTime;

		var start = getDateTime(startTime, startDate);
		var end = getDateTime(endTime, endDate);

		ev = {
			EVENT: {
				title: title,
				details: details,
				day: -1,
				start_time: start,
				end_time: end,
				created_at: created,
				updated_at: created
			}
		}

		insertEvent(ev, estId, null, null, null, req.session.tz);
	}

	res.redirect('dashboard?page=events');
}

function insertEvent(ev, estId, eventId, start, res, tz) {
	var now = Common.newDate(tz);
	var created = new Date(now);
	var days = ev["EVENT"].days;

	if(days == "-1") {
		var ev2 = {
			EVENT: {
				title: ev["EVENT"].title,
				details: ev["EVENT"].details,
				day: -1,
				start_time: ev["EVENT"].start_time,
				end_time: ev["EVENT"].end_time,
				created_at: created,
				updated_at: created
			}
		}

		Event.insert(ev2, function(err, result) {
			if(result.affectedRows > 0) {
				eventId = result.insertId;
				const establishment_event = {
				    ESTABLISHMENT_EVENT: {
				      establishment_id: estId,
				      event_id: eventId,
				      created_at: created,
				      updated_at: created
				    }
				};
				EstablishmentEvent.insert(establishment_event, function(err1, result1) {
					res.send({
		    			eventId: eventId,
		    			success: true,
		    			oneTimeEventLimit: false,
	    				recurringEventLimit: false,
		    			err: ""
		    		});
				});
			}
			else {
				res.send({
	    			eventId: null,
	    			success: false,
	    			oneTimeEventLimit: false,
	    			recurringEventLimit: false,
	    			err: ""
	    		});
			}
		});
	}
	else {
		days = days.split(",");
		if(start <= days.length-1) {
			var ev2 = {
				EVENT: {
					title: ev["EVENT"].title,
					details: ev["EVENT"].details,
					day: days[start++],
					start_time: ev["EVENT"].start_time,
					end_time: ev["EVENT"].end_time,
					created_at: created,
					updated_at: created
				}
			}

			Event.insert(ev2, function(err, result) {
				if(result.affectedRows > 0) {
					eventId = result.insertId;
					const establishment_event = {
					    ESTABLISHMENT_EVENT: {
					      establishment_id: estId,
					      event_id: eventId,
					      created_at: created,
					      updated_at: created
					    }
					};
					EstablishmentEvent.insert(establishment_event, function(err1, result1) {
						insertEvent(ev, estId, eventId, start, res, tz);
					});
				}
				else {
					res.send({
		    			eventId: null,
		    			success: false,
		    			oneTimeEventLimit: false,
	    				recurringEventLimit: false,
		    			err: ""
		    		});
				}
			});
		}
		else {
			res.send({
    			eventId: eventId,
    			success: true,
    			oneTimeEventLimit: false,
	    		recurringEventLimit: false,
    			err: ""
    		});
		}
	}
}



/* Today's events */

exports.editTodaysEvent = function(req, res) {
	var now = Common.newDate(req.session.tz);
	var updated = new Date(now);
	var id = req.body.hiddenTodaysEventId;
	var eventIds = req.body.eventId;

	var title;
	var details;
	var startDate;
	var start
	var end;
	var days
	var editMon, editTue, editWed, editThu, editFri, editSat, editSun;

	if(Array.isArray(eventIds)) {
		var row = 0;
		for(var i = 0; i < eventIds.length; i++) {
			if(eventIds[i] == id) {
				row = i;
			}
		}

		title = req.body.editEventTitle[row];
		details = req.body.editEventDetails[row];
		startDate = req.body.editEventStartDate[row];
		start = req.body.editEventStart[row];
		end = req.body.editEventEnd[row];
		days = req.body.hiddenTodaysEventDay[row];
		editMon = req.body.editEventDayMon[row];
		editTue = req.body.editEventDayTue[row];
		editWed = req.body.editEventDayWed[row];
		editThu = req.body.editEventDayThu[row];
		editFri = req.body.editEventDayFri[row];
		editSat = req.body.editEventDaySat[row];
		editSun = req.body.editEventDaySun[row];
	}
	else {
		title = req.body.editEventTitle;
		details = req.body.editEventDetails;
		startDate = req.body.editEventStartDate;
		start = req.body.editEventStart;
		end = req.body.editEventEnd;
		days = req.body.hiddenTodaysEventDay;
		editMon = req.body.editEventDayMon;
		editTue = req.body.editEventDayTue;
		editWed = req.body.editEventDayWed;
		editThu = req.body.editEventDayThu;
		editFri = req.body.editEventDayFri;
		editSat = req.body.editEventDaySat;
		editSun = req.body.editEventDaySun;
	}

	var dayArr = days.split(",");

	if(dayArr.length == 1 && dayArr[0] == "-1") {
		var s = getDateTime(start, startDate);
		var e = getDateTime(end, startDate);

		const ev = {
		    EVENT: {
		      event_id: id,
		      details: details,
		      day: -1,
		      start_time: s,
		      end_time: e,
	      	  updated_at: updated
		    }
		};

		Event.update(ev, function(err, result) {
			Special.getAllByEventId(id, function(err2, result2) {
				updateSpecials(result2, 0, s, e, req, res);
			});
		});
	}
	else {
		start = new Date("01 Jan 1970 "+start+" UTC"); // used to be CST
		end = new Date("01 Jan 1970 "+end+" UTC"); // used to be CST

		start.subtractHoursDST(req.session.tz);
		end.subtractHoursDST(req.session.tz);
		//start.setHours(start.getHours()-6); // UTC issue
		//end.setHours(end.getHours()-6); // UTC issue

		if(start.getHours() < 4) {
			start.setDate(2);
		} else {
			start.setDate(1);
		}
		if(end.getHours() < 4) {
			end.setDate(2);
		} else {
			end.setDate(1);
		}

		// fix for weird bug that sets date to 1969-12-02
		if(start.getYear() == 69 && start.getMonth() == 11) {
			start.setYear(70);
			start.setMonth(0);
		}
		if(end.getYear() == 69 && end.getMonth() == 11) {
			end.setYear(70);
			end.setMonth(0);
		}

		var parameters = [
			title,
			details,
			dayArr[0],
			start,
			end,
			editMon,
			editTue,
			editWed,
			editThu,
			editFri,
			editSat,
			editSun
		];

		var idsArr;
		try {
			idsArr = eventIds.split(",");
		}
		catch(e) {
			idsArr = eventIds;
		}
		Special.getAllByEventId(idsArr[0], function(err, result) {
			insertRecurringEvent(parameters, 0, result, req, res);
		});
	}
}

exports.deleteTodaysEvent = function(req, res) {
	var id = req.body.hiddenTodaysEventId;

	Event.getAllByEventId(id, function(err, result) {
		if(result.day == -1) {
			SpecialArchive.archiveByEventId(id, function(err5, result5) {
				Special.deleteByEventId(id, function(err2, result2) {
					//EventSpecial.deleteByEventId(id, function(err1, result1) {
						//EstablishmentEvent.deleteByEventId(id, function(err3, result3) {
							//if(result3.affectedRows > 0){
								Event.delete(id, function(err4, result4) {

									const ev = {
									    EVENT_ARCHIVE: {
									      event_id: result.event_id,
									      title: result.title,
									      details: result.details,
									      day: result.day,
									      start_time: result.start_time,
									      end_time: result.end_time,
									      deleted_at: Common.newDate(req.session.tz),
									      created_at: result.created_at,
								      	  updated_at: Common.newDate(req.session.tz)
									    }
									};

									EventArchive.insert(ev, function(err6, result6) {
									//if(result4.affectedRows > 0){
										res.redirect('dashboard?page=events');
									//}
									});
								});
							//} else {
							//	res.redirect('dashboard?page=events');
							//}
						//});
					//});
				});
			});
		}
		else {
			deleteRecurringEvent(result.title, 0, res, req.session.tz);
		}
	});
}

exports.addSpecialToTodaysEvent = function(req, res) {
	var estId = req.session.establishment.establishment_id;
	var now = Common.newDate(req.session.tz);
	var created = new Date(now);
	var eventId = req.query.eventId;
	var days = req.query.days;
	var startTime = req.query.startTime;
	var endTime = req.query.endTime;
	var priceType = req.query.priceType;
	var price = req.query.price;
	var details = req.query.details;
	var specialType = req.query.specialType;

	Event.getAllByEventId(eventId, function(err, result) {
		if(days == undefined) {
			days = [result.start_time];
			startTime = result.start_time.getHours() + ":" + result.start_time.getMinutes();
			endTime = result.end_time.getHours() + ":" + result.end_time.getMinutes();
		}

		if(result.day == -1) {
			const parameters = {
			  estId: estId,
			  eventId: eventId,
		      price: price,
		      price_type_ref_id: priceType,
		      details: details,
		      deal_type_ref_id: specialType,
		      day: -1,
		      startTime: startTime,
		      endTime: endTime,
		      created_at: created,
		      updated_at: created
			};

			insertSpecialToMultidayEvent(parameters, days, [], 0, res);
		}
		else {
			const parameters = {
			  estId: estId,
		      price: price,
		      price_type_ref_id: priceType,
		      details: details,
		      deal_type_ref_id: specialType,
		      start_time: result.start_time,
		      end_time: result.end_time,
		      created_at: created,
		      updated_at: created
			};

			insertSpecialToRecurringEvent(result.title, result.details, parameters, 0, 0, res);
		}
	});
}

function insertSpecialToMultidayEvent(parameters, days, specialIds, start, res) {
	if(start < days.length) {
		var startTime = new Date(days[start]);
		var startH = parameters.startTime.substring(0, parameters.startTime.indexOf(":"));
		var startM = parameters.startTime.substring(parameters.startTime.indexOf(":")+1);

		startTime.setHours(startH);
		startTime.setMinutes(startM);

		var endTime = new Date(days[start]);
		var endH = parameters.endTime.substring(0, parameters.endTime.indexOf(":"));
		var endM = parameters.endTime.substring(parameters.endTime.indexOf(":")+1);

		endTime.setHours(endH);
		endTime.setMinutes(endM);

		const special = {
		    SPECIAL: {
		      price: parameters.price,
		      price_type_ref_id: parameters.price_type_ref_id,
		      details: parameters.details,
		      deal_type_ref_id: parameters.deal_type_ref_id,
		      day: parameters.day,
		      start_time: startTime,
		      end_time: endTime,
		      created_at: parameters.created_at,
		      updated_at: parameters.updated_at
		    }
		};

		Special.insert(special, function(err1, result1) {
			if(result1.affectedRows > 0) {
				var specialId = result1.insertId;
				specialIds.push(specialId);

				const establishment_special = {
				    ESTABLISHMENT_SPECIAL: {
				      establishment_id: parameters.estId,
				      special_id: specialId,
				      created_at: parameters.created_at,
				      updated_at: parameters.created_at
				    }
				};

				EstablishmentSpecial.insert(establishment_special, function(err2, result2) {

					if(result2.affectedRows > 0) {

						const event_special = {
						    EVENT_SPECIAL: {
						      event_id: parameters.eventId,
						      special_id: specialId,
						      created_at: parameters.created_at,
						      updated_at: parameters.created_at
						    }
						};

						EventSpecial.insert(event_special, function(err3, result3) {
							if(result3.affectedRows > 0) {
								//res.send({ specialId: specialId });
								insertSpecialToMultidayEvent(parameters, days, specialIds, ++start, res);
							} else {
								res.send(false);
							}
						});
					} else {
						res.send(false);
					}
				});
			} else {
				res.send(false);
			}
		});
	}
	else {
		res.send({
			specialIds: specialIds
		});
	}

	/*

	const special = {
	    SPECIAL: {
	      price: parameters.price,
	      price_type_ref_id: parameters.price_type_ref_id,
	      details: parameters.details,
	      deal_type_ref_id: parameters.deal_type_ref_id,
	      day: parameters.day,
	      start_time: startTime,
	      end_time: endTime,
	      created_at: parameters.created_at,
	      updated_at: parameters.updated_at
	    }
	};

	Special.insert(special, function(err1, result1) {
		if(result1.affectedRows > 0) {
			var specialId = result1.insertId;
			const establishment_special = {
			    ESTABLISHMENT_SPECIAL: {
			      establishment_id: estId,
			      special_id: specialId,
			      created_at: created,
			      updated_at: created
			    }
			};
			EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
				if(result2.affectedRows > 0) {
					const event_special = {
					    EVENT_SPECIAL: {
					      event_id: eventId,
					      special_id: specialId,
					      created_at: created,
					      updated_at: created
					    }
					};
					EventSpecial.insert(event_special, function(err3, result3) {
						if(result3.affectedRows > 0) {
							res.send({ specialId: specialId });
						} else {
							res.send(false);
						}
					});
				} else {
					res.send(false);
				}
			});
		} else {
			res.send(false);
		}
	});
	*/
}

function insertSpecialToRecurringEvent(title, details, parameters, start, specialId, res) {
	if(start < 7) {
		var newParameters = [ title, details, start ];
		Event.getEventIdForRecurring(newParameters, function(err, result) {
			if(result.length > 0) {
				const special = {
				    SPECIAL: {
				      price: parameters.price,
				      price_type_ref_id: parameters.price_type_ref_id,
				      details: parameters.details,
				      deal_type_ref_id: parameters.deal_type_ref_id,
				      day: start,
				      start_time: parameters.start_time,
				      end_time: parameters.end_time,
				      created_at: parameters.created_at,
				      updated_at: parameters.updated_at
				    }
				};

				Special.insert(special, function(err1, result1) {
					if(result1.affectedRows > 0) {
						var specialId = result1.insertId;
						const establishment_special = {
						    ESTABLISHMENT_SPECIAL: {
						      establishment_id: parameters.estId,
						      special_id: specialId,
						      created_at: parameters.created_at,
						      updated_at: parameters.updated_at
						    }
						};
						EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
							if(result2.affectedRows > 0) {
								const event_special = {
								    EVENT_SPECIAL: {
								      event_id: result[0].event_id,
								      special_id: specialId,
								      created_at: parameters.created_at,
						      		  updated_at: parameters.updated_at
								    }
								};
								EventSpecial.insert(event_special, function(err3, result3) {
									if(result3.affectedRows > 0) {
										insertSpecialToRecurringEvent(title, details, parameters, ++start, specialId, res);
									} else {
										res.send(false);
									}
								});
							} else {
								res.send(false);
							}
						});
					} else {
						res.send(false);
					}
				});
			}
			else {
				insertSpecialToRecurringEvent(title, details, parameters, ++start, specialId, res);
			}
		});
	}
	else {
		res.send({
			specialId: specialId
		});
	}
}

function setupTodaysEventsTable(events, specials, tz) {
	var eventsResponse = [];
	var n = getTodaysDayInt(tz);
	var now = Common.newDate(tz);
	if(now.getHours() < 4)
		now.setDate(now.getDate()+1);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);

	var expired = [];
  	var current = [];
  	var upcoming = [];

	for(var i in events) {
		var e = events[i];
		e.specials = [];

		if(isToday(e.days, e.start_time, e.end_time, tz)) {
			// weekly
			if(isToday(e.days, null, null, tz)) {
				if(e.start_time.getHours() <= 4) {
					e.start_time.setMonth(tmw.getMonth());
					e.start_time.setDate(tmw.getDate());
					e.start_time.setYear(1900+tmw.getYear());
				}
				else {
					e.start_time.setMonth(now.getMonth());
					e.start_time.setDate(now.getDate());
					e.start_time.setYear(1900+now.getYear());
				}

				if(e.end_time.getHours() <= 4) {
					e.end_time.setMonth(tmw.getMonth());
					e.end_time.setDate(tmw.getDate());
					e.end_time.setYear(1900+tmw.getYear());
				}
				else {
					e.end_time.setMonth(now.getMonth());
					e.end_time.setDate(now.getDate());
					e.end_time.setYear(1900+now.getYear());
				}

				e.reoccurringStr = getReoccurringStr(e.days);

				e.editEventDayMon = false;
				e.editEventDayTue = false;
				e.editEventDayWed = false;
				e.editEventDayThu = false;
				e.editEventDayFri = false;
				e.editEventDaySat = false;
				e.editEventDaySun = false;

				var days = e.days.split(",");
				for(var x = 0; x < 7; x++) {
					for(var d in days) {
						if(days[d] == x && x == 0) {
							e.editEventDaySun = true;
						}
						else if(days[d] == x && x == 1) {
							e.editEventDayMon = true;
						}
						else if(days[d] == x && x == 2) {
							e.editEventDayTue = true;
						}
						else if(days[d] == x && x == 3) {
							e.editEventDayWed = true;
						}
						else if(days[d] == x && x == 4) {
							e.editEventDayThu = true;
						}
						else if(days[d] == x && x == 5) {
							e.editEventDayFri = true;
						}
						else if(days[d] == x && x == 6) {
							e.editEventDaySat = true;
						}
					}
				}
			}

			//e.eventDate = getEventDate(e.start_time, tz);
			e.duration = formatStartEndTime(e.start_time) + " - " + formatStartEndTime(e.end_time);

			e.start = getISOString(e.start_time);
			e.end = getISOString(e.end_time);

			e.startDate = e.start.substring(0, e.start.indexOf("T"));
			e.startTime = e.start.substring(e.start.indexOf("T")+1);
			e.endTime = e.end.substring(e.end.indexOf("T")+1);

			for(var s in specials) {
				var special = specials[s];
				if(special.event_id == e.event_id) {
					special.deal_type_ref_id_TS = getDealTypeImageByRefId(special.deal_type_ref_id);
					e.specials.push(special);
				}
			}

			// expired
			if(e.end_time < now) {
				e.expired = true;
				e.current = false;
				expired.push(e);
			}
			// current
			else if(e.start_time < now && e.end_time > now) {
				e.timeremaining = dateDiff(e.end_time, tz);
				e.current = true;
				current.push(e);
			}
			// upcoming
			else if(e.start_time > now) {
				e.upcoming = true;
				e.current = false;
				upcoming.push(e);
			}
		}
	}

	expired = sortByStartTimeThenEndTime(expired);
	current = sortByEndTime(current);
	upcoming = sortByStartTimeThenEndTime(upcoming);

	for (c in current)
		eventsResponse.push(current[c]);

	for (u in upcoming)
		eventsResponse.push(upcoming[u]);

	for (ex in expired)
		eventsResponse.push(expired[ex]);

	return eventsResponse;
}



/* Manage upcoming events */

function setupManageEventsTable(events, specials, tz) {
	var eventsResponse = [];
	var n = getTodaysDayInt(tz);
	var now = Common.newDate(tz);
	if(now.getHours() < 4)
		now.setDate(now.getDate()+1);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);

	var expired = [];
  	var current = [];
  	var upcoming = [];

	for(var i in events) {
		var e = events[i];
		e.specials = [];

		if(e.days == "-1") {
			//e.eventDate = getEventDate(e.start_time, tz);
			e.duration = formatStartEndTime(e.start_time) + " - " + formatStartEndTime(e.end_time);

			e.durationStartDate = getEventDate(e.start_time, tz);
			e.durationStartTime = formatStartEndTime(e.start_time);
			e.durationEndDate = getEventDate(e.end_time, tz);
			e.durationEndTime = formatStartEndTime(e.end_time);
			if(e.start_time.getDate() != e.end_time.getDate() || e.start_time.getMonth() != e.end_time.getMonth() || e.start_time.getYear() != e.end_time.getYear()) {
				e.multiday = true;
			}
			else {
				e.multiday = false;
			}

			e.start = getISOString(e.start_time);
			e.end = getISOString(e.end_time);

			e.startDate = e.start.substring(0, e.start.indexOf("T"));
			e.startTime = e.start.substring(e.start.indexOf("T")+1);
			e.endTime = e.end.substring(e.end.indexOf("T")+1);

			for(var s in specials) {
				var special = specials[s];
				if(special.event_id == e.event_id) {
					special.deal_type_ref_id_TS = getDealTypeImageByRefId(special.deal_type_ref_id);
					e.specials.push(special);
				}
			}

			// current
			if(e.start_time < now && e.end_time > now) {
				e.timeremaining = dateDiff(e.end_time, tz);
				e.current = true;
				current.push(e);
			}
			// upcoming
			else if(e.start_time > now) {
				e.upcoming = true;
				e.current = false;
				upcoming.push(e);
			}
		}
	}

	current = sortByEndTime(current);
	upcoming = sortByStartTimeThenEndTime(upcoming);

	for (c in current)
		eventsResponse.push(current[c]);

	for (u in upcoming)
		eventsResponse.push(upcoming[u]);

	return eventsResponse;
}

function setupManageWeeklyEventsTable(events, specials, tz) {
	var eventsResponse = [];
	var n = getTodaysDayInt(tz);
	var now = Common.newDate(tz);
	if(now.getHours() < 4)
		now.setDate(now.getDate()+1);

	// we need tmw's date so we can assign to daily specials that run past midnight
	var tmw = Common.newDate(tz);
	tmw.setDate(tmw.getDate()+1);

	//var expired = [];
  	var current = [];
  	var upcoming = [];

	for(var i in events) {
		var e = events[i];
		e.specials = [];

		if(e.days != "-1") {
			if(e.start_time.getHours() <= 4) {
				e.start_time.setMonth(tmw.getMonth());
				e.start_time.setDate(tmw.getDate());
				e.start_time.setYear(1900+tmw.getYear());
			}
			else {
				e.start_time.setMonth(now.getMonth());
				e.start_time.setDate(now.getDate());
				e.start_time.setYear(1900+now.getYear());
			}

			if(e.end_time.getHours() <= 4) {
				e.end_time.setMonth(tmw.getMonth());
				e.end_time.setDate(tmw.getDate());
				e.end_time.setYear(1900+tmw.getYear());
			}
			else {
				e.end_time.setMonth(now.getMonth());
				e.end_time.setDate(now.getDate());
				e.end_time.setYear(1900+now.getYear());
			}

			e.reoccurringStr = getReoccurringStr(e.days);

			e.editEventDayMon = false;
			e.editEventDayTue = false;
			e.editEventDayWed = false;
			e.editEventDayThu = false;
			e.editEventDayFri = false;
			e.editEventDaySat = false;
			e.editEventDaySun = false;

			var days = e.days.split(",");
			for(var x = 0; x < 7; x++) {
				for(var d in days) {
					if(days[d] == x && x == 0) {
						e.editEventDaySun = true;
					}
					else if(days[d] == x && x == 1) {
						e.editEventDayMon = true;
					}
					else if(days[d] == x && x == 2) {
						e.editEventDayTue = true;
					}
					else if(days[d] == x && x == 3) {
						e.editEventDayWed = true;
					}
					else if(days[d] == x && x == 4) {
						e.editEventDayThu = true;
					}
					else if(days[d] == x && x == 5) {
						e.editEventDayFri = true;
					}
					else if(days[d] == x && x == 6) {
						e.editEventDaySat = true;
					}
				}
			}


			//e.eventDate = getEventDate(e.start_time, tz);
			e.duration = formatStartEndTime(e.start_time) + " - " + formatStartEndTime(e.end_time);

			e.durationStartDate = getEventDate(e.start_time, tz);
			e.durationStartTime = formatStartEndTime(e.start_time);
			e.durationEndDate = getEventDate(e.end_time, tz);
			e.durationEndTime = formatStartEndTime(e.end_time);
			if(e.start_time.getDate() != e.end_time.getDate() || e.start_time.getMonth() != e.end_time.getMonth() || e.start_time.getYear() != e.end_time.getYear()) {
				e.multiday = true;
			}
			else {
				e.multiday = false;
			}

			e.start = getISOString(e.start_time);
			e.end = getISOString(e.end_time);

			e.startDate = e.start.substring(0, e.start.indexOf("T"));
			e.startTime = e.start.substring(e.start.indexOf("T")+1);
			e.endTime = e.end.substring(e.end.indexOf("T")+1);

			for(var s in specials) {
				var special = specials[s];
				if(special.event_id == e.event_id) {
					special.deal_type_ref_id_TS = getDealTypeImageByRefId(special.deal_type_ref_id);
					e.specials.push(special);
				}
			}

			var curr = Common.newDate(tz);

			var actualStart = getDatesThisWeek(e.start_time, e.days, tz);
			var actualEnd = getDatesThisWeek(e.end_time, e.days, tz);

			var active = false;
			for(var a in actualStart) {
				if(actualStart[a].getTime() <= curr.getTime() && actualEnd[a].getTime() >= curr.getTime()) {
					active = true;
					break;
				}
			}

			// expired
			/*if(e.end_time < now) {
				e.expired = true;
				e.current = false;
				expired.push(e);
			}*/
			if(active) {
				e.timeremaining = dateDiff(e.end_time, tz);
				e.current = true;
				current.push(e);
			}
			else {
				e.upcoming = true;
				e.current = false;
				upcoming.push(e);
			}
		}
	}

	//expired = sortByStartTimeThenEndTime(expired);
	current = sortByEndTime(current);
	upcoming = sortByStartTimeThenEndTime(upcoming);

	for (c in current)
		eventsResponse.push(current[c]);

	for (u in upcoming)
		eventsResponse.push(upcoming[u]);

	//for (ex in expired)
	//	eventsResponse.push(expired[ex]);

	return eventsResponse;
}

exports.loadEventsForWeek = function(req, res) {
	Event.getAllByEstablishmentId(req.session.establishment.establishment_id, function(err, events) {
		Special.getAllByEstablishmentId(req.session.establishment.establishment_id, function(err2, specials) {
			var now = Common.newDate(req.session.tz);
			var tmw = Common.newDate(req.session.tz);
			tmw.setDate(tmw.getDate()+1);
			var week = parseInt(req.query.week);
			var thisWeekEvents = [];
			var expired = [];
			var current = [];
			var upcoming = []

			for(var i in events) {
				var start = new Date(req.query.startDate);
				var end = new Date(req.query.endDate);

				if(events[i].day > -1) {
					if(events[i].start_time.getHours() <= 4) {
						events[i].start_time.setMonth(tmw.getMonth());
						events[i].start_time.setDate(tmw.getDate());
						events[i].start_time.setYear(1900+tmw.getYear());
					}
					else {
						events[i].start_time.setMonth(now.getMonth());
						events[i].start_time.setDate(now.getDate());
						events[i].start_time.setYear(1900+now.getYear());
					}

					if(events[i].end_time.getHours() <= 4) {
						events[i].end_time.setMonth(tmw.getMonth());
						events[i].end_time.setDate(tmw.getDate());
						events[i].end_time.setYear(1900+tmw.getYear());
					}
					else {
						events[i].end_time.setMonth(now.getMonth());
						events[i].end_time.setDate(now.getDate());
						events[i].end_time.setYear(1900+now.getYear());
					}
				}

				if(events[i].day == -1) {
					if(events[i].start_time > start && events[i].start_time < end) {
						if(week == 0) {
							// expired
							if(events[i].end_time < now) {
								events[i].expired = true;
								events[i].current = false;
								expired.push(events[i]);
							}
							// current
							else if(events[i].start_time < now && events[i].end_time > now) {
								events[i].timeremaining = dateDiff(events[i].end_time, req.session.tz);
								events[i].current = true;
								current.push(events[i]);
							}
							// upcoming
							else if(events[i].start_time > now) {
								events[i].upcoming = true;
								events[i].current = false;
								upcoming.push(events[i]);
							}
						}
						else if(week > 0) {
							events[i].upcoming = true;
							events[i].current = false;
							upcoming.push(events[i]);
						}
						else if(week < 0) {
							events[i].expired = true;
							events[i].current = false;
							expired.push(events[i]);
						}
					}
				}
				else {
					if(week == 0) {
						var n = getTodaysDayInt(req.session.tz) == 0 ? 7 : getTodaysDayInt(req.session.tz);
						var eventDay = events[i].day == 0 ? 7 : events[i].day;

						if(eventDay == n) {
							var startt = Common.newDate(req.session.tz);
							if(startt.getHours() < 4)
								startt.setDate(startt.getDate()-1);
							startt.setHours(events[i].start_time.getHours());
							startt.setMinutes(events[i].start_time.getMinutes());
							startt.setSeconds(0);
							var endd = Common.newDate(req.session.tz);
							if(endd.getHours() < 4)
								endd.setDate(endd.getDate()-1);
							endd.setHours(events[i].end_time.getHours());
							endd.setMinutes(events[i].end_time.getMinutes());
							endd.setSeconds(0);

							if(startt.getHours() < 4) {
								startt.setDate(startt.getDate()+1);
							}
							if(endd.getHours() < 4) {
								endd.setDate(endd.getDate()+1);
							}

							// expired
							if(endd < now) {
								events[i].expired = true;
								events[i].current = false;
								expired.push(events[i]);
							}
							// current
							else if(startt < now && endd > now) {
								events[i].timeremaining = dateDiff(events[i].end_time, req.session.tz);
								events[i].current = true;
								current.push(events[i]);
							}
							// upcoming
							else if(startt > now) {
								events[i].upcoming = true;
								events[i].current = false;
								upcoming.push(events[i]);
							}
						}
						// upcoming
						else if(eventDay > n) {
							events[i].upcoming = true;
							events[i].current = false;
							upcoming.push(events[i]);
						}
						// expired
						else if(eventDay < n) {
							events[i].expired = true;
							events[i].current = false;
							expired.push(events[i]);
						}
					}
					else if(week > 0) {
						events[i].upcoming = true;
						events[i].current = false;
						upcoming.push(events[i]);
					}
					else if(week < 0) {
						var weekDates = getWeekDates(week, req.session.tz);
						var eventDay = events[i].day == 0 ? 6 : (events[i].day - 1);

						if(eventDay < weekDates.length) {
							var eventDate = weekDates[eventDay];
							var createdAt = new Date(events[i].created_at);
							createdAt.setHours(0,0,0,0);

							if(eventDate >= createdAt) {
								events[i].expired = true;
								events[i].current = false;
								expired.push(events[i]);
							}
						}
						else {
							events[i].expired = true;
							events[i].current = false;
							expired.push(events[i]);
						}
					}
				}

			}

			expired = sortByStartTimeThenEndTime(expired);
			current = sortByEndTime(current);
			upcoming = sortByStartTimeThenEndTime(upcoming);

			for(var ex in expired)
				thisWeekEvents.push(expired[ex]);

			for(var c in current)
				thisWeekEvents.push(current[c]);

			for(var u in upcoming)
				thisWeekEvents.push(upcoming[u]);

			for(var twe in thisWeekEvents) {
				thisWeekEvents[twe].specials = [];
				for(var s in specials) {
					if(specials[s].event_id == thisWeekEvents[twe].event_id) {
						thisWeekEvents[twe].specials.push(specials[s]);
					}
				}
			}

			res.json(thisWeekEvents);
		});
	});
}

exports.loadEvent = function(req, res) {
	var eventId = req.query.eventId;
	var ev;

	Event.getAllGrouped(function(err, result) {
		for(var r in result) {
			var eventIds = result[r].event_ids;
			eventIds = eventIds.split(",");
			if(eventIds.includes(eventId)) {
				ev = result[r];

				ev.start_time.addHoursDST(req.session.tz);
				ev.end_time.addHoursDST(req.session.tz);

				if(ev.day == -1) {
					ev.start_time.setHours(ev.start_time.getHours()-1);
					ev.end_time.setHours(ev.end_time.getHours()-1);
				}

				if(!needsToPay || (cardOnFile && hasSubscription))
					ev.eventSpecialLimit = 3;
				else
					ev.eventSpecialLimit = 1;

				break;
			}
		}
		Special.getAllByEventIdGrouped(eventId, function(err2, result2) {
			ev.specials = [];
			for(var r2 in result2) {
				ev.specials.push(result2[r2]);
			}
			res.json(ev);
		});
	});
}

exports.validateEvent = function(req, res) {
	var eventCount = parseInt(req.query.eventCount);
	var eventIds = req.query.eventIds;
	eventIds = eventIds.split(",");

	Establishment.getAllByEventId(eventIds[0], function(errEst, est) {
		Event.getRecurringEventsForEstId(est.establishment_id, function(err, events) {
			for(var e in events) {
				for(var eid in eventIds) {
					if(eventIds[eid] == events[e].event_id) {
						eventCount--;
					}
				}
			}

			if(events.length + eventCount <= 3) {
				res.send(true);
			}
			else {
				res.send(false);
			}
		});
	});
}

exports.editEvent = function(req, res) {
	var ids = req.body.eventIds;
	var days = req.body.days;
	var title = req.body.title;
	var details = req.body.details;
	var origDetails = req.body.origDetails;
	var startDate = req.body.startDate;
	//var startDateTime = req.body.startDateTime;
	//var endDateTime = req.body.endDateTime;
	var startTime = req.body.startTime;
	var endDate = req.body.endDate;
	var endTime = req.body.endTime;
	var mon = req.body.eventDayMon;
	var tue = req.body.eventDayTue;
	var wed = req.body.eventDayWed;
	var thu = req.body.eventDayThu;
	var fri = req.body.eventDayFri;
	var sat = req.body.eventDaySat;
	var sund = req.body.eventDaySun;
	var updated = Common.newDate(req.session.tz);

	// one-time
	if(days == "-1") {
		var s = getDateTime(startTime, startDate);
		var e = getDateTime(endTime, endDate);

		const ev = {
		    EVENT: {
		      event_id: ids,
		      details: details,
		      day: -1,
		      start_time: s,
		      end_time: e,
	      	  updated_at: updated
		    }
		};

		//console.log(s + " - " + e);
		//console.log(daysBetween(s, e));

		//console.log("EVENT START");
		//console.log(s);
		//console.log("EVENT END");
		//console.log(e);

		Event.update(ev, function(err, result) {
			Special.getAllByEventId(ids, function(err2, result2) {
				if(daysBetween(s, e) == 0)
					updateSpecials(result2, 0, s, e, req, res);
				else
					updateSpecialsMultiday(result2, 0, s, e, req, res);
			});
		});
	}
	// recurring
	else {
		var start = new Date("01 Jan 1970 "+startTime+" UTC");
		var end = new Date("01 Jan 1970 "+endTime+" UTC");

		//console.log("tz: " + req.session.tz);
		//console.log("-");
		//console.log(start);
		//console.log(end);
		//console.log("-");

		//start.subtractHoursDST(req.session.tz);
		//end.subtractHoursDST(req.session.tz);
		//start.setHours(start.getHours()-6); // UTC issue
		//end.setHours(end.getHours()-6); // UTC issue

		//console.log(start);
		//console.log(end);


		if(start.getHours() < 4) {
			start.setDate(2);
		} else {
			start.setDate(1);
		}
		if(end.getHours() < 4) {
			end.setDate(2);
		} else {
			end.setDate(1);
		}

		// fix for weird bug that sets date to 1969-12-02
		if(start.getYear() == 69 && start.getMonth() == 11) {
			start.setYear(70);
			start.setMonth(0);
		}
		if(end.getYear() == 69 && end.getMonth() == 11) {
			end.setYear(70);
			end.setMonth(0);
		}

		var parameters = [
			title,
			details,
			days.split(",")[0],
			start,
			end,
			mon,
			tue,
			wed,
			thu,
			fri,
			sat,
			sund,
			origDetails
		];

		var idsArr = ids.split(",");

		Special.getAllByEventId(idsArr[0], function(err, result) {
			insertRecurringEvent(parameters, 0, result, req, res);
		});
	}
}

exports.deleteEvent = function(req, res) {
	var ids = req.body.eventIds;
	var id = ids.split(",")[0];

	Event.getAllByEventId(id, function(err, result) {
		if(result.day == -1) {
			SpecialArchive.archiveByEventId(id, function(err5, result5) {
				Special.deleteByEventId(id, function(err2, result2) {
					//EventSpecial.deleteByEventId(id, function(err1, result1) {
						//EstablishmentEvent.deleteByEventId(id, function(err3, result3) {
							//if(result3.affectedRows > 0){
								Event.delete(id, function(err4, result4) {

									const ev = {
									    EVENT_ARCHIVE: {
									      event_id: result.event_id,
									      title: result.title,
									      details: result.details,
									      day: result.day,
									      start_time: result.start_time,
									      end_time: result.end_time,
									      deleted_at: Common.newDate(req.session.tz),
									      created_at: result.created_at,
								      	  updated_at: Common.newDate(req.session.tz)
									    }
									};

									EventArchive.insert(ev, function(err6, result6) {

									//if(result4.affectedRows > 0){
										res.redirect('dashboard?page=events');
									//}
									});
								});
							//} else {
							//	res.redirect('dashboard?page=events');
							//}
						//});
					//});
				});
			});
		}
		else {
			deleteRecurringEvent(result.title, 0, res, req.session.tz);
		}
	});
}



/* Shared */

function insertRecurringEvent(parameters, start, specials, req, res) {
	if(start < 7) {
		var now = Common.newDate(req.session.tz);
		var updated = new Date(now);
		var editMon = parameters[5];
		var editTue = parameters[6];
		var editWed = parameters[7];
		var editThu = parameters[8];
		var editFri = parameters[9];
		var editSat = parameters[10];
		var editSun = parameters[11];

		var newParameters = [
			parameters[0],
			parameters[12], //origDetails
			start
		];

		Event.getEventIdForRecurring(newParameters, function(err, results) {
			var inDayArr = false;
			if(start == 0 && editSun == 'true')
				inDayArr = true;
			else if(start == 1 && editMon == 'true')
				inDayArr = true;
			else if(start == 2 && editTue == 'true')
				inDayArr = true;
			else if(start == 3 && editWed == 'true')
				inDayArr = true;
			else if(start == 4 && editThu == 'true')
				inDayArr = true;
			else if(start == 5 && editFri == 'true')
				inDayArr = true;
			else if(start == 6 && editSat == 'true')
				inDayArr = true;

			// in database
			if(results && results.length > 0) {
				// in database and selected on form
				if(inDayArr) {
					const ev = {
					    EVENT: {
					      event_id: results[0].event_id,
					      details: parameters[1], // new details
					      day: start,
					      start_time: parameters[3],
					      end_time: parameters[4],
				      	  updated_at: updated
					    }
					};

					Event.update(ev, function(err2, result2) {
						Special.getAllByEventId(results[0].event_id, function(err3, result3) {
							updateSpecialsRecurring(result3, 0, parameters, start, specials, req, res);
						});
					});
				}
				// in database but not selected on form
				else {
					var id = results[0].event_id;
					Special.deleteByEventId(id, function(err3, result3) {
						EventSpecial.deleteByEventId(id, function(err2, result2) {
							EstablishmentEvent.deleteByEventId(id, function(err4, result4) {
								if(result4.affectedRows > 0){
									Event.delete(id, function(err5, result5) {
										//if(result5.affectedRows > 0){
											insertRecurringEvent(parameters, ++start, specials, req, res);
										//}
									});
								} else {
									insertRecurringEvent(parameters, ++start, specials, req, res);
								}
							});
						});
					});
				}
			}
			// not in database
			else {
				// not in database but selected on form
				if(inDayArr) {
					const ev = {
					    EVENT: {
					      title: parameters[0],
					      details: parameters[1],
					      day: start,
					      start_time: parameters[3],
					      end_time: parameters[4],
				      	  updated_at: updated
					    }
					};

					Event.insert(ev, function(err2, result2) {
						if(result2.affectedRows > 0) {
							const establishment_event = {
							    ESTABLISHMENT_EVENT: {
							      establishment_id: req.session.establishment.establishment_id,
							      event_id: result2.insertId,
							      created_at: updated,
							      updated_at: updated
							    }
							};
							EstablishmentEvent.insert(establishment_event, function(err3, result3) {
								addSpecialsRecurring(result2.insertId, 0, parameters, start, specials, req, res);
							});
						}
						else {
							insertRecurringEvent(parameters, ++start, specials, req, res);
						}
					});
				}
				// not in database and not selected on form
				else {
					insertRecurringEvent(parameters, ++start, specials, req, res);
				}
			}
		});
	}
	else {
		res.redirect('dashboard?page=events');
	}
}

function deleteRecurringEvent(title, start, res, tz) {
	if(start < 7) {
		var newParameters = [ title, start ];

		Event.getAllForRecurring(newParameters, function(err, result) {
			if(result.length > 0) {
				SpecialArchive.archiveByEventId(result[0].event_id, function(err5, result5) {
					Special.deleteByEventId(result[0].event_id, function(err2, result2) {
						//EventSpecial.deleteByEventId(result[0].event_id, function(err1, result1) {
							//EstablishmentEvent.deleteByEventId(result[0].event_id, function(err3, result3) {
								//if(result3.affectedRows > 0){
									Event.delete(result[0].event_id, function(err4, result4) {
										var now = Common.newDate(tz);
										const ev = {
										    EVENT_ARCHIVE: {
										      event_id: result[0].event_id,
										      title: result[0].title,
										      details: result[0].details,
										      day: result[0].day,
										      start_time: result[0].start_time,
										      end_time: result[0].end_time,
										      deleted_at: Common.newDate(tz),
										      created_at: result[0].created_at,
									      	  updated_at: Common.newDate(tz)
										    }
										};

										EventArchive.insert(ev, function(err6, result6) {


										//if(result4.affectedRows > 0){
											deleteRecurringEvent(title, ++start, res, tz);
										//}
										});
									});
								//} else {
								//	deleteRecurringEvent(title, ++start, res, tz);
								//}
							//});
						//});
					});
				});
			}
			else {
				deleteRecurringEvent(title, ++start, res, tz);
			}
		});
	}
	else {
		res.redirect('dashboard?page=events');
	}
}

exports.deleteSpecialFromEvent = function(req, res) {
	var now = Common.newDate(req.session.tz);
	var deleted = new Date(now);
	var id = req.query.specialId;

	Special.getAllBySpecialId(id, function(err, s) {
		if(s.day == -1) {
			Special.getEventSpecialsBySpecialId(s.special_id, function(err, eventSpecials) {
				if(eventSpecials && eventSpecials.length > 1) {
					var sToDelete = [];
					for(var es in eventSpecials) {
						if(s.price == eventSpecials[es].price && s.price_type_ref_id == eventSpecials[es].price_type_ref_id &&
						   s.details == eventSpecials[es].details && s.deal_type_ref_id == eventSpecials[es].deal_type_ref_id &&
						   s.start_time.getHours() == eventSpecials[es].start_time.getHours() &&
						   s.start_time.getMinutes() == eventSpecials[es].start_time.getMinutes() &&
						   s.end_time.getHours() == eventSpecials[es].end_time.getHours() &&
						   s.end_time.getMinutes() == eventSpecials[es].end_time.getMinutes()) {
								sToDelete.push(eventSpecials[es]);
						}
					}
					deleteSpecialFromRecurringEvent(sToDelete, 0, [], res, req.session.tz);
				}
				else {
					const special = {
					    SPECIAL_ARCHIVE: {
					    	special_id: s.special_id,
					      	price: s.price,
					      	price_type_ref_id: s.price_type_ref_id,
					      	details: s.details,
					      	deal_type_ref_id: s.deal_type_ref_id,
					      	day: s.day,
					      	start_time: s.start_time,
					      	end_time: s.end_time,
					      	deleted_at: deleted,
					      	created_at: s.created_at,
					      	updated_at: s.updated_at
					    }
					};

					SpecialArchive.insert(special, function(err1, result1) {
						if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
							Special.delete(id, function(err2, result2) {
								if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
									//EventSpecial.deleteBySpecialId(id, function(err3, result3) {
										//if(result3 != null && result3 != undefined && result3.affectedRows > 0) {
											res.send([id]);
										//}
									//});
								} else {
									res.send(false);
								}
							});
						}
						else {
							res.send(false);
						}
					});
				}
			});
		}
		else {
			var parameters = [
				s.price,
				s.price_type_ref_id,
				s.details,
				s.deal_type_ref_id,
				s.start_time,
				s.end_time
			];
			Special.getAllRecurringSpecials(parameters, function(err1, rSpecials) {
				deleteSpecialFromRecurringEvent(rSpecials, 0, [], res, req.session.tz);
			});
		}
	});
}

function deleteSpecialFromRecurringEvent(rSpecials, start, specialIds, res, tz) {
	if(start < rSpecials.length) {
		var now = Common.newDate(tz);
		var deleted = new Date(now);

		const special = {
		    SPECIAL_ARCHIVE: {
		    	special_id: rSpecials[start].special_id,
		      	price: rSpecials[start].price,
		      	price_type_ref_id: rSpecials[start].price_type_ref_id,
		      	details: rSpecials[start].details,
		      	deal_type_ref_id: rSpecials[start].deal_type_ref_id,
		      	day: rSpecials[start].day,
		      	start_time: rSpecials[start].start_time,
		      	end_time: rSpecials[start].end_time,
		      	deleted_at: deleted,
		      	created_at: rSpecials[start].created_at,
		      	updated_at: rSpecials[start].updated_at
		    }
		};

		SpecialArchive.insert(special, function(err1, result1) {
			if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
				Special.delete(rSpecials[start].special_id, function(err2, result2) {
					if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
						//EventSpecial.deleteBySpecialId(rSpecials[start].special_id, function(err3, result3) {
						//	if(result3 != null && result3 != undefined && result3.affectedRows > 0) {
								specialIds.push(rSpecials[start].special_id);
								deleteSpecialFromRecurringEvent(rSpecials, ++start, specialIds, res, tz);
						//	}
						//});
					} else {
						res.send(false);
					}
				});
			}
			else {
				res.send(false);
			}
		});
	}
	else {
		res.send(specialIds);
	}
}

function updateSpecials(specials, start, startTime, endTime, req, res) {
	if(start < specials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	special_id: specials[start].special_id,
		      	start_time: startTime,
		      	end_time: endTime,
		      	updated_at: now
		    }
		};

		Special.update(special, function(err, result) {
			updateSpecials(specials, ++start, startTime, endTime, req, res);
		});
	}
	else {
		res.redirect('dashboard?page=events');
	}
}

function updateSpecialsMultiday(specials, start, startTime, endTime, req, res) {
	if(start < specials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	special_id: specials[start].special_id,
		      	start_time: startTime,
		      	end_time: endTime,
		      	updated_at: now
		    }
		};

		if(specials[start].start_time < startTime || specials[start].end_time > endTime) {
			EstablishmentSpecial.deleteBySpecialId(specials[start].special_id, function(err1, result1) {
				if(result1 != null && result1 != undefined && result1.affectedRows > 0) {
					Special.delete(specials[start].special_id, function(err2, result2) {
						if(result2 != null && result2 != undefined && result2.affectedRows > 0) {
							EventSpecial.deleteBySpecialId(specials[start].special_id, function(err3, result3) {
								if(result3 != null && result3 != undefined && result3.affectedRows > 0) {
									updateSpecialsMultiday(specials, ++start, startTime, endTime, req, res);
								}
							});
						} else {
							res.send(false);
						}
					});
				}
				else {
					res.send(false);
				}
			});
		}
		else {
			updateSpecialsMultiday(specials, ++start, startTime, endTime, req, res);
		}
	}
	else {
		res.redirect('dashboard?page=events');
	}
}

function updateSpecialsRecurring(specials, start, parameters, eventStart, eventSpecials, req, res) {
	if(start < specials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	special_id: specials[start].special_id,
		      	start_time: parameters[3],
		      	end_time: parameters[4],
		      	updated_at: now
		    }
		};

		Special.update(special, function(err, result) {
			updateSpecialsRecurring(specials, ++start, parameters, eventStart, eventSpecials, req, res);
		});
	}
	else {
		insertRecurringEvent(parameters, ++eventStart, eventSpecials, req, res);
	}
}

function addSpecialsRecurring(eventId, start, parameters, eventStart, eventSpecials, req, res) {
	if(start < eventSpecials.length) {
		var now = Common.newDate(req.session.tz);

		const special = {
		    SPECIAL: {
		    	price: eventSpecials[start].price,
		    	price_type_ref_id: eventSpecials[start].price_type_ref_id,
		    	details: eventSpecials[start].details,
		    	deal_type_ref_id: eventSpecials[start].deal_type_ref_id,
		    	day: eventStart,
		      	start_time: parameters[3],
		      	end_time: parameters[4],
		      	created_at: now,
		      	updated_at: now
		    }
		};

		Special.insert(special, function(err, result) {
			if(result.affectedRows > 0) {
				var specialId = result.insertId;
				const establishment_special = {
				    ESTABLISHMENT_SPECIAL: {
				      establishment_id: req.session.establishment.establishment_id,
				      special_id: specialId,
				      created_at: now,
				      updated_at: now
				    }
				};

				EstablishmentSpecial.insert(establishment_special, function(err2, result2) {
					const event_special = {
					    EVENT_SPECIAL: {
					      event_id: eventId,
					      special_id: specialId,
					      created_at: now,
					      updated_at: now
					    }
					};
					EventSpecial.insert(event_special, function(err3, result3) {
						addSpecialsRecurring(eventId, ++start, parameters, eventStart, eventSpecials, req, res);
					});
				});
			}
		});
	}
	else {
		insertRecurringEvent(parameters, ++eventStart, eventSpecials, req, res);
	}
}

function formatSpecials(specials) {
	for(var s in specials) {
		specials[s].duration = formatStartEndTime(new Date(specials[s].special_start_time)) + " - " + formatStartEndTime(new Date(specials[s].special_end_time));
		var sDate = new Date(specials[s].special_start_time);
		specials[s].days = (sDate.getMonth()+1)+"/"+sDate.getDate();
	}

	// sort once before grouping
	var sorted = false;
	while(!sorted) {
		sorted = true;
		for(var i = 0; i < specials.length-1; i++) {
			if(specials[i].special_start_time > specials[i+1].special_start_time) {
				var temp = specials[i];
				specials[i] = specials[i+1];
				specials[i+1] = temp;
				sorted = false;
			}
		}
	}

	if(specials.length > 1) {
		var grouped = [];

		grouped.push(specials[0]);

		var equal = false;

		for(var x = 1; x < specials.length; x++) {
			equal = false;

			for(var y = 0; y < grouped.length; y++) {
				if(specials[x].event_id == grouped[y].event_id && specials[x].price == grouped[y].price && specials[x].price_type_ref_id == grouped[y].price_type_ref_id &&
				   specials[x].details == grouped[y].details && specials[x].deal_type_ref_id == grouped[y].deal_type_ref_id &&
				   specials[x].special_start_time.getHours() == grouped[y].special_start_time.getHours() &&
				   specials[x].special_start_time.getMinutes() == grouped[y].special_start_time.getMinutes() &&
				   specials[x].special_end_time.getHours() == grouped[y].special_end_time.getHours() &&
				   specials[x].special_end_time.getMinutes() == grouped[y].special_end_time.getMinutes()) {
				   		equal = true;
						grouped[y].days = grouped[y].days + ", " + specials[x].days;
				}
			}

			if(!equal) {
				grouped.push(specials[x]);
			}
		}

		for(var z = 0; z < grouped.length; z++) {
			var numdays = grouped[z].days.split(", ").length;
			var startMidnight = grouped[z].start_time.setHours(0,0,0,0);
			var endMidnight = grouped[z].end_time.setHours(0,0,0,0);
			var db = daysBetween(startMidnight, endMidnight);

			if(db > 0 && numdays == db+1 && numdays > 1) {
				grouped[z].days = "All days";
			}
			else if(db > 0 && numdays == db+1 && numdays == 1) {
				grouped[z].days = "";
			}
		}

		specials = grouped;
	}

	// sort after grouping
	var sorted = false;
	while(!sorted) {
		sorted = true;
		for(var i = 0; i < specials.length-1; i++) {
			if(specials[i].days != "All days" && specials[i+1].days == "All days") {
				var temp = specials[i];
				specials[i] = specials[i+1];
				specials[i+1] = temp;
				sorted = false;
			}
			else if(specials[i].special_start_time > specials[i+1].special_start_time && specials[i].days != "All days" && specials[i+1].days != "All days") {
				var temp = specials[i];
				specials[i] = specials[i+1];
				specials[i+1] = temp;
				sorted = false;
			}
		}
	}

	return specials;
}


/* Common functions */

function sortByStartTimeThenEndTime(events) {
  	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < events.length; i++) {
    		var startA = new Date(events[i-1].start_time);
    		var startB = new Date(events[i].start_time);
    		var endA = new Date(events[i-1].end_time);
    		var endB = new Date(events[i].end_time);

    		if (startA > startB || (startA.getTime() === startB.getTime() && endA > endB)) {
				var s = events[i-1];
		        events[i-1] = events[i];
		        events[i] = s;
		        sorted = false;
		    }
		}

		if(sorted)
		    break;
  	}

  	return events;
}

function sortByEndTime(events) {
	var sorted = false;

  	while (!sorted) {
   		sorted = true;
    	for (var i = 1; i < events.length; i++) {
    		var endA = new Date(events[i-1].end_time);
    		var endB = new Date(events[i].end_time);

    		if (endA > endB) {
				var s = events[i-1];
		        events[i-1] = events[i];
		        events[i] = s;
		        sorted = false;
		    }
		}

		if(sorted)
		    break;
  	}

  	return events;
}

function getTodaysDate(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	var month = intMonthToString(t.getMonth());
	var day = t.getDate();
	var year = 1900+t.getYear();
	return month + " " + day + getDaySuperscript(day) + ", " + year;
}

function getTodaysTime(tz) {
	var t = Common.newDate(tz);
	return formatStartEndTime(t) + " " + tz;
}

function getTodaysDay(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	var dayOfWeek = intDayToString(t.getDay());

	return dayOfWeek;
}

function getTodaysDayInt(tz) {
	var t = Common.newDate(tz);

	// show yesterday's date if before 4am
	if (t.getHours() < 4)
		t.setDate(t.getDate()-1);

	return t.getDay();
}

function isToday(days, start, end, tz) {
	var n = getTodaysDayInt(tz);

	var dayArr = days.split(",");
	for(var i in dayArr) {
		if(dayArr[i] == n) {
			return true;
		}
	}

	if(start && end) {
		if(daysBetween(start, end) > 0) {
			var t = Common.newDate(tz);
			if(start <= t && end >= t) { //current
				return true;
			}
			else if(t.getDate() == start.getDate() && t.getMonth() == start.getMonth() && t.getFullYear() == start.getFullYear()) { //upcoming
				return true;
			}
		}
		else {
			var t = Common.newDate(tz);
			if(t.getHours() < 4)
				t.setDate(t.getDate()-1);

			return t.getDate() == start.getDate() && t.getMonth() == start.getMonth() && t.getFullYear() == start.getFullYear();
		}
	}

	return false;
}

function intDayToString(day) {
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    if (day >= 0 && day <= 6)
        return weekdays[day];
    else
        return "";
}

function intDayToStringAbbr(day) {
	var weekday = new Array(7);
	weekday[0] = "Sun";
	weekday[1] = "Mon";
	weekday[2] = "Tue";
	weekday[3] = "Wed";
	weekday[4] = "Thu";
	weekday[5] = "Fri";
	weekday[6] = "Sat";
	return weekday[day];
}

function intMonthToString(month) {
	var m = new Array(7);
	m[0] = "January";
	m[1] = "February";
	m[2] = "March";
	m[3] = "April";
	m[4] = "May";
	m[5] = "June";
	m[6] = "July";
	m[7] = "August";
	m[8] = "September";
	m[9] = "October";
	m[10] = "November";
	m[11] = "December";
	return m[month];
}

function intMonthToStr(month) {
    var months = new Array(12);
    months[0] = "Jan";
    months[1] = "Feb";
    months[2] = "Mar";
    months[3] = "Apr";
    months[4] = "May";
    months[5] = "Jun";
    months[6] = "Jul";
    months[7] = "Aug";
    months[8] = "Sep";
    months[9] = "Oct";
    months[10] = "Nov";
    months[11] = "Dec";
    if (month >= 0 && month <= 11)
        return months[month];

    return "";
}

function dateDiff(a, tz) {
	var today = Common.newDate(tz);
  	var dealDate = new Date(a);

  	if (dealDate < today) {
    	dealDate.setDate(dealDate.getDate()+1);
    	a = dealDate.getTime();
  	}

	var diffMs = (a - today); // milliseconds between now & a
	var diffDays = Math.floor(diffMs / (1000 * 60 * 60 * 24));
	var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
	var diffMins = Math.ceil(((diffMs % 86400000) % 3600000) / 60000); // minutes

	if (diffMins == 60) {
		diffHrs++;
		diffMins = 0;
	}

	var str = "";

	if (diffDays > 0) {
		//str = diffDays + "d";
		str = "Ends " + timestampToDayMonDate(dealDate) + getDaySuperscript(dealDate.getDate()) + " - " + formatStartEndTime(dealDate);
	}
	else {
		if (diffHrs == 0)
			str = str + diffMins + "m";
		else
			str = str + diffHrs + "h " + diffMins + "m";
	}

	return str;
}

function daysBetween(a, b) {
  	var aa = new Date(a);
  	var bb = new Date(b);

	var diffMs = Math.abs(bb - aa);
	var diffDays = Math.floor(diffMs / (1000 * 60 * 60 * 24));

	return diffDays;
}

function getEventDate(startTime, tz) {
    var now = Common.newDate(tz);
    var tmw = Common.newDate(tz);
    tmw.setDate(now.getDate()+1);

    //startTime = new Date(startTime.replace(' ', 'T')); // ios bug where datetime not formatted correct

    //startTime.addHoursDST(tz);
    //startTime.setHours(startTime.getHours()+6); //utc issue - fall
    //startTime.setHours(startTime.getHours()+5); //utc issue - spring

    if (startTime < now) {
        return "";
    }
    else if(startTime.getDate() == now.getDate() && startTime.getMonth() == now.getMonth() && startTime.getFullYear() == now.getFullYear()) {
        return "Today";
    }
    //else if(startTime.getDate() == tmw.getDate() && startTime.getMonth() == tmw.getMonth() && startTime.getFullYear() == tmw.getFullYear()) {
    //    return "Tomorrow"
    //}
    else {
        return timestampToDayMonDate(startTime);
    }
}

function timestampToDayMonDate(timestamp) {
    var d = new Date(timestamp);
    var day = intDayToStringAbbr(d.getDay());
    var month = intMonthToStr(d.getMonth());
    var date = d.getDate();
    return day + ", " + month + " " + date;
}

function formatStartEndTime(datetime){
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}

	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function getDealTypeImageByRefId(refId) {
	switch(refId) {
		case 200: return 'beer-icon.png';
		case 202: return 'shot-icon.png';
    	case 204: return 'wine-icon.png';
    	case 206: return 'mixed-drink-icon.png';
    	case 208: return 'margarita-icon.png';
    	case 210: return 'martini-icon.png';
    	case 212: return 'tumbler-icon.png';
    	case 214: return 'beerbottle.png';
    	case 216: return 'beercan.png';
		case 251: return 'burger-icon.png';
		case 253: return 'appetizer-icon.png';
		case 255: return 'pizza-icon.png';
    	case 257: return 'taco-icon.png';
    	case 259: return 'sushi.png';
    	case 261: return 'bowl.png';
    	case 263: return 'chickenwing.png';
		default: return '';
	}
}

function getISOString(dt) {
	var current_date = dt.getDate();
	var current_month = dt.getMonth() + 1;
	var current_year = dt.getFullYear();
	var current_hrs = dt.getHours();
	var current_mins = dt.getMinutes();
	var current_secs = dt.getSeconds();
	var current_datetime;

	// Add 0 before date, month, hrs, mins or secs if they are less than 0
	current_date = current_date < 10 ? '0' + current_date : current_date;
	current_month = current_month < 10 ? '0' + current_month : current_month;
	current_hrs = current_hrs < 10 ? '0' + current_hrs : current_hrs;
	current_mins = current_mins < 10 ? '0' + current_mins : current_mins;
	current_secs = current_secs < 10 ? '0' + current_secs : current_secs;

	// Current datetime
	// String such as 2016-07-16T19:20:30
	current_datetime = current_year + '-' + current_month + '-' + current_date + 'T' + current_hrs + ':' + current_mins + ':' + current_secs;
	return current_datetime;
}

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
    return year + "-" + month + "-" + day;
}

function getReoccurringStr(days) {
	var str = "";
	var dayArr = days.split(",");

	//sort day array 1, 2, 3, 4, 5, 6, 0
	if(dayArr.length > 1) {
		var sorted = false;
		while(!sorted) {
			sorted = true;
			for(var d = 0; d < dayArr.length - 1; d++) {
				var d1 = parseInt(dayArr[d+1]);
				var d2 = parseInt(dayArr[d]);
				if((d1 < d2 && d1 != 0) || d2 == 0) {
					var temp = dayArr[d];
					dayArr[d] = dayArr[d+1];
					dayArr[d+1] = temp;
					sorted = false;
				}
			}
		}
	}

	if(dayArr.length == 1) {
		str = str + intDayToString(dayArr[0]);
	}
	else if(dayArr.length == 2) {
		str = str + intDayToString(dayArr[0]) + " and " + intDayToString(dayArr[1]);
	}
	else {
		for(var i in dayArr) {
			if(i < dayArr.length - 2) {
				str = str + intDayToString(dayArr[i]) + ", ";
			}
			else if(i < dayArr.length - 1) {
				str = str + intDayToString(dayArr[i]) + ", and ";
			}
			else {
				str = str + intDayToString(dayArr[i]);
			}
		}
	}

	return str;
}

function getDateTime(start, startDate) {
	var startH = parseInt(start.substring(0, start.indexOf(":")));
	var startM = parseInt(start.substring(start.indexOf(":")+1));

	var s = new Date(startDate);
	s.setHours(s.getHours() + startH);
	s.setMinutes(s.getMinutes() + startM);

	//if(startH < 4) {
	//	s.setDate(s.getDate() + 1);
	//}

	return s;
}

function epochToDate(epoch) {
	var d = new Date(0);
	d.setUTCSeconds(epoch);

  	var monthNames = [
    	"January", "February", "March",
    	"April", "May", "June", "July",
    	"August", "September", "October",
    	"November", "December"
  	];

  	var day = d.getDate();
  	var monthIndex = d.getMonth();
  	var year = d.getFullYear();

  	return monthNames[monthIndex] + ' ' + day + getDaySuperscript(day) + ', ' + year;
}

function getDaySuperscript(day) {
	if(day == 1 || day == 21 || day == 31)
		return "st";
	else if(day == 2 || day == 22)
		return "nd";
	else if(day == 3 || day == 23)
		return "rd";
	else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
		return "th";
	else
		return "";
}

function getWeekDates(week, tz) {
	var weekDates = [];

	var addDays = 1;
    if(week != 0)
        addDays = week * 7 + 1;

    var curr = Common.newDate(tz);
    curr.setHours(0,0,0,0);

    var first = 0;
    if(curr.getDay() > 0)
    	first = curr.getDate() - curr.getDay() + addDays;
    else
    	first = curr.getDate() - 7 + addDays;

    for(var x = 0; x <= 6; x++) {
        var d = new Date(curr.setDate(first));
        weekDates.push(d);
        first = first + 1;
        curr = Common.newDate(tz);
        curr.setHours(0,0,0,0);
    }

    return weekDates;
}

function getDatesThisWeek(dt, days, tz) {
	var datesThisWeek = [];

	var curr = Common.newDate(tz);
	var first = curr.getDate() - curr.getDay();
    var firstDayOfWeek = new Date(curr.setDate(first));

    //console.log(firstDayOfWeek);

    days = days.split(",");
    for(var d in days) {
    	var day = parseInt(days[d]);
    	var newDt = new Date(firstDayOfWeek.getTime());
    	newDt.setDate(newDt.getDate()+day);
    	newDt.setHours(dt.getHours());
    	newDt.setMinutes(dt.getMinutes());
    	newDt.setSeconds(0);
    	//console.log(newDt);
    	datesThisWeek.push(newDt);
    }


    return datesThisWeek;
}
