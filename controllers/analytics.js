var Common = require('../models/common.js');
var Establishment = require('../models/establishment.js');
var Special = require('../models/special.js');
var SpecialArchive = require('../models/special_archive.js');
var Event = require('../models/event.js');
var AnalyticsCache = require('../models/analytics_cache.js')
var EventArchive = require('../models/event_archive.js');
var Favorites = require('../models/favorites.js');
var MixpanelExport = require('mixpanel-data-export');
var panel = new MixpanelExport({
	api_key: "4f98c92e0ecf95d97d76d6eca2cb0298",
	api_secret: "3e9796ed74b2cdda13edc3bbe904f073"
});
const { parseJsonArrays, convertAnalyticsCacheJsonToDbObject } = require('../utils/analytics/data')
var NodeGeocoder = require('node-geocoder');
var Geocoder = NodeGeocoder({ provider: 'google', apiKey: 'AIzaSyA8ObhYNLNSbaaHrF1B7dC1a10t9iF50II' });
const isEmpty = require('lodash/isEmpty')
const { secondsDifferent } = require('../utils')

exports.show = function (req, res) {
	var curr = Common.newDate(req.session.tz);

	var toDate = getYYYYMMDD(curr);
	var weekStart = getYYYYMMDD(new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() - curr.getDay() + 1));
	var weekEnd = curr.getDay();
	if (weekEnd == 0)
		weekEnd = 7;
	weekEnd = 7 - weekEnd;
	weekEnd = "+" + weekEnd + "d";

	curr.setDate(curr.getDate() - 365);
	var yearAgo = getYYYYMMDD(new Date(curr.getFullYear(), curr.getMonth(), curr.getDate() - curr.getDay())) + " 00:00:00";

	Establishment.getEverythingByEstablishmentId(req.session.establishment.establishment_id, function (err, establishment) {
		if (!establishment) {
			res.status(403).end()
		}
		var params = [establishment.establishment_id, yearAgo];

		Special.getAllByEstablishmentIdForYear(params, function (err, specials) {

			Event.getAllByEstablishmentIdForYear(params, function (err, events) {

				EventArchive.getAllByEstablishmentIdForYear(params, function (err, eventsArc) {
					var params = [establishment.establishment_id, yearAgo, yearAgo];

					SpecialArchive.getAllByEstablishmentIdForYear(params, function (err, specialsArc) {

						Favorites.getAllByEstablishmentId(establishment.establishment_id, function (err, favs) {
							for (var f in favs) {
								favs[f].createdAt = dateToStr(favs[f].created_at);
							}

							Favorites.getAllProfiles(function (err, profiles) {
								var mspLat = 44.9778;
								var mspLon = -93.2650;
								var icLat = 41.6611;
								var icLon = -91.5302;
								var denLat = 39.7392;
								var denLon = -104.9903;
								var louLat = 38.2527;
								var louLon = -85.7585;
								var location = "";
								var profs = 0;
								var profsLastWeek = 0;

								var cardOnFile = false;
								var hasSubscription = false;
								var needsToPay = false;
								var favorites = 0;
								var favoritesLastWeek = 0;
								var imp = [];
								var impPastWeek = 0;
								var impPrevWeek = 0;
								var impChange = 0;
								var eventImp = [];
								var eventImpPastWeek = 0;
								var eventImpPrevWeek = 0;
								var eventImpChange = 0;
								var isAppyHour = false;

								var endWeek = Common.newDate(req.session.tz);
								endWeek.setDate(endWeek.getDate() - 1);
								endWeek.setHours(0, 0, 0, 0);
								var startWeek = new Date(endWeek);
								startWeek.setDate(startWeek.getDate() - 6);

								var endWeekPrev = new Date(startWeek);
								var startWeekPrev = new Date(endWeekPrev);
								startWeekPrev.setDate(startWeekPrev.getDate() - 7);

								if (establishment.name == "AppyHour")
									isAppyHour = true;


								// ACTIVE USERS

								var dist = distance(establishment.latitude, establishment.longitude, mspLat, mspLon);
								if (dist <= 30) {
									location = "msp";
									for (var p in profiles) {
										var d = distance(profiles[p].location_lat, profiles[p].location_lon, mspLat, mspLon);
										if (d <= 30) {
											profs += 1;
											var lw = Common.newDate(req.session.tz);
											lw.setDate(lw.getDate() - 7);
											if (profiles[p].created_at <= lw) {
												profsLastWeek += 1;
											}
										}
									}
								}
								dist = distance(establishment.latitude, establishment.longitude, icLat, icLon);
								if (dist <= 30) {
									location = "ic";
									for (var p in profiles) {
										var d = distance(profiles[p].location_lat, profiles[p].location_lon, icLat, icLon);
										if (d <= 30) {
											profs += 1;
											var lw = Common.newDate(req.session.tz);
											lw.setDate(lw.getDate() - 7);
											if (profiles[p].created_at <= lw) {
												profsLastWeek += 1;
											}
										}
									}
								}
								dist = distance(establishment.latitude, establishment.longitude, denLat, denLon);
								if (dist <= 30) {
									location = "den";
									for (var p in profiles) {
										var d = distance(profiles[p].location_lat, profiles[p].location_lon, denLat, denLon);
										if (d <= 30) {
											profs += 1;
											var lw = Common.newDate(req.session.tz);
											lw.setDate(lw.getDate() - 7);
											if (profiles[p].created_at <= lw) {
												profsLastWeek += 1;
											}
										}
									}
								}
								dist = distance(establishment.latitude, establishment.longitude, louLat, louLon);
								if (dist <= 30) {
									location = "lou";
									for (var p in profiles) {
										var d = distance(profiles[p].location_lat, profiles[p].location_lon, louLat, louLon);
										if (d <= 30) {
											profs += 1;
											var lw = Common.newDate(req.session.tz);
											lw.setDate(lw.getDate() - 7);
											if (profiles[p].created_at <= lw) {
												profsLastWeek += 1;
											}
										}
									}
								}

								var profsChange = 0;
								if (profs != profsLastWeek && profsLastWeek > 0) {
									profsChange = ((profs - profsLastWeek) / profsLastWeek) * 100;
									profsChange = profsChange.toFixed(0);
								}
								else if (profs > profsLastWeek && profsLastWeek == 0) {
									profsChange = 100;
								}



								// FAVORITES

								favorites = 0;

								for (var f in favs) {
									if (favs[f].created_at >= startWeek && favs[f].created_at <= endWeek) {
										favorites += 1;
									}
									else if (favs[f].created_at >= startWeekPrev && favs[f].created_at <= endWeekPrev) {
										favoritesLastWeek += 1;
									}
								}

								var favoritesChange = 0;
								if (favorites != favoritesLastWeek && favoritesLastWeek > 0) {
									favoritesChange = ((favorites - favoritesLastWeek) / favoritesLastWeek) * 100;
									favoritesChange = favoritesChange.toFixed(0);
								}
								else if (favorites > favoritesLastWeek && favoritesLastWeek == 0) {
									favoritesChange = 100;
								}



								// SPECIAL IMPRESSIONS

								for (var x in specialsArc) {
									specials.push(specialsArc[x]);
								}

								var impressions = [];
								// mixpanel can only handle 100 events
								for (var s = 0; s < 100; s++) {
									if (specials[s] !== undefined) {
										impressions.push("Impression-" + specials[s].special_id);
									}
									else {
										break;
									}
								}

								/*
								const fetch = require('node-fetch');

								const url = 'https://mixpanel.com/api/2.0/events';

								const options = {
								  method: 'GET',
								  qs: {
									project_id: '811237',
									event: '["Impression-13588"]',
									type: 'general',
									unit: 'week',
									from_date: '2019-01-01',
									to_date: '2020-03-17'
								  },
								  headers: {
									Accept: 'application/json',
									Authorization: 'Basic SmFrZS45YWVmMDAubXAtc2VydmljZS1hY2NvdW50Om5YeTMxS3dOMWdEZk92Smhsek9GdGlhZjdhZ09IbDc3'
								  }
								};

								fetch(url, options)
								  .then(res => res.json())
								  .then(json => console.log(json))
								  .catch(err => console.error('error:' + err));
								*/

								// console.log(impressions);
								try {
									panel.events({
										event: impressions,
										type: "general",
										unit: "week",
										from_date: "2019-01-01",
										to_date: toDate
									}).then(function (data) {
										//console.log("PANEL1");
										if (data !== undefined && data.data !== undefined && data.data.values !== undefined) {
											var vals = data.data.values;

											for (var d in vals) {
												var objKeys = Object.keys(vals[d]); // [date, date,...]
												var objVals = Object.values(vals[d]); // [1,23,5,1,2, ...]

												for (var v in objVals) {
													var impObj = {};
													impObj.specialId = d.substring(d.indexOf("-") + 1);
													impObj.impressions = objVals[v];

													//if(impObj.specialId == 11878 && objVals[v] > 0)
													//	console.log(objVals[v]);

													var objDate;
													var objWeek;
													var objCreated;
													var isArchive = true;

													for (var sp in specials) {
														if (specials[sp].special_id == impObj.specialId) {
															if (specials[sp].price_type_ref_id == 300)
																impObj.details = "$" + specials[sp].price.toFixed(2) + " " + specials[sp].details;
															else if (specials[sp].price_type_ref_id == 301)
																impObj.details = specials[sp].price + "% Off " + specials[sp].details;
															else if (specials[sp].price_type_ref_id == 302)
																impObj.details = "BOGO " + specials[sp].details;
															else if (specials[sp].price_type_ref_id == 307)
																impObj.details = "Free " + specials[sp].details;

															impObj.dealType = specials[sp].deal_type_ref_id;

															var newDate = new Date(objKeys[v]);
															//newDate.setHours(newDate.getHours()-5); // UTC issue
															newDate.subtractHoursDST(req.session.tz);

															impObj.start = specials[sp].start_time;
															impObj.date = getDateStr(newDate, specials[sp].start_time, specials[sp].day); // ????
															impObj.time = getTimeStr(specials[sp].start_time) + " - " + getTimeStr(specials[sp].end_time);
															impObj.day = specials[sp].day;
															impObj.createdAt = dateToStr(specials[sp].created_at);
															var impDate = new Date(objKeys[v] + " 00:00:00");
															impObj.impDate = dateToStr(impDate);

															if (specials[sp].deleted_at != undefined) {
																impObj.deleted = specials[sp].deleted_at;
																impObj.status = "Deleted";
																impObj.status2 = dateToStr(impObj.deleted);
															}
															else if (impObj.day > -1) {
																impObj.status = "Active every " + intDayToString(impObj.day);
															}

															if (specials[sp].event)
																impObj.event = specials[sp].event;
															else if (specials[sp].eventArc)
																impObj.event = specials[sp].eventArc;

															objDate = new Date(impObj.date);
															objWeek = newDate;
															objCreated = new Date(specials[sp].created_at);
															break;
														}
													}

													var nextDay = new Date(objDate);
													nextDay.setDate(nextDay.getDate() + 1);

													var nextWeek = new Date(objWeek);
													nextWeek.setDate(nextWeek.getDate() + 7);

													if ((impObj.day == -1 && objWeek <= impObj.start && nextWeek > impObj.start) ||
														(impObj.day > -1 && objCreated < nextDay)) {
														if (impObj.deleted == undefined || (impObj.deleted != undefined && impObj.deleted > objDate)) {
															if ((impObj.day == -1 && startWeek <= impObj.start && endWeek > impObj.start) ||
																(impObj.day > -1 && startWeek <= objDate && endWeek > objDate)) {
																impPastWeek += impObj.impressions;
															}
															else if ((impObj.day == -1 && startWeekPrev <= impObj.start && endWeekPrev > impObj.start) ||
																(impObj.day > -1 && startWeekPrev <= objDate && endWeekPrev > objDate)) {
																impPrevWeek += impObj.impressions;
															}

															//if(impObj.specialId == 11878)
															//	console.log(impObj);
															imp.push(impObj);
														}
													}
												}
											}
										}

										if (impPastWeek > 0 && impPrevWeek > 0) {
											impChange = ((impPastWeek - impPrevWeek) / impPrevWeek) * 100;
											impChange = impChange.toFixed(0);
										}
										else if (impPastWeek > 0 && impPrevWeek == 0) {
											impChange = 100;
										}
										else if (impPastWeek == 0 && impPrevWeek > 0) {
											impChange = -100;
										}




										// EVENT IMPRESSIONS

										for (var x in eventsArc) {
											events.push(eventsArc[x]);
										}

										var eimpressions = [];
										// mixpanel can only handle 100 events
										for (var e = 0; e < 100; e++) {
											if (events[e] !== undefined) {
												eimpressions.push("EventImpression-" + events[e].event_id);
											}
											else {
												break;
											}
										}

										//console.log("HERE2");
										panel.events({
											event: eimpressions,
											type: "general",
											unit: "day",
											from_date: "2020-10-01",
											to_date: toDate
										}).then(function (data) {
											//console.log("PANEL2");
											console.log("🚀 ~ file: analytics.js ~ line 384 ~ Favorites.getAllProfiles ~ data", data.error)
											if (data !== undefined && data.data !== undefined && data.data.values !== undefined) {
												var vals = data.data.values;

												for (var d in vals) {
													var objKeys = Object.keys(vals[d]);
													var objVals = Object.values(vals[d]);

													for (var v in objVals) {
														var impObj = {};
														impObj.eventId = d.substring(d.indexOf("-") + 1);
														impObj.impressions = objVals[v];

														for (var e in events) {
															if (events[e].event_id == impObj.eventId) {
																impObj.startTime = dateToStr(events[e].start_time);
																impObj.endTime = dateToStr(events[e].end_time);
																impObj.day = events[e].day;
																var impDate = new Date(objKeys[v]);
																impDate.addHoursDST(req.session.tz);
																impObj.impDate = dateToStr(impDate);

																//if(impDate.getDate() == 2 && objVals[v] == 5) {
																//	console.log(events[e]);
																//	console.log(objVals[v]);
																//}

																impObj.createdAt = dateToStr(events[e].created_at);
																impObj.duration = getTimeStr(events[e].start_time) + " - " + getTimeStr(events[e].end_time);
																impObj.title = events[e].title;
																if (events[e].deleted_at != undefined)
																	impObj.arcDate = dateToStr(events[e].deleted_at);

																break;
															}
														}

														if (impDate >= startWeek && impDate <= endWeek)
															eventImpPastWeek += parseInt(impObj.impressions);
														else if (impDate >= startWeekPrev && impDate <= endWeekPrev)
															eventImpPrevWeek += parseInt(impObj.impressions);

														//if(parseInt(impObj.day) > -1 || (impObj.startTime >= startWeek && impObj.startTime <= endWeek))
														//	eventImpPastWeek += parseInt(impObj.impressions);
														//else if(impObj.startTime >= startWeekPrev && impObj.startTime <= endWeekPrev)
														//	eventImpPrevWeek += parseInt(impObj.impressions);

														eventImp.push(impObj);
													}
												}
											}

											if (eventImpPastWeek > 0 && eventImpPrevWeek > 0) {
												eventImpChange = ((eventImpPastWeek - eventImpPrevWeek) / eventImpPrevWeek) * 100;
												eventImpChange = eventImpChange.toFixed(0);
											}
											else if (eventImpPastWeek > 0 && eventImpPrevWeek == 0) {
												eventImpChange = 100;
											}
											else if (eventImpPastWeek == 0 && eventImpPrevWeek > 0) {
												eventImpChange = -100;
											}




											// PROFILE VIEWS

											var profViews = [];
											var profViewsCount = 0;
											var profViewsCountPast = 0;

											//console.log("HERE3");
											panel.events({
												event: ["Est loaded " + establishment.establishment_id],
												type: "general",
												unit: "day",
												from_date: "2019-01-01",
												to_date: toDate
											}).then(function (data) {
												//console.log("PANEL3");
												console.log("🚀 ~ file: analytics.js ~ line 465 ~ Favorites.getAllProfiles ~ data", data.error)
												if (data !== undefined && data.data !== undefined && data.data.values !== undefined) {
													var vals = data.data.values;

													for (var d in vals) {
														var objKeys = Object.keys(vals[d]);
														var objVals = Object.values(vals[d]);

														for (var v in objVals) {
															var loadObj = {};
															var objKeyDt = new Date(objKeys[v]);
															objKeyDt.addHoursDST(req.session.tz);
															loadObj.date = dateToStr(objKeyDt);
															loadObj.count = objVals[v];
															profViews.push(loadObj);


															var objDate = new Date(loadObj.date);
															if (objDate >= startWeek && objDate <= endWeek) {
																profViewsCount += parseInt(loadObj.count);
															}
															else if (objDate >= startWeekPrev && objDate <= endWeekPrev) {
																profViewsCountPast += parseInt(loadObj.count);
															}
														}
													}

													var profViewsChange = 0;
													if (profViewsCount != profViewsCountPast && profViewsCountPast > 0) {
														profViewsChange = ((profViewsCount - profViewsCountPast) / profViewsCountPast) * 100;
														profViewsChange = profViewsChange.toFixed(0);
													}
													else if (profViewsCount > profViewsCountPast && profViewsCountPast == 0) {
														profViewsChange = 100;
													}

												}




												if (req.session.user.needs_to_pay == 1) {
													needsToPay = true;
													if (req.session.user.stripe_id) {
														var stripe = require("stripe")("sk_live_VHSdezT2ss0h7qlIClACIEFr"); //PROD
														//var stripe = require("stripe")("sk_test_5jI95CpiAjjTUrZBGqGvkpRP"); //LOCAL
														stripe.customers.retrieve(req.session.user.stripe_id, function (err, customer) {
															if (customer) {
																if (customer.subscriptions.data.length > 0) {
																	hasSubscription = true;
																}
																stripe.customers.retrieveCard(req.session.user.stripe_id, customer.default_source, function (err, card) {
																	if (card) {
																		cardOnFile = true;
																	}
																	const result = {
																		needsToPay: needsToPay,
																		cardOnFile: cardOnFile,
																		hasSubscription: hasSubscription,
																		allFavs: favs,
																		favorites: favorites,
																		favoritesChange: favoritesChange,
																		impPastWeek: impPastWeek,
																		impChange: impChange,
																		location: location,
																		profs: profs,
																		profsChange: profsChange,
																		profViews: profViews,
																		profViewsCount: profViewsCount,
																		profViewsChange: profViewsChange,
																		impressions: imp,
																		eventImp: eventImp,
																		eventImpPastWeek: eventImpPastWeek,
																		eventImpChange: eventImpChange,
																		isAppyHour: isAppyHour,
																		toDate: toDate,
																		weekStart: weekStart,
																		weekEnd: weekEnd
																	}
																	if (req.isMissCache) {
																		AnalyticsCache.upsert(
																			convertAnalyticsCacheJsonToDbObject(establishment.establishment_id, req.session.tz, result),
																			() => {}
																		)
																	}
																	return res.render('analytics', result);
																});
															} else {
																const result = {
																	needsToPay: needsToPay,
																	cardOnFile: cardOnFile,
																	hasSubscription: hasSubscription,
																	allFavs: favs,
																	favorites: favorites,
																	favoritesChange: favoritesChange,
																	impPastWeek: impPastWeek,
																	impChange: impChange,
																	location: location,
																	profs: profs,
																	profsChange: profsChange,
																	profViews: profViews,
																	profViewsCount: profViewsCount,
																	profViewsChange: profViewsChange,
																	impressions: imp,
																	eventImp: eventImp,
																	eventImpPastWeek: eventImpPastWeek,
																	eventImpChange: eventImpChange,
																	isAppyHour: isAppyHour,
																	toDate: toDate,
																	weekStart: weekStart,
																	weekEnd: weekEnd
																}
																if (req.isMissCache) {
																	AnalyticsCache.upsert(
																		convertAnalyticsCacheJsonToDbObject(establishment.establishment_id, req.session.tz, result),
																		() => {}
																	)
																}
																return res.render('analytics', result);
															}
														});
													} else {
														const result = {
															needsToPay: needsToPay,
															cardOnFile: cardOnFile,
															hasSubscription: hasSubscription,
															allFavs: favs,
															favorites: favorites,
															favoritesChange: favoritesChange,
															impPastWeek: impPastWeek,
															impChange: impChange,
															location: location,
															profs: profs,
															profsChange: profsChange,
															profViews: profViews,
															profViewsCount: profViewsCount,
															profViewsChange: profViewsChange,
															impressions: imp,
															eventImp: eventImp,
															eventImpPastWeek: eventImpPastWeek,
															eventImpChange: eventImpChange,
															isAppyHour: isAppyHour,
															toDate: toDate,
															weekStart: weekStart,
															weekEnd: weekEnd
														}
														if (req.isMissCache) {
															AnalyticsCache.upsert(
																convertAnalyticsCacheJsonToDbObject(establishment.establishment_id, req.session.tz, result),
																() => {}
															)
														}
														return res.render('analytics', result);
													}
												} else {
													const result = {
														needsToPay: needsToPay,
														cardOnFile: cardOnFile,
														hasSubscription: hasSubscription,
														allFavs: favs,
														favorites: favorites,
														favoritesChange: favoritesChange,
														impPastWeek: impPastWeek,
														impChange: impChange,
														location: location,
														profs: profs,
														profsChange: profsChange,
														profViews: profViews,
														profViewsCount: profViewsCount,
														profViewsChange: profViewsChange,
														impressions: imp,
														eventImp: eventImp,
														eventImpPastWeek: eventImpPastWeek,
														eventImpChange: eventImpChange,
														isAppyHour: isAppyHour,
														toDate: toDate,
														weekStart: weekStart,
														weekEnd: weekEnd
													}
													if (req.isMissCache) {
														AnalyticsCache.upsert(
															convertAnalyticsCacheJsonToDbObject(establishment.establishment_id, req.session.tz, result),
															() => {}
														)
													}
													return res.render('analytics', result);
												}
											})
										})
									})
								}
								catch (errjake) {
									console.log("ERROR");
									console.log(errjake);
								}
							});
						});

					});
				});
			});
		});
	});
};

function getDateStr(d, sStart, day) {
	var month;
	var date;
	var year;

	if (day == -1) {
		month = sStart.getMonth() + 1;
		if (month < 10)
			month = "0" + month;
		date = sStart.getDate();
		if (date < 10)
			date = "0" + date;
		year = sStart.getYear() + 1900;
	}
	else {
		if (day == 0)
			day = 7;
		var newDate = new Date(d);
		newDate.setDate(newDate.getDate() + day);

		month = newDate.getMonth() + 1;
		if (month < 10)
			month = "0" + month;
		date = newDate.getDate();
		if (date < 10)
			date = "0" + date;
		year = newDate.getYear() + 1900;
	}

	return month + "/" + date + "/" + year;
}

function getTimeStr(datetime) {
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}

	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function getYYYYMMDD(d) {
	var year = d.getYear() + 1900;
	var month = d.getMonth() + 1;
	if (month < 10)
		month = "0" + month;
	var day = d.getDate();
	if (day < 10)
		day = "0" + day;
	return year + "-" + month + "-" + day;
}

function intDayToString(day) {
	var weekdays = new Array(7);
	weekdays[0] = "Sunday";
	weekdays[1] = "Monday";
	weekdays[2] = "Tuesday";
	weekdays[3] = "Wednesday";
	weekdays[4] = "Thursday";
	weekdays[5] = "Friday";
	weekdays[6] = "Saturday";
	if (day >= 0 && day <= 6)
		return weekdays[day];
	else
		return "";
}

function dateToStr(d) {
	var month;
	var date;
	var year;
	var hours;
	var minutes;
	var ampm;

	month = d.getMonth() + 1;
	if (month < 10)
		month = "0" + month;

	date = d.getDate();
	if (date < 10)
		date = "0" + date;

	year = d.getYear() + 1900;

	hours = d.getHours();
	if (hours > 12) {
		hours = hours - 12;
		ampm = "PM";
	}
	else if (hours == 12) {
		ampm = "PM";
	}
	else if (hours < 12 && hours != 0) {
		ampm = "AM";
	}
	else if (hours == 0) {
		hours = 12;
		ampm = "AM";
	}

	minutes = d.getMinutes();
	if (minutes < 10) {
		minutes = "0" + minutes;
	}

	return month + "/" + date + "/" + year + " " + hours + ":" + minutes + " " + ampm;
}

function distance(lat1, lon1, lat2, lon2) {
	if ((lat1 == lat2) && (lon1 == lon2)) {
		return 0;
	}
	else {
		var radlat1 = Math.PI * lat1 / 180;
		var radlat2 = Math.PI * lat2 / 180;
		var theta = lon1 - lon2;
		var radtheta = Math.PI * theta / 180;
		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		if (dist > 1) {
			dist = 1;
		}
		dist = Math.acos(dist);
		dist = dist * 180 / Math.PI;
		dist = dist * 60 * 1.1515;
		return dist;
	}
}

exports.analyticsIntercept = function (req, res, next) {
	const { establishment, tz } = req.session
	const establishmentId = establishment.establishment_id
	const CACHE_TIMEOUT = 1.5 * 60 * 60 // INFO: invalidate cache after 1 hours 30 minutes
	AnalyticsCache.getAllByEstablishmentId([establishmentId, tz], function (error, result) {
		if (error || isEmpty(result) || secondsDifferent(result.updated_at) >= CACHE_TIMEOUT) {
			req.isMissCache = true
			return next()
		} else {
			const json = JSON.parse(result.analytics_cache_json)
			const convertedJson = parseJsonArrays(json)
			return res.render('analytics', convertedJson)
		}
	})
}
