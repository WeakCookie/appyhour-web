var User = require('../models/user.js');
var UserEstablishment = require('../models/user_establishment.js');
var Establishment = require('../models/establishment.js');
var Metrics = require('../models/metrics.js');

var fs = require('fs');
var bcrypt = require('bcrypt');


exports.show = function(req, res) {
	if(req.session.metricsAuth === undefined)
		req.session.metricsAuth = false;

	if(req.session.metricsAuth) {
		loadMetrics(req, res);
	}
	else {
		res.render('metrics', {
			showLogin: !req.session.metricsAuth
		});
	}
}

exports.metricsAuth = function(req, res) {
	/* KEEP: use this to create a new password 
	var pass = "XXX";
	bcrypt.hash(pass, 10, function(err, hash) {
		console.log(hash);
	});
	*/

	fs.readFile( './metricsauth.xml', 'utf8', function(err, pw) {
    	bcrypt.compare(req.body.pw, pw, function(err, match) {
			if(match) {
				req.session.metricsAuth = true;
				loadMetrics(req, res);
			}
			else {
				res.render('metrics', {
					showLogin: !req.session.metricsAuth,
					status: "Wrong password",
					statusSuccess: false
				});
			}
		});
 	});
}

exports.metricsLogout = function(req, res) {
	req.session.metricsAuth = false;

	res.render('metrics', {
		showLogin: !req.session.metricsAuth,
		status: "Successfully logged out",
		statusSuccess: true
	});
}

function loadMetrics(req, res) {
	Establishment.getAll(function(errEsts, ests) {
		var mnCount = 0;
		var kyCount = 0;
		var iaCount = 0;
		var denCount = 0;
		var tampaCount = 0;
		var allEsts = [];

		for(var e in ests) {
			if(ests[e].name.indexOf('AppyHour') == -1) {
				if(ests[e].topic == 'minneapolismn') {
					mnCount++;
					ests[e].created = ests[e].created_at.getTime();
					ests[e].paying = ests[e].stripe_id != null ? true : false;
					allEsts.push(ests[e]);
				}
				else if(ests[e].topic == 'louisvilleky') {
					kyCount++;
					ests[e].created = ests[e].created_at.getTime();
					ests[e].paying = ests[e].stripe_id != null ? true : false;
					allEsts.push(ests[e]);
				}
				else if(ests[e].topic == 'iowacityia') {
					iaCount++;
					ests[e].created = ests[e].created_at.getTime();
					ests[e].paying = ests[e].stripe_id != null ? true : false;
					allEsts.push(ests[e]);
				}
				else if(ests[e].topic == 'denverco') {
					denCount++;
					ests[e].created = ests[e].created_at.getTime();
					ests[e].paying = ests[e].stripe_id != null ? true : false;
					allEsts.push(ests[e]);
				}
				else if(ests[e].topic == 'tampabayfl') {
					tampaCount++;
					ests[e].created = ests[e].created_at.getTime();
					ests[e].paying = ests[e].stripe_id != null ? true : false;
					allEsts.push(ests[e]);
				}
			}
		}

		User.getPayingUsers(function(errUsers, users) {
			var price99ests = [];
			var price89ests = [];
			var mnPayingCount = 0;
			var kyPayingCount = 0;
			var iaPayingCount = 0;
			var denPayingCount = 0;
			var tampaPayingCount = 0;

			for(var u in users) {
				console.log(users[u].username);
				if(users[u].username != 'teamappyhour') {
					if(users[u].topic == 'minneapolismn' || users[u].topic == 'louisvilleky' || users[u].topic == 'iowacityia' || users[u].topic == 'denverco' || users[u].topic == 'tampabayfl') {
						if(users[u].topic == 'minneapolismn') {
							mnPayingCount++;
						}
						else if(users[u].topic == 'louisvilleky') {
							kyPayingCount++;
						}
						else if(users[u].topic == 'iowacityia') {
							iaPayingCount++;
						}
						else if(users[u].topic == 'denverco') {
							denPayingCount++;
						}
						else if(users[u].topic == 'tampabayfl') {
							tampaPayingCount++;
						}

						if(price99ests.length > 0 && price99ests.some(e => e.user_id === users[u].user_id)) {
							price89ests.push(users[u]);
						}
						else {
							price99ests.push(users[u]);
						}
					}
				}
			}

			var price99total = price99ests.length * 99.99;
			var price89total = price89ests.length * 89.99;
			var monthlyRevenue = price99total + price89total;

			Metrics.getAllInstalls(function(errMetrics, installs) {
				var iosInstalls = 0;
				var androidInstalls = 0;
				var installDateRange = dateToStr(installs[0].start_date) + " - " + dateToStr(installs[0].end_date);

				if(installs[0].platform == "ios") {
					iosInstalls = installs[0].value;
					androidInstalls = installs[1].value;
				}
				else {
					iosInstalls = installs[1].value;
					androidInstalls = installs[0].value;
				}

				var allIosInstalls = [];
				var allAndroidInstalls = [];

				for(var i = 0; i < 20; i++) {
					if(i < installs.length) {
						installs[i].start = installs[i].start_date.getTime();
						installs[i].end = installs[i].end_date.getTime();
						if(installs[i].platform == "ios") {
							allIosInstalls.push(installs[i]);
						}
						else {
							allAndroidInstalls.push(installs[i]);
						}
					}
					else { break; }
				}

				/*
				for(var i in installs) {
					installs[i].start = installs[i].start_date.getTime();
					installs[i].end = installs[i].end_date.getTime();
					if(installs[i].platform == "ios") {
						allIosInstalls.push(installs[i]);
					}
					else {
						allAndroidInstalls.push(installs[i]);
					}
				}
				*/

				res.render('metrics', {
					showLogin: !req.session.metricsAuth,
					allEstCount: mnCount + kyCount + iaCount + denCount,
					mnCount: mnCount,
					kyCount: kyCount,
					iaCount: iaCount,
					denCount: denCount,
					tampaCount: tampaCount,
					allEsts: allEsts,
					payingEstCount: mnPayingCount + kyPayingCount + iaPayingCount + denPayingCount,
					mnPayingCount: mnPayingCount,
					kyPayingCount: kyPayingCount,
					iaPayingCount: iaPayingCount,
					denPayingCount: denPayingCount,
					tampaPayingCount: tampaPayingCount,
					price99count: price99ests.length,
					price89count: price89ests.length,
					price99total: formatMoney(price99total, 2),
					price89total: formatMoney(price89total, 2),
					monthlyRevenue: formatMoney(monthlyRevenue, 2),
					iosInstalls: iosInstalls,
					androidInstalls: androidInstalls,
					allInstalls: iosInstalls + androidInstalls,
					installDateRange: installDateRange,
					allIosInstalls: allIosInstalls,
					allAndroidInstalls: allAndroidInstalls
				});
			});
		});
	});
}

function formatMoney(number, decPlaces, decSep, thouSep) {
	decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
	decSep = typeof decSep === "undefined" ? "." : decSep;
	thouSep = typeof thouSep === "undefined" ? "," : thouSep;
	var sign = number < 0 ? "-" : "";
	var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
	var j = (j = i.length) > 3 ? j % 3 : 0;

	return sign +
		(j ? i.substr(0, j) + thouSep : "") +
		i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
		(decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}

function dateToStr(dt) {
	return (dt.getMonth()+1) + "/" + dt.getDate() + "/" + dt.getFullYear();
}










