const DB_DATE_FORMAT = 'yyyy-MM-dd'
const DB_DATE_FORMAT_WITH_TIME = 'yyyy-MM-dd HH:mm:ss'

module.exports = {
    DB_DATE_FORMAT,
    DB_DATE_FORMAT_WITH_TIME
}