const PanelEventTypes = {
    GENERAL: 'general'
}

const PanelEventUnits = {
    WEEK: 'week',
    DAY: 'day'
}

const DataTypes = {
    SPECIAL: 'special',
    EVENT: 'event'
}

const DATE_FORMAT_WITH_TIME = 'yyyy-MM-dd HH:mm:ss'
const DATE_FORMAT = 'yyyy-MM-dd'

module.exports = {
    PanelEventTypes,
    PanelEventUnits,
    DataTypes,
    DATE_FORMAT_WITH_TIME,
    DATE_FORMAT
}