const EstablishmentName = {
    APPY_HOUR: 'AppyHour'
}

const Locations = {
    MS: { name: 'msp', latitude: 44.9778, longitude: -93.2650 },
    IC: { name: 'ic', latitude: 41.6611, longitude: -91.5302 },
    DEN: { name: 'den', latitude: 39.7392, longitude: -104.9903 },
    LOU: { name: 'lou', latitude: 38.2527, longitude: -85.7585 }
}

const Timezones = {
    MST: 'MST',
    CST: 'CST',
    EST: 'EST'
}

const TopicNames = {

}

module.exports ={
    EstablishmentName,
    Locations,
    Timezones,
    TopicNames
}