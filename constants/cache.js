const Errors = {
    TIME_OUT: 'time out',
    NOT_FOUND: 'not found'
}

module.exports = {
    Errors
}