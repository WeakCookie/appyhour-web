/* ACCOUNT SETTINGS */

function editSettings() {
	$("#settings-edit-phone").inputmask({"mask": "(999) 999-9999"});

	document.getElementById("btnEditAccountSettings").style.display = "none";
	document.getElementById("displayAccountSettings").style.display = "none";
	document.getElementById("editAccountSettings").style.display = "block";
	document.getElementById("btnSaveAccountSettingsTop").style.display = "inline-block";
	document.getElementById("btnCancelEditAccountSettings").style.display = "inline-block";
}

function cancelSettings(e) {
	e.preventDefault();

	document.getElementById("btnEditAccountSettings").style.display = "block";
	document.getElementById("displayAccountSettings").style.display = "block";
	document.getElementById("editAccountSettings").style.display = "none";
	document.getElementById("btnSaveAccountSettingsTop").style.display = "none";
	document.getElementById("btnCancelEditAccountSettings").style.display = "none";

	document.getElementById("saveSettingsErr").innerHTML = "";
	document.getElementById("saveSettingsErr").style.display = "none";
	document.getElementById("editAccountSettings").reset();
}

function saveSettings(e) {
	e.preventDefault();

	var error = "";

	var email = document.getElementById("settings-edit-email").value;
	var phone = document.getElementById("settings-edit-phone").value;
	phone = phone.replace(/\D/g,'');
	var username = document.getElementById("settings-edit-username").value;

	if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
		error = error + "<br/>Error: Invalid email address.";
	}

	if(phone.length > 0 && phone.length != 10) {
		error = error + "<br/>Error: Please enter a valid phone number.";
	}

	if(username == "") {
		error = error + "<br/>Error: Please enter a username.";
	}

	if(error == "") {
		var params = {
			email: email,
			phone: phone,
			username: username
		};

		$.post('/savesettings', params, function(data) {
			if(data == "true") {
				location.reload();
			}
			else {
				document.getElementById("saveSettingsErr").innerHTML = "Error: " + data;
				document.getElementById("saveSettingsErr").style.display = "block";
			}
		});
	}
	else {
		document.getElementById("saveSettingsErr").innerHTML = error;
		document.getElementById("saveSettingsErr").style.display = "block";
	}
}

function openUpdatePasswordModal(e) {
	e.preventDefault();
	$("#updatePasswordModal").modal("show");
}

function cancelUpdatePassword() {
	document.getElementById("updatePasswordErr").innerHTML = "";
	document.getElementById("updatePasswordErr").style.display = "none";
	document.getElementById("updatePasswordForm").reset();
}

function updatePassword(e) {
	e.preventDefault();

	var currPw = document.getElementById("settings-current-pw").value;
	var newPw = document.getElementById("settings-new-pw").value;
	var newPwConfirm = document.getElementById("settings-new-pw-confirm").value;

	var error = "";

	if(currPw == "") {
		error = error + "<br/>Error: Please enter your current password.";
	}

	if(newPw == "") {
		error = error + "<br/>Error: Please enter your new password.";
	}

	if(newPwConfirm == "") {
		error = error + "<br/>Error: Please enter your new password confirmation.";
	}

	if(newPw != "" && newPw.length < 6) {
		error = error + "<br/>Error: Password must be at least 6 characters.";
	}

	if(newPw != "" && newPwConfirm != "" && newPw != newPwConfirm) {
		error = error + "<br/>Error: New Password and New Password Confirm do not match.";
	}

	if(currPw != "" && newPw != "" && currPw == newPw) {
		error = error + "<br/>Error: New Password can't be your current password.";
	}

	if(error == "") {
		var params = {
			currPw: currPw,
			newPw: newPw
		};

		$.post('/updatepassword', params, function(data) {
			if(data == "true") {
				location.reload();
			}
			else {
				document.getElementById("updatePasswordErr").innerHTML = "Error: " + data;
				document.getElementById("updatePasswordErr").style.display = "block";
			}
		});
	}
	else {
		document.getElementById("updatePasswordErr").innerHTML = error;
		document.getElementById("updatePasswordErr").style.display = "block";
	}
}

/* PAYMENT INFO */

function viewPlanDetails(e) {
	e.preventDefault();
	if (document.getElementById("plan-metadata").style.display == "none") {
		document.getElementById("plan-metadata").style.display = "block";
		document.getElementById("view-details-show").style.display = "none";
		document.getElementById("view-details-hide").style.display = "block";
		document.getElementById("view-details-show-mobile").style.display = "none";
		document.getElementById("view-details-hide-mobile").style.display = "block";
	} else {
		document.getElementById("plan-metadata").style.display = "none";
		document.getElementById("view-details-show").style.display = "block";
		document.getElementById("view-details-hide").style.display = "none";
		document.getElementById("view-details-show-mobile").style.display = "block";
		document.getElementById("view-details-hide-mobile").style.display = "none";
	}
}

/* MANAGERS */

function openAddManagerModal() {
	$("#settings-manager-phone").inputmask({"mask": "(999) 999-9999"});
	$("#addManagerModal").modal("show");
}

function cancelAddManager() {
	document.getElementById("addManagerErr").innerHTML = "";
	document.getElementById("addManagerErr").style.display = "none";
	document.getElementById("addManagerForm").reset();
}

function addManager(e) {
	e.preventDefault();
	document.getElementById("btnAddManager").disabled = true;
	$("body").css("cursor", "progress");

	var error = "";

	var estId = document.getElementById("managerEst").value;
	var email = document.getElementById("settings-manager-email").value;
	var phone = document.getElementById("settings-manager-phone").value;
	phone = phone.replace(/\D/g,'');
	var username = document.getElementById("settings-manager-username").value;

	if(estId == "") {
		error = error + "<br/>Error: Please choose an establishment.";
	}

	if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
		error = error + "<br/>Error: Invalid email address.";
	}

	if(phone.length > 0 && phone.length != 10) {
		error = error + "<br/>Error: Please enter a valid phone number.";
	}

	if(username == "") {
		error = error + "<br/>Error: Please enter a username.";
	}

	if(error == "") {
		var params = {
			managerEst: estId,
			managerEmail: email,
			managerPhone: phone,
			managerUsername: username
		};

		$.post('/addmanager', params, function(data) {
			if(data == "true") {
				location.reload();
			}
			else {
				document.getElementById("btnAddManager").disabled = false;
				$("body").css("cursor", "default");
				document.getElementById("addManagerErr").innerHTML = "Error: " + data;
				document.getElementById("addManagerErr").style.display = "block";
			}
		});
	}
	else {
		document.getElementById("btnAddManager").disabled = false;
		$("body").css("cursor", "default");
		document.getElementById("addManagerErr").innerHTML = error;
		document.getElementById("addManagerErr").style.display = "block";
	}
}

function deleteManager(e, managerId) {
	e.preventDefault();

	swal({
	  title: "Are you sure you want to delete this manager?",
	  buttons: ["Cancel", "Delete"],
	  dangerMode: true,
	  icon: "warning"
	})
	.then((willDelete) => {
		if(willDelete) {
			var params = { managerId: managerId };

			$.post('/deletemanager', params, function(data) {
				if(data == "true") {
					location.reload();
				}
				else {
					console.log(data);
				}
			});
		}
	});
}