// Sections:
// 1. Global Variables
// 2. Common Function
// 3. Random/Other Function
// 4. Validations

// GLOBAL VARIABLES START
var errorMsgs = [];
var isDrinkToggleSelected = null;
var dealTypeID = 0;
var dailyDealTypeId = 0; 
var isDealTypeSelected = false;
var isDailyDealTypeSelected = false;
var isEditDealTypeSelected = false;
var show = true;
var priceVal;
var priceType = "$";
var priceTypeId = 300;
var dailyPriceType = "$";
var dailyPriceTypeId = 300;
var price;
var details;
var hr;
var min;
var dinein;
var carryout;

var editErrorMsgs = [];

// GLOBAL VARIABLES END

// COMMON FUNCTIONS START
// Checks if array contains item, returns true if array DOES contain item
function contains(array, item){
	for (var i = 0; i < array.length; i++) {
      if (array[i] === item) {
        return true;
      }
    }
    return false;
}

// Checks if key pressed is a number, great for limiting what a user can enter
function isNumberKey(evt) {
	var charCode = (evt.which) ? evt.which : evt.keyCode;
	if (priceType == "$") {
		//46 = period
		if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		else
			return true;
	}
	else if (priceType == "%") {
		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;
		else
			return true;
	}
}

function countNumberOfPeriods(str) {
	var periods = 0;
	for (var i = 0; i < str.length; i++) {
		if (str.charAt(i) == ".")
			periods++;
	}
	return periods;
}

function getDealTypeRefIdByOGId(id){
	if (id == 0) {
		return 200; //beer
	} else if (id == 1) {
		return 202; //shot
	} else if (id == 2) {
		return 206; //mixed drink
	} else if (id == 3) {
		return 208; //margarita
	} else if (id == 4) {
		return 210; //martini
	} else if (id == 5) {
		return 212; //tumbler
	} else if (id == 6) {
		return 204; //wine
	} else if (id == 7) {
		return 251; //burger
	} else if (id == 8) {
		return 253; //appetizer
	} else if (id == 9) {
		return 255; //pizza
	} else if (id == 10) {
		return 257; //taco
	} else if (id == 11) {
		return 259; //sushi
	} else if (id == 12) {
		return 261; //bowl
	} else if (id == 13) {
		return 263; //wing
	} else if (id == 14) {
		return 214; //bottle
	} else if (id == 15) {
		return 216; //can
	}

	return 200;
}

function getDealTypeOGRefIdById(id){
	if (id == 200) {
		return 0; //beer
	} else if (id == 202) {
		return 1; //shot
	} else if (id == 206) {
		return 2; //mixed drink
	} else if (id == 208) {
		return 3; //margarita
	} else if (id == 210) {
		return 4; //martini
	} else if (id == 212) {
		return 5; //tumbler
	} else if (id == 204) {
		return 6; //wine
	} else if (id == 251) {
		return 7; //burger
	} else if (id == 253) {
		return 8; //appetizer
	} else if (id == 255) {
		return 9; //pizza
	} else if (id == 257) {
		return 10; //taco
	} else if (id == 259) {
		return 11; //sushi
	} else if (id == 261) {
		return 12; //bowl
	} else if (id == 263) {
		return 13; //wing
	} else if (id == 214) {
		return 14; //bottle
	} else if (id == 216) {
		return 15; //can
	}

	return 0;
}

// COMMON FUNCTIONS END hiddenDailyDealId

function selectDailyDay(day) {
	if(document.getElementById("daily-"+day+"ToggleButton").className == "btn btn-default") {
		document.getElementById("daily-"+day+"ToggleButton").className = "btn btn-primary";
		document.getElementById("daily-day-"+day).value = true;
	} else {
		document.getElementById("daily-"+day+"ToggleButton").className = "btn btn-default";
		document.getElementById("daily-day-"+day).value = false;
	}
}

function selectEditDay(day) {
	if(document.getElementById("edit-"+day+"ToggleButton").className == "btn btn-default") {
		document.getElementById("edit-"+day+"ToggleButton").className = "btn btn-primary";
		document.getElementById("edit-day-"+day).value = true;
	} else {
		document.getElementById("edit-"+day+"ToggleButton").className = "btn btn-default";
		document.getElementById("edit-day-"+day).value = false;
	}
}

function selectDailyDineinTakeout(dOrT) {
	if(document.getElementById("daily-"+dOrT+"ToggleButton").className == "btn btn-default") {
		document.getElementById("daily-"+dOrT+"ToggleButton").className = "btn btn-primary";
		document.getElementById("daily-"+dOrT).value = true;
	}
	else {
		document.getElementById("daily-"+dOrT+"ToggleButton").className = "btn btn-default";
		document.getElementById("daily-"+dOrT).value = false;
	}
}

function selectEditDineinTakeout(dOrT, force) {
	if(document.getElementById("edit-"+dOrT+"ToggleButton").className == "btn btn-default" || force) {
		document.getElementById("edit-"+dOrT+"ToggleButton").className = "btn btn-primary";
		document.getElementById("edit-"+dOrT).value = true;
	}
	else {
		document.getElementById("edit-"+dOrT+"ToggleButton").className = "btn btn-default";
		document.getElementById("edit-"+dOrT).value = false;
	}
}

function deleteDailySpecial(sid, day){
	document.getElementById("hiddenSpecialId").value = sid;

	swal({
	  	title: "Are you sure?",
	  	text: "Are you sure you want to delete this special?",
	  	icon: "warning",
	  	dangerMode: true,
	  	buttons: {
		  	cancel: {
		    	text: "Cancel",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Yes, delete it!",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((willDelete) => {
	  	if (willDelete) {
	    	document.getElementById("todaysSpecialsForm").submit();
	  	} else {
	    	return false;
	  	}
	});

	/*
	swal({   
	    title: "Are you sure?",   
	    text: "Are you sure you want to delete this special?",   
	    type: "warning",      
	    confirmButtonColor: "#DD6B55",   
	    confirmButtonText: "Yes, delete it!", 
	    showCancelButton: true,
	    closeOnConfirm: true,
	    html: true
  	}, function(isConfirm) {
      	if (isConfirm) {
        	document.getElementById("todaysSpecialsForm").submit();
        	// return true;
      	} else {
        	return false;
      	}
  	});*/
}



function editDeal(sid){
	var parameters = {
		sid: sid
	};

	$.get('/loadspecial', parameters, function(data) {
		// hidden, integrity fields
		document.getElementById("edit-special-ids").value = data.special_ids;
		document.getElementById("edit-special-id").value = data.special_id;
		document.getElementById("edit-price-type-id").value = data.price_type_ref_id;
		document.getElementById("edit-price").value = data.price;
		document.getElementById("edit-details").value = data.details;

		// days
		var days = data.days.split(",");
		var daysStr = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
		// clear day toggles
		for(var d in daysStr) {
			document.getElementById("edit-"+daysStr[d]+"ToggleButton").className = "btn btn-default";
			document.getElementById("edit-day-"+daysStr[d]).value = false;
			document.getElementById("edit-day-"+daysStr[d]+"-orig").value = false; // set the original values, used for saving
		}
		// set day toggles
		for(var d in days) {
			document.getElementById("edit-"+daysStr[days[d]]+"ToggleButton").className = "btn btn-primary";
			document.getElementById("edit-day-"+daysStr[days[d]]).value = true;
			document.getElementById("edit-day-"+daysStr[days[d]]+"-orig").value = true; // set the original values, used for saving
		}

		// start and end dates
		var start = new Date(data.start_time);
		var end = new Date(data.end_time);
		var startStr = (start.getHours() > 9 ? start.getHours() : "0"+start.getHours()) + ":" + (start.getMinutes() > 9 ? start.getMinutes() : "0"+start.getMinutes());
		var endStr =(end.getHours() > 9 ? end.getHours() : "0"+end.getHours()) + ":" + (end.getMinutes() > 9 ? end.getMinutes() : "0"+end.getMinutes());

		document.getElementById("edit-start").value = startStr;
		document.getElementById("edit-end").value = endStr;

		// dine-in or takeout
		if(data.dinein == 1)
			selectEditDineinTakeout('dinein', true);
		if(data.carryout == 1)
			selectEditDineinTakeout('takeout', true);

		// special type
		chooseEditDealType(getDealTypeOGRefIdById(data.deal_type_ref_id));

		$("#edit-deal-modal").modal("show");
	});
}

function deleteEditSpecial() {
	var sids = document.getElementById("edit-special-ids").value;

	swal({
	  	title: "Are you sure?",
	  	text: "Are you sure you want to delete this special?",
	  	icon: "warning",
	  	dangerMode: true,
	  	buttons: {
		  	cancel: {
		    	text: "Cancel",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Yes, delete it!",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((willDelete) => {
	  	if (willDelete) {
	    	var parameters = {
				sids: sids
			};

			$.post('/deleteeditspecial', parameters, function(data) {
				location.reload();
			});

	  	} else {
	    	return false;
	  	}
	});
} 

function validateEditSpecial(e) {
	e.preventDefault();

	validateEditSpecialDay();
	validateEditSpecialStartTime();
	validateEditSpecialEndTime();
	validateEditSpecialDineinTakeout();
	validateEditSpecialType();

	if(editErrorMsgs.length == 0)
		document.getElementById("editSpecialForm").submit();
}

function showEditErrors() {
	document.getElementById("edit-special-errors").innerHTML = "";
	for(el in editErrorMsgs) {
		document.getElementById("edit-special-errors").innerHTML += "<p class='error-alerts' style='margin:0 15px;'>" + editErrorMsgs[el] + "</p>";
	}

	if(editErrorMsgs.length > 0)
		document.getElementById("edit-special-errors").style.display = "block";
	else
		document.getElementById("edit-special-errors").style.display = "none";
}

function validateEditSpecialStartTime() {
	var startTime = document.getElementById("edit-start").value;
	var endTime = document.getElementById("edit-end").value;

	if(startTime == undefined || startTime == null || startTime == "") {
		document.getElementById("edit-start").className = "form-control-error";
		if (!contains(editErrorMsgs, "Make sure you set a start time!")) {
			editErrorMsgs.push("Make sure you set a start time!");
		}
	} 
	else {
		document.getElementById("edit-start").className = "form-control";
		var index = editErrorMsgs.indexOf("Make sure you set a start time!");
		if (index >= 0) {
			editErrorMsgs.splice(index, 1);
		}
	}

	if(startTime != undefined && startTime != null && startTime != "" && endTime != undefined && endTime != null && endTime != "") {
		var s = new Date("01/01/1970 " + startTime);
		var e = new Date("01/01/1970 " + endTime);
		
		if(s.getHours() <= 0) 
			s.setDate(s.getDate()+1);
		
		if(e.getHours() <= 0) 
			e.setDate(e.getDate()+1);

		if(s >= e) {
			document.getElementById("edit-start").className = "form-control-error";
			document.getElementById("edit-end").className = "form-control-error";
			if (!contains(editErrorMsgs, "Make sure the start time is after the end time!")) {
				editErrorMsgs.push("Make sure the start time is after the end time!");
			}
		}
		else {
			document.getElementById("edit-start").className = "form-control";
			document.getElementById("edit-end").className = "form-control";
			var index = editErrorMsgs.indexOf("Make sure the start time is after the end time!");
			if (index >= 0) {
				editErrorMsgs.splice(index, 1);
			}
		}
	}

	showEditErrors();
}

function validateEditSpecialEndTime() {
	var startTime = document.getElementById("edit-start").value;
	var endTime = document.getElementById("edit-end").value;

	if(endTime == undefined || endTime == null || endTime == "") {
		document.getElementById("edit-end").className = "form-control-error";
		if (!contains(editErrorMsgs, "Make sure you set an end time!")) {
			editErrorMsgs.push("Make sure you set an end time!");
		}
	} 
	else {
		document.getElementById("edit-end").className = "form-control";
		var index = editErrorMsgs.indexOf("Make sure you set an end time!");
		if (index >= 0) {
			editErrorMsgs.splice(index, 1);
		}
	}

	if(startTime != undefined && startTime != null && startTime != "" && endTime != undefined && endTime != null && endTime != "") {
		var s = new Date("01/01/1970 " + startTime);
		var e = new Date("01/01/1970 " + endTime);
		
		if(s.getHours() <= 0) 
			s.setDate(s.getDate()+1);
		
		if(e.getHours() <= 0) 
			e.setDate(e.getDate()+1);

		if(s >= e) {
			document.getElementById("edit-start").className = "form-control-error";
			document.getElementById("edit-end").className = "form-control-error";
			if (!contains(editErrorMsgs, "Make sure the start time is after the end time!")) {
				editErrorMsgs.push("Make sure the start time is after the end time!");
			}
		}
		else {
			document.getElementById("edit-start").className = "form-control";
			document.getElementById("edit-end").className = "form-control";
			var index = editErrorMsgs.indexOf("Make sure the start time is after the end time!");
			if (index >= 0) {
				editErrorMsgs.splice(index, 1);
			}
		}
	}

	showEditErrors();
}

function validateEditSpecialDay() {
	var isOneDayChecked = false;

	var mon = 0;
	var tue = 0;
	var wed = 0;
	var thu = 0;
	var fri = 0;
	var sat = 0;
	var sun = 0;

	var monrow = false;
	var tuerow = false;
	var wedrow = false;
	var thurow = false;
	var frirow = false;
	var satrow = false;
	var sunrow = false;

	var rows = document.getElementById("dailyDeals").rows;
	for(var r in rows) {
		var row = rows[r];
		if(row.cells && row.cells.length > 0) {
			if(row.cells[0].innerHTML.indexOf("Monday") > -1) {
				monrow = true;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Tuesday") > -1) {
				monrow = false;
				tuerow = true;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Wednesday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = true;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Thursday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = true;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Friday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = true;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Saturday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = true;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Sunday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = true;
			}
			else if(row.cells[0].innerHTML.indexOf("No specials") == -1) {
				if(monrow) {
					mon += 1;
				}
				else if(tuerow) {
					tue += 1;
				}
				else if(wedrow) {
					wed += 1;
				}
				else if(thurow) {
					thu += 1;
				}
				else if(frirow) {
					fri += 1;
				}
				else if(satrow) {
					sat += 1;
				}
				else if(sunrow) {
					sun += 1;
				}
			}
		}
	}	

	var weeklySpecialsLimit = parseInt(document.getElementById("weeklySpecialsLimit").value);
	
	if(document.getElementById("edit-day-mon").value == "true" && mon < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("edit-day-mon").value = true;
	} 
	else if(document.getElementById("edit-day-mon").value == "true" && mon >= weeklySpecialsLimit) {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Monday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.")) {
			editErrorMsgs.push("Monday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.");
		}
	}

	if(document.getElementById("edit-day-tue").value == "true" && tue < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("edit-day-tue").value = true;
	} 
	else if(document.getElementById("edit-day-tue").value == "true" && tue >= weeklySpecialsLimit) {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Tuesday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.")) {
			editErrorMsgs.push("Tuesday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.");
		}
	}

	if(document.getElementById("edit-day-wed").value == "true" && wed < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("edit-day-wed").value = true;
	} 
	else if(document.getElementById("edit-day-wed").value == "true" && wed >= weeklySpecialsLimit) {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Wednesday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.")) {
			editErrorMsgs.push("Wednesday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.");
		}
	}

	if(document.getElementById("edit-day-thu").value == "true" && thu < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("edit-day-thu").value = true;
	} 
	else if(document.getElementById("edit-day-thu").value == "true" && thu >= weeklySpecialsLimit) {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Thursday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.")) {
			editErrorMsgs.push("Thursday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.");
		}
	}

	if(document.getElementById("edit-day-fri").value == "true" && fri < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("edit-day-fri").value = true;
	} 
	else if(document.getElementById("edit-day-fri").value == "true" && fri >= weeklySpecialsLimit) {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Friday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.")) {
			editErrorMsgs.push("Friday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.");
		}
	}

	if(document.getElementById("edit-day-sat").value == "true" && sat < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("edit-day-sat").value = true;
	} 
	else if(document.getElementById("edit-day-sat").value == "true" && sat >= weeklySpecialsLimit) {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Saturday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.")) {
			editErrorMsgs.push("Saturday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.");
		}
	}

	if(document.getElementById("edit-day-sun").value == "true" && sun < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("edit-day-sun").value = true;
	} 
	else if(document.getElementById("edit-day-sun").value == "true" && sun >= weeklySpecialsLimit) {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Sunday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.")) {
			editErrorMsgs.push("Sunday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.");
		}
	}

	if(isOneDayChecked){
		document.getElementById("edit-dayToggle").style.border = "none";
		var index = editErrorMsgs.indexOf("Please select the day(s) you would like this special to occur on!");
		if (index >= 0) {
			editErrorMsgs.splice(index, 1);
		}
	} 
	else {
		document.getElementById("edit-dayToggle").style.border = "1px solid #FF0000";
		document.getElementById("edit-dayToggle").style.borderRadius = "4px";
		if (!contains(editErrorMsgs, "Please select the day(s) you would like this special to occur on!")) {
			editErrorMsgs.push("Please select the day(s) you would like this special to occur on!");
		}
	}

	showEditErrors();
}

function validateEditSpecialDineinTakeout() {
	var dinein = document.getElementById("edit-dinein").value;
	var takeout = document.getElementById("edit-takeout").value;

	if(dinein == "false" && takeout == "false") {
		document.getElementById("edit-takeout-toggle").className = "btn-group btn-group-justified takeout-frame-error";
		if (!contains(editErrorMsgs, "Make sure you select dine-in and/or takeout!")) {
			editErrorMsgs.push("Make sure you select dine-in and/or takeout!");
		}
	}
	else {
		document.getElementById("edit-takeout-toggle").className = "btn-group btn-group-justified";
		var index = editErrorMsgs.indexOf("Make sure you select dine-in and/or takeout!");
		if (index >= 0) {
			editErrorMsgs.splice(index, 1);
		}
	}

	showEditErrors();
}

function validateEditSpecialType() {
	if (!isEditDealTypeSelected) {
		if (!contains(editErrorMsgs, "Make sure you select a deal type icon!")) {
			editErrorMsgs.push("Make sure you select a deal type icon!");
		}
	} 
	else {
		var index = editErrorMsgs.indexOf("Make sure you select a deal type icon!");
		if (index >= 0) {
			editErrorMsgs.splice(index, 1);
		}
	}

	showEditErrors();
}









function removeDailyDeal(sid){
	document.getElementById("hiddenDailyDealId").value = sid;

	swal({
	  	title: "Are you sure?",
	  	text: "Are you sure you want to delete this special?",
	  	icon: "warning",
	  	dangerMode: true,
	  	buttons: {
		  	cancel: {
		    	text: "Cancel",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Yes, delete it!",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((willDelete) => {
	  	if (willDelete) {
	    	document.getElementById("removeDailyDeal").submit();
	  	} else {
	    	return false;
	  	}
	});

	/*
	swal({   
	    title: "Are you sure?",   
	    text: "Are you sure you want to delete this special?",   
	    type: "warning",      
	    confirmButtonColor: "#DD6B55",   
	    confirmButtonText: "Yes, delete it!", 
	    showCancelButton: true,
	    closeOnConfirm: true,
	    html: true
  	}, function(isConfirm) {
      	if (isConfirm) {
        	document.getElementById("removeDailyDeal").submit();
        	// return true;
      	} else {
        	return false;
      	}
  	});
	*/
}

function isMobile() {
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent))
		return true;
	else
		return false;
}

function chooseHours(hrSelect) {
	var options = "";
	var hrs = parseInt(hrSelect.options[hrSelect.selectedIndex].text);
	
	var durationMinString = "duration-min";
	if (hrSelect.id == "edit-duration-hr")
		durationMinString = "edit-" + durationMinString;
	
	var min = document.getElementById(durationMinString).options[document.getElementById(durationMinString).selectedIndex].text;

	if (min != "Minutes") {
		min = parseInt(min);

		if (hrs == 5) {
			options += "<option disabled>Minutes</option><option value='0' selected>0 min</option>";
		} else if (hrs == 0) {
			if (min < 30) {
				options += "<option disabled selected>Minutes</option>";
			} else {
				options += "<option disabled>Minutes</option>";
			}
			for(var i = 30; i < 60; i += 15) {
				if (min == i) {
			    	options += "<option value='" + i + "' selected>" + i + " min</option>";
			    } else {
			    	options += "<option value='" + i + "'>" + i + " min</option>";
			    }
			}
		} else {
			options += "<option disabled>Minutes</option>";
			for(var i = 0; i < 60; i += 15) {
			    if (min == i) {
			    	options += "<option value='" + i + "' selected>" + i + " min</option>";
			    } else {
			    	options += "<option value='" + i + "'>" + i + " min</option>";
			    }
			}
		}
	} else {
		options += "<option disabled selected>Minutes</option>";
		if (hrs == 5) {
			options = "<option value='0'>0 min</option>";
		} else if (hrs == 0) {
			for(var i = 30; i < 60; i += 15) {
				options += "<option value='" + i + "'>" + i + " min</option>";
			}
		} else {
			for(var i = 0; i < 60; i += 15) {
			    options += "<option value='" + i + "'>" + i + " min</option>";
			}
		}
	}

	$("select[id='" + durationMinString + "']").find('option').remove().end().append($(options));
}

function toggleType(type) {
	if (type == 'food') {
		isDrinkToggleSelected = false;
		document.getElementById("allDealTypes").style.display = "block";
		document.getElementById("drinkIcons").style.display = "none";
		document.getElementById("foodIcons").style.display = "block";
		document.getElementById("allDealTypes").className = "deal-type-button-frame";
		//document.getElementById("drinkToggleButton").className = "btn btn-default";
		//document.getElementById("foodToggleButton").className = "btn btn-default active";
	}
	else if (type == 'drink') {
		isDrinkToggleSelected = true;
		document.getElementById("allDealTypes").style.display = "block";
		document.getElementById("drinkIcons").style.display = "block";
		document.getElementById("foodIcons").style.display = "none";
		document.getElementById("allDealTypes").className = "deal-type-button-frame";
		//document.getElementById("drinkToggleButton").className = "btn btn-default active";
		//document.getElementById("foodToggleButton").className = "btn btn-default";
	}
}

function togglePriceType(type) {
	document.getElementById("price").value = "";
	document.getElementById("price").className = "form-control";

	var index = errorMsgs.indexOf("Price needs to be less than $100!");
	if (index >= 0) {
		errorMsgs.splice(index, 1);
	}
	index = errorMsgs.indexOf("Make sure you add a price!");
	if (index >= 0) {
		errorMsgs.splice(index, 1);
	}
	index = errorMsgs.indexOf("Percent off needs to be equal or less than 100%!");
	if (index >= 0) {
		errorMsgs.splice(index, 1);
	}
	index = errorMsgs.indexOf("Make sure you add a percent off!");
	if (index >= 0) {
		errorMsgs.splice(index, 1);
	}
	getValidateInnerHtml();
	showValidateCreateLabel();

	if (type == 'money') {
		priceType = "$";
		priceTypeId = 300;
		document.getElementById("price").disabled = false;
		document.getElementById("priceLabel").innerHTML = "Price";
		document.getElementById("percentSignInPriceTextbox").style.display = "none";
		document.getElementById("moneySignInPriceTextbox").style.display = "block";
		document.getElementById("bogoInPriceTextbox").style.display = "none";
		document.getElementById("freeInPriceTextbox").style.display = "none";
		document.getElementById("moneyToggleButton").className = "btn btn-primary";
		document.getElementById("percentToggleButton").className = "btn btn-default";
		document.getElementById("bogoToggleButton").className = "btn btn-default";
		document.getElementById("freeToggleButton").className = "btn btn-default";
	}
	else if (type == 'percent') {
		priceType = "%";
		priceTypeId = 301;
		document.getElementById("price").disabled = false;
		document.getElementById("priceLabel").innerHTML = "Percent Off";
		document.getElementById("percentSignInPriceTextbox").style.display = "block";
		document.getElementById("moneySignInPriceTextbox").style.display = "none";
		document.getElementById("bogoInPriceTextbox").style.display = "none";
		document.getElementById("freeInPriceTextbox").style.display = "none";
		document.getElementById("moneyToggleButton").className = "btn btn-default";
		document.getElementById("percentToggleButton").className = "btn btn-primary";
		document.getElementById("bogoToggleButton").className = "btn btn-default";
		document.getElementById("freeToggleButton").className = "btn btn-default";
	} else if (type == 'bogo'){
		priceType = "Buy One Get One";
		priceTypeId = 302;
		document.getElementById("price").disabled = true;
		document.getElementById("priceLabel").innerHTML = "BOGO";
		document.getElementById("percentSignInPriceTextbox").style.display = "none";
		document.getElementById("moneySignInPriceTextbox").style.display = "none";
		document.getElementById("bogoInPriceTextbox").style.display = "block";
		document.getElementById("freeInPriceTextbox").style.display = "none";
		document.getElementById("moneyToggleButton").className = "btn btn-default";
		document.getElementById("percentToggleButton").className = "btn btn-default";
		document.getElementById("bogoToggleButton").className = "btn btn-primary";
		document.getElementById("freeToggleButton").className = "btn btn-default";
	} else if (type == 'free'){
		priceType = "Free";
		priceTypeId = 307;
		document.getElementById("price").disabled = true;
		document.getElementById("priceLabel").innerHTML = "Free";
		document.getElementById("percentSignInPriceTextbox").style.display = "none";
		document.getElementById("moneySignInPriceTextbox").style.display = "none";
		document.getElementById("bogoInPriceTextbox").style.display = "none";
		document.getElementById("freeInPriceTextbox").style.display = "block";
		document.getElementById("moneyToggleButton").className = "btn btn-default";
		document.getElementById("percentToggleButton").className = "btn btn-default";
		document.getElementById("bogoToggleButton").className = "btn btn-default";
		document.getElementById("freeToggleButton").className = "btn btn-primary";
	}
}

function selectDineinTakeout(dOrT) {
	if(document.getElementById(dOrT+"ToggleButton").className == "btn btn-default") {
		document.getElementById(dOrT+"ToggleButton").className = "btn btn-primary";
		document.getElementById(dOrT).value = true;
	}
	else {
		document.getElementById(dOrT+"ToggleButton").className = "btn btn-default";
		document.getElementById(dOrT).value = false;
	}
	validateCreateNewDealDineinTakeout();
	getValidateInnerHtml();
	showValidateCreateLabel();
}

function toggleDailyPriceType(type) {
	document.getElementById("daily-price").value = "";
	document.getElementById("daily-price").className = "form-control";

	if (type == 'money') {
		dailyPriceType = "$";
		dailyPriceTypeId = 300;
		document.getElementById("daily-price").disabled = false;
		document.getElementById("daily-priceLabel").innerHTML = "Price";
		document.getElementById("daily-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("daily-moneySignInPriceTextbox").style.display = "block";
		document.getElementById("daily-bogoInPriceTextbox").style.display = "none";
		document.getElementById("daily-freeInPriceTextbox").style.display = "none";
		document.getElementById("daily-moneyToggleButton").className = "btn btn-primary";
		document.getElementById("daily-percentToggleButton").className = "btn btn-default";
		document.getElementById("daily-bogoToggleButton").className = "btn btn-default";
		document.getElementById("daily-freeToggleButton").className = "btn btn-default";
	}
	else if (type == 'percent') {
		dailyPriceType = "%";
		dailyPriceTypeId = 301;
		document.getElementById("daily-price").disabled = false;
		document.getElementById("daily-priceLabel").innerHTML = "Percent Off";
		document.getElementById("daily-percentSignInPriceTextbox").style.display = "block";
		document.getElementById("daily-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("daily-bogoInPriceTextbox").style.display = "none";
		document.getElementById("daily-freeInPriceTextbox").style.display = "none";
		document.getElementById("daily-moneyToggleButton").className = "btn btn-default";
		document.getElementById("daily-percentToggleButton").className = "btn btn-primary";
		document.getElementById("daily-bogoToggleButton").className = "btn btn-default";
		document.getElementById("daily-freeToggleButton").className = "btn btn-default";
	} else if (type == 'bogo'){
		dailyPriceType = "Buy One Get One";
		dailyPriceTypeId = 302;
		document.getElementById("daily-price").disabled = true;
		document.getElementById("daily-priceLabel").innerHTML = "BOGO";
		document.getElementById("daily-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("daily-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("daily-bogoInPriceTextbox").style.display = "block";
		document.getElementById("daily-freeInPriceTextbox").style.display = "none";
		document.getElementById("daily-moneyToggleButton").className = "btn btn-default";
		document.getElementById("daily-percentToggleButton").className = "btn btn-default";
		document.getElementById("daily-bogoToggleButton").className = "btn btn-primary";
		document.getElementById("daily-freeToggleButton").className = "btn btn-default";
	} else if (type == 'free'){
		dailyPriceType = "Free";
		dailyPriceTypeId = 307;
		document.getElementById("daily-price").disabled = true;
		document.getElementById("daily-priceLabel").innerHTML = "Free";
		document.getElementById("daily-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("daily-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("daily-bogoInPriceTextbox").style.display = "none";
		document.getElementById("daily-freeInPriceTextbox").style.display = "block";
		document.getElementById("daily-moneyToggleButton").className = "btn btn-default";
		document.getElementById("daily-percentToggleButton").className = "btn btn-default";
		document.getElementById("daily-bogoToggleButton").className = "btn btn-default";
		document.getElementById("daily-freeToggleButton").className = "btn btn-primary";
	}

	document.getElementById("hiddenDailyPriceType").value = dailyPriceTypeId;
}

function chooseDealType(typeID, addOrEdit) {
	document.getElementById("allDealTypes").className = "";

	dealTypeID = typeID;
	
	var dealTypeString = "deal-type-";
	if (addOrEdit == "Edit")
		dealTypeString = "edit-" + dealTypeString;
	
	for (var i = 0; i <= 15; i++) {
		if (i == typeID) {
			document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
			document.getElementById(dealTypeString+i).style.border = "2px solid orange";
			isDealTypeSelected = true;
			var index = errorMsgs.indexOf("Make sure you select a special type icon!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}	
			getValidateInnerHtml();
			
		} else {
			document.getElementById(dealTypeString+i).style.background = "white";
			document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
		}
	}

	if (dealTypeID == null) {
		isDealTypeSelected = false;
	}
}

function goToConfirmation() {
	document.getElementById("add-edit-deal-submit").disabled = false;

	price = document.getElementById("price").value;
	details = document.getElementById("details").value.trim();
	dinein = document.getElementById("dinein").value;
	takeout = document.getElementById("takeout").value;
	hr = document.getElementById("duration-hr").value;
	min = document.getElementById("duration-min").value;
	startDate = document.getElementById("special-start-date").value;
	startTime = document.getElementById("special-start-time").value;

	var validateLabel = validateCreateNewDealFields();
	var validate = validateLabel[0];

	if(validate != undefined){
		document.getElementById("validateCreateLabel").innerHTML = null;
		for (val in validateLabel) {
			document.getElementById("validateCreateLabel").innerHTML += validateLabel[val] + "<br />";
			showValidateCreateLabel();		
		}
		return;
	}
	document.getElementById("validateCreateLabel").style.display = "none";

	document.getElementById("type-confirm").innerHTML = getDealTypeIcon(dealTypeID);

  	if(priceType != undefined && priceType != null && priceType == "Buy One Get One"){
		document.getElementById("price-confirm").innerHTML = "BOGO";
		document.getElementById("details-confirm").innerHTML = priceType + " " + details;
	} else if (priceType != undefined && priceType != null && priceType == "Free"){
		document.getElementById("price-confirm").innerHTML = "Free";
		document.getElementById("details-confirm").innerHTML = priceType + " " + details;
	} else if (priceType != undefined && priceType != null && priceType == "%"){
		document.getElementById("price-confirm").innerHTML = parseFloat(price).toFixed(0) + priceType + " Off";
		document.getElementById("details-confirm").innerHTML = parseFloat(price).toFixed(0) + priceType + " Off " + details;
	} else {
		document.getElementById("price-confirm").innerHTML = priceType + parseFloat(price).toFixed(2);
		document.getElementById("details-confirm").innerHTML = priceType + parseFloat(price).toFixed(2) + " " + details;
	}

  	document.getElementById("establishment-confirm").innerHTML = document.getElementById("establishment-name").innerHTML;
	if (hr != 0)
		document.getElementById("time-remaining-confirm").innerHTML = hr + "h " + min + "m";
	else
		document.getElementById("time-remaining-confirm").innerHTML = min + "m";

	if (document.getElementById("showScheduleFields").style.display == "block") {
		document.getElementById("scheduled-div").style.display = "block";
		document.getElementById("start-time-confirm").innerHTML = formatStartTime(startTime);
		document.getElementById("start-date-confirm").innerHTML = formatStartDate(startDate);
	}

	document.getElementById("hiddenPriceType").value = priceTypeId;
	document.getElementById("hiddenPrice").value = price;
	document.getElementById("hiddenDetails").value = details;
	document.getElementById("hiddenHours").value = hr;
	document.getElementById("hiddenMinutes").value = min;
	document.getElementById("hiddenTypeConfirm").value = dealTypeID;
	document.getElementById("hiddenSpecialStartDate").value = startDate;
	document.getElementById("hiddenSpecialStartTime").value = startTime;
	document.getElementById("hiddenDinein").value = dinein;
	document.getElementById("hiddenTakeout").value = takeout;

	if (document.getElementById("showScheduleFields").style.display == "block")
		document.getElementById("hiddenIsScheduledSpecial").value = true;
	else
		document.getElementById("hiddenIsScheduledSpecial").value = false;

	$("#add-deal-confirm").modal("show");	
}

function clearSpecial(e) {
	e.preventDefault();
	document.getElementById("price").value = "";
	document.getElementById("price").className = "form-control";
	document.getElementById("details").value = "";
	document.getElementById("details").className = "form-control";
	document.getElementById("duration-hr").selectedIndex = 0;
	document.getElementById("duration-hr").className = "form-control";
	document.getElementById("duration-min").selectedIndex = 0;
	document.getElementById("duration-min").className = "form-control";
	document.getElementById("special-start-date").selectedIndex = 0;
	document.getElementById("special-start-date").className = "form-control";
	document.getElementById("special-start-time").value = "";
	document.getElementById("special-start-time").className = "form-control";
	//isDrinkToggleSelected = null;
	//document.getElementById("allDealTypes").style.display = "none";
	//document.getElementById("drinkIcons").style.display = "none";
	//document.getElementById("foodIcons").style.display = "none";
	//document.getElementById("drinkToggleButton").className = "btn btn-default";
	//document.getElementById("foodToggleButton").className = "btn btn-default";
	chooseDealType(null, null);
	togglePriceType("money");
	errorMsgs = [];
	getValidateInnerHtml();
	showValidateCreateLabel();
}

function getDealTypeIcon(typeID) {
	switch (typeID) {
	case 0:
		return "<div class='deal-type-icon'><img src='assets/img/beer-icon.png' /></div>";
		break;
	case 1:
		return "<div class='deal-type-icon'><img src='assets/img/shot-icon.png' /></div>";
		break;
	case 2:
		return "<div class='deal-type-icon'><img src='assets/img/mixed-drink-icon.png' /></div>";
		break;
	case 3:
		return "<div class='deal-type-icon'><img src='assets/img/margarita-icon.png' /></div>";
		break;
	case 4:
		return "<div class='deal-type-icon'><img src='assets/img/martini-icon.png' /></div>";
		break;
	case 5:
		return "<div class='deal-type-icon'><img src='assets/img/tumbler-icon.png' /></div>";
		break;
	case 6:
		return "<div class='deal-type-icon'><img src='assets/img/wine-icon.png' /></div>";
		break;
	case 7:
		return "<div class='deal-type-icon'><img src='assets/img/burger-icon.png' /></div>";
		break;
	case 8:
		return "<div class='deal-type-icon'><img src='assets/img/appetizer-icon.png' /></div>";
		break;
	case 9:
		return "<div class='deal-type-icon'><img src='assets/img/pizza-icon.png' /></div>";
		break;
	case 10:
		return "<div class='deal-type-icon'><img src='assets/img/taco-icon.png' /></div>";
		break;
	case 11:
		return "<div class='deal-type-icon'><img src='assets/img/sushi.png' /></div>";
		break;
	case 12:
		return "<div class='deal-type-icon'><img src='assets/img/bowl.png' /></div>";
		break;
	case 13:
		return "<div class='deal-type-icon'><img src='assets/img/chickenwing.png' /></div>";
		break;
	case 14:
		return "<div class='deal-type-icon'><img src='assets/img/beerbottle.png' /></div>";
		break;
	case 15:
		return "<div class='deal-type-icon'><img src='assets/img/beercan.png' /></div>";
		break;
	default:
		return "";
		break;
	}
}

function toggleAddDailyDealDiv() {
  if(show) {
    document.getElementById("addDailyDealDiv").style.display = "block";
    document.getElementById("ds-edit").style.display = "none";
    show = false;
  } else {
    document.getElementById("addDailyDealDiv").style.display = "none";
    document.getElementById("ds-edit").style.display = "inline";
    show = true;
  }
}

function chooseDailyDealType(typeID) {
	dailyDealTypeId = getDealTypeRefIdByOGId(typeID);
  	document.getElementById("hiddenType").value = dailyDealTypeId;
  
  	var dealTypeString = "daily-deal-type-";
  
  	for (var i = 0; i <= 15; i++) {
    	if (i == typeID) {
      		document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
      		document.getElementById(dealTypeString+i).style.border = "2px solid orange";
      		document.getElementById(dealTypeString+i).value = dailyDealTypeId;
      		isDailyDealTypeSelected = true;
    	} else {
      		document.getElementById(dealTypeString+i).style.background = "white";
      		document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
      		document.getElementById(dealTypeString+i).value = 0;
    	}
  	}
}

function chooseEditDealType(typeID) {
	editDealTypeId = getDealTypeRefIdByOGId(typeID);
  	document.getElementById("hiddenTypeEdit").value = editDealTypeId;
  
  	var dealTypeString = "edit-deal-type-";
  
  	for (var i = 0; i <= 15; i++) {
    	if (i == typeID) {
      		document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
      		document.getElementById(dealTypeString+i).style.border = "2px solid orange";
      		document.getElementById(dealTypeString+i).value = editDealTypeId;
      		isEditDealTypeSelected = true;
    	} else {
      		document.getElementById(dealTypeString+i).style.background = "white";
      		document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
      		document.getElementById(dealTypeString+i).value = 0;
    	}
  	}
}

function addDailyDeal() {
  document.getElementById("daily-day-mon").checked = false;
  document.getElementById("daily-day-tue").checked = false;
  document.getElementById("daily-day-wed").checked = false;
  document.getElementById("daily-day-thu").checked = false;
  document.getElementById("daily-day-fri").checked = false;
  document.getElementById("daily-day-sat").checked = false;
  document.getElementById("daily-day-sun").checked = false;
  document.getElementById("daily-price").value = "";
  document.getElementById("daily-details").value = "";
  document.getElementById("daily-start").value = "";
  document.getElementById("daily-end").value = "";
  toggleAddDailyDealDiv();
}

function clearWeeklySpecial(e) {
	e.preventDefault();

	var days = ["mon", "tue", "wed", "thu", "fri", "sat", "sun"];
	for(var d in days) {
		document.getElementById("daily-"+days[d]+"ToggleButton").className = "btn btn-default";
		document.getElementById("daily-"+days[d]+"ToggleButton").style.backgroundColor = "white";
		document.getElementById("daily-"+days[d]+"ToggleButton").style.borderColor = "#ccc";
		document.getElementById("daily-day-"+days[d]).value = false;
	}

	document.getElementById("daily-start").value = "";
	document.getElementById("daily-start").className = "form-control";
	document.getElementById("daily-end").value = "";
	document.getElementById("daily-end").className = "form-control";

	toggleDailyPriceType("money");
	document.getElementById("daily-price").value = "";
	document.getElementById("daily-price").className = "form-control";
	document.getElementById("daily-details").value = "";
	document.getElementById("daily-details").className = "form-control";

	chooseDailyDealType(null, null);

	document.getElementById("validateDailyDealsAdd").innerHTML = "<br />";
}

// VALIDATION START - APPYHOUR EXCLUSIVE
function getValidateInnerHtml() {
	document.getElementById("validateCreateLabel").innerHTML = "";
	for(el in errorMsgs) {
		document.getElementById("validateCreateLabel").innerHTML += errorMsgs[el] + "<br />";
	}
}

function showValidateCreateLabel() {
	if (errorMsgs.length <= 0) {
		document.getElementById("validateCreateLabelDiv").style.display = "none";
		document.getElementById("validateCreateLabelDiv").style.marginBottom = "0";
	} else {
		document.getElementById("validateCreateLabelDiv").style.display = "block";
		document.getElementById("validateCreateLabelDiv").style.marginBottom = "20px";
	}
}

function validateCreateNewDealPrice() {
	price = document.getElementById("price").value;

	if (priceType == "$") {
		if (price == undefined || price == "") {
			document.getElementById("price").className = "form-control-error";
			if (!contains(errorMsgs, "Make sure you add a price!")) {
				errorMsgs.push("Make sure you add a price!");
			}
			var index = errorMsgs.indexOf("Price needs to be less than $100!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}	
		} else if(parseFloat(price) > 100) {
			document.getElementById("price").className = "form-control-error";
			if (!contains(errorMsgs, "Price needs to be less than $100!")) {
				errorMsgs.push("Price needs to be less than $100!");
			}
			index = errorMsgs.indexOf("Make sure you add a price!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}
		} else {
			document.getElementById("price").className = "form-control";
			var index = errorMsgs.indexOf("Price needs to be less than $100!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}
			index = errorMsgs.indexOf("Make sure you add a price!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}
		}
	}
	else if (priceType == "%") {
		if (price == undefined || price == "") {
			document.getElementById("price").className = "form-control-error";
			if (!contains(errorMsgs, "Make sure you add a percent off!")) {
				errorMsgs.push("Make sure you add a percent off!");
			}
			var index = errorMsgs.indexOf("Percent off needs to be equal or less than 100%!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}	
		} else if(parseFloat(price) > 100) {
			document.getElementById("price").className = "form-control-error";
			if (!contains(errorMsgs, "Percent off needs to be equal or less than 100%!")) {
				errorMsgs.push("Percent off needs to be equal or less than 100%!");
			}
			index = errorMsgs.indexOf("Make sure you add a percent off!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}
		} else {
			document.getElementById("price").className = "form-control";
			var index = errorMsgs.indexOf("Percent off needs to be equal or less than 100%!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}
			index = errorMsgs.indexOf("Make sure you add a percent off!");
			if (index >= 0) {
				errorMsgs.splice(index, 1);
			}
		}
	}

	getValidateInnerHtml();
	showValidateCreateLabel();
}

function validateCreateNewDealDetails() {
	details = document.getElementById("details").value;
	if (details == undefined || details == "") {
		document.getElementById("details").className = "form-control-error";
		if (!contains(errorMsgs, "Make sure you add some details!")) {
			errorMsgs.push("Make sure you add some details!");
		}
	} else {
		document.getElementById("details").className = "form-control";
		var index = errorMsgs.indexOf("Make sure you add some details!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
	}

	getValidateInnerHtml();
	showValidateCreateLabel();
}

function validateCreateNewDealHour() {
	hr = document.getElementById("duration-hr").value;
	if (hr == "Hours") {
		document.getElementById("duration-hr").className = "form-control-error";
		if (!contains(errorMsgs, "Make sure you set hours!")) {
			errorMsgs.push("Make sure you set hours!");
		}
	} else {
		document.getElementById("duration-hr").className = "form-control";
		var index = errorMsgs.indexOf("Make sure you set hours!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
		
	}

	getValidateInnerHtml();
	showValidateCreateLabel();
}

function validateCreateNewDealMinute() {
	min = document.getElementById("duration-min").value;
	if (min == "Minutes") {
		document.getElementById("duration-min").className = "form-control-error";
		if (!contains(errorMsgs, "Make sure you set minutes!")) {
			errorMsgs.push("Make sure you set minutes!");
		}
	} else {
		document.getElementById("duration-min").className = "form-control";
		var index = errorMsgs.indexOf("Make sure you set minutes!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
	}

	getValidateInnerHtml();
	showValidateCreateLabel();
}

function validateCreateNewDealStartDate() {
	startDate = document.getElementById("special-start-date").value;
	startTime = document.getElementById("special-start-time").value;
	var index;

	index = errorMsgs.indexOf("Make sure you enter a valid start time!");
	if (index >= 0) {
		errorMsgs.splice(index, 1);
	}

	if (startDate == undefined || startDate == null || startDate == "" || startDate == "Select Date") {
		document.getElementById("special-start-date").className = "form-control-error";
		if (!contains(errorMsgs, "Make sure you set start date!")) {
			errorMsgs.push("Make sure you set start date!");
		}
	} else {
		document.getElementById("special-start-date").className = "form-control";
		index = errorMsgs.indexOf("Make sure you set start date!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
	}

	if (startTime != undefined && startTime != null && startTime != "") {
		var start = new Date(startDate);
		var hr = startTime.substring(0, startTime.indexOf(":"));
		var min = startTime.substring(startTime.indexOf(":")+1);

		try {
			hr = parseInt(hr);
			min = parseInt(min);

			var now = newDate();
			start.setHours(hr);
			start.setMinutes(min);

			if(start < now) {
				document.getElementById("special-start-date").className = "form-control-error";
				document.getElementById("special-start-time").className = "form-control-error";
				if (!contains(errorMsgs, "Start time must be after the current time!")) {
					errorMsgs.push("Start time must be after the current time!");
				}
			} else {
				document.getElementById("special-start-date").className = "form-control";
				document.getElementById("special-start-time").className = "form-control";
				index = errorMsgs.indexOf("Start time must be after the current time!");
				if (index >= 0) {
					errorMsgs.splice(index, 1);
				}
			}
		} catch(e) {
			document.getElementById("special-start-time").className = "form-control-error";
			if (!contains(errorMsgs, "Make sure you enter a valid start time!")) {
				errorMsgs.push("Make sure you enter a valid start time!");
			}
		}
	}

	getValidateInnerHtml();
	showValidateCreateLabel();
}

function validateCreateNewDealStartTime() {
	startTime = document.getElementById("special-start-time").value;
	startDate = document.getElementById("special-start-date").value;

	if (startTime == undefined || startTime == null || startTime == "") {
		document.getElementById("special-start-time").className = "form-control-error";
		if (!contains(errorMsgs, "Make sure you set start time!")) {
			errorMsgs.push("Make sure you set start time!");
		}
	} else {
		document.getElementById("special-start-time").className = "form-control";
		var index = errorMsgs.indexOf("Make sure you set start time!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
	}

	if (startTime != undefined && startTime != null && startTime != "") {
		var start = new Date(startDate);
		var hr = startTime.substring(0, startTime.indexOf(":"));
		var min = startTime.substring(startTime.indexOf(":")+1);

		try {
			hr = parseInt(hr);
			min = parseInt(min);

			var now = newDate();
			start.setHours(hr);
			start.setMinutes(min);

			if(start < now) {
				document.getElementById("special-start-date").className = "form-control-error";
				document.getElementById("special-start-time").className = "form-control-error";
				if (!contains(errorMsgs, "Start time must be after the current time!")) {
					errorMsgs.push("Start time must be after the current time!");
				}
			} else {
				document.getElementById("special-start-date").className = "form-control";
				document.getElementById("special-start-time").className = "form-control";
				index = errorMsgs.indexOf("Start time must be after the current time!");
				if (index >= 0) {
					errorMsgs.splice(index, 1);
				}
			}
		} catch(e) {
			document.getElementById("special-start-time").className = "form-control-error";
			if (!contains(errorMsgs, "Make sure you enter a valid start time!")) {
				errorMsgs.push("Make sure you enter a valid start time!");
			}
		}
	}

	getValidateInnerHtml();
	showValidateCreateLabel();
}

function validateCreateNewDealType() {
	if (!isDealTypeSelected) {
		document.getElementById("allDealTypes").className = "deal-type-button-frame-error";
		//document.getElementById("drinkToggleButton").className = "btn btn-default form-control-btn-error";
		//document.getElementById("foodToggleButton").className = "btn btn-default form-control-btn-error";
		if (!contains(errorMsgs, "Make sure you select a special type icon!")) {
			errorMsgs.push("Make sure you select a special type icon!");
		}
	} 
	else {
		document.getElementById("allDealTypes").className = "";
		var index = errorMsgs.indexOf("Make sure you select a special type icon!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
	}
}

function validateCreateNewDealDineinTakeout() {
	var dinein = document.getElementById("dinein").value;
	var takeout = document.getElementById("takeout").value;

	if(dinein == "false" && takeout == "false") {
		document.getElementById("takeout-toggle").className = "btn-group btn-group-justified takeout-frame-error";
		if (!contains(errorMsgs, "Make sure you select dine-in and/or takeout!")) {
			errorMsgs.push("Make sure you select dine-in and/or takeout!");
		}
	}
	else {
		document.getElementById("takeout-toggle").className = "btn-group btn-group-justified";
		var index = errorMsgs.indexOf("Make sure you select dine-in and/or takeout!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
	}
}

function validateCreateNewDealFields() {
	if(document.getElementById("showScheduleFields").style.display == "block") {
		validateCreateNewDealStartDate();
		validateCreateNewDealStartTime();
	}
	validateCreateNewDealHour();
	validateCreateNewDealMinute();
	validateCreateNewDealPrice();
	validateCreateNewDealDetails();
	validateCreateNewDealType();
	validateCreateNewDealDineinTakeout();
	return errorMsgs;
}
// VALIDATION END - APPYHOUR EXCLUSIVE


// VALIDATION START - DAILY SPECIAL
function validateCreateDailyDealDay() {
	var isOneDayChecked = false;
	var maxSpecials = false;

	var mon = 0;
	var tue = 0;
	var wed = 0;
	var thu = 0;
	var fri = 0;
	var sat = 0;
	var sun = 0;

	var monrow = false;
	var tuerow = false;
	var wedrow = false;
	var thurow = false;
	var frirow = false;
	var satrow = false;
	var sunrow = false;

	var rows = document.getElementById("dailyDeals").rows;
	for(var r in rows) {
		var row = rows[r];
		if(row.cells && row.cells.length > 0) {
			if(row.cells[0].innerHTML.indexOf("Monday") > -1) {
				monrow = true;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Tuesday") > -1) {
				monrow = false;
				tuerow = true;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Wednesday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = true;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Thursday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = true;
				frirow = false;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Friday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = true;
				satrow = false;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Saturday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = true;
				sunrow = false;
			}
			else if(row.cells[0].innerHTML.indexOf("Sunday") > -1) {
				monrow = false;
				tuerow = false;
				wedrow = false;
				thurow = false;
				frirow = false;
				satrow = false;
				sunrow = true;
			}
			else if(row.cells[0].innerHTML.indexOf("No specials") == -1) {
				if(monrow) {
					mon += 1;
				}
				else if(tuerow) {
					tue += 1;
				}
				else if(wedrow) {
					wed += 1;
				}
				else if(thurow) {
					thu += 1;
				}
				else if(frirow) {
					fri += 1;
				}
				else if(satrow) {
					sat += 1;
				}
				else if(sunrow) {
					sun += 1;
				}
			}
		}
	}	

	var weeklySpecialsLimit = parseInt(document.getElementById("weeklySpecialsLimit").value);
	
	if(document.getElementById("daily-day-mon").value == "true" && mon < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("daily-day-mon").value = true;
	} 
	else if(document.getElementById("daily-day-mon").value == "true" && mon >= weeklySpecialsLimit) {
		maxSpecials = true;
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		if(weeklySpecialsLimit != 1)
			document.getElementById("validateDailyDealsAdd").innerHTML += "Monday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.<br/>";
		else
			document.getElementById("validateDailyDealsAdd").innerHTML += "Monday already has "+weeklySpecialsLimit+" special. Please delete another special if you would like to add this one.<br/>";
	}

	if(document.getElementById("daily-day-tue").value == "true" && tue < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("daily-day-tue").value = true;
	} 
	else if(document.getElementById("daily-day-tue").value == "true" && tue >= weeklySpecialsLimit) {
		maxSpecials = true;
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		if(weeklySpecialsLimit != 1)
			document.getElementById("validateDailyDealsAdd").innerHTML += "Tuesday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.<br/>";
		else
			document.getElementById("validateDailyDealsAdd").innerHTML += "Tuesday already has "+weeklySpecialsLimit+" special. Please delete another special if you would like to add this one.<br/>";
	}

	if(document.getElementById("daily-day-wed").value == "true" && wed < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("daily-day-wed").value = true;
	} 
	else if(document.getElementById("daily-day-wed").value == "true" && wed >= weeklySpecialsLimit) {
		maxSpecials = true;
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		if(weeklySpecialsLimit != 1)
			document.getElementById("validateDailyDealsAdd").innerHTML += "Wednesday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.<br/>";
		else
			document.getElementById("validateDailyDealsAdd").innerHTML += "Wednesday already has "+weeklySpecialsLimit+" special. Please delete another special if you would like to add this one.<br/>";
	}

	if(document.getElementById("daily-day-thu").value == "true" && thu < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("daily-day-thu").value = true;
	} 
	else if(document.getElementById("daily-day-thu").value == "true" && thu >= weeklySpecialsLimit) {
		maxSpecials = true;
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		if(weeklySpecialsLimit != 1)
			document.getElementById("validateDailyDealsAdd").innerHTML += "Thursday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.<br/>";
		else
			document.getElementById("validateDailyDealsAdd").innerHTML += "Thursday already has "+weeklySpecialsLimit+" special. Please delete another special if you would like to add this one.<br/>";
	}

	if(document.getElementById("daily-day-fri").value == "true" && fri < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("daily-day-fri").value = true;
	} 
	else if(document.getElementById("daily-day-fri").value == "true" && fri >= weeklySpecialsLimit) {
		maxSpecials = true;
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		if(weeklySpecialsLimit != 1)
			document.getElementById("validateDailyDealsAdd").innerHTML += "Friday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.<br/>";
		else
			document.getElementById("validateDailyDealsAdd").innerHTML += "Friday already has "+weeklySpecialsLimit+" special. Please delete another special if you would like to add this one.<br/>";
	}

	if(document.getElementById("daily-day-sat").value == "true" && sat < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("daily-day-sat").value = true;
	} 
	else if(document.getElementById("daily-day-sat").value == "true" && sat >= weeklySpecialsLimit) {
		maxSpecials = true;
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		if(weeklySpecialsLimit != 1)
			document.getElementById("validateDailyDealsAdd").innerHTML += "Saturday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.<br/>";
		else
			document.getElementById("validateDailyDealsAdd").innerHTML += "Saturday already has "+weeklySpecialsLimit+" special. Please delete another special if you would like to add this one.<br/>";
	}

	if(document.getElementById("daily-day-sun").value == "true" && sun < weeklySpecialsLimit){
		isOneDayChecked = true;
		document.getElementById("daily-day-sun").value = true;
	} 
	else if(document.getElementById("daily-day-sun").value == "true" && sun >= weeklySpecialsLimit) {
		maxSpecials = true;
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		if(weeklySpecialsLimit != 1)
			document.getElementById("validateDailyDealsAdd").innerHTML += "Sunday already has "+weeklySpecialsLimit+" specials. Please delete another special if you would like to add this one.<br/>";
		else
			document.getElementById("validateDailyDealsAdd").innerHTML += "Sunday already has "+weeklySpecialsLimit+" special. Please delete another special if you would like to add this one.<br/>";
	}

	if(maxSpecials) {
		return false;
	}

	if(isOneDayChecked){
		return true;
	} else {
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		document.getElementById("validateDailyDealsAdd").innerHTML += "Please select the day(s) you would like this special to occur on!<br/>";
		return false;
	}
}

function validateCreateDailyDealPrice() {
	var dailyPrice = document.getElementById("daily-price").value;

	if (dailyPriceType == "$") {
		if (dailyPrice == undefined || dailyPrice == "") {
			//document.getElementById("daily-price").className = "form-control-error";
			document.getElementById("validateDailyDealsAdd").style.display = "block";
			document.getElementById("validateDailyDealsAdd").innerHTML += "Make sure you add a price!<br/>";
			return false;
		} else if(parseFloat(dailyPrice) > 100) {
			//document.getElementById("daily-price").className = "form-control-error";
			document.getElementById("validateDailyDealsAdd").style.display = "block";
			document.getElementById("validateDailyDealsAdd").innerHTML += "Price needs to be less than $100!<br/>";
			return false;
		} 
	}
	else if (priceType == "%") {
		if (dailyPrice == undefined || dailyPrice == "") {
			//document.getElementById("daily-price").className = "form-control-error";
			document.getElementById("validateDailyDealsAdd").style.display = "block";
			document.getElementById("validateDailyDealsAdd").innerHTML += "Make sure you add a percent off!<br/>";
			return false;
		} else if(parseFloat(dailyPrice) > 100) {
			//document.getElementById("daily-price").className = "form-control-error";
			document.getElementById("validateDailyDealsAdd").style.display = "block";
			document.getElementById("validateDailyDealsAdd").innerHTML += "Percent off needs to be less than $100!<br/>";
			return false;
		} 
	}

	return true;
}

function validateCreateDailyDealDetails() {
	var dailyDetails = document.getElementById("daily-details").value;

	if (dailyDetails == undefined || dailyDetails == "") {
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		document.getElementById("validateDailyDealsAdd").innerHTML += "Make sure you add some details!<br/>";
		return false;
	} 

	return true;
}

function validateCreateDailyDealStart() {
	var dailyStart = document.getElementById("daily-start").value;

	if (dailyStart == undefined || dailyStart == "") {
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		document.getElementById("validateDailyDealsAdd").innerHTML += "Make sure you add a start time!<br/>";
		return false;
	} 

	return true;
}

function validateCreateDailyDealEnd() {
	var dailyEnd = document.getElementById("daily-end").value;

	if (dailyEnd == undefined || dailyEnd == "") {
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		document.getElementById("validateDailyDealsAdd").innerHTML += "Make sure you add an end time!<br/>";
		return false;
	} 

	return true;
}

function validateCreateDailyDealType() {
	if (!isDailyDealTypeSelected) {
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		document.getElementById("validateDailyDealsAdd").innerHTML += "Make sure you select a deal type icon!<br/>";
		return false;
	} 

	return true;
}

function validateCreateDailyDealDineinTakeout() {
	var dinein = document.getElementById("daily-dinein").value;
	var takeout = document.getElementById("daily-takeout").value;

	if(dinein == "false" && takeout == "false") {
		document.getElementById("validateDailyDealsAdd").style.display = "block";
		document.getElementById("validateDailyDealsAdd").innerHTML += "Make sure you select dine-in and/or takeout!<br/>";
		return false;
	}

	return true;
}

function validateAddDailyDealsBeforeSubmit() {
	document.getElementById("validateDailyDealsAdd").innerHTML = "<br />";

	var validDay = validateCreateDailyDealDay();
	var validPrice = validateCreateDailyDealPrice();
	var validDetails = validateCreateDailyDealDetails();
	var validStart = validateCreateDailyDealStart();
	var validEnd = validateCreateDailyDealEnd();
	var validType = validateCreateDailyDealType();
	var validDineinTakeout = validateCreateDailyDealDineinTakeout();

	if (validDay && validPrice && validDetails && validStart && validEnd && validType && validDineinTakeout)
		document.getElementById("addDealForm").submit();
}
// VALIDATION END - DAILY SPECIALS

function validateField(field) {
	if (field.id == "price" || field.id.toString().indexOf("daily-price") >= 0) {
		if (priceType == "$") {
			if (field.value.trim() != "") {
				var periods = countNumberOfPeriods(field.value);
				if (periods == field.value.length) {
					field.value = "";
				}
				else if (periods == 1) {
					field.value = parseFloat(field.value).toFixed(2);
					if (field.value.indexOf(".00") > 0)
						field.value = field.value.replace(".00", "");
				}
				else if (periods > 1) {
					var firstPeriod = true;
					var i = 0;

					while(i < field.value.length) {
						if (field.value.charAt(i) == "." && !firstPeriod) {
							field.value = field.value.removeAt(i);
							i--;
						} 
						else if (field.value.charAt(i) == "." && firstPeriod) {
							firstPeriod = false;
						}
						i++;
					}
					field.value = parseFloat(field.value).toFixed(2);
					if (field.value.indexOf(".00") > 0)
						field.value = field.value.replace(".00", "");
				}
	    	}
    	}
    	/*
    	else if (priceType == "%") {
    		if (field.value.trim() != "") {
    			if (parseInt(field.value) > 100) {
    				field.value = "100";
    			}
    		}
    	}
    	*/
    }
    else if (field.id.toString().indexOf("daily-start") >= 0) {
    	if (field.value != "") {
    		var h = parseInt(field.value.substring(0, field.value.indexOf(":")));
    		if (h >= 2 && h < 5) {
    			//invalidField(field); // has to start before 2:00 AM or after 5:00 AM
    			return;
    		}
    	} else {
    		//invalidField(field); // field can't be blank
    		return;
    	}
    	//validField(field);
    }
    else if (field.id.toString().indexOf("daily-end") >= 0) {
    	//if (field.value != "") {
    		
    	//}
    	//validField(field);
    }
}




function showCreateInstantSpecialFields(e) {
	e.preventDefault();
	document.getElementById("btnCreateSpecialDiv").style.display = "none";
	document.getElementById("showCreateSpecialFields").style.display = "block";
}

function createSpecialCancel(e) {
	e.preventDefault();
	document.getElementById("btnCreateSpecialDiv").style.display = "block";
	document.getElementById("showCreateSpecialFields").style.display = "none";
}

function toggleStartsToday(specialStart, sr, srNW, srTWO) {
	if(specialStart == "now") {
		document.getElementById("noSpecialsRemainingSchedule").style.display = "none";

		if(sr > 0) {
			document.getElementById("showSharedFields").style.display = "block";
			document.getElementById("btnCancelCreateSpecial").style.display = "none";
			document.getElementById("noSpecialsRemaining").style.display = "none";
		} else {
			document.getElementById("showSharedFields").style.display = "none";
			document.getElementById("btnCancelCreateSpecial").style.display = "inline-block";
			document.getElementById("noSpecialsRemaining").style.display = "block";
		}

		document.getElementById("btnStartsToday").className = "btn btn-primary";
		document.getElementById("showScheduleFields").style.display = "none";
		document.getElementById("btnSchedule").className = "btn btn-default";

		var index = errorMsgs.indexOf("Make sure you set start date!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
		index = errorMsgs.indexOf("Make sure you set start time!");
		if (index >= 0) {
			errorMsgs.splice(index, 1);
		}
		getValidateInnerHtml();
		showValidateCreateLabel();
	} else {
		document.getElementById("noSpecialsRemaining").style.display = "none";

		if(srNW > 0 || srTWO > 0) {
			document.getElementById("showSharedFields").style.display = "block";
			document.getElementById("btnCancelCreateSpecial").style.display = "none";
			document.getElementById("noSpecialsRemainingSchedule").style.display = "none";
		} else {
			document.getElementById("showSharedFields").style.display = "none";
			document.getElementById("btnCancelCreateSpecial").style.display = "inline-block";
			document.getElementById("noSpecialsRemainingSchedule").style.display = "block";
		}

		setSpecialStartDates(sr, srNW, srTWO);
		document.getElementById("btnStartsToday").className = "btn btn-default";
		document.getElementById("showScheduleFields").style.display = "block";
		document.getElementById("btnSchedule").className = "btn btn-primary";

		if(document.getElementById("special-start-date").className == "form-control-error") {
			if (!contains(errorMsgs, "Make sure you set start date!")) {
				errorMsgs.push("Make sure you set start date!");
			}
		}
		if(document.getElementById("special-start-time").className == "form-control-error") {
			if (!contains(errorMsgs, "Make sure you set start time!")) {
				errorMsgs.push("Make sure you set start time!");
			}
		}
	}
}

function setSpecialStartDates(sr, srNW, srTWO) {
	if(document.getElementById("special-start-date").length == 0) {
		var options = "";
		options += "<option disabled selected>Select Date</option>";
		
		var day = newDate();
		var dayStr = intDayToStringAbbr(day.getDay()) + " " + intMonthToStringAbbr(day.getMonth()) + " " + day.getDate() + ", " + (1900+day.getYear());
		if(sr > 0)
			options += "<option value='" + day + "'>" + dayStr + "</option>";
		else
			options += "<option disabled value='" + day + "'>" + dayStr + "</option>";

		var days = 14;
		if(day.getDay() > 0) {
			days += (8 - day.getDay());
		}

		for(var i = 1; i < days; i++) {
			day.setDate(day.getDate()+1);
			dayStr = intDayToStringAbbr(day.getDay()) + " " + intMonthToStringAbbr(day.getMonth()) + " " + day.getDate() + ", " + (1900+day.getYear());
			if((sr > 0 && isDayInWeek(day, 0)) || (srNW > 0 && isDayInWeek(day, 1)) || (srTWO > 0 && isDayInWeek(day, 2)))
				options += "<option value='" + day + "'>" + dayStr + "</option>";
			else
				options += "<option disabled value='" + day + "'>" + dayStr + "</option>";
		}

		$("select[id='special-start-date']").find('option').remove().end().append($(options));
	}
}

function isDayInWeek(day, week) {
	var addDays = 1;
	if(week == 0)
		addDays = 1;
	else if(week == 1)
		addDays = 8;
	else
		addDays = 15;

	var curr = newDate(); 
	curr.setHours(0,0,0,0);
	var first = 0;
	if(curr.getDay() != 0)
		first = curr.getDate() - curr.getDay() + addDays; 
	else
		first = curr.getDate() - 7 + addDays; 
	var last = first + 7;
	var firstday = new Date(curr.setDate(first));
	var lastday = new Date(curr.setDate(last));

	/*
	if(lastday.getDate() < firstday.getDate() && lastday.getMonth() < 11) 
		lastday.setMonth(lastday.getMonth()+1);
	else if(lastday.getDate() < firstday.getDate() && lastday.getMonth() == 11)
		lastday.setMonth(0);
	*/

	if(day > firstday && day < lastday) {
		return true;
	}

	return false;
}

function intDayToString(day) {
	var weekday = new Array(7);
	weekday[0] = "Sunday";
	weekday[1] = "Monday";
	weekday[2] = "Tuesday";
	weekday[3] = "Wednesday";
	weekday[4] = "Thursday";
	weekday[5] = "Friday";
	weekday[6] = "Saturday";
	return weekday[day];
}

function intDayToStringAbbr(day) {
	var weekday = new Array(7);
	weekday[0] = "Sun";
	weekday[1] = "Mon";
	weekday[2] = "Tue";
	weekday[3] = "Wed";
	weekday[4] = "Thu";
	weekday[5] = "Fri";
	weekday[6] = "Sat";
	return weekday[day];
}

function intMonthToStringAbbr(month) {
	var m = new Array(7);
	m[0] = "Jan";
	m[1] = "Feb";
	m[2] = "Mar";
	m[3] = "Apr";
	m[4] = "May";
	m[5] = "Jun";
	m[6] = "Jul";
	m[7] = "Aug";
	m[8] = "Sep";
	m[9] = "Oct";
	m[10] = "Nov";
	m[11] = "Dec";
	return m[month];
}

function formatStartTime(startTime) {
	var hr = startTime.substring(0, startTime.indexOf(":"));
	var min = startTime.substring(startTime.indexOf(":")+1);
	var ampm = "AM";

	if(hr == 12) {
		ampm = "PM";
	}
	else if(hr == 24) {
		hr = hr - 12;
	}
	else if(hr > 12) {	
		hr = hr - 12;
		ampm = "PM";
	}

	return hr + ":" + min + " " + ampm;
}

function formatStartDate(startDate) {
	startDate = new Date(startDate);
	return intDayToStringAbbr(startDate.getDay()) + " " + intMonthToStringAbbr(startDate.getMonth()) + " " + startDate.getDate() + ", " + (1900+startDate.getYear());
}

