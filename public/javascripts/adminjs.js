var adminPriceType = "$";
var adminPriceTypeId = 300;
var adminDailyPriceType = "$";
var adminDailyPriceTypeId = 300;

var adminAddEventSpecialPriceType = "$";
var adminAddEventSpecialPriceTypeId = 300;
var adminAddEventSpecialDealTypeId = 0;
var isAdminAddEventSpecialDealTypeSelected = false;

if(window.location.href.indexOf("/auth") > -1)
    window.history.pushState("", "", '/admin');
else if(window.location.href.indexOf("/saveimportests") > -1)
    window.history.pushState("", "", '/importests');
else if(document.getElementById("admin-import-ests-city"))
    window.history.pushState("", "", '/importests?city='+document.getElementById("admin-import-ests-city").value);

$(function() {
  // owner dropdown
  var ownerHidden = document.getElementById("ownerHidden");
  if (ownerHidden) {
    document.getElementById("owner").value = ownerHidden.value;
    if(ownerHidden.value != "") {
      document.getElementById("btnDeleteOwner").style.display = "inline";
    }
    else {
      document.getElementById("btnDeleteOwner").style.display = "none";
    }
  }

  // establishment dropdown
  var estHidden = document.getElementById("estHidden");
  if (estHidden) {
    document.getElementById("est").value = estHidden.value;
    if (window.location.pathname != "/adminanalytics") {
      if (estHidden.value != "") {
        document.getElementById("btnDelete").style.display = "inline";
        document.getElementById("dealDiv").style.display = "block";
        document.getElementById("eventsDiv").style.display = "block";
      }
      else {
        document.getElementById("btnDelete").style.display = "none";
        if(document.getElementById("dealDiv"))
          document.getElementById("dealDiv").style.display = "none";
        if(document.getElementById("eventsDiv"))
          document.getElementById("eventsDiv").style.display = "none";
      }
    }
  }

  // topic dropdown
  var topicHidden = document.getElementById("topicHidden");
  if (topicHidden)
    document.getElementById("topic").value = topicHidden.value;

  // needsToPay dropdown
  var needsToPayHidden = document.getElementById("needsToPayHidden");
  if (needsToPayHidden) {
    if(needsToPayHidden.value != "")
      document.getElementById("needsToPay").value = needsToPayHidden.value;
    else
      document.getElementById("needsToPay").value = 0;
  }

  // showInApp dropdown
  var showInAppHidden = document.getElementById("showInAppHidden");
  if (showInAppHidden) {
    if(showInAppHidden.value != "")
      document.getElementById("showInApp").value = showInAppHidden.value;
    else
      document.getElementById("showInApp").value = 1;
  }

  var adminDeliveryHidden = document.getElementById("adminDeliveryHidden");
  if(adminDeliveryHidden) {
    if(adminDeliveryHidden.value != "")
      document.getElementById("adminDelivery").value = adminDeliveryHidden.value;
    else
      document.getElementById("adminDelivery").value = 0;
  }

  var adminTakeoutHidden = document.getElementById("adminTakeoutHidden");
  if(adminTakeoutHidden) {
    if(adminTakeoutHidden.value != "")
      document.getElementById("adminTakeout").value = adminTakeoutHidden.value;
    else
      document.getElementById("adminTakeout").value = 0;
  }

  var adminPatioHidden = document.getElementById("adminPatioHidden");
  if(adminPatioHidden) {
    if(adminPatioHidden.value != "")
      document.getElementById("adminPatio").value = adminPatioHidden.value;
    else
      document.getElementById("adminPatio").value = 0;
  }

  var adminRooftopHidden = document.getElementById("adminRooftopHidden");
  if(adminRooftopHidden) {
    if(adminRooftopHidden.value != "")
      document.getElementById("adminRooftop").value = adminRooftopHidden.value;
    else
      document.getElementById("adminRooftop").value = 0;
  }

  var adminBrunchHidden = document.getElementById("adminBrunchHidden");
  if(adminBrunchHidden) {
    if(adminBrunchHidden.value != "")
      document.getElementById("adminBrunch").value = adminBrunchHidden.value;
    else
      document.getElementById("adminBrunch").value = 0;
  }

  var adminDogHidden = document.getElementById("adminDogHidden");
  if(adminDogHidden) {
    if(adminDogHidden.value != "")
      document.getElementById("adminDog").value = adminDogHidden.value;
    else
      document.getElementById("adminDog").value = 0;
  }


  var labels = [];
  var dataArr = [];

  var pressTable = document.getElementById("pressTable");
  if(pressTable != null) {
    var rows = pressTable.rows;
    for(var r = rows.length-1; r >= 0; r--) {
      var cells = rows[r].getElementsByTagName("td");
      if(cells.length == 2) {
        labels.push(cells[0].innerHTML);
        dataArr.push(cells[1].innerHTML);
      }
    }
  }

  if(document.getElementById("pressChart") != null) {
    var ctx = document.getElementById('pressChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                data: dataArr
            }]
        },

        // Configuration options go here
        options: {
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Presses";
                        else
                            return tooltipItem.yLabel + " Press";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
  }
});

$('#copyToAll').on('click', function(e) {
  e.preventDefault();
  var open = document.getElementById("monOpen").value;
  var close = document.getElementById("monClose").value;
  document.getElementById("tueOpen").value = open;
  document.getElementById("tueClose").value = close;
  document.getElementById("wedOpen").value = open;
  document.getElementById("wedClose").value = close;
  document.getElementById("thuOpen").value = open;
  document.getElementById("thuClose").value = close;
  document.getElementById("friOpen").value = open;
  document.getElementById("friClose").value = close;
  document.getElementById("satOpen").value = open;
  document.getElementById("satClose").value = close;
  document.getElementById("sunOpen").value = open;
  document.getElementById("sunClose").value = close;
});

$('#btnDelete').on('click', function(e) {
  e.preventDefault();
  if (confirm("Are you sure you want to delete this establishment?")) {
    $("#est-info-form").attr("action", "/deleteest");
    $("#est-info-form").submit();
  }
});

$('#cancelAddSpecial').on('click', function(e) {
  e.preventDefault();
});

$('#cancelAddDailySpecial').on('click', function(e) {
  e.preventDefault();
});




/* IMPORT ESTABLISHMENTS */

var map;

function loadMap() {
  var myStyles = [
    {
      featureType: "poi",
      elementType: "labels",
      stylers: [
        { visibility: "off" }
      ]
    }
  ];

  var zip = document.getElementById("admin-import-ests-zip").value;

  if(zip != "") {
    fetch("https://maps.googleapis.com/maps/api/geocode/json?address="+zip+"&region=us&key=AIzaSyA8ObhYNLNSbaaHrF1B7dC1a10t9iF50II")
    .then(response => response.json())
    .then(data => {
      var lat = data.results[0].geometry.location.lat;
      var lng = data.results[0].geometry.location.lng;

      map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: lat, lng: lng },
        zoom: 12,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: myStyles
      });

      // AH Establishment markers
      var ahEstNames = document.getElementsByClassName("ahEstName");
      for(var n in ahEstNames) {
        if(ahEstNames[n].tagName == "INPUT") {
          var id = ahEstNames[n].id.replace("ahEstNameHidden", "");
          var ahZip = document.getElementById("ahEstZipHidden"+id).value;

          if(ahZip == zip) {
            var lat = document.getElementById("ahEstLatHidden"+id).value;
            var lng = document.getElementById("ahEstLngHidden"+id).value;
            var latlng = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
              position: latlng,
              map,
              title: ahEstNames[n].value,
              icon: "/assets/img/orangepin.png"
            });

            var address1 = document.getElementById("ahEstAddress1Hidden"+id).value;
            if(document.getElementById("ahEstAddress2Hidden"+id).value != "")
              address1 += " " + document.getElementById("ahEstAddress2Hidden"+id).value;
            
            var address2 = document.getElementById("ahEstCityHidden"+id).value;
            address2 += ", " + document.getElementById("ahEstStateHidden"+id).value;
            address2 += " " + ahZip;

            const contentString =
            '<div>' +
              '<h4 style="font-size:14px; font-weight:600; color:black; margin-bottom:5px;">'+ahEstNames[n].value+'</h4>' +
              '<div>' +
                '<p style="font-size:12px;">'+address1+'</p>' +
                '<p style="font-size:12px;">'+address2+'</p>' +
              '</div>'+
            '</div>';

            const infowindow = new google.maps.InfoWindow({
              content: contentString,
              maxWidth: 200,
            });

            google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
              return function() {
                var allWindows = document.getElementsByClassName("gm-style-iw-a");
                //console.log(allWindows);
                for(var a in allWindows) {
                  if(allWindows[a].tagName == "DIV") {
                    allWindows[a].style.display = "none";
                  }
                }
                infowindow.open(map,marker);
              };
            })(marker,infowindow)); 
          }
        }
      }

      // Establishment Queue markers
      var queueEstNames = document.getElementsByClassName("queueEstName");
      for(var n in queueEstNames) {
        if(queueEstNames[n].tagName == "INPUT") {
          var id = queueEstNames[n].id.replace("estNameHidden", "");
          var status = document.getElementById("estStatusHidden"+id).value;

          if(status != "Will Not Insert" && status != "Already in AH" && status != "Added to AH") {
            var lat = document.getElementById("estLatHidden"+id).value;
            var lng = document.getElementById("estLngHidden"+id).value;
            var latlng = new google.maps.LatLng(lat, lng);

            var marker = new google.maps.Marker({
              position: latlng,
              map,
              title: queueEstNames[n].value,
              icon: status == "Reviewed" ? "/assets/img/greenpin.png" : "/assets/img/greypin.png"
            });

            var address1 = document.getElementById("estAddress1Hidden"+id).value;
            if(document.getElementById("estAddress2Hidden"+id).value != "")
              address1 += " " + document.getElementById("estAddress2Hidden"+id).value;
            
            var address2 = document.getElementById("estCityHidden"+id).value;
            address2 += ", " + document.getElementById("estStateHidden"+id).value;
            address2 += " " + document.getElementById("estZipHidden"+id).value;

            const contentString =
            '<div>' +
              '<h4 style="font-size:14px; font-weight:600; color:black; margin-bottom:5px;">'+queueEstNames[n].value+'</h4>' +
              '<div>' +
                '<p style="font-size:12px;">'+address1+'</p>' +
                '<p style="font-size:12px;">'+address2+'</p>' +
              '</div>'+
            '</div>';

            const infowindow = new google.maps.InfoWindow({
              content: contentString,
              maxWidth: 200,
            });

            google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
              return function() {
                var allWindows = document.getElementsByClassName("gm-style-iw-a");
                //console.log(allWindows);
                for(var a in allWindows) {
                  if(allWindows[a].tagName == "DIV") {
                    allWindows[a].style.display = "none";
                  }
                }
                infowindow.open(map,marker);
              };
            })(marker,infowindow)); 
          }
        }
      }
    });
  }
  else {
    map = new google.maps.Map(document.getElementById("map"), {
      center: { lat: 41.6611, lng: -91.5302 },
      zoom: 4,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      styles: myStyles
    });

    // AH Establishment markers
    var ahEstNames = document.getElementsByClassName("ahEstName");
    for(var n in ahEstNames) {
      if(ahEstNames[n].tagName == "INPUT") {
        var id = ahEstNames[n].id.replace("ahEstNameHidden", "");

        var lat = document.getElementById("ahEstLatHidden"+id).value;
        var lng = document.getElementById("ahEstLngHidden"+id).value;
        var latlng = new google.maps.LatLng(lat, lng);

        var marker = new google.maps.Marker({
          position: latlng,
          map,
          title: ahEstNames[n].value,
          icon: "/assets/img/orangepin.png"
        });

        var address1 = document.getElementById("ahEstAddress1Hidden"+id).value;
        if(document.getElementById("ahEstAddress2Hidden"+id).value != "")
          address1 += " " + document.getElementById("ahEstAddress2Hidden"+id).value;
        
        var address2 = document.getElementById("ahEstCityHidden"+id).value;
        address2 += ", " + document.getElementById("ahEstStateHidden"+id).value;
        address2 += " " + document.getElementById("ahEstZipHidden"+id).value;

        const contentString =
        '<div>' +
          '<h4 style="font-size:14px; font-weight:600; color:black; margin-bottom:5px;">'+ahEstNames[n].value+'</h4>' +
          '<div>' +
            '<p style="font-size:12px;">'+address1+'</p>' +
            '<p style="font-size:12px;">'+address2+'</p>' +
          '</div>'+
        '</div>';

        const infowindow = new google.maps.InfoWindow({
          content: contentString,
          maxWidth: 200,
        });

        google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
          return function() {
            var allWindows = document.getElementsByClassName("gm-style-iw-a");
            //console.log(allWindows);
            for(var a in allWindows) {
              if(allWindows[a].tagName == "DIV") {
                allWindows[a].style.display = "none";
              }
            }
            infowindow.open(map,marker);
          };
        })(marker,infowindow)); 
      }
    }

    // Establishment Queue markers
    var queueEstNames = document.getElementsByClassName("queueEstName");
    for(var n in queueEstNames) {
      if(queueEstNames[n].tagName == "INPUT") {
        var id = queueEstNames[n].id.replace("estNameHidden", "");
        var status = document.getElementById("estStatusHidden"+id).value;

        if(status != "Will Not Insert" && status != "Already in AH" && status != "Added to AH") {
          var lat = document.getElementById("estLatHidden"+id).value;
          var lng = document.getElementById("estLngHidden"+id).value;
          var latlng = new google.maps.LatLng(lat, lng);

          var marker = new google.maps.Marker({
            position: latlng,
            map,
            title: queueEstNames[n].value,
            icon: status == "Reviewed" ? "/assets/img/greenpin.png" : "/assets/img/greypin.png"
          });

          var address1 = document.getElementById("estAddress1Hidden"+id).value;
          if(document.getElementById("estAddress2Hidden"+id).value != "")
            address1 += " " + document.getElementById("estAddress2Hidden"+id).value;
          
          var address2 = document.getElementById("estCityHidden"+id).value;
          address2 += ", " + document.getElementById("estStateHidden"+id).value;
          address2 += " " + document.getElementById("estZipHidden"+id).value;

          const contentString =
          '<div>' +
            '<h4 style="font-size:14px; font-weight:600; color:black; margin-bottom:5px;">'+queueEstNames[n].value+'</h4>' +
            '<div>' +
              '<p style="font-size:12px;">'+address1+'</p>' +
              '<p style="font-size:12px;">'+address2+'</p>' +
            '</div>'+
          '</div>';

          const infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 200,
          });

          google.maps.event.addListener(marker,'click', (function(marker,infowindow){ 
            return function() {
              var allWindows = document.getElementsByClassName("gm-style-iw-a");
              //console.log(allWindows);
              for(var a in allWindows) {
                if(allWindows[a].tagName == "DIV") {
                  allWindows[a].style.display = "none";
                }
              }
              infowindow.open(map,marker);
            };
          })(marker,infowindow)); 
        }
      }
    }
  }
}

var circle1, circle2, circle3 , circle4, circle5;
var p1, p2, p3, p4, p5;
var radius;

function adminToggleCircleSize(e, size) {
  e.preventDefault();

  /*if(circle1 != undefined)
    circle1.setMap(null);

  if(circle2 != undefined)
    circle2.setMap(null);

  if(circle3 != undefined)
    circle3.setMap(null);

  if(circle4 != undefined)
    circle4.setMap(null);

  if(circle5 != undefined)
    circle5.setMap(null);*/

  document.getElementById("admin-circle-0").className = "btn btn-default";
  document.getElementById("admin-circle-1").className = "btn btn-default";
  document.getElementById("admin-circle-2").className = "btn btn-default";
  document.getElementById("admin-circle-"+size).className = "btn btn-primary";

  document.getElementById("btnImportEsts2").disabled = false;

  var zip = document.getElementById("admin-import-ests-zip").value;

  fetch("https://maps.googleapis.com/maps/api/geocode/json?address="+zip+"&region=us&key=AIzaSyA8ObhYNLNSbaaHrF1B7dC1a10t9iF50II")
    .then(response => response.json())
    .then(data => {
      var lat = data.results[0].geometry.location.lat;
      var lng = data.results[0].geometry.location.lng;
      
      if(p1 == undefined) {
        p1 = new google.maps.LatLng(lat, lng);
      }
      else if(circle1 != undefined) {
        p1 = circle1.center;
        circle1.setMap(null);
      }

      if(p2 == undefined) {
        p2 = new google.maps.LatLng(lat+0.02, lng);
      }
      else if(circle2 != undefined) {
        p2 = circle2.center;
        circle2.setMap(null);
      }

      if(p3 == undefined) {
        p3 = new google.maps.LatLng(lat-0.02, lng);
      }
      else if(circle3 != undefined) {
        p3 = circle3.center;
        circle3.setMap(null);
      }

      if(p4 == undefined) {
        p4 = new google.maps.LatLng(lat, lng+0.025);
      }
      else if(circle4 != undefined) {
        p4 = circle4.center;
        circle4.setMap(null);
      }

      if(p5 == undefined) {
        p5 = new google.maps.LatLng(lat, lng-0.025);
      }
      else if(circle5 != undefined) {
        p5 = circle5.center;
        circle5.setMap(null);
      }

      radius = 500;
      if(size == 1)
        radius = 1000;
      else if(size == 2)
        radius = 1500;

      circle1 = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        center: p1,
        radius: radius,
        draggable: true
      });
      circle2 = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        center: p2,
        radius: radius,
        draggable: true
      });
      circle3 = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        center: p3,
        radius: radius,
        draggable: true
      });
      circle4 = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        center: p4,
        radius: radius,
        draggable: true
      });
      circle5 = new google.maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        map,
        center: p5,
        radius: radius,
        draggable: true
      });
    });
}

function adminImportEsts2(e) {
  document.getElementById("btnZipSearch").disabled = true;
  //document.getElementById("btnImportEsts").disabled = true;
  document.getElementById("admin-circle-0").disabled = true;
  document.getElementById("admin-circle-1").disabled = true;
  document.getElementById("admin-circle-2").disabled = true;
  document.getElementById("btnImportEsts2").disabled = true;
  document.getElementById("btnImportEsts2").innerHTML = "Importing...";
  document.body.style.cursor = "wait";

  var zip = document.getElementById("admin-import-ests-zip").value;

  p1 = circle1.center;
  p2 = circle2.center;
  p3 = circle3.center;
  p4 = circle4.center;
  p5 = circle5.center;

  var progress = document.getElementById("admin-search-ests-progress");
  progress.style.width = "20%";
  progress.innerHTML = "Searching...";

  importEstsForLocation(p1, [], zip, function(ests1) {
    //console.log(ests1);
    progress.style.width = "35%";
    //progress.style.width = "45%";
    progress.innerHTML = "Searching for establishments... (" + ests1.length + " found)";
    importEstsForLocation(p2, ests1, zip, function(ests2) {
      //console.log(ests2);
      progress.style.width = "50%";
      //progress.style.width = "70%";
      progress.innerHTML = "Searching for establishments... (" + ests2.length + " found)";
      importEstsForLocation(p3, ests2, zip, function(ests3) {
        //console.log(ests3);
        progress.style.width = "65%";
        progress.innerHTML = "Searching for establishments... (" + ests3.length + " found)";
        importEstsForLocation(p4, ests3, zip, function(ests4) {
          //console.log(ests4);
          progress.style.width = "80%";
          progress.innerHTML = "Searching for establishments... (" + ests4.length + " found)";
          importEstsForLocation(p5, ests4, zip, function(ests) {
            //console.log(ests[0]);
            //console.log(ests);
            progress.style.width = "95%";
            progress.innerHTML = "Importing " + ests.length + " establishments...";

            try {
              var formattedEsts = [];

              for(var e in ests) {
                var tempEst = {};

                // ESTABLISHMENT

                tempEst.google_id = ests[e].place_id;
                tempEst.status = ests[e].business_status;
                tempEst.name = ests[e].name;
                tempEst.phone_num = ests[e].formatted_phone_number !== undefined ? ests[e].formatted_phone_number.replace("(", "").replace(")", "").replace(" ", "").replace("-", "") : null;
                tempEst.website = ests[e].website;

                var topic = "";

                if(getDistanceFromLatLng(ests[e].geometry.location.lat(), ests[e].geometry.location.lng(), 44.9778, -93.2650) <= 30) // Minne/STP
                  topic = "minneapolismn";
                else if(getDistanceFromLatLng(ests[e].geometry.location.lat(), ests[e].geometry.location.lng(), 41.6611, -91.5302) <= 30) // Iowa City
                  topic = "iowacityia";
                else if(getDistanceFromLatLng(ests[e].geometry.location.lat(), ests[e].geometry.location.lng(), 41.5868, -93.6250) <= 30) // Des Moines
                  topic = "desmoinesia";
                else if(getDistanceFromLatLng(ests[e].geometry.location.lat(), ests[e].geometry.location.lng(), 39.7392, -104.9903) <= 30) // Denver
                  topic = "denverco";
                else if(getDistanceFromLatLng(ests[e].geometry.location.lat(), ests[e].geometry.location.lng(), 38.2527, -85.7585) <= 30) // Louisville
                  topic = "louisvilleky";
                else if(getDistanceFromLatLng(ests[e].geometry.location.lat(), ests[e].geometry.location.lng(), 27.7634, -82.5437) <= 30) // Tampa
                  topic = "tampabayfl";

                // tempEst.topic = city;
                tempEst.topic = topic;

                // HOURS

                tempEst.sun_open = "";
                tempEst.sun_close = "";
                tempEst.mon_open = "";
                tempEst.mon_close = "";
                tempEst.tue_open = "";
                tempEst.tue_close = "";
                tempEst.wed_open = "";
                tempEst.wed_close = "";
                tempEst.thu_open = "";
                tempEst.thu_close = "";
                tempEst.fri_open = "";
                tempEst.fri_close = "";
                tempEst.sat_open = "";
                tempEst.sat_close = "";

                if(ests[e].opening_hours !== undefined) {
                  var periods = ests[e].opening_hours.periods;
                  for(var p in periods) {
                    if(periods[p].open !== undefined && periods[p].open.day == 0 && periods[p].close !== undefined && periods[p].close.day == 0) {
                      tempEst.sun_open = periods[p].open.time;
                      tempEst.sun_close = periods[p].close.time;
                    }
                    else if(periods[p].open !== undefined && periods[p].open.day == 1 && periods[p].close !== undefined && periods[p].close.day == 1) {
                      tempEst.mon_open = periods[p].open.time;
                      tempEst.mon_close = periods[p].close.time;
                    }
                    else if(periods[p].open !== undefined && periods[p].open.day == 2 && periods[p].close !== undefined && periods[p].close.day == 2) {
                      tempEst.tue_open = periods[p].open.time;
                      tempEst.tue_close = periods[p].close.time;
                    }
                    else if(periods[p].open !== undefined && periods[p].open.day == 3 && periods[p].close !== undefined && periods[p].close.day == 3) {
                      tempEst.wed_open = periods[p].open.time;
                      tempEst.wed_close = periods[p].close.time;
                    }
                    else if(periods[p].open !== undefined && periods[p].open.day == 4 && periods[p].close !== undefined && periods[p].close.day == 4) {
                      tempEst.thu_open = periods[p].open.time;
                      tempEst.thu_close = periods[p].close.time;
                    }
                    else if(periods[p].open !== undefined && periods[p].open.day == 5 && periods[p].close !== undefined && periods[p].close.day == 5) {
                      tempEst.fri_open = periods[p].open.time;
                      tempEst.fri_close = periods[p].close.time;
                    }
                    else if(periods[p].open !== undefined && periods[p].open.day == 6 && periods[p].close !== undefined && periods[p].close.day == 6) {
                      tempEst.sat_open = periods[p].open.time;
                      tempEst.sat_close = periods[p].close.time;
                    }
                  }
                }

                // ADDRESS

                for(var a in ests[e].address_components) {
                  if(ests[e].address_components[a].types[0] == "street_number") {
                    tempEst.address_1 = ests[e].address_components[a].long_name;
                  }
                  else if(ests[e].address_components[a].types[0] == "route") {
                    tempEst.address_1 += " " + ests[e].address_components[a].long_name;
                  }
                  else if(ests[e].address_components[a].types[0] == "subpremise") {
                    tempEst.address_2 = ests[e].address_components[a].long_name;
                  }
                  else if(ests[e].address_components[a].types[0] == "locality") {
                    tempEst.city = ests[e].address_components[a].long_name;
                  }
                  else if(ests[e].address_components[a].types[0] == "administrative_area_level_1") {
                    tempEst.state = ests[e].address_components[a].short_name;
                  }
                  else if(ests[e].address_components[a].types[0] == "postal_code") {
                    tempEst.zip = ests[e].address_components[a].long_name;
                  }
                }

                tempEst.latitude = ests[e].geometry.location.lat();
                tempEst.longitude = ests[e].geometry.location.lng();

                if(tempEst.zip == zip)
                  formattedEsts.push(tempEst);
              }

              document.getElementById("admin-imported-ests").value = JSON.stringify(formattedEsts);

              // Export to Excel
              //var estsWS = XLSX.utils.json_to_sheet(formattedEsts); 

              // Create a new Workbook
              //var wb = XLSX.utils.book_new() 

              // Name your sheet
              //XLSX.utils.book_append_sheet(wb, estsWS, 'Establishments') 

              // export your excel
              //XLSX.writeFile(wb, city+"_establishments.xlsx");  

              document.getElementById("admin-import-ests-form").submit();
            }
            catch(e) {
              //console.log(e);
            }
          });
        });
      });
    });
  });
}

function importEstsForLocation(location, ests, zip, _callback) {
  try {
    if(map === undefined) {
      map = new google.maps.Map(document.getElementById('map'), {
        center: location,
        zoom: 30
      });
    }

    var request = {
      location: location,
      radius: radius,
      type: ['restaurant'],
      //keyword: zip
    };

    setTimeout(function () {
      var service = new google.maps.places.PlacesService(map);
      let getNextPage;
      var count = 0;
      var restaurants = [];

      service.nearbySearch(request, function(results, status, pagination) {
        if (status !== "OK" || !results) {
          //console.log("STATUS: " + status + ", RESULTS: " + results);
        }
        else {
          restaurants = [...restaurants, ...results];
        }

        if (pagination && pagination.hasNextPage) {
          getNextPage = () => {
            pagination.nextPage(); // Note: nextPage will call the same handler function as the initial call
          };
        }

        setTimeout(function () {
          if(getNextPage && ++count < 3) {
            getNextPage();
          }
          else {
            persistImportedEsts(restaurants, ests, zip, 0, function(returnEsts) {
              request = {
                location: location,
                radius: radius,
                type: ['bar'],
                //keyword: zip
              };

              setTimeout(function () {
                service = new google.maps.places.PlacesService(map);
                let getNextPage2;
                count = 0;

                service.nearbySearch(request, function(results2, status2, pagination2) {
                  if (status2 !== "OK" || !results2) {
                    //console.log("STATUS: " + status2 + ", RESULTS: " + results2);
                  }
                  else {  
                    restaurants = [...restaurants, ...results2];
                  }

                  if (pagination2 && pagination2.hasNextPage) {
                    getNextPage2 = () => {
                      pagination2.nextPage(); // Note: nextPage will call the same handler function as the initial call
                    };
                  }

                  setTimeout(function () {
                    if(getNextPage2 && ++count < 3) {
                      getNextPage2();
                    }
                    else {
                      persistImportedEsts(restaurants, ests, zip, 0, function(returnEsts2) {
                        _callback(returnEsts2);
                      });
                    }
                  }, 1000);
                });
              }, 1000);
            });
          }
        }, 1000);
      });
    }, 1000);
  }
  catch(e) {
    console.log("ERROR");
    console.log(e);
  }

  /*
      var service = new google.maps.places.PlacesService(map);
      service.nearbySearch(request, function(results, status) {
        //console.log("STATUS 1: " + status);
        if(status == google.maps.places.PlacesServiceStatus.OK) {
          persistImportedEsts(results, ests, zip, 0, function(returnEsts) {
            request = {
              location: location,
              radius: '1500',
              type: ['bar']
            };

            setTimeout(function () {
              service = new google.maps.places.PlacesService(map);
              service.nearbySearch(request, function(results2, status2) {
                //console.log("STATUS 2: " + status2);
                //console.log(results2);
                //console.log(returnEsts);
                if(status2 == google.maps.places.PlacesServiceStatus.OK) {
                  persistImportedEsts(results2, returnEsts, zip, 0, function(returnEsts2) {
                    _callback(returnEsts2);
                  });
                }
                else {
                  _callback(returnEsts);
                }
              });
            }, 500);
          });
        }
        else {
          request = {
            location: location,
            radius: '1500',
            type: ['bar']
          };

          setTimeout(function () {
            service = new google.maps.places.PlacesService(map);
            service.nearbySearch(request, function(results2, status2) {
              //console.log("STATUS 3: " + status);
              if(status2 == google.maps.places.PlacesServiceStatus.OK) {
                persistImportedEsts(results2, ests, zip, 0, function(returnEsts) {
                  _callback(returnEsts);
                });
              }
              else {
                _callback(ests);
              }
            });
          }, 500);
        }
      });*/
}

function persistImportedEsts(results, ests, zip, start, _callback) {
  if(ests != undefined) {
    if(start < results.length) {
      if(ests.length == 0 || (ests.length != 0 && ests.filter(e => e.place_id === results[start].place_id).length == 0)) {
        request = {
          placeId: results[start].place_id
        };

        setTimeout(function () {
          service = new google.maps.places.PlacesService(map);
          service.getDetails(request, function(details, statusDetails) {
            //console.log("STATUS 4: " + statusDetails);
            if(statusDetails == google.maps.places.PlacesServiceStatus.OK) {
              for(var a in details.address_components) {
                if(details.address_components[a].types[0] == "postal_code" && details.address_components[a].long_name == zip) {
                  ests.push(details);
                  break;
                }
              }
            }

            document.getElementById("admin-search-ests-progress").innerHTML =  "Searching for establishments... (" + ests.length + " found)";

            persistImportedEsts(results, ests, zip, ++start, _callback);
          });
        }, 100);
      }
      else {
        persistImportedEsts(results, ests, zip, ++start, _callback);
      }
    }
    else {
      _callback(ests);
    }
  }
  else {
    _callback([]);
  }
}

function adminImportEstsChangeCity(city) {
  window.location.href = '/importests?city=' + city;
}

function adminReviewEst(estId) {
  $('#review-est-modal').modal({backdrop: 'static', keyboard: false}); 

  clearReviewEstModal();

  var ownerId = document.getElementById("estOwnerIdHidden"+estId).value;

  document.getElementById("review-est-owner").value = ownerId;
  document.getElementById("review-est-owner-username").value = document.getElementById("estOwnerUsernameHidden"+estId).value;
  document.getElementById("review-est-owner-email").value = document.getElementById("estOwnerEmailHidden"+estId).value;
  document.getElementById("review-est-owner-phoneNumber").value = ownerId != "" ? document.getElementById("estOwnerPhoneHidden"+estId).value : document.getElementById("estPhoneHidden"+estId).value;
  document.getElementById("review-est-owner-needsToPay").value = ownerId != "" ? document.getElementById("estOwnerNeedsToPayHidden"+estId).value : 1;
  $("#review-est-owner-phoneNumber").inputmask({"mask": "(999) 999-9999"});

  if(ownerId != "") {
    document.getElementById("review-est-owner").disabled = true;
    document.getElementById("review-est-owner-username").disabled = true;
    document.getElementById("review-est-owner-email").disabled = true;
    document.getElementById("review-est-owner-phoneNumber").disabled = true;
    document.getElementById("review-est-owner-needsToPay").disabled = true;
  }

  document.getElementById("review-est-id").value = estId;
  document.getElementById("review-est-address-id").value = document.getElementById("estAddressIdHidden"+estId).value;
  document.getElementById("review-est-hours-id").value = document.getElementById("estHoursIdHidden"+estId).value;

  document.getElementById("review-est-img-circle").src = document.getElementById("estImgHidden"+estId).value;
  document.getElementById("review-est-name").value = document.getElementById("estNameHidden"+estId).value;
  document.getElementById("review-est-address").value = document.getElementById("estAddress1Hidden"+estId).value;
  document.getElementById("review-est-address2").value = document.getElementById("estAddress2Hidden"+estId).value;
  document.getElementById("review-est-city").value = document.getElementById("estCityHidden"+estId).value;
  document.getElementById("review-est-state").value = document.getElementById("estStateHidden"+estId).value;
  document.getElementById("review-est-zip").value = document.getElementById("estZipHidden"+estId).value;
  document.getElementById("review-est-phone").value = document.getElementById("estPhoneHidden"+estId).value;
  document.getElementById("review-est-website").value = document.getElementById("estWebsiteHidden"+estId).value;
  document.getElementById("review-est-resWebsite").value = document.getElementById("estResWebsiteHidden"+estId).value;
  document.getElementById("review-est-menu-link").value = document.getElementById("estMenuLinkHidden"+estId).value;
  $("#review-est-phone").inputmask({"mask": "(999) 999-9999"});

  document.getElementById("review-est-mon-open").value = formatInputTime(document.getElementById("estMonOpenHidden"+estId).value);
  document.getElementById("review-est-mon-close").value = formatInputTime(document.getElementById("estMonCloseHidden"+estId).value); 
  document.getElementById("review-est-tue-open").value = formatInputTime(document.getElementById("estTueOpenHidden"+estId).value);  
  document.getElementById("review-est-tue-close").value = formatInputTime(document.getElementById("estTueCloseHidden"+estId).value); 
  document.getElementById("review-est-wed-open").value = formatInputTime(document.getElementById("estWedOpenHidden"+estId).value);  
  document.getElementById("review-est-wed-close").value = formatInputTime(document.getElementById("estWedCloseHidden"+estId).value); 
  document.getElementById("review-est-thu-open").value = formatInputTime(document.getElementById("estThuOpenHidden"+estId).value);  
  document.getElementById("review-est-thu-close").value = formatInputTime(document.getElementById("estThuCloseHidden"+estId).value); 
  document.getElementById("review-est-fri-open").value = formatInputTime(document.getElementById("estFriOpenHidden"+estId).value);  
  document.getElementById("review-est-fri-close").value = formatInputTime(document.getElementById("estFriCloseHidden"+estId).value); 
  document.getElementById("review-est-sat-open").value = formatInputTime(document.getElementById("estSatOpenHidden"+estId).value);  
  document.getElementById("review-est-sat-close").value = formatInputTime(document.getElementById("estSatCloseHidden"+estId).value); 
  document.getElementById("review-est-sun-open").value = formatInputTime(document.getElementById("estSunOpenHidden"+estId).value);  
  document.getElementById("review-est-sun-close").value = formatInputTime(document.getElementById("estSunCloseHidden"+estId).value);  

  if(document.getElementById("review-est-mon-open").value == "" && document.getElementById("review-est-mon-close").value == "")
    setReviewEstClosed(null, "review-est-mon");
  if(document.getElementById("review-est-tue-open").value == "" && document.getElementById("review-est-tue-close").value == "")
    setReviewEstClosed(null, "review-est-tue");
  if(document.getElementById("review-est-wed-open").value == "" && document.getElementById("review-est-wed-close").value == "")
    setReviewEstClosed(null, "review-est-wed");
  if(document.getElementById("review-est-thu-open").value == "" && document.getElementById("review-est-thu-close").value == "")
    setReviewEstClosed(null, "review-est-thu");
  if(document.getElementById("review-est-fri-open").value == "" && document.getElementById("review-est-fri-close").value == "")
    setReviewEstClosed(null, "review-est-fri");
  if(document.getElementById("review-est-sat-open").value == "" && document.getElementById("review-est-sat-close").value == "")
    setReviewEstClosed(null, "review-est-sat");
  if(document.getElementById("review-est-sun-open").value == "" && document.getElementById("review-est-sun-close").value == "")
    setReviewEstClosed(null, "review-est-sun");

  document.getElementById("review-est-description").value = document.getElementById("estDescriptionHidden"+estId).value;

  if(document.getElementById("estDeliveryHidden"+estId).value == "1")
    reviewEstFeatureClick(document.getElementById("review-est-deliveryYes"));
  else
    reviewEstFeatureClick(document.getElementById("review-est-deliveryNo"));

  if(document.getElementById("estTakeoutHidden"+estId).value == "1")
    reviewEstFeatureClick(document.getElementById("review-est-takeoutYes"));
  else
    reviewEstFeatureClick(document.getElementById("review-est-takeoutNo"));

  if(document.getElementById("estPatioHidden"+estId).value == "1")
    reviewEstFeatureClick(document.getElementById("review-est-patioYes"));
  else
    reviewEstFeatureClick(document.getElementById("review-est-patioNo"));

  if(document.getElementById("estRooftopHidden"+estId).value == "1")
    reviewEstFeatureClick(document.getElementById("review-est-rooftopYes"));
  else
    reviewEstFeatureClick(document.getElementById("review-est-rooftopNo"));

  if(document.getElementById("estBrunchHidden"+estId).value == "1")
    reviewEstFeatureClick(document.getElementById("review-est-brunchYes"));
  else
    reviewEstFeatureClick(document.getElementById("review-est-brunchNo"));

  if(document.getElementById("estDogHidden"+estId).value == "1")
    reviewEstFeatureClick(document.getElementById("review-est-dogYes"));
  else
    reviewEstFeatureClick(document.getElementById("review-est-dogNo"));
}

function adminValidateReviewEst(e) {
  e.preventDefault();

  document.getElementById("review-est-err").innerHTML = "";
  document.getElementById("review-est-err").style.display = "none";

  var parameters = {
    ownerId:      document.getElementById('review-est-owner').value,
    email:        document.getElementById('review-est-owner-email').value,
    username:     document.getElementById('review-est-owner-username').value,
    phone:        document.getElementById('review-est-owner-phoneNumber').value,
    needsToPay:   document.getElementById('review-est-owner-needsToPay').value,
    estId:        document.getElementById('review-est-id').value, 
    img:          document.getElementById('review-est-img-circle').src != "" ? "true" : "",
    name:         document.getElementById('review-est-name').value,  
    address_id:   document.getElementById('review-est-address-id').value,  
    address_1:    document.getElementById('review-est-address').value,  
    address_2:    document.getElementById('review-est-address2').value,  
    city:         document.getElementById('review-est-city').value,         
    state:        document.getElementById('review-est-state').value,  
    zip:          document.getElementById('review-est-zip').value,  
    phone_num:    document.getElementById('review-est-phone').value,  
    website:      document.getElementById('review-est-website').value,  
    resWebsite:   document.getElementById('review-est-resWebsite').value,  
    menu_link:    document.getElementById('review-est-menu-link').value,  
    hours_id:     document.getElementById('review-est-hours-id').value,  
    mon_open:     document.getElementById('review-est-mon-open').value,  
    mon_close:    document.getElementById('review-est-mon-close').value,  
    tue_open:     document.getElementById('review-est-tue-open').value,  
    tue_close:    document.getElementById('review-est-tue-close').value, 
    wed_open:     document.getElementById('review-est-wed-open').value,  
    wed_close:    document.getElementById('review-est-wed-close').value, 
    thu_open:     document.getElementById('review-est-thu-open').value,  
    thu_close:    document.getElementById('review-est-thu-close').value, 
    fri_open:     document.getElementById('review-est-fri-open').value,  
    fri_close:    document.getElementById('review-est-fri-close').value, 
    sat_open:     document.getElementById('review-est-sat-open').value,  
    sat_close:    document.getElementById('review-est-sat-close').value, 
    sun_open:     document.getElementById('review-est-sun-open').value,  
    sun_close:    document.getElementById('review-est-sun-close').value, 
    description:  document.getElementById('review-est-description').value,
    delivery:     document.getElementById('review-est-delivery').value,
    takeout:      document.getElementById('review-est-takeout').value,
    patio:        document.getElementById('review-est-patio').value,
    rooftop:      document.getElementById('review-est-rooftop').value,
    brunch:       document.getElementById('review-est-brunch').value,
    dog:          document.getElementById('review-est-dog').value
  };

  $.get('/validatereviewest', parameters, function(err) {
    if(err.length == 0) {
      document.getElementById("review-est-form").submit();
    }
    else {
      var errs = "<span style='font-weight:600;'>Error(s):</span><br/>";
      for(var i = 0; i < err.length; i++) {
        errs += err[i] + "<br/>";
      }
      document.getElementById("review-est-err").innerHTML = errs;
      document.getElementById("review-est-err").style.display = "block";
    }
  });
}

function clearReviewEstModal() {
  document.getElementById("review-est-owner").value = "";
  document.getElementById("review-est-owner-username").value = "";
  document.getElementById("review-est-owner-email").value = "";
  document.getElementById("review-est-owner-phoneNumber").value = "";
  document.getElementById("review-est-owner-needsToPay").value = "";

  document.getElementById("review-est-owner").disabled = false;
  document.getElementById("review-est-owner-username").disabled = false;
  document.getElementById("review-est-owner-email").disabled = false;
  document.getElementById("review-est-owner-phoneNumber").disabled = false;
  document.getElementById("review-est-owner-needsToPay").disabled = false;

  document.getElementById("review-est-id").value = "";

  document.getElementById('review-est-img-circle').src = "";
  document.getElementById("review-est-img").value = "";
  document.getElementById("review-est-name").value = "";
  document.getElementById("review-est-address").value = "";
  document.getElementById("review-est-address2").value = "";
  document.getElementById("review-est-city").value = "";
  document.getElementById("review-est-state").value = "";
  document.getElementById("review-est-zip").value = "";
  document.getElementById("review-est-phone").value = "";

  document.getElementById("review-est-mon-open").value = "";
  document.getElementById("review-est-mon-close").value = "";
  document.getElementById("review-est-tue-open").value = ""; 
  document.getElementById("review-est-tue-close").value = "";
  document.getElementById("review-est-wed-open").value = "";  
  document.getElementById("review-est-wed-close").value = "";
  document.getElementById("review-est-thu-open").value = "";
  document.getElementById("review-est-thu-close").value = "";
  document.getElementById("review-est-fri-open").value = "";
  document.getElementById("review-est-fri-close").value = "";
  document.getElementById("review-est-sat-open").value = ""; 
  document.getElementById("review-est-sat-close").value = "";
  document.getElementById("review-est-sun-open").value = "";
  document.getElementById("review-est-sun-close").value = "";

  document.getElementById("review-est-mon").className = "btn btn-light";
  document.getElementById("review-est-mon-open").disabled = false;
  document.getElementById("review-est-mon-close").disabled = false;
  document.getElementById("review-est-tue").className = "btn btn-light";
  document.getElementById("review-est-tue-open").disabled = false;
  document.getElementById("review-est-tue-close").disabled = false;
  document.getElementById("review-est-wed").className = "btn btn-light";
  document.getElementById("review-est-wed-open").disabled = false;
  document.getElementById("review-est-wed-close").disabled = false;
  document.getElementById("review-est-thu").className = "btn btn-light";
  document.getElementById("review-est-thu-open").disabled = false;
  document.getElementById("review-est-thu-close").disabled = false;
  document.getElementById("review-est-fri").className = "btn btn-light";
  document.getElementById("review-est-fri-open").disabled = false;
  document.getElementById("review-est-fri-close").disabled = false;
  document.getElementById("review-est-sat").className = "btn btn-light";
  document.getElementById("review-est-sat-open").disabled = false;
  document.getElementById("review-est-sat-close").disabled = false;
  document.getElementById("review-est-sun").className = "btn btn-light";
  document.getElementById("review-est-sun-open").disabled = false;
  document.getElementById("review-est-sun-close").disabled = false;

  document.getElementById("review-est-description").value = "";

  reviewEstFeatureClick(document.getElementById("review-est-deliveryNo"));
  reviewEstFeatureClick(document.getElementById("review-est-takeoutNo"));
  reviewEstFeatureClick(document.getElementById("review-est-patioNo"));
  reviewEstFeatureClick(document.getElementById("review-est-rooftopNo"));
  reviewEstFeatureClick(document.getElementById("review-est-brunchNo"));
  reviewEstFeatureClick(document.getElementById("review-est-dogNo"));
}

function previewReviewEstImg(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById('review-est-img-circle').src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}

function formatInputTime(time) {
  var returnTime = "";

  if(time.length == 4)
    returnTime = time.substring(0,2) + ":" + time.substring(2);
  else if(time.length == 3)
    returnTime = "0" + time.substring(0,1) + ":" + time.substring(1);

  return returnTime;
}

function setReviewEstClosed(e, day) {
  if(e != null)
    e.preventDefault();

  if(document.getElementById(day).className == "btn btn-light") {
    document.getElementById(day).className = "btn btn-primary";
    document.getElementById(day+"-open").value = "";
    document.getElementById(day+"-close").value = "";
    document.getElementById(day+"-open").disabled = true;
    document.getElementById(day+"-close").disabled = true;
  }
  else {
    document.getElementById(day).className = "btn btn-light";
    document.getElementById(day+"-open").disabled = false;
    document.getElementById(day+"-close").disabled = false;
  }
}

function reviewEstFeatureClick(el) {
  document.getElementById(el.id).className = "btn btn-primary active";
  document.getElementById(el.id).style = "";

  var feature;

  if(el.id.indexOf("Yes") > -1) {
    feature = el.id.substring(0, el.id.indexOf("Yes"));
    document.getElementById(feature+"No").className = "btn btn-secondary";
    document.getElementById(feature+"No").style.backgroundColor = "rgb(240,240,240)";
    document.getElementById(feature).value = "Yes";
  }
  else if(el.id.indexOf("No") > -1) {
    feature = el.id.substring(0, el.id.indexOf("No"));
    document.getElementById(feature+"Yes").className = "btn btn-secondary";
    document.getElementById(feature+"Yes").style.backgroundColor = "rgb(240,240,240)";
    document.getElementById(feature).value = "No";

    document.getElementById(el.id).style.borderRadius = "5px";
    document.getElementById(el.id).style.borderTopLeftRadius = "0";
    document.getElementById(el.id).style.borderBottomLeftRadius = "0";
  }
}

function adminAddToAH(estId, add) {
  if(add) {
    swal({
      title: "Add to AppyHour!",
      text: "Do you want to add this establishment to AppyHour?",
      icon: "success",
      dangerMode: false,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, add it!",
          value: "duplicate",
          visible: true,
          className: "",
          closeModal: true
        }
      }
    })
    .then((confirm) => {
      if(confirm) {
        var parameters = {
          estId: estId,
          add: add
        }

        $.post('/addtoah', parameters, function(res) {
            document.getElementById("estQueueRow"+estId).style.backgroundColor = "rgba(255,170,0,0.1)";
            document.getElementById("estQueueAHStatus"+estId).innerHTML = "Added to AH";
            document.getElementById("estQueueAHStatus"+estId).style.color = "orange";
            document.getElementById("btnEdit"+estId).style.display = "none";
            document.getElementById("btnWillInsert"+estId).style.display = "none";
            document.getElementById("btnReview"+estId).style.display = "none";
            document.getElementById("btnWillNotInsert"+estId).style.display = "none";
            document.getElementById("btnAddToQueue"+estId).style.display = "none";

            var newRow = '<tr><td style="padding:4px;">'+
                          '<div style="background-color:orange; width:10px; height:10px; border-radius:50%; display:inline-block; vertical-align:middle; margin:0 4px;"></div>'+
                          '<div style="display:inline-block; vertical-align:middle;">'+
                           '<p style="color:black; font-weight:500; line-height:1;">' + res.name + '</p>'+
                           '<p style="font-size:10px;">' + res.address + '</p>'+
                          '</div>'+
                         '</td></tr>';

            $(newRow).insertBefore('#ahEstsTable > tbody > tr:first');
        });
      }
    });
  }
  else {
    swal({
      title: "Will Not Insert",
      text: "Why should this establishment not be added to AH?",
      icon: "warning",
      dangerMode: true,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        duplicate: {
          text: "Already in AH",
          value: "duplicate",
          visible: true,
          className: "",
          closeModal: true
        },
        willnotinsert: {
          text: "Don't want it",
          value: "willnotinsert",
          visible: true,
          className: "",
          closeModal: true
        }
      }
    })
    .then((reason) => {
      if(reason == "duplicate" || reason == "willnotinsert") {
        var parameters = {
          estId: estId,
          add: add,
          reason: reason
        }

        $.post('/addtoah', parameters, function(res) {
            document.getElementById("estQueueRow"+estId).style.backgroundColor = "rgb(240,240,240)";
            document.getElementById("estQueueAHStatus"+estId).innerHTML = reason == "duplicate" ? "Already in AH" : "Will Not Insert";
            document.getElementById("estQueueAHStatus"+estId).style.color = "grey";
            //document.getElementById("btnEdit"+estId).style.display = "none";
            //document.getElementById("btnInsert"+estId).style.display = "none";
            document.getElementById("btnReview"+estId).style.display = "none";
            document.getElementById("btnWillNotInsert"+estId).style.display = "none";
            document.getElementById("btnAddToQueue"+estId).style.display = "block";
        });
      }
    });
  }
}

function adminAddToQueue(estId) {
  var parameters = {
    estId: estId
  }
  $.post('/addtoqueue', parameters, function(res) {
    document.getElementById("estQueueRow"+estId).style.backgroundColor = "unset";
    document.getElementById("estQueueAHStatus"+estId).innerHTML = "Pending Review";
    document.getElementById("estQueueAHStatus"+estId).style.color = "blue";
    //document.getElementById("btnEdit"+estId).style.display = "inline-block";
    //document.getElementById("btnInsert"+estId).style.display = "inline-block";
    document.getElementById("btnReview"+estId).style.display = "inline-block";
    document.getElementById("btnWillNotInsert"+estId).style.display = "inline-block";
    document.getElementById("btnAddToQueue"+estId).style.display = "none";
  });
}

function adminEstSearch(search) {
  search = search.toUpperCase();

  var table = document.getElementById("ahEstsTable");
  var tr = table.getElementsByTagName("tr");

  for(i = 0; i < tr.length; i++) {
    var td = tr[i].getElementsByTagName("td")[0];
    if(td) {
      var txtValue = td.textContent || td.innerText;
      if(txtValue.toUpperCase().indexOf(search) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}

function reviewEstOwnerChange(userId) {
  document.getElementById("review-est-owner-username").value = "";
  document.getElementById("review-est-owner-email").value = "";
  document.getElementById("review-est-owner-phoneNumber").value = "";
  document.getElementById("review-est-owner-needsToPay").value = "";

  if(userId != "") {
    document.getElementById("review-est-owner-username").value = document.getElementById("owner-username"+userId).value;
    document.getElementById("review-est-owner-email").value = document.getElementById("owner-email"+userId).value;
    document.getElementById("review-est-owner-phoneNumber").value = document.getElementById("owner-phoneNumber"+userId).value;
    document.getElementById("review-est-owner-needsToPay").value = document.getElementById("owner-needsToPay"+userId).value;

    document.getElementById("review-est-owner-username").disabled = true;
    document.getElementById("review-est-owner-email").disabled = true;
    document.getElementById("review-est-owner-phoneNumber").disabled = true;
    document.getElementById("review-est-owner-needsToPay").disabled = true;
  }
  else {
    document.getElementById("review-est-owner-username").disabled = false;
    document.getElementById("review-est-owner-email").disabled = false;
    document.getElementById("review-est-owner-phoneNumber").disabled = false;
    document.getElementById("review-est-owner-needsToPay").disabled = false;
  }
}

function getDistanceFromLatLng(userLat, userLon, lat, lon) {
    Number.prototype.toRad = function() {
        return this * Math.PI / 180;
    }

    if(userLat && userLon && lat && lon) {
        var lat2 = userLat;
        var lon2 = userLon;
        var lat1 = parseFloat(lat);
        var lon1 = parseFloat(lon);

        var R = 3959; // Radius in miles has a problem with the .toRad() method below.
        var x1 = lat2 - lat1;
        var dLat = x1.toRad();
        var x2 = lon2 - lon1;
        var dLon = x2.toRad();
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) *
                Math.sin(dLon / 2) * Math.sin(dLon / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;

        d = Math.round(d * 100)/100;

        return d;
    } else {
        return "";
    }
}

function numOnly(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]/;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}



/* SUPPLIERS */

$("#add-edit-supplier-form").bind('ajax:complete', function() {
  document.body.style.cursor = "default";
});

function initSuppliersCampaigns(locationId) {
  window.location.href = "/promos?location="+locationId;
}

function openAddSupplierModal() {
  $("#supplier-phone").inputmask({"mask": "(999) 999-9999"});
  $('#edit-supplier-modal').modal({backdrop: 'static', keyboard: false}); 

  document.getElementById("supplier-modal-title").innerHTML = "Add Supplier";

  document.getElementById("add-edit-supplier-err").innerHTML = "";
  document.getElementById("add-edit-supplier-err").style.display = "none";

  //document.getElementById("btnSaveAndSend").style.display = "inline-block";
  document.getElementById("btnArchiveSupplier").style.display = "none";

  // clear inputs
  document.getElementById("supplier_id").value = "";
  document.getElementById("supplier-img").value = "";
  document.getElementById("supplierImg").value = "";
  document.getElementById("supplierImgDiv").style.backgroundImage = "none";
  document.getElementById("supplierImgDiv").style.border = "1px dashed gainsboro";

  document.getElementById("supplier-name").value = "";
  document.getElementById("supplier-username").value = "";
  document.getElementById("supplier-email").value = "";
  document.getElementById("supplier-phone").value = "";
}

function adminEditSupplier(supplierId, e) {
  e.preventDefault();

  $("#supplier-phone").inputmask({"mask": "(999) 999-9999"});
  $('#edit-supplier-modal').modal({backdrop: 'static', keyboard: false}); 

  document.getElementById("supplier-modal-title").innerHTML = "Edit Supplier";

  //document.getElementById("btnSaveAndSend").style.display = "none";
  document.getElementById("btnArchiveSupplier").style.display = "inline-block";

  var img = document.getElementById("supplier-image-"+supplierId).innerHTML;
  var name = document.getElementById("supplier-name-"+supplierId).innerHTML;
  var username = document.getElementById("supplier-username-"+supplierId).innerHTML;
  var email = document.getElementById("supplier-email-"+supplierId).innerHTML;
  var phone = document.getElementById("supplier-phone-"+supplierId).innerHTML;

  document.getElementById("supplier_id").value = supplierId;
  document.getElementById("supplier-img").value = img;
  document.getElementById("supplierImgDiv").style.backgroundImage = "url('https://est-img.s3.us-east-2.amazonaws.com/"+img+"')";
  document.getElementById("supplierImgDiv").style.backgroundPosition = "center";
  document.getElementById("supplierImgDiv").style.backgroundSize = "cover";

  document.getElementById("supplier-name").value = name;
  document.getElementById("supplier-username").value = username;
  document.getElementById("supplier-email").value = email;
  document.getElementById("supplier-phone").value = phone;
}

function previewSupplierImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById("supplierImgDiv").style.backgroundImage = "url('"+reader.result+"')";
    document.getElementById("supplierImgDiv").style.backgroundPosition = "center";
    document.getElementById("supplierImgDiv").style.backgroundSize = "cover";
    document.getElementById("supplierImgDiv").style.border = "none";
    //document.getElementById("btnRemoveSupplierImg").style.display = "inline-block";
  }
  reader.readAsDataURL(event.target.files[0]);
}

function adminValidateSupplier(e) {
  e.preventDefault();
  document.getElementById("btnAdminSaveSupplier").disabled = true;
  document.body.style.cursor = "wait";
  document.getElementById("add-edit-supplier-err").innerHTML = "";
  document.getElementById("add-edit-supplier-err").style.display = "none";

  var err = [];
  var supplierId = document.getElementById("supplier_id").value;
  var oldImg = document.getElementById("supplier-img").value;
  var newImg = document.getElementById("supplierImg").value;
  var name = document.getElementById("supplier-name").value;
  var username = document.getElementById("supplier-username").value;
  var email = document.getElementById("supplier-email").value;
  var phone = document.getElementById("supplier-phone").value;

  if(oldImg == '' && newImg == '')
    err.push("Please choose a profile pic");
  if(name == '')
    err.push("Please choose a name");
  if(username == '')
    err.push("Please choose a username");
  if(email == '')
    err.push("Please choose an email");

  if(phone != '')
    phone = phone.replace("(", "").replace(")", "").replace(" ", "").replace("-", "").replace("_", "");

  if(phone.length > 0 && phone.length != 10)
    err.push("Please enter a valid phone number or leave it blank");

  if(err.length == 0) {
    var parameters = {
      supplierId: supplierId,
      img: newImg != '' ? newImg : oldImg,
      name: name,
      username: username,
      email: email,
      phone: phone
    }
    $.get('/adminvalidatesupplier', parameters, function(data) {
      if(data.length == 0) {
        document.getElementById("add-edit-supplier-form").submit();
      }
      else {
        for(var d in data)
          document.getElementById("add-edit-supplier-err").innerHTML += "<p style='color:red; font-size:12px;'>" + data[d] + "</p>";
        document.getElementById("add-edit-supplier-err").style.display = "block";
        document.getElementById("btnAdminSaveSupplier").disabled = false;
        document.body.style.cursor = "default";
      }
    });
  }
  else {
    for(var e in err)
      document.getElementById("add-edit-supplier-err").innerHTML += "<p style='color:red; font-size:12px;'>" + err[e] + "</p>";
    document.getElementById("add-edit-supplier-err").style.display = "block";
    document.getElementById("btnAdminSaveSupplier").disabled = false;
    document.body.style.cursor = "default";
  }
}

function supplierSendClaimEmail(supplierId, email, username, e) {
  e.preventDefault();

  swal({
      title: "Send Claim Email",
      text: "Are you sure you want to send a claim email to " + email + "?",
      icon: "info",
      dangerMode: false,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, send it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
    }
  })
  .then((willSend) => {
    if(willSend) {
      document.getElementById("btnSupplierSendClaimEmail"+supplierId).disabled = true;
      document.getElementById("btnSupplierSendClaimEmail"+supplierId).innerHTML = "Sending..."; 
      document.body.style.cursor = "wait";

      var parameters = {
        email: email,
        username: username
      }
      $.get('/suppliersendclaimemail', parameters, function(data) {
        location.reload();
      });
    }
  });
}

function adminArchiveSupplier(supplierId, name, e) {
  e.preventDefault();

  //var supplierId = document.getElementById("supplier_id").value;
  //var name = document.getElementById("supplier-name").value;

  swal({
      title: "Archive Supplier",
      text: "Are you sure you want to archive " + name + "? All associated campaigns and managers will also be archived.",
      icon: "warning",
      dangerMode: true,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, archive it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
    }
  })
  .then((willDelete) => {
      if(willDelete) {
        var parameters = {
          supplierId: supplierId
        };

        $.post('/archivesupplier', parameters, function(err) {
          location.reload();
        });
      }
  });
}

function supplierDropdown(supplierId, e) {
  e.preventDefault();

  // close open menus
  var openMenus = document.querySelectorAll('.dropdown-content');
  openMenus.forEach(function(menus) {
    menus.classList.remove('show');
  }); 

  // open new menu
  document.getElementById("supplierDropdown"+supplierId).classList.toggle("show");
}




/* PROMOS */

const table = document.getElementById('promoTable');

let draggingEle;
let draggingRowIndex;
let placeholder;
let list;
let isDraggingStarted = false;

// The current position of mouse relative to the dragging element
let x = 0;
let y = 0;

const swap = function(nodeA, nodeB) {
    const parentA = nodeA.parentNode;
    const siblingA = nodeA.nextSibling === nodeB ? nodeA : nodeA.nextSibling;

    // Move `nodeA` to before the `nodeB`
    nodeB.parentNode.insertBefore(nodeA, nodeB);

    // Move `nodeB` to before the sibling of `nodeA`
    parentA.insertBefore(nodeB, siblingA);
};

const isAbove = function(nodeA, nodeB) {
    // Get the bounding rectangle of nodes
    const rectA = nodeA.getBoundingClientRect();
    const rectB = nodeB.getBoundingClientRect();

    return (rectA.top + rectA.height / 2 < rectB.top + rectB.height / 2);
};

const cloneTable = function() {
    const rect = table.getBoundingClientRect();
    const width = parseInt(window.getComputedStyle(table).width);

    list = document.createElement('div');
    list.classList.add('clone-list');
    list.style.position = 'absolute';
    list.style.left = `${rect.left}px`;
    //list.style.top = `${rect.top}px`;
    table.parentNode.insertBefore(list, table);

    // Hide the original table
    table.style.visibility = 'hidden';

    table.querySelectorAll('tr').forEach(function(row) {
        if(row.cells[0].innerHTML != "-") {
          // Create a new table from given row
          const item = document.createElement('div');
          item.classList.add('draggable');

          const newTable = document.createElement('table');
          newTable.setAttribute('class', 'clone-table');
          newTable.style.width = `${width}px`;

          const newRow = document.createElement('tr');
          const cells = [].slice.call(row.children);
          cells.forEach(function(cell) {
              const newCell = cell.cloneNode(true);
              newCell.style.width = `${parseInt(window.getComputedStyle(cell).width)}px`;
              newRow.appendChild(newCell);
          });

          newTable.appendChild(newRow);
          item.appendChild(newTable);
          list.appendChild(item);
        }
    });
};

const mouseDownHandler = function(e) {
    // Get the original row
    const originalRow = e.target.parentNode;
    draggingRowIndex = [].slice.call(table.querySelectorAll('tr')).indexOf(originalRow);

    // Determine the mouse position
    x = e.clientX;
    y = e.clientY;

    // Attach the listeners to `document`
    document.addEventListener('mousemove', mouseMoveHandler);
    document.addEventListener('mouseup', mouseUpHandler);
};

const mouseMoveHandler = function(e) {
    if (!isDraggingStarted) {
        isDraggingStarted = true;

        cloneTable();

        draggingEle = [].slice.call(list.children)[draggingRowIndex];
        draggingEle.classList.add('dragging');
        
        // Let the placeholder take the height of dragging element
        // So the next element won't move up
        placeholder = document.createElement('div');
        placeholder.classList.add('placeholder');
        draggingEle.parentNode.insertBefore(placeholder, draggingEle.nextSibling);
        placeholder.style.height = `${draggingEle.offsetHeight}px`;
    }

    // Set position for dragging element
    draggingEle.style.position = 'absolute';
    draggingEle.style.top = `${draggingEle.offsetTop + e.clientY - y}px`;
    draggingEle.style.left = `${draggingEle.offsetLeft + e.clientX - x}px`;

    // Reassign the position of mouse
    x = e.clientX;
    y = e.clientY;

    // The current order
    // prevEle
    // draggingEle
    // placeholder
    // nextEle
    const prevEle = draggingEle.previousElementSibling;
    const nextEle = placeholder.nextElementSibling;
    
    // The dragging element is above the previous element
    // User moves the dragging element to the top
    // We don't allow to drop above the header 
    // (which doesn't have `previousElementSibling`)
    if (prevEle && prevEle.previousElementSibling && isAbove(draggingEle, prevEle)) {
        // The current order    -> The new order
        // prevEle              -> placeholder
        // draggingEle          -> draggingEle
        // placeholder          -> prevEle
        swap(placeholder, draggingEle);
        swap(placeholder, prevEle);
        return;
    }

    // The dragging element is below the next element
    // User moves the dragging element to the bottom
    if (nextEle && isAbove(nextEle, draggingEle)) {
        // The current order    -> The new order
        // draggingEle          -> nextEle
        // placeholder          -> placeholder
        // nextEle              -> draggingEle
        swap(nextEle, placeholder);
        swap(nextEle, draggingEle);
    }
};

const mouseUpHandler = function() {
    // Remove the placeholder
    if(placeholder && placeholder.parentNode)
      placeholder && placeholder.parentNode.removeChild(placeholder);
    
    if(draggingEle) {
      draggingEle.classList.remove('dragging');
      draggingEle.style.removeProperty('top');
      draggingEle.style.removeProperty('left');
      draggingEle.style.removeProperty('position');
  

      // Get the end index
      var endRowIndex = [].slice.call(list.children).indexOf(draggingEle);

      isDraggingStarted = false;

      // Remove the `list` element
      if(list && list.parentNode)
        list.parentNode.removeChild(list);

      // Move the dragged row to `endRowIndex`
      let rows = [].slice.call(table.querySelectorAll('tr'));
      draggingRowIndex > endRowIndex
          ? rows[endRowIndex].parentNode.insertBefore(rows[draggingRowIndex], rows[endRowIndex])
          : rows[endRowIndex].parentNode.insertBefore(rows[draggingRowIndex], rows[endRowIndex].nextSibling);
    }

    // Bring back the table
    table.style.removeProperty('visibility');

    // Remove the handlers of `mousemove` and `mouseup`
    document.removeEventListener('mousemove', mouseMoveHandler);
    document.removeEventListener('mouseup', mouseUpHandler);
};

function editPriorities(e) {
  e.preventDefault();

  var editSaveIcon = document.getElementById("editSaveIcon");

  // make table editable
  if(editSaveIcon.className == "fa fa-pencil") {
    editSaveIcon.className = "fa fa-check";
    editSaveIcon.style.color = "green";
    document.getElementById("editCancelIcon").style.display = "inline";

    table.querySelectorAll('tr').forEach(function(row, index) {
        if(index === 0) { return; } // ignore the header

        if(row.cells[0].innerHTML != "-") {
          row.classList.add('draggable');
          row.addEventListener('mousedown', mouseDownHandler);
        }
    });

    // hide the dropdown buttons
    var dropbtns = document.getElementsByClassName("dropbtn");
    for(var d in dropbtns) {
      if(dropbtns[d].tagName == "BUTTON") {
        dropbtns[d].style.display = "none";
      }
    }
  }
  // save table priorities
  else {
    var promoIds = [];
    var priorities = [];

    table.querySelectorAll('tr').forEach(function(row, index) {
        if(index === 0) { return; } // ignore the header

        if(row.cells[0].innerHTML != "-") {
          priorities.push(index);
          promoIds.push(row.cells[2].innerHTML);

          row.classList.remove('draggable');
          row.removeEventListener('mousedown', mouseDownHandler);
        }
    });

    var parameters = {
      locationId: parseInt(document.getElementById("locationId").value),
      priorities: JSON.stringify(priorities),
      promoIds: JSON.stringify(promoIds)
    }

    $.post('/savepromopriority', parameters, function(res) {
      if(res) {
        table.querySelectorAll('tr').forEach(function(row, index) {
          if(index === 0) { return; } // ignore the header

          if(row.cells[0].innerHTML != "-") {
            row.cells[0].innerHTML = index;
          }
        });

        // fix edit/save & cancel icons
        document.getElementById("editCancelIcon").style.display = "none";
        editSaveIcon.className = "fa fa-pencil";
        editSaveIcon.style.color = "#337ab7";

        // show the dropdown buttons
        var dropbtns = document.getElementsByClassName("dropbtn");
        for(var d in dropbtns) {
          if(dropbtns[d].tagName == "BUTTON") {
            dropbtns[d].style.display = "block";
          }
        }
      }
    });
  }
}

function cancelEditPriorities(e) {
  e.preventDefault();

  document.getElementById("editCancelIcon").style.display = "none";
  editSaveIcon.className = "fa fa-pencil";
  editSaveIcon.style.color = "#337ab7";

  table.querySelectorAll('tr').forEach(function(row, index) {
      if(index === 0) { return; } // ignore the header

      row.classList.remove('draggable');
      row.removeEventListener('mousedown', mouseDownHandler);
  });

  // show the dropdown buttons
  var dropbtns = document.getElementsByClassName("dropbtn");
  for(var d in dropbtns) {
    if(dropbtns[d].tagName == "BUTTON") {
      dropbtns[d].style.display = "block";
    }
  }

  // sort by priority
  var rows, switching, i, x, y, shouldSwitch;
  switching = true;
  while (switching) {
    switching = false;
    rows = table.rows;
    for (i = 1; i < (rows.length - 1); i++) {
      shouldSwitch = false;
      x = rows[i].getElementsByTagName("TD")[0];
      y = rows[i + 1].getElementsByTagName("TD")[0];
      if (parseInt(x.innerHTML) > parseInt(y.innerHTML)) {
        shouldSwitch = true;
        break;
      }
    }
    if (shouldSwitch) {
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
    }
  }
}

function adminViewPromo(promoId) {
  $("#view-promo-modal").modal("show"); 

  var img = document.getElementById("image"+promoId).innerHTML;
  var title = document.getElementById("title"+promoId).innerHTML;
  var label = document.getElementById("label"+promoId).innerHTML;
  var fontColor = document.getElementById("fontColor"+promoId).innerHTML;
  var textAlign = document.getElementById("textAlign"+promoId).innerHTML;
  var description = document.getElementById("description"+promoId).innerHTML;
  var day = document.getElementById("day"+promoId).innerHTML;
  var location = document.getElementById("locationStr"+promoId).innerHTML;
  var promoType = document.getElementById("promo_type"+promoId).innerHTML;
  var keywords = document.getElementById("keywords"+promoId).innerHTML;
  var features = document.getElementById("features"+promoId).innerHTML;
  var textColor = document.getElementById("fontColor"+promoId).innerHTML;
  var textAlign = document.getElementById("textAlign"+promoId).innerHTML;
  var active = document.getElementById("activeYN"+promoId).innerHTML;

  document.getElementById("view-promo-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+img+"')";
  document.getElementById("view-promo-bg").style.backgroundPosition = "center";
  document.getElementById("view-promo-bg").style.backgroundSize = "cover";

  if(title != "") {
    document.getElementById("view-promo-title").style.display = "block";
    document.getElementById("view-promo-title").innerHTML = title;
    document.getElementById("view-promo-title").style.backgroundColor = fontColor == "white" ? "black" : "white";
    document.getElementById("view-promo-title").style.color = fontColor;
    if(textAlign == "left") {
      document.getElementById("view-promo-title").style.left = "24px";
      document.getElementById("view-promo-title").style.right = "";
    }
    else {
      document.getElementById("view-promo-title").style.right = "24px";
      document.getElementById("view-promo-title").style.left = "";
    }
  }
  else {
    document.getElementById("view-promo-title").style.display = "none";
  }

  if(label != "") {
    document.getElementById("view-promo-label").style.display = "block";
    document.getElementById("view-promo-label").innerHTML = label;
    document.getElementById("view-promo-label").style.backgroundColor = fontColor == "white" ? "black" : "white";
    document.getElementById("view-promo-label").style.color = fontColor;
    if(textAlign == "left") {
      document.getElementById("view-promo-label").style.left = "24px";
      document.getElementById("view-promo-label").style.right = "";
    }
    else {
      document.getElementById("view-promo-label").style.right = "24px";
      document.getElementById("view-promo-label").style.left = "";
    }
  }
  else {
    document.getElementById("view-promo-label").style.display = "none";
  }

  document.getElementById("view-promo-type").innerHTML = promoType;
  document.getElementById("view-promo-keywords").innerHTML = keywords != "" ? keywords.toLowerCase() : "-";
  document.getElementById("view-promo-location").innerHTML = location != "" ? location.toLowerCase() : "all";
  document.getElementById("view-promo-active").innerHTML = active.toLowerCase();

  if(active == "Yes")
    document.getElementById("view-promo-active").style.color = "green";
  else if(active == "No")
    document.getElementById("view-promo-active").style.color = "red";

  if(promoType == "establishments" || promoType == "specials")
    document.getElementById("view-promo-features-row").style.display = "block";
  else
    document.getElementById("view-promo-features-row").style.display = "none";

  document.getElementById("view-promo-features").innerHTML = "";
  if(features != "") {
    features = features.split(";");
    
    for(var f in features) {
      if(features[f] == "patio") {
        document.getElementById("view-promo-features").innerHTML += "patio;";
      }
      else if(features[f] == "rooftop") {
        document.getElementById("view-promo-features").innerHTML += "rooftop;";
      }
      else if(features[f] == "dog_friendly") {
        document.getElementById("view-promo-features").innerHTML += "dog-friendly;";
      }
      else if(features[f] == "brunch") {
        document.getElementById("view-promo-features").innerHTML += "brunch;";
      }
      else if(features[f] == "delivery") {
        document.getElementById("view-promo-features").innerHTML += "delivery;";
      }
      else if(features[f] == "takeout") {
        document.getElementById("view-promo-features").innerHTML += "takeout;";
      }
      else if(features[f] == "black_owned") {
        document.getElementById("view-promo-features").innerHTML += "black-owned;";
      }
      else if(features[f] == "na_options") {
        document.getElementById("view-promo-features").innerHTML += "na-options;";
      }
      else if(features[f] == "paying") {
        document.getElementById("view-promo-features").innerHTML += "paying;";
      }
    }

    var featHtml = document.getElementById("view-promo-features").innerHTML;
    featHtml = featHtml.substring(0, featHtml.length-1);
    document.getElementById("view-promo-features").innerHTML = featHtml;
  }

  if(day == -1) {
    document.getElementById("view-promo-day").innerHTML = "everyday";
  }
  else if(day == 0) {
    document.getElementById("view-promo-day").innerHTML = "sunday";
  }
  else if(day == 1) {
    document.getElementById("view-promo-day").innerHTML = "monday";
  }
  else if(day == 2) {
    document.getElementById("view-promo-day").innerHTML = "tuesday";
  }
  else if(day == 3) {
    document.getElementById("view-promo-day").innerHTML = "wednesday";
  }
  else if(day == 4) {
    document.getElementById("view-promo-day").innerHTML = "thursday";
  }
  else if(day == 5) {
    document.getElementById("view-promo-day").innerHTML = "friday";
  }
  else if(day == 6) {
    document.getElementById("view-promo-day").innerHTML = "saturday";
  }
}

function adminEditPromo(promoId, e) {
  e.preventDefault();
  $("#edit-promo-modal").modal("show"); 
  document.getElementById("modal-title").innerHTML = "Edit Promo";

  var img = document.getElementById("image"+promoId).innerHTML;
  var detailsImg = document.getElementById("detailsImage"+promoId).innerHTML;
  var title = document.getElementById("title"+promoId).innerHTML;
  var label = document.getElementById("label"+promoId).innerHTML;
  var fontColor = document.getElementById("fontColor"+promoId).innerHTML;
  var textAlign = document.getElementById("textAlign"+promoId).innerHTML;
  var description = document.getElementById("description"+promoId).innerHTML;
  var day = document.getElementById("day"+promoId).innerHTML;
  var location = document.getElementById("location"+promoId).innerHTML;
  var promoType = document.getElementById("promo_type"+promoId).innerHTML;
  var keywords = document.getElementById("keywords"+promoId).innerHTML;
  var features = document.getElementById("features"+promoId).innerHTML;
  var textColor = document.getElementById("fontColor"+promoId).innerHTML;
  var textAlign = document.getElementById("textAlign"+promoId).innerHTML;
  var vertAlign = document.getElementById("vertAlign"+promoId).innerHTML;
  var link = document.getElementById("link"+promoId).innerHTML;
  var linkText = document.getElementById("ctaLabel"+promoId).innerHTML;
  var linkTextColor = document.getElementById("ctaLabelTextColor"+promoId).innerHTML;
  var linkBgColor = document.getElementById("ctaLabelBgColor"+promoId).innerHTML;
  var linkBtnText = document.getElementById("linkText"+promoId).innerHTML;
  var linkBtnTextColor = document.getElementById("linkTextColor"+promoId).innerHTML;
  var linkBtnBgColor = document.getElementById("linkBgColor"+promoId).innerHTML;
  var active = document.getElementById("active"+promoId).innerHTML;

  document.getElementById("admin-campaign-promo_id").value = promoId;

  /* LOGISTICS */

  // campaign type
  document.getElementById("admin-campaign-type").value = promoType;
  selectPromoType(promoType);

  // features
  document.getElementById("admin-campaign-patio").checked = false;
  document.getElementById("admin-campaign-rooftop").checked = false;
  document.getElementById("admin-campaign-dogs").checked = false;
  document.getElementById("admin-campaign-brunch").checked = false;
  document.getElementById("admin-campaign-delivery").checked = false;
  //document.getElementById("admin-campaign-blackowned").checked = false;
  //document.getElementById("naoptions").checked = false;
  document.getElementById("admin-campaign-paying").checked = false;
  document.getElementById("admin-campaign-takeout").checked = false;
  document.getElementById("admin-campaign-carryout").checked = false;
  if(features != "") {
    features = features.split(";");
    
    for(var f in features) {
      if(features[f] == "patio") {
        document.getElementById("admin-campaign-patio").checked = true;
      }
      else if(features[f] == "rooftop") {
        document.getElementById("admin-campaign-rooftop").checked = true;
      }
      else if(features[f] == "dog_friendly") {
        document.getElementById("admin-campaign-dogs").checked = true;
      }
      else if(features[f] == "brunch") {
        document.getElementById("admin-campaign-brunch").checked = true;
      }
      else if(features[f] == "delivery") {
        document.getElementById("admin-campaign-delivery").checked = true;
      }
      else if(features[f] == "black_owned") {
        //document.getElementById("admin-campaign-blackowned").checked = true;
      }
      else if(features[f] == "na_options") {
        //document.getElementById("naoptions").checked = true;
      }
      else if(features[f] == "paying") {
        document.getElementById("admin-campaign-paying").checked = true;
      }
      else if(features[f] == "takeout" && promoType == "establishments") {
        document.getElementById("admin-campaign-takeout").checked = true;
      }
      else if(features[f] == "takeout" && promoType == "specials") {
        document.getElementById("admin-campaign-carryout").checked = true;
      }
    }
  }

  // keywords
  if(promoType == 'specials' || promoType == 'events') {
    // don't allow special characters (except spaces) for keywords
    $('.bootstrap-tagsinput input').on('keypress', function(e) {
      var regex = new RegExp("^[0-9a-zA-Z \b]+$");
      var key = String.fromCharCode(e.charCode ? e.charCode : !e.which);
      if (!regex.test(key)) {
         e.preventDefault();
      }
    });
    $('#admin-campaign-keywords').on('beforeItemAdd', function(event) {
      var tags = $('#admin-campaign-keywords').val();
      tags = tags.split(",");
      if(tags.length + 1 > 10)
        event.cancel = true;
    });

    keywords = keywords.split(";");

    for(var k in keywords)
      $('#admin-campaign-keywords').tagsinput('add', keywords[k]);
  }

  // days(s)
  document.getElementById("admin-campaign-everyday").value = "false";
  document.getElementById("admin-campaign-mon").value = "false";
  document.getElementById("admin-campaign-tue").value = "false";
  document.getElementById("admin-campaign-wed").value = "false";
  document.getElementById("admin-campaign-thu").value = "false";
  document.getElementById("admin-campaign-fri").value = "false";
  document.getElementById("admin-campaign-sat").value = "false";
  document.getElementById("admin-campaign-sun").value = "false";
  var dayArr = day.split(",");
  for(var d in dayArr)
    togglePromoDay(parseInt(dayArr[d]));

  // location(s)
  document.getElementById("admin-campaign-location").value = location;

  // active
  document.getElementById("admin-campaign-activeY").checked = active == "1" ? true : false;
  document.getElementById("admin-campaign-activeN").checked = active == "0" ? true : false;


  /* DEAL FEED VIEW */

  // preview
  document.getElementById("admin-campaign-img-original").value = img;
  document.getElementById("admin-campaign-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+img+"')";
  document.getElementById("admin-campaign-bg").style.backgroundPosition = "center";
  document.getElementById("admin-campaign-bg").style.backgroundSize = "cover";
  if(title != "") {
    document.getElementById("admin-campaign-bg-title").style.display = "block";
    document.getElementById("admin-campaign-bg-title").innerHTML = title;
  }
  else {
    document.getElementById("admin-campaign-bg-title").style.display = "none";
  }
  if(label != "") {
    document.getElementById("admin-campaign-bg-label").style.display = "block";
    document.getElementById("admin-campaign-bg-label").innerHTML = label;
  }
  else {
    document.getElementById("admin-campaign-bg-label").style.display = "none";
  }

  // text overlay
  adminToggleTextOverlay(title != "" ? "Y" : "N");

  // title
  document.getElementById("admin-campaign-title").value = title;

  // label
  document.getElementById("admin-campaign-label").value = label;

  // text color
  changeTextColor(fontColor);

  // horizontal align
  changeTextAlign(textAlign);

  // vertical align
  changeVertAlign(vertAlign);
  
  
  /* DETAILs VIEW */

  // preview
  document.getElementById("admin-campaign-details-img-original").value = detailsImg;
  document.getElementById("admin-campaign-details-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+detailsImg+"')";
  document.getElementById("admin-campaign-details-bg").style.backgroundPosition = "center";
  document.getElementById("admin-campaign-details-bg").style.backgroundSize = "cover";
  if(description != "") {
    document.getElementById("admin-campaign-details-bg-text").style.display = "block";
    document.getElementById("admin-campaign-details-bg-text").innerHTML = description;
  }
  else {
    document.getElementById("admin-campaign-details-bg-text").style.display = "none";
  }

  // action button preview
  document.getElementById("admin-campaign-action-btn-div").style.display = link.trim() != "" ? "block" : "none";
  document.getElementById("admin-campaign-action-btn-txt").innerHTML = linkBtnText;
  document.getElementById("admin-campaign-action-btn-txt").style.color = linkBtnTextColor;
  document.getElementById("admin-campaign-action-btn").style.backgroundColor = linkBtnBgColor;
  document.getElementById("admin-campaign-cta-txt").innerHTML = linkText;
  document.getElementById("admin-campaign-cta-txt").style.color = linkTextColor;
  document.getElementById("admin-campaign-action-btn-div").style.backgroundColor = linkBgColor;


  // text overlay
  adminToggleDetailsTextOverlay(description != "" ? "Y" : "N");

  // text  
  document.getElementById("admin-campaign-description").value = description;
  
  // text color
  changeDetailsTextColor(fontColor);

  // action button
  adminToggleDetailsActionBtn(link.trim() != "" ? "Y" : "N");
  document.getElementById("admin-campaign-link").value = link;
  document.getElementById("admin-campaign-linkText").value = linkBtnText;
  document.getElementById("admin-campaign-linkTextColor").value = linkBtnTextColor;
  document.getElementById("admin-campaign-linkBgColor").value = linkBtnBgColor;
  document.getElementById("admin-campaign-ctaLabel").value = linkText;
  document.getElementById("admin-campaign-ctaLabelTextColor").value = linkTextColor;
  document.getElementById("admin-campaign-ctaLabelBgColor").value = linkBgColor;
}

function adminDeletePromo(promoId, e) {
  e.preventDefault();

  swal({
      title: "Archive Promo",
      text: "Are you sure you want to archive this promo?",
      icon: "warning",
      dangerMode: true,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, archive it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
    }
  })
  .then((willDelete) => {
      if (willDelete) {
        var parameters = {
          promoId: promoId
        };

        $.post('/deletepromo', parameters, function(err) {
          location.reload();
        });
      } 
      else {
        return false;
      }
  });
}

function togglePromoDay(day) {
  if(day == -1) {
    document.getElementById("admin-campaign-everydayToggleButton").style.backgroundColor = document.getElementById("admin-campaign-everyday").value == "false" ? "rgb(49,151,255)" : "white";
    document.getElementById("admin-campaign-everydayToggleButton").style.color = document.getElementById("admin-campaign-everyday").value == "false" ? "white" : "#555";
    document.getElementById("admin-campaign-everyday").value = document.getElementById("admin-campaign-everyday").value == "false" ? "true" : "false";

    document.getElementById("admin-campaign-monToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-monToggleButton").style.color = "#555";
    document.getElementById("admin-campaign-tueToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-tueToggleButton").style.color = "#555";
    document.getElementById("admin-campaign-wedToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-wedToggleButton").style.color = "#555";
    document.getElementById("admin-campaign-thuToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-thuToggleButton").style.color = "#555";
    document.getElementById("admin-campaign-friToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-friToggleButton").style.color = "#555";
    document.getElementById("admin-campaign-satToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-satToggleButton").style.color = "#555";
    document.getElementById("admin-campaign-sunToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-sunToggleButton").style.color = "#555";
    
    document.getElementById("admin-campaign-mon").value = "false";
    document.getElementById("admin-campaign-tue").value = "false";
    document.getElementById("admin-campaign-wed").value = "false";
    document.getElementById("admin-campaign-thu").value = "false";
    document.getElementById("admin-campaign-fri").value = "false";
    document.getElementById("admin-campaign-sat").value = "false";
    document.getElementById("admin-campaign-sun").value = "false";
  }
  else {
    document.getElementById("admin-campaign-everydayToggleButton").style.backgroundColor = "white";
    document.getElementById("admin-campaign-everydayToggleButton").style.color = "#555";
    document.getElementById("admin-campaign-everyday").value = "false";

    if(day == 0) {
      document.getElementById("admin-campaign-sunToggleButton").style.backgroundColor = document.getElementById("admin-campaign-sun").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("admin-campaign-sunToggleButton").style.color = document.getElementById("admin-campaign-sun").value == "false" ? "white" : "#555";
      document.getElementById("admin-campaign-sun").value = document.getElementById("admin-campaign-sun").value == "false" ? "true" : "false";
    }
    else if(day == 1) {
      document.getElementById("admin-campaign-monToggleButton").style.backgroundColor = document.getElementById("admin-campaign-mon").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("admin-campaign-monToggleButton").style.color = document.getElementById("admin-campaign-mon").value == "false" ? "white" : "#555";
      document.getElementById("admin-campaign-mon").value = document.getElementById("admin-campaign-mon").value == "false" ? "true" : "false";
    }
    else if(day == 2) {
      document.getElementById("admin-campaign-tueToggleButton").style.backgroundColor = document.getElementById("admin-campaign-tue").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("admin-campaign-tueToggleButton").style.color = document.getElementById("admin-campaign-tue").value == "false" ? "white" : "#555";
      document.getElementById("admin-campaign-tue").value = document.getElementById("admin-campaign-tue").value == "false" ? "true" : "false";
    }
    else if(day == 3) {
      document.getElementById("admin-campaign-wedToggleButton").style.backgroundColor = document.getElementById("admin-campaign-wed").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("admin-campaign-wedToggleButton").style.color = document.getElementById("admin-campaign-wed").value == "false" ? "white" : "#555";
      document.getElementById("admin-campaign-wed").value = document.getElementById("admin-campaign-wed").value == "false" ? "true" : "false";
    }
    else if(day == 4) {
      document.getElementById("admin-campaign-thuToggleButton").style.backgroundColor = document.getElementById("admin-campaign-thu").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("admin-campaign-thuToggleButton").style.color = document.getElementById("admin-campaign-thu").value == "false" ? "white" : "#555";
      document.getElementById("admin-campaign-thu").value = document.getElementById("admin-campaign-thu").value == "false" ? "true" : "false";
    }
    else if(day == 5) {
      document.getElementById("admin-campaign-friToggleButton").style.backgroundColor = document.getElementById("admin-campaign-fri").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("admin-campaign-friToggleButton").style.color = document.getElementById("admin-campaign-fri").value == "false" ? "white" : "#555";
      document.getElementById("admin-campaign-fri").value = document.getElementById("admin-campaign-fri").value == "false" ? "true" : "false";
    }
    else if(day == 6) {
      document.getElementById("admin-campaign-satToggleButton").style.backgroundColor = document.getElementById("admin-campaign-sat").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("admin-campaign-satToggleButton").style.color = document.getElementById("admin-campaign-sat").value == "false" ? "white" : "#555";
      document.getElementById("admin-campaign-sat").value = document.getElementById("admin-campaign-sat").value == "false" ? "true" : "false";
    }
  }


  /*
  document.getElementById("admin-campaign-everydayToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-everydayToggleButton").style.color = "black";
  document.getElementById("admin-campaign-monToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-monToggleButton").style.color = "black";
  document.getElementById("admin-campaign-tueToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-tueToggleButton").style.color = "black";
  document.getElementById("admin-campaign-wedToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-wedToggleButton").style.color = "black";
  document.getElementById("admin-campaign-thuToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-thuToggleButton").style.color = "black";
  document.getElementById("admin-campaign-friToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-friToggleButton").style.color = "black";
  document.getElementById("admin-campaign-satToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-satToggleButton").style.color = "black";
  document.getElementById("admin-campaign-sunToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-sunToggleButton").style.color = "black";

  document.getElementById("admin-campaign-everyday").value = "false";
  document.getElementById("admin-campaign-mon").value = "false";
  document.getElementById("admin-campaign-tue").value = "false";
  document.getElementById("admin-campaign-wed").value = "false";
  document.getElementById("admin-campaign-thu").value = "false";
  document.getElementById("admin-campaign-fri").value = "false";
  document.getElementById("admin-campaign-sat").value = "false";
  document.getElementById("admin-campaign-sun").value = "false";

  if(day == -1) {
    document.getElementById("admin-campaign-everydayToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-everydayToggleButton").style.color = "white";
    document.getElementById("admin-campaign-everyday").value = "true";
  }
  else if(day == 0) {
    document.getElementById("admin-campaign-sunToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-sunToggleButton").style.color = "white";
    document.getElementById("admin-campaign-sun").value = "true";
  }
  else if(day == 1) {
    document.getElementById("admin-campaign-monToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-monToggleButton").style.color = "white";
    document.getElementById("admin-campaign-mon").value = "true";
  }
  else if(day == 2) {
    document.getElementById("admin-campaign-tueToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-tueToggleButton").style.color = "white";
    document.getElementById("admin-campaign-tue").value = "true";
  }
  else if(day == 3) {
    document.getElementById("admin-campaign-wedToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-wedToggleButton").style.color = "white";
    document.getElementById("admin-campaign-wed").value = "true";
  }
  else if(day == 4) {
    document.getElementById("admin-campaign-thuToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-thuToggleButton").style.color = "white";
    document.getElementById("admin-campaign-thu").value = "true";
  }
  else if(day == 5) {
    document.getElementById("admin-campaign-friToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-friToggleButton").style.color = "white";
    document.getElementById("admin-campaign-fri").value = "true";
  }
  else if(day == 6) {
    document.getElementById("admin-campaign-satToggleButton").style.backgroundColor = "royalblue";
    document.getElementById("admin-campaign-satToggleButton").style.color = "white";
    document.getElementById("admin-campaign-sat").value = "true";
  }
  */
}

function adminToggleTextOverlay(yn) {
  document.getElementById("admin-campaign-text-overlay-div").style.display = yn == "Y" ? "block" : "none";
  document.getElementById("admin-campaign-textOverlayY").checked = yn == "Y" ? true : false;
  document.getElementById("admin-campaign-textOverlayN").checked = yn == "N" ? true : false;
  document.getElementById("admin-campaign-bg-title").style.display = yn == "Y" ? "block" : "none";
  document.getElementById("admin-campaign-bg-label").style.display = yn == "Y" ? "block" : "none";  
}

function adminToggleDetailsTextOverlay(yn) {
  document.getElementById("admin-campaign-details-text-overlay-div").style.display = yn == "Y" ? "block" : "none";
  document.getElementById("admin-campaign-details-textOverlayY").checked = yn == "Y" ? true : false;
  document.getElementById("admin-campaign-details-textOverlayN").checked = yn == "N" ? true : false;
  document.getElementById("admin-campaign-details-bg-text").style.display = yn == "Y" ? "block" : "none";
}

function adminToggleDetailsActionBtn(yn) {
  document.getElementById("admin-campaign-details-action-btn-div").style.display = yn == "Y" ? "block" : "none";
  document.getElementById("admin-campaign-details-actionBtnY").checked = yn == "Y" ? true : false;
  document.getElementById("admin-campaign-details-actionBtnN").checked = yn == "N" ? true : false;
  document.getElementById("admin-campaign-action-btn-div").style.display = yn == "Y" ? "block" : "none";
}

function adminCampaignFill(el) {
  var id = el.id.replace("admin-campaign-", "");
  if(id != "description")
    document.getElementById("admin-campaign-bg-"+id).innerHTML = el.value;
  else
    document.getElementById("admin-campaign-details-bg-text").innerHTML = el.value;
}

function adminCampaignFillLink(el) {
  if(el.name == "linkText") {
    document.getElementById("admin-campaign-action-btn-txt").innerHTML = el.value;
  }
  else if(el.name == "linkTextColor") {
    document.getElementById("admin-campaign-action-btn-txt").style.color = el.value;
  }
  else if(el.name == "linkBgColor") {
    document.getElementById("admin-campaign-action-btn").style.backgroundColor = el.value;
  }
  else if(el.name == "ctaLabel") {
    document.getElementById("admin-campaign-cta-txt").innerHTML = el.value;
  }
  else if(el.name == "ctaLabelTextColor") {
    document.getElementById("admin-campaign-cta-txt").style.color = el.value;
  }
  else if(el.name == "ctaLabelBgColor") {
    document.getElementById("admin-campaign-action-btn-div").style.backgroundColor = el.value;
  }
}

function changeTextColor(fontColor) {
  document.getElementById("admin-campaign-textColorWhite").checked = fontColor == "white" ? true : false;
  document.getElementById("admin-campaign-textColorBlack").checked = fontColor == "black" ? true : false;

  document.getElementById("admin-campaign-bg-title").style.backgroundColor = fontColor == "white" ? "black" : "white";
  document.getElementById("admin-campaign-bg-title").style.color = fontColor;
  document.getElementById("admin-campaign-bg-label").style.backgroundColor = fontColor == "white" ? "black" : "white";
  document.getElementById("admin-campaign-bg-label").style.color = fontColor;
}

function changeDetailsTextColor(fontColor) {
  document.getElementById("admin-campaign-details-textColorWhite").checked = fontColor == "white" ? true : false;
  document.getElementById("admin-campaign-details-textColorBlack").checked = fontColor == "black" ? true : false;
  document.getElementById("admin-campaign-details-bg-text").style.backgroundColor = fontColor == "white" ? "black" : "white";
  document.getElementById("admin-campaign-details-bg-text").style.color = fontColor;
}

function changeTextAlign(textAlign) {
  document.getElementById("admin-campaign-textAlignLeft").checked = false;
  document.getElementById("admin-campaign-textAlignCenter").checked = false;
  document.getElementById("admin-campaign-textAlignRight").checked = false;

  if(textAlign == "left") {
    document.getElementById("admin-campaign-textAlignLeft").checked = true;
    document.getElementById("admin-campaign-bg-title").style.marginLeft = "8px";
    document.getElementById("admin-campaign-bg-title").style.marginRight = "auto";
    document.getElementById("admin-campaign-bg-label").style.marginLeft = "8px";
    document.getElementById("admin-campaign-bg-label").style.marginRight = "auto";
  }
  else if(textAlign == "center") {
    document.getElementById("admin-campaign-textAlignCenter").checked = true;
    document.getElementById("admin-campaign-bg-title").style.left = "0";
    document.getElementById("admin-campaign-bg-title").style.right = "0";
    document.getElementById("admin-campaign-bg-label").style.left = "0";
    document.getElementById("admin-campaign-bg-label").style.right = "0";
    document.getElementById("admin-campaign-bg-title").style.margin = "auto";
    document.getElementById("admin-campaign-bg-label").style.margin = "auto";
  }
  else {
    document.getElementById("admin-campaign-textAlignRight").checked = true;
    document.getElementById("admin-campaign-bg-title").style.marginLeft = "auto";
    document.getElementById("admin-campaign-bg-title").style.marginRight = "8px";
    document.getElementById("admin-campaign-bg-label").style.marginLeft = "auto";
    document.getElementById("admin-campaign-bg-label").style.marginRight = "8px";
  }
}

function changeVertAlign(vertAlign) {
  document.getElementById("admin-campaign-vertAlignTop").checked = false;
  document.getElementById("admin-campaign-vertAlignMiddle").checked = false;
  document.getElementById("admin-campaign-vertAlignBottom").checked = false;

  if(vertAlign == "top") {
    document.getElementById("admin-campaign-vertAlignTop").checked = true;
    document.getElementById("admin-campaign-bg-title").style.top = "8px";
    document.getElementById("admin-campaign-bg-label").style.top = "8px";
  }
  else if(vertAlign == "middle") {
    document.getElementById("admin-campaign-vertAlignMiddle").checked = true;
    document.getElementById("admin-campaign-bg-title").style.top = "46px";
    document.getElementById("admin-campaign-bg-label").style.top = "46px";
  }
  else {
    document.getElementById("admin-campaign-vertAlignBottom").checked = true;
    document.getElementById("admin-campaign-bg-title").style.top = "84px";
    document.getElementById("admin-campaign-bg-label").style.top = "84px";
  }
}

function previewPromoImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById("admin-campaign-bg").style.backgroundImage = "url('"+reader.result+"')";
    document.getElementById("admin-campaign-bg").style.backgroundPosition = "center";
    document.getElementById("admin-campaign-bg").style.backgroundSize = "cover";
    document.getElementById("btnRemoveCampaignImg").style.display = "inline-block";
  }
  reader.readAsDataURL(event.target.files[0]);
}

function removePromoImage(event) {
  event.preventDefault();

  var img = document.getElementById("admin-campaign-img-original").value;
  document.getElementById("admin-campaign-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+img+"')";
  document.getElementById("admin-campaign-bg").style.backgroundPosition = "center";
  document.getElementById("admin-campaign-bg").style.backgroundSize = "cover";

  document.getElementById("admin-campaign-img").value = null;
  document.getElementById("btnRemoveCampaignImg").style.display = "none";
}

function previewCampaignDetailsImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById("admin-campaign-details-bg").style.backgroundImage = "url('"+reader.result+"')";
    document.getElementById("admin-campaign-details-bg").style.backgroundPosition = "center";
    document.getElementById("admin-campaign-details-bg").style.backgroundSize = "cover";
    document.getElementById("btnRemoveCampaignDetailsImg").style.display = "inline-block";
  }
  reader.readAsDataURL(event.target.files[0]);
}

function removeCampaignDetailsImage(event) {
  event.preventDefault();

  var img = document.getElementById("admin-campaign-details-img-original").value;
  document.getElementById("admin-campaign-details-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+img+"')";
  document.getElementById("admin-campaign-details-bg").style.backgroundPosition = "center";
  document.getElementById("admin-campaign-details-bg").style.backgroundSize = "cover";

  document.getElementById("admin-campaign-details-img").value = null;
  document.getElementById("btnRemoveCampaignDetailsImg").style.display = "none";
}

function selectPromoType(type) {
  document.getElementById("admin-campaign-features-row").style.display = type == "establishments" ? "block" : "none";
  document.getElementById("admin-campaign-special-features-row").style.display = type == "specials" ? "block" : "none";
  document.getElementById("admin-campaign-keywords-row").style.display = type == "specials" || type == "events" ? "block" : "none";

  if(type == "specials" || type == "events") {
    // don't allow special characters (except spaces) for keywords
    $('.bootstrap-tagsinput input').on('keypress', function(e) {
      var regex = new RegExp("^[0-9a-zA-Z \b]+$");
      var key = String.fromCharCode(e.charCode ? e.charCode : !e.which);
      if (!regex.test(key)) {
         e.preventDefault();
      }
    });
    $('#admin-campaign-keywords').on('beforeItemAdd', function(event) {
      var tags = $('#admin-campaign-keywords').val();
      tags = tags.split(",");
      if(tags.length + 1 > 10)
        event.cancel = true;
    });
  }
}

function load(chosenLocation) {
  $("#claimEmailHtml").load("emails/claim_email.html");

  if(window.location.href.indexOf("/savesupplier") > -1)
    window.history.pushState("", "", '/promos');
  else if(window.location.href.indexOf("/savepromo") > -1)
    window.history.pushState("", "", '/promos?location='+chosenLocation);
  else if(window.location.href.indexOf("/importests") > -1)
    loadMap();
}

function openAddPromoModal() {
  $("#edit-promo-modal").modal("show"); 
  document.getElementById("modal-title").innerHTML = "Add Promo";

  /* LOGISTICS */

  document.getElementById("admin-campaign-promo_id").value = "";

  // campaign type
  document.getElementById("admin-campaign-type").value = "";

  // features
  document.getElementById("admin-campaign-features-row").style.display = "none";
  document.getElementById("admin-campaign-special-features-row").style.display = "none";
  document.getElementById("admin-campaign-patio").checked = false;
  document.getElementById("admin-campaign-rooftop").checked = false;
  document.getElementById("admin-campaign-dogs").checked = false;
  document.getElementById("admin-campaign-brunch").checked = false;
  document.getElementById("admin-campaign-delivery").checked = false;
  document.getElementById("admin-campaign-takeout").checked = false;
  //document.getElementById("admin-campaign-blackowned").checked = false;
  //document.getElementById("naoptions").checked = false;
  document.getElementById("admin-campaign-paying").checked = false;
  document.getElementById("admin-campaign-carryout").checked = false;

  // keywords
  $('#admin-campaign-keywords').tagsinput('removeAll');

  // day(s)
  document.getElementById("admin-campaign-everydayToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-everydayToggleButton").style.color = "black";
  document.getElementById("admin-campaign-monToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-monToggleButton").style.color = "black";
  document.getElementById("admin-campaign-tueToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-tueToggleButton").style.color = "black";
  document.getElementById("admin-campaign-wedToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-wedToggleButton").style.color = "black";
  document.getElementById("admin-campaign-thuToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-thuToggleButton").style.color = "black";
  document.getElementById("admin-campaign-friToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-friToggleButton").style.color = "black";
  document.getElementById("admin-campaign-satToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-satToggleButton").style.color = "black";
  document.getElementById("admin-campaign-sunToggleButton").style.backgroundColor = "white";
  document.getElementById("admin-campaign-sunToggleButton").style.color = "black";
  document.getElementById("admin-campaign-everyday").value = "false";
  document.getElementById("admin-campaign-mon").value = "false";
  document.getElementById("admin-campaign-tue").value = "false";
  document.getElementById("admin-campaign-wed").value = "false";
  document.getElementById("admin-campaign-thu").value = "false";
  document.getElementById("admin-campaign-fri").value = "false";
  document.getElementById("admin-campaign-sat").value = "false";
  document.getElementById("admin-campaign-sun").value = "false";

  // location(s)
  document.getElementById("admin-campaign-location").value = "";

  // active
  document.getElementById("admin-campaign-activeY").checked = false;
  document.getElementById("admin-campaign-activeN").checked = false;


  /* DEAL FEED VIEW */

  // preview
  document.getElementById("admin-campaign-bg").style.backgroundImage = "";
  document.getElementById("admin-campaign-bg-title").innerHTML = "";
  document.getElementById("admin-campaign-bg-label").innerHTML = "";

  // image
  document.getElementById("admin-campaign-img").value = "";
  document.getElementById("admin-campaign-img-original").value = "";

  // text overlay
  document.getElementById("admin-campaign-textOverlayY").checked = false;
  document.getElementById("admin-campaign-textOverlayN").checked = false;
  document.getElementById("admin-campaign-text-overlay-div").style.display = "none";

  // title
  document.getElementById("admin-campaign-title").value = "";

  // label
  document.getElementById("admin-campaign-label").value = "";
  
  // text color
  document.getElementById("admin-campaign-textColorWhite").checked = false;
  document.getElementById("admin-campaign-textColorBlack").checked = false;

  // horizontal align
  document.getElementById("admin-campaign-textAlignLeft").checked = false;
  document.getElementById("admin-campaign-textAlignCenter").checked = false;
  document.getElementById("admin-campaign-textAlignRight").checked = false;

  // vertical align
  document.getElementById("admin-campaign-vertAlignTop").checked = false;
  document.getElementById("admin-campaign-vertAlignMiddle").checked = false;
  document.getElementById("admin-campaign-vertAlignBottom").checked = false;


  /* DETAILS VIEW */

  // preview
  document.getElementById("admin-campaign-details-bg").style.backgroundImage = "";
  document.getElementById("admin-campaign-details-bg-text").innerHTML = "";

  // image
  document.getElementById("admin-campaign-details-img").value = "";
  document.getElementById("admin-campaign-details-img-original").value = "";
  
  // text overlay
  document.getElementById("admin-campaign-details-textOverlayY").checked = false;
  document.getElementById("admin-campaign-details-textOverlayN").checked = false;
  document.getElementById("admin-campaign-details-text-overlay-div").style.display = "none";

  // description
  document.getElementById("admin-campaign-description").value = "";
  
  // text color
  document.getElementById("admin-campaign-details-textColorWhite").checked = false;
  document.getElementById("admin-campaign-details-textColorBlack").checked = false;
}

function adminValidateCampaign(e) {
  e.preventDefault();
  var btnSave = document.getElementById("btnSaveCampaign");
  btnSave.disabled = true;
  btnSave.innerHTML = "Saving...";
  document.body.style.cursor = "wait";

  document.getElementById("admin-campaign-form").submit();
}

function promoApprove(approve) {
  if(approve) {
    swal({
      title: "Approve Campaign",
      text: "Are you sure you want to approve this campaign?",
      icon: "success",
      dangerMode: false,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, approve it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
      }
    })
    .then((willDo) => {
      if(willDo) {
        var parameters = { 
          promoId: document.getElementById("view-promo-id").value
        };

        $.post('/adminapprovepromo', parameters, function(data) {
          window.location.href = "/promos";
        });
      }
    });
  }
  else {
    swal({
      title: "Deny Campaign",
      text: "Are you sure you want to deny this campaign?",
      icon: "error",
      dangerMode: true,
      content: {
        element: "input",
        attributes: {
          placeholder: "Reason for denial",
          type: "text",
        }
      },
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, deny it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
      }
    })
    .then((willDo) => {
      if(willDo) {
        var parameters = { 
          promoId: document.getElementById("view-promo-id").value,
          reason: willDo
        };

        $.post('/admindenypromo', parameters, function(data) {
          window.location.href = "/promos";
        });
      }
    });
  }
}

function promoDropdown(promoId, e) {
  e.preventDefault();

  // close open menus
  var openMenus = document.querySelectorAll('.dropdown-content');
  openMenus.forEach(function(menus) {
    menus.classList.remove('show');
  }); 

  // open new menu
  document.getElementById("promoDropdown"+promoId).classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn')) {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
      var openDropdown = dropdowns[i];
      if (openDropdown.classList.contains('show')) {
        openDropdown.classList.remove('show');
      }
    }
  }
}




function sendClaimEmail(e) {
  e.preventDefault();

  var parameters = { 
    email: document.getElementById("email").value
    //claimEmail: $('#claimEmailHtml').html() 
  };

  $.get('/sendclaimemail', parameters, function(data) {
    location.reload();
  });
}

function adminToggleAddDailyDealDiv(show) {
  if(show) {
    document.getElementById("addDailyDealDiv").style.display = "block";
  } else {
    document.getElementById("addDailyDealDiv").style.display = "none";
  }
}

function adminToggleAddExclusiveDealDiv(show) {
  if(show) {
    document.getElementById("addExclusiveDealDiv").style.display = "block";
  } else {
    document.getElementById("addExclusiveDealDiv").style.display = "none";
  }
}

function adminPreviewImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById('admin-edit-profile-picture').src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}

function adminToggleDailyPriceType(type) {
  document.getElementById("admin-daily-price").value = "";

  if (type == 'money') {
    adminDailyPriceType = "$";
    adminDailyPriceTypeId = 300;
    document.getElementById("admin-daily-price").disabled = false;
    document.getElementById("admin-daily-moneyToggleButton").className = "btn btn-primary active";
    document.getElementById("admin-daily-percentToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-bogoToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-freeToggleButton").className = "btn btn-default";
  }
  else if (type == 'percent') {
    adminDailyPriceType = "%";
    adminDailyPriceTypeId = 301;
    document.getElementById("admin-daily-price").disabled = false;
    document.getElementById("admin-daily-moneyToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-percentToggleButton").className = "btn btn-primary active";
    document.getElementById("admin-daily-bogoToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-freeToggleButton").className = "btn btn-default";
  } else if (type == 'bogo'){
    adminDailyPriceType = "BOGO";
    adminDailyPriceTypeId = 302;
    document.getElementById("admin-daily-price").disabled = false;
    document.getElementById("admin-daily-moneyToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-percentToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-bogoToggleButton").className = "btn btn-primary active";
    document.getElementById("admin-daily-freeToggleButton").className = "btn btn-default";
  } else if (type == 'free'){
    adminDailyPriceType = "Free";
    adminDailyPriceTypeId = 307;
    document.getElementById("admin-daily-price").disabled = false;
    document.getElementById("admin-daily-moneyToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-percentToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-bogoToggleButton").className = "btn btn-default";
    document.getElementById("admin-daily-freeToggleButton").className = "btn btn-primary active";
  }

  document.getElementById("admin-daily-price").placeholder = adminDailyPriceType;
  document.getElementById("adminDailyPriceType").value = adminDailyPriceTypeId;
}

function adminValidateOwner(e) {
  e.preventDefault();

  var parameters = {
    ownerId:    document.getElementById('ownerHidden').value,
    email:      document.getElementById('email').value,
    username:   document.getElementById('username').value,
    phone:      document.getElementById('phoneNumber').value,
    delete:     false
  };

  $.get('/validateowner', parameters, function(err) {
    if(err.length == 0) {
      document.getElementById("owner-form").submit();
    }
    else {
      var errs = "";
      for(var i = 0; i < err.length; i++) {
        errs += err[i] + "<br/>";
      }
      document.getElementById("ownerErrs").innerHTML = errs;
    }
  });
}

function adminValidateOwnerDelete(e) {
  e.preventDefault();

  var parameters = {
    ownerId:    document.getElementById('ownerHidden').value,
    delete:     true
  };

  $.get('/validateowner', parameters, function(err) {
    if(err.length == 0) {
      //$('#btnDeleteOwner').on('click', function(e) {
        //e.preventDefault();
        if (confirm("Are you sure you want to delete this owner?")) {
          $("#owner-form").attr("action", "/deleteowner");
          $("#owner-form").submit();
        }
      //});
      //document.getElementById("owner-form").submit();
    }
    else {
      var errs = "";
      for(var i = 0; i < err.length; i++) {
        errs += err[i] + "<br/>";
      }
      document.getElementById("ownerErrs").innerHTML = errs;
    }
  });
}

function adminValidateEstablishment(e) {
  e.preventDefault();

  var validImg = false;
  var imgSrc = document.getElementById('admin-edit-profile-picture').src;

  if(imgSrc.indexOf("est-img") > -1 || imgSrc.indexOf("data:image") > -1)
    validImg = true;

  var parameters = {
    validImg:   validImg,
    name:       document.getElementById('establishment').value,
    email:      document.getElementById('email').value,
    username:   document.getElementById('username').value,
    phone:      document.getElementById('phoneNumber').value,
    address1:   document.getElementById('line1').value,
    address2:   document.getElementById('line2').value,
    city:       document.getElementById('city').value,
    state:      document.getElementById('state').value,
    zip:        document.getElementById('zip').value,
    topic:      document.getElementById('topic').value,
    needsToPay: document.getElementById('needsToPay').value,
    showInApp:  document.getElementById('showInApp').value,
    //twitter:    document.getElementById('twitter').value,
    //facebook:   document.getElementById('facebook').value,
    //instagram:  document.getElementById('instagram').value,
    //snapchat:   document.getElementById('snapchat').value,
    monOpen:    document.getElementById('monOpen').value,
    monClose:   document.getElementById('monClose').value,
    tueOpen:    document.getElementById('tueOpen').value,
    tueClose:   document.getElementById('tueClose').value,
    wedOpen:    document.getElementById('wedOpen').value,
    wedClose:   document.getElementById('wedClose').value,
    thuOpen:    document.getElementById('thuOpen').value,
    thuClose:   document.getElementById('thuClose').value,
    friOpen:    document.getElementById('friOpen').value,
    friClose:   document.getElementById('friClose').value,
    satOpen:    document.getElementById('satOpen').value,
    satClose:   document.getElementById('satClose').value,
    sunOpen:    document.getElementById('sunOpen').value,
    sunClose:   document.getElementById('sunClose').value
  };

  $.get('/validateest', parameters, function(err) {
    if(err.length == 0) {
      document.getElementById("est-info-form").submit();
    }
    else {
      var errs = "";
      for(var i = 0; i < err.length; i++) {
        errs += err[i] + "<br/>";
      }
      document.getElementById("estErrs").innerHTML = errs;
    }
  });
}

function adminValidateExclusiveSpecial(e) {
  e.preventDefault();

  var errorMsgs = [];

  var price = document.getElementById("adminPrice").value;
  var priceType = document.getElementById("adminPriceType").value;
  var details = document.getElementById("exclusive-details").value;
  var hours = document.getElementById("exclusive-hr").value;
  var minutes = document.getElementById("exclusive-min").value;
  var type = document.getElementById("exclusive-type").value;

  if(priceType == "300") {
    if (price == undefined || price == "") {
      errorMsgs.push("add a price!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Price needs to be less than $100!");
    }
  }
  else if(priceType == "301") {
    if(price == undefined || price == "") {
      errorMsgs.push("add a percent off!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Percent off needs to be equal or less than 100%!");
    }
  }

  if(details == undefined || details == "") {
    errorMsgs.push("add some details!");
  }

  if(hours == "0" && minutes == "0") {
    errorMsgs.push("set the duration!");
  }

  if(type == undefined || type == "") {
    errorMsgs.push("select a type!");
  }

  if(errorMsgs.length == 0) {
    document.getElementById("admin-add-exclusive-special-form").submit();
  }
  else {
    var errs = "";
    for(var i = 0; i < errorMsgs.length; i++) {
      errs += errorMsgs[i] + "<br/>";
    }
    document.getElementById("exclusiveSpecialErrors").innerHTML = errs;
  }
}

function adminValidateDailySpecial(e) {
  e.preventDefault();

  var errorMsgs = [];

  var price = document.getElementById("admin-daily-price").value;
  var priceType = document.getElementById("adminDailyPriceType").value;
  var details = document.getElementById("daily-details").value;
  var start = document.getElementById("daily-start").value;
  var end = document.getElementById("daily-end").value;
  var type = document.getElementById("daily-type").value;

  if(!document.getElementById("daily-day-mon").checked &&
     !document.getElementById("daily-day-tue").checked &&
     !document.getElementById("daily-day-wed").checked &&
     !document.getElementById("daily-day-thu").checked &&
     !document.getElementById("daily-day-fri").checked &&
     !document.getElementById("daily-day-sat").checked &&
     !document.getElementById("daily-day-sun").checked) {
    errorMsgs.push("select at least one day!");
  }

  if(priceType == "300") {
    if (price == undefined || price == "") {
      errorMsgs.push("add a price!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Price needs to be less than $100!");
    }
  }
  else if(priceType == "301") {
    if(price == undefined || price == "") {
      errorMsgs.push("add a percent off!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Percent off needs to be equal or less than 100%!");
    }
  }

  if(details == undefined || details == "") {
    errorMsgs.push("add some details!");
  }

  if(start == undefined || start == "") {
    errorMsgs.push("set a start time!");
  }

  if(end == undefined || end == "") {
    errorMsgs.push("set an end time!");
  }

  if(start != undefined && start != "" && end != undefined && end != "") {
    var sDate = new Date("01-01-2000 " + start);
    var eDate = new Date("01-01-2000 " + end);

    if(eDate.getHours() < 4)
      eDate.setDate(eDate.getDate()+1);

    if(sDate >= eDate)
      errorMsgs.push("Make sure start time is before end time!");
  }

  if(type == undefined || type == "") {
    errorMsgs.push("select a type!");
  }

  if(errorMsgs.length == 0) {
    document.getElementById("admin-add-daily-special-form").submit();
  }
  else {
    var errs = "";
    for(var i = 0; i < errorMsgs.length; i++) {
      errs += errorMsgs[i] + "<br/>";
    }
    document.getElementById("dailySpecialErrors").innerHTML = errs;
  }
}




function daysBetween(a, b) {
  var aa = new Date(a);
  var bb = new Date(b);

  var diffMs = Math.abs(bb - aa); 
  var diffDays = Math.floor(diffMs / (1000 * 60 * 60 * 24)); 

  return diffDays;
}








/*** SPECIALS ***/


// TOGGLE SPECIALS

function adminToggleWeeklyOneTime(weeklyOrOneTime) {
  if(weeklyOrOneTime == 'weekly') {
    document.getElementById("admin-toggle-weekly").className = "btn btn-primary active";
    document.getElementById("admin-toggle-onetime").className = "btn btn-default";
    document.getElementById("weekly-row").style.display = "block";
    document.getElementById("one-time-row").style.display = "none";

  }
  else {
    document.getElementById("admin-toggle-weekly").className = "btn btn-default";
    document.getElementById("admin-toggle-onetime").className = "btn btn-primary active";
    document.getElementById("weekly-row").style.display = "none";
    document.getElementById("one-time-row").style.display = "block";
  }

  document.getElementById("weekly-or-exclusive").value = weeklyOrOneTime;
}

function adminToggleNowSchedule(nowOrSchedule) {
  if(nowOrSchedule == 'now') {
    document.getElementById("admin-toggle-now").className = "btn btn-primary active";
    document.getElementById("admin-toggle-schedule").className = "btn btn-default";
    document.getElementById("schedule-row").style.display = "none";
  }
  else {
    document.getElementById("admin-toggle-now").className = "btn btn-default";
    document.getElementById("admin-toggle-schedule").className = "btn btn-primary active";
    document.getElementById("schedule-row").style.display = "inline-block";
  }
}

function adminToggleDailyDay(day) {
  if(document.getElementById("daily-day-"+day).className == "btn btn-default") {
    document.getElementById("daily-day-"+day).className = "btn btn-primary active";
    document.getElementById("dailyDay"+day).value = "true";
  }
  else {
    document.getElementById("daily-day-"+day).className = "btn btn-default";
    document.getElementById("dailyDay"+day).value = "false";
  }
}

function adminTogglePriceType(type) {
  document.getElementById("admin-price").value = "";

  if (type == 'money') {
    adminPriceType = "$";
    adminPriceTypeId = 300;
    document.getElementById("admin-price").disabled = false;
    document.getElementById("adminMoneyToggleButton").className = "btn btn-primary active";
    document.getElementById("adminPercentToggleButton").className = "btn btn-default";
    document.getElementById("adminBogoToggleButton").className = "btn btn-default";
    document.getElementById("adminFreeToggleButton").className = "btn btn-default";
  }
  else if (type == 'percent') {
    adminPriceType = "%";
    adminPriceTypeId = 301;
    document.getElementById("admin-price").disabled = false;
    document.getElementById("adminMoneyToggleButton").className = "btn btn-default";
    document.getElementById("adminPercentToggleButton").className = "btn btn-primary active";
    document.getElementById("adminBogoToggleButton").className = "btn btn-default";
    document.getElementById("adminFreeToggleButton").className = "btn btn-default";
  } else if (type == 'bogo'){
    adminPriceType = "BOGO";
    adminPriceTypeId = 302;
    document.getElementById("admin-price").disabled = true;
    document.getElementById("adminMoneyToggleButton").className = "btn btn-default";
    document.getElementById("adminPercentToggleButton").className = "btn btn-default";
    document.getElementById("adminBogoToggleButton").className = "btn btn-primary active";
    document.getElementById("adminFreeToggleButton").className = "btn btn-default";
  } else if (type == 'free'){
    adminPriceType = "Free";
    adminPriceTypeId = 307;
    document.getElementById("admin-price").disabled = true;
    document.getElementById("adminMoneyToggleButton").className = "btn btn-default";
    document.getElementById("adminPercentToggleButton").className = "btn btn-default";
    document.getElementById("adminBogoToggleButton").className = "btn btn-default";
    document.getElementById("adminFreeToggleButton").className = "btn btn-primary active";
  }

  document.getElementById("admin-price").placeholder = adminPriceType;
  document.getElementById("admin-price-type").value = adminPriceTypeId;
}

function adminToggleAddSpecial(e) {
  if(e)
    e.preventDefault();

  if(document.getElementById("addSpecialDiv").style.display == "none") {
    document.getElementById("addSpecialDiv").style.display = "block";
    document.getElementById("iToggleAddSpecial").className = "fa fa-minus";
    document.getElementById("iToggleAddSpecial").style.color = "rgb(200, 20, 40)";
  }
  else {
    document.getElementById("addSpecialDiv").style.display = "none";
    document.getElementById("iToggleAddSpecial").className = "fa fa-plus";
    document.getElementById("iToggleAddSpecial").style.color = "#337ab7";
  }
}

function adminToggleSpecialType(typeId) {
  document.getElementById("admin-special-type").value = typeId;

  for(var i = 0; i <= 15; i++) {
    if(i == typeId) {
      document.getElementById("admin-special-type-"+i).style.border = "2px solid orange";
      document.getElementById("admin-special-type-"+i).style.boxShadow = "0px 1px 10px grey";
    }
    else {
      document.getElementById("admin-special-type-"+i).style.border = "1px solid #ccc";
      document.getElementById("admin-special-type-"+i).style.boxShadow = "none";
    } 
  }
}


// VALIDATE SPECIAL

function adminValidateAddSpecial(e) {
  e.preventDefault();

  var errorMsgs = [];

  // common fields
  var price = document.getElementById("admin-price").value;
  var priceType = document.getElementById("admin-price-type").value;
  var details = document.getElementById("admin-details").value;
  var specialType;
  for(var i = 0; i <= 15; i++) {
    if(document.getElementById("admin-special-type-"+i).style.border == "2px solid orange") {
      specialType = i;
    }
  }

  if(priceType == "300") {
    if (price == undefined || price == "") {
      errorMsgs.push("Add a price!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Price needs to be less than $100!");
    }
  }
  else if(priceType == "301") {
    if(price == undefined || price == "") {
      errorMsgs.push("add a percent off!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Percent off needs to be equal or less than 100%!");
    }
  }
  else if(priceType == undefined || priceType == "") {
    errorMsgs.push("Select a price type and enter a price!");
  }

  if(details == undefined || details == "") {
    errorMsgs.push("Add some details!");
  }

  if(specialType == undefined) {
    errorMsgs.push("Select a special type!");
  }


  // weekly
  if(document.getElementById("weekly-or-exclusive").value == "weekly") {
    var startTime = document.getElementById("daily-start").value;
    var endTime = document.getElementById("daily-end").value;
    var mon = document.getElementById("daily-day-mon").className == "btn btn-primary active" ? true : false;
    var tue = document.getElementById("daily-day-tue").className == "btn btn-primary active" ? true : false;
    var wed = document.getElementById("daily-day-wed").className == "btn btn-primary active" ? true : false;
    var thu = document.getElementById("daily-day-thu").className == "btn btn-primary active" ? true : false;
    var fri = document.getElementById("daily-day-fri").className == "btn btn-primary active" ? true : false;
    var sat = document.getElementById("daily-day-sat").className == "btn btn-primary active" ? true : false;
    var sun = document.getElementById("daily-day-sun").className == "btn btn-primary active" ? true : false;

    if(!mon && !tue && !wed && !thu && !fri && !sat && !sun) {
      errorMsgs.push("Select at least one day!");
    }

    if(startTime == undefined || startTime == "") {
      errorMsgs.push("Set a start time!");
    }

    if(endTime == undefined || endTime == "") {
      errorMsgs.push("Set an end time!");
    }

    if(startTime != undefined && startTime != "" && endTime != undefined && endTime != "") {
      var sDate = new Date("01-01-2000 " + startTime);
      var eDate = new Date("01-01-2000 " + endTime);

      if(eDate.getHours() < 4)
        eDate.setDate(eDate.getDate()+1);

      if(sDate >= eDate)
        errorMsgs.push("Start time needs to be before end time!");
    }
  }
  // exclusive
  else {
    var start = document.getElementById("exclusive-start").value;
    var hours = document.getElementById("exclusive-hr").value;
    var minutes = document.getElementById("exclusive-min").value;

    if(start != undefined && start != "") {
      var sDate = new Date(start);
      var now = new Date();

      if(sDate.getTime() < now.getTime())
        errorMsgs.push("Special needs to start in the future!");
    }
    else if(document.getElementById("exclusive-start").style.display == "none" && (start == undefined || start == "")) {
      errorMsgs.push("Set the start date/time!");
    }

    if(hours == "0" && minutes == "0") {
      errorMsgs.push("Set the duration!");
    }
  }

  if(errorMsgs.length == 0) {
    document.getElementById("addSpecialForm").submit();
  }
  else {
    var errs = "";
    for(var i = 0; i < errorMsgs.length; i++) {
      errs += errorMsgs[i] + "<br/>";
    }
    document.getElementById("addSpecialErrors").innerHTML = errs;
  }
}


// DELETE SPECIAL

function adminDeleteSpecial(sid) {
  document.getElementById("delSpecialId").value = sid;
  document.getElementById("exclusiveSpecialsForm").submit();
}

function adminDeleteDailySpecial(sid) {
  document.getElementById("delDailySpecialId").value = sid;
  document.getElementById("dailySpecialsForm").submit();
}



/*** EVENTS ***/


// TOGGLE EVENTS

function adminToggleAddEvent(e) {
  if(e)
    e.preventDefault();

  if(document.getElementById("addEventDiv").style.display == "none") {
    document.getElementById("addEventDiv").style.display = "block";
    document.getElementById("iToggleAddEvent").className = "fa fa-minus";
    document.getElementById("iToggleAddEvent").style.color = "rgb(200, 20, 40)";
  }
  else {
    document.getElementById("addEventDiv").style.display = "none";
    document.getElementById("iToggleAddEvent").className = "fa fa-plus";
    document.getElementById("iToggleAddEvent").style.color = "#337ab7";
  }
}

function adminToggleWeeklyEventDay(day) {
  if(document.getElementById("weekly-event-day-"+day).className == "btn btn-default") {
    document.getElementById("weekly-event-day-"+day).className = "btn btn-primary active";
    document.getElementById("weeklyEventDay"+day).value = "true";
  }
  else {
    document.getElementById("weekly-event-day-"+day).className = "btn btn-default";
    document.getElementById("weeklyEventDay"+day).value = "false";
  }
}

function adminToggleWeeklyOneTimeEvent(weeklyOrOneTime) {
  if(weeklyOrOneTime == 'weekly') {
    document.getElementById("admin-toggle-weekly-event").className = "btn btn-primary active";
    document.getElementById("admin-toggle-onetime-event").className = "btn btn-default";
    document.getElementById("weekly-events-row").style.display = "block";
    document.getElementById("one-time-events-row").style.display = "none";

  }
  else {
    document.getElementById("admin-toggle-weekly-event").className = "btn btn-default";
    document.getElementById("admin-toggle-onetime-event").className = "btn btn-primary active";
    document.getElementById("weekly-events-row").style.display = "none";
    document.getElementById("one-time-events-row").style.display = "block";
  }

  document.getElementById("weekly-or-exclusive-event").value = weeklyOrOneTime;
}

function adminToggleAddEventSpecialDiv() {
  var display = document.getElementById("adminAddEventSpecialRow").style.display;

  // show
  if(display == "none" || display == "") {
    document.getElementById("adminAddEventSpecialRow").style.display = "block";
    document.getElementById("adminAddEventSpecialToggleMinus").style.display = "inline";
    document.getElementById("adminAddEventSpecialTogglePlus").style.display = "none";

    var startDate = document.getElementById("admin-edit-event-start-date").value;
    var endDate = document.getElementById("admin-edit-event-end-date").value;

    if(startDate != null && startDate != "" && endDate != null && endDate != "") {
      var dayToggles = '';
      startDate = new Date(startDate);
      endDate = new Date(endDate);

      startDate.addHoursDST();
      endDate.addHoursDST();

      while(startDate <= endDate) {
        var id = ""+(startDate.getMonth()+1)+startDate.getDate();
        dayToggles = dayToggles + 
             '<div class="btn-group">'+
                '<button id="day-'+id+'" type="button" style="text-decoration: none" class="btn btn-default" onclick="adminToggleAddEventSpecialDay('+id+')">'+(startDate.getMonth()+1)+'/'+startDate.getDate()+'</button>'+
               '</div>'+
               '<input id="day-'+id+'-hidden" type="hidden" value="'+startDate+'" />';
        startDate.setDate(startDate.getDate()+1);
      }

      document.getElementById("adminAddEventSpecial-dayToggle").innerHTML = dayToggles;
      document.getElementById("multidayDiv").style.display = "block";
    }
    else {
      document.getElementById("multidayDiv").style.display = "none";
    }
  }
  // hide
  else {
    document.getElementById("adminAddEventSpecialRow").style.display = "none";
    document.getElementById("adminAddEventSpecialToggleMinus").style.display = "none";
    document.getElementById("adminAddEventSpecialTogglePlus").style.display = "inline";
  }
}


// VALIDATE EVENT

function adminValidateAddEvent(e) {
  e.preventDefault();

  var errorMsgs = [];

  // common fields
  var title = document.getElementById("event-title").value;
  var details = document.getElementById("event-details").value;

  if(title == undefined || title == "") {
    errorMsgs.push("Add a title!");
  }

  if(details == undefined || details == "") {
    errorMsgs.push("Add some details!");
  }

  // weekly
  if(document.getElementById("weekly-or-exclusive-event").value == "weekly") {
    var startTime = document.getElementById("weekly-event-start").value;
    var endTime = document.getElementById("weekly-event-end").value;
    var mon = document.getElementById("weekly-event-day-mon").className == "btn btn-primary active" ? true : false;
    var tue = document.getElementById("weekly-event-day-tue").className == "btn btn-primary active" ? true : false;
    var wed = document.getElementById("weekly-event-day-wed").className == "btn btn-primary active" ? true : false;
    var thu = document.getElementById("weekly-event-day-thu").className == "btn btn-primary active" ? true : false;
    var fri = document.getElementById("weekly-event-day-fri").className == "btn btn-primary active" ? true : false;
    var sat = document.getElementById("weekly-event-day-sat").className == "btn btn-primary active" ? true : false;
    var sun = document.getElementById("weekly-event-day-sun").className == "btn btn-primary active" ? true : false;

    if(!mon && !tue && !wed && !thu && !fri && !sat && !sun) {
      errorMsgs.push("Select at least one day!");
    }

    if(startTime == undefined || startTime == "") {
      errorMsgs.push("Set a start time!");
    }

    if(endTime == undefined || endTime == "") {
      errorMsgs.push("Set an end time!");
    }

    if(startTime != undefined && startTime != "" && endTime != undefined && endTime != "") {
      var sDate = new Date("01-01-2000 " + startTime);
      var eDate = new Date("01-01-2000 " + endTime);

      if(eDate.getHours() < 4)
        eDate.setDate(eDate.getDate()+1);

      if(sDate >= eDate) {
        errorMsgs.push("Start time needs to be before end time!");
      }
    }
  }
  // exclusive
  else {
    var start = document.getElementById("one-time-event-start").value;
    var end = document.getElementById("one-time-event-end").value;

    if(start == undefined || start == "") {
      errorMsgs.push("Set a start date/time!");
    }

    if(end == undefined || end == "") {
      errorMsgs.push("Set an end date/time!");
    }

    if(start != undefined && start != "" && end != undefined && end != "") {
      var sDate = new Date(start);
      var eDate = new Date(end);
      var now = new Date();

      if(sDate.getTime() < now.getTime()) {
        errorMsgs.push("Event needs to start in the future!");
      }
      else if(sDate.getTime() >= eDate.getTime()) {
        errorMsgs.push("End time needs to be greater than start time!");
      }
      else if(daysBetween(sDate, eDate) > 7) {
        errorMsgs.push("Event can only last up to 7 days!");
      }
    }
  }

  if(errorMsgs.length == 0) {
    document.getElementById("addEventForm").submit();
  }
  else {
    var errs = "";
    for(var i = 0; i < errorMsgs.length; i++) {
      errs += errorMsgs[i] + "<br/>";
    }
    document.getElementById("addEventErrors").innerHTML = errs;
  }
}


// DELETE EVENT

function adminDeleteEvent(eventId) {
  swal({
      title: "Delete Event",
      text: "Are you sure you want to delete this event?",
      icon: "warning",
      dangerMode: true,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, delete it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
    }
  })
  .then((willDelete) => {
      if (willDelete) {
        var parameters = {
          eventId: eventId,
          estId: document.getElementById("estId").value
        };

        $.post('/admindeleteevent', parameters, function(err) {
          location.reload();
        });
      } 
      else {
        return false;
      }
  });
}


// EDIT EVENT

function adminEditEvent(e, eventId, current) {
  e.preventDefault();

  var parameters = {
    eventId: eventId
  };

  document.getElementById("adminAddEventSpecialToggleMinus").style.display = "none";
  document.getElementById("adminAddEventSpecialRow").style.display = "none";

  if(current) {
    document.getElementById("btnSaveAdminEvent").style.display = "none";
    document.getElementById("admin-edit-event-start-date").disabled = true;
    document.getElementById("admin-edit-event-end-date").disabled = true;
    document.getElementById("admin-edit-event-start-time").disabled = true;
    document.getElementById("admin-edit-event-end-time").disabled = true;
    document.getElementById("admin-edit-event-details").disabled = true;
    document.getElementById("adminAddEventSpecialTogglePlus").style.display = "none";
  }
  else {
    document.getElementById("btnSaveAdminEvent").style.display = "inline-block";
    document.getElementById("admin-edit-event-start-date").disabled = false;
    document.getElementById("admin-edit-event-end-date").disabled = false;
    document.getElementById("admin-edit-event-start-time").disabled = false;
    document.getElementById("admin-edit-event-end-time").disabled = false;
    document.getElementById("admin-edit-event-details").disabled = false;
    document.getElementById("adminAddEventSpecialTogglePlus").style.display = "inline-block";
  }

  $.get('/loadevent', parameters, function(data) {
    clearAdminEditEventModal();
    document.getElementById("admin-edit-event-ids").value = data.event_ids;
    document.getElementById("admin-edit-event-days").value = data.days;
    document.getElementById("admin-edit-event-title").value = data.title;
    document.getElementById("admin-edit-event-title-hidden").value = data.title;
    document.getElementById("admin-edit-event-details").value = data.details;
    document.getElementById("admin-edit-event-origDetails").value = data.details;

    //var startTime = getISOString(new Date(data.start_time));
    //var endTime = getISOString(new Date(data.end_time));

    var startTime = new Date(data.start_time);
    var endTime = new Date(data.end_time);

    startTime.addHoursDST();
    endTime.addHoursDST();

    //if(data.day != -1) {
    //  startTime.setHours(startTime.getHours()+1); // UTC issue
    //  endTime.setHours(endTime.getHours()+1); // UTC issue
    //}

    startTime = getISOString(startTime);
    endTime = getISOString(endTime);

    if(data.day == -1) {
      // start/end datetime or time
      document.getElementById("admin-edit-event-start-date").value = startTime.substring(0, startTime.indexOf("T"));
      document.getElementById("admin-edit-event-end-date").value = endTime.substring(0, endTime.indexOf("T"));
      startTime = startTime.substring(startTime.indexOf("T")+1);
      endTime = endTime.substring(endTime.indexOf("T")+1);
      document.getElementById("admin-edit-event-start-time").value = startTime;
      document.getElementById("admin-edit-event-end-time").value = endTime;
      document.getElementById("start-date-div").style.display = "block";
      document.getElementById("end-date-div").style.display = "block";

      //document.getElementById("title-div").className = "col col-md-12 col-sm-12 col-xs-12";
      //document.getElementById("start-time-div").className = "col col-md-12 col-sm-6 col-xs-6";
      //document.getElementById("end-time-div").className = "col col-md-12 col-sm-6 col-xs-6";

      // day
      document.getElementById("admin-edit-day-div").style.display = "none"
    }
    else {
      // start/end datetime or time
      startTime = startTime.substring(startTime.indexOf("T")+1);
      endTime = endTime.substring(endTime.indexOf("T")+1);
      document.getElementById("admin-edit-event-start-time").value = startTime;
      document.getElementById("admin-edit-event-end-time").value = endTime;
      document.getElementById("start-date-div").style.display = "none";
      document.getElementById("end-date-div").style.display = "none";

      //document.getElementById("title-div").className = "col col-md-4 col-sm-12 col-xs-12";
      //document.getElementById("start-time-div").className = "col col-md-4 col-sm-6 col-xs-6";
      //document.getElementById("end-time-div").className = "col col-md-4 col-sm-6 col-xs-6";

      // days
      var dayArr = data.days.split(",");
      for(var i = 0; i < 7; i++) {
        var selected = false;
        for(var d in dayArr) {
          if(dayArr[d] == i) {
            selected = true;
            selectAdminEditEventDay(intDayToStringAbbr(dayArr[d]).toLowerCase(), true);
            break;
          }
        }
        if(!selected) {
          selectAdminEditEventDay(intDayToStringAbbr(i).toLowerCase(), false, true);
        }
      }
      document.getElementById("admin-edit-day-div").style.display = "block";
    }

    var html = "";

    for(var s in data.specials) {
      var special = data.specials[s];

      if(data.day == -1) {
        var specialDates = "";
        var specialDatesArr = special.special_dates.split(",");

        var daysBetween = 0;
        var startDate = new Date(data.start_time);
        var endDate = new Date(data.end_time);

        while(startDate <= endDate) {
          daysBetween += 1;
          startDate.setDate(startDate.getDate()+1);
        }

        if(daysBetween == specialDatesArr.length) {
          specialDates = "All Days";
        }
        else {
          for(var sd = 0; sd < specialDatesArr.length; sd++) {
            var sdDate = new Date(specialDatesArr[sd]);
            sdDate.addHoursDST();
            specialDates = specialDates + (sdDate.getMonth()+1) + "/" + sdDate.getDate();
            if(sd < specialDatesArr.length-1)
              specialDates = specialDates + ", ";
          }
        }

        var sStart = new Date(special.start_time);
        sStart.addHoursDST();
        sStart = formatStartEndTime(sStart);
        var sEnd = new Date(special.end_time);
        sEnd.addHoursDST();
        sEnd = formatStartEndTime(sEnd);

        html = html + "<div class='row' id='adminEventSpecialRow"+special.special_id+"' style='margin-left:0px; margin-right:0px;'>"+
                   "<div class='col col-md-5 col-sm-5 col-xs-5' style='line-height:1.5;'>"+
                  "<img src='assets/img/"+getDealTypeImageByRefId(special.deal_type_ref_id)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;' />"+
                   "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>";

        if(special.price_type_ref_id == 300) {
          html = html + "$" + special.price.toFixed(2) + " " + special.details;
        }
        else if(special.price_type_ref_id == 301) {
          html = html + special.price.toFixed(0) + "% Off " + special.details;
        }
        else if(special.price_type_ref_id == 302) {
          html = html + "BOGO " + special.details;
        }
        else if(special.price_type_ref_id == 307) {
          html = html + "Free " + special.details;
        }

        html = html + "</p>"+
               "</div>"+
               "<div class='col col-md-2 col-sm-2 col-xs-2' style='line-height:1.5;'>"+
                  "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+specialDates+"</p>"+
                 "</div>"+
                 "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5;'>"+
                  "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>" + sStart + " - " + sEnd + "</p>"+
                 "</div>"+
               "<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
                "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                 "<span style='cursor:pointer'>"+
                  "<a onclick='adminDeleteSpecialFromEvent("+special.special_id+")' data-toggle='tooltip' title='Delete Special'>"+
                   "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                  "</a>"+
                 "</span>"+
                "</p>"+
                 "</div>"+
                "</div>";
      }
      else {
        html = html+"<div class='row' id='adminEventSpecialRow"+special.special_id+"' style='margin-left:0px; margin-right:0px;'>"+
                    "<div class='col col-md-12' style='line-height:1.5;'>"+
                 "<img src='assets/img/"+getDealTypeImageByRefId(special.deal_type_ref_id)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
                  "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>";

                  if(special.price_type_ref_id == 300) {
                    html = html + "$" + special.price.toFixed(2) + " " + special.details;
                  }
                  else if(special.price_type_ref_id == 301) {
                    html = html + special.price.toFixed(0) + "% Off " + special.details;
                  }
                  else if(special.price_type_ref_id == 302) {
                    html = html + "BOGO " + special.details;
                  }
                  else if(special.price_type_ref_id == 307) {
                    html = html + "Free " + special.details;
                  }

        html = html+"</p>"+
                 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                  "<span style='cursor:pointer'>"+
                   "<a id='deleteAdminEventSpecial"+special.special_id+"' onclick='adminDeleteSpecialFromEvent("+special.special_id+")' data-toggle='tooltip' title='Delete'>"+
                    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                   "</a>"+
                  "</span>"+
                 "</p>"+
                "</div>"+
               "</div>";
      }

    }

    $("#adminEventSpecialsDiv").html(html);

    $("#admin-edit-event-modal").modal("show");
  });
}

function clearAdminEditEventModal() {
  document.getElementById("admin-edit-event-title").value = "";
  document.getElementById("admin-edit-event-details").value = "";
  document.getElementById("admin-edit-event-origDetails").value = "";
  document.getElementById("admin-edit-event-start-date").value = "";
  document.getElementById("admin-edit-event-end-date").value = "";
  //document.getElementById("admin-edit-event-start-datetime").value = "";
  //document.getElementById("admin-edit-event-end-datetime").value = "";
  document.getElementById("admin-edit-event-start-time").value = "";
  document.getElementById("admin-edit-event-end-time").value = "";
  for(var i = 0; i < 7; i++) {
    selectAdminEditEventDay(intDayToStringAbbr(i).toLowerCase(), false, true);
  }
}

function selectAdminEditEventDay(day, select, deselect) {
  if(select) {
    document.getElementById("admin-edit-event-"+day+"ToggleButton").className = "btn btn-primary active";
    document.getElementById("admin-edit-event-day-"+day).value = true;
  }
  else if(deselect) {
    document.getElementById("admin-edit-event-"+day+"ToggleButton").className = "btn btn-default";
    document.getElementById("admin-edit-event-day-"+day).value = false;
  }
  else {
    if(document.getElementById("admin-edit-event-"+day+"ToggleButton").className == "btn btn-default") {
      document.getElementById("admin-edit-event-"+day+"ToggleButton").className = "btn btn-primary active";
      document.getElementById("admin-edit-event-day-"+day).value = true;
    } else {
      document.getElementById("admin-edit-event-"+day+"ToggleButton").className = "btn btn-default";
      document.getElementById("admin-edit-event-day-"+day).value = false;
    }
  }

  document.getElementById("adminEditEvent-dayToggle").style.border = "none";
  if(document.getElementById("eventDaysError")) {
    document.getElementById("eventDaysError").style.display = "none";
  }
}

function adminSaveEvent(e) {
  e.preventDefault();


  $("#adminEditEventForm").submit();
}


// ADD SPECIAL TO EVENT

function adminSelectAllDays(yes) {
  var startDate = document.getElementById("admin-edit-event-start-date").value;
  var endDate = document.getElementById("admin-edit-event-end-date").value;

  startDate = new Date(startDate);
  endDate = new Date(endDate);

  startDate.addHoursDST();
  endDate.addHoursDST();

  if(yes) {
    while(startDate <= endDate) {
      var id = ""+(startDate.getMonth()+1)+startDate.getDate();
      document.getElementById("day-"+id).className = "btn btn-primary active";
      document.getElementById("day-"+id).style.backgroundColor = "#286090";
      document.getElementById("day-"+id).style.border = "1px solid #204d74";
      startDate.setDate(startDate.getDate()+1);
    }
  } 
  else {
    while(startDate <= endDate) {
      var id = ""+(startDate.getMonth()+1)+startDate.getDate();
      document.getElementById("day-"+id).className = "btn btn-default";
      document.getElementById("day-"+id).style.backgroundColor = "#fff";
      document.getElementById("day-"+id).style.border = "1px solid #ccc";
      startDate.setDate(startDate.getDate()+1);
    }
  }
  document.getElementById("adminAddEventSpecial-daysError").style.display = "none";
}

function adminToggleAddEventSpecialDay(dt) {
  if(document.getElementById("adminAddEventSpecial-daysError") && document.getElementById("adminAddEventSpecial-daysError").style.display != "none") {
    var dayButtons = document.getElementById("adminAddEventSpecial-dayToggle").getElementsByTagName("button");
    for(var d in dayButtons) {
      if(dayButtons[d].style) {
        dayButtons[d].style.backgroundColor = "#fff";
        dayButtons[d].style.border = "1px solid #ccc";
      }
    }
    document.getElementById("adminAddEventSpecial-daysError").style.display = "none";
  }

  if(document.getElementById("day-"+dt).className == "btn btn-default") {
    document.getElementById("day-"+dt).className = "btn btn-primary active";
    document.getElementById("day-"+dt).style.backgroundColor = "#286090";
    document.getElementById("day-"+dt).style.border = "1px solid #204d74";
  } 
  else {
    document.getElementById("day-"+dt).className = "btn btn-default";
    document.getElementById("day-"+dt).style.backgroundColor = "#fff";
    document.getElementById("day-"+dt).style.border = "1px solid #ccc";
  }
}

function toggleAdminAddEventSpecialPriceType(type) {
  document.getElementById("adminAddEventSpecial-price").value = "";
  document.getElementById("adminAddEventSpecial-price").className = "form-control";

  if (type == 'money') {
    adminAddEventSpecialPriceType = "$";
    adminAddEventSpecialPriceTypeId = 300;
    document.getElementById("adminAddEventSpecial-price").disabled = false;
    document.getElementById("adminAddEventSpecial-priceLabel").innerHTML = "Price";
    document.getElementById("adminAddEventSpecial-percentSignInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-moneySignInPriceTextbox").style.display = "block";
    document.getElementById("adminAddEventSpecial-bogoInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-freeInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-moneyToggleButton").className = "btn btn-primary active";
    document.getElementById("adminAddEventSpecial-percentToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-bogoToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-freeToggleButton").className = "btn btn-default";
  }
  else if (type == 'percent') {
    adminAddEventSpecialPriceType = "%";
    adminAddEventSpecialPriceTypeId = 301;
    document.getElementById("adminAddEventSpecial-price").disabled = false;
    document.getElementById("adminAddEventSpecial-priceLabel").innerHTML = "Percent Off";
    document.getElementById("adminAddEventSpecial-percentSignInPriceTextbox").style.display = "block";
    document.getElementById("adminAddEventSpecial-moneySignInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-bogoInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-freeInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-moneyToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-percentToggleButton").className = "btn btn-primary active";
    document.getElementById("adminAddEventSpecial-bogoToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-freeToggleButton").className = "btn btn-default";
  } else if (type == 'bogo'){
    adminAddEventSpecialPriceType = "Buy One Get One";
    adminAddEventSpecialPriceTypeId = 302;
    document.getElementById("adminAddEventSpecial-price").disabled = true;
    document.getElementById("adminAddEventSpecial-priceLabel").innerHTML = "BOGO";
    document.getElementById("adminAddEventSpecial-percentSignInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-moneySignInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-bogoInPriceTextbox").style.display = "block";
    document.getElementById("adminAddEventSpecial-freeInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-moneyToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-percentToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-bogoToggleButton").className = "btn btn-primary active";
    document.getElementById("adminAddEventSpecial-freeToggleButton").className = "btn btn-default";
  } else if (type == 'free'){
    adminAddEventSpecialPriceType = "Free";
    adminAddEventSpecialPriceTypeId = 307;
    document.getElementById("adminAddEventSpecial-price").disabled = true;
    document.getElementById("adminAddEventSpecial-priceLabel").innerHTML = "Free";
    document.getElementById("adminAddEventSpecial-percentSignInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-moneySignInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-bogoInPriceTextbox").style.display = "none";
    document.getElementById("adminAddEventSpecial-freeInPriceTextbox").style.display = "block";
    document.getElementById("adminAddEventSpecial-moneyToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-percentToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-bogoToggleButton").className = "btn btn-default";
    document.getElementById("adminAddEventSpecial-freeToggleButton").className = "btn btn-primary active";
  }

  document.getElementById("hiddenAdminAddEventSpecialPriceType").value = adminAddEventSpecialPriceTypeId;

  document.getElementById("adminAddEventSpecial-price").style.backgroundColor = "#fff";
  document.getElementById("adminAddEventSpecial-price").style.border = "1px solid #ccc";
  if(document.getElementById("adminAddEventSpecial-priceError")) {
    document.getElementById("adminAddEventSpecial-priceError").style.display = "none";
  }
}

function chooseAdminAddEventSpecialDealType(typeID, eventId) {
  isAdminAddEventSpecialDealTypeSelected = false;
  adminAddEventSpecialDealTypeId = getDealTypeRefIdByOGId(typeID);

  document.getElementById("hiddenAdminAddEventSpecialType").value = adminAddEventSpecialDealTypeId;

  var dealTypeString = "adminAddEventSpecial-deal-type-";

  for (var i = 0; i <= 15; i++) {
    if (i == typeID) {
        document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
        document.getElementById(dealTypeString+i).style.border = "2px solid orange";
        document.getElementById(dealTypeString+i).value = adminAddEventSpecialDealTypeId;
        isAdminAddEventSpecialDealTypeSelected = true;
    } else {
        document.getElementById(dealTypeString+i).style.background = "white";
        document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
        document.getElementById(dealTypeString+i).value = 0;
    }
  }

  if(document.getElementById("adminAddEventSpecial-dealTypeError")) {
    document.getElementById("adminAddEventSpecial-dealTypeError").style.display = "none";
  }
}

function addSpecialToAdminEvent() {
  var eventIds = document.getElementById("admin-edit-event-ids").value;
  eventIds = eventIds.split(",");

  var days = [];
  var daysStr = [];
  var content = document.getElementById("adminAddEventSpecial-dayToggle");
  var dayButtons = content.getElementsByTagName("button");
  for(var i = 0; i < dayButtons.length; i++) {
    if(dayButtons[i].className.indexOf("active") > -1) {
      dayInput = document.getElementById(dayButtons[i].id+"-hidden");
      days.push(dayInput.value);
      daysStr.push(dayButtons[i].innerHTML);
    }
  }

  var startTime = document.getElementById("adminAddEventSpecial-startTime").value;
  var endTime = document.getElementById("adminAddEventSpecial-endTime").value;

  var price = document.getElementById("adminAddEventSpecial-price").value;
  var details = document.getElementById("adminAddEventSpecial-details").value;

  var parameters = {
    estId: document.getElementById("estId").value,
    eventId: eventIds[0],
    days: days,
    startTime: startTime,
    endTime: endTime,
    priceType: adminAddEventSpecialPriceTypeId,
    price: price,
    details: details,
    specialType: adminAddEventSpecialDealTypeId
  };

  if(adminValidateAddSpecialToEvent(parameters)) {
    $.get('/adminaddspecialtoevent', parameters, function(data) {
      if(data) {
        var htmlRow = "";
        var specialDays = "";

        if(daysStr.length != dayButtons.length) {
          for(var d = 0; d < daysStr.length; d++) {
            specialDays = specialDays + daysStr[d];
            if(d < daysStr.length-1)
              specialDays = specialDays + ", ";
          }
        }
        else {
          specialDays = "All Days";
        }

        if(data.specialIds) {
          htmlRow = htmlRow+"<div class='row' id='adminEditEventSpecialRow"+data.specialIds[0]+"' style='margin-left:0px; margin-right:0px;'>"+
                      "<div class='col col-md-5 col-sm-5 col-xs-5' style='line-height:1.5;'>"+
                   "<img src='assets/img/"+getDealTypeImageByRefId(adminAddEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>";

                   if(adminAddEventSpecialPriceTypeId == 300)
                    htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
                   else if(adminAddEventSpecialPriceTypeId == 301)
                    htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
                   else if(adminAddEventSpecialPriceTypeId == 302)
                    htmlRow = htmlRow+"BOGO "+details;
                   else if(adminAddEventSpecialPriceTypeId == 307)
                    htmlRow = htmlRow+"Free "+details;

          htmlRow = htmlRow+"</p>"+
                  "</div>"+
                  "<div class='col col-md-2 col-sm-2 col-xs-2' style='line-height:1.5;'>"+
                       "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+specialDays+"</p>"+
                      "</div>"+
                      "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5;'>"+
                       "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+formatStartEndTime(new Date("01/01/2020 " + startTime)) + " - " + formatStartEndTime(new Date("01/01/2020 " + endTime))+"</p>"+
                      "</div>"+
                  "<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
                   "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                    "<span style='cursor:pointer'>"+
                     "<a id='adminDeleteEventSpecial"+data.specialIds[0]+"' onclick='adminDeleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
                      "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                     "</a>"+
                    "</span>"+
                   "</p>"+
                  "</div>"+
                   "</div>";
        }
        else {
          htmlRow = htmlRow+"<div class='row' id='adminEditEventSpecialRow"+data.specialId+"' style='margin-left:0px; margin-right:0px;'>"+
                      "<div class='col col-md-12' style='line-height:1.5;'>"+
                   "<img src='assets/img/"+getDealTypeImageByRefId(adminAddEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>";

                   if(adminAddEventSpecialPriceTypeId == 300)
                    htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
                   else if(adminAddEventSpecialPriceTypeId == 301)
                    htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
                   else if(adminAddEventSpecialPriceTypeId == 302)
                    htmlRow = htmlRow+"BOGO "+details;
                   else if(adminAddEventSpecialPriceTypeId == 307)
                    htmlRow = htmlRow+"Free "+details;

          htmlRow = htmlRow+"</p>"+
                   "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                    "<span style='cursor:pointer'>"+
                     "<a id='adminDeleteEventSpecial"+data.specialId+"' onclick='adminDeleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
                      "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                     "</a>"+
                    "</span>"+
                   "</p>"+
                  "</div>"+
                   "</div>";
        }

        document.getElementById("adminEventSpecialsDiv").innerHTML += htmlRow;

        var newRow = htmlRow;

        newRow = newRow.replace("style='margin-left:0px; margin-right:0px;'", "");
        newRow = newRow.replace("style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'", "style='display:inline; vertical-align:middle;'");

        var todayRow = newRow;

        if(data.specialIds) {
          newRow = newRow.replace("id='adminEditEventSpecialRow"+data.specialIds[0]+"'", "id='adminAddedEventSpecialRow"+data.specialIds[0]+"'");
          newRow = newRow.replace("<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
                       "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='adminDeleteEventSpecial"+data.specialIds[0]+"' onclick='adminDeleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>"+
                      "</div>", "");
          todayRow = todayRow.replace("id='adminEditEventSpecialRow"+data.specialIds[0]+"'", "id='adminAddedEventSpecialRow"+data.specialIds[0]+"'");
          todayRow = todayRow.replace("<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
                       "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='adminDeleteEventSpecial"+data.specialIds[0]+"' onclick='adminDeleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>"+
                      "</div>", "");
        }
        else {
          newRow = newRow.replace("id='adminEditEventSpecialRow"+data.specialId+"'", "id='adminAddedEventSpecialRow"+data.specialId+"'");
          newRow = newRow.replace("<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='adminDeleteEventSpecial"+data.specialId+"' onclick='adminDeleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>", "");
          todayRow = todayRow.replace("id='adminEditEventSpecialRow"+data.specialId+"'", "id='adminAddedEventSpecialRow"+data.specialId+"'");
          todayRow = todayRow.replace("<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='adminDeleteEventSpecial"+data.specialId+"' onclick='adminDeleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>", "");
        }

        
        for(var i = 0; i < eventIds.length; i++) {
          var el = "manageEventsAddedSpecials"+eventIds[i];
          var el2 = "eventsAddedSpecials"+eventIds[i];
          var noSpecRow = "manageEventsNoSpecialsRow"+eventIds[i];

          if(document.getElementById(el))
            document.getElementById(el).innerHTML += newRow;

          if(document.getElementById(el2))
            document.getElementById(el2).innerHTML += todayRow;

          if(document.getElementById(noSpecRow))
            document.getElementById(noSpecRow).style.display = "none";
        }
        

        toggleAdminAddEventSpecialPriceType('money');
        document.getElementById("adminAddEventSpecial-details").value = "";
        chooseAdminAddEventSpecialDealType(-1);
        adminToggleAddEventSpecialDiv();
      }
    });
  }
}

function adminValidateAddSpecialToEvent(p) {
  var valid = true;
  var msg = [];
  var cnt = 0;
  document.getElementById("addSpecialToAdminEventErrors").innerHTML = "";

  // multiday
  if(document.getElementById("multidayDiv").style.display != "none") {
    // days
    if(p.days.length == 0) {
      var dayButtons = document.getElementById("adminAddEventSpecial-dayToggle").getElementsByTagName("button");
      for(var d in dayButtons) {
        if(dayButtons[d].style) {
          dayButtons[d].style.backgroundColor = "#FFEAEA";
          dayButtons[d].style.border = "1px solid #FF0000";
        }
      }

      msg[cnt++] = "<p id='adminAddEventSpecial-daysError'>Choose the day(s) the special will occur!</p>";
      valid = false;
    }

    // start time
    if(p.startTime == "") {
      document.getElementById("adminAddEventSpecial-startTime").style.backgroundColor = "#FFEAEA";
      document.getElementById("adminAddEventSpecial-startTime").style.border = "1px solid #FF0000";
      msg[cnt++] = "<p id='adminAddEventSpecial-startTimeError'>Add a start time!</p>";
      valid = false;
    }
    else {
      document.getElementById("adminAddEventSpecial-startTime").style.backgroundColor = "#fff";
      document.getElementById("adminAddEventSpecial-startTime").style.border = "1px solid #ccc";
    }

    // end time
    if(p.endTime == "") {
      document.getElementById("adminAddEventSpecial-endTime").style.backgroundColor = "#FFEAEA";
      document.getElementById("adminAddEventSpecial-endTime").style.border = "1px solid #FF0000";
      msg[cnt++] = "<p id='adminAddEventSpecial-endTimeError'>Add an end time!</p>";
      valid = false;
    }
    else {
      document.getElementById("adminAddEventSpecial-endTime").style.backgroundColor = "#fff";
      document.getElementById("adminAddEventSpecial-endTime").style.border = "1px solid #ccc";
    }
  }

  // price
  if(p.price == "" && (p.priceType == 300 || p.priceType == 301)) {
    document.getElementById("adminAddEventSpecial-price").style.backgroundColor = "#FFEAEA";
    document.getElementById("adminAddEventSpecial-price").style.border = "1px solid #FF0000";
    msg[cnt++] = "<p id='adminAddEventSpecial-priceError'>Add a price!</p>";
    valid = false;
  }
  else {
    document.getElementById("adminAddEventSpecial-price").style.backgroundColor = "#fff";
    document.getElementById("adminAddEventSpecial-price").style.border = "1px solid #ccc";
  }

  // details
  if(p.details == "") {
    document.getElementById("adminAddEventSpecial-details").style.backgroundColor = "#FFEAEA";
    document.getElementById("adminAddEventSpecial-details").style.border = "1px solid #FF0000";
    msg[cnt++] = "<p id='adminAddEventSpecial-detailsError'>Add some details!</p>";
    valid = false;
  }
  else {
    document.getElementById("adminAddEventSpecial-details").style.backgroundColor = "#fff";
    document.getElementById("adminAddEventSpecial-details").style.border = "1px solid #ccc";
  }

  // icon
  if(p.specialType == "") {
    for(var i = 0; i <= 15; i++) {
      document.getElementById("adminAddEventSpecial-deal-type-"+i).style.border = "1px solid #FF0000";
    }
    msg[cnt++] = "<p id='adminAddEventSpecial-dealTypeError'>Choose an icon!</p>";
    valid = false;
  }
  else {
    for(var i = 0; i <= 15; i++) {
      document.getElementById("adminAddEventSpecial-deal-type-"+i).style.border = "1px solid #c0c0c0";
    }
  }

  if(!valid) {
    for(var m in msg) {
      document.getElementById("addSpecialToAdminEventErrors").innerHTML += msg[m];
    }
  }

  return valid;
}

// DELETE SPECIAL FROM EVENT

function adminDeleteSpecialFromEvent(specialId) {
  var parameters = {
    specialId: specialId
  };

  $.get('/deletespecialfromevent', parameters, function(data) {
    if(data && data.length > 0) {
      for(var d in data) {

        var x = document.getElementById("adminEventSpecialRow"+data[d]);
        if(x != null && x != undefined) {
          x.style.display = "none";
        }

        x = document.getElementById("adminEventSpecialRoww"+data[d]);
        if(x != null && x != undefined) {
          x.style.display = "none";
        }

        x = document.getElementById("adminEditEventSpecialRow"+data[d]);
        if(x != null && x != undefined) {
          x.style.display = "none";
        }

        x = document.getElementById("adminSpecialRow"+data[d]);
        if(x != null && x != undefined) {
          x.style.display = "none";
        }

      }
    }
  });
}



function adminValidateField(field) {
  if(field.value != "") {
    field.style.backgroundColor = "#fff";
    field.style.border = "1px solid #ccc";
    if(document.getElementById(field.id+"Error")) {
      document.getElementById(field.id+"Error").style.display = "none";
    }
  }
}







/* AUDIT */

var auditPriceType = "$";
var auditPriceTypeId = 300;
var auditDailyPriceType = "$";
var auditDailyPriceTypeId = 300;

var auditAddEventSpecialPriceType = "$";
var auditAddEventSpecialPriceTypeId = 300;
var auditAddEventSpecialDealTypeId = 0;
var isAuditAddEventSpecialDealTypeSelected = false;

// sort table 
function sortAuditTable(id) {
  var table, rows, switching, i, x, y, xx, yy, shouldSwitch, column, sort, ascending;
  table = document.getElementById("auditTable");
  switching = true;
  column = 1;
  sort = "string";
  ascending = true;

  if(id == "btnAuditSortName") {
    column = 1;
    sort = "string";
    if(document.getElementById(id).innerHTML.indexOf("arrow-down") > -1) {
      document.getElementById(id).innerHTML = "<span style='font-weight:900;'>Business Info</span> <i class='fa fa-arrow-up' style='font-size:18px; margin-left:5px;'></i>";
      ascending = false;
    }
    else {
      document.getElementById(id).innerHTML = "<span style='font-weight:900;'>Business Info</span> <i class='fa fa-arrow-down' style='font-size:18px; margin-left:5px;'></i>";
      ascending = true;
    }

    document.getElementById("btnAuditSortLastReviewed").innerHTML = "Last Reviewed";
    document.getElementById("btnAuditSortClaimed").innerHTML = "Claimed";
  }
  else if(id == "btnAuditSortClaimed") {
    column = 3;
    sort = "date";
    if(document.getElementById(id).innerHTML.indexOf("arrow-up") > -1) {
      document.getElementById(id).innerHTML = "<span style='font-weight:900;'>Claimed</span> <i class='fa fa-arrow-down' style='font-size:18px; margin-left:5px;'></i>";
      ascending = false;
    }
    else {
      document.getElementById(id).innerHTML = "<span style='font-weight:900;'>Claimed</span> <i class='fa fa-arrow-up' style='font-size:18px; margin-left:5px;'></i>";
      ascending = true;
    }

    document.getElementById("btnAuditSortName").innerHTML = "Business Info";
    document.getElementById("btnAuditSortLastReviewed").innerHTML = "Last Reviewed";
  }
  else if(id == "btnAuditSortLastReviewed") {
    column = 4;
    sort = "date";
    if(document.getElementById(id).innerHTML.indexOf("arrow-up") > -1) {
      document.getElementById(id).innerHTML = "<span style='font-weight:900;'>Last Reviewed</span> <i class='fa fa-arrow-down' style='font-size:18px; margin-left:5px;'></i>";
      ascending = false;
    }
    else {
      document.getElementById(id).innerHTML = "<span style='font-weight:900;'>Last Reviewed</span> <i class='fa fa-arrow-up' style='font-size:18px; margin-left:5px;'></i>";
      ascending = true;
    }

    document.getElementById("btnAuditSortName").innerHTML = "Business Info";
    document.getElementById("btnAuditSortClaimed").innerHTML = "Claimed";
  }


  /*Make a loop that will continue until
  no switching has been done:*/
  while (switching) {
    //start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /*Loop through all table rows (except the
    first, which contains table headers):*/
    for (i = 1; i < (rows.length - 2); i++) {
      //start by saying there should be no switching:
      shouldSwitch = false;
      /*Get the two elements you want to compare,
      one from current row and one from the next:*/
      x = rows[i].getElementsByTagName("TD")[column];
      y = rows[i + 2].getElementsByTagName("TD")[column];

      if(x && y) {
        var xx = x.getElementsByTagName("P")[0];
        var yy = y.getElementsByTagName("P")[0];
        if(xx && yy) {
          //check if the two rows should switch place:

          if((sort == "string" && ascending && xx.innerHTML.toLowerCase() > yy.innerHTML.toLowerCase()) ||
             (sort == "string" && !ascending && xx.innerHTML.toLowerCase() < yy.innerHTML.toLowerCase())) {
            //if so, mark as a switch and break the loop:
            shouldSwitch = true;
            break;
          }
          else if(sort == "date") {
            if((ascending && xx.innerHTML != "-" && yy.innerHTML == "-") ||
               (!ascending && xx.innerHTML == "-" && yy.innerHTML != "-")) {
              shouldSwitch = true;
              break;
            }
            else if(xx.innerHTML != "-" && yy.innerHTML != "-") {
              var xxDt = xx.innerHTML.substring(0, xx.innerHTML.indexOf(" "));
              var xxTime = xx.innerHTML.substring(xx.innerHTML.indexOf(" ")+1);
              xxTime = dateStrToSelectDate(xxTime.toUpperCase());
              var xxDate = new Date(xxDt + " " + xxTime);

              var yyDt = yy.innerHTML.substring(0, yy.innerHTML.indexOf(" "));
              var yyTime = yy.innerHTML.substring(yy.innerHTML.indexOf(" ")+1);
              yyTime = dateStrToSelectDate(yyTime.toUpperCase());
              var yyDate = new Date(yyDt + " " + yyTime);

              if((ascending && xxDate > yyDate) ||
                 (!ascending && xxDate < yyDate)) {
                shouldSwitch = true;
                break;
              }
            }
          }
        }
      }
    }
    if (shouldSwitch) {
      /*If a switch has been marked, make the switch
      and mark that a switch has been done:*/
      rows[i].parentNode.insertBefore(rows[i + 2], rows[i]);
      rows[i+1].parentNode.insertBefore(rows[i + 3], rows[i+1]);
      switching = true;
    }
  }
}

// show more
function auditShowMore(estId) {
  var moreLess = document.getElementById("btnAuditMoreLess"+estId);
  var caret = document.getElementById("btnAuditMoreLessCaret"+estId);
  var row = document.getElementById("auditEstMoreRow"+estId);

  if(moreLess.innerHTML == "More") {
    moreLess.innerHTML = "Less";
    caret.className = "fa fa-caret-up";
    row.style.display = "table-row";

  }
  else if(moreLess.innerHTML == "Less") {
    moreLess.innerHTML = "More";
    caret.className = "fa fa-caret-down";
    row.style.display = "none";
  }
}

// edit establishment
function auditEditEst(estId) {
  $('#auditEditEstModal').modal({backdrop: 'static', keyboard: false}); 

  clearAuditEditEstModal();

  /*
  var ownerId = document.getElementById("estOwnerIdHidden"+estId).value;
  document.getElementById("review-est-owner").value = ownerId;
  document.getElementById("review-est-owner-username").value = document.getElementById("estOwnerUsernameHidden"+estId).value;
  document.getElementById("review-est-owner-email").value = document.getElementById("estOwnerEmailHidden"+estId).value;
  document.getElementById("review-est-owner-phoneNumber").value = ownerId != "" ? document.getElementById("estOwnerPhoneHidden"+estId).value : document.getElementById("estPhoneHidden"+estId).value;
  document.getElementById("review-est-owner-needsToPay").value = ownerId != "" ? document.getElementById("estOwnerNeedsToPayHidden"+estId).value : 1;
  $("#review-est-owner-phoneNumber").inputmask({"mask": "(999) 999-9999"});

  if(ownerId != "") {
    document.getElementById("review-est-owner").disabled = true;
    document.getElementById("review-est-owner-username").disabled = true;
    document.getElementById("review-est-owner-email").disabled = true;
    document.getElementById("review-est-owner-phoneNumber").disabled = true;
    document.getElementById("review-est-owner-needsToPay").disabled = true;
  }
  */

  document.getElementById("audit-edit-est-id").value = estId;
  document.getElementById("audit-edit-est-address-id").value = document.getElementById("auditEstAddressIdHidden"+estId).value;
  document.getElementById("audit-edit-est-hours-id").value = document.getElementById("auditEstHoursIdHidden"+estId).value;

  document.getElementById("audit-edit-est-img-circle").src = document.getElementById("auditEstImgHidden"+estId).value;
  document.getElementById("audit-edit-est-name").value = document.getElementById("auditEstNameHidden"+estId).value;
  document.getElementById("audit-edit-est-address").value = document.getElementById("auditEstAddress1Hidden"+estId).value;
  document.getElementById("audit-edit-est-address2").value = document.getElementById("auditEstAddress2Hidden"+estId).value;
  document.getElementById("audit-edit-est-city").value = document.getElementById("auditEstCityHidden"+estId).value;
  document.getElementById("audit-edit-est-state").value = document.getElementById("auditEstStateHidden"+estId).value;
  document.getElementById("audit-edit-est-zip").value = document.getElementById("auditEstZipHidden"+estId).value;
  document.getElementById("audit-edit-est-phone").value = document.getElementById("auditEstPhoneHidden"+estId).value;
  document.getElementById("audit-edit-est-website").value = document.getElementById("auditEstWebsiteHidden"+estId).value;
  document.getElementById("audit-edit-est-resWebsite").value = document.getElementById("auditEstResWebsiteHidden"+estId).value;
  document.getElementById("audit-edit-est-menu-link").value = document.getElementById("auditEstMenuLinkHidden"+estId).value;
  $("#audit-edit-est-phone").inputmask({"mask": "(999) 999-9999"});

  document.getElementById("audit-edit-est-mon-open").value = formatInputTime(document.getElementById("auditEstMonOpenHidden"+estId).value);
  document.getElementById("audit-edit-est-mon-close").value = formatInputTime(document.getElementById("auditEstMonCloseHidden"+estId).value); 
  document.getElementById("audit-edit-est-tue-open").value = formatInputTime(document.getElementById("auditEstTueOpenHidden"+estId).value);  
  document.getElementById("audit-edit-est-tue-close").value = formatInputTime(document.getElementById("auditEstTueCloseHidden"+estId).value); 
  document.getElementById("audit-edit-est-wed-open").value = formatInputTime(document.getElementById("auditEstWedOpenHidden"+estId).value);  
  document.getElementById("audit-edit-est-wed-close").value = formatInputTime(document.getElementById("auditEstWedCloseHidden"+estId).value); 
  document.getElementById("audit-edit-est-thu-open").value = formatInputTime(document.getElementById("auditEstThuOpenHidden"+estId).value);  
  document.getElementById("audit-edit-est-thu-close").value = formatInputTime(document.getElementById("auditEstThuCloseHidden"+estId).value); 
  document.getElementById("audit-edit-est-fri-open").value = formatInputTime(document.getElementById("auditEstFriOpenHidden"+estId).value);  
  document.getElementById("audit-edit-est-fri-close").value = formatInputTime(document.getElementById("auditEstFriCloseHidden"+estId).value); 
  document.getElementById("audit-edit-est-sat-open").value = formatInputTime(document.getElementById("auditEstSatOpenHidden"+estId).value);  
  document.getElementById("audit-edit-est-sat-close").value = formatInputTime(document.getElementById("auditEstSatCloseHidden"+estId).value); 
  document.getElementById("audit-edit-est-sun-open").value = formatInputTime(document.getElementById("auditEstSunOpenHidden"+estId).value);  
  document.getElementById("audit-edit-est-sun-close").value = formatInputTime(document.getElementById("auditEstSunCloseHidden"+estId).value);  

  if(document.getElementById("audit-edit-est-mon-open").value == "" && document.getElementById("audit-edit-est-mon-close").value == "")
    setAuditEstClosed(null, "audit-edit-est-mon");
  if(document.getElementById("audit-edit-est-tue-open").value == "" && document.getElementById("audit-edit-est-tue-close").value == "")
    setAuditEstClosed(null, "audit-edit-est-tue");
  if(document.getElementById("audit-edit-est-wed-open").value == "" && document.getElementById("audit-edit-est-wed-close").value == "")
    setAuditEstClosed(null, "audit-edit-est-wed");
  if(document.getElementById("audit-edit-est-thu-open").value == "" && document.getElementById("audit-edit-est-thu-close").value == "")
    setAuditEstClosed(null, "audit-edit-est-thu");
  if(document.getElementById("audit-edit-est-fri-open").value == "" && document.getElementById("audit-edit-est-fri-close").value == "")
    setAuditEstClosed(null, "audit-edit-est-fri");
  if(document.getElementById("audit-edit-est-sat-open").value == "" && document.getElementById("audit-edit-est-sat-close").value == "")
    setAuditEstClosed(null, "audit-edit-est-sat");
  if(document.getElementById("audit-edit-est-sun-open").value == "" && document.getElementById("audit-edit-est-sun-close").value == "")
    setAuditEstClosed(null, "audit-edit-est-sun");

  document.getElementById("audit-edit-est-description").value = document.getElementById("auditEstDescriptionHidden"+estId).value;

  if(document.getElementById("auditEstDeliveryHidden"+estId).value == "1")
    auditEstFeatureClick(document.getElementById("audit-edit-est-deliveryYes"));
  else
    auditEstFeatureClick(document.getElementById("audit-edit-est-deliveryNo"));

  if(document.getElementById("auditEstTakeoutHidden"+estId).value == "1")
    auditEstFeatureClick(document.getElementById("audit-edit-est-takeoutYes"));
  else
    auditEstFeatureClick(document.getElementById("audit-edit-est-takeoutNo"));

  if(document.getElementById("auditEstPatioHidden"+estId).value == "1")
    auditEstFeatureClick(document.getElementById("audit-edit-est-patioYes"));
  else
    auditEstFeatureClick(document.getElementById("audit-edit-est-patioNo"));

  if(document.getElementById("auditEstRooftopHidden"+estId).value == "1")
    auditEstFeatureClick(document.getElementById("audit-edit-est-rooftopYes"));
  else
    auditEstFeatureClick(document.getElementById("audit-edit-est-rooftopNo"));

  if(document.getElementById("auditEstBrunchHidden"+estId).value == "1")
    auditEstFeatureClick(document.getElementById("audit-edit-est-brunchYes"));
  else
    auditEstFeatureClick(document.getElementById("audit-edit-est-brunchNo"));

  if(document.getElementById("auditEstDogHidden"+estId).value == "1")
    auditEstFeatureClick(document.getElementById("audit-edit-est-dogYes"));
  else
    auditEstFeatureClick(document.getElementById("audit-edit-est-dogNo"));
}

function auditValidateEst(e) {
  e.preventDefault();

  document.getElementById("audit-edit-est-err").innerHTML = "";
  document.getElementById("audit-edit-est-err").style.display = "none";

  var parameters = {
    /*ownerId:      document.getElementById('audit-edit-est-owner').value,
    email:        document.getElementById('audit-edit-est-owner-email').value,
    username:     document.getElementById('audit-edit-est-owner-username').value,
    phone:        document.getElementById('audit-edit-est-owner-phoneNumber').value,
    needsToPay:   document.getElementById('audit-edit-est-owner-needsToPay').value,*/
    estId:        document.getElementById('audit-edit-est-id').value, 
    img:          document.getElementById('audit-edit-est-img-circle').src != "" ? "true" : "",
    name:         document.getElementById('audit-edit-est-name').value,  
    address_id:   document.getElementById('audit-edit-est-address-id').value,  
    address_1:    document.getElementById('audit-edit-est-address').value,  
    address_2:    document.getElementById('audit-edit-est-address2').value,  
    city:         document.getElementById('audit-edit-est-city').value,         
    state:        document.getElementById('audit-edit-est-state').value,  
    zip:          document.getElementById('audit-edit-est-zip').value,  
    phone_num:    document.getElementById('audit-edit-est-phone').value,  
    website:      document.getElementById('audit-edit-est-website').value,  
    resWebsite:   document.getElementById('audit-edit-est-resWebsite').value,  
    menu_link:    document.getElementById('audit-edit-est-menu-link').value,  
    hours_id:     document.getElementById('audit-edit-est-hours-id').value,  
    mon_open:     document.getElementById('audit-edit-est-mon-open').value,  
    mon_close:    document.getElementById('audit-edit-est-mon-close').value,  
    tue_open:     document.getElementById('audit-edit-est-tue-open').value,  
    tue_close:    document.getElementById('audit-edit-est-tue-close').value, 
    wed_open:     document.getElementById('audit-edit-est-wed-open').value,  
    wed_close:    document.getElementById('audit-edit-est-wed-close').value, 
    thu_open:     document.getElementById('audit-edit-est-thu-open').value,  
    thu_close:    document.getElementById('audit-edit-est-thu-close').value, 
    fri_open:     document.getElementById('audit-edit-est-fri-open').value,  
    fri_close:    document.getElementById('audit-edit-est-fri-close').value, 
    sat_open:     document.getElementById('audit-edit-est-sat-open').value,  
    sat_close:    document.getElementById('audit-edit-est-sat-close').value, 
    sun_open:     document.getElementById('audit-edit-est-sun-open').value,  
    sun_close:    document.getElementById('audit-edit-est-sun-close').value, 
    description:  document.getElementById('audit-edit-est-description').value,
    delivery:     document.getElementById('audit-edit-est-delivery').value,
    takeout:      document.getElementById('audit-edit-est-takeout').value,
    patio:        document.getElementById('audit-edit-est-patio').value,
    rooftop:      document.getElementById('audit-edit-est-rooftop').value,
    brunch:       document.getElementById('audit-edit-est-brunch').value,
    dog:          document.getElementById('audit-edit-est-dog').value
  };

  $.get('/auditvalidateest', parameters, function(err) {
    if(err.length == 0) {
      var params = {};
      const formData = new FormData(document.getElementById("audit-edit-est-form"));
      for(var pair of formData.entries()) {
        params[pair[0]] = pair[1];
      }

      //console.log(params);

      jQuery.ajax({
        url: '/auditsaveest',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST', // For jQuery < 1.9
        success: function(data){
          $("#auditEditEstModal").modal("hide");

          if(data.newDate) {
            //document.getElementById("auditEstStatusHidden"+params.establishment_id)

            // hidden fields
            if(data.imgUrl != "")
              document.getElementById("auditEstImgHidden"+params.establishment_id).value = data.imgUrl;

            document.getElementById("auditEstNameHidden"+params.establishment_id).value = params.name;
            document.getElementById("auditEstAddressIdHidden"+params.establishment_id).value = params.address_id;
            document.getElementById("auditEstAddress1Hidden"+params.establishment_id).value = params.address_1;
            document.getElementById("auditEstAddress2Hidden"+params.establishment_id).value = params.address_2;
            document.getElementById("auditEstCityHidden"+params.establishment_id).value = params.city;
            document.getElementById("auditEstStateHidden"+params.establishment_id).value = params.state;
            document.getElementById("auditEstZipHidden"+params.establishment_id).value = params.zip;
            document.getElementById("auditEstLatHidden"+params.establishment_id).value = params.latitude;
            document.getElementById("auditEstLngHidden"+params.establishment_id).value = params.longitude;
            document.getElementById("auditEstPhoneHidden"+params.establishment_id).value = params.phone_num;
            document.getElementById("auditEstWebsiteHidden"+params.establishment_id).value = params.website;
            document.getElementById("auditEstResWebsiteHidden"+params.establishment_id).value = params.resWebsite;
            document.getElementById("auditEstMenuLinkHidden"+params.establishment_id).value = params.menu_link;
            document.getElementById("auditEstActiveHidden"+params.establishment_id).value = params.is_active;

            document.getElementById("auditEstHoursIdHidden"+params.establishment_id).value = params.hours_id;
            document.getElementById("auditEstMonOpenHidden"+params.establishment_id).value = params.mon_open ? params.mon_open.replace(":", "") : "";
            document.getElementById("auditEstMonCloseHidden"+params.establishment_id).value = params.mon_close ? params.mon_close.replace(":", "") : "";
            document.getElementById("auditEstTueOpenHidden"+params.establishment_id).value = params.tue_open ? params.tue_open.replace(":", "") : "";
            document.getElementById("auditEstTueCloseHidden"+params.establishment_id).value = params.tue_close ? params.tue_close.replace(":", "") : "";
            document.getElementById("auditEstWedOpenHidden"+params.establishment_id).value = params.wed_open ? params.wed_open.replace(":", "") : "";
            document.getElementById("auditEstWedCloseHidden"+params.establishment_id).value = params.wed_close ? params.wed_close.replace(":", "") : "";
            document.getElementById("auditEstThuOpenHidden"+params.establishment_id).value = params.thu_open ? params.thu_open.replace(":", "") : "";
            document.getElementById("auditEstThuCloseHidden"+params.establishment_id).value = params.thu_close ? params.thu_close.replace(":", "") : "";
            document.getElementById("auditEstFriOpenHidden"+params.establishment_id).value = params.fri_open ? params.fri_open.replace(":", "") : "";
            document.getElementById("auditEstFriCloseHidden"+params.establishment_id).value = params.fri_close ? params.fri_close.replace(":", "") : "";
            document.getElementById("auditEstSatOpenHidden"+params.establishment_id).value = params.sat_open ? params.sat_open.replace(":", "") : "";
            document.getElementById("auditEstSatCloseHidden"+params.establishment_id).value = params.sat_close ? params.sat_close.replace(":", "") : "";
            document.getElementById("auditEstSunOpenHidden"+params.establishment_id).value = params.sun_open ? params.sun_open.replace(":", "") : "";
            document.getElementById("auditEstSunCloseHidden"+params.establishment_id).value = params.sun_close ? params.sun_close.replace(":", "") : "";

            document.getElementById("auditEstDescriptionHidden"+params.establishment_id).value = params.description;
            document.getElementById("auditEstDeliveryHidden"+params.establishment_id).value = params.delivery == "Yes" ? "1" : "0";
            document.getElementById("auditEstTakeoutHidden"+params.establishment_id).value = params.takeout == "Yes" ? "1" : "0";
            document.getElementById("auditEstPatioHidden"+params.establishment_id).value = params.patio == "Yes" ? "1" : "0";
            document.getElementById("auditEstRooftopHidden"+params.establishment_id).value = params.rooftop == "Yes" ? "1" : "0";
            document.getElementById("auditEstBrunchHidden"+params.establishment_id).value = params.brunch == "Yes" ? "1" : "0";
            document.getElementById("auditEstDogHidden"+params.establishment_id).value = params.dog == "Yes" ? "1" : "0";

            // pic
            document.getElementById("auditEstRowPic"+params.establishment_id).style.backgroundImage = "url("+document.getElementById('audit-edit-est-img-circle').src+")";

            // name
            document.getElementById("auditEstRowName"+params.establishment_id).innerHTML = params.name;

            // address
            var address = params.address_1;
            if(params.address_2 != null && params.address_2 != "")
              address += " " + params.address_2;
            address += "<br />" + params.city + ", " + params.state + " " + params.zip;
            document.getElementById("auditEstRowAddress"+params.establishment_id).innerHTML = address;

            // phone
            document.getElementById("auditEstRowPhone"+params.establishment_id).innerHTML = params.phone_num;

            // hours
            document.getElementById("auditEstRowMon"+params.establishment_id).innerHTML = params.mon_open != null ? timeStrToTime(params.mon_open.replace(":", "")) + " - " + timeStrToTime(params.mon_close.replace(":", "")) : "Closed";
            document.getElementById("auditEstRowTue"+params.establishment_id).innerHTML = params.tue_open != null ? timeStrToTime(params.tue_open.replace(":", "")) + " - " + timeStrToTime(params.tue_close.replace(":", "")) : "Closed";
            document.getElementById("auditEstRowWed"+params.establishment_id).innerHTML = params.wed_open != null ? timeStrToTime(params.wed_open.replace(":", "")) + " - " + timeStrToTime(params.wed_close.replace(":", "")) : "Closed";
            document.getElementById("auditEstRowThu"+params.establishment_id).innerHTML = params.thu_open != null ? timeStrToTime(params.thu_open.replace(":", "")) + " - " + timeStrToTime(params.thu_close.replace(":", "")) : "Closed";
            document.getElementById("auditEstRowFri"+params.establishment_id).innerHTML = params.fri_open != null ? timeStrToTime(params.fri_open.replace(":", "")) + " - " + timeStrToTime(params.fri_close.replace(":", "")) : "Closed";
            document.getElementById("auditEstRowSat"+params.establishment_id).innerHTML = params.sat_open != null ? timeStrToTime(params.sat_open.replace(":", "")) + " - " + timeStrToTime(params.sat_close.replace(":", "")) : "Closed";
            document.getElementById("auditEstRowSun"+params.establishment_id).innerHTML = params.sun_open != null ? timeStrToTime(params.sun_open.replace(":", "")) + " - " + timeStrToTime(params.sun_close.replace(":", "")) : "Closed";

            // last reviewed
            document.getElementById("audit-last-reviewed"+params.establishment_id).innerHTML = data.newDate;

            swal({
                title:  "Success!",
                text:   params.name + " was updated successfully!",
                icon:   "success",
                timer:  2000,
                buttons: false 
            });
          }
        },
        error: function(err) {
          console.log("ERROR");
          console.log(err);
        }
      });

    }
    else {
      var errs = "<span style='font-weight:600;'>Error(s):</span><br/>";
      for(var i = 0; i < err.length; i++) {
        errs += err[i] + "<br/>";
      }
      document.getElementById("audit-edit-est-err").innerHTML = errs;
      document.getElementById("audit-edit-est-err").style.display = "block";
    }
  });
}

function clearAuditEditEstModal() {
  /*
  document.getElementById("audit-edit-est-owner").value = "";
  document.getElementById("audit-edit-est-owner-username").value = "";
  document.getElementById("audit-edit-est-owner-email").value = "";
  document.getElementById("audit-edit-est-owner-phoneNumber").value = "";
  document.getElementById("audit-edit-est-owner-needsToPay").value = "";

  document.getElementById("audit-edit-est-owner").disabled = false;
  document.getElementById("audit-edit-est-owner-username").disabled = false;
  document.getElementById("audit-edit-est-owner-email").disabled = false;
  document.getElementById("audit-edit-est-owner-phoneNumber").disabled = false;
  document.getElementById("audit-edit-est-owner-needsToPay").disabled = false;
  */

  document.getElementById("audit-edit-est-id").value = "";

  document.getElementById('audit-edit-est-img-circle').src = "";
  document.getElementById("audit-edit-est-img").value = "";
  document.getElementById("audit-edit-est-name").value = "";
  document.getElementById("audit-edit-est-address").value = "";
  document.getElementById("audit-edit-est-address2").value = "";
  document.getElementById("audit-edit-est-city").value = "";
  document.getElementById("audit-edit-est-state").value = "";
  document.getElementById("audit-edit-est-zip").value = "";
  document.getElementById("audit-edit-est-phone").value = "";

  document.getElementById("audit-edit-est-mon-open").value = "";
  document.getElementById("audit-edit-est-mon-close").value = "";
  document.getElementById("audit-edit-est-tue-open").value = ""; 
  document.getElementById("audit-edit-est-tue-close").value = "";
  document.getElementById("audit-edit-est-wed-open").value = "";  
  document.getElementById("audit-edit-est-wed-close").value = "";
  document.getElementById("audit-edit-est-thu-open").value = "";
  document.getElementById("audit-edit-est-thu-close").value = "";
  document.getElementById("audit-edit-est-fri-open").value = "";
  document.getElementById("audit-edit-est-fri-close").value = "";
  document.getElementById("audit-edit-est-sat-open").value = ""; 
  document.getElementById("audit-edit-est-sat-close").value = "";
  document.getElementById("audit-edit-est-sun-open").value = "";
  document.getElementById("audit-edit-est-sun-close").value = "";

  document.getElementById("audit-edit-est-mon").className = "btn btn-light";
  document.getElementById("audit-edit-est-mon-open").disabled = false;
  document.getElementById("audit-edit-est-mon-close").disabled = false;
  document.getElementById("audit-edit-est-tue").className = "btn btn-light";
  document.getElementById("audit-edit-est-tue-open").disabled = false;
  document.getElementById("audit-edit-est-tue-close").disabled = false;
  document.getElementById("audit-edit-est-wed").className = "btn btn-light";
  document.getElementById("audit-edit-est-wed-open").disabled = false;
  document.getElementById("audit-edit-est-wed-close").disabled = false;
  document.getElementById("audit-edit-est-thu").className = "btn btn-light";
  document.getElementById("audit-edit-est-thu-open").disabled = false;
  document.getElementById("audit-edit-est-thu-close").disabled = false;
  document.getElementById("audit-edit-est-fri").className = "btn btn-light";
  document.getElementById("audit-edit-est-fri-open").disabled = false;
  document.getElementById("audit-edit-est-fri-close").disabled = false;
  document.getElementById("audit-edit-est-sat").className = "btn btn-light";
  document.getElementById("audit-edit-est-sat-open").disabled = false;
  document.getElementById("audit-edit-est-sat-close").disabled = false;
  document.getElementById("audit-edit-est-sun").className = "btn btn-light";
  document.getElementById("audit-edit-est-sun-open").disabled = false;
  document.getElementById("audit-edit-est-sun-close").disabled = false;

  document.getElementById("audit-edit-est-description").value = "";

  auditEstFeatureClick(document.getElementById("audit-edit-est-deliveryNo"));
  auditEstFeatureClick(document.getElementById("audit-edit-est-takeoutNo"));
  auditEstFeatureClick(document.getElementById("audit-edit-est-patioNo"));
  auditEstFeatureClick(document.getElementById("audit-edit-est-rooftopNo"));
  auditEstFeatureClick(document.getElementById("audit-edit-est-brunchNo"));
  auditEstFeatureClick(document.getElementById("audit-edit-est-dogNo"));
}

function previewAuditEstImg(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById('audit-edit-est-img-circle').src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}

function setAuditEstClosed(e, day) {
  if(e != null)
    e.preventDefault();

  if(document.getElementById(day).className == "btn btn-light") {
    document.getElementById(day).className = "btn btn-primary";
    document.getElementById(day+"-open").value = "";
    document.getElementById(day+"-close").value = "";
    document.getElementById(day+"-open").disabled = true;
    document.getElementById(day+"-close").disabled = true;
  }
  else {
    document.getElementById(day).className = "btn btn-light";
    document.getElementById(day+"-open").disabled = false;
    document.getElementById(day+"-close").disabled = false;
  }
}

function auditEstFeatureClick(el) {
  document.getElementById(el.id).className = "btn btn-primary active";
  document.getElementById(el.id).style = "";

  var feature;

  if(el.id.indexOf("Yes") > -1) {
    feature = el.id.substring(0, el.id.indexOf("Yes"));
    document.getElementById(feature+"No").className = "btn btn-secondary";
    document.getElementById(feature+"No").style.backgroundColor = "rgb(240,240,240)";
    document.getElementById(feature).value = "Yes";
  }
  else if(el.id.indexOf("No") > -1) {
    feature = el.id.substring(0, el.id.indexOf("No"));
    document.getElementById(feature+"Yes").className = "btn btn-secondary";
    document.getElementById(feature+"Yes").style.backgroundColor = "rgb(240,240,240)";
    document.getElementById(feature).value = "No";

    document.getElementById(el.id).style.borderRadius = "5px";
    document.getElementById(el.id).style.borderTopLeftRadius = "0";
    document.getElementById(el.id).style.borderBottomLeftRadius = "0";
  }
}

// review establishment
function auditReviewEst(estId) {
  swal({
      title: "Mark as Reviewed",
      text: "Are you sure you want to mark this establishment as reviewed?",
      icon: "info",
      dangerMode: false,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Mark as Reviewed",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
    }
  })
  .then((review) => {
      var parameters = {
        estId: estId
      }
      $.post('/auditreviewest', parameters, function(newDate) {
        document.getElementById("audit-last-reviewed"+estId).innerHTML = newDate;
      });
  });
}


// AUDIT SPECIAL TOGGLES

function auditToggleAddEditSpecial(e, specialId, estId) {
  $('#auditAddEditSpecialModal').modal({backdrop: 'static', keyboard: false}); 

  document.getElementById("btnSaveAuditSpecial").innerHTML = "Save";
  document.getElementById("btnSaveAuditSpecial").disabled = false;

  document.getElementById("audit-add-edit-special-estId").value = estId;
  document.getElementById("audit-add-edit-special-specialId").value = specialId;
  if(specialId != null)
    document.getElementById("audit-add-edit-special-specialIds").value = document.getElementById("auditAddEditSpecialIdsHidden"+specialId).value;

  // establishment pic & name in header
  document.getElementById("audit-add-edit-special-est-pic").style.backgroundImage = "url('" + document.getElementById("auditEstImgHidden"+estId).value + "')";
  document.getElementById("audit-add-edit-special-est").innerHTML = document.getElementById("auditEstNameHidden"+estId).value;
  document.getElementById("audit-add-edit-special-title").innerHTML = specialId != null ? "Edit Special" : "Add Special";

  // clear inputs
  document.getElementById("audit-daily-day-mon").className = "btn btn-default";
  document.getElementById("auditDailyDaymon").value = false;
  document.getElementById("audit-daily-day-tue").className = "btn btn-default";
  document.getElementById("auditDailyDaytue").value = false;
  document.getElementById("audit-daily-day-wed").className = "btn btn-default";
  document.getElementById("auditDailyDaywed").value = false;
  document.getElementById("audit-daily-day-thu").className = "btn btn-default";
  document.getElementById("auditDailyDaythu").value = false;
  document.getElementById("audit-daily-day-fri").className = "btn btn-default";
  document.getElementById("auditDailyDayfri").value = false;
  document.getElementById("audit-daily-day-sat").className = "btn btn-default";
  document.getElementById("auditDailyDaysat").value = false;
  document.getElementById("audit-daily-day-sun").className = "btn btn-default";
  document.getElementById("auditDailyDaysun").value = false;

  document.getElementById("auditDailyDaymonOrig").value = "false";
  document.getElementById("auditDailyDaytueOrig").value = "false";
  document.getElementById("auditDailyDaywedOrig").value = "false";
  document.getElementById("auditDailyDaythuOrig").value = "false";
  document.getElementById("auditDailyDayfriOrig").value = "false";
  document.getElementById("auditDailyDaysatOrig").value = "false";
  document.getElementById("auditDailyDaysunOrig").value = "false";

  document.getElementById("audit-daily-start").value = "";
  document.getElementById("audit-daily-end").value = "";

  auditPriceType = "$";
  auditPriceTypeId = 300;
  document.getElementById("audit-price").value = "";
  document.getElementById("audit-price").disabled = false;
  document.getElementById("auditMoneyToggleButton").className = "btn btn-default";
  document.getElementById("auditPercentToggleButton").className = "btn btn-default";
  document.getElementById("auditBogoToggleButton").className = "btn btn-default";
  document.getElementById("auditFreeToggleButton").className = "btn btn-default";

  document.getElementById("audit-details").value = "";

  document.getElementById("audit-special-type").value = "";

  for(var i = 0; i <= 15; i++) {
    document.getElementById("audit-special-type-"+i).style.border = "1px solid #ccc";
    document.getElementById("audit-special-type-"+i).style.boxShadow = "none";
  }

  document.getElementById("auditAddEditSpecialErrors").innerHTML = "";
  
  // edit special
  if(specialId != null) {
    var days = document.getElementById("auditAddEditSpecialDaysHidden"+specialId).value;
    days = days.split(",");
    for(var d in days) {
      auditToggleDailyDay(intDayToStringAbbr(days[d]).toLowerCase());
    }

    document.getElementById("auditDailyDaymonOrig").value = days.indexOf("1") > -1 ? "true" : "false";
    document.getElementById("auditDailyDaytueOrig").value = days.indexOf("2") > -1 ? "true" : "false";
    document.getElementById("auditDailyDaywedOrig").value = days.indexOf("3") > -1 ? "true" : "false";
    document.getElementById("auditDailyDaythuOrig").value = days.indexOf("4") > -1 ? "true" : "false";
    document.getElementById("auditDailyDayfriOrig").value = days.indexOf("5") > -1 ? "true" : "false";
    document.getElementById("auditDailyDaysatOrig").value = days.indexOf("6") > -1 ? "true" : "false";
    document.getElementById("auditDailyDaysunOrig").value = days.indexOf("0") > -1 ? "true" : "false";

    var start = dateStrToSelectDate(document.getElementById("auditAddEditSpecialStartTimeHidden"+specialId).value);
    var end = dateStrToSelectDate(document.getElementById("auditAddEditSpecialEndTimeHidden"+specialId).value);
  
    document.getElementById("audit-daily-start").value = start;
    document.getElementById("audit-daily-end").value = end;

    var priceType = document.getElementById("auditAddEditSpecialPriceTypeHidden"+specialId).value;
    auditTogglePriceType(priceType);
    if(priceType == 300 || priceType == 301) {
      document.getElementById("audit-price").value = document.getElementById("auditAddEditSpecialPriceHidden"+specialId).value;
    }

    document.getElementById("audit-details").value = document.getElementById("auditAddEditSpecialDetailsHidden"+specialId).value;

    var dealType = getDealTypeOGRefIdById(document.getElementById("auditAddEditSpecialDealTypeHidden"+specialId).value);
    auditToggleSpecialType(dealType);
  }
}

function auditToggleDailyDay(day) {
  if(document.getElementById("audit-daily-day-"+day).className == "btn btn-default") {
    document.getElementById("audit-daily-day-"+day).className = "btn btn-primary active";
    document.getElementById("auditDailyDay"+day).value = "true";
  }
  else {
    document.getElementById("audit-daily-day-"+day).className = "btn btn-default";
    document.getElementById("auditDailyDay"+day).value = "false";
  }
}

function auditTogglePriceType(type) {
  document.getElementById("audit-price").value = "";

  if (type == 'money' || type == 300) {
    auditPriceType = "$";
    auditPriceTypeId = 300;
    document.getElementById("audit-price").disabled = false;
    document.getElementById("auditMoneyToggleButton").className = "btn btn-primary active";
    document.getElementById("auditPercentToggleButton").className = "btn btn-default";
    document.getElementById("auditBogoToggleButton").className = "btn btn-default";
    document.getElementById("auditFreeToggleButton").className = "btn btn-default";
  }
  else if (type == 'percent' || type == 301) {
    auditPriceType = "%";
    auditPriceTypeId = 301;
    document.getElementById("audit-price").disabled = false;
    document.getElementById("auditMoneyToggleButton").className = "btn btn-default";
    document.getElementById("auditPercentToggleButton").className = "btn btn-primary active";
    document.getElementById("auditBogoToggleButton").className = "btn btn-default";
    document.getElementById("auditFreeToggleButton").className = "btn btn-default";
  } else if (type == 'bogo' || type == 302){
    auditPriceType = "BOGO";
    auditPriceTypeId = 302;
    document.getElementById("audit-price").disabled = true;
    document.getElementById("auditMoneyToggleButton").className = "btn btn-default";
    document.getElementById("auditPercentToggleButton").className = "btn btn-default";
    document.getElementById("auditBogoToggleButton").className = "btn btn-primary active";
    document.getElementById("auditFreeToggleButton").className = "btn btn-default";
  } else if (type == 'free' || type == 307){
    auditPriceType = "Free";
    auditPriceTypeId = 307;
    document.getElementById("audit-price").disabled = true;
    document.getElementById("auditMoneyToggleButton").className = "btn btn-default";
    document.getElementById("auditPercentToggleButton").className = "btn btn-default";
    document.getElementById("auditBogoToggleButton").className = "btn btn-default";
    document.getElementById("auditFreeToggleButton").className = "btn btn-primary active";
  }

  document.getElementById("audit-price").placeholder = auditPriceType;
  document.getElementById("audit-price-type").value = auditPriceTypeId;
}

function auditToggleSpecialType(typeId) {
  document.getElementById("audit-special-type").value = typeId;

  for(var i = 0; i <= 15; i++) {
    if(i == typeId) {
      document.getElementById("audit-special-type-"+i).style.border = "2px solid orange";
      document.getElementById("audit-special-type-"+i).style.boxShadow = "0px 1px 10px grey";
    }
    else {
      document.getElementById("audit-special-type-"+i).style.border = "1px solid #ccc";
      document.getElementById("audit-special-type-"+i).style.boxShadow = "none";
    } 
  }
}

// AUDIT SPECIAL VALIDATE

function auditValidateAddEditSpecial(e) {
  e.preventDefault();

  document.getElementById("btnSaveAuditSpecial").innerHTML = "Saving...";
  document.getElementById("btnSaveAuditSpecial").disabled = true;
  document.body.style.cursor = "wait";

  var errorMsgs = [];

  // common fields
  var price = document.getElementById("audit-price").value;
  var priceType = document.getElementById("audit-price-type").value;
  var details = document.getElementById("audit-details").value;
  var specialType;
  for(var i = 0; i <= 15; i++) {
    if(document.getElementById("audit-special-type-"+i).style.border == "2px solid orange") {
      specialType = i;
    }
  }

  if(priceType == "300") {
    if (price == undefined || price == "") {
      errorMsgs.push("Add a price!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Price needs to be less than $100!");
    }
  }
  else if(priceType == "301") {
    if(price == undefined || price == "") {
      errorMsgs.push("add a percent off!");
    }
    else if(parseFloat(price) > 100) {
      errorMsgs.push("Percent off needs to be equal or less than 100%!");
    }
  }
  else if(priceType == undefined || priceType == "") {
    errorMsgs.push("Select a price type and enter a price!");
  }

  if(details == undefined || details == "") {
    errorMsgs.push("Add some details!");
  }

  if(specialType == undefined) {
    errorMsgs.push("Select a special type!");
  }

  var startTime = document.getElementById("audit-daily-start").value;
  var endTime = document.getElementById("audit-daily-end").value;
  var mon = document.getElementById("audit-daily-day-mon").className == "btn btn-primary active" ? true : false;
  var tue = document.getElementById("audit-daily-day-tue").className == "btn btn-primary active" ? true : false;
  var wed = document.getElementById("audit-daily-day-wed").className == "btn btn-primary active" ? true : false;
  var thu = document.getElementById("audit-daily-day-thu").className == "btn btn-primary active" ? true : false;
  var fri = document.getElementById("audit-daily-day-fri").className == "btn btn-primary active" ? true : false;
  var sat = document.getElementById("audit-daily-day-sat").className == "btn btn-primary active" ? true : false;
  var sun = document.getElementById("audit-daily-day-sun").className == "btn btn-primary active" ? true : false;

  if(!mon && !tue && !wed && !thu && !fri && !sat && !sun) {
    errorMsgs.push("Select at least one day!");
  }

  if(startTime == undefined || startTime == "") {
    errorMsgs.push("Set a start time!");
  }

  if(endTime == undefined || endTime == "") {
    errorMsgs.push("Set an end time!");
  }

  if(startTime != undefined && startTime != "" && endTime != undefined && endTime != "") {
    var sDate = new Date("01-01-2000 " + startTime);
    var eDate = new Date("01-01-2000 " + endTime);

    if(eDate.getHours() < 4)
      eDate.setDate(eDate.getDate()+1);

    if(sDate >= eDate)
      errorMsgs.push("Start time needs to be before end time!");
  }

  if(errorMsgs.length == 0) {
    var params = {};
    const formData = new FormData(document.getElementById("auditAddEditSpecialForm"));
    for(var pair of formData.entries()) {
      params[pair[0]] = pair[1];
    }

    //console.log(params);

    jQuery.ajax({
      url: '/auditaddeditspecial',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      method: 'POST',
      type: 'POST', // For jQuery < 1.9
      success: function(data) {
        $("#auditAddEditSpecialModal").modal("hide");

        // day(s)
        var days = "";
        if(params.dailyDayMon == "true")
          days += "1,";
        if(params.dailyDayTue == "true")
          days += "2,";
        if(params.dailyDayWed == "true")
          days += "3,";
        if(params.dailyDayThu == "true")
          days += "4,";
        if(params.dailyDayFri == "true")
          days += "5,";
        if(params.dailyDaySat == "true")
          days += "6,";
        if(params.dailyDaySun == "true")
          days += "0,";

        days = days.substring(0, days.length-1);

        // price & details
        var priceAndDetails = "";
        if(params.priceType == "300")
          priceAndDetails = "$" + parseFloat(params.price).toFixed(2) + " " + params.details;
        else if(params.priceType == "301")
          priceAndDetails = parseFloat(params.price).toFixed(0) + "% Off " + params.details;
        else if(params.priceType == "302")
          priceAndDetails = "BOGO " + params.details;
        else if(params.priceType == "307")
          priceAndDetails = "Free " + params.details;

        //console.log(params);

        var newSpecialId;
        var specialIds = data.specialIds;

        for(var sid in specialIds) {
          if(document.getElementById("auditAddEditSpecialIdHidden"+specialIds[sid])) {
            newSpecialId = specialIds[sid];
          }
        }

        // special id still exists
        if(newSpecialId) {
          // hidden fields
          document.getElementById("auditAddEditSpecialIdHidden"+newSpecialId).value = newSpecialId;
          document.getElementById("auditAddEditSpecialIdsHidden"+newSpecialId).value = data.specialIds;
          document.getElementById("auditAddEditSpecialDealTypeHidden"+newSpecialId).value = getDealTypeRefIdByOGId(params.specialType);
          document.getElementById("auditAddEditSpecialPriceTypeHidden"+newSpecialId).value = params.priceType;
          document.getElementById("auditAddEditSpecialPriceHidden"+newSpecialId).value = params.price;
          document.getElementById("auditAddEditSpecialDetailsHidden"+newSpecialId).value = params.details;
          document.getElementById("auditAddEditSpecialDaysHidden"+newSpecialId).value = days;
          document.getElementById("auditAddEditSpecialStartTimeHidden"+newSpecialId).value = timeStrToTime(params.dailyStartTime.replace(":", "")).toUpperCase();
          document.getElementById("auditAddEditSpecialEndTimeHidden"+newSpecialId).value = timeStrToTime(params.dailyEndTime.replace(":", "")).toUpperCase();

          // icon
          document.getElementById("auditAddEditSpecialDealType"+newSpecialId).src = "assets/img/"+getDealTypeImageByRefId(getDealTypeRefIdByOGId(params.specialType));

          // price & details
          document.getElementById("auditAddEditSpecialDetails"+newSpecialId).innerHTML = priceAndDetails;

          // day(s)
          document.getElementById("auditAddEditSpecialDays"+newSpecialId).innerHTML = getDaysString(days.split(","));

          // duration
          document.getElementById("auditAddEditSpecialStartTime"+newSpecialId).innerHTML = timeStrToTime(params.dailyStartTime.replace(":", "")).toUpperCase();
          document.getElementById("auditAddEditSpecialEndTime"+newSpecialId).innerHTML = timeStrToTime(params.dailyEndTime.replace(":", "")).toUpperCase();
        }
        // special id no longer exists
        else {
          if(params.specialId) {
            document.getElementById("auditAddEditSpecialRow"+params.specialId).style.display = "none";
          }
          else {

          }

          var addedSpecialRow = "<div id='auditAddEditSpecialRow"+data.specialIds[0]+"'>";

          addedSpecialRow += '<input id="auditAddEditSpecialIdHidden'+data.specialIds[0]+'" value="'+data.specialIds[0]+'" hidden />'+
                            '<input id="auditAddEditSpecialIdsHidden'+data.specialIds[0]+'" value="'+data.specialIds+'" hidden />'+
                            '<input id="auditAddEditSpecialDealTypeHidden'+data.specialIds[0]+'" value="'+getDealTypeRefIdByOGId(params.specialType)+'" hidden />'+
                            '<input id="auditAddEditSpecialPriceTypeHidden'+data.specialIds[0]+'" value="'+params.priceType+'" hidden />'+
                            '<input id="auditAddEditSpecialPriceHidden'+data.specialIds[0]+'" value="'+(params.price != undefined ? params.price : 0)+'" hidden />'+
                            '<input id="auditAddEditSpecialDetailsHidden'+data.specialIds[0]+'" value="'+params.details+'" hidden />'+
                            '<input id="auditAddEditSpecialDaysHidden'+data.specialIds[0]+'" value="'+days+'" hidden />'+
                            '<input id="auditAddEditSpecialStartTimeHidden'+data.specialIds[0]+'" value="'+timeStrToTime(params.dailyStartTime.replace(":", "")).toUpperCase()+'" hidden />'+
                            '<input id="auditAddEditSpecialEndTimeHidden'+data.specialIds[0]+'" value="'+timeStrToTime(params.dailyEndTime.replace(":", "")).toUpperCase()+'" hidden />';

          addedSpecialRow += '<div class="row" style="padding:5px 0; border-bottom:1px solid gainsboro; background-color:white;">'+
                              '<img id="auditAddEditSpecialDealType'+data.specialIds[0]+'" src="assets/img/'+getDealTypeImageByRefId(getDealTypeRefIdByOGId(params.specialType))+'" style="width:34px; margin-right:4px;">'+
                              '<p id="auditAddEditSpecialDetails'+data.specialIds[0]+'" style="display:inline-block; font-size:14px; color:rgb(50,50,50); vertical-align:middle; line-height:1.2; width:200px; margin-right:5px;">'+
                                priceAndDetails+
                              '</p>'+
                              '<div style="display:inline-block; vertical-align:middle; text-align:right; margin-right:4px;">'+
                                '<div style="line-height:1;">'+
                                  '<p id="auditAddEditSpecialDays'+data.specialIds[0]+'" style="color:rgb(50,50,50); font-size:10px;">'+
                                    getDaysString(days.split(","))+
                                  '</p>'+
                                '</div>'+
                                '<div style="line-height:1;">'+
                                  '<p id="auditAddEditSpecialStartTime'+data.specialIds[0]+'" style="display:inline-block; font-size:12px; color:red; width:55px; margin-right:4px;">'+
                                    timeStrToTime(params.dailyStartTime.replace(":", "")).toUpperCase()+
                                  '</p>'+
                                  '<p style="display:inline-block; font-size:12px; color:red; text-align:center; width:8px; margin-right:4px;">-</p>'+
                                  '<p id="auditAddEditSpecialEndTime'+data.specialIds[0]+'" style="display:inline-block; font-size:12px; color:red; width:55px;">'+
                                    timeStrToTime(params.dailyEndTime.replace(":", "")).toUpperCase()+
                                  '</p>'+
                                '</div>'+
                              '</div>'+
                              '<div style="display:inline-block; vertical-align:middle;">'+
                                '<a onclick="auditToggleAddEditSpecial(event, '+data.specialIds[0]+', '+params.estId+')" style="cursor:pointer; padding:0 3px 0 10px; margin-right:5px;"><i class="fa fa-pencil"></i></a>'+
                                '<a onclick="auditDeleteSpecial(event, \''+data.specialIds+'\')" style="cursor:pointer; padding:0 10px 0 3px;"><i class="fa fa-trash" style="color:#d9534f"></i></a>'+
                              '</div>'+
                            '</div>'+
                          '</div>';

          document.getElementById("auditAddedSpecials"+params.estId).innerHTML += addedSpecialRow;
          document.getElementById("audit-special-count"+params.estId).innerHTML = parseInt(document.getElementById("audit-special-count"+params.estId).innerHTML) + 1;
          if(document.getElementById("auditNoSpecialsRow"+params.estId))
            document.getElementById("auditNoSpecialsRow"+params.estId).style.display = "none";
        }

        // last reviewed
        document.getElementById("audit-last-reviewed"+params.estId).innerHTML = data.newDate;

        document.getElementById("btnSaveAuditSpecial").innerHTML = "Save";
        document.getElementById("btnSaveAuditSpecial").disabled = false;
        document.body.style.cursor = "default";
      },
      error: function(err) {
        console.log(err);
        document.getElementById("btnSaveAuditSpecial").innerHTML = "Save";
        document.getElementById("btnSaveAuditSpecial").disabled = false;
        document.body.style.cursor = "default";
      }
    });
  }
  else {
    var errs = "";
    for(var i = 0; i < errorMsgs.length; i++) {
      errs += errorMsgs[i] + "<br/>";
    }
    document.getElementById("auditAddEditSpecialErrors").innerHTML = errs;
    document.getElementById("btnSaveAuditSpecial").innerHTML = "Save";
    document.getElementById("btnSaveAuditSpecial").disabled = false;
    document.body.style.cursor = "default";
  }
}

// AUDIT DELETE SPECIAL

function auditDeleteSpecial(e, specialIds) {
  e.preventDefault();

  swal({
      title: "Delete Special",
      text: "Are you sure you want to delete this special?",
      icon: "warning",
      dangerMode: true,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, delete it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
    }
  })
  .then((willDelete) => {
      if(willDelete) {
        var parameters = {
          specialIds: specialIds
        };

        $.post('/auditdeletespecial', parameters, function(err) {
          //location.reload();
          var specialIdsArr = specialIds.split(",");
          for(var s in specialIdsArr) {
            if(document.getElementById("auditAddEditSpecialRow"+specialIdsArr[s]))
              document.getElementById("auditAddEditSpecialRow"+specialIdsArr[s]).style.display = "none";
          }
        });
      }
  });
}



// AUDIT EVENT TOGGLES

function auditToggleAddEditEvent(e, eventId, estId) {
  $('#auditAddEditEventModal').modal({backdrop: 'static', keyboard: false}); 

  document.getElementById("btnSaveAuditEvent").innerHTML = "Save";
  document.getElementById("btnSaveAuditEvent").disabled = false;

  document.getElementById("auditEventSpecialsDiv").style.display = eventId == null ? "none" : "block";

  document.getElementById("audit-add-edit-event-estId").value = estId;
  document.getElementById("audit-add-edit-event-eventId").value = eventId;
  if(eventId != null)
    document.getElementById("audit-add-edit-event-eventIds").value = document.getElementById("auditAddEditEventIdsHidden"+eventId).value;

  // establishment pic & name in header
  document.getElementById("audit-add-edit-event-est-pic").style.backgroundImage = "url('" + document.getElementById("auditEstImgHidden"+estId).value + "')";
  document.getElementById("audit-add-edit-event-est").innerHTML = document.getElementById("auditEstNameHidden"+estId).value;
  document.getElementById("audit-add-edit-event-header").innerHTML = eventId != null ? "Edit Event" : "Add Event";

  document.getElementById("audit-add-edit-event-title").value = "";
  document.getElementById("audit-add-edit-event-details").value = "";

  document.getElementById("audit-weekly-or-onetime-div").style.display = "block";
  auditToggleWeeklyOneTimeEvent("weekly");
  auditSelectEventDay("mon", false, true);
  auditSelectEventDay("tue", false, true);
  auditSelectEventDay("wed", false, true);
  auditSelectEventDay("thu", false, true);
  auditSelectEventDay("fri", false, true);
  auditSelectEventDay("sat", false, true);
  auditSelectEventDay("sun", false, true);

  document.getElementById("audit-add-edit-event-start-date").value = "";
  document.getElementById("audit-add-edit-event-end-date").value = "";
  document.getElementById("audit-add-edit-event-start-time").value = "";
  document.getElementById("audit-add-edit-event-end-time").value = "";

  document.getElementById("audit-add-specials-div").style.display = "none";

  document.getElementById("auditAddEventSpecialRow").style.display = "none";
  document.getElementById("auditAddEventSpecialToggleMinus").style.display = "none";
  document.getElementById("auditAddEventSpecialTogglePlus").style.display = "inline";

  document.getElementById("auditAddEventSpecial-startTime").value = "";
  document.getElementById("auditAddEventSpecial-endTime").value = "";
  document.getElementById("auditAddEventSpecial-price").value = "";
  document.getElementById("auditAddEventSpecial-details").value = "";
  auditToggleAddEventSpecialPriceType('money');
  
  isAuditAddEventSpecialDealTypeSelected = false;
  auditAddEventSpecialDealTypeId = "";
  document.getElementById("hiddenAuditAddEventSpecialType").value = "";
  for(var i = 0; i <= 15; i++) {
    document.getElementById("auditAddEventSpecial-deal-type-"+i).style.background = "white";
    document.getElementById("auditAddEventSpecial-deal-type-"+i).style.border = "1px solid #c0c0c0";
    document.getElementById("auditAddEventSpecial-deal-type-"+i).style.boxShadow = "none";
  }

  // edit event
  if(eventId != null) {
    document.getElementById("audit-add-edit-event-title").value = document.getElementById("auditAddEditEventTitleHidden"+eventId).value;
    document.getElementById("audit-add-edit-event-details").value = document.getElementById("auditAddEditEventDetailsHidden"+eventId).value;
    var startTime = dateStrToSelectDate(document.getElementById("auditAddEditEventStartTimeHidden"+eventId).value);
    var endTime = dateStrToSelectDate(document.getElementById("auditAddEditEventEndTimeHidden"+eventId).value);
    document.getElementById("audit-add-edit-event-start-time").value = startTime;
    document.getElementById("audit-add-edit-event-end-time").value = endTime;

    var days = document.getElementById("auditAddEditEventDaysHidden"+eventId).value;

    // weekly
    if(days != "-1") {
      auditToggleWeeklyOneTimeEvent("weekly");

      days = days.split(",");
      for(var d in days) {
        auditSelectEventDay(intDayToStringAbbr(days[d]).toLowerCase(), true, false);
      }

      document.getElementById("audit-add-edit-event-day-mon-orig").value = days.indexOf("1") > -1 ? "true" : "false";
      document.getElementById("audit-add-edit-event-day-tue-orig").value = days.indexOf("2") > -1 ? "true" : "false";
      document.getElementById("audit-add-edit-event-day-wed-orig").value = days.indexOf("3") > -1 ? "true" : "false";
      document.getElementById("audit-add-edit-event-day-thu-orig").value = days.indexOf("4") > -1 ? "true" : "false";
      document.getElementById("audit-add-edit-event-day-fri-orig").value = days.indexOf("5") > -1 ? "true" : "false";
      document.getElementById("audit-add-edit-event-day-sat-orig").value = days.indexOf("6") > -1 ? "true" : "false";
      document.getElementById("audit-add-edit-event-day-sun-orig").value = days.indexOf("0") > -1 ? "true" : "false";
    }
    // one-time
    else {
      auditToggleWeeklyOneTimeEvent("onetime");

      var startDate = document.getElementById("auditAddEditEventStartDateHidden"+eventId).value;
      var endDate = document.getElementById("auditAddEditEventEndDateHidden"+eventId).value;

      startDate = new Date(startDate);
      endDate = new Date(endDate);

      startDate = getISOString(startDate);
      endDate = getISOString(endDate);

      document.getElementById("audit-add-edit-event-start-date").value = startDate.substring(0, startDate.indexOf("T"));
      document.getElementById("audit-add-edit-event-end-date").value = endDate.substring(0, endDate.indexOf("T"));
    }

    document.getElementById("audit-weekly-or-onetime-div").style.display = "none";
    document.getElementById("audit-add-specials-div").style.display = "block";





    var html = "";

    var parameters = {
      eventId: eventId
    };

    $.get('/loadevent', parameters, function(data) {

      for(var s in data.specials) {
        var special = data.specials[s];

        if(data.day == -1) {
          var specialDates = "";
          var specialDatesArr = special.special_dates.split(",");

          var daysBetween = 0;
          var startDate = new Date(data.start_time);
          var endDate = new Date(data.end_time);

          while(startDate <= endDate) {
            daysBetween += 1;
            startDate.setDate(startDate.getDate()+1);
          }

          if(daysBetween == 1) {
            specialDates = "";
          }
          else if(daysBetween == specialDatesArr.length) {
            specialDates = "All Days";
          }
          else {
            for(var sd = 0; sd < specialDatesArr.length; sd++) {
              var sdDate = new Date(specialDatesArr[sd]);
              sdDate.addHoursDST();
              specialDates = specialDates + (sdDate.getMonth()+1) + "/" + sdDate.getDate();
              if(sd < specialDatesArr.length-1)
                specialDates = specialDates + ", ";
            }
          }

          var sStart = new Date(special.start_time);
          sStart.addHoursDST();
          sStart = formatStartEndTime(sStart);
          var sEnd = new Date(special.end_time);
          sEnd.addHoursDST();
          sEnd = formatStartEndTime(sEnd);

          html = html + "<div class='row' id='auditUpcomingEventSpecialRow"+special.special_id+"' style='margin-left:0px; margin-right:0px;'>"+
                     "<div class='col col-md-5 col-sm-5 col-xs-5' style='line-height:1.5; padding-left:0;'>"+
                    "<img src='assets/img/"+getDealTypeImageByRefId(special.deal_type_ref_id)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;' />"+
                     "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555;'>";

          if(special.price_type_ref_id == 300) {
            html = html + "$" + special.price.toFixed(2) + " " + special.details;
          }
          else if(special.price_type_ref_id == 301) {
            html = html + special.price.toFixed(0) + "% Off " + special.details;
          }
          else if(special.price_type_ref_id == 302) {
            html = html + "BOGO " + special.details;
          }
          else if(special.price_type_ref_id == 307) {
            html = html + "Free " + special.details;
          }

          html = html + "</p>"+
                 "</div>";

          if(specialDates != "") {
            html = html+
                   "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5; padding-left:0;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555;'>"+specialDates+"</p>"+
                   "</div>";
          }

          html = html+
                  "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5; padding-left:0;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555;'>" + sStart + " - " + sEnd + "</p>"+
                  "</div>"+
                  "<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5; padding-left:0;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                      "<span style='cursor:pointer'>"+
                        "<a onclick='auditDeleteSpecialFromEvent("+special.special_id+")' data-toggle='tooltip' title='Delete Special'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                        "</a>"+
                      "</span>"+
                    "</p>"+
                  "</div>"+
                "</div>";
        }
        else {
          html = html+"<div class='row' id='auditUpcomingEventSpecialRow"+special.special_id+"' style='margin-left:0px; margin-right:0px;'>"+
                      "<div class='col col-md-12' style='line-height:1.5; padding-left:0;'>"+
                   "<img src='assets/img/"+getDealTypeImageByRefId(special.deal_type_ref_id)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:10px;'>";

                    if(special.price_type_ref_id == 300) {
                      html = html + "$" + special.price.toFixed(2) + " " + special.details;
                    }
                    else if(special.price_type_ref_id == 301) {
                      html = html + special.price.toFixed(0) + "% Off " + special.details;
                    }
                    else if(special.price_type_ref_id == 302) {
                      html = html + "BOGO " + special.details;
                    }
                    else if(special.price_type_ref_id == 307) {
                      html = html + "Free " + special.details;
                    }

          html = html+"</p>"+
                   "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                    "<span style='cursor:pointer'>"+
                     "<a id='auditDeleteUpcomingEventSpecial"+special.special_id+"' onclick='auditDeleteSpecialFromEvent("+special.special_id+")' data-toggle='tooltip' title='Delete'>"+
                      "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                     "</a>"+
                    "</span>"+
                   "</p>"+
                  "</div>"+
                   "</div>";
        }
      }

      $("#auditEventSpecialsDiv").html(html);
    });


  }
}

function auditToggleWeeklyOneTimeEvent(weeklyOrOneTime) {
  if(weeklyOrOneTime == 'weekly') {
    document.getElementById("audit-toggle-weekly-event").className = "btn btn-primary active";
    document.getElementById("audit-toggle-onetime-event").className = "btn btn-default";
    
    document.getElementById("audit-add-edit-day-div").style.display = "block";
    document.getElementById("audit-add-edit-event-start-date").style.display = "none";
    document.getElementById("audit-add-edit-event-end-date").style.display = "none";
  }
  else {
    document.getElementById("audit-toggle-weekly-event").className = "btn btn-default";
    document.getElementById("audit-toggle-onetime-event").className = "btn btn-primary active";
    
    document.getElementById("audit-add-edit-day-div").style.display = "none";
    document.getElementById("audit-add-edit-event-start-date").style.display = "block";
    document.getElementById("audit-add-edit-event-end-date").style.display = "block";
  }

  document.getElementById("audit-weekly-or-exclusive-event").value = weeklyOrOneTime;
}

function auditSelectEventDay(day, select, deselect) {
  if(select) {
    document.getElementById("audit-add-edit-event-"+day+"ToggleButton").className = "btn btn-primary active";
    document.getElementById("audit-add-edit-event-day-"+day).value = true;
  }
  else if(deselect) {
    document.getElementById("audit-add-edit-event-"+day+"ToggleButton").className = "btn btn-default";
    document.getElementById("audit-add-edit-event-day-"+day).value = false;
  }
  else {
    if(document.getElementById("audit-add-edit-event-"+day+"ToggleButton").className == "btn btn-default") {
      document.getElementById("audit-add-edit-event-"+day+"ToggleButton").className = "btn btn-primary active";
      document.getElementById("audit-add-edit-event-day-"+day).value = true;
    } else {
      document.getElementById("audit-add-edit-event-"+day+"ToggleButton").className = "btn btn-default";
      document.getElementById("audit-add-edit-event-day-"+day).value = false;
    }
  }

  document.getElementById("auditEvent-dayToggle").style.border = "none";
}

function auditToggleAddEventSpecialDiv() {
  var display = document.getElementById("auditAddEventSpecialRow").style.display;

  // show
  if(display == "none" || display == "") {
    document.getElementById("auditAddEventSpecialRow").style.display = "block";
    document.getElementById("auditAddEventSpecialToggleMinus").style.display = "inline";
    document.getElementById("auditAddEventSpecialTogglePlus").style.display = "none";

    var startDate = document.getElementById("audit-add-edit-event-start-date").value;
    var endDate = document.getElementById("audit-add-edit-event-end-date").value;

    if(startDate != null && startDate != "" && endDate != null && endDate != "") {
      var dayToggles = '';
      startDate = new Date(startDate);
      endDate = new Date(endDate);

      startDate.addHoursDST();
      endDate.addHoursDST();

      while(startDate <= endDate) {
        var id = ""+(startDate.getMonth()+1)+startDate.getDate();
        dayToggles = dayToggles + 
             '<div class="btn-group">'+
                '<button id="audit-day-'+id+'" type="button" style="text-decoration: none" class="btn btn-default" onclick="auditToggleAddEventSpecialDay('+id+')">'+(startDate.getMonth()+1)+'/'+startDate.getDate()+'</button>'+
               '</div>'+
               '<input id="audit-day-'+id+'-hidden" type="hidden" value="'+startDate+'" />';
        startDate.setDate(startDate.getDate()+1);
      }

      document.getElementById("auditAddEventSpecial-dayToggle").innerHTML = dayToggles;
      document.getElementById("auditMultidayDiv").style.display = "block";
    }
    else {
      document.getElementById("auditMultidayDiv").style.display = "none";
    }
  }
  // hide
  else {
    document.getElementById("auditAddEventSpecialRow").style.display = "none";
    document.getElementById("auditAddEventSpecialToggleMinus").style.display = "none";
    document.getElementById("auditAddEventSpecialTogglePlus").style.display = "inline";
  }
}

// AUDIT EVENT VALIDATE

function auditValidateAddEditEvent(e) {
  e.preventDefault();

  document.getElementById("btnSaveAuditEvent").innerHTML = "Saving...";
  document.getElementById("btnSaveAuditEvent").disabled = true;
  document.body.style.cursor = "wait";

  var errorMsgs = [];

  var title = document.getElementById("audit-add-edit-event-title").value;
  var details = document.getElementById("audit-add-edit-event-details").value;
  var weeklyOrOneTime = document.getElementById("audit-weekly-or-exclusive-event").value;
  var startTime = document.getElementById("audit-add-edit-event-start-time").value;
  var endTime = document.getElementById("audit-add-edit-event-end-time").value;

  if(title == undefined || title == "") {
    errorMsgs.push("Set a title!");
  }

  if(startTime == undefined || startTime == "") {
    errorMsgs.push("Set a start time!");
  }

  if(endTime == undefined || endTime == "") {
    errorMsgs.push("Set an end time!");
  }

  // weekly
  if(weeklyOrOneTime == "weekly") {
    var mon = document.getElementById("audit-add-edit-event-day-mon").value == "true" ? true : false;
    var tue = document.getElementById("audit-add-edit-event-day-tue").value == "true" ? true : false;
    var wed = document.getElementById("audit-add-edit-event-day-wed").value == "true" ? true : false;
    var thu = document.getElementById("audit-add-edit-event-day-thu").value == "true" ? true : false;
    var fri = document.getElementById("audit-add-edit-event-day-fri").value == "true" ? true : false;
    var sat = document.getElementById("audit-add-edit-event-day-sat").value == "true" ? true : false;
    var sun = document.getElementById("audit-add-edit-event-day-sun").value == "true" ? true : false;

    if(!mon && !tue && !wed && !thu && !fri && !sat && !sun) {
      errorMsgs.push("Select at least one day!");
    }

    if(startTime != undefined && startTime != "" && endTime != undefined && endTime != "") {
      var sDate = new Date("01-01-2000 " + startTime);
      var eDate = new Date("01-01-2000 " + endTime);

      if(eDate.getHours() < 4)
        eDate.setDate(eDate.getDate()+1);

      if(sDate >= eDate)
        errorMsgs.push("Start time needs to be before end time!");
    }
  }
  // one-time
  else {
    var startDate = document.getElementById("audit-add-edit-event-start-date").value;
    var endDate = document.getElementById("audit-add-edit-event-end-date").value;

    if(startDate == undefined || startDate == "") {
      errorMsgs.push("Set a start date!");
    }

    if(endDate == undefined || endDate == "") {
      errorMsgs.push("Set an end date!");
    }

    if(startDate != undefined && startDate != "" && endDate != undefined && endDate != "" && 
       startTime != undefined && startTime != "" && endTime != undefined && endTime != "") {
      var sDate = new Date(startDate + " " + startTime);
      var eDate = new Date(endDate + " " + endTime);

      if(sDate >= eDate)
        errorMsgs.push("Start time needs to be before end time!");
    }
  }

  if(errorMsgs.length == 0) {
    var params = {};
    const formData = new FormData(document.getElementById("auditAddEditEventForm"));
    for(var pair of formData.entries()) {
      params[pair[0]] = pair[1];
    }

    jQuery.ajax({
      url: '/auditaddeditevent',
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      method: 'POST',
      type: 'POST', // For jQuery < 1.9
      success: function(data) {
        $("#auditAddEditEventModal").modal("hide");

        if(data) {
          var days = "-1";
          var sDateStr = params.startDate;
          var eDateStr = params.endDate;

          if(sDateStr == "") {
            // day(s)
            days = "";
            if(params.eventDayMon == "true")
              days += "1,";
            if(params.eventDayTue == "true")
              days += "2,";
            if(params.eventDayWed == "true")
              days += "3,";
            if(params.eventDayThu == "true")
              days += "4,";
            if(params.eventDayFri == "true")
              days += "5,";
            if(params.eventDaySat == "true")
              days += "6,";
            if(params.eventDaySun == "true")
              days += "0,";

            days = days.substring(0, days.length-1);

            sDateStr = "01/01/1970";
            eDateStr = "01/01/1970";
            var sDate = new Date("01-01-1970 " + params.startTime);
            var eDate = new Date("01-01-1970 " + params.endTime);
            if(eDate.getHours() < 4)
              eDateStr = "01/02/1970";
          }
          else {
            sDateStr = (sDateStr.substring(5) + "/" + sDateStr.substring(0, 4)).replaceAll("-", "/");
            eDateStr = (eDateStr.substring(5) + "/" + eDateStr.substring(0, 4)).replaceAll("-", "/");
          }

          var newEventId;
          var eventIds = data.eventIds;

          for(var evId in eventIds) {
            if(document.getElementById("auditAddEditEventIdHidden"+eventIds[evId])) {
              newEventId = eventIds[evId];
            }
          }

          // current event id still exists
          if(newEventId) {
            // hidden fields
            document.getElementById("auditAddEditEventIdHidden"+newEventId).value = newEventId;
            document.getElementById("auditAddEditEventIdsHidden"+newEventId).value = data.eventIds;
            document.getElementById("auditAddEditEventTitleHidden"+newEventId).value = params.title;
            document.getElementById("auditAddEditEventDetailsHidden"+newEventId).value = params.details;
            document.getElementById("auditAddEditEventDaysHidden"+newEventId).value = days;
            document.getElementById("auditAddEditEventStartDateHidden"+newEventId).value = sDateStr;
            document.getElementById("auditAddEditEventEndDateHidden"+newEventId).value = eDateStr;
            document.getElementById("auditAddEditEventStartTimeHidden"+newEventId).value = timeStrToTime(params.startTime.replace(":", "")).toUpperCase();
            document.getElementById("auditAddEditEventEndTimeHidden"+newEventId).value = timeStrToTime(params.endTime.replace(":", "")).toUpperCase();

            // title
            document.getElementById("auditAddEditEventTitle"+newEventId).innerHTML = params.title;
          
            // date
            if(params.startDate == "")
              document.getElementById("auditAddEditEventDate"+newEventId).innerHTML = getDaysString(days.split(","));
            else 
              document.getElementById("auditAddEditEventDate"+newEventId).innerHTML = sDateStr + " - " + eDateStr;

            // time
            document.getElementById("auditAddEditEventTime"+newEventId).innerHTML = timeStrToTime(params.startTime.replace(":", "")).toUpperCase() + " - " + timeStrToTime(params.endTime.replace(":", "")).toUpperCase();

            // details
            document.getElementById("auditAddEditEventDetails"+newEventId).innerHTML = params.details;
          }
          // event id no longer exists
          else {
            var evSpecialsHtml = "";

            if(params.eventId) {
              //console.log("EDIT");
              evSpecialsHtml = document.getElementById("auditAddEditEventSpecials"+params.eventId).innerHTML;
              document.getElementById("auditAddEditEventRow"+params.eventId).style.display = "none";
            }
            else {
              //console.log("ADD");
            }

            newEventId = data.eventIds[0];

            var newEventDate;
            if(params.startDate == "")
              newEventDate = getDaysString(days.split(","));
            else 
              newEventDate = sDateStr + " - " + eDateStr;

            var addedEventRow = "<div id='auditAddEditEventRow"+newEventId+"'>";

            addedEventRow += '<input id="auditAddEditEventIdHidden'+newEventId+'" value="'+newEventId+'" hidden />'+
                             '<input id="auditAddEditEventIdsHidden'+newEventId+'" value="'+data.eventIds+'" hidden />'+
                             '<input id="auditAddEditEventTitleHidden'+newEventId+'" value="'+params.title+'" hidden />'+
                             '<input id="auditAddEditEventDetailsHidden'+newEventId+'" value="'+params.details+'" hidden />'+
                             '<input id="auditAddEditEventDaysHidden'+newEventId+'" value="'+days+'" hidden />'+
                             '<input id="auditAddEditEventStartDateHidden'+newEventId+'" value="'+sDateStr+'" hidden />'+
                             '<input id="auditAddEditEventEndDateHidden'+newEventId+'" value="'+eDateStr+'" hidden />'+
                             '<input id="auditAddEditEventStartTimeHidden'+newEventId+'" value="'+timeStrToTime(params.startTime.replace(":", "")).toUpperCase()+'" hidden />'+
                             '<input id="auditAddEditEventEndTimeHidden'+newEventId+'" value="'+timeStrToTime(params.endTime.replace(":", "")).toUpperCase()+'" hidden />';

            addedEventRow += '<div id="auditAddEditEventRow'+newEventId+'" class="row" style="padding:10px; border-bottom:1px solid gainsboro; background-color:white;">'+
                                '<div class="row">'+
                                  '<div class="col col-md-8 col-sm-8 col-xs-8" style="padding-left:0;">'+
                                    '<p id="auditAddEditEventTitle'+newEventId+'" style="color:rgb(50,50,50); font-size:14px; font-weight:500; display:inline-block;">'+
                                      params.title+
                                    '</p>'+
                                    '<a id="btnAuditEditEvent'+newEventId+'" onclick="auditToggleAddEditEvent(event, '+newEventId+', '+params.estId+')" style="cursor:pointer; padding:0 3px 0 10px; display:inline-block;"><i class="fa fa-pencil"></i></a>'+
                                    '<a id="btnAuditDeleteEvent'+newEventId+'" onclick="auditDeleteEvent(event, \''+newEventId+'\')" style="cursor:pointer; padding:0 10px 0 3px; display:inline-block;"><i class="fa fa-trash" style="color:#d9534f"></i></a>'+
                                  '</div>'+
                                  '<div class="col col-md-4 col-sm-4 col-xs-4" style="padding:0;">'+
                                    '<p id="auditAddEditEventDate'+newEventId+'" style="font-size:10px; line-height:1; color:rgb(50,50,50); text-align:right;">'+
                                      newEventDate+
                                    '</p>'+
                                    '<p id="auditAddEditEventTime'+newEventId+'" style="font-size:12px; color:red; text-align:right;">'+
                                      timeStrToTime(params.startTime.replace(":", "")).toUpperCase() + " - " + timeStrToTime(params.endTime.replace(":", "")).toUpperCase()+
                                    '</p>'+
                                  '</div>'+
                                '</div>'+
                                '<div class="row" style="margin-bottom:5px;">'+
                                  '<div class="col col-md-12 col-sm-12 col-xs-12" style="padding:0;">'+
                                    '<p id="auditAddEditEventDetails'+newEventId+'" style="font-size:12px; line-height:1.2; width:360px;">'+
                                      params.details+
                                    '</p>'+
                                  '</div>'+
                                '</div>'+
                                '<div id="auditAddEditEventSpecials'+newEventId+'">'+
                                  evSpecialsHtml+
                                '</div>'+
                             '</div>';

            document.getElementById("auditAddedEvents"+params.estId).innerHTML += addedEventRow;
            document.getElementById("audit-event-count"+params.estId).innerHTML = parseInt(document.getElementById("audit-event-count"+params.estId).innerHTML) + 1;
            if(document.getElementById("auditNoEventsRow"+params.estId))
              document.getElementById("auditNoEventsRow"+params.estId).style.display = "none";
          }
        }

        document.getElementById("btnSaveAuditEvent").innerHTML = "Save";
        document.getElementById("btnSaveAuditEvent").disabled = false;
        document.body.style.cursor = "default";
      },
      err: function(postErr) {
        console.log(postErr);
        document.getElementById("btnSaveAuditEvent").innerHTML = "Save";
        document.getElementById("btnSaveAuditEvent").disabled = false;
        document.body.style.cursor = "default";
      }
    });

  }
  else {
    var errs = "";
    for(var i = 0; i < errorMsgs.length; i++) {
      errs += errorMsgs[i] + "<br/>";
    }
    document.getElementById("auditEventErrors").innerHTML = errs;
    document.getElementById("btnSaveAuditEvent").innerHTML = "Save";
    document.getElementById("btnSaveAuditEvent").disabled = false;
    document.body.style.cursor = "default";
  }
}

// AUDIT DELETE EVENT

function auditDeleteEvent(e, eventIds) {
  e.preventDefault();

  var eventIdsArr = eventIds.split(",");
  var eid;
  var row;
  for(var ev in eventIdsArr) {
    if(document.getElementById("auditAddEditEventRow"+eventIdsArr[ev])) {
      eid = eventIdsArr[ev];
      row = document.getElementById("auditAddEditEventRow"+eventIdsArr[ev]);
      row.style.backgroundColor = "rgba(210,0,0,0.2)";
      break;
    }
  }

  swal({
      title: "Delete Event",
      text: "Are you sure you want to delete this event?",
      icon: "warning",
      dangerMode: true,
      buttons: {
        cancel: {
          text: "Cancel",
          value: null,
          visible: true,
          className: "",
          closeModal: true,
        },
        confirm: {
          text: "Yes, delete it!",
          value: true,
          visible: true,
          className: "",
          closeModal: true
        }
    }
  })
  .then((willDelete) => {
      if(willDelete) {
        document.body.style.cursor = "wait";
        document.getElementById("btnAuditEditEvent"+eid).style.display = "none";
        document.getElementById("btnAuditDeleteEvent"+eid).style.display = "none";

        var parameters = {
          eventIds: eventIds
        };

        $.post('/auditdeleteevent', parameters, function(err) {
          //location.reload();
          document.body.style.cursor = "default";
          document.getElementById("auditAddEditEventRow"+eid).style.display = "none";
        });
      }
      else {
        row.style.backgroundColor = "white";
      }
  });
}

// AUDIT EVENT SPECIAL TOGGLES

function auditToggleAddEventSpecialPriceType(type) {
  document.getElementById("auditAddEventSpecial-price").value = "";
  document.getElementById("auditAddEventSpecial-price").className = "form-control";

  if (type == 'money') {
    auditAddEventSpecialPriceType = "$";
    auditAddEventSpecialPriceTypeId = 300;
    document.getElementById("auditAddEventSpecial-price").disabled = false;
    //document.getElementById("auditAddEventSpecial-priceLabel").innerHTML = "Price";
    document.getElementById("auditAddEventSpecial-percentSignInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-moneySignInPriceTextbox").style.display = "block";
    document.getElementById("auditAddEventSpecial-bogoInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-freeInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-moneyToggleButton").className = "btn btn-primary active";
    document.getElementById("auditAddEventSpecial-percentToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-bogoToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-freeToggleButton").className = "btn btn-default";
  }
  else if (type == 'percent') {
    auditAddEventSpecialPriceType = "%";
    auditAddEventSpecialPriceTypeId = 301;
    document.getElementById("auditAddEventSpecial-price").disabled = false;
    //document.getElementById("auditAddEventSpecial-priceLabel").innerHTML = "Percent Off";
    document.getElementById("auditAddEventSpecial-percentSignInPriceTextbox").style.display = "block";
    document.getElementById("auditAddEventSpecial-moneySignInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-bogoInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-freeInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-moneyToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-percentToggleButton").className = "btn btn-primary active";
    document.getElementById("auditAddEventSpecial-bogoToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-freeToggleButton").className = "btn btn-default";
  } else if (type == 'bogo'){
    auditAddEventSpecialPriceType = "Buy One Get One";
    auditAddEventSpecialPriceTypeId = 302;
    document.getElementById("auditAddEventSpecial-price").disabled = true;
    //document.getElementById("auditAddEventSpecial-priceLabel").innerHTML = "BOGO";
    document.getElementById("auditAddEventSpecial-percentSignInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-moneySignInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-bogoInPriceTextbox").style.display = "block";
    document.getElementById("auditAddEventSpecial-freeInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-moneyToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-percentToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-bogoToggleButton").className = "btn btn-primary active";
    document.getElementById("auditAddEventSpecial-freeToggleButton").className = "btn btn-default";
  } else if (type == 'free'){
    auditAddEventSpecialPriceType = "Free";
    auditAddEventSpecialPriceTypeId = 307;
    document.getElementById("auditAddEventSpecial-price").disabled = true;
    //document.getElementById("auditAddEventSpecial-priceLabel").innerHTML = "Free";
    document.getElementById("auditAddEventSpecial-percentSignInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-moneySignInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-bogoInPriceTextbox").style.display = "none";
    document.getElementById("auditAddEventSpecial-freeInPriceTextbox").style.display = "block";
    document.getElementById("auditAddEventSpecial-moneyToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-percentToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-bogoToggleButton").className = "btn btn-default";
    document.getElementById("auditAddEventSpecial-freeToggleButton").className = "btn btn-primary active";
  }

  document.getElementById("hiddenAuditAddEventSpecialPriceType").value = auditAddEventSpecialPriceTypeId;

  document.getElementById("auditAddEventSpecial-price").style.backgroundColor = "#fff";
  document.getElementById("auditAddEventSpecial-price").style.border = "1px solid #ccc";
  if(document.getElementById("auditAddEventSpecial-priceError")) {
    document.getElementById("auditAddEventSpecial-priceError").style.display = "none";
  }
}

function auditChooseAddEventSpecialDealType(typeID, eventId) {
  isAuditAddEventSpecialDealTypeSelected = false;
  auditAddEventSpecialDealTypeId = getDealTypeRefIdByOGId(typeID);

  document.getElementById("hiddenAuditAddEventSpecialType").value = auditAddEventSpecialDealTypeId;

  var dealTypeString = "auditAddEventSpecial-deal-type-";

  for (var i = 0; i <= 15; i++) {
    if (i == typeID) {
        document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
        document.getElementById(dealTypeString+i).style.border = "2px solid orange";
        document.getElementById(dealTypeString+i).value = auditAddEventSpecialDealTypeId;
        isAuditAddEventSpecialDealTypeSelected = true;
    } else {
        document.getElementById(dealTypeString+i).style.background = "white";
        document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
        document.getElementById(dealTypeString+i).value = 0;
    }
  }

  if(document.getElementById("auditAddEventSpecial-dealTypeError")) {
    document.getElementById("auditAddEventSpecial-dealTypeError").style.display = "none";
  }
}

function auditToggleAddEventSpecialDay(dt) {
  
  /*var dayButtons = document.getElementById("auditAddEventSpecial-dayToggle").getElementsByTagName("button");
  for(var d in dayButtons) {
    if(dayButtons[d].style) {
      dayButtons[d].style.backgroundColor = "#fff";
      dayButtons[d].style.border = "1px solid #ccc";
    }
  }*/
  
  if(document.getElementById("audit-day-"+dt).className == "btn btn-default") {
    document.getElementById("audit-day-"+dt).className = "btn btn-primary active";
    document.getElementById("audit-day-"+dt).style.backgroundColor = "#286090";
    document.getElementById("audit-day-"+dt).style.border = "1px solid #204d74";
  } 
  else {
    document.getElementById("audit-day-"+dt).className = "btn btn-default";
    document.getElementById("audit-day-"+dt).style.backgroundColor = "#fff";
    document.getElementById("audit-day-"+dt).style.border = "1px solid #ccc";
  }
}

// AUDIT ADD EVENT SPECIAL

function addSpecialToAuditEvent() {
  var eventIds = document.getElementById("audit-add-edit-event-eventIds").value;
  eventIds = eventIds.split(",");

  var days = [];
  var daysStr = [];
  var content = document.getElementById("auditAddEventSpecial-dayToggle");
  var dayButtons = content.getElementsByTagName("button");
  for(var i = 0; i < dayButtons.length; i++) {
    if(dayButtons[i].className.indexOf("active") > -1) {
      dayInput = document.getElementById(dayButtons[i].id+"-hidden");
      days.push(dayInput.value);
      daysStr.push(dayButtons[i].innerHTML);
    }
  }

  var startTime = document.getElementById("auditAddEventSpecial-startTime").value;
  var endTime = document.getElementById("auditAddEventSpecial-endTime").value;

  var price = document.getElementById("auditAddEventSpecial-price").value;
  var details = document.getElementById("auditAddEventSpecial-details").value;

  var parameters = {
    estId: document.getElementById("audit-add-edit-event-estId").value,
    eventId: eventIds[0],
    days: days,
    startTime: startTime,
    endTime: endTime,
    priceType: auditAddEventSpecialPriceTypeId,
    price: price,
    details: details,
    specialType: auditAddEventSpecialDealTypeId
  };

  if(auditValidateAddSpecialToEvent(parameters)) {
    $.get('/adminaddspecialtoevent', parameters, function(data) {
      if(data) {
        var htmlRow = "";
        var specialDays = "";

        if(dayButtons.length == 1) {
          specialDates = "";
        }
        else if(daysStr.length != dayButtons.length) {
          for(var d = 0; d < daysStr.length; d++) {
            specialDays = specialDays + daysStr[d];
            if(d < daysStr.length-1)
              specialDays = specialDays + ", ";
          }
        }
        else {
          specialDays = "All Days";
        }

        if(data.specialIds) {
          htmlRow = htmlRow+
                    "<div class='row' id='auditEditEventSpecialRow"+data.specialIds[0]+"' style='margin-left:0px; margin-right:0px;'>"+
                      "<div class='col col-md-5 col-sm-5 col-xs-5' style='line-height:1.5; padding-left:0;'>"+
                        "<img src='assets/img/"+getDealTypeImageByRefId(auditAddEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
                          "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555;'>";

                   if(auditAddEventSpecialPriceTypeId == 300)
                    htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
                   else if(auditAddEventSpecialPriceTypeId == 301)
                    htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
                   else if(auditAddEventSpecialPriceTypeId == 302)
                    htmlRow = htmlRow+"BOGO "+details;
                   else if(auditAddEventSpecialPriceTypeId == 307)
                    htmlRow = htmlRow+"Free "+details;

          htmlRow = htmlRow+"</p>"+
                    "</div>";

          if(specialDays != "") {
            htmlRow = htmlRow+
                      "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5; padding-left:0;'>"+
                       "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555;'>"+specialDays+"</p>"+
                      "</div>";
          }
          htmlRow = htmlRow+
                    "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5; padding-left:0;'>"+
                     "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555;'>"+formatStartEndTime(new Date("01/01/2020 " + startTime)) + " - " + formatStartEndTime(new Date("01/01/2020 " + endTime))+"</p>"+
                    "</div>"+
                    "<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5; padding-left:0;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                      "<span style='cursor:pointer'>"+
                      "<a id='adminDeleteEventSpecial"+data.specialIds[0]+"' onclick='adminDeleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
                        "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                      "</a>"+
                      "</span>"+
                    "</p>"+
                    "</div>"+
                   "</div>";
        }
        else {
          htmlRow = htmlRow+"<div class='row' id='adminEditEventSpecialRow"+data.specialId+"' style='margin-left:0px; margin-right:0px;'>"+
                      "<div class='col col-md-12' style='line-height:1.5; padding-left:0;'>"+
                   "<img src='assets/img/"+getDealTypeImageByRefId(auditAddEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
                    "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:10px;'>";

                   if(auditAddEventSpecialPriceTypeId == 300)
                    htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
                   else if(auditAddEventSpecialPriceTypeId == 301)
                    htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
                   else if(auditAddEventSpecialPriceTypeId == 302)
                    htmlRow = htmlRow+"BOGO "+details;
                   else if(auditAddEventSpecialPriceTypeId == 307)
                    htmlRow = htmlRow+"Free "+details;

          htmlRow = htmlRow+"</p>"+
                   "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                    "<span style='cursor:pointer'>"+
                     "<a id='adminDeleteEventSpecial"+data.specialId+"' onclick='adminDeleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
                      "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                     "</a>"+
                    "</span>"+
                   "</p>"+
                  "</div>"+
                   "</div>";
        }

        document.getElementById("auditEventSpecialsDiv").innerHTML += htmlRow;

        var newRow = htmlRow;

        newRow = newRow.replace("style='margin-left:0px; margin-right:0px;'", "");
        newRow = newRow.replace("style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'", "style='display:inline; vertical-align:middle;'");

        var todayRow = newRow;

        if(data.specialIds) {
          newRow = newRow.replace("id='auditEditEventSpecialRow"+data.specialIds[0]+"'", "id='auditAddedEventSpecialRow"+data.specialIds[0]+"'");
          newRow = newRow.replace("<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
                       "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='auditDeleteEventSpecial"+data.specialIds[0]+"' onclick='auditDeleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>"+
                      "</div>", "");
          todayRow = todayRow.replace("id='auditEditEventSpecialRow"+data.specialIds[0]+"'", "id='auditAddedEventSpecialRow"+data.specialIds[0]+"'");
          todayRow = todayRow.replace("<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
                       "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='auditDeleteEventSpecial"+data.specialIds[0]+"' onclick='auditDeleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>"+
                      "</div>", "");
        }
        else {
          newRow = newRow.replace("id='auditEditEventSpecialRow"+data.specialId+"'", "id='auditAddedEventSpecialRow"+data.specialId+"'");
          newRow = newRow.replace("<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='auditDeleteEventSpecial"+data.specialId+"' onclick='auditDeleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>", "");
          todayRow = todayRow.replace("id='auditEditEventSpecialRow"+data.specialId+"'", "id='auditAddedEventSpecialRow"+data.specialId+"'");
          todayRow = todayRow.replace("<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
                        "<span style='cursor:pointer'>"+
                         "<a id='auditDeleteEventSpecial"+data.specialId+"' onclick='auditDeleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
                          "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
                         "</a>"+
                        "</span>"+
                       "</p>", "");
        }

        
        for(var i = 0; i < eventIds.length; i++) {
          var el = "auditManageEventsAddedSpecials"+eventIds[i];
          var el2 = "auditEventsAddedSpecials"+eventIds[i];
          var noSpecRow = "auditManageEventsNoSpecialsRow"+eventIds[i];

          if(document.getElementById(el))
            document.getElementById(el).innerHTML += newRow;

          if(document.getElementById(el2))
            document.getElementById(el2).innerHTML += todayRow;

          if(document.getElementById(noSpecRow))
            document.getElementById(noSpecRow).style.display = "none";
        }
        

        auditToggleAddEventSpecialPriceType('money');
        document.getElementById("auditAddEventSpecial-details").value = "";
        auditChooseAddEventSpecialDealType(-1);
        auditToggleAddEventSpecialDiv();
      }
    });
  }
}

function auditValidateAddSpecialToEvent(p) {
  var valid = true;
  var msg = [];
  var cnt = 0;
  document.getElementById("addSpecialToAuditEventErrors").innerHTML = "";

  // multiday
  if(document.getElementById("auditMultidayDiv").style.display != "none") {
    // days
    if(p.days.length == 0) {
      var dayButtons = document.getElementById("auditAddEventSpecial-dayToggle").getElementsByTagName("button");
      for(var d in dayButtons) {
        if(dayButtons[d].style) {
          dayButtons[d].style.backgroundColor = "#FFEAEA";
          dayButtons[d].style.border = "1px solid #FF0000";
        }
      }

      msg[cnt++] = "<p id='auditAddEventSpecial-daysError'>Choose the day(s) the special will occur!</p>";
      valid = false;
    }

    // start time
    if(p.startTime == "") {
      document.getElementById("auditAddEventSpecial-startTime").style.backgroundColor = "#FFEAEA";
      document.getElementById("auditAddEventSpecial-startTime").style.border = "1px solid #FF0000";
      msg[cnt++] = "<p id='auditAddEventSpecial-startTimeError'>Add a start time!</p>";
      valid = false;
    }
    else {
      document.getElementById("auditAddEventSpecial-startTime").style.backgroundColor = "#fff";
      document.getElementById("auditAddEventSpecial-startTime").style.border = "1px solid #ccc";
    }

    // end time
    if(p.endTime == "") {
      document.getElementById("auditAddEventSpecial-endTime").style.backgroundColor = "#FFEAEA";
      document.getElementById("auditAddEventSpecial-endTime").style.border = "1px solid #FF0000";
      msg[cnt++] = "<p id='auditAddEventSpecial-endTimeError'>Add an end time!</p>";
      valid = false;
    }
    else {
      document.getElementById("auditAddEventSpecial-endTime").style.backgroundColor = "#fff";
      document.getElementById("auditAddEventSpecial-endTime").style.border = "1px solid #ccc";
    }
  }

  // price
  if(p.price == "" && (p.priceType == 300 || p.priceType == 301)) {
    document.getElementById("auditAddEventSpecial-price").style.backgroundColor = "#FFEAEA";
    document.getElementById("auditAddEventSpecial-price").style.border = "1px solid #FF0000";
    msg[cnt++] = "<p id='auditAddEventSpecial-priceError'>Add a price!</p>";
    valid = false;
  }
  else {
    document.getElementById("auditAddEventSpecial-price").style.backgroundColor = "#fff";
    document.getElementById("auditAddEventSpecial-price").style.border = "1px solid #ccc";
  }

  // details
  if(p.details == "") {
    document.getElementById("auditAddEventSpecial-details").style.backgroundColor = "#FFEAEA";
    document.getElementById("auditAddEventSpecial-details").style.border = "1px solid #FF0000";
    msg[cnt++] = "<p id='auditAddEventSpecial-detailsError'>Add some details!</p>";
    valid = false;
  }
  else {
    document.getElementById("auditAddEventSpecial-details").style.backgroundColor = "#fff";
    document.getElementById("auditAddEventSpecial-details").style.border = "1px solid #ccc";
  }

  // icon
  if(p.specialType == "") {
    for(var i = 0; i <= 15; i++) {
      document.getElementById("auditAddEventSpecial-deal-type-"+i).style.border = "1px solid #FF0000";
    }
    msg[cnt++] = "<p id='auditAddEventSpecial-dealTypeError'>Choose an icon!</p>";
    valid = false;
  }
  else {
    for(var i = 0; i <= 15; i++) {
      document.getElementById("auditAddEventSpecial-deal-type-"+i).style.border = "1px solid #c0c0c0";
    }
  }

  if(!valid) {
    for(var m in msg) {
      document.getElementById("addSpecialToAuditEventErrors").innerHTML += msg[m];
    }
  }

  return valid;
}

// AUDIT DELETE EVENT SPECIAL

function auditDeleteSpecialFromEvent(specialId) {
  var parameters = {
    specialId: specialId
  };

  $.get('/deletespecialfromevent', parameters, function(data) {
    if(data && data.length > 0) {
      for(var d in data) {
        x = document.getElementById("auditEditEventSpecialRow"+data[d]);
        if(x != null && x != undefined) {
          x.style.display = "none";
        }

        x = document.getElementById("auditUpcomingEventSpecialRow"+data[d]);
        if(x != null && x != undefined) {
          x.style.display = "none";
        }

        x = document.getElementById("auditEventSpecialRow"+data[d]);
        if(x != null && x != undefined) {
          x.style.display = "none";
        }
      }
    }
  });
}




function auditValidateField(field) {
  if(field.value != "") {
    field.style.backgroundColor = "#fff";
    field.style.border = "1px solid #ccc";
    if(document.getElementById(field.id+"Error")) {
      document.getElementById(field.id+"Error").style.display = "none";
    }
  }
}

function dateStrToSelectDate(dt) {
  var ampm = "AM";
  if(dt.indexOf("PM") > -1) {
    ampm = "PM";
  }
  dt = dt.replace("AM", "").replace("PM", "");

  var hr = parseInt(dt.substring(0, dt.indexOf(":")));
  var min = dt.substring(dt.indexOf(":")+1);

  if(ampm == "AM" && hr != 12) {
    if(hr < 10)
      hr = "0" + hr;
    dt = hr + ":" + min;
  }
  else if(ampm == "AM" && hr == 12) {
    dt = "00:" + min;
  }
  else if(ampm == "PM" && hr != 12) {
    dt = (hr + 12) + ":" + min;
  }
  else if(ampm == "PM" && hr == 12) {
    dt = hr + ":" + min;
  }

  return dt;
}

function timeStrToTime(time) {
  try {
    if(time && time.length == 4) {
      var h = time.substring(0, 2);
      var m = time.substring(2);
      var ampm = "am";

      h = parseInt(h);

      if(h == 0) {
        h = 12;
      }
      else if(h == 12) {
        ampm = "pm";
      }
      else if(h > 12) {
        h = h - 12;
        ampm = "pm";
      }

      return h + ":" + m + ampm;
    }
    else {
      return time;
    }
  }
  catch(e) {
    return time;
  }
}

function getDaysString(days) {
  days = days.sort();
  var dayStr = "";

  if(days.length == 7) {
    dayStr = "All Days";
  }
  else if(days.length == 6) {
    if(days[0] == 0) {
      for(var d = 0; d < days.length - 1; d++) {
        var temp = days[d];
        days[d] = days[d+1];
        days[d+1] = temp;
      }
    }
    dayStr = intDayToStringAbbr(days[0]) + " - " + intDayToStringAbbr(days[5]);
  }
  else if(days.length >= 3) {
    var inARow = true;
    for(var d = 0; d < days.length - 1; d++) {
      if(Math.abs(days[d+1] - days[d]) != 1) {
        inARow = false;
      }
    }
    if(inARow) {
      if(days[0] == 0) {
        for(var d = 0; d < days.length - 1; d++) {
          var temp = days[d];
          days[d] = days[d+1];
          days[d+1] = temp;
        }
      }
      dayStr = intDayToStringAbbr(days[0]) + " - " + intDayToStringAbbr(days[days.length-1]);
    }
    else {
      if(days[0] == 0) {
        for(var d = 0; d < days.length - 1; d++) {
          var temp = days[d];
          days[d] = days[d+1];
          days[d+1] = temp;
        }
      }
      for(var d = 0; d < days.length; d++) {
        dayStr += intDayToStringAbbr(days[d]);
        if(d < days.length - 2) {
          dayStr += ", ";
        }
        else if(d == days.length - 2) {
          dayStr += " & ";
        }
      }
    }
  }
  else {
    if(days[0] == 0) {
      for(var d = 0; d < days.length - 1; d++) {
        var temp = days[d];
        days[d] = days[d+1];
        days[d+1] = temp;
      }
    }
    for(var d = 0; d < days.length; d++) {
      dayStr += intDayToStringAbbr(days[d]);
      if(d < days.length - 2) {
        dayStr += ", ";
      }
      else if(d == days.length - 2) {
        dayStr += " & ";
      }
    }
  }
  return dayStr;
}