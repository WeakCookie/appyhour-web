jQuery(document).ready(checkContainerDash);

function checkContainerDash () {
    if($('#upcomingSoeDateRange').is(':visible')) { //if the container is visible on the page
        initDash();  
    } else {
        setTimeout(checkContainerDash, 50); //wait 50 ms, then try again
    }
}

function initDash() {
	var curr = newDate();
	var currPlus7 = newDate();
	currPlus7.setDate(currPlus7.getDate()+7);

    $('#upcomingSoeDateRange').val(getMMDDYYYY(curr) + " - " + getMMDDYYYY(currPlus7));

    $('#upcomingSoeDateRange').daterangepicker({
    	minDate: curr,
		opens: 'left'
	}, function(start, end, label) {
	    var parameters = { 
			startDate: start.format('MM/DD/YYYY'),
			endDate: end.format('MM/DD/YYYY')
		};

		$.get('/upcomingsoefilter', parameters, function(data) {
			setupUpcomingSoeTable(data);
		});
	});
}

function selectTypeFilter(type) {
	var btn = "upcomingSoeFilter"+type;
	var parameters;

	if(document.getElementById(btn).className == "btn btn-default active") {
		document.getElementById(btn).className = "btn btn-default";
		document.getElementById(btn).style.backgroundColor = "white";
		document.getElementById(btn).style.borderColor = "#ccc";
		if(type == 'Specials') {
			parameters = { showSpecials: false };
		}
		else if(type == 'Events') {
			parameters = { showEvents: false };
		}
	}
	else {
		document.getElementById(btn).className = "btn btn-default active";
		document.getElementById(btn).style.backgroundColor = "#e6e6e6";
		document.getElementById(btn).style.borderColor = "#adadad";
		if(type == 'Specials') {
			parameters = { showSpecials: true };
		}
		else if(type == 'Events') {
			parameters = { showEvents: true };
		}
	}

	$.get('/upcomingsoefilter', parameters, function(data) {
		setupUpcomingSoeTable(data);
	});
}

function upcomingSoeSearch(event) {
	if(event == null || (event != null && (event.keyCode == 13 || event.which == 13))) {
		var parameters = { search: document.getElementById("upcomingSoeSearch").value };
		$.get('/upcomingsoefilter', parameters, function(data) {
			setupUpcomingSoeTable(data);
		});
	}
}

function upcomingSoeClear() {
	document.getElementById("upcomingSoeSearch").value = "";
	upcomingSoeSearch(null);
}

function setupUpcomingSoeTable(data) {
	var html = "<thead style='background-color:rgba(192, 192, 192, 0.3); color:black;'>"+
			   "<tr><td>Date</td><td>Type</td><td>Details</td><td>Duration</td><td></td></tr>"+
			   "</thead>";

	if(data.length > 0) {
		html = html + "<tbody>";
	}	

	for(var i in data) {
		var soe = data[i];

		if(soe.current) {
			html = html + "<tr class='active-special'>";
		}
		else if(soe.upcoming) {
			html = html + "<tr class='upcoming-special'>";
		}

		html = html + "<td class='ts-date-cell' style='vertical-align:middle;'>"+soe.soeDate+"</td>";

		html = html + "<td class='ts-icon-cell' style='vertical-align:middle; padding-left:13px;'>"+
					  "<div class='deal-type-icon'>";
		if(soe.deal_type_ref_id_TS) {
			html = html + "<img src='assets/img/"+soe.deal_type_ref_id_TS+"'>";
		}
		else {
			html = html + "<img src='assets/img/event-star3.png'>";
		}
		html = html + "</div></td>";

		html = html + "<td class='ts-details-cell' style='vertical-align:middle;'>"+
					  "<div class='details'>";
		if(soe.special_id) {
			if(soe.price_type_ref_id == 300) {
				html = html + "$" + soe.price.toFixed(2) + " " + soe.details;
			}
			else if(soe.price_type_ref_id == 301) {
				html = html + soe.price.toFixed(0) + "% Off " + soe.details;
			}
			else if(soe.price_type_ref_id == 302) {
				html = html + "BOGO " + soe.details;
			}
			else if(soe.price_type_ref_id == 307) {
				html = html + "Free " + soe.details;
			}
		}
		else {
			html = html + soe.title;
		}
		html = html + "</div></td>";

		html = html + "<td class='ts-time-cell' style='vertical-align:middle;'>"+
					  "<div class='ends-at'>";
		if(soe.current) {
			html = html + soe.timeremaining;
		}
		else {
			html = html + soe.duration;
		}
		html = html + "</div></td>";

		if(soe.reoccurringStr) {
			html = html + "<td class='ts-recurring-cell' style='vertical-align:middle;'>"+
					  	  "<i class='fa fa-retweet' style='margin-right:1px;'></i>"+
					  	  soe.reoccurringStr+
					  	  "</td>";
		}
		else {
			html = html + "<td></td>";
		}

		html = html + "</tr>"
	}
					  		
	if(data.length > 0) {
		html = html + "</tbody>";
	}

	$('#upcomingSoeTable').html(html);
}


/* Common */

function getMMDDYYYY(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
    return month + "/" + day + "/" + year;
}

