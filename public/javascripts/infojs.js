function editInfo() {
	document.getElementById("btnSaveEstInfoTop").style.display = "inline-block";

	$("#info-edit-phone").inputmask({"mask": "(999) 999-9999"});

    document.getElementById("displayEstablishmentInfo").style.display = "none";
    document.getElementById("editEstablishmentInfo").style.display = "block";
    document.getElementById("btnEditEstInfo").style.display = "none";
    document.getElementById("btnCancelEditInfo").style.display = "inline-block";

    // set closed hours
	if (document.getElementById("mon-open").value == "") {
		setClosedCheckbox("mon");
		//document.getElementById("mon").checked = true;
	}
	if (document.getElementById("tue-open").value == "") {
		setClosedCheckbox("tue");
		//document.getElementById("tue").checked = true;
	}
	if (document.getElementById("wed-open").value == "") {
		setClosedCheckbox("wed");
		//document.getElementById("wed").checked = true;
	}
	if (document.getElementById("thu-open").value == "") {
		setClosedCheckbox("thu");
		//document.getElementById("thu").checked = true;
	}
	if (document.getElementById("fri-open").value == "") {
		setClosedCheckbox("fri");
		//document.getElementById("fri").checked = true;
	}
	if (document.getElementById("sat-open").value == "") {
		setClosedCheckbox("sat");
		//document.getElementById("sat").checked = true;
	}
	if (document.getElementById("sun-open").value == "") {
		setClosedCheckbox("sun");
		//document.getElementById("sun").checked = true;
	}

	var features = ["delivery", "takeout", "patio", "rooftop", "brunch", "dog"];

	for(var f in features) {
		if(document.getElementById("info-"+features[f]).innerHTML == "Yes") {
			document.getElementById(features[f]+"Yes").className = "btn btn-primary active";
			document.getElementById(features[f]+"Yes").style = "";
			document.getElementById(features[f]+"No").className = "btn btn-secondary";
			document.getElementById(features[f]+"No").style.backgroundColor = "rgb(240,240,240)";
		}
		else {
			document.getElementById(features[f]+"No").className = "btn btn-primary active";
			document.getElementById(features[f]+"No").style.borderRadius = "5px";
			document.getElementById(features[f]+"No").style.borderTopLeftRadius = "0";
			document.getElementById(features[f]+"No").style.borderBottomLeftRadius = "0";
			document.getElementById(features[f]+"Yes").className = "btn btn-secondary";
			document.getElementById(features[f]+"Yes").style.backgroundColor = "rgb(240,240,240)";
		}
	}
}

function cancelEditInfo(e) {
	e.preventDefault();
    document.getElementById("displayEstablishmentInfo").style.display = "block";
    document.getElementById("editEstablishmentInfo").style.display = "none";
    document.getElementById("btnEditEstInfo").style.display = "block";
    document.getElementById("btnCancelEditInfo").style.display = "none";
    document.getElementById("btnSaveEstInfoTop").style.display = "none";
}

function saveInfo() {
	document.getElementById("infoform").submit();
}

var openBeforeClick = [7];
var closeBeforeClick = [7];

function setClosed(day) {
	var open = day + "-open";
	var close = day + "-close";
	var intDay = -1;

	if (day == "sun")
		intDay = 0
	else if (day == "mon")
		intDay = 1
	else if (day == "tue")
		intDay = 2
	else if (day == "wed")
		intDay = 3
	else if (day == "thu")
		intDay = 4
	else if (day == "fri")
		intDay = 5
	else if (day == "sat")
		intDay = 6

	if (document.getElementById(open).disabled) {
		document.getElementById(open).value = openBeforeClick[intDay];
		document.getElementById(open).disabled = false;
		document.getElementById(close).value = closeBeforeClick[intDay];
		document.getElementById(close).disabled = false;
	}
	else {
		openBeforeClick[intDay] = document.getElementById(open).value;
		closeBeforeClick[intDay] = document.getElementById(close).value;
		
		document.getElementById(open).value = "";
		document.getElementById(open).disabled = true;
		document.getElementById(close).value= "";
		document.getElementById(close).disabled = true;
	}
}

function setClosed(e, day) {
	e.preventDefault();

	var open = day + "-open";
	var close = day + "-close";
	var intDay = -1;

	if (day == "sun")
		intDay = 0
	else if (day == "mon")
		intDay = 1
	else if (day == "tue")
		intDay = 2
	else if (day == "wed")
		intDay = 3
	else if (day == "thu")
		intDay = 4
	else if (day == "fri")
		intDay = 5
	else if (day == "sat")
		intDay = 6

	if (document.getElementById(open).disabled) {
		document.getElementById(open).value = openBeforeClick[intDay];
		document.getElementById(open).disabled = false;
		document.getElementById(close).value = closeBeforeClick[intDay];
		document.getElementById(close).disabled = false;
		document.getElementById(day).style.backgroundColor = "initial";
		document.getElementById(day).style.color = "inherit";
	}
	else {
		openBeforeClick[intDay] = document.getElementById(open).value;
		closeBeforeClick[intDay] = document.getElementById(close).value;
		
		document.getElementById(open).value = "";
		document.getElementById(open).disabled = true;
		document.getElementById(close).value= "";
		document.getElementById(close).disabled = true;
		document.getElementById(day).style.backgroundColor = "#d9534f";
		document.getElementById(day).style.color = "white";
	}
}

function setClosedCheckbox(day) {
	var open = day + "-open";
	var close = day + "-close";
	document.getElementById(open).value = "";
	document.getElementById(open).disabled = true;
	document.getElementById(close).value= "";
	document.getElementById(close).disabled = true;
	document.getElementById(day).style.backgroundColor = "#d9534f";
	document.getElementById(day).style.color = "white";
}

function previewImage(event) {
	var reader = new FileReader();
 	reader.onload = function() {
 		document.getElementById('info-edit-profile-picture').src = reader.result;
 	}
 	reader.readAsDataURL(event.target.files[0]);
}

function charOnly(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[a-zA-z]/;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

function numOnly(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]/;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
}

function featureClick(el) {
	document.getElementById(el.id).className = "btn btn-primary active";
	document.getElementById(el.id).style = "";

	var feature;

	if(el.id.indexOf("Yes") > -1) {
		feature = el.id.substring(0, el.id.indexOf("Yes"));
		document.getElementById(feature+"No").className = "btn btn-secondary";
		document.getElementById(feature+"No").style.backgroundColor = "rgb(240,240,240)";
		document.getElementById(feature+"Hidden").value = "Yes";
	}
	else if(el.id.indexOf("No") > -1) {
		feature = el.id.substring(0, el.id.indexOf("No"));
		document.getElementById(feature+"Yes").className = "btn btn-secondary";
		document.getElementById(feature+"Yes").style.backgroundColor = "rgb(240,240,240)";
		document.getElementById(feature+"Hidden").value = "No";

		document.getElementById(el.id).style.borderRadius = "5px";
		document.getElementById(el.id).style.borderTopLeftRadius = "0";
		document.getElementById(el.id).style.borderBottomLeftRadius = "0";
	}
}