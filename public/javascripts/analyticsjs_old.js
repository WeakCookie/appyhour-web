var data = [];
var backgroundColors = [];
var labels = [];
var myCharts = [];
var week = 0;
var currentDay = 0;
var showDayPicker = true;


jQuery(document).ready(checkContainerAnalytics);

$(document).ready(function() {
    $.fn.dataTable.moment('MM/DD/YYYY');
});

function checkContainerAnalytics() {
    if($('#week').is(':visible') || $('#mobile-day').is(':visible')) { //if the container is visible on the page
        initAnalytics();
    } else {
        setTimeout(checkContainerAnalytics, 50); //wait 50 ms, then try again
    }
}

function initAnalytics() {
    loadData();
    //loadImpressionsChart();
    loadTable();
    loadCalTable();
    loadCalTableMobile();

    backgroundColors = getBackgroundColors();
    labels = getChartWeekLabels();
    document.getElementById("week").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);

    if(localStorage.getItem("hideMsg") == undefined) {
        document.getElementById("msg02042020").style.display = "block";
    }

    var curr = newDate();
    curr.setHours(0,0,0,0);

    $('#week').datepicker({
        weekStart: 1
    }).on('changeDate', function(ev) {
        document.getElementById("week").disabled = true;
        document.getElementById("prevWeek").disabled = true;
        document.getElementById("nextWeek").disabled = true;

        $('#week').datepicker('hide');

        var date = ev.date;

        var startDate;
        if(date.getDay() > 0)
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()+1);
        else
            startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 6);
        $('#week').datepicker('update', startDate);

        var currYear = curr.getWeekYear();
        var currWeek = curr.getWeek();
        var dateYear = date.getWeekYear();
        var dateWeek = date.getWeek();

        var yearDiff = currYear - dateYear;
        var weekDiff = currWeek - dateWeek;

        if(yearDiff != 0) {
            week = -(currWeek + (53 * yearDiff) - dateWeek) + 1;
        }
        else {
            if(weekDiff != 0)
                week = -weekDiff;
            else
                week = 0;
        }

        if(week == 0)
            document.getElementById("nextWeek").disabled = true;
        else
            document.getElementById("nextWeek").disabled = false;

        //loadImpressionsChart();
        loadCalTable();
    }).on("show", function (date) {
        var datepickerDropDown = $('.datepicker');
        datepickerDropDown.addClass("weekPicker");
    });


    $('#mobile-day').datepicker({
        weekStart: 1
    }).on('changeDate', function(ev) {
        $('#mobile-day').datepicker('hide');

        document.getElementById("mobile-day").disabled = true;
        document.getElementById("prevDay").disabled = true;
        document.getElementById("nextDay").disabled = true;

        var days = (curr - ev.date) / 86400000;
        if(days != 0)
            currentDay = -days;
        else
            currentDay = 0;

        if(currentDay == 0)
            document.getElementById("nextDay").disabled = true;
        else
            document.getElementById("nextDay").disabled = false;

        //loadImpressionsChartMobile();
        loadCalTableMobile();
    });
}

function loadData() {
    var impSpan = document.getElementsByTagName("span");
    for(var is in impSpan) {
        if(impSpan[is].id !== undefined && impSpan[is].id.indexOf("impressionspan") > -1) {
            //console.log(impSpan[is]);
            var dataArr = [];
            for(var x = 0; x < 8; x++) {
                if(x == 3)
                    dataArr.push(parseInt(impSpan[is].children[x].innerHTML));
                else
                    dataArr.push(impSpan[is].children[x].innerHTML);

            }

            data.push(dataArr);
        }
    }

    /*
    data = [
        ['$5 Burger Baskets', '04/24/2019', '4:30pm - 8:00pm', 37, 3],
        ['$3 Big Beers', '04/24/2019', '7:00pm - 11:00pm', 97, -1],
        ['BOGO Free Drafts', '04/25/2019', '7:00pm - 11:00pm', 46, 4],
        ['50% off Apps', '04/26/2019', '11:00pm - 2:00am', 81, -1],
        ['$1 Apple Pie Shots', '04/26/2019', '9:00am - 11:00am', 44, 5],
        ['Free Beer w/ Burger', '04/26/2019', '4:00pm - 8:00pm', 93, 5],
        ['$1 U-Call-It', '04/27/2019', '7:00pm - 10:00pm', 120, -1],
        ['BOGO Free PBR', '04/28/2019', '3:00pm - 10:00pm', 74, 0],
        ['$5 Burger Baskets', '04/28/2019', '11:00am - 8:00pm', 40, 0],
        ['50% off Drafts', '04/29/2019', '3:00pm - 10:00pm', 67, 1],
        ['$2 Wells', '04/29/2019', '7:00pm - 2:00am', 29, 1],
        ['50% off Pizza', '04/30/2019', '11:00am - 8:00pm', 58, 2],
        ['BOGO Free Drafts', '04/30/2019', '4:30pm - 6:00pm', 96, -1],
        ['$1.99 Miller Lite Bottles', '04/30/2019', '4:00pm - 2:00am', 78, 2],
        ['50% off Appetizers', '05/01/2019', '4:00pm - 8:00pm', 55, 3],
        ['$3 Margs', '05/01/2019', '8:00pm - 2:00am', 87, -1],
        ['$5 off Large Pizzas', '05/02/2019', '4:00pm - 10:00pm', 0, 4]
    ];*/
}

function loadImpressionsChart() {
    backgroundColors = getBackgroundColors();
    labels = getChartWeekLabels();

    document.getElementById("week").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);

    var dateArr = getWeekArr();
    var mon = [];
    var tue = [];
    var wed = [];
    var thu = [];
    var fri = [];
    var sat = [];
    var sun = [];
    var monL = [];
    var tueL = [];
    var wedL = [];
    var thuL = [];
    var friL = [];
    var satL = [];
    var sunL = [];
    var monB = [];
    var tueB = [];
    var wedB = [];
    var thuB = [];
    var friB = [];
    var satB = [];
    var sunB = [];

    for(var i = 0; i < data.length; i++) {
        if(data[i][3] == 0)
            data[i][3] = 0.5;

        if(data[i][1] == dateArr[0]) {
            mon.push(data[i][3]);
            monL.push([data[i][0], data[i][2]]);
            monB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[1]) {
            tue.push(data[i][3]);
            tueL.push([data[i][0], data[i][2]]);
            tueB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[2]) {
            wed.push(data[i][3]);
            wedL.push([data[i][0], data[i][2]]);
            wedB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[3]) {
            thu.push(data[i][3]);
            thuL.push([data[i][0], data[i][2]]);
            thuB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[4]) {
            fri.push(data[i][3]);
            friL.push([data[i][0], data[i][2]]);
            friB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[5]) {
            sat.push(data[i][3]);
            satL.push([data[i][0], data[i][2]]);
            satB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[6]) {
            sun.push(data[i][3]);
            sunL.push([data[i][0], data[i][2]]);
            sunB.push(backgroundColors[i]);
        }
    }

    var datasets = [];
    datasets.push(mon);
    datasets.push(tue);
    datasets.push(wed);
    datasets.push(thu);
    datasets.push(fri);
    datasets.push(sat);
    datasets.push(sun);

    var labelsArr = [];
    labelsArr.push(monL);
    labelsArr.push(tueL);
    labelsArr.push(wedL);
    labelsArr.push(thuL);
    labelsArr.push(friL);
    labelsArr.push(satL);
    labelsArr.push(sunL);

    var bgArr = [];
    bgArr.push(monB);
    bgArr.push(tueB);
    bgArr.push(wedB);
    bgArr.push(thuB);
    bgArr.push(friB);
    bgArr.push(satB);
    bgArr.push(sunB);

    var weekArr = [];
    weekArr[0] = "mon";
    weekArr[1] = "tue";
    weekArr[2] = "wed";
    weekArr[3] = "thu";
    weekArr[4] = "fri";
    weekArr[5] = "sat";
    weekArr[6] = "sun";

    clearCharts();

    var curr = newDate();
    var day = curr.getDay();
    if(day == 0)
        day = 7;
    day = day - 1;

    for(var j = 0; j < 7; j++) {
        var arr = {};
        arr.data = datasets[j];
        arr.backgroundColor = bgArr[j];
        //arr.borderColor = "rgba(54, 162, 235, 1)";
        arr.borderColor = "black";
        arr.borderWidth = 1;

        var impressionsData = {};
        if(j <= day || week != 0) {
            impressionsData = {
                labels: labelsArr[j],
                datasets: [arr]
            }
        }
        else {
            impressionsData = {
                labels: [],
                datasets: []
            }
        }

        var showYAxisLabels = true;
        if(j > 0)
            showYAxisLabels = false;

        var ctx = document.getElementById(weekArr[j]+'-impressions');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: impressionsData,
            options: {
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            stepSize: getStepSize(),
                            max: getMaxY(),
                            display: false
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: labels[j]
                        },
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                        },
                        barThickness: 20
                    }]
                },
                legend: {
                    display: false
                },
                tooltips: {
                    displayColors: false,
                    callbacks: {
                        title: function(tooltipItem) {
                            return tooltipItem[0].xLabel[0];
                        },
                        label: function(tooltipItem) {
                            return tooltipItem.xLabel[1];
                        }
                    }
                },
                hover: {
                    animationDuration: 1
                },
                animation: {
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 1)";
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                           // console.log(meta);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                if(data == 0.5)
                                    data = 0;
                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                }
            }
        });
        myCharts.push(myChart);

        if(j == day) {
            document.getElementById("mobile-day").innerHTML = labels[j] + "<sup>" + getDateSuperscript(labels[j]) + "</sup>";

            ctx = document.getElementById('impressions-mobile');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: impressionsData,
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stepSize: getStepSize(),
                                max: getMaxY(),
                                display: false
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                display: true,
                                fontSize: 7
                            },
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    tooltips: {
                        enabled: false
                    },
                    hover: {
                        animationDuration: 1
                    },
                    animation: {
                        duration: 1,
                        onComplete: function () {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            ctx.textAlign = 'center';
                            ctx.fillStyle = "rgba(0, 0, 0, 1)";
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                               // console.log(meta);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
                }
            });
            myCharts.push(myChart);
        }
    }

    document.getElementById("week").disabled = false;
    document.getElementById("prevWeek").disabled = false;

    if(week == 0)
        document.getElementById("nextWeek").disabled = true;
    else
        document.getElementById("nextWeek").disabled = false;
}

function loadImpressionsChartMobile() {
    clearCharts();

    var curr = newDate();
    curr.setDate(curr.getDate()+currentDay);
    curr.setHours(0,0,0,0);
    var d = intMonthToStringAbbr(curr.getMonth()) + " " + curr.getDate();
    document.getElementById("mobile-day").innerHTML = d + "<sup>" + getDateSuperscript(d) + "</sup>";

    var dataArr = [];
    var labelArr = [];
    var bgArr = [];

    for(var i in data) {
        var dDate = new Date(data[i][1]);
        dDate.setHours(0,0,0,0);
        if(dDate.getDate() == curr.getDate() && dDate.getMonth() == curr.getMonth() && dDate.getYear() == curr.getYear()) {
            dataArr.push(data[i][3]);
            labelArr.push([data[i][0], data[i][2]]);
            bgArr.push(backgroundColors[i]);
        }
    }

    var arr = {};
    arr.data = dataArr;
    arr.backgroundColor = bgArr;
    arr.borderColor = "black";
    arr.borderWidth = 1;

    var impressionsData = {
        labels: labelArr,
        datasets: [arr]
    }

    ctx = document.getElementById('impressions-mobile');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: impressionsData,
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: getStepSize(),
                        max: getMaxY(),
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: true,
                        fontSize: 7
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }]
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 1
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                    ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            if(data == 0.5)
                                data = 0;
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        }
    });
    myCharts.push(myChart);

    document.getElementById("mobile-day").disabled = false;
    document.getElementById("prevDay").disabled = false;

    if(currentDay == 0)
        document.getElementById("nextDay").disabled = true;
    else
        document.getElementById("nextDay").disabled = false;
}

function loadTable() {
    var dataSet = [];

    for(var i = 0; i < data.length; i++) {
        var ds = [];
        var curr = newDate();
        var d = new Date(data[i][1]);

        if(d <= curr) {
            ds.push(data[i][1]);
            ds.push(data[i][2]);

            if(data[i][7] != "")
                ds.push(data[i][0] + "<p style='font-size:0.8em;'><i class='fa fa-calendar fa-fw'></i> " + data[i][7]) + "</p>";
            else
                ds.push(data[i][0]);

            if(data[i][3] == 0.5)
                ds.push(0)
            else
                ds.push(data[i][3]);

            if(data[i][5] == "Deleted")
                ds.push("<p style='line-height:1.3; font-size:0.6em'>" + data[i][5] + "<br/><span style='font-size:1.7em'>" + data[i][6]) + "</span></p>";
            else
                ds.push(data[i][5]);

            ds.push(data[i][4]);
            dataSet.push(ds);
        }
    }

    $('#dataTable').DataTable({
        data: dataSet,
        columns: [
            { title: "Date" },
            { title: "Time" },
            { title: "Details" },
            { title: "Impressions" },
            { title: "Status" },
        ],
        columnDefs: [ {
            "targets": [1, 2, 4],
            "orderable": false
        } ],
        order: [[ 0, "desc" ]],
        createdRow: function(row, data, dataIndex) {
            if(data[5] ==  -1 && data[2].indexOf("fa-calendar") == -1){
                $(row).addClass('orangeClass');
            }
            if(data[4].indexOf("Deleted") > -1) {
                var statusRow = $(row)[0].cells[4];
                statusRow.style.color = "red";
            }
            else if(data[4].indexOf("Active") > -1) {
                var statusRow = $(row)[0].cells[4];
                statusRow.style.color = "green";
            }
        }
    });
}

function loadCalTable() {
    labels = getChartWeekLabels();

    document.getElementById("week").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);

    var table = document.getElementById("calTable");

    var tLen = table.rows.length;
    for(var t = 0; t < tLen; t++) {
        table.deleteRow(0);
    }

    // data
    var dataSet = [];

    var curr = newDate(); // get current date
    var first;
    if(curr.getDay() > 0)
        first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    else
        first = curr.getDate() - 7;
    var last = first + 7; // last day is the first day + 6

    var weekStart = new Date(curr.setDate(first));
    weekStart.setDate(weekStart.getDate()+1);
    weekStart.setHours(0,0,0,0);

    var currNew = newDate();
    var weekEnd = new Date(currNew.setDate(last));
    weekEnd.setHours(0,0,0,0);

    if(week != 0) {
        weekStart.setDate(weekStart.getDate() + (week * 7));
        weekEnd.setDate(weekEnd.getDate() + (week * 7));
    }
    else {
        curr = newDate();
        curr.setHours(0,0,0,0);
        if(weekEnd > curr) {
           weekEnd = curr;
        }
    }

    var dataset = getImpForWeek(weekStart, weekEnd);

    dataset = dataset.sort(DatasetComparator);

    var sorted = false;
    while(!sorted) {
        sorted = true;
        for(var d = 0; d < dataset.length - 1; d++) {
            var dur1 = dataset[d][1];
            var s1 = getDateFromDuration(dur1, 0);
            var e1 = getDateFromDuration(dur1, 1);
            var dur2 = dataset[d+1][1];
            var s2 = getDateFromDuration(dur2, 0);
            var e2 = getDateFromDuration(dur2, 1);

            if(s1.getTime() > s2.getTime() || (s1.getTime() == s2.getTime() && e1.getTime() > e2.getTime())) {
                var temp = dataset[d];
                dataset[d] = dataset[d+1];
                dataset[d+1] = temp;
                sorted = false;
            }
        }
    }

    var r = table.insertRow(0);
    //r.style.borderLeft = "1px solid gainsboro";
    //r.style.borderRight = "1px solid gainsboro";

    for(var i = 0; i < 7; i++) {
        var cell = r.insertCell(i);
        cell.style.verticalAlign = "bottom";
        cell.style.border = 0;

        curr = newDate();
        curr.setHours(0,0,0.0,0);
        var today = new Date(weekStart);
        today.setDate(today.getDate()+i);
        today.setHours(0,0,0,0,0);

        if(curr.getTime() == today.getTime()) {
            cell.style.backgroundColor = "rgba(0,200,0,0.2)";
        }
        else if(today.getTime() > curr.getTime()) {
            cell.style.backgroundColor = "rgba(192,192,192,0.2)";
        }

        var dayHtml = "";
        var dayImps = 0;

        for(var d = 0; d < dataset.length; d++) {
            var exclusive = false;
            var day = parseInt(dataset[d][5]);

            if(day == -1) {
                var ahDate = new Date(Date.parse(dataset[d][0]));
                day = ahDate.getDay();
                if(dataset[d][2].indexOf("fa-calendar") == -1)
                    exclusive = true;
            }
            day -= 1;
            if(day == -1)
                day = 6;

            if(day == i) {
                dayImps += dataset[d][3];
                if(dayHtml == "") {
                    if(exclusive) {
                        dayHtml = "<div class='calBubble'><div class='calExclusive'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                    else {
                        dayHtml = "<div class='calBubble'><div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                }
                else {
                    if(exclusive) {
                        dayHtml = dayHtml + "<div class='calExclusive'>" + dataset[d][2] + "</span><br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                    else {
                        dayHtml = dayHtml + "<div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                }
            }
            if(d == dataset.length - 1 && dayHtml != "") {
                dayHtml = dayHtml + "</div>";
                cell.innerHTML = "<p class='calImps'>"+dayImps+"</p>"+dayHtml;
            }
        }
    }

    // headers
    var row = table.insertRow(1);
    row.style.textAlign = "center";

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);

    cell1.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Monday</p><p style='font-size:0.8em'>" + labels[0] + getDateSuperscript(labels[0]) + "</p></div>";
    cell2.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Tuesday</p><p style='font-size:0.8em'>" + labels[1] + getDateSuperscript(labels[1]) + "</p></div>";
    cell3.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Wednesday</p><p style='font-size:0.8em'>" + labels[2] + getDateSuperscript(labels[2]) + "</p></div>";
    cell4.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Thursday</p><p style='font-size:0.8em'>" + labels[3] + getDateSuperscript(labels[3]) + "</p></div>";
    cell5.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Friday</p><p style='font-size:0.8em'>" + labels[4] + getDateSuperscript(labels[4]) + "</p></div>";
    cell6.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Saturday</p><p style='font-size:0.8em'>" + labels[5] + getDateSuperscript(labels[5]) + "</p></div>";
    cell7.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Sunday</p><p style='font-size:0.8em'>" + labels[6] + getDateSuperscript(labels[6]) + "</p></div>";

    cell1.style.width = "14.28%";
    cell2.style.width = "14.28%";
    cell3.style.width = "14.28%";
    cell4.style.width = "14.28%";
    cell5.style.width = "14.28%";
    cell6.style.width = "14.28%";
    cell7.style.width = "14.28%";

    document.getElementById("week").disabled = false;
    document.getElementById("prevWeek").disabled = false;

    if(week == 0)
        document.getElementById("nextWeek").disabled = true;
    else
        document.getElementById("nextWeek").disabled = false;
}

function loadCalTableMobile() {
    labels = getChartWeekLabels();

    var curr = newDate();
    curr.setDate(curr.getDate()+currentDay);
    curr.setHours(0,0,0,0);
    var d = intMonthToStringAbbr(curr.getMonth()) + " " + curr.getDate();
    document.getElementById("mobile-day").innerHTML = d + "<sup>" + getDateSuperscript(d) + "</sup>";

    var table = document.getElementById("calTableMobile");

    var tLen = table.rows.length;
    for(var t = 0; t < tLen; t++) {
        table.deleteRow(0);
    }

    // data
    var dataset = getImpForWeek(curr, curr);

    dataset = dataset.sort(DatasetComparator);

    var sorted = false;
    while(!sorted) {
        sorted = true;
        for(var d = 0; d < dataset.length - 1; d++) {
            var dur1 = dataset[d][1];
            var s1 = getDateFromDuration(dur1, 0);
            var e1 = getDateFromDuration(dur1, 1);
            var dur2 = dataset[d+1][1];
            var s2 = getDateFromDuration(dur2, 0);
            var e2 = getDateFromDuration(dur2, 1);

            if(s1.getTime() > s2.getTime() || (s1.getTime() == s2.getTime() && e1.getTime() > e2.getTime())) {
                var temp = dataset[d];
                dataset[d] = dataset[d+1];
                dataset[d+1] = temp;
                sorted = false;
            }
        }
    }

    var r = table.insertRow(0);
    //r.style.borderLeft = "1px solid gainsboro";
    //r.style.borderRight = "1px solid gainsboro";

    var cell = r.insertCell(0);
    cell.style.verticalAlign = "bottom";
    cell.style.border = 0;

    var dayHtml = "";
    var dayImps = 0;

    for(var d = 0; d < dataset.length; d++) {
        var exclusive = false;
        var day = parseInt(dataset[d][5]);

        if(day == -1) {
            var ahDate = new Date(Date.parse(dataset[d][0]));
            day = ahDate.getDay();
            exclusive = true;
        }
        day -= 1;
        if(day == -1)
            day = 6;

        dayImps += dataset[d][3];
        if(dayHtml == "") {
            if(exclusive) {
                dayHtml = "<div class='calBubble'><div class='calExclusive'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
            else {
                dayHtml = "<div class='calBubble'><div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
        }
        else {
            if(exclusive) {
                dayHtml = dayHtml + "<div class='calExclusive'>" + dataset[d][2] + "</span><br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
            else {
                dayHtml = dayHtml + "<div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
        }
        if(d == dataset.length - 1 && dayHtml != "") {
            dayHtml = dayHtml + "</div>";
            cell.innerHTML = "<p class='calImps'>"+dayImps+"</p>"+dayHtml;
        }
    }

    if(dataset.length == 0) {
        cell.innerHTML = "<p style='text-align:center;'>No Specials</p>";
    }


    // headers
    var row = table.insertRow(1);
    row.style.textAlign = "center";

    var cell1 = row.insertCell(0);

    cell1.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>" + intDayToStr(curr.getDay()) + "</p></div>";

    cell1.style.width = "14.28%";

    document.getElementById("mobile-day").disabled = false;
    document.getElementById("prevDay").disabled = false;

    if(currentDay == 0)
        document.getElementById("nextDay").disabled = true;
    else
        document.getElementById("nextDay").disabled = false;
}

function DatasetComparator(a, b) {
   if (a[2] < b[2]) return -1;
   if (a[2] > b[2]) return 1;
   return 0;
}

function getDateFromDuration(dur, startEnd) {
    var s1;

    if(startEnd == 0) {
        s1 = dur.substring(0, dur.indexOf(" - "));
    }
    else if(startEnd == 1) {
        s1 = dur.substring(dur.indexOf(" - ")+3);
    }

    var h1 = parseInt(s1.substring(0, s1.indexOf(":")));
    var m1 = s1.substring(s1.indexOf(":")+1);
    m1 = m1.replace("AM", "").replace("PM", "");
    if(s1.indexOf("AM") > -1 && h1 == 12) {
        h1 = 0;
    }
    else if(s1.indexOf("PM") > -1 && h1 != 12) {
        h1 = h1 + 12;
    }
    if(h1 < 10) {
        h1 = "0" + h1;
    }
    s1 = new Date("01-01-2020 " + h1 + ":" + m1 + ":00");
    return s1;
}

function getImpForWeek(s, e) {
    var dataSet = [];
    for(var i = 0; i < data.length; i++) {
        var ds = [];
        var d = new Date(data[i][1]);

        if(d >= s && d <= e) {
            ds.push(data[i][1]);
            ds.push(data[i][2]);

            if(data[i][7] != "")
                ds.push(data[i][0] + "<p style='font-size:0.8em;'><i class='fa fa-calendar fa-fw'></i> " + data[i][7]) + "</p>";
            else
                ds.push(data[i][0]);

            if(data[i][3] == 0.5)
                ds.push(0)
            else
                ds.push(data[i][3]);

            if(data[i][5] == "Deleted")
                ds.push("<p style='line-height:1.3; font-size:0.6em'>" + data[i][5] + "<br/><span style='font-size:1.7em'>" + data[i][6]) + "</span></p>";
            else
                ds.push(data[i][5]);

            ds.push(data[i][4]);
            dataSet.push(ds);
        }
    }
    return dataSet;
}

function weekPicker(e) {
    e.preventDefault();
    $('#week').datepicker('show');
}

function dayPicker(e) {
    e.preventDefault();
    $('#mobile-day').datepicker('show');
}

function nextWeek(e) {
    e.preventDefault();
    week = week + 1;

    //loadImpressionsChart();
    //loadCalTable();

    var d = newDate();
    d.setDate(d.getDate() + (week * 7));
    var startDate;
    if(d.getDay() > 0)
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay()+1);
    else
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 6);
    $('#week').data({date: getYYYYMMDD(startDate)});
    $('#week').datepicker('update');

    if(week == 0)
        document.getElementById("nextWeek").disabled = true;
}

function prevWeek(e) {
    e.preventDefault();
    week = week - 1;

    //loadImpressionsChart();
    //loadCalTable();

    var d = newDate();
    d.setDate(d.getDate() + (week * 7));
    var startDate;
    if(d.getDay() > 0)
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay()+1);
    else
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 6);
    $('#week').data({date: getYYYYMMDD(startDate)});
    $('#week').datepicker('update');

    document.getElementById("nextWeek").disabled = false;
}

function nextDay(e) {
    e.preventDefault();
    currentDay = currentDay + 1;

    //loadImpressionsChartMobile();

    var d = newDate();
    d.setDate(d.getDate()+currentDay);
    $('#mobile-day').data({date: getYYYYMMDD(d)});
    $('#mobile-day').datepicker('update');

    if(currentDay == 0)
        document.getElementById("nextDay").disabled = true;
}

function prevDay(e) {
    e.preventDefault();
    currentDay = currentDay - 1;

    //loadImpressionsChartMobile();

    var d = newDate();
    d.setDate(d.getDate()+currentDay);
    $('#mobile-day').data({date: getYYYYMMDD(d)});
    $('#mobile-day').datepicker('update');

    document.getElementById("nextDay").disabled = false;
}

function clearCharts() {
    for(var c in myCharts) {
        myCharts[c].destroy();
    }
}

function getBackgroundColors() {
    var backgroundColor = [];

    if(data.length > 0) {
        var low = data[0][3];
        var high = data[0][3];
        var len = data.length;

        for(var i = 1; i < len; i++) {
            if(data[i][3] < low)
                low = data[i][3];
            if(data[i][3] > high)
                high = data[i][3];
        }

        var diff = high - low;
        if(diff > 0) {
            for(var j = 0; j < data.length; j++) {
                if(data[j][4] == "-1") {
                    backgroundColor.push('rgba(244, 149, 60, 0.9)');
                }
                else {
                    var opacity = (data[j][3] - low) / diff;
                    if(opacity < 0.1)
                        opacity = 0.1;
                    else if(opacity > 0.9)
                        opacity = 0.9;
                    backgroundColor.push('rgba(54, 162, 235, ' + opacity + ')');
                }
            }
        }
        else {
            for(var k = 0; k < data.length; k++) {
                if(data[k][4] == "-1")
                    backgroundColor.push('rgba(244, 149, 60, 0.9)');
                else
                    backgroundColor.push('rgba(54, 162, 235, 0.6)');
            }
        }
    }

    return backgroundColor;
}

function getChartWeekLabels() {
    var weekLabels = [];

    var addDays = 1;
    if(week != 0)
        addDays = week * 7 + 1;

    var curr = newDate();
    curr.setHours(0,0,0,0);
    var first = 0;
    if(curr.getDay() != 0)
        first = curr.getDate() - curr.getDay() + addDays;
    else
        first = curr.getDate() - 7 + addDays;
    for(var i = 0; i <= 6; i++) {
        var d = new Date(curr.setDate(first));
        var dStr = intMonthToStringAbbr(d.getMonth()) + " " + d.getDate();
        weekLabels.push(dStr);
        first = first + 1;
        curr = newDate();
        curr.setHours(0,0,0,0);
    }

    return weekLabels;
}

function getWeekArr() {
    var weekArr = [];

    var addDays = 1;
    if(week != 0)
        addDays = week * 7 + 1;

    var curr = newDate();
    curr.setHours(0,0,0,0);
    var first = curr.getDate() - curr.getDay() + addDays;
    for(var i = 0; i <= 6; i++) {
        var d = new Date(curr.setDate(first));
        var month = d.getMonth()+1;
        if(month < 10)
            month = "0" + month;
        var date = d.getDate();
        if(date < 10)
            date = "0"+date;
        var year = d.getYear()+1900;
        weekArr.push(month+"/"+date+"/"+year);
        first = first + 1;
        curr = newDate();
        curr.setHours(0,0,0,0);
    }

    return weekArr;
}

function getStepSize() {
    var max = getMax();
    var stepSize = max / 6;
    var mod;
    if(stepSize < 1)
        return 1;
    else if(stepSize <= 2)
        return 2;
    else if(stepSize < 5)
        mod = 5;
    else
        mod = 10;
    var m = stepSize % mod;
    return Math.round(stepSize + mod - m);
}

function getMax() {
    var max = 1;
    for(var i = 0; i < data.length; i++)
        if(data[i][3] > max)
            max = data[i][3];
    return max;
}

function getMaxY() {
    var step = getStepSize();
    var max = getMax();
    var maxY = max + (max / 10);
    var mod = maxY % step;
    if(mod != 0)
        return Math.round(maxY + (step - mod));
    else
        return maxY;
}

function intMonthToStringAbbr(month) {
    var m = new Array(7);
    m[0] = "Jan";
    m[1] = "Feb";
    m[2] = "Mar";
    m[3] = "Apr";
    m[4] = "May";
    m[5] = "Jun";
    m[6] = "Jul";
    m[7] = "Aug";
    m[8] = "Sep";
    m[9] = "Oct";
    m[10] = "Nov";
    m[11] = "Dec";
    return m[month];
}

function intDayToStr(day) {
    var m = new Array(7);
    m[1] = "Monday";
    m[2] = "Tuesday";
    m[3] = "Wednesday";
    m[4] = "Thursday";
    m[5] = "Friday";
    m[6] = "Saturday";
    m[0] = "Sunday";
    return m[day];
}

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
   return year + "-" + month + "-" + day;
}

// Returns the ISO week of the date.
Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
                        - 3 + (week1.getDay() + 6) % 7) / 7);
}

// Returns the four-digit year corresponding to the ISO week of the date.
Date.prototype.getWeekYear = function() {
  var date = new Date(this.getTime());
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  return date.getFullYear();
}

function hideMessage() {
    document.getElementById("msg02042020").style.display = "none";
    localStorage.setItem("hideMsg", "true");
}
