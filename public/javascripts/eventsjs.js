var addSpecialPriceType = "$";
var addSpecialPriceTypeId = 300;
var addSpecialDealTypeId = 0;
var isAddSpecialDealTypeSelected = false;

var addEventSpecialPriceType = "$";
var addEventSpecialPriceTypeId = 300;
var addEventSpecialDealTypeId = 0;
var isAddEventSpecialDealTypeSelected = false;

var addUpcomingEventSpecialPriceType = "$";
var addUpcomingEventSpecialPriceTypeId = 300;
var addUpcomingEventSpecialDealTypeId = 0;
var isAddUpcomingEventSpecialDealTypeSelected = false;

var addModalEventSpecialPriceType = "$";
var addModalEventSpecialPriceTypeId = 300;
var addModalEventSpecialDealTypeId = 0;
var isAddModalEventSpecialDealTypeSelected = false;

var week = 0;
var labels = [];


/* Schedule an event */

function scheduleEvent() {
	var recurring = document.getElementById("eventReoccurring").value;
	var title = document.getElementById("eventTitle").value;
	var details = document.getElementById("eventDetails").value;
	var startDate = document.getElementById("eventStartDate").value;
	var startTime = document.getElementById("eventStartTime").value;
	var endDate = document.getElementById("eventEndDate").value;
	var endTime = document.getElementById("eventEndTime").value;
	var startTimeRecur = document.getElementById("eventStartTimeReoccurring").value;
	var endTimeRecur = document.getElementById("eventEndTimeReoccurring").value;

	var mon = document.getElementById("createEvent-day-mon").value;
	var tue = document.getElementById("createEvent-day-tue").value;
	var wed = document.getElementById("createEvent-day-wed").value;
	var thu = document.getElementById("createEvent-day-thu").value;
	var fri = document.getElementById("createEvent-day-fri").value;
	var sat = document.getElementById("createEvent-day-sat").value;
	var sund = document.getElementById("createEvent-day-sun").value;

	var parameters = {
		recurring: recurring,
		title: title,
		details: details,
		startDate: startDate,
		startTime: startTime,
		endDate: endDate,
		endTime: endTime,
		startTimeRecur: startTimeRecur,
		endTimeRecur: endTimeRecur,
		mon: mon,
		tue: tue,
		wed: wed,
		thu: thu,
		fri: fri,
		sat: sat,
		sund: sund
	};

	if(validateScheduleEvent(parameters)) {
		$.get('/scheduleevent', parameters, function(data) {
			//if(data && data.oneTimeEventLimit == true) {
			//	document.getElementById("createEventErrors").innerHTML = "<p id='eventLimitError'>You've already scheduled a One-Time event for this week.</p>";
			//}
			if(data && data.recurringEventLimit == true) {
				document.getElementById("createEventErrors").innerHTML = "<p id='eventLimitError'>Adding this event will put you over your 3 Weekly events limit. Please delete a weekly event to add another.</p>";
			}
			else if(data && data.success == true) {
				var id = data.eventId;
				var recurringStr = "";
				var duration = "";

				if(recurring == "false") {
					duration = getDurationStr(startDate, endDate, startTime, endTime);
				}
				else {
					var days = "";
					if(mon == "true") {
						days = days + "1,";
					}
					if(tue == "true") {
						days = days + "2,";
					}
					if(wed == "true") {
						days = days + "3,";
					}
					if(thu == "true") {
						days = days + "4,";
					}
					if(fri == "true") {
						days = days + "5,";
					}
					if(sat == "true") {
						days = days + "6,";
					}
					if(sund == "true") {
						days = days + "0,";
					}

					days = days.substring(0, days.length-1);

					var str = "";
					var dayArr = days.split(",");

					if(dayArr.length == 1) {
						str = str + intDayToString(dayArr[0]);
					}
					else if(dayArr.length == 2) {
						str = str + intDayToString(dayArr[0]) + " and " + intDayToString(dayArr[1]);
					}
					else {
						for(var i in dayArr) {
							if(i < dayArr.length - 2) {
								str = str + intDayToString(dayArr[i]) + ", ";
							}
							else if(i < dayArr.length - 1) {
								str = str + intDayToString(dayArr[i]) + ", and ";
							}
							else {
								str = str + intDayToString(dayArr[i]);
							}
						}
					}

					recurringStr = "<i class='fa fa-retweet' style='margin-right:3px;'></i>" + str;
					duration = getDurationStr(null, null, startTimeRecur, endTimeRecur);
				}

				document.getElementById("modalEventId").value = id;
				document.getElementById("modalEventTitle").innerHTML = title;
				document.getElementById("modalEventDetails").innerHTML = details;
				document.getElementById("modalRecurringStr").innerHTML = recurringStr;
				
				if(recurring == "true") {
					document.getElementById("modalEventDuration").innerHTML = duration;
					document.getElementById("modal-event-recurring-duration").style.display = "block";
					document.getElementById("modal-event-duration").style.display = "none";
				}
				else {
					document.getElementById("modalEventStartDate").innerHTML = getEventDate(startDate);
					document.getElementById("modalEventStartTime").innerHTML = formatStartEndTime(new Date("01/01/2020 " + startTime));
					document.getElementById("modalEventEndDate").innerHTML = getEventDate(endDate);
					document.getElementById("modalEventEndTime").innerHTML = formatStartEndTime(new Date("01/01/2020 " + endTime));
					document.getElementById("modal-event-recurring-duration").style.display = "none";
					document.getElementById("modal-event-duration").style.display = "block";
				}
				

				
				document.getElementById("modalEventStartDateHidden").innerHTML = startDate;
				document.getElementById("modalEventEndDateHidden").innerHTML = endDate;
				document.getElementById("modalEventStartTimeHidden").innerHTML = startTime;
				document.getElementById("modalEventEndTimeHidden").innerHTML = endTime;

				$("#add-specials-modal").modal("show");
			}
		});
	}
}

function getEventDate(startTime) {
    var now = newDate();

    var timeArr = startTime.split("-");
    var yr = timeArr[0];
    var month = timeArr[1];
    var day = timeArr[2];

    startTime = new Date(month+"/"+day+"/"+yr);

    if(startTime.getDate() == now.getDate() && startTime.getMonth() == now.getMonth() && startTime.getFullYear() == now.getFullYear()) {
        return "Today";
    }
    else {
        return timestampToDayMonDate(startTime);
    }
}

function timestampToDayMonDate(timestamp) {
    var d = new Date(timestamp);
    var day = intDayToStringAbbr(d.getDay());
    var month = intMonthToStringAbbr(d.getMonth());
    var date = d.getDate();
    return day + ", " + month + " " + date;
}



function validateScheduleEvent(p) {
	var valid = true;
	var msg = [];
	var cnt = 0;
	document.getElementById("createEventErrors").innerHTML = "";

	// title
	if(p.title == "") {
		document.getElementById("eventTitle").style.backgroundColor = "#FFEAEA";
		document.getElementById("eventTitle").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='eventTitleError'>Make sure to add a title!</p>";
		valid = false;
	}
	else {
		document.getElementById("eventTitle").style.backgroundColor = "#fff";
		document.getElementById("eventTitle").style.border = "1px solid #ccc";
	}

	// details
	if(p.details == "") {
		document.getElementById("eventDetails").style.backgroundColor = "#FFEAEA";
		document.getElementById("eventDetails").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='eventDetailsError'>Make sure to add some details!</p>";
		valid = false;
	}
	else {
		document.getElementById("eventDetails").style.backgroundColor = "#fff";
		document.getElementById("eventDetails").style.border = "1px solid #ccc";
	}

	// one-time
	if(p.recurring == "false") {
		// start date
		if(p.startDate == "") {
			document.getElementById("eventStartDate").style.backgroundColor = "#FFEAEA";
			document.getElementById("eventStartDate").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventStartDateError'>Make sure to choose a valid date!</p>";
			valid = false;
		}
		else {
			document.getElementById("eventStartDate").style.backgroundColor = "#fff";
			document.getElementById("eventStartDate").style.border = "1px solid #ccc";
		}

		// start time
		if(p.startTime == "") {
			document.getElementById("eventStartTime").style.backgroundColor = "#FFEAEA";
			document.getElementById("eventStartTime").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventStartTimeError'>Make sure to choose a valid start time!</p>";
			valid = false;
		}
		else {
			document.getElementById("eventStartTime").style.backgroundColor = "#fff";
			document.getElementById("eventStartTime").style.border = "1px solid #ccc";
		}

		// end time
		if(p.endTime == "") {
			document.getElementById("eventEndTime").style.backgroundColor = "#FFEAEA";
			document.getElementById("eventEndTime").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventEndTimeError'>Make sure to choose a valid end time!</p>";
			valid = false;
		}
		else {
			document.getElementById("eventEndTime").style.backgroundColor = "#fff";
			document.getElementById("eventEndTime").style.border = "1px solid #ccc";
		}

		if(p.startDate != "" && p.endDate != "" && p.startTime != "" && p.endTime != "") {
			var start = getDateTime(p.startTime, p.startDate);
			var end = getDateTime(p.endTime, p.endDate);
			var now = newDate();

			if(start < now) {
				msg[cnt++] = "<p>Make sure the event starts in the future!</p>";
				valid = false;
			}
			if(end <= start) {
				msg[cnt++] = "<p>Make sure the end time is later than the start time!</p>";
				valid = false;
			}
		}
	}
	// recurring
	else if(p.recurring == "true") {
		// day(s)
		if(p.mon == "false" && p.tue == "false" && p.wed == "false" && p.thu == "false" && p.fri == "false" && p.sat == "false" && p.sund == "false") {
			document.getElementById("createEvent-dayToggle").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventDaysError'>Make sure to choose at least one day!</p>";
			valid = false;
		}
		else {
			document.getElementById("createEvent-dayToggle").style.border = "none";
		}

		// start time
		if(p.startTimeRecur == "") {
			document.getElementById("eventStartTimeReoccurring").style.backgroundColor = "#FFEAEA";
			document.getElementById("eventStartTimeReoccurring").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventStartTimeReoccurringError'>Make sure to choose a start time!</p>";
			valid = false;
		}
		else {
			document.getElementById("eventStartTimeReoccurring").style.backgroundColor = "#fff";
			document.getElementById("eventStartTimeReoccurring").style.border = "1px solid #ccc";
		}

		// end time
		if(p.endTimeRecur == "") {
			document.getElementById("eventEndTimeReoccurring").style.backgroundColor = "#FFEAEA";
			document.getElementById("eventEndTimeReoccurring").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventEndTimeReoccurringError'>Make sure to choose an end time!</p>";
			valid = false;
		}
		else {
			document.getElementById("eventEndTimeReoccurring").style.backgroundColor = "#fff";
			document.getElementById("eventEndTimeReoccurring").style.border = "1px solid #ccc";
		}

		if(p.startTimeRecur != "" && p.endTimeRecur != "") {
			var now = newDate();
			var start = getDateTime(p.startTimeRecur, now);
			var end = getDateTime(p.endTimeRecur, now);

			if(end <= start) {
				msg[cnt++] = "<p>Make sure the end time is later than the start time!</p>";
				valid = false;
			}
		}
	}

	if(!valid) {
		for(var m in msg) {
			document.getElementById("createEventErrors").innerHTML = document.getElementById("createEventErrors").innerHTML + msg[m];
		}
	}

	return valid;
}

function addSpecialToEvent() {
	var eventId = document.getElementById("modalEventId").value;

	var days = [];
	var daysStr = [];
	var content = document.getElementById("addModalEventSpecial-dayToggle");
	var dayButtons = content.getElementsByTagName("button");
	for(var i = 0; i < dayButtons.length; i++) {
		if(dayButtons[i].className.indexOf("dayActive") > -1) {
			dayInput = document.getElementById(dayButtons[i].id+"-hidden");
			days.push(dayInput.value);
			daysStr.push(dayButtons[i].innerHTML);
		}
	}

	var startTime = document.getElementById("addModalEventSpecial-startTime").value;
	var endTime = document.getElementById("addModalEventSpecial-endTime").value;

	var price = document.getElementById("addModalEventSpecial-price").value;
	var details = document.getElementById("addModalEventSpecial-details").value;

	var parameters = {
		eventId: eventId,
		days: days,
		startTime: startTime,
		endTime: endTime,
		priceType: addModalEventSpecialPriceTypeId,
		price: price,
		details: details,
		specialType: addModalEventSpecialDealTypeId
	};

	if(validateAddSpecialToEvent(parameters)) {
		$.get('/addspecialtotodaysevent', parameters, function(data) {
			if(data) {
				var htmlRow = "";
				var specialDays = "";

				if(daysStr.length != dayButtons.length) {
					for(var d = 0; d < daysStr.length; d++) {
						specialDays = specialDays + daysStr[d];
						if(d < daysStr.length-1)
							specialDays = specialDays + ", ";
					}
				}
				else if(daysStr.length == dayButtons.length && dayButtons.length > 1) {
					specialDays = "All days";
				}
				else {
					specialDays = "";
				}

				var st = document.getElementById("modalEventStartTimeHidden").innerHTML;
				var et = document.getElementById("modalEventEndTimeHidden").innerHTML;
				if(startTime == "") {
					startTime = st;
					endTime = et;
				}

				if(data.specialIds) {
					htmlRow = "<div class='row' id='modalEventSpecialRow"+data.specialIds[0]+"' style='margin-left:0px; margin-right:0px;'>"+
				     	   			"<div class='col col-md-5 col-sm-5 col-xs-5' style='line-height:1.5;'>"+
									 "<img src='assets/img/"+getDealTypeImageByRefId(addModalEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
									 "<p style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'>";

									 if(addModalEventSpecialPriceTypeId == 300)
									 	htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
									 else if(addModalEventSpecialPriceTypeId == 301)
									 	htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
									 else if(addModalEventSpecialPriceTypeId == 302)
									 	htmlRow = htmlRow+"BOGO "+details;
									 else if(addModalEventSpecialPriceTypeId == 307)
									 	htmlRow = htmlRow+"Free "+details;

					htmlRow = htmlRow+"</p>"+
									"</div>"+
									"<div class='col col-md-2 col-sm-2 col-xs-2' style='line-height:1.5;'>"+
						    	     "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+specialDays+"</p>"+
						     	    "</div>"+
						     	    "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5;'>"+
				    	    		 "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+formatStartEndTime(new Date("01/01/2020 " + startTime)) + " - " + formatStartEndTime(new Date("01/01/2020 " + endTime))+"</p>"+
				     	   			"</div>"+
									"<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
									 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
									  "<span style='cursor:pointer'>"+
									   "<a id='deleteModalEventSpecial"+data.specialIds[0]+"' onclick='deleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
									    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
									   "</a>"+
									  "</span>"+
									 "</p>"+
									"</div>"+
								   "</div>";
				}
				else {
					htmlRow = "<div class='row' id='modalEventSpecialRow"+data.specialId+"' style='margin-left:0px; margin-right:0px;'>"+
				     	   			"<div class='col col-md-12' style='line-height:1.5;'>"+
									 "<img src='assets/img/"+getDealTypeImageByRefId(addModalEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
									 "<p style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'>";

									 if(addModalEventSpecialPriceTypeId == 300)
									 	htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
									 else if(addModalEventSpecialPriceTypeId == 301)
									 	htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
									 else if(addModalEventSpecialPriceTypeId == 302)
									 	htmlRow = htmlRow+"BOGO "+details;
									 else if(addModalEventSpecialPriceTypeId == 307)
									 	htmlRow = htmlRow+"Free "+details;

					htmlRow = htmlRow+"</p>"+
									 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
									  "<span style='cursor:pointer'>"+
									   "<a id='deleteModalEventSpecial"+data.specialId+"' onclick='deleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
									    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
									   "</a>"+
									  "</span>"+
									 "</p>"+
									"</div>"+
								   "</div>";
				}

				document.getElementById("modalEventSpecialsDiv").innerHTML = document.getElementById("modalEventSpecialsDiv").innerHTML + htmlRow;
				//TODOJAKE
				//document.getElementById("manageEventsAddedSpecials"+eventId).innerHTML += htmlRow;

				toggleAddModalEventSpecialPriceType('money');
				document.getElementById("addModalEventSpecial-details").value = "";
				chooseAddModalEventSpecialDealType(-1);
				toggleAddModalEventSpecialDiv();


				/* remove add special button if limit is reached */
				var limit = parseInt(document.getElementById("eventSpecialsLimit").value);
				var msd = document.getElementById("modalEventSpecialsDiv").innerHTML;
				var specialCount = 0;

				if(msd != "") 
					specialCount = (msd.match(/modalEventSpecialRow/g) || []).length;

				if(specialCount + 1 >= limit)
					document.getElementById("addModalEventSpecialTogglePlus").style.display = "none";
			}
		});
	}
}

function validateAddSpecialToEvent(p) {
	var valid = true;
	var msg = [];
	var cnt = 0;
	document.getElementById("addSpecialToEventErrors").innerHTML = "";

	// price
	if(p.price == "" && (p.priceType == 300 || p.priceType == 301)) {
		document.getElementById("addModalEventSpecial-price").style.backgroundColor = "#FFEAEA";
		document.getElementById("addModalEventSpecial-price").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='addModalEventSpecial-priceError'>Make sure you add a price!</p>";
		valid = false;
	}
	else {
		document.getElementById("addModalEventSpecial-price").style.backgroundColor = "#fff";
		document.getElementById("addModalEventSpecial-price").style.border = "1px solid #ccc";
	}

	// details
	if(p.details == "") {
		document.getElementById("addModalEventSpecial-details").style.backgroundColor = "#FFEAEA";
		document.getElementById("addModalEventSpecial-details").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='addModalEventSpecial-detailsError'>Make sure you add some details!</p>";
		valid = false;
	}
	else {
		document.getElementById("addModalEventSpecial-details").style.backgroundColor = "#fff";
		document.getElementById("addModalEventSpecial-details").style.border = "1px solid #ccc";
	}

	// icon
	if(p.specialType == "") {
		for(var i = 0; i <= 15; i++) {
			document.getElementById("addModalEventSpecial-deal-type-"+i).style.border = "1px solid #FF0000";
		}
		msg[cnt++] = "<p id='addModalEventSpecial-dealTypeError'>Make sure you choose an icon!</p>";
		valid = false;
	}
	else {
		for(var i = 0; i <= 15; i++) {
			document.getElementById("addModalEventSpecial-deal-type-"+i).style.border = "1px solid #c0c0c0";
		}
	}

	if(!valid) {
		for(var m in msg) {
			document.getElementById("addSpecialToEventErrors").innerHTML = document.getElementById("addSpecialToEventErrors").innerHTML + msg[m];
		}
	}

	return valid;
}

function clearCreateEventFields() {
	//if(document.getElementById("btnOneTime").className == "btn btn-default active") {
	//	document.getElementById("btnOneTime").className = "btn btn-default";
	//}
	//document.getElementById("btnReoccurring").className = "btn btn-default";
	//document.getElementById("oneTimeDiv").style.display = "none";
	//document.getElementById("reoccurDiv").style.display = "none";
	//document.getElementById("createEventSharedDiv").style.display = "none";
	//document.getElementById("weeklyEventsRemainingDiv").style.display = "none";
	document.getElementById("eventReoccurring").value = "";
	document.getElementById("eventTitle").value = "";
	document.getElementById("eventDetails").value = "";
	document.getElementById("eventStartDate").value = getYYYYMMDD(newDate());
	document.getElementById("eventStartTime").value = "";
	document.getElementById("eventEndDate").value = getYYYYMMDD(newDate());
	document.getElementById("eventEndTime").value = "";
	document.getElementById("eventStartTimeReoccurring").value = "";
	document.getElementById("eventEndTimeReoccurring").value = "";

	var w = new Array(7);
	w[0] = "sun";
	w[1] = "mon";
	w[2] = "tue";
	w[3] = "wed";
	w[4] = "thu";
	w[5] = "fri";
	w[6] = "sat";

	for(var i = 0; i <= 6; i++) {
		document.getElementById("createEvent-"+w[i]+"ToggleButton").className = "btn btn-default";
		document.getElementById("createEvent-"+w[i]+"ToggleButton").style.backgroundColor = "white";
		document.getElementById("createEvent-"+w[i]+"ToggleButton").style.borderColor = "#ccc";
		document.getElementById("createEvent-day-"+w[i]).value = false;
	}
}

function cancelCreateEvent() {
	if(document.getElementById("btnOneTime").className == "btn btn-default active") {
		document.getElementById("btnOneTime").className = "btn btn-default";
	}
	document.getElementById("btnReoccurring").className = "btn btn-default";
	document.getElementById("oneTimeDiv").style.display = "none";
	document.getElementById("reoccurDiv").style.display = "none";
	document.getElementById("createEventSharedDiv").style.display = "none";
	document.getElementById("weeklyEventsRemainingDiv").style.display = "none";
	document.getElementById("eventReoccurring").value = "";
	document.getElementById("eventTitle").value = "";
	document.getElementById("eventDetails").value = "";
	document.getElementById("eventStartDate").value = getYYYYMMDD(newDate());
	document.getElementById("eventStartTime").value = "";
	document.getElementById("eventEndDate").value = getYYYYMMDD(newDate());
	document.getElementById("eventEndTime").value = "";
	document.getElementById("eventStartTimeReoccurring").value = "";
	document.getElementById("eventEndTimeReoccurring").value = "";

	var w = new Array(7);
	w[0] = "sun";
	w[1] = "mon";
	w[2] = "tue";
	w[3] = "wed";
	w[4] = "thu";
	w[5] = "fri";
	w[6] = "sat";

	for(var i = 0; i <= 6; i++) {
		document.getElementById("createEvent-"+w[i]+"ToggleButton").className = "btn btn-default";
		document.getElementById("createEvent-"+w[i]+"ToggleButton").style.backgroundColor = "white";
		document.getElementById("createEvent-"+w[i]+"ToggleButton").style.borderColor = "#ccc";
		document.getElementById("createEvent-day-"+w[i]).value = false;
	}
}

function toggleReoccurring(reoccur) {
	if(reoccur == true) {
		document.getElementById("btnReoccurring").className = "btn btn-default active";
		if(document.getElementById("btnOneTime").className == "btn btn-default active") {
			document.getElementById("btnOneTime").className = "btn btn-default";
		}
		document.getElementById("reoccurDiv").style.display = "block";
		document.getElementById("oneTimeDiv").style.display = "none";
		document.getElementById("weeklyEventsRemainingDiv").style.display = "block";
		if(document.getElementById("weeklyEventsRemaining").value <= 0) {
			document.getElementById("createEventSharedDiv").style.filter = "blur(6px)";
			document.getElementById("createEventSharedDiv").style.pointerEvents = "none";
		}
	}
	else {
		document.getElementById("btnOneTime").className = "btn btn-default active";
		document.getElementById("btnReoccurring").className = "btn btn-default";
		document.getElementById("oneTimeDiv").style.display = "block";
		document.getElementById("reoccurDiv").style.display = "none";
		document.getElementById("weeklyEventsRemainingDiv").style.display = "none";
		document.getElementById("createEventSharedDiv").style.filter = "none";
		document.getElementById("createEventSharedDiv").style.pointerEvents = "auto";
	}

	document.getElementById("eventReoccurring").value = reoccur;
	document.getElementById("createEventSharedDiv").style.display = "block";
}

function selectCreateEventDay(day) {
	if(document.getElementById("createEvent-"+day+"ToggleButton").className == "btn btn-default") {
		document.getElementById("createEvent-"+day+"ToggleButton").className = "btn btn-default active";
		document.getElementById("createEvent-"+day+"ToggleButton").style.backgroundColor = "#e6e6e6";
		document.getElementById("createEvent-"+day+"ToggleButton").style.borderColor = "#adadad";
		document.getElementById("createEvent-day-"+day).value = true;
	} else {
		document.getElementById("createEvent-"+day+"ToggleButton").className = "btn btn-default";
		document.getElementById("createEvent-"+day+"ToggleButton").style.backgroundColor = "white";
		document.getElementById("createEvent-"+day+"ToggleButton").style.borderColor = "#ccc";
		document.getElementById("createEvent-day-"+day).value = false;
	}

	document.getElementById("createEvent-dayToggle").style.border = "none";
	if(document.getElementById("eventDaysError")) {
		document.getElementById("eventDaysError").style.display = "none";
	}
}

function toggleAddModalEventSpecialDiv() {
	var display = document.getElementById("addModalEventSpecialRow").style.display;
	if(display == "none" || display == "") {
		document.getElementById("addModalEventSpecialRow").style.display = "block";
		document.getElementById("addModalEventSpecialToggleMinus").style.display = "inline";
		document.getElementById("addModalEventSpecialTogglePlus").style.display = "none";

		var startDate = document.getElementById("modalEventStartDateHidden").innerHTML;
		var endDate = document.getElementById("modalEventEndDateHidden").innerHTML;

		if(startDate != null && startDate != "" && endDate != null && endDate != "") {
			if(daysBetween(startDate, endDate) > 0) {
				var dayToggles = '';
				startDate = new Date(startDate);
				endDate = new Date(endDate);

				startDate.addHoursDST();
				endDate.addHoursDST();

				while(startDate <= endDate) {
					var id = ""+(startDate.getMonth()+1)+startDate.getDate();
					dayToggles = dayToggles + 
							 '<div class="btn-group">'+
						      '<button id="day-modal-'+id+'" type="button" style="text-decoration: none" class="btn btn-default" onclick="toggleAddModalEventSpecialDay('+id+')">'+(startDate.getMonth()+1)+'/'+startDate.getDate()+'</button>'+
						     '</div>'+
						     '<input id="day-modal-'+id+'-hidden" type="hidden" value="'+startDate+'" />';
					startDate.setDate(startDate.getDate()+1);
				}

				document.getElementById("addModalEventSpecial-dayToggle").innerHTML = dayToggles;
			}
			else {
				document.getElementById("multidayDivModal").style.display = "none";
			}
		}
	}
	else {
		document.getElementById("addModalEventSpecialRow").style.display = "none";
		document.getElementById("addModalEventSpecialToggleMinus").style.display = "none";
		document.getElementById("addModalEventSpecialTogglePlus").style.display = "inline";
	}
}

function toggleAddModalEventSpecialPriceType(type) {
	document.getElementById("addModalEventSpecial-price").value = "";
	document.getElementById("addModalEventSpecial-price").className = "form-control";

	if (type == 'money') {
		addModalEventSpecialPriceType = "$";
		addModalEventSpecialPriceTypeId = 300;
		document.getElementById("addModalEventSpecial-price").disabled = false;
		document.getElementById("addModalEventSpecial-priceLabel").innerHTML = "Price";
		document.getElementById("addModalEventSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-moneySignInPriceTextbox").style.display = "block";
		document.getElementById("addModalEventSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-moneyToggleButton").className = "btn btn-default active";
		document.getElementById("addModalEventSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-freeToggleButton").className = "btn btn-default";
	}
	else if (type == 'percent') {
		addModalEventSpecialPriceType = "%";
		addModalEventSpecialPriceTypeId = 301;
		document.getElementById("addModalEventSpecial-price").disabled = false;
		document.getElementById("addModalEventSpecial-priceLabel").innerHTML = "Percent Off";
		document.getElementById("addModalEventSpecial-percentSignInPriceTextbox").style.display = "block";
		document.getElementById("addModalEventSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-percentToggleButton").className = "btn btn-default active";
		document.getElementById("addModalEventSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-freeToggleButton").className = "btn btn-default";
	} else if (type == 'bogo'){
		addModalEventSpecialPriceType = "Buy One Get One";
		addModalEventSpecialPriceTypeId = 302;
		document.getElementById("addModalEventSpecial-price").disabled = true;
		document.getElementById("addModalEventSpecial-priceLabel").innerHTML = "BOGO";
		document.getElementById("addModalEventSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-bogoInPriceTextbox").style.display = "block";
		document.getElementById("addModalEventSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-bogoToggleButton").className = "btn btn-default active";
		document.getElementById("addModalEventSpecial-freeToggleButton").className = "btn btn-default";
	} else if (type == 'free'){
		addModalEventSpecialPriceType = "Free";
		addModalEventSpecialPriceTypeId = 307;
		document.getElementById("addModalEventSpecial-price").disabled = true;
		document.getElementById("addModalEventSpecial-priceLabel").innerHTML = "Free";
		document.getElementById("addModalEventSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addModalEventSpecial-freeInPriceTextbox").style.display = "block";
		document.getElementById("addModalEventSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addModalEventSpecial-freeToggleButton").className = "btn btn-default active";
	}

	document.getElementById("hiddenAddModalEventSpecialPriceType").value = addModalEventSpecialPriceTypeId;

	document.getElementById("addModalEventSpecial-price").style.backgroundColor = "#fff";
	document.getElementById("addModalEventSpecial-price").style.border = "1px solid #ccc";
	if(document.getElementById("addModalEventSpecial-priceError")) {
		document.getElementById("addModalEventSpecial-priceError").style.display = "none";
	}
}

function chooseAddModalEventSpecialDealType(typeID) {
	isAddUcomingEventSpecialDealTypeSelected = false;
	addModalEventSpecialDealTypeId = getDealTypeRefIdByOGId(typeID);
  	document.getElementById("hiddenAddModalEventSpecialType").value = addModalEventSpecialDealTypeId;

  	var dealTypeString = "addModalEventSpecial-deal-type-";

  	for (var i = 0; i <= 15; i++) {
    	if (i == typeID) {
      		document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
      		document.getElementById(dealTypeString+i).style.border = "2px solid orange";
      		document.getElementById(dealTypeString+i).value = addModalEventSpecialDealTypeId;
      		isAddModalEventSpecialDealTypeSelected = true;
    	} else {
      		document.getElementById(dealTypeString+i).style.background = "white";
      		document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
      		document.getElementById(dealTypeString+i).value = 0;
    	}
  	}

  	if(document.getElementById("addModalEventSpecial-dealTypeError")) {
  		document.getElementById("addModalEventSpecial-dealTypeError").style.display = "none";
  	}
}



/* Today's events */

function saveEditEvent(e, eventId) {
	e.preventDefault();
	if(validateEditTodaysEvent(eventId)) {
		if(document.getElementById("edit-event-start-date-"+eventId)) {
			document.getElementById("hiddenTodaysEventId").value = eventId;
			document.getElementById("todaysEventsForm").submit();
		}
		else {
			var mon = document.getElementById("editEvent-day-mon-"+eventId).value;
			var tue = document.getElementById("editEvent-day-tue-"+eventId).value;
			var wed = document.getElementById("editEvent-day-wed-"+eventId).value;
			var thu = document.getElementById("editEvent-day-thu-"+eventId).value;
			var fri = document.getElementById("editEvent-day-fri-"+eventId).value;
			var sat = document.getElementById("editEvent-day-sat-"+eventId).value;
			var sund = document.getElementById("editEvent-day-sun-"+eventId).value;

			var eventCount = 0;
			if(mon == "true") { eventCount++; }
			if(tue == "true") { eventCount++; }
			if(wed == "true") { eventCount++; }
			if(thu == "true") { eventCount++; }
			if(fri == "true") { eventCount++; }
			if(sat == "true") { eventCount++; }
			if(sun == "true") { eventCount++; }

			var parameters = {
				eventCount: eventCount,
				eventIds: eventId
			};

			$.get('/validateevent', parameters, function(data) {
				if(data) {
					document.getElementById("hiddenTodaysEventId").value = eventId;
					document.getElementById("todaysEventsForm").submit();
				}
				else {
					document.getElementById("editEventErrors"+eventId).innerHTML = "<p id='edit-event-limitError'>Adding this event will put you over your 3 weekly events limit. Please delete a weekly event to add another.</p>";
				}
			});
		}
	}
}

function validateEditTodaysEvent(eventId) {
	var valid = true;
	var msg = [];
	var cnt = 0;
	document.getElementById("editEventErrors"+eventId).innerHTML = "";

	var recurring = false;
	var details = document.getElementById("edit-event-details-"+eventId).value;
	var startTime = document.getElementById("edit-event-start-time-"+eventId).value;
	var endTime = document.getElementById("edit-event-end-time-"+eventId).value;

	var startDate = "";
	var mon = "";
	var tue = "";
	var wed = "";
	var thu = "";
	var fri = "";
	var sat = "";
	var sund = "";

	if(document.getElementById("edit-event-start-date-"+eventId)) {
		startDate = document.getElementById("edit-event-start-date-"+eventId).value;
	}
	else {
		recurring = true;
		mon = document.getElementById("editEvent-day-mon-"+eventId).value;
		tue = document.getElementById("editEvent-day-tue-"+eventId).value;
		wed = document.getElementById("editEvent-day-wed-"+eventId).value;
		thu = document.getElementById("editEvent-day-thu-"+eventId).value;
		fri = document.getElementById("editEvent-day-fri-"+eventId).value;
		sat = document.getElementById("editEvent-day-sat-"+eventId).value;
		sund = document.getElementById("editEvent-day-sun-"+eventId).value;
	}

	// details
	if(details == "") {
		document.getElementById("edit-event-details-"+eventId).style.backgroundColor = "#FFEAEA";
		document.getElementById("edit-event-details-"+eventId).style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='edit-event-details-"+eventId+"Error'>Make sure to add some details!</p>";
		valid = false;
	}
	else {
		document.getElementById("edit-event-details-"+eventId).style.backgroundColor = "#fff";
		document.getElementById("edit-event-details-"+eventId).style.border = "1px solid #ccc";
	}

	// start time
	if(startTime == "") {
		document.getElementById("edit-event-start-time-"+eventId).style.backgroundColor = "#FFEAEA";
		document.getElementById("edit-event-start-time-"+eventId).style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='edit-event-start-time-"+eventId+"Error'>Make sure to choose a valid start time!</p>";
		valid = false;
	}
	else {
		document.getElementById("edit-event-start-time-"+eventId).style.backgroundColor = "#fff";
		document.getElementById("edit-event-start-time-"+eventId).style.border = "1px solid #ccc";
	}

	// end time
	if(endTime == "") {
		document.getElementById("edit-event-end-time-"+eventId).style.backgroundColor = "#FFEAEA";
		document.getElementById("edit-event-end-time-"+eventId).style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='edit-event-end-time-"+eventId+"Error'>Make sure to choose a valid end time!</p>";
		valid = false;
	}
	else {
		document.getElementById("edit-event-end-time-"+eventId).style.backgroundColor = "#fff";
		document.getElementById("edit-event-end-time-"+eventId).style.border = "1px solid #ccc";
	}

	if(!recurring) {
		// start date
		if(startDate == "") {
			document.getElementById("edit-event-start-date-"+eventId).style.backgroundColor = "#FFEAEA";
			document.getElementById("edit-event-start-date-"+eventId).style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='edit-event-start-date-"+eventId+"Error'>Make sure to choose a valid date!</p>";
			valid = false;
		}
		else {
			document.getElementById("edit-event-start-date-"+eventId).style.backgroundColor = "#fff";
			document.getElementById("edit-event-start-date-"+eventId).style.border = "1px solid #ccc";
		}

		if(startDate != "" && startTime != "" && endTime != "") {
			var start = getDateTime(startTime, startDate);
			var end = getDateTime(endTime, startDate);
			var now = newDate();

			if(start < now) {
				msg[cnt++] = "<p>Make sure the event starts in the future!</p>";
				valid = false;
			}
			if(end <= start) {
				msg[cnt++] = "<p>Make sure the end time is later than the start time!</p>";
				valid = false;
			}
		}
	}
	else {
		// day(s)
		if(mon == "false" && tue == "false" && wed == "false" && thu == "false" && fri == "false" && sat == "false" && sund == "false") {
			document.getElementById("editEvent-dayToggle"+eventId).style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventDays"+eventId+"Error'>Make sure to choose at least one day!</p>";
			valid = false;
		}
		else {
			document.getElementById("editEvent-dayToggle"+eventId).style.border = "none";
		}

		if(startTime != "" && endTime != "") {
			var now = newDate();
			var start = getDateTime(startTime, now);
			var end = getDateTime(endTime, now);

			if(end <= start) {
				msg[cnt++] = "<p>Make sure the end time is later than the start time!</p>";
				valid = false;
			}
		}
	}

	if(!valid) {
		for(var m in msg) {
			document.getElementById("editEventErrors"+eventId).innerHTML = document.getElementById("editEventErrors"+eventId).innerHTML + msg[m];
		}
	}

	return valid;
}

function deleteTodaysEvent(e, eventId) {
	e.preventDefault();
	document.getElementById("hiddenTodaysEventId").value = eventId;

	var recurring = false;
	if(document.getElementById("hiddenEditEventDayMon"+eventId).value == "true")
		recurring = true;
	else if(document.getElementById("hiddenEditEventDayTue"+eventId).value == "true")
		recurring = true;
	else if(document.getElementById("hiddenEditEventDayWed"+eventId).value == "true")
		recurring = true;
	else if(document.getElementById("hiddenEditEventDayThu"+eventId).value == "true")
		recurring = true;
	else if(document.getElementById("hiddenEditEventDayFri"+eventId).value == "true")
		recurring = true;
	else if(document.getElementById("hiddenEditEventDaySat"+eventId).value == "true")
		recurring = true;
	else if(document.getElementById("hiddenEditEventDaySun"+eventId).value == "true")
		recurring = true;

	var confirmText = "Are you sure you want to delete this event?";
	if(recurring)
		confirmText = confirmText + " This will delete all occurrences of this event."

	swal({
	  	title: "Are you sure?",
	  	text: confirmText,
	  	icon: "warning",
	  	dangerMode: true,
	  	buttons: {
		  	cancel: {
		    	text: "Cancel",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Yes, delete it!",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		document.getElementById("todaysEventsForm").action = "/deletetodaysevent";
	    	document.getElementById("todaysEventsForm").submit();
	  	} else {
	    	return false;
	  	}
	});
}

function addSpecialToTodaysEvent(eventId) {
	var price = document.getElementById("addEventSpecial-price"+eventId).value;
	var details = document.getElementById("addEventSpecial-details"+eventId).value;

	var parameters = {
		eventId: eventId,
		priceType: addEventSpecialPriceTypeId,
		price: price,
		details: details,
		specialType: addEventSpecialDealTypeId
	};

	if(validateAddSpecialToTodaysEvent(parameters)) {
		$.get('/addspecialtotodaysevent', parameters, function(data) {
			if(data) {
				var htmlRow = "<div class='row' id='editEventSpecialRow"+data.specialId+"'>"+
							   "<div class='col col-md-12 mobile-padding-left-right' style='line-height:1.5;'>"+
								"<img src='assets/img/"+getDealTypeImageByRefId(addEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
								 "<p style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'>";

								 if(addEventSpecialPriceTypeId == 300)
								 	htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
								 else if(addEventSpecialPriceTypeId == 301)
								 	htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
								 else if(addEventSpecialPriceTypeId == 302)
								 	htmlRow = htmlRow+"BOGO "+details;
								 else if(addEventSpecialPriceTypeId == 307)
								 	htmlRow = htmlRow+"Free "+details;

				htmlRow = htmlRow+"</p>"+
								 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
								  "<span style='cursor:pointer'>"+
								   "<a id='deleteEventSpecial"+data.specialId+"' onclick='deleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
								    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
								   "</a>"+
								  "</span>"+
								 "</p>"+
								"</div>"+
							   "</div>";

				document.getElementById("editEventsAddedSpecials"+eventId).innerHTML = document.getElementById("editEventsAddedSpecials"+eventId).innerHTML + htmlRow;

				htmlRow = "<div class='row' id='eventSpecialRow"+data.specialId+"'>"+
							   "<div class='col col-md-12' style='line-height:1.5;'>"+
								"<img src='assets/img/"+getDealTypeImageByRefId(addEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
								 "<p style='display:inline; vertical-align:middle; color:rgb(50,50,50); font-weight:500;'>";

								 if(addEventSpecialPriceTypeId == 300)
								 	htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
								 else if(addEventSpecialPriceTypeId == 301)
								 	htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
								 else if(addEventSpecialPriceTypeId == 302)
								 	htmlRow = htmlRow+"BOGO "+details;
								 else if(addEventSpecialPriceTypeId == 307)
								 	htmlRow = htmlRow+"Free "+details;

				htmlRow = htmlRow+"</p>"+
								"</div>"+
							   "</div>";

				document.getElementById("eventsAddedSpecials"+eventId).innerHTML += htmlRow;
				//TODOJAKE
				/*
				var newRow = htmlRow;
				newRow = newRow.replace("style='margin-left:0px; margin-right:0px;'", "");
				newRow = newRow.replace("style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'", "style='display:inline; vertical-align:middle;'");
				newRow = newRow.replace("id='addedEventSpecialRow"+data.specialId+"'", "id='upcomingEventSpecialRow"+data.specialId+"'");
				newRow = newRow.replace("<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
										  "<span style='cursor:pointer'>"+
										   "<a id='deleteUpcomingEventSpecial"+data.specialId+"' onclick='deleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
										    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
										   "</a>"+
										  "</span>"+
										 "</p>", "");
				
				for(var i = 0; i < eventIds.length; i++) {
					var el = "manageEventsAddedSpecials"+eventIds[i];
					var el2 = "manageEventsNoSpecialsRow"+eventIds[i];
					if(document.getElementById(el))
						document.getElementById(el).innerHTML += newRow;
					if(document.getElementById(el2))
						document.getElementById(el2).style.display = "none";
				}*/



				toggleAddEventSpecialPriceType('money', eventId);
				document.getElementById("addEventSpecial-details"+eventId).value = "";
				chooseAddEventSpecialDealType(-1, eventId);
				toggleAddEventSpecialDiv(eventId);

				if(document.getElementById("todaysEventsNoSpecialsRow"+eventId) && document.getElementById("editTodaysEventsNoSpecialsRow"+eventId)) {
					document.getElementById("todaysEventsNoSpecialsRow"+eventId).style.display = "none";
					document.getElementById("editTodaysEventsNoSpecialsRow"+eventId).style.display = "none";
				}

			}
		});
	}
}

function validateAddSpecialToTodaysEvent(p) {
	var valid = true;
	var msg = [];
	var cnt = 0;
	document.getElementById("addSpecialToTodaysEventErrors"+p.eventId).innerHTML = "";

	// price
	if(p.price == "" && (p.priceType == 300 || p.priceType == 301)) {
		document.getElementById("addEventSpecial-price"+p.eventId).style.backgroundColor = "#FFEAEA";
		document.getElementById("addEventSpecial-price"+p.eventId).style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='addEventSpecial-price"+p.eventId+"Error'>Make sure you add a price!</p>";
		valid = false;
	}
	else {
		document.getElementById("addEventSpecial-price"+p.eventId).style.backgroundColor = "#fff";
		document.getElementById("addEventSpecial-price"+p.eventId).style.border = "1px solid #ccc";
	}

	// details
	if(p.details == "") {
		document.getElementById("addEventSpecial-details"+p.eventId).style.backgroundColor = "#FFEAEA";
		document.getElementById("addEventSpecial-details"+p.eventId).style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='addEventSpecial-details"+p.eventId+"Error'>Make sure you add some details!</p>";
		valid = false;
	}
	else {
		document.getElementById("addEventSpecial-details"+p.eventId).style.backgroundColor = "#fff";
		document.getElementById("addEventSpecial-details"+p.eventId).style.border = "1px solid #ccc";
	}

	// icon
	if(p.specialType == "") {
		for(var i = 0; i <= 15; i++) {
			document.getElementById("addEventSpecial-deal-type-"+i+"-"+p.eventId).style.border = "1px solid #FF0000";
		}
		msg[cnt++] = "<p id='addEventSpecial-dealType"+p.eventId+"Error'>Make sure you choose an icon!</p>";
		valid = false;
	}
	else {
		for(var i = 0; i <= 15; i++) {
			document.getElementById("addEventSpecial-deal-type-"+i+"-"+p.eventId).style.border = "1px solid #c0c0c0";
		}
	}

	if(!valid) {
		for(var m in msg) {
			document.getElementById("addSpecialToTodaysEventErrors"+p.eventId).innerHTML = document.getElementById("addSpecialToTodaysEventErrors"+p.eventId).innerHTML + msg[m];
		}
	}

	return valid;
}

function selectEditEventDay(day, eventId, select) {
	var toggle = document.getElementById("editEvent-"+day+"ToggleButton"+eventId);

	if(toggle.className.indexOf("active") == -1 || select) {
		//toggle.className = "btn btn-default";
		//document.getElementById("editEvent-"+day+"ToggleButton"+eventId).style.backgroundColor = "#e6e6e6";
		//document.getElementById("editEvent-"+day+"ToggleButton"+eventId).style.borderColor = "#adadad";

		if(day == "mon") {
			document.getElementById("editEvent-"+day+"ToggleButton"+eventId).className = "btn btn-default btn-mon active";
		}
		else if(day == "sun") {
			document.getElementById("editEvent-"+day+"ToggleButton"+eventId).className = "btn btn-default btn-sun active";
		}
		else {
			document.getElementById("editEvent-"+day+"ToggleButton"+eventId).className = "btn btn-default btn-weekday active";
		}

		/*
		if(day == "mon") {
			toggle.style.color = "#fff !important";
  			toggle.style.backgroundColor = "#337ab7 !important";
  			toggle.style.borderColor = "#2e6da4 !important";
  			toggle.style.borderBottomLeftRadius = "4px !important";
  			toggle.style.borderTopLeftRadius = "4px !important";
  			toggle.style.boxShadow = "inset 0 3px 5px rgba(0,0,0,.125) !important";
		}
		else if(day == "sun") {
			toggle.style.color = "#fff !important";
  			toggle.style.backgroundColor = "#337ab7 !important";
  			toggle.style.borderColor = "#2e6da4 !important";
  			toggle.style.borderBottomRightRadius = "4px !important";
  			toggle.style.borderTopRightRadius = "4px !important";
  			toggle.style.boxShadow = "inset 0 3px 5px rgba(0,0,0,.125) !important";
		}
		else {
			toggle.style.color = "#fff !important";
  			toggle.style.backgroundColor = "#337ab7 !important";
  			toggle.style.borderColor = "#2e6da4 !important";
  			toggle.style.boxShadow = "inset 0 3px 5px rgba(0,0,0,.125) !important";
		}
		*/

		document.getElementById("editEvent-day-"+day+"-"+eventId).value = true;
	} else {
		//toggle.className = "btn btn-default";
		//toggle.style.backgroundColor = "white";
		//toggle.style.borderColor = "#ccc";

		if(day == "mon") {
			document.getElementById("editEvent-"+day+"ToggleButton"+eventId).className = "btn btn-default btn-mon";
		}
		else if(day == "sun") {
			document.getElementById("editEvent-"+day+"ToggleButton"+eventId).className = "btn btn-default btn-sun";
		}
		else {
			document.getElementById("editEvent-"+day+"ToggleButton"+eventId).className = "btn btn-default btn-weekday";
		}

		document.getElementById("editEvent-day-"+day+"-"+eventId).value = false;
	}

	document.getElementById("editEvent-dayToggle"+eventId).style.border = "none";
	if(document.getElementById("eventDays"+eventId+"Error")) {
		document.getElementById("eventDays"+eventId+"Error").style.display = "none";
	}
}

function editTodaysEvent(e, eventId, current) {
	e.preventDefault();

	var mon = document.getElementById("hiddenEditEventDayMon"+eventId).value;
	var tue = document.getElementById("hiddenEditEventDayTue"+eventId).value;
	var wed = document.getElementById("hiddenEditEventDayWed"+eventId).value;
	var thu = document.getElementById("hiddenEditEventDayThu"+eventId).value;
	var fri = document.getElementById("hiddenEditEventDayFri"+eventId).value;
	var sat = document.getElementById("hiddenEditEventDaySat"+eventId).value;
	var sund = document.getElementById("hiddenEditEventDaySun"+eventId).value;

	if(mon == "true") {
		selectEditEventDay("mon", eventId, true);
	}
	if(tue == "true") {
		selectEditEventDay("tue", eventId, true);
	}
	if(wed == "true") {
		selectEditEventDay("wed", eventId, true);
	}
	if(thu == "true") {
		selectEditEventDay("thu", eventId, true);
	}
	if(fri == "true") {
		selectEditEventDay("fri", eventId, true);
	}
	if(sat == "true") {
		selectEditEventDay("sat", eventId, true);
	}
	if(sund == "true") {
		selectEditEventDay("sun", eventId, true);
	}

	document.getElementById("editEvent"+eventId).style.display = "block";
	document.getElementById("showEvent"+eventId).style.display = "none";
	if(current) {
		document.getElementById("edit-event-details-"+eventId).disabled = true;
		if(document.getElementById("edit-event-start-"+eventId) != null) {
			document.getElementById("edit-event-start-"+eventId).disabled = true;
			document.getElementById("edit-event-end-"+eventId).disabled = true;
		}
		if(document.getElementById("edit-event-start-time-"+eventId) != null) {
			document.getElementById("edit-event-start-time-"+eventId).disabled = true;
			document.getElementById("edit-event-end-time-"+eventId).disabled = true;
		}
		if(document.getElementById("editEvent-monToggleButton"+eventId) != null) {
			document.getElementById("editEvent-monToggleButton"+eventId).disabled = true;
			document.getElementById("editEvent-tueToggleButton"+eventId).disabled = true;
			document.getElementById("editEvent-wedToggleButton"+eventId).disabled = true;
			document.getElementById("editEvent-thuToggleButton"+eventId).disabled = true;
			document.getElementById("editEvent-friToggleButton"+eventId).disabled = true;
			document.getElementById("editEvent-satToggleButton"+eventId).disabled = true;
			document.getElementById("editEvent-sunToggleButton"+eventId).disabled = true;
		}
	}
}

function cancelEditEvent(e, eventId) {
	e.preventDefault();
	document.getElementById("editEvent"+eventId).style.display = "none";
	document.getElementById("showEvent"+eventId).style.display = "block";
}

function toggleAddEventSpecialDiv(eventId) {
	var display = document.getElementById("addEventSpecialRow"+eventId).style.display;
	if(display == "none" || display == "") {
		document.getElementById("addEventSpecialRow"+eventId).style.display = "block";
		document.getElementById("addEventSpecialToggleMinus"+eventId).style.display = "inline";
		document.getElementById("addEventSpecialTogglePlus"+eventId).style.display = "none";
	}
	else {
		document.getElementById("addEventSpecialRow"+eventId).style.display = "none";
		document.getElementById("addEventSpecialToggleMinus"+eventId).style.display = "none";
		document.getElementById("addEventSpecialTogglePlus"+eventId).style.display = "inline";
	}
}

function toggleAddEventSpecialPriceType(type, eventId) {
	document.getElementById("addEventSpecial-price"+eventId).value = "";
	document.getElementById("addEventSpecial-price"+eventId).className = "form-control";

	if (type == 'money') {
		addEventSpecialPriceType = "$";
		addEventSpecialPriceTypeId = 300;
		document.getElementById("addEventSpecial-price"+eventId).disabled = false;
		document.getElementById("addEventSpecial-priceLabel"+eventId).innerHTML = "Price";
		document.getElementById("addEventSpecial-percentSignInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-moneySignInPriceTextbox"+eventId).style.display = "block";
		document.getElementById("addEventSpecial-bogoInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-freeInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-moneyToggleButton"+eventId).className = "btn btn-default active";
		document.getElementById("addEventSpecial-percentToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-bogoToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-freeToggleButton"+eventId).className = "btn btn-default";
	}
	else if (type == 'percent') {
		addEventSpecialPriceType = "%";
		addEventSpecialPriceTypeId = 301;
		document.getElementById("addEventSpecial-price"+eventId).disabled = false;
		document.getElementById("addEventSpecial-priceLabel"+eventId).innerHTML = "Percent Off";
		document.getElementById("addEventSpecial-percentSignInPriceTextbox"+eventId).style.display = "block";
		document.getElementById("addEventSpecial-moneySignInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-bogoInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-freeInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-moneyToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-percentToggleButton"+eventId).className = "btn btn-default active";
		document.getElementById("addEventSpecial-bogoToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-freeToggleButton"+eventId).className = "btn btn-default";
	} else if (type == 'bogo'){
		addEventSpecialPriceType = "Buy One Get One";
		addEventSpecialPriceTypeId = 302;
		document.getElementById("addEventSpecial-price"+eventId).disabled = true;
		document.getElementById("addEventSpecial-priceLabel"+eventId).innerHTML = "BOGO";
		document.getElementById("addEventSpecial-percentSignInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-moneySignInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-bogoInPriceTextbox"+eventId).style.display = "block";
		document.getElementById("addEventSpecial-freeInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-moneyToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-percentToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-bogoToggleButton"+eventId).className = "btn btn-default active";
		document.getElementById("addEventSpecial-freeToggleButton"+eventId).className = "btn btn-default";
	} else if (type == 'free'){
		addEventSpecialPriceType = "Free";
		addEventSpecialPriceTypeId = 307;
		document.getElementById("addEventSpecial-price"+eventId).disabled = true;
		document.getElementById("addEventSpecial-priceLabel"+eventId).innerHTML = "Free";
		document.getElementById("addEventSpecial-percentSignInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-moneySignInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-bogoInPriceTextbox"+eventId).style.display = "none";
		document.getElementById("addEventSpecial-freeInPriceTextbox"+eventId).style.display = "block";
		document.getElementById("addEventSpecial-moneyToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-percentToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-bogoToggleButton"+eventId).className = "btn btn-default";
		document.getElementById("addEventSpecial-freeToggleButton"+eventId).className = "btn btn-default active";
	}

	document.getElementById("hiddenAddEventSpecialPriceType"+eventId).value = addEventSpecialPriceTypeId;

	document.getElementById("addEventSpecial-price"+eventId).style.backgroundColor = "#fff";
	document.getElementById("addEventSpecial-price"+eventId).style.border = "1px solid #ccc";
	if(document.getElementById("addEventSpecial-price"+eventId+"Error")) {
		document.getElementById("addEventSpecial-price"+eventId+"Error").style.display = "none";
	}
}

function chooseAddEventSpecialDealType(typeID, eventId) {
	isAddEventSpecialDealTypeSelected = false;
	addEventSpecialDealTypeId = getDealTypeRefIdByOGId(typeID);
  	document.getElementById("hiddenAddEventSpecialType"+eventId).value = addEventSpecialDealTypeId;

  	var dealTypeString = "addEventSpecial-deal-type-";

  	for (var i = 0; i <= 15; i++) {
    	if (i == typeID) {
      		document.getElementById(dealTypeString+i+"-"+eventId).style.background = "rgba(255, 215, 0, 0.7)";
      		document.getElementById(dealTypeString+i+"-"+eventId).style.border = "2px solid orange";
      		document.getElementById(dealTypeString+i+"-"+eventId).value = addEventSpecialDealTypeId;
      		isAddEventSpecialDealTypeSelected = true;
    	} else {
      		document.getElementById(dealTypeString+i+"-"+eventId).style.background = "white";
      		document.getElementById(dealTypeString+i+"-"+eventId).style.border = "1px solid #c0c0c0";
      		document.getElementById(dealTypeString+i+"-"+eventId).value = 0;
    	}
  	}

  	if(document.getElementById("addEventSpecial-dealType"+eventId+"Error")) {
  		document.getElementById("addEventSpecial-dealType"+eventId+"Error").style.display = "none";
  	}
}

function toggleAddSpecialDiv() {
	var display = document.getElementById("addSpecialRow").style.display;
	if(display == "none" || display == "") {
		document.getElementById("addSpecialRow").style.display = "block";
		document.getElementById("addSpecialToggleMinus").style.display = "inline";
		document.getElementById("addSpecialTogglePlus").style.display = "none";
	}
	else {
		document.getElementById("addSpecialRow").style.display = "none";
		document.getElementById("addSpecialToggleMinus").style.display = "none";
		document.getElementById("addSpecialTogglePlus").style.display = "inline";
	}
}

function toggleAddSpecialPriceType(type) {
	document.getElementById("addSpecial-price").value = "";
	document.getElementById("addSpecial-price").className = "form-control";

	if (type == 'money') {
		addSpecialPriceType = "$";
		addSpecialPriceTypeId = 300;
		document.getElementById("addSpecial-price").disabled = false;
		document.getElementById("addSpecial-priceLabel").innerHTML = "Price";
		document.getElementById("addSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-moneySignInPriceTextbox").style.display = "block";
		document.getElementById("addSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-moneyToggleButton").className = "btn btn-default active";
		document.getElementById("addSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-freeToggleButton").className = "btn btn-default";
	}
	else if (type == 'percent') {
		addSpecialPriceType = "%";
		addSpecialPriceTypeId = 301;
		document.getElementById("addSpecial-price").disabled = false;
		document.getElementById("addSpecial-priceLabel").innerHTML = "Percent Off";
		document.getElementById("addSpecial-percentSignInPriceTextbox").style.display = "block";
		document.getElementById("addSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-percentToggleButton").className = "btn btn-default active";
		document.getElementById("addSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-freeToggleButton").className = "btn btn-default";
	} else if (type == 'bogo'){
		addSpecialPriceType = "Buy One Get One";
		addSpecialPriceTypeId = 302;
		document.getElementById("addSpecial-price").disabled = true;
		document.getElementById("addSpecial-priceLabel").innerHTML = "BOGO";
		document.getElementById("addSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-bogoInPriceTextbox").style.display = "block";
		document.getElementById("addSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-bogoToggleButton").className = "btn btn-default active";
		document.getElementById("addSpecial-freeToggleButton").className = "btn btn-default";
	} else if (type == 'free'){
		addSpecialPriceType = "Free";
		addSpecialPriceTypeId = 307;
		document.getElementById("addSpecial-price").disabled = true;
		document.getElementById("addSpecial-priceLabel").innerHTML = "Free";
		document.getElementById("addSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addSpecial-freeInPriceTextbox").style.display = "block";
		document.getElementById("addSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addSpecial-freeToggleButton").className = "btn btn-default active";
	}

	document.getElementById("hiddenAddSpecialPriceType").value = addSpecialPriceTypeId;
}

function chooseAddSpecialDealType(typeID) {
	isAddSpecialDealTypeSelected = false;
	addSpecialDealTypeId = getDealTypeRefIdByOGId(typeID);
  	document.getElementById("hiddenAddSpecialType").value = addSpecialDealTypeId;

  	var dealTypeString = "addSpecial-deal-type-";

  	for (var i = 0; i <= 10; i++) {
    	if (i == typeID) {
      		document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
      		document.getElementById(dealTypeString+i).style.border = "2px solid orange";
      		document.getElementById(dealTypeString+i).value = addSpecialDealTypeId;
      		isAddSpecialDealTypeSelected = true;
    	} else {
      		document.getElementById(dealTypeString+i).style.background = "white";
      		document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
      		document.getElementById(dealTypeString+i).value = 0;
    	}
  	}
}

function toggleAddModalEventSpecialDay(dt) {
	if(document.getElementById("day-modal-"+dt).className == "btn btn-default") {
		document.getElementById("day-modal-"+dt).className = "btn btn-default dayActive";
		document.getElementById("day-modal-"+dt).style.backgroundColor = "#e6e6e6";
		document.getElementById("day-modal-"+dt).style.borderColor = "#adadad";
	} 
	else {
		document.getElementById("day-modal-"+dt).className = "btn btn-default";
		document.getElementById("day-modal-"+dt).style.backgroundColor = "white";
		document.getElementById("day-modal-"+dt).style.borderColor = "#ccc";
	}
}

function selectAllDaysModal(yes) {
	var startDate = document.getElementById("modalEventStartDateHidden").innerHTML;
	var endDate = document.getElementById("modalEventEndDateHidden").innerHTML;

	startDate = new Date(startDate);
	endDate = new Date(endDate);

	startDate.addHoursDST();
	endDate.addHoursDST();

	if(yes) {
		while(startDate <= endDate) {
			var id = ""+(startDate.getMonth()+1)+startDate.getDate();
			document.getElementById("day-modal-"+id).className = "btn btn-default dayActive";
			document.getElementById("day-modal-"+id).style.backgroundColor = "#e6e6e6";
			document.getElementById("day-modal-"+id).style.borderColor = "#adadad";
			startDate.setDate(startDate.getDate()+1);
		}
	} 
	else {
		while(startDate <= endDate) {
			var id = ""+(startDate.getMonth()+1)+startDate.getDate();
			document.getElementById("day-modal-"+id).className = "btn btn-default";
			document.getElementById("day-modal-"+id).style.backgroundColor = "white";
			document.getElementById("day-modal-"+id).style.borderColor = "#ccc";
			startDate.setDate(startDate.getDate()+1);
		}
	}
}



/* Manage upcoming events */

/*
jQuery(document).ready(checkContainer);

function checkContainer () {
    if($('#weekEvents').is(':visible')) { //if the container is visible on the page
        init();
    } else {
        setTimeout(checkContainer, 50); //wait 50 ms, then try again
    }
}

function init() {
	var curr = newDate();
    curr.setHours(0,0,0,0);

    labels = getWeekLabels();
    document.getElementById("weekEvents").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);
    loadUpcomingEvents();

	$('#weekEvents').datepicker({
        weekStart: 1
    }).on('changeDate', function(ev) {
        $('#weekEvents').datepicker('hide');
        document.getElementById("weekEvents").disabled = true;
        document.getElementById("prevWeekEvents").disabled = true;
        document.getElementById("nextWeekEvents").disabled = true;
        document.getElementById("prevWeekEventsMobile").disabled = true;
        document.getElementById("nextWeekEventsMobile").disabled = true;

        var date = ev.date;
        if(date.getDay() > 0) {
        	startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()+1);
        	endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - date.getDay()+7);
    	}
    	else {
    		startDate = new Date(date.getFullYear(), date.getMonth(), date.getDate() - 6);
        	endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    	}
        $('#weekEvents').datepicker('update', startDate);

        var currYear = curr.getWeekYear();
        var currWeek = curr.getWeek();
        var dateYear = date.getWeekYear();
        var dateWeek = date.getWeek();

        var yearDiff = currYear - dateYear;
        var weekDiff = currWeek - dateWeek;

        if(yearDiff != 0) {
            week = -(currWeek + (53 * yearDiff) - dateWeek);
        }
        else {
            if(weekDiff != 0)
                week = -weekDiff;
            else
                week = 0;
        }

  		labels = getWeekLabels();
    	document.getElementById("weekEvents").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);
    	loadUpcomingEvents();
    }).on("show", function (date) {
        var datepickerDropDown = $('.datepicker');
        datepickerDropDown.addClass("weekPicker");
    });
}

function loadUpcomingEvents() {
	var addDays = 1;
    if(week != 0)
        addDays = week * 7 + 1;

    var curr = newDate();
    curr.setHours(0,0,0,0);
    var first = 0;
    if(curr.getDay() != 0)
    	first = curr.getDate() - curr.getDay() + addDays;
    else
    	first = curr.getDate() - 7 + addDays;
    var startDate;
    for(var i = 0; i <= 6; i++) {
    	var d;
    	if(i == 0) {
    		startDate = new Date(curr.setDate(first));
    		d = startDate;
    	} else {
    		d = new Date(curr.setDate(first));
    	}
        first = first + 1;
        curr = newDate();
        curr.setHours(0,0,0,0);
    }
    var endDate = new Date(curr.setDate(first));

    var parameters = {
    	week: week,
		startDate: startDate,
		endDate: endDate
	};

	$.get('/loadeventsforweek', parameters, function(data) {
		var now = newDate();
		var tmw = newDate();
		tmw.setDate(tmw.getDate()+1);

		var monRow = [];
		var tueRow = [];
		var wedRow = [];
		var thuRow = [];
		var friRow = [];
		var satRow = [];
		var sunRow = [];

		// get recurring days
		var dataArr = [];
		var count = 0;
		if(data.length > 0) {
			dataArr[count++] = [data[0], [data[0].day]];

			for(var x = 1; x < data.length; x++) {
				var match = false;
				for(var y = 0; y < dataArr.length; y++) {
					if(data[x].title == dataArr[y][0].title) {
						match = true;
						dataArr[y][1].push(data[x].day);
					}
				}
				if(!match) {
					dataArr[count++] = [data[x], [data[x].day]];
				}
			}

			for(var v = 0; v < data.length; v++) {
				for(var w = 0; w < dataArr.length; w++) {
					if(data[v].title == dataArr[w][0].title) {
						data[v].days = dataArr[w][1];
					}
				}
			}

			for(var c in data) {
				var start = new Date(data[c].start_time);
				var end = new Date(data[c].end_time);
				var day = data[c].day;
				var days = data[c].days;

				start.addHoursDST();
				end.addHoursDST();

				if(day != -1) {
					var addDays = 1;
				    if(week != 0)
				        addDays = week * 7 + 1;

				    curr = newDate();
				    curr.setHours(0,0,0,0);
				    var first = 0;
				    if(curr.getDay() != 0)
				    	first = curr.getDate() - curr.getDay() + addDays;
				    else
				    	first = curr.getDate() - 7 + addDays;
				    for(var i = 0; i <= 6; i++) {
				        var d = new Date(curr.setDate(first));
				        var dTmw = new Date(curr.setDate(first));
				        dTmw.setDate(dTmw.getDate()+1);
				        var d2 = new Date(curr.setDate(first));
				        var d2Tmw = new Date(curr.setDate(first));
				        d2Tmw.setDate(d2Tmw.getDate()+1);

				        if(i == day) {
				        	var startTime;
				        	var endTime;

				        	if(start.getHours() >= 4) {
				        		startTime = d;
				        	} else {
				        		startTime = dTmw;
				        	}

				        	if(end.getHours() >= 4) {
				        		endTime = d2;
				        	} else {
				        		endTime = d2Tmw;
				        	}

				        	startTime.setHours(start.getHours());
				        	startTime.setMinutes(start.getMinutes());

				        	endTime.setHours(end.getHours());
				        	endTime.setMinutes(end.getMinutes());

				        	start = startTime;
				        	end = endTime;
				        }

				        first = first + 1;
				        curr = newDate();
				        curr.setHours(0,0,0,0);
				    }
				}

				data[c].duration = formatStartEndTime(start)+" - "+formatStartEndTime(end);

				if(start.getDate() != end.getDate() || start.getMonth() != end.getMonth() || start.getYear() != end.getYear()) {
					data[c].multiday = true;
					data[c].startDate = start.getMonth()+1 + "/" + start.getDate();
					data[c].startTime = formatStartEndTime(start);
					data[c].endDate = end.getMonth()+1 + "/" + end.getDate();
					data[c].endTime = formatStartEndTime(end);
				}


				var rowHtml = "";

				if(data[c].expired)
					rowHtml = "<tr id='showUpcomingEvent"+data[c].event_id+"' style='border-bottom:1px solid rgba(192,192,192,0.5); background-color:rgba(192,192,192,0.1); color:rgb(192,192,192);'>";
				else
					rowHtml = "<tr id='showUpcomingEvent"+data[c].event_id+"' style='border-bottom:1px solid rgba(192,192,192,0.5);'>";

				rowHtml = rowHtml + "<td style='width:40%; padding-left:5px; vertical-align:middle;'>"+
							    	 "<div class='upcoming-event-title'>"+
									  data[c].title+
									 "</div>"+
							   		"</td>"+
							   		"<td style='width:40%; padding-left:5px; vertical-align:middle;'>";

				if(data[c].expired) {
					rowHtml = rowHtml + "<div class='upcoming-event-duration' style='color:rgba(255,0,0,0.4);'>" + data[c].duration + "</div>";
				}
				else if(data[c].upcoming) {
					if(data[c].multiday) {
						rowHtml = rowHtml + "<div class='upcoming-event-duration' style='display:inline-block;'>" +
											 "<p style='color:red; font-size:0.8em; line-height:1;'>" + data[c].startDate + "</p>" +
											 "<p style='color:red;'>" + data[c].startTime + "</p>" +
											"</div>" +
											"<div style='display:inline-block; vertical-align:super; width:20px;'>" +
										     "<p style='text-align:center; color:red;'>-</p>" +
											"</div>" +
											"<div class='upcoming-event-duration' style='display:inline-block;'>" +
											 "<p style='color:red; font-size:0.8em; line-height:1;'>" + data[c].endDate + "</p>" +
											 "<p style='color:red;'>" + data[c].endTime + "</p>" +
											"</div>";
					}
					else {
						rowHtml = rowHtml + "<div class='upcoming-event-duration' style='color:red;'>" + data[c].duration + "</div>";
					}
				}
				else if(data[c].current) {
					rowHtml = rowHtml + "<div class='upcoming-event-duration' style='color:red;'>" + data[c].timeremaining + "</div>";
				}

				rowHtml = rowHtml+"</td>"+
							"<td style='width:10%; padding-left:5px; vertical-align:middle;'>"+
							 "<div class='upcoming-event-reoccurring'>";

				if(data[c].day > -1) {
					rowHtml = rowHtml+"<a href='' onclick='event.preventDefault()' data-toggle='tooltip' title='Every "+daysToString(data[c].days)+"'>"+
									   "<i class='fa fa-retweet' style='margin-right:1px; color:#797979;'></i></a>";
				}

				rowHtml = rowHtml+"</div></td>"+
							"<td style='width:10%; padding-left:5px; vertical-align:middle; text-align:right;'>";

				if(data[c].upcoming || data[c].day > -1) {
					rowHtml = rowHtml+"<div class='upcoming-event-edit-div'>"+
									   "<span style='cursor:pointer'>"+
										"<a onclick='editEvent(event, "+data[c].event_id+")' data-toggle='tooltip' title='Edit Event' data-target='#edit-upcoming-event-modal'>"+
								  		 "<i class='fa fa-pencil fa-sm'></i>"+
								  		"</a>"+
								  	   "</span>"+
								  	  "</div>";
				}
				rowHtml = rowHtml+"</td></tr>";

				if(data[c].expired)
					rowHtml = rowHtml+"<tr><td colspan='4' style='padding:0 0 10px 15px; vertical-align:middle; border:0; font-size:0.8em; background-color:rgba(192,192,192,0.1); color:rgb(192,192,192);'>"+data[c].event_details+"</td></tr>";
				else
					rowHtml = rowHtml+"<tr><td colspan='4' style='padding:0 0 10px 15px; vertical-align:middle; border:0; font-size:0.8em;'>"+data[c].event_details+"</td></tr>";

				for(var s = 0; s < data[c].specials.length; s++) {
					var sp = data[c].specials[s];

					if(data[c].expired)
						rowHtml = rowHtml+"<tr style='background-color:rgba(192,192,192,0.1); color:rgb(192,192,192);'>";
					else
						rowHtml = rowHtml+"<tr>";

					if(s < data[c].specials.length-1)
						rowHtml = rowHtml+"<td colspan='4' style='padding:0 0 0 30px; vertical-align:middle; border:0; font-size:0.8em;'>";
					else
						rowHtml = rowHtml+"<td colspan='4' style='padding:0 0 10px 30px; vertical-align:middle; border:0; font-size:0.8em;'>";

					rowHtml = rowHtml+"<img src='assets/img/"+getDealTypeImageByRefId(sp.deal_type_ref_id)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>";

					if (sp.price_type_ref_id == 300)
	                    rowHtml = rowHtml+ "<div style='display:inline; vertical-align:middle;'>$" + sp.price + " " + sp.details + "</div></div></td>";
	                else if (sp.price_type_ref_id == 301)
	                    rowHtml = rowHtml+"<div style='display:inline; vertical-align:middle;'>"+sp.price+"% Off " + sp.details + "</div></td>";
	                else if (sp.price_type_ref_id == 302)
	                    rowHtml = rowHtml+"<div style='display:inline; vertical-align:middle;'>BOGO " + sp.details + "</div></td>";
	                else if (sp.price_type_ref_id == 307)
	                    rowHtml = rowHtml+"<div style='display:inline; vertical-align:middle;'>Free " + sp.details + "</div></td>";

					rowHtml = rowHtml+"</tr>";
				}

				var startTime = data[c].start_time.replace("Z", "");
				var endTime = data[c].end_time.replace("Z", "");

				rowHtml = rowHtml+"<tr style='display:none;' id='editUpcomingEvent"+data[c].event_id+"'>"+
								   "<td colspan='4' style='border:1px solid rgba(192,192,192,0.5); padding: 10px 20px 20px 20px;'>"+
								    "<div class='row'>"+
								     "<div class='col col-md-4'>"+
								      "<div class='editUpcomingTitle'>"+
	                        		   "<label for='editEventTitle'>Title</label>"+
	                        		   "<input type='text' class='form-control' id='edit-upcoming-event-title-"+data[c].event_id+"' value='"+data[c].title+"' name='editUpcomingEventTitle' disabled />"+
	                    			  "</div>"+
	                    			 "</div>"+
	                    			 "<div class='col col-md-4'>"+
	                    			  "<div class='editUpcomingStart'>"+
	                    			   "<label for='editUpcomingEventStart'>Start Date & Time</label>"+
	                        		   "<input type='datetime-local' class='form-control' id='edit-upcoming-event-start-"+data[c].event_id+"' value='"+startTime+"' name='editUpcomingEventStart' required />"+
	                    			  "</div>"+
	                    			 "</div>"+
	                    			 "<div class='col col-md-4'>"+
	                    			  "<div class='editUpcomingEnd'>"+
	                    			   "<label for='editUpcomingEventEnd'>End Date & Time</label>"+
	                        		   "<input type='datetime-local' class='form-control' id='edit-upcoming-event-end-"+data[c].event_id+"' value='"+endTime+"' name='editUpcomingEventEnd' required />"+
	                    			  "</div>"+
	                    			 "</div>"+
	                    		    "</div>"+
	                    		    "<div class='row' style='margin-top:10px'>"+
	                    		     "<div class='col col-md-12'>"+
	                    			  "<div class='editUpcomingDetails'>"+
	                    			   "<label for='editUpcomingDetails'>Details</label>"+
	                        		   "<input type='text' class='form-control' id='edit-upcoming-event-details-"+data[c].event_id+"' value='"+data[c].event_details+"' name='editUpcomingEventDetails' required />"+
	                    			  "</div>"+
	                    			 "</div>"+
	                    		    "</div>"+
	                    		   "</td>"+
	                    		  "</tr>";


	            var startDay = data[c].day;
	            if(startDay == -1)
	            	startDay = start.getDay();

				if(startDay == 1)
					monRow.push(rowHtml);
				else if(startDay == 2)
					tueRow.push(rowHtml);
				else if(startDay == 3)
					wedRow.push(rowHtml);
				else if(startDay == 4)
					thuRow.push(rowHtml);
				else if(startDay == 5)
					friRow.push(rowHtml);
				else if(startDay == 6)
					satRow.push(rowHtml);
				else if(startDay == 0)
					sunRow.push(rowHtml);
			}
		}

		var htmlStr = "<table id='upcomingEvents' class='table' style='margin-top:20px; border-collapse:unset;'>"+
	    			   "<tbody>"+
						"<tr style='background-color:rgba(43,73,145,0.1); color:rgba(0,0,0,0.8);'>"+
						 "<td colspan='4' style='padding:3px 3px 3px 6px; border-top:0;'>"+
						  "<div id='monLabel' class='upcoming-day-label'>Monday"+
						   "<span style='padding-left:3px; padding-right:3px;'>&#8729;</span>"+
						   labels[0]+getDateSuperscript(labels[0])+
						  "</div>"+
						 "</td>"+
						"</tr>";

		for(var mon in monRow) {
			htmlStr = htmlStr + monRow[mon];
		}

		if(monRow.length == 0) {
			htmlStr = htmlStr + "<tr>"+
								 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'>"+
								  "<span style='font-size:.8em; color:rgb(150,150,150);'>"+
							  	   "No events"+
							  	  "</span>"+
							  	 "</td>"+
							  	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	"</tr>";

		}

		htmlStr = htmlStr + "<tr>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	"</tr>";

		htmlStr = htmlStr + "<tr style='background-color:rgba(43,73,145,0.1);'>"+
							 "<td colspan='4' style='padding:3px 3px 3px 6px; border-top:0; color:rgba(0,0,0,0.8);'>"+
							  "<div id='tueLabel' class='upcoming-day-label'>Tuesday"+
							   "<span style='padding-left:3px; padding-right:3px;'>&#8729;</span>"+
							   labels[1]+getDateSuperscript(labels[1])+
							  "</div>"+
							 "</td>"+
							"</tr>";

		for(var tue in tueRow) {
			htmlStr = htmlStr + tueRow[tue];
		}

		if(tueRow.length == 0) {
			htmlStr = htmlStr + "<tr>"+
								 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'>"+
								  "<span style='font-size:.8em; color:rgb(150,150,150);'>"+
							  	   "No events"+
							  	  "</span>"+
							  	 "</td>"+
							  	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	"</tr>";

		}

		htmlStr = htmlStr + "<tr>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	"</tr>";

		htmlStr = htmlStr + "<tr style='background-color:rgba(43,73,145,0.1);'>"+
							 "<td colspan='4' style='padding:3px 3px 3px 6px; border-top:0; color:rgba(0,0,0,0.8);'>"+
							  "<div id='wedLabel' class='upcoming-day-label'>Wednesday"+
							   "<span style='padding-left:3px; padding-right:3px;'>&#8729;</span>"+
							   labels[2]+getDateSuperscript(labels[2])+
							  "</div>"+
							 "</td>"+
							"</tr>";

		for(var wed in wedRow) {
			htmlStr = htmlStr + wedRow[wed];
		}

		if(wedRow.length == 0) {
			htmlStr = htmlStr + "<tr>"+
								 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'>"+
								  "<span style='font-size:.8em; color:rgb(150,150,150);'>"+
							  	   "No events"+
							  	  "</span>"+
							  	 "</td>"+
							  	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	"</tr>";

		}

		htmlStr = htmlStr + "<tr>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	"</tr>";

		htmlStr = htmlStr + "<tr style='background-color:rgba(43,73,145,0.1);'>"+
							 "<td colspan='4' style='padding:3px 3px 3px 6px; border-top:0; color:rgba(0,0,0,0.8);'>"+
							  "<div id='thuLabel' class='upcoming-day-label'>Thursday"+
							   "<span style='padding-left:3px; padding-right:3px;'>&#8729;</span>"+
							   labels[3]+getDateSuperscript(labels[3])+
							  "</div>"+
							 "</td>"+
							"</tr>";

		for(var thu in thuRow) {
			htmlStr = htmlStr + thuRow[thu];
		}

		if(thuRow.length == 0) {
			htmlStr = htmlStr + "<tr>"+
								 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'>"+
								  "<span style='font-size:.8em; color:rgb(150,150,150);'>"+
							  	   "No events"+
							  	  "</span>"+
							  	 "</td>"+
							  	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	"</tr>";

		}

		htmlStr = htmlStr + "<tr>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	"</tr>";

		htmlStr = htmlStr + "<tr style='background-color:rgba(43,73,145,0.1);'>"+
							 "<td colspan='4' style='padding:3px 3px 3px 6px; border-top:0; color:rgba(0,0,0,0.8);'>"+
							  "<div id='friLabel' class='upcoming-day-label'>Friday"+
							   "<span style='padding-left:3px; padding-right:3px;'>&#8729;</span>"+
							   labels[4]+getDateSuperscript(labels[4])+
							  "</div>"+
							 "</td>"+
							"</tr>";

		for(var fri in friRow) {
			htmlStr = htmlStr + friRow[fri];
		}

		if(friRow.length == 0) {
			htmlStr = htmlStr + "<tr>"+
								 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'>"+
								  "<span style='font-size:.8em; color:rgb(150,150,150);'>"+
							  	   "No events"+
							  	  "</span>"+
							  	 "</td>"+
							  	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	"</tr>";

		}

		htmlStr = htmlStr + "<tr>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	"</tr>";

		htmlStr = htmlStr + "<tr style='background-color:rgba(43,73,145,0.1);'>"+
							 "<td colspan='4' style='padding:3px 3px 3px 6px; border-top:0; color:rgba(0,0,0,0.8);'>"+
							  "<div id='satLabel' class='upcoming-day-label'>Saturday"+
							   "<span style='padding-left:3px; padding-right:3px;'>&#8729;</span>"+
							   labels[5]+getDateSuperscript(labels[5])+
							  "</div>"+
							 "</td>"+
							"</tr>";

		for(var sat in satRow) {
			htmlStr = htmlStr + satRow[sat];
		}

		if(satRow.length == 0) {
			htmlStr = htmlStr + "<tr>"+
								 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'>"+
								  "<span style='font-size:.8em; color:rgb(150,150,150);'>"+
							  	   "No events"+
							  	  "</span>"+
							  	 "</td>"+
							  	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	"</tr>";

		}

		htmlStr = htmlStr + "<tr>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	"</tr>";

		htmlStr = htmlStr + "<tr style='background-color:rgba(43,73,145,0.1);'>"+
							 "<td colspan='4' style='padding:3px 3px 3px 6px; border-top:0; color:rgba(0,0,0,0.8);'>"+
							  "<div id='sunLabel' class='upcoming-day-label'>Sunday"+
							   "<span style='padding-left:3px; padding-right:3px;'>&#8729;</span>"+
							   labels[6]+getDateSuperscript(labels[6])+
							  "</div>"+
							 "</td>"+
							"</tr>";

		for(var sund in sunRow) {
			htmlStr = htmlStr + sunRow[sund];
		}

		if(sunRow.length == 0) {
			htmlStr = htmlStr + "<tr>"+
								 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'>"+
								  "<span style='font-size:.8em; color:rgb(150,150,150); padding-left:7px;'>"+
							  	   "No events"+
							  	  "</span>"+
							  	 "</td>"+
							  	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	 "<td style='border-bottom:1px solid rgba(192,192,192,0.5);'></td>"+
						  	 	"</tr>";

		}

		htmlStr = htmlStr + "<tr>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	 "<td style='border-top:0;'></td>"+
						  	"</tr>";

		htmlStr = htmlStr + "</tbody></table>";

		document.getElementById("upcomingEventsDiv").innerHTML = htmlStr;
		document.getElementById("weekEvents").disabled = false;
        document.getElementById("prevWeekEvents").disabled = false;
        document.getElementById("nextWeekEvents").disabled = false;
        document.getElementById("prevWeekEventsMobile").disabled = false;
        document.getElementById("nextWeekEventsMobile").disabled = false;
	});
}
*/

function toggleManageReoccurring(reoccur) {
	if(reoccur == true) {
		document.getElementById("one-time-events-div").style.display = "none";
		document.getElementById("weekly-events-div").style.display = "block";
		document.getElementById("btnManageOneTime").className = "btn btn-default";
		document.getElementById("btnManageReoccurring").className = "btn btn-default active";
	}
	else {
		document.getElementById("one-time-events-div").style.display = "block";
		document.getElementById("weekly-events-div").style.display = "none";
		document.getElementById("btnManageOneTime").className = "btn btn-default active";
		document.getElementById("btnManageReoccurring").className = "btn btn-default";
	}
}

function editEvent(e, eventId, current) {
	e.preventDefault();

	var parameters = {
		eventId: eventId
	};

	if(current) {
		document.getElementById("btnSaveUpcomingEvent").style.display = "none";
		document.getElementById("edit-upcoming-event-start-date").disabled = true;
		document.getElementById("edit-upcoming-event-end-date").disabled = true;
		document.getElementById("edit-upcoming-event-start-time").disabled = true;
		document.getElementById("edit-upcoming-event-end-time").disabled = true;
		document.getElementById("edit-upcoming-event-details").disabled = true;
		document.getElementById("addUpcomingEventSpecialTogglePlus").style.display = "none";
	}
	else {
		document.getElementById("btnSaveUpcomingEvent").style.display = "inline-block";
		document.getElementById("edit-upcoming-event-start-date").disabled = false;
		document.getElementById("edit-upcoming-event-end-date").disabled = false;
		document.getElementById("edit-upcoming-event-start-time").disabled = false;
		document.getElementById("edit-upcoming-event-end-time").disabled = false;
		document.getElementById("edit-upcoming-event-details").disabled = false;
		if(document.getElementById("addUpcomingEventSpecialToggleMinus").style.display == "none")
			document.getElementById("addUpcomingEventSpecialTogglePlus").style.display = "inline-block";
	}

	$.get('/loadevent', parameters, function(data) {
		clearEditUpcomingEventModal();
		document.getElementById("edit-upcoming-event-ids").value = data.event_ids;
		document.getElementById("edit-upcoming-event-days").value = data.days;
		document.getElementById("edit-upcoming-event-title").value = data.title;
		document.getElementById("edit-upcoming-event-title-hidden").value = data.title;
		document.getElementById("edit-upcoming-event-details").value = data.details;
		document.getElementById("edit-upcoming-event-origDetails").value = data.details;

		/* remove add special button if limit is reached */
		if(data.specials.length >= data.eventSpecialLimit)
			document.getElementById("addUpcomingEventSpecialTogglePlus").style.display = "none";

		//var startTime = getISOString(new Date(data.start_time));
		//var endTime = getISOString(new Date(data.end_time));

		var startTime = new Date(data.start_time);
		var endTime = new Date(data.end_time);

		startTime = getISOString(startTime);
		endTime = getISOString(endTime);

		if(data.day == -1) {
			// start/end datetime or time
			document.getElementById("edit-upcoming-event-start-date").value = startTime.substring(0, startTime.indexOf("T"));
			document.getElementById("edit-upcoming-event-end-date").value = endTime.substring(0, endTime.indexOf("T"));
			startTime = startTime.substring(startTime.indexOf("T")+1);
			endTime = endTime.substring(endTime.indexOf("T")+1);
			document.getElementById("edit-upcoming-event-start-time").value = startTime;
			document.getElementById("edit-upcoming-event-end-time").value = endTime;
			document.getElementById("start-date-div").style.display = "block";
			document.getElementById("end-date-div").style.display = "block";

			//document.getElementById("title-div").className = "col col-md-12 col-sm-12 col-xs-12";
			//document.getElementById("start-time-div").className = "col col-md-12 col-sm-6 col-xs-6";
			//document.getElementById("end-time-div").className = "col col-md-12 col-sm-6 col-xs-6";

			// day
			document.getElementById("edit-upcoming-day-div").style.display = "none"
		}
		else {
			// start/end datetime or time
			startTime = startTime.substring(startTime.indexOf("T")+1);
			endTime = endTime.substring(endTime.indexOf("T")+1);
			document.getElementById("edit-upcoming-event-start-time").value = startTime;
			document.getElementById("edit-upcoming-event-end-time").value = endTime;
			document.getElementById("start-date-div").style.display = "none";
			document.getElementById("end-date-div").style.display = "none";

			//document.getElementById("title-div").className = "col col-md-4 col-sm-12 col-xs-12";
			//document.getElementById("start-time-div").className = "col col-md-4 col-sm-6 col-xs-6";
			//document.getElementById("end-time-div").className = "col col-md-4 col-sm-6 col-xs-6";

			// days
			var dayArr = data.days.split(",");
			for(var i = 0; i < 7; i++) {
				var selected = false;
				for(var d in dayArr) {
					if(dayArr[d] == i) {
						selected = true;
						selectEditUpcomingEventDay(intDayToStringAbbr(dayArr[d]).toLowerCase(), true);
						break;
					}
				}
				if(!selected) {
					selectEditUpcomingEventDay(intDayToStringAbbr(i).toLowerCase(), false, true);
				}
			}
			document.getElementById("edit-upcoming-day-div").style.display = "block";
		}

		var html = "";

		for(var s in data.specials) {
			var special = data.specials[s];

			if(data.day == -1) {
				var specialDates = "";
				var specialDatesArr = special.special_dates.split(",");

				var daysBetween = 0;
				var startDate = new Date(data.start_time);
				var endDate = new Date(data.end_time);

				while(startDate <= endDate) {
					daysBetween += 1;
					startDate.setDate(startDate.getDate()+1);
				}

				if(daysBetween == specialDatesArr.length && specialDatesArr.length != 1) {
					specialDates = "All days";
				}
				else if(daysBetween == specialDatesArr.length && specialDatesArr.length == 1) {
					specialDates = "";
				}
				else {
					for(var sd = 0; sd < specialDatesArr.length; sd++) {
						var sdDate = new Date(specialDatesArr[sd]);
						sdDate.addHoursDST();
						specialDates = specialDates + (sdDate.getMonth()+1) + "/" + sdDate.getDate();
						if(sd < specialDatesArr.length-1)
							specialDates = specialDates + ", ";
					}
				}

				var sStart = new Date(special.start_time);
				sStart.addHoursDST();
				sStart = formatStartEndTime(sStart);
				var sEnd = new Date(special.end_time);
				sEnd.addHoursDST();
				sEnd = formatStartEndTime(sEnd);

				html = html + "<div class='row' id='upcomingEventSpecialRow"+special.special_id+"' style='margin-left:0px; margin-right:0px;'>"+
					     	   "<div class='col col-md-5 col-sm-5 col-xs-5' style='line-height:1.5;'>"+
					     		"<img src='assets/img/"+getDealTypeImageByRefId(special.deal_type_ref_id)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;' />"+
					     		 "<p style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'>";

				if(special.price_type_ref_id == 300) {
					html = html + "$" + special.price.toFixed(2) + " " + special.details;
				}
				else if(special.price_type_ref_id == 301) {
					html = html + special.price.toFixed(0) + "% Off " + special.details;
				}
				else if(special.price_type_ref_id == 302) {
					html = html + "BOGO " + special.details;
				}
				else if(special.price_type_ref_id == 307) {
					html = html + "Free " + special.details;
				}

				html = html + "</p>"+
							 "</div>"+
							 "<div class='col col-md-2 col-sm-2 col-xs-2' style='line-height:1.5;'>"+
					    	  "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+specialDates+"</p>"+
					     	 "</div>"+
					     	 "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5;'>"+
					    	  "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>" + sStart + " - " + sEnd + "</p>"+
					     	 "</div>"+
							 "<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
							  "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
							   "<span style='cursor:pointer'>"+
							    "<a onclick='deleteSpecialFromEvent("+special.special_id+")' data-toggle='tooltip' title='Delete Special'>"+
							     "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
							    "</a>"+
							   "</span>"+
							  "</p>"+
						     "</div>"+
						    "</div>";
			}
			else {
				html = html+"<div class='row' id='upcomingEventSpecialRow"+special.special_id+"' style='margin-left:0px; margin-right:0px;'>"+
			     	   			"<div class='col col-md-12' style='line-height:1.5;'>"+
								 "<img src='assets/img/"+getDealTypeImageByRefId(special.deal_type_ref_id)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
								  "<p style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'>";

								 	if(special.price_type_ref_id == 300) {
										html = html + "$" + special.price.toFixed(2) + " " + special.details;
									}
									else if(special.price_type_ref_id == 301) {
										html = html + special.price.toFixed(0) + "% Off " + special.details;
									}
									else if(special.price_type_ref_id == 302) {
										html = html + "BOGO " + special.details;
									}
									else if(special.price_type_ref_id == 307) {
										html = html + "Free " + special.details;
									}

				html = html+"</p>"+
								 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
								  "<span style='cursor:pointer'>"+
								   "<a id='deleteUpcomingEventSpecial"+special.special_id+"' onclick='deleteSpecialFromEvent("+special.special_id+")' data-toggle='tooltip' title='Delete'>"+
								    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
								   "</a>"+
								  "</span>"+
								 "</p>"+
								"</div>"+
							   "</div>";
			}

		}

		$("#upcomingEventSpecialsDiv").html(html);

		$("#edit-upcoming-event-modal").modal("show");
	});
}

function saveUpcomingEvent(e) {
	e.preventDefault();
	if(validateEditUpcomingEvent()) {
		var	div = document.getElementById("edit-upcoming-day-div");
		if(div.style.display == "none") {
			document.getElementById("editUpcomingEventForm").submit();
		}
		else {
			var mon = document.getElementById("edit-upcoming-event-day-mon").value;
			var tue = document.getElementById("edit-upcoming-event-day-tue").value;
			var wed = document.getElementById("edit-upcoming-event-day-wed").value;
			var thu = document.getElementById("edit-upcoming-event-day-thu").value;
			var fri = document.getElementById("edit-upcoming-event-day-fri").value;
			var sat = document.getElementById("edit-upcoming-event-day-sat").value;
			var sun = document.getElementById("edit-upcoming-event-day-sun").value;

			var eventCount = 0;
			if(mon == "true") { eventCount++; }
			if(tue == "true") { eventCount++; }
			if(wed == "true") { eventCount++; }
			if(thu == "true") { eventCount++; }
			if(fri == "true") { eventCount++; }
			if(sat == "true") { eventCount++; }
			if(sun == "true") { eventCount++; }

			var eventIds = document.getElementById("edit-upcoming-event-ids").value;

			var parameters = {
				eventCount: eventCount,
				eventIds: eventIds
			};

			$.get('/validateevent', parameters, function(data) {
				if(data) {
					document.getElementById("editUpcomingEventForm").submit();
				}
				else {
					document.getElementById("upcomingEventErrors").innerHTML = "<p id='edit-upcoming-event-limitError'>Adding this event will put you over your 3 Weekly events limit. Please delete a weekly event to add another.</p>";
				}
			});
		}
	}
}

function validateEditUpcomingEvent() {
	var valid = true;
	var msg = [];
	var cnt = 0;
	document.getElementById("upcomingEventErrors").innerHTML = "";

	var recurring = false;
	//var eventIds = document.getElementById("edit-upcoming-event-ids").value;
	//eventIds = eventIds.split(",");
	//if(eventIds.length > 1)
	//	recurring = true;

	var details = document.getElementById("edit-upcoming-event-details").value;
	var startTime = document.getElementById("edit-upcoming-event-start-time").value;
	var endTime = document.getElementById("edit-upcoming-event-end-time").value;

	var startDate = "";
	var endDate = "";
	var mon = "";
	var tue = "";
	var wed = "";
	var thu = "";
	var fri = "";
	var sat = "";
	var sund = "";

	var	div = document.getElementById("edit-upcoming-day-div");
	if(div.style.display == "none") {
		startDate = document.getElementById("edit-upcoming-event-start-date").value;
		endDate = document.getElementById("edit-upcoming-event-end-date").value;
	}
	else {
		recurring = true;
		mon = document.getElementById("edit-upcoming-event-day-mon").value;
		tue = document.getElementById("edit-upcoming-event-day-tue").value;
		wed = document.getElementById("edit-upcoming-event-day-wed").value;
		thu = document.getElementById("edit-upcoming-event-day-thu").value;
		fri = document.getElementById("edit-upcoming-event-day-fri").value;
		sat = document.getElementById("edit-upcoming-event-day-sat").value;
		sund = document.getElementById("edit-upcoming-event-day-sun").value;
	}

	// details
	if(details == "") {
		document.getElementById("edit-upcoming-event-details").style.backgroundColor = "#FFEAEA";
		document.getElementById("edit-upcoming-event-details").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='edit-upcoming-event-detailsError'>Make sure to add some details!</p>";
		valid = false;
	}
	else {
		document.getElementById("edit-upcoming-event-details").style.backgroundColor = "#fff";
		document.getElementById("edit-upcoming-event-details").style.border = "1px solid #ccc";
	}

	// start time
	if(startTime == "") {
		document.getElementById("edit-upcoming-event-start-time").style.backgroundColor = "#FFEAEA";
		document.getElementById("edit-upcoming-event-start-time").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='edit-upcoming-event-start-timeError'>Make sure to choose a valid start time!</p>";
		valid = false;
	}
	else {
		document.getElementById("edit-upcoming-event-start-time").style.backgroundColor = "#fff";
		document.getElementById("edit-upcoming-event-start-time").style.border = "1px solid #ccc";
	}

	// end time
	if(endTime == "") {
		document.getElementById("edit-upcoming-event-end-time").style.backgroundColor = "#FFEAEA";
		document.getElementById("edit-upcoming-event-end-time").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='edit-upcoming-event-end-timeError'>Make sure to choose a valid end time!</p>";
		valid = false;
	}
	else {
		document.getElementById("edit-upcoming-event-end-time").style.backgroundColor = "#fff";
		document.getElementById("edit-upcoming-event-end-time").style.border = "1px solid #ccc";
	}

	if(!recurring) {
		// start date
		if(startDate == "") {
			document.getElementById("edit-upcoming-event-start-date").style.backgroundColor = "#FFEAEA";
			document.getElementById("edit-upcoming-event-start-date").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='edit-upcoming-event-start-dateError'>Make sure to choose a valid date!</p>";
			valid = false;
		}
		else {
			document.getElementById("edit-upcoming-event-start-date").style.backgroundColor = "#fff";
			document.getElementById("edit-upcoming-event-start-date").style.border = "1px solid #ccc";
		}

		if(startDate != "" && endDate != "" && startTime != "" && endTime != "") {
			var start = getDateTime(startTime, startDate);
			var end = getDateTime(endTime, endDate);
			var now = newDate();

			if(start < now) {
				msg[cnt++] = "<p>Make sure the event starts in the future!</p>";
				valid = false;
			}
			if(end <= start) {
				msg[cnt++] = "<p>Make sure the end time is later than the start time!</p>";
				valid = false;
			}
		}
	}
	else {
		// day(s)
		if(mon == "false" && tue == "false" && wed == "false" && thu == "false" && fri == "false" && sat == "false" && sund == "false") {
			document.getElementById("editEvent-dayToggle").style.border = "1px solid #FF0000";
			msg[cnt++] = "<p id='eventDaysError'>Make sure to choose at least one day!</p>";
			valid = false;
		}
		else {
			document.getElementById("editEvent-dayToggle").style.border = "none";
		}

		if(startTime != "" && endTime != "") {
			var now = newDate();
			var start = getDateTime(startTime, now);
			var end = getDateTime(endTime, now);

			if(end <= start) {
				msg[cnt++] = "<p>Make sure the end time is later than the start time!</p>";
				valid = false;
			}
		}
	}

	if(!valid) {
		for(var m in msg) {
			document.getElementById("upcomingEventErrors").innerHTML = document.getElementById("upcomingEventErrors").innerHTML + msg[m];
		}
	}

	return valid;
}

function deleteEvent(e) {
	e.preventDefault();

	var recurring = false;

	var eventIds = document.getElementById("edit-upcoming-event-ids").value;
	if(eventIds.split(",").length > 1)
		recurring = true;

	var confirmText = "Are you sure you want to delete this event?";
	if(recurring)
		confirmText = confirmText + " This will delete all occurrences of this event."

	swal({
	  	title: "Are you sure?",
	  	text: confirmText,
	  	icon: "warning",
	  	dangerMode: true,
	  	buttons: {
		  	cancel: {
		    	text: "Cancel",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Yes, delete it!",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((willDelete) => {
	  	if (willDelete) {
	  		document.getElementById("editUpcomingEventForm").action = "/deleteevent";
	    	document.getElementById("editUpcomingEventForm").submit();
	  	} else {
	    	return false;
	  	}
	});
}

function addSpecialToUpcomingEvent() {
	var eventIds = document.getElementById("edit-upcoming-event-ids").value;
	eventIds = eventIds.split(",");

	var days = [];
	var daysStr = [];
	var content = document.getElementById("addUpcomingEventSpecial-dayToggle");
	var dayButtons = content.getElementsByTagName("button");
	for(var i = 0; i < dayButtons.length; i++) {
		if(dayButtons[i].className.indexOf("dayActive") > -1) {
			dayInput = document.getElementById(dayButtons[i].id+"-hidden");
			days.push(dayInput.value);
			daysStr.push(dayButtons[i].innerHTML);
		}
	}

	var startTime = document.getElementById("addUpcomingEventSpecial-startTime").value;
	var endTime = document.getElementById("addUpcomingEventSpecial-endTime").value;

	var price = document.getElementById("addUpcomingEventSpecial-price").value;
	var details = document.getElementById("addUpcomingEventSpecial-details").value;

	var parameters = {
		eventId: eventIds[0],
		days: days,
		startTime: startTime,
		endTime: endTime,
		priceType: addUpcomingEventSpecialPriceTypeId,
		price: price,
		details: details,
		specialType: addUpcomingEventSpecialDealTypeId
	};

	if(validateAddSpecialToUpcomingEvent(parameters)) {
		$.get('/addspecialtotodaysevent', parameters, function(data) {
			if(data) {
				var htmlRow = "";
				var specialDays = "";

				if(daysStr.length != dayButtons.length) {
					for(var d = 0; d < daysStr.length; d++) {
						specialDays = specialDays + daysStr[d];
						if(d < daysStr.length-1)
							specialDays = specialDays + ", ";
					}
				}
				else if(daysStr.length == dayButtons.length && dayButtons.length > 1) {
					specialDays = "All days";
				}
				else {
					specialDays = "";
				}

				if(data.specialIds) {
					htmlRow = htmlRow+"<div class='row' id='upcomingEventSpecialRow"+data.specialIds[0]+"' style='margin-left:0px; margin-right:0px;'>"+
				     	   			"<div class='col col-md-5 col-sm-5 col-xs-5' style='line-height:1.5;'>"+
									 "<img src='assets/img/"+getDealTypeImageByRefId(addUpcomingEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
									 "<p style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'>";

									 if(addUpcomingEventSpecialPriceTypeId == 300)
									 	htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
									 else if(addUpcomingEventSpecialPriceTypeId == 301)
									 	htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
									 else if(addUpcomingEventSpecialPriceTypeId == 302)
									 	htmlRow = htmlRow+"BOGO "+details;
									 else if(addUpcomingEventSpecialPriceTypeId == 307)
									 	htmlRow = htmlRow+"Free "+details;

					htmlRow = htmlRow+"</p>"+
									"</div>"+
									"<div class='col col-md-2 col-sm-2 col-xs-2' style='line-height:1.5;'>"+
						    	     "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+specialDays+"</p>"+
						     	    "</div>"+
						     	    "<div class='col col-md-3 col-sm-3 col-xs-3' style='line-height:1.5;'>"+
				    	    		 "<p style='display:inline; vertical-align:middle; font-size:14px; color:#555; margin-right:9px;'>"+formatStartEndTime(new Date("01/01/2020 " + startTime)) + " - " + formatStartEndTime(new Date("01/01/2020 " + endTime))+"</p>"+
				     	   			"</div>"+
									"<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
									 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
									  "<span style='cursor:pointer'>"+
									   "<a id='deleteUpcomingEventSpecial"+data.specialIds[0]+"' onclick='deleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
									    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
									   "</a>"+
									  "</span>"+
									 "</p>"+
									"</div>"+
								   "</div>";
				}
				else {
					htmlRow = htmlRow+"<div class='row' id='upcomingEventSpecialRow"+data.specialId+"' style='margin-left:0px; margin-right:0px;'>"+
				     	   			"<div class='col col-md-12' style='line-height:1.5;'>"+
									 "<img src='assets/img/"+getDealTypeImageByRefId(addUpcomingEventSpecialDealTypeId)+"' height='26px' width='26px' style='margin-left:5px; margin-right:9px; display:inline;'>"+
									  "<p style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'>";

									 if(addUpcomingEventSpecialPriceTypeId == 300)
									 	htmlRow = htmlRow+"$"+parseFloat(price).toFixed(2)+" "+details;
									 else if(addUpcomingEventSpecialPriceTypeId == 301)
									 	htmlRow = htmlRow+parseInt(price).toFixed(0)+"% Off "+details
									 else if(addUpcomingEventSpecialPriceTypeId == 302)
									 	htmlRow = htmlRow+"BOGO "+details;
									 else if(addUpcomingEventSpecialPriceTypeId == 307)
									 	htmlRow = htmlRow+"Free "+details;

					htmlRow = htmlRow+"</p>"+
									 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
									  "<span style='cursor:pointer'>"+
									   "<a id='deleteUpcomingEventSpecial"+data.specialId+"' onclick='deleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
									    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
									   "</a>"+
									  "</span>"+
									 "</p>"+
									"</div>"+
								   "</div>";
				}

				document.getElementById("upcomingEventSpecialsDiv").innerHTML += htmlRow;
				
				var newRow = htmlRow;

				newRow = newRow.replace("style='margin-left:0px; margin-right:0px;'", "");
				newRow = newRow.replace("style='display:inline; vertical-align:middle; font-size:14px; color:rgb(50,50,50); font-weight:500; margin-right:9px;'", "style='display:inline; vertical-align:middle;'");

				var todayRow = newRow;

				if(data.specialIds) {
					newRow = newRow.replace("id='upcomingEventSpecialRow"+data.specialIds[0]+"'", "id='addedEventSpecialRow"+data.specialIds[0]+"'");
					newRow = newRow.replace("<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
											 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
											  "<span style='cursor:pointer'>"+
											   "<a id='deleteUpcomingEventSpecial"+data.specialIds[0]+"' onclick='deleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
											    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
											   "</a>"+
											  "</span>"+
											 "</p>"+
											"</div>", "");
					todayRow = todayRow.replace("id='upcomingEventSpecialRow"+data.specialIds[0]+"'", "id='addedTodayEventSpecialRow"+data.specialIds[0]+"'");
					todayRow = todayRow.replace("<div class='col col-md-1 col-sm-1 col-xs-1' style='line-height:1.5;'>"+
											 "<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
											  "<span style='cursor:pointer'>"+
											   "<a id='deleteUpcomingEventSpecial"+data.specialIds[0]+"' onclick='deleteSpecialFromEvent("+data.specialIds[0]+")' data-toggle='tooltip' title='Delete'>"+
											    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
											   "</a>"+
											  "</span>"+
											 "</p>"+
											"</div>", "");
				}
				else {
					newRow = newRow.replace("id='upcomingEventSpecialRow"+data.specialId+"'", "id='addedEventSpecialRow"+data.specialId+"'");
					newRow = newRow.replace("<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
											  "<span style='cursor:pointer'>"+
											   "<a id='deleteUpcomingEventSpecial"+data.specialId+"' onclick='deleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
											    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
											   "</a>"+
											  "</span>"+
											 "</p>", "");
					todayRow = todayRow.replace("id='upcomingEventSpecialRow"+data.specialId+"'", "id='addedTodayEventSpecialRow"+data.specialId+"'");
					todayRow = todayRow.replace("<p style='display:inline; vertical-align:middle; font-size:14px;'>"+
											  "<span style='cursor:pointer'>"+
											   "<a id='deleteUpcomingEventSpecial"+data.specialId+"' onclick='deleteSpecialFromEvent("+data.specialId+")' data-toggle='tooltip' title='Delete'>"+
											    "<i class='fa fa-trash fa-sm' style='color:rgba(255,0,0,0.6)'> </i>"+
											   "</a>"+
											  "</span>"+
											 "</p>", "");
				}

				for(var i = 0; i < eventIds.length; i++) {
					var el = "manageEventsAddedSpecials"+eventIds[i];
					var el2 = "eventsAddedSpecials"+eventIds[i];
					var noSpecRow = "manageEventsNoSpecialsRow"+eventIds[i];

					if(document.getElementById(el))
						document.getElementById(el).innerHTML += newRow;

					if(document.getElementById(el2))
						document.getElementById(el2).innerHTML += todayRow;

					if(document.getElementById(noSpecRow))
						document.getElementById(noSpecRow).style.display = "none";
				}

				toggleAddUpcomingEventSpecialPriceType('money');
				document.getElementById("addUpcomingEventSpecial-details").value = "";
				chooseAddUpcomingEventSpecialDealType(-1);
				toggleAddUpcomingEventSpecialDiv();

				//document.getElementById("todaysEventsNoSpecialsRow"+eventId).style.display = "none";
				//document.getElementById("editTodaysEventsNoSpecialsRow"+eventId).style.display = "none";


				/* remove add special button if limit is reached */
				var limit = parseInt(document.getElementById("eventSpecialsLimit").value);
				var usd = document.getElementById("upcomingEventSpecialsDiv").innerHTML;
				var specialCount = 0;

				if(usd != "") 
					specialCount = (usd.match(/upcomingEventSpecialRow/g) || []).length;

				if(specialCount + 1 >= limit)
					document.getElementById("addUpcomingEventSpecialTogglePlus").style.display = "none";
			}
		});
	}
}

function validateAddSpecialToUpcomingEvent(p) {
	var valid = true;
	var msg = [];
	var cnt = 0;
	document.getElementById("addSpecialToUpcomingEventErrors").innerHTML = "";

	// price
	if(p.price == "" && (p.priceType == 300 || p.priceType == 301)) {
		document.getElementById("addUpcomingEventSpecial-price").style.backgroundColor = "#FFEAEA";
		document.getElementById("addUpcomingEventSpecial-price").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='addUpcomingEventSpecial-priceError'>Make sure you add a price!</p>";
		valid = false;
	}
	else {
		document.getElementById("addUpcomingEventSpecial-price").style.backgroundColor = "#fff";
		document.getElementById("addUpcomingEventSpecial-price").style.border = "1px solid #ccc";
	}

	// details
	if(p.details == "") {
		document.getElementById("addUpcomingEventSpecial-details").style.backgroundColor = "#FFEAEA";
		document.getElementById("addUpcomingEventSpecial-details").style.border = "1px solid #FF0000";
		msg[cnt++] = "<p id='addUpcomingEventSpecial-detailsError'>Make sure you add some details!</p>";
		valid = false;
	}
	else {
		document.getElementById("addUpcomingEventSpecial-details").style.backgroundColor = "#fff";
		document.getElementById("addUpcomingEventSpecial-details").style.border = "1px solid #ccc";
	}

	// icon
	if(p.specialType == "") {
		for(var i = 0; i <= 15; i++) {
			document.getElementById("addUpcomingEventSpecial-deal-type-"+i).style.border = "1px solid #FF0000";
		}
		msg[cnt++] = "<p id='addUpcomingEventSpecial-dealTypeError'>Make sure you choose an icon!</p>";
		valid = false;
	}
	else {
		for(var i = 0; i <= 15; i++) {
			document.getElementById("addUpcomingEventSpecial-deal-type-"+i).style.border = "1px solid #c0c0c0";
		}
	}

	if(!valid) {
		for(var m in msg) {
			document.getElementById("addSpecialToUpcomingEventErrors").innerHTML = document.getElementById("addSpecialToUpcomingEventErrors").innerHTML + msg[m];
		}
	}

	return valid;
}

function clearEditUpcomingEventModal() {
	document.getElementById("edit-upcoming-event-title").value = "";
	document.getElementById("edit-upcoming-event-details").value = "";
	document.getElementById("edit-upcoming-event-origDetails").value = "";
	document.getElementById("edit-upcoming-event-start-date").value = "";
	//document.getElementById("edit-upcoming-event-start-datetime").value = "";
	//document.getElementById("edit-upcoming-event-end-datetime").value = "";
	document.getElementById("edit-upcoming-event-start-time").value = "";
	document.getElementById("edit-upcoming-event-end-time").value = "";
	for(var i = 0; i < 7; i++) {
		selectEditUpcomingEventDay(intDayToStringAbbr(i).toLowerCase(), false, true);
	}
}

function selectEditUpcomingEventDay(day, select, deselect) {
	if(select) {
		document.getElementById("edit-upcoming-event-"+day+"ToggleButton").className = "btn btn-default active";
		document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.backgroundColor = "#e6e6e6";
		document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.borderColor = "#adadad";
		document.getElementById("edit-upcoming-event-day-"+day).value = true;
	}
	else if(deselect) {
		document.getElementById("edit-upcoming-event-"+day+"ToggleButton").className = "btn btn-default";
		document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.backgroundColor = "white";
		document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.borderColor = "#ccc";
		document.getElementById("edit-upcoming-event-day-"+day).value = false;
	}
	else {
		if(document.getElementById("edit-upcoming-event-"+day+"ToggleButton").className == "btn btn-default") {
			document.getElementById("edit-upcoming-event-"+day+"ToggleButton").className = "btn btn-default active";
			document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.backgroundColor = "#e6e6e6";
			document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.borderColor = "#adadad";
			document.getElementById("edit-upcoming-event-day-"+day).value = true;
		} else {
			document.getElementById("edit-upcoming-event-"+day+"ToggleButton").className = "btn btn-default";
			document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.backgroundColor = "white";
			document.getElementById("edit-upcoming-event-"+day+"ToggleButton").style.borderColor = "#ccc";
			document.getElementById("edit-upcoming-event-day-"+day).value = false;
		}
	}

	document.getElementById("editEvent-dayToggle").style.border = "none";
	if(document.getElementById("eventDaysError")) {
		document.getElementById("eventDaysError").style.display = "none";
	}
}

function toggleAddUpcomingEventSpecialDiv() {
	var display = document.getElementById("addUpcomingEventSpecialRow").style.display;

	// show
	if(display == "none" || display == "") {
		document.getElementById("addUpcomingEventSpecialRow").style.display = "block";
		document.getElementById("addUpcomingEventSpecialToggleMinus").style.display = "inline";
		document.getElementById("addUpcomingEventSpecialTogglePlus").style.display = "none";

		var startDate = document.getElementById("edit-upcoming-event-start-date").value;
		var endDate = document.getElementById("edit-upcoming-event-end-date").value;

		if(startDate != null && startDate != "" && endDate != null && endDate != "") {
			var dayToggles = '';
			startDate = new Date(startDate);
			endDate = new Date(endDate);

			startDate.addHoursDST();
			endDate.addHoursDST();

			while(startDate <= endDate) {
				var id = ""+(startDate.getMonth()+1)+startDate.getDate();
				dayToggles = dayToggles + 
						 '<div class="btn-group">'+
					      '<button id="day-'+id+'" type="button" style="text-decoration: none" class="btn btn-default" onclick="toggleAddUpcomingEventSpecialDay('+id+')">'+(startDate.getMonth()+1)+'/'+startDate.getDate()+'</button>'+
					     '</div>'+
					     '<input id="day-'+id+'-hidden" type="hidden" value="'+startDate+'" />';
				startDate.setDate(startDate.getDate()+1);
			}

			document.getElementById("addUpcomingEventSpecial-dayToggle").innerHTML = dayToggles;
		}
		else {
			document.getElementById("multidayDiv").style.display = "none";
		}
	}
	// hide
	else {
		document.getElementById("addUpcomingEventSpecialRow").style.display = "none";
		document.getElementById("addUpcomingEventSpecialToggleMinus").style.display = "none";
		document.getElementById("addUpcomingEventSpecialTogglePlus").style.display = "inline";
	}
}

function toggleAddUpcomingEventSpecialDay(dt) {
	if(document.getElementById("day-"+dt).className == "btn btn-default") {
		document.getElementById("day-"+dt).className = "btn btn-default dayActive";
		document.getElementById("day-"+dt).style.backgroundColor = "#e6e6e6";
		document.getElementById("day-"+dt).style.borderColor = "#adadad";
	} 
	else {
		document.getElementById("day-"+dt).className = "btn btn-default";
		document.getElementById("day-"+dt).style.backgroundColor = "white";
		document.getElementById("day-"+dt).style.borderColor = "#ccc";
	}
}

function selectAllDays(yes) {
	var startDate = document.getElementById("edit-upcoming-event-start-date").value;
	var endDate = document.getElementById("edit-upcoming-event-end-date").value;

	startDate = new Date(startDate);
	endDate = new Date(endDate);

	startDate.addHoursDST();
	endDate.addHoursDST();

	if(yes) {
		while(startDate <= endDate) {
			var id = ""+(startDate.getMonth()+1)+startDate.getDate();
			document.getElementById("day-"+id).className = "btn btn-default dayActive";
			document.getElementById("day-"+id).style.backgroundColor = "#e6e6e6";
			document.getElementById("day-"+id).style.borderColor = "#adadad";
			startDate.setDate(startDate.getDate()+1);
		}
	} 
	else {
		while(startDate <= endDate) {
			var id = ""+(startDate.getMonth()+1)+startDate.getDate();
			document.getElementById("day-"+id).className = "btn btn-default";
			document.getElementById("day-"+id).style.backgroundColor = "white";
			document.getElementById("day-"+id).style.borderColor = "#ccc";
			startDate.setDate(startDate.getDate()+1);
		}
	}
}

function toggleAddUpcomingEventSpecialPriceType(type) {
	document.getElementById("addUpcomingEventSpecial-price").value = "";
	document.getElementById("addUpcomingEventSpecial-price").className = "form-control";

	if (type == 'money') {
		addUpcomingEventSpecialPriceType = "$";
		addUpcomingEventSpecialPriceTypeId = 300;
		document.getElementById("addUpcomingEventSpecial-price").disabled = false;
		document.getElementById("addUpcomingEventSpecial-priceLabel").innerHTML = "Price";
		document.getElementById("addUpcomingEventSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-moneySignInPriceTextbox").style.display = "block";
		document.getElementById("addUpcomingEventSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-moneyToggleButton").className = "btn btn-default active";
		document.getElementById("addUpcomingEventSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-freeToggleButton").className = "btn btn-default";
	}
	else if (type == 'percent') {
		addUpcomingEventSpecialPriceType = "%";
		addUpcomingEventSpecialPriceTypeId = 301;
		document.getElementById("addUpcomingEventSpecial-price").disabled = false;
		document.getElementById("addUpcomingEventSpecial-priceLabel").innerHTML = "Percent Off";
		document.getElementById("addUpcomingEventSpecial-percentSignInPriceTextbox").style.display = "block";
		document.getElementById("addUpcomingEventSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-percentToggleButton").className = "btn btn-default active";
		document.getElementById("addUpcomingEventSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-freeToggleButton").className = "btn btn-default";
	} else if (type == 'bogo'){
		addUpcomingEventSpecialPriceType = "Buy One Get One";
		addUpcomingEventSpecialPriceTypeId = 302;
		document.getElementById("addUpcomingEventSpecial-price").disabled = true;
		document.getElementById("addUpcomingEventSpecial-priceLabel").innerHTML = "BOGO";
		document.getElementById("addUpcomingEventSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-bogoInPriceTextbox").style.display = "block";
		document.getElementById("addUpcomingEventSpecial-freeInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-bogoToggleButton").className = "btn btn-default active";
		document.getElementById("addUpcomingEventSpecial-freeToggleButton").className = "btn btn-default";
	} else if (type == 'free'){
		addUpcomingEventSpecialPriceType = "Free";
		addUpcomingEventSpecialPriceTypeId = 307;
		document.getElementById("addUpcomingEventSpecial-price").disabled = true;
		document.getElementById("addUpcomingEventSpecial-priceLabel").innerHTML = "Free";
		document.getElementById("addUpcomingEventSpecial-percentSignInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-moneySignInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-bogoInPriceTextbox").style.display = "none";
		document.getElementById("addUpcomingEventSpecial-freeInPriceTextbox").style.display = "block";
		document.getElementById("addUpcomingEventSpecial-moneyToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-percentToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-bogoToggleButton").className = "btn btn-default";
		document.getElementById("addUpcomingEventSpecial-freeToggleButton").className = "btn btn-default active";
	}

	document.getElementById("hiddenAddUpcomingEventSpecialPriceType").value = addUpcomingEventSpecialPriceTypeId;

	document.getElementById("addUpcomingEventSpecial-price").style.backgroundColor = "#fff";
	document.getElementById("addUpcomingEventSpecial-price").style.border = "1px solid #ccc";
	if(document.getElementById("addUpcomingEventSpecial-priceError")) {
		document.getElementById("addUpcomingEventSpecial-priceError").style.display = "none";
	}
}

function chooseAddUpcomingEventSpecialDealType(typeID) {
	isAddUcomingEventSpecialDealTypeSelected = false;
	addUpcomingEventSpecialDealTypeId = getDealTypeRefIdByOGId(typeID);
  	document.getElementById("hiddenAddUpcomingEventSpecialType").value = addUpcomingEventSpecialDealTypeId;

  	var dealTypeString = "addUpcomingEventSpecial-deal-type-";

  	for (var i = 0; i <= 15; i++) {
    	if (i == typeID) {
      		document.getElementById(dealTypeString+i).style.background = "rgba(255, 215, 0, 0.7)";
      		document.getElementById(dealTypeString+i).style.border = "2px solid orange";
      		document.getElementById(dealTypeString+i).value = addUpcomingEventSpecialDealTypeId;
      		isAddUpcomingEventSpecialDealTypeSelected = true;
    	} else {
      		document.getElementById(dealTypeString+i).style.background = "white";
      		document.getElementById(dealTypeString+i).style.border = "1px solid #c0c0c0";
      		document.getElementById(dealTypeString+i).value = 0;
    	}
  	}

  	if(document.getElementById("addUpcomingEventSpecial-dealTypeError")) {
  		document.getElementById("addUpcomingEventSpecial-dealTypeError").style.display = "none";
  	}
}

function daysToString(days) {
	var str = "";

	//sort day array 1, 2, 3, 4, 5, 6, 0
	if(days.length > 1) {
		var sorted = false;
		while(!sorted) {
			sorted = true;
			for(var d = 0; d < days.length - 1; d++) {
				var d1 = parseInt(days[d+1]);
				var d2 = parseInt(days[d]);
				if((d1 < d2 && d1 != 0) || d2 == 0) {
					var temp = days[d];
					days[d] = days[d+1];
					days[d+1] = temp;
					sorted = false;
				}
			}
		}
	}

	if(days.length == 1) {
		str = str + intDayToString(days[0]);
	}
	else if(days.length == 2) {
		str = str + intDayToString(days[0]) + " and " + intDayToString(days[1]);
	}
	else {
		for(var i in days) {
			if(i < days.length - 2) {
				str = str + intDayToString(days[i]) + ", ";
			}
			else if(i < days.length - 1) {
				str = str + intDayToString(days[i]) + ", and ";
			}
			else {
				str = str + intDayToString(days[i]);
			}
		}
	}

	return str;
}

function weekPickerEvents(e) {
    e.preventDefault();
    $('#weekEvents').datepicker('show');
}

function nextWeekEvents(e) {
    e.preventDefault();
    week = week + 1;

    //labels = getWeekLabels();
    //document.getElementById("weekEvents").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);

    var d = newDate();
    d.setDate(d.getDate() + (week * 7));
    var startDate;
    if(d.getDay() > 0)
    	startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay()+1);
    else
    	startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 6);

    $('#weekEvents').data({date: getYYYYMMDD(startDate)});
    $('#weekEvents').datepicker('update');
}

function prevWeekEvents(e) {
    e.preventDefault();
    week = week - 1;

	//labels = getWeekLabels();
    //document.getElementById("weekEvents").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);

    var d = newDate();
    d.setDate(d.getDate() + (week * 7));
    var startDate;
    if(d.getDay() > 0)
    	startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay()+1);
    else
    	startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 6);

    $('#weekEvents').data({date: getYYYYMMDD(startDate)});
    $('#weekEvents').datepicker('update');
}

function getWeekLabels() {
    var weekLabels = [];

    var addDays = 1;
    if(week != 0)
        addDays = week * 7 + 1;

    var curr = newDate();
    curr.setHours(0,0,0,0);

    var first = 0;
    if(curr.getDay() > 0)
    	first = curr.getDate() - curr.getDay() + addDays;
    else
    	first = curr.getDate() - 7 + addDays;

    for(var i = 0; i <= 6; i++) {
        var d = new Date(curr.setDate(first));
        var dStr = intMonthToStringAbbr(d.getMonth()) + " " + d.getDate();
        weekLabels.push(dStr);
        first = first + 1;
        curr = newDate();
        curr.setHours(0,0,0,0);
    }

    return weekLabels;
}



/* Shared */

function deleteSpecialFromEvent(specialId) {
	var parameters = {
		specialId: specialId
	};

	$.get('/deletespecialfromevent', parameters, function(data) {
		if(data && data.length > 0) {

			/* show add special button  */
			document.getElementById("addUpcomingEventSpecialTogglePlus").style.display = "inline";
			document.getElementById("addModalEventSpecialTogglePlus").style.display = "inline";

			for(var d in data) {
				var esr = document.getElementById("eventSpecialRow"+data[d]);
				if(esr != null && esr != undefined) {
					esr.style.display = "none";
				}
				var eesr = document.getElementById("editEventSpecialRow"+data[d]);
				if(eesr != null && eesr != undefined) {
					eesr.style.display = "none";
				}
				var uesr = document.getElementById("upcomingEventSpecialRow"+data[d]);
				if(uesr != null && uesr != undefined) {
					uesr.style.display = "none";
				}
				var maesr = document.getElementById("manageEventSpecialRow"+data[d]);
				if(maesr != null && maesr != undefined) {
					maesr.style.display = "none";
				}
				var mesr = document.getElementById("modalEventSpecialRow"+data[d]);
				if(mesr != null && mesr != undefined) {
					mesr.style.display = "none";
				}
				var aesr = document.getElementById("addedEventSpecialRow"+data[d]);
				if(aesr != null && aesr != undefined) {
					aesr.style.display = "none";
				}
				var atesr = document.getElementById("addedTodayEventSpecialRow"+data[d]);
				if(atesr != null && atesr != undefined) {
					atesr.style.display = "none";
				}
			}
		}
	});
}

function validateField(field) {
	if(field.value != "") {
		field.style.backgroundColor = "#fff";
		field.style.border = "1px solid #ccc";
		if(document.getElementById(field.id+"Error")) {
			document.getElementById(field.id+"Error").style.display = "none";
		}
	}
}




/* Common functions */

function formatStartEndTime(datetime){
	var h = datetime.getHours();
	var m = datetime.getMinutes();
	var ampm = "AM";

	if (h > 12) {
		h = h - 12;
		ampm = "PM";
	}
	else if (h == 12) {
		ampm = "PM";
	}
	else if (h == 0) {
		h = 12;
	}

	if (m < 10) {
		m = "0" + m;
	}

	return h + ":" + m + ampm;
}

function dateDiff(a) {
	var today = newDate();
  	var dealDate = new Date(a);

  	if (dealDate < today) {
    	dealDate.setDate(dealDate.getDate()+1);
    	a = dealDate.getTime();
  	}

	var diffMs = (a - today); // milliseconds between now & a
	var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
	var diffMins = Math.ceil(((diffMs % 86400000) % 3600000) / 60000); // minutes

	if (diffMins == 60) {
		diffHrs++;
		diffMins = 00;
	}

	if (diffHrs == 0) {
		return(diffMins + "m");
	} else {
		return(diffHrs + "h " + diffMins + "m");
	}
}

function intDayToString(day) {
    var weekdays = new Array(7);
    weekdays[0] = "Sunday";
    weekdays[1] = "Monday";
    weekdays[2] = "Tuesday";
    weekdays[3] = "Wednesday";
    weekdays[4] = "Thursday";
    weekdays[5] = "Friday";
    weekdays[6] = "Saturday";
    if (day >= 0 && day <= 6)
        return weekdays[day];
    else
        return "";
}

function intDayToStringAbbr(day) {
	var weekday = new Array(7);
	weekday[0] = "Sun";
	weekday[1] = "Mon";
	weekday[2] = "Tue";
	weekday[3] = "Wed";
	weekday[4] = "Thu";
	weekday[5] = "Fri";
	weekday[6] = "Sat";
	return weekday[day];
}

function getDateSuperscript(monthDay) {
    var day = parseInt(monthDay.substring(monthDay.indexOf(" ")+1));
    return getSuperscript(day);
}

function getSuperscript(day) {
    if(day == 1 || day == 21 || day == 31)
        return "st";
    else if(day == 2 || day == 22)
        return "nd";
    else if(day == 3 || day == 23)
        return "rd";
    else if((day >= 4 && day <= 20) || (day >= 24 && day <= 30))
        return "th";
    else
        return "";
}

function getDealTypeRefIdByOGId(id){
	if (id == 0) {
		return 200; //beer
	} else if (id == 1) {
		return 202; //shot
	} else if (id == 2) {
		return 206; //mixed drink
	} else if (id == 3) {
		return 208; //margarita
	} else if (id == 4) {
		return 210; //martini
	} else if (id == 5) {
		return 212; //tumbler
	} else if (id == 6) {
		return 204; //wine
	} else if (id == 7) {
		return 251; //burger
	} else if (id == 8) {
		return 253; //appetizer
	} else if (id == 9) {
		return 255; //pizza
	} else if (id == 10) {
		return 257; //taco
	} else if (id == 11) {
		return 259; //sushi
	} else if (id == 12) {
		return 261; //bowl
	} else if (id == 13) {
		return 263; //wing
	} else if (id == 14) {
		return 214; //bottle
	} else if (id == 15) {
		return 216; //can
	}

	return 200;
}

function getDealTypeImageByRefId(refId) {
	switch(refId) {
		case 200: return 'beer-icon.png';
		case 202: return 'shot-icon.png';
    	case 204: return 'wine-icon.png';
    	case 206: return 'mixed-drink-icon.png';
    	case 208: return 'margarita-icon.png';
    	case 210: return 'martini-icon.png';
    	case 212: return 'tumbler-icon.png';
    	case 214: return 'beerbottle.png';
    	case 216: return 'beercan.png';
		case 251: return 'burger-icon.png';
		case 253: return 'appetizer-icon.png';
		case 255: return 'pizza-icon.png';
    	case 257: return 'taco-icon.png';
    	case 259: return 'sushi.png';
    	case 261: return 'bowl.png';
    	case 263: return 'chickenwing.png';
		default: return '';
	}
}

function getISOString(dt) {
	var current_date = dt.getDate();
	var current_month = dt.getMonth() + 1;
	var current_year = dt.getFullYear();
	var current_hrs = dt.getHours();
	var current_mins = dt.getMinutes();
	var current_secs = dt.getSeconds();
	var current_datetime;

	// Add 0 before date, month, hrs, mins or secs if they are less than 0
	current_date = current_date < 10 ? '0' + current_date : current_date;
	current_month = current_month < 10 ? '0' + current_month : current_month;
	current_hrs = current_hrs < 10 ? '0' + current_hrs : current_hrs;
	current_mins = current_mins < 10 ? '0' + current_mins : current_mins;
	current_secs = current_secs < 10 ? '0' + current_secs : current_secs;

	// Current datetime
	// String such as 2016-07-16T19:20:30
	current_datetime = current_year + '-' + current_month + '-' + current_date + 'T' + current_hrs + ':' + current_mins + ':' + current_secs;
	return current_datetime;
}

function getDurationStr(startDate, endDate, start, end) {
	var duration = "";
	var dStartDate = "";
	var dStartTime = "";
	var dEndDate = "";
	var dEndTime = "";

	var sh = parseInt(start.substring(0, start.indexOf(":")));
	var sm = start.substring(start.indexOf(":")+1);
	var sampm = "AM";
	var eh = parseInt(end.substring(0, end.indexOf(":")));
	var em = end.substring(end.indexOf(":")+1);
	var eampm = "AM";

	if(sh > 12 && sh != 24) {
		sh = sh - 12;
		sampm = "PM";
	}
	else if(sh == 12) {
		sampm = "PM";
	}
	else if(sh == 24) {
		sh = sh - 12;
	}
	else if(sh == 0) {
		sh = 12;
	}

	if(eh > 12 && eh != 24) {
		eh = eh - 12;
		eampm = "PM";
	}
	else if(eh == 12) {
		eampm = "PM";
	}
	else if(eh == 24) {
		eh = eh - 12;
		eampm = "AM";
	}
	else if(eh == 0) {
		eh = 12;
	}

	if(startDate != null && endDate != null) {
		startDate = new Date(startDate);
		endDate = new Date(endDate);

		startDate.addHoursDST();
		endDate.addHoursDST();

		var now = newDate();
		if(now.getHours() < 4)
			now.setDate(now.getDate()-1);
		var tmw = new Date(now);
		tmw.setDate(tmw.getDate()+1);

		if(startDate.getDate() == now.getDate() && startDate.getMonth() == now.getMonth() && startDate.getYear() == now.getYear()) {
			dStartDate = "Today";
		}
		else {
			dStartDate = intDayToStringAbbr(startDate.getDay()) + ", " + intMonthToStringAbbr(startDate.getMonth()) + " " + startDate.getDate() + "<sup style='font-size:0.5em; top:-0.7em;'>" + getSuperscript(startDate.getDate()) + "</sup>";
		}

		if(endDate.getDate() == now.getDate() && endDate.getMonth() == now.getMonth() && endDate.getYear() == now.getYear()) {
			dEndDate = "Today";
		}
		else {
			dEndDate = intDayToStringAbbr(endDate.getDay()) + ", " + intMonthToStringAbbr(endDate.getMonth()) + " " + endDate.getDate() + "<sup style='font-size:0.5em; top:-0.7em;'>" + getSuperscript(endDate.getDate()) + "</sup>";
		}

		duration = dStartDate + "<span style='color:black; padding-left:1px; padding-right:1px;'>&#8729;</span>" + sh + ":" + sm + sampm;
		duration = duration + " - " + dEndDate + "<span style='color:black; padding-left:1px; padding-right:1px;'>&#8729;</span>" + eh + ":" + em + eampm;
		//duration = duration + "<span style='color:black; padding-left:1px; padding-right:1px;'>&#8729;</span>" + sh + ":" + sm + sampm;
	}
	else {
		duration = sh + ":" + sm + sampm + " - " + eh + ":" + em + eampm;
	}

	return duration;
}

function getDurationStrReccur(days, start, end) {
	var duration = "";

	var sh = parseInt(start.substring(0, start.indexOf(":")));
	var sm = start.substring(start.indexOf(":")+1);
	var eh = parseInt(end.substring(0, end.indexOf(":")));
	var em = end.substring(end.indexOf(":")+1);
	var sampm = "AM";
	var eampm = "AM";

	if(sh > 12 && sh != 24) {
		sh = sh - 12;
		sampm = "PM";
	}
	else if(sh == 12) {
		sampm = "PM";
	}
	else if(sh == 24) {
		sh = sh - 12;
		sapmp = "AM";
	}

	if(eh > 12 && eh != 24) {
		eh = eh - 12;
		eampm = "PM";
	}
	else if(eh == 12) {
		eampm = "PM";
	}
	else if(eh == 24) {
		eh = eh - 12;
		eampm = "AM";
	}

	duration = sh + ":" + sm + sampm + " - " + eh + ":" + em + eampm;

	return duration;
}

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
    return year + "-" + month + "-" + day;
}

function getDateTime(start, startDate) {
	var startH = parseInt(start.substring(0, start.indexOf(":")));
	var startM = parseInt(start.substring(start.indexOf(":")+1));

	var s = new Date(startDate);
	s.setHours(s.getHours() + startH);
	s.setMinutes(s.getMinutes() + startM);

	if(startH < 4) {
		s.setDate(s.getDate() + 1);
	}

	return s;
}

function daysBetween(a, b) {
  	var aa = new Date(a);
  	var bb = new Date(b);

	var diffMs = Math.abs(bb - aa); 
	var diffDays = Math.floor(diffMs / (1000 * 60 * 60 * 24)); 

	return diffDays;
}
