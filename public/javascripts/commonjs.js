jQuery(document).ready(removeLoading);

var images = ['assets/img/pizza-icon.png', 
			  'assets/img/burger-icon.png', 
			  'assets/img/mixed-drink-icon.png', 
			  'assets/img/tumbler-icon.png',
			  'assets/img/beer-icon.png', 
			  'assets/img/shot-icon.png', 
			  'assets/img/martini-icon.png', 
			  'assets/img/margarita-icon.png',
			  'assets/img/wine-icon.png', 
			  'assets/img/appetizer-icon.png', 
			  'assets/img/taco-icon.png',
			  'assets/img/sushi.png',
			  'assets/img/bowl.png',
			  'assets/img/chickenwing.png',
			  'assets/img/beerbottle.png',
			  'assets/img/beercan.png'];

$('#loader').css({'background-image': 'url(' + images[Math.floor(Math.random() * images.length)] + ')'});

var loadingCount = 600;

function removeLoading() {
	if(document.getElementById("loader")) {
		if(loadingCount-- > 0) {
		    //if($('#specialsAnalyticsTable').is(':visible') || //analytics
		    //   $('#specialsAnalyticsTableMobile').is(':visible') || //analytics mobile
		    if($('#analytics-overview').is(':visible') || //analytics
		       $('#todaysSpecialsForm').is(':visible') || //specials
		       $('#todaysEventsForm').is(':visible') || //events
		       $('#displayEstablishmentInfo').is(':visible') || //info
		       $('#settingsHeader').is(':visible') || //settings
		       $('#partnersHeader').is(':visible') || //partners
		       $('#upgradeHeader').is(':visible')) { //upgrade
		        document.getElementById("loader").style.display = "none";
		    	document.getElementById("loaderText").style.display = "none";
		    } else {
		        setTimeout(removeLoading, 50); //wait 50 ms, then try again
		    }
		}
		else {
			// timeout
			document.getElementById("loader").style.display = "none";
		    document.getElementById("loaderText").style.display = "none";
		}
	}
}

function newDate() {
	var t = new Date();
	var tz = t.getTimezoneOffset() / 60;
	t.setHours(t.getHours() - tz);
	return t;
}

Date.prototype.addHoursDST = function () {
	var t = new Date();
	var tz = t.getTimezoneOffset() / 60;
	this.setHours(this.getHours() + tz);
}

Date.prototype.subtractHoursDST = function () {
	var t = new Date();
	var tz = t.getTimezoneOffset() / 60;
	this.setHours(this.getHours() - tz);
}



// LOAD TERMS OF SERVICE

function showTerms(e, planId, trial) {
	e.preventDefault();
	document.getElementById("planId-hidden").innerHTML = planId;
	document.getElementById("trial-hidden").innerHTML = trial;

	if(planId == "plan_GoYcTsLVOjFmgw") {
		document.getElementById("terms-iframe").src = "https://docs.google.com/gview?url=https://app.appyhourmobile.com/terms/SupportLocalTermsofService.docx&embedded=true";
	}
	else if(planId == "price_1IYYDNJjoHPs2g2pYdsw6kXB") {
		document.getElementById("terms-iframe").src = "https://docs.google.com/gview?url=https://app.appyhourmobile.com/terms/LiteTermsofService.docx&embedded=true";
	}

	$("#terms-and-conditions").modal("show");	
}

var timerId;
var timerCount = 5;

if(document.getElementById("terms-iframe")) {
	timerId = setInterval("reloadIFrame();", 2000);
}

function reloadIFrame() {
	if(timerCount-- > 0) {
		var iframe = document.getElementById("terms-iframe");
	  	if(iframe && iframe.contentDocument.URL == "about:blank"){
	    	iframe.src =  iframe.src;
	  	}
  	}
  	else {
  		clearInterval(timerId);
  	}
}

$(document).ready(function() {
    $('#terms-iframe').on('load', function() {
        clearInterval(timerId);
    });
});



// SUBSCRIPTION FUNCTIONS

function addPayment(e) {
	e.preventDefault();

	var planId = document.getElementById("planId-hidden").innerHTML;
	var trial = document.getElementById("trial-hidden").innerHTML;

	var handler = StripeCheckout.configure({
	  key: 'pk_live_0QkNn481JzNrjD8QFggCPnVg', //PROD
	  //key: 'pk_test_w4VMStVRr4Df80ompdgnw6KH', //LOCAL
	  image: 'assets/img/ah.ico',
	  name: 'AppyHour',
	  panelLabel: 'Subscribe',
	  locale: 'auto',
	  zipCode: true,
	  token: function(token) {
	    var parameters = { token: token.id, planId: planId, trial: trial };
	    $.get('/savetoken', parameters, function(result) {
  			window.open('/dashboard', '_self');
		});
	  },
	  closed: function() {
	  	
	  }
	});

	// Open Checkout
	handler.open();

	// Close Checkout on page navigation
	window.addEventListener('popstate', function() {
		handler.close();
	});
}

function updatePayment(e) {
	e.preventDefault();

	document.getElementById("btnPayment").disabled = true;

	var handler = StripeCheckout.configure({
	  key: 'pk_live_0QkNn481JzNrjD8QFggCPnVg', //PROD
	  //key: 'pk_test_w4VMStVRr4Df80ompdgnw6KH', //LOCAL
	  image: 'assets/img/ah.ico',
	  name: 'AppyHour',
	  //description: 'Update credit card information',
	  panelLabel: 'Update Card Details',
	  locale: 'auto',
	  zipCode: true,
	  token: function(token) {
	    var parameters = { token: token.id };
	    $.get('/savetoken', parameters, function(result) {
  			location.reload();
		});
	  },
	  closed: function() {
	  	document.getElementById("btnPayment").disabled = false;
	  }
	});

	// Open Checkout
	handler.open();

	// Close Checkout on page navigation
	window.addEventListener('popstate', function() {
		handler.close();
	});
}

function reactivateSubscription(e) {
	e.preventDefault();

	swal({
	  	title: "Are you sure?",
	  	text: "Are you sure you want to reactivate your AppyHour subscription? Your card on file will be charged on your next billing date.",
	  	icon: "warning",
	  	buttons: {
		  	cancel: {
		    	text: "Back",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Reactivate Subscription",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((reactivate) => {
	  	if (reactivate) {
	    	$.get('/reactivatesubscription', function(result) {
  				location.reload();
			});
	  	} 
	});
}

function cancelSubscription(e) {
	e.preventDefault();
	
	swal({
	  	title: "Are you sure?",
	  	text: "Are you sure you want to cancel your AppyHour subscription? You will no longer be able to access any features on your profile at the end of this billing period.",
	  	icon: "warning",
	  	dangerMode: true,
	  	buttons: {
		  	cancel: {
		    	text: "Back",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Cancel Subscription",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((willDelete) => {
	  	if (willDelete) {
	    	$.get('/cancelsubscription', function(result) {
  				location.reload();
			});
	  	} 
	});
} 

function deleteAccount(e) {
	e.preventDefault();
	
	swal({
	  	title: "Are you sure?",
	  	text: "Are you sure you want to delete your AppyHour account? Your profile will be deleted, and your establishment will be removed from the platform.",
	  	icon: "warning",
	  	dangerMode: true,
	  	buttons: {
		  	cancel: {
		    	text: "Back",
		    	value: null,
		    	visible: true,
		    	className: "",
		    	closeModal: true,
		  	},
		  	confirm: {
		    	text: "Delete Account",
		    	value: true,
		    	visible: true,
		    	className: "",
		    	closeModal: true
		  	}
		}
	})
	.then((willDelete) => {
	  	if (willDelete) {
	    	swal("Please enter your password to confirm account deletion:", {
			  	content: "input",
			})
			.then((value) => {
			  	swal(`You typed: ${value}`);
			});
	  	} else {
	    
	  	}
	});
}





/*
function newDate() {
	var t = new Date();
	var tz = t.getTimezoneOffset() / 60;

	if(t.isDstObserved()) { 
		t.setHours(t.getHours() - tz - 1);  
	} else {
		t.setHours(t.getHours() - tz);
	}

	return t;
}

Date.prototype.stdTimezoneOffset = function () {
    var jan = new Date(this.getFullYear(), 0, 1);
    var jul = new Date(this.getFullYear(), 6, 1);
    return Math.max(jan.getTimezoneOffset(), jul.getTimezoneOffset());
}

Date.prototype.isDstObserved = function () {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
}

Date.prototype.addHoursDST = function () {
	var t = new Date();
	var tz = t.getTimezoneOffset() / 60;

	if(t.isDstObserved()) {
		this.setHours(this.getHours() + tz - 1);
	} else {
		this.setHours(this.getHours() + tz);
	}
}

Date.prototype.subtractHoursDST = function () {
	var t = new Date();
	var tz = t.getTimezoneOffset() / 60;
	
	if(t.isDstObserved()) {
		this.setHours(this.getHours() - tz - 1);
	} else {
		this.setHours(this.getHours() - tz);
	}
}
*/