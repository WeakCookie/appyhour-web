/* MENU ACTIONS */

function showSupplierDashboard(e) {
	e.preventDefault();
	window.open('/supplierdashboard', '_self');
}

function showSupplierAccountInfo(e) {
	e.preventDefault();
	document.getElementById("supplier-dropdown-form").method = "get";
	document.getElementById("supplier-dropdown-form").action = "/supplieraccountinfo";
	document.getElementById("supplier-dropdown-form").submit();
  document.getElementById("supplier-dropdown-form-mobile").method = "get";
  document.getElementById("supplier-dropdown-form-mobile").action = "/supplieraccountinfo";
  document.getElementById("supplier-dropdown-form-mobile").submit();
}

function showSupplierManagers(e) {
	e.preventDefault();
	document.getElementById("supplier-dropdown-form").method = "get";
	document.getElementById("supplier-dropdown-form").action = "/suppliereditmanagers";
	document.getElementById("supplier-dropdown-form").submit();
  document.getElementById("supplier-dropdown-form-mobile").method = "get";
  document.getElementById("supplier-dropdown-form-mobile").action = "/suppliereditmanagers";
  document.getElementById("supplier-dropdown-form-mobile").submit();
}

function supplierConfirmLogout(e) {
	e.preventDefault();

	swal({
	  title: "Are you sure you want to logout?",
	  buttons: ["Cancel", "Logout"],
	  dangerMode: true,
	  icon: "warning"
	})
	.then((willLogout) => {
		if(willLogout)
			document.getElementById("supplier-dropdown-form").submit();
      document.getElementById("supplier-dropdown-form-mobile").submit();
	});
}



/* CAMPAIGN ACTIONS */

const TITLE_MAX_CHAR = 30;
const LABEL_MAX_CHAR = 30;
const DESC_MAX_CHAR = 50;
const CTA_LABEL_MAX_CHAR = 50;
const CTA_BTN_MAX_CHAR = 20;

if(window.location.href.indexOf("/suppliersavecampaign") > -1 || window.location.href.indexOf("/suppliersavepromo") > -1)
    window.history.pushState("", "", '/supplierdashboard');

function supplierActivateCampaign(promoId) {
  swal({
    title: "Are you sure you want to activate this campaign?",
    buttons: ["Cancel", "Activate"],
    dangerMode: false,
    icon: "info"
  })
  .then((willActivate) => {
    if(willActivate) {
      document.getElementById("btnActivateCampaign"+promoId).disabled = true;
      document.getElementById("btnActivateCampaign"+promoId).innerHTML = "Activating...";
      $("body").css("cursor", "progress");

      var params = { promoId: promoId };

      $.post('/supplieractivatecampaign', params, function(data) {
        location.reload();
      });
    }
  });
}


/* ADD CAMPAIGN */

var slideIndex = 1;

if(document.getElementsByClassName("addCampaignSlide").length > 0) {
  showSlide(slideIndex);
}

function showSlide(n) { 
  var i;
  var slides = document.getElementsByClassName("addCampaignSlide");
  var steps = document.getElementsByClassName("addCampaignStep");
  var stepLabels = document.getElementsByClassName("addCampaignStepLabel");
  if(n > slides.length)
    slideIndex = 1;
  else if(n < 1) 
    slideIndex = slides.length;
  else 
    slideIndex = n;

  for(i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";

      if(i < slideIndex-1) {
        document.getElementById("addCampaignStep"+(i+1)+"Status").innerHTML = "Complete <i class='fa fa-check'></i>";
        document.getElementById("addCampaignStep"+(i+1)+"Status").style.display = "block";
        document.getElementById("addCampaignStep"+(i+1)+"Status").style.color = "green";
      }
      else if(i == slideIndex-1) {
        document.getElementById("addCampaignStep"+(i+1)+"Status").innerHTML = "In Progress <i class='fa fa-clock-o'></i>";
        document.getElementById("addCampaignStep"+(i+1)+"Status").style.display = "block";
        document.getElementById("addCampaignStep"+(i+1)+"Status").style.color = "#3379b7";
      }
      else {
        document.getElementById("addCampaignStep"+(i+1)+"Status").style.display = "none";
      }
  }
  slides[slideIndex-1].style.display = "block";

  for(i = 0; i < steps.length; i++) {
      if(i <= slideIndex-1)
        steps[i].className += " addCampaignStepActive";
      else
        steps[i].className = steps[i].className.replace(" addCampaignStepActive", "");
  }

  for(i = 0; i < stepLabels.length; i++) {
      if(i <= slideIndex-1)
        stepLabels[i].className += " addCampaignStepLabelActive";
      else
        stepLabels[i].className = stepLabels[i].className.replace(" addCampaignStepLabelActive", "");
  }
}

/* add - logistics */

function supplierTogglePromoDay(day) {
  if(day == -1) {
    document.getElementById("supplier-everydayToggleButton").style.backgroundColor = document.getElementById("supplier-promoDayEveryday").value == "false" ? "rgb(49,151,255)" : "white";
    document.getElementById("supplier-everydayToggleButton").style.color = document.getElementById("supplier-promoDayEveryday").value == "false" ? "white" : "#555";
    document.getElementById("supplier-promoDayEveryday").value = document.getElementById("supplier-promoDayEveryday").value == "false" ? "true" : "false";

    document.getElementById("supplier-monToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-monToggleButton").style.color = "#555";
    document.getElementById("supplier-tueToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-tueToggleButton").style.color = "#555";
    document.getElementById("supplier-wedToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-wedToggleButton").style.color = "#555";
    document.getElementById("supplier-thuToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-thuToggleButton").style.color = "#555";
    document.getElementById("supplier-friToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-friToggleButton").style.color = "#555";
    document.getElementById("supplier-satToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-satToggleButton").style.color = "#555";
    document.getElementById("supplier-sunToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-sunToggleButton").style.color = "#555";

    
    document.getElementById("supplier-promoDayMon").value = "false";
    document.getElementById("supplier-promoDayTue").value = "false";
    document.getElementById("supplier-promoDayWed").value = "false";
    document.getElementById("supplier-promoDayThu").value = "false";
    document.getElementById("supplier-promoDayFri").value = "false";
    document.getElementById("supplier-promoDaySat").value = "false";
    document.getElementById("supplier-promoDaySun").value = "false";
  }
  else {
    document.getElementById("supplier-everydayToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-everydayToggleButton").style.color = "#555";
    document.getElementById("supplier-promoDayEveryday").value = "false";

    if(day == 0) {
      document.getElementById("supplier-sunToggleButton").style.backgroundColor = document.getElementById("supplier-promoDaySun").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-sunToggleButton").style.color = document.getElementById("supplier-promoDaySun").value == "false" ? "white" : "#555";
      document.getElementById("supplier-promoDaySun").value = document.getElementById("supplier-promoDaySun").value == "false" ? "true" : "false";
    }
    else if(day == 1) {
      document.getElementById("supplier-monToggleButton").style.backgroundColor = document.getElementById("supplier-promoDayMon").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-monToggleButton").style.color = document.getElementById("supplier-promoDayMon").value == "false" ? "white" : "#555";
      document.getElementById("supplier-promoDayMon").value = document.getElementById("supplier-promoDayMon").value == "false" ? "true" : "false";
    }
    else if(day == 2) {
      document.getElementById("supplier-tueToggleButton").style.backgroundColor = document.getElementById("supplier-promoDayTue").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-tueToggleButton").style.color = document.getElementById("supplier-promoDayTue").value == "false" ? "white" : "#555";
      document.getElementById("supplier-promoDayTue").value = document.getElementById("supplier-promoDayTue").value == "false" ? "true" : "false";
    }
    else if(day == 3) {
      document.getElementById("supplier-wedToggleButton").style.backgroundColor = document.getElementById("supplier-promoDayWed").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-wedToggleButton").style.color = document.getElementById("supplier-promoDayWed").value == "false" ? "white" : "#555";
      document.getElementById("supplier-promoDayWed").value = document.getElementById("supplier-promoDayWed").value == "false" ? "true" : "false";
    }
    else if(day == 4) {
      document.getElementById("supplier-thuToggleButton").style.backgroundColor = document.getElementById("supplier-promoDayThu").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-thuToggleButton").style.color = document.getElementById("supplier-promoDayThu").value == "false" ? "white" : "#555";
      document.getElementById("supplier-promoDayThu").value = document.getElementById("supplier-promoDayThu").value == "false" ? "true" : "false";
    }
    else if(day == 5) {
      document.getElementById("supplier-friToggleButton").style.backgroundColor = document.getElementById("supplier-promoDayFri").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-friToggleButton").style.color = document.getElementById("supplier-promoDayFri").value == "false" ? "white" : "#555";
      document.getElementById("supplier-promoDayFri").value = document.getElementById("supplier-promoDayFri").value == "false" ? "true" : "false";
    }
    else if(day == 6) {
      document.getElementById("supplier-satToggleButton").style.backgroundColor = document.getElementById("supplier-promoDaySat").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-satToggleButton").style.color = document.getElementById("supplier-promoDaySat").value == "false" ? "white" : "#555";
      document.getElementById("supplier-promoDaySat").value = document.getElementById("supplier-promoDaySat").value == "false" ? "true" : "false";
    }
  }
}

function supplierToggleCampaignType(type) {
  document.getElementById("btnSupSpecials").style.backgroundColor = "white";
  document.getElementById("btnSupSpecials").style.color = "#555";
  document.getElementById("btnSupEvents").style.backgroundColor = "white";
  document.getElementById("btnSupEvents").style.color = "#555";
  document.getElementById("btnSupEsts").style.backgroundColor = "white";
  document.getElementById("btnSupEsts").style.color = "#555";

  document.getElementById("btnSup"+type).style.backgroundColor = "rgb(49,151,255)";
  document.getElementById("btnSup"+type).style.color = "white";
  document.getElementById("supCampaignType").value = type;

  if(type == 'Specials') {
    document.getElementById("fg-features-specials").style.display = "block";
    document.getElementById("fg-features-establishments").style.display = "none";
    document.getElementById("fg-keywords").style.display = "block";
  }
  else if(type == 'Events') {
    document.getElementById("fg-features-specials").style.display = "none";
    document.getElementById("fg-features-establishments").style.display = "none";
    document.getElementById("fg-keywords").style.display = "block";
  }
  else if(type == 'Ests') {
    document.getElementById("fg-features-specials").style.display = "none";
    document.getElementById("fg-features-establishments").style.display = "block";
    document.getElementById("fg-keywords").style.display = "none";
  }

  if(type == 'Specials' || type == 'Events') {
    // don't allow special characters (except spaces) for keywords
    $('.bootstrap-tagsinput input').on('keypress', function(e) {
      var regex = new RegExp("^[0-9a-zA-Z \b]+$");
      var key = String.fromCharCode(e.charCode ? e.charCode : !e.which);
      if (!regex.test(key)) {
         e.preventDefault();
      }
    });
    $('#supKeywords').on('beforeItemAdd', function(event) {
      var tags = $('#supKeywords').val();
      tags = tags.split(",");
      if(tags.length + 1 > 10)
        event.cancel = true;
    });
  }
}

function supplierToggleDineinTakeout(choice) {
  document.getElementById("btnSupBoth").style.backgroundColor = "white";
  document.getElementById("btnSupBoth").style.color = "#555";
  document.getElementById("btnSupDinein").style.backgroundColor = "white";
  document.getElementById("btnSupDinein").style.color = "#555";
  document.getElementById("btnSupCarryout").style.backgroundColor = "white";
  document.getElementById("btnSupCarryout").style.color = "#555";

  document.getElementById("btnSup"+choice).style.backgroundColor = "rgb(49,151,255)";
  document.getElementById("btnSup"+choice).style.color = "white";
  document.getElementById("supDineinTakeout").value = choice;
}

function supplierToggleEstFeature(feat) {
  if(document.getElementById("sup"+feat).value) {
    document.getElementById("btnSup"+feat).style.backgroundColor = "white";
    document.getElementById("btnSup"+feat).style.color = "#555";
    document.getElementById("sup"+feat).value = false;
  }
  else {
    document.getElementById("btnSup"+feat).style.backgroundColor = "rgb(49,151,255)";
    document.getElementById("btnSup"+feat).style.color = "white";
    document.getElementById("sup"+feat).value = true;
  }
}

/* add - design deal feed */

var persistCampaignImg;

function supplierPreviewPromoImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    persistCampaignImg = reader.result;
    document.getElementById("supplier-promo-bg").style.backgroundImage = "url('"+reader.result+"')";
    document.getElementById("supplier-promo-bg").style.backgroundPosition = "center";
    document.getElementById("supplier-promo-bg").style.backgroundSize = "cover";
    document.getElementById("btnSupplierRemovePromo").style.display = "inline-block";
    document.getElementById("supplier-promo-preview").style.display = "none";
    document.getElementById("supplier-use-same-image-div").style.display = "block";
  }
  reader.readAsDataURL(event.target.files[0]);
}

function supplierRemovePromoImage(event) {
  event.preventDefault();

  persistCampaignImg = "";

  document.getElementById("supplier-promo-bg").style.backgroundImage = "none";
  document.getElementById("supplier-promo-preview").style.display = "block";

  document.getElementById("supplier-promoImg").value = null;
  document.getElementById("btnSupplierRemovePromo").style.display = "none";

  document.getElementById("supplier-use-same-image-div").style.display = "none";
}

function supplierToggleTextOverlay(yn) {
  if(yn == "Y") {
    document.getElementById("supplier-text-div").style.display = "block";
    document.getElementById("supplier-promo-preview-p").style.display = "none";

    document.getElementById("btnSupTextOverlayY").style.backgroundColor = "rgb(49,151,255)";
    document.getElementById("btnSupTextOverlayY").style.color = "white";
    document.getElementById("btnSupTextOverlayN").style.backgroundColor = "white";
    document.getElementById("btnSupTextOverlayN").style.color = "#555";

    document.getElementById("supplierAddTextY").value = true;
    document.getElementById("supplierAddTextN").value = false;

    document.getElementById("supplier-promo-title").style.display = "block";
    document.getElementById("supplier-promo-label").style.display = "block";

    if(document.getElementById("supplier-title").value == "") {
      document.getElementById("supplier-promo-title").innerHTML = "Title";
    }
    if(document.getElementById("supplier-label").value == "") {
      document.getElementById("supplier-promo-label").innerHTML = "Label";
    }

    supplierToggleTextColor('white');
    supplierToggleTextAlign('left');
    supplierToggleTextAlignVert('top');
  }
  else {
    document.getElementById("supplier-text-div").style.display = "none";
    document.getElementById("supplier-promo-preview-p").style.display = "block";

    document.getElementById("btnSupTextOverlayY").style.backgroundColor = "white";
    document.getElementById("btnSupTextOverlayY").style.color = "#555";
    document.getElementById("btnSupTextOverlayN").style.backgroundColor = "rgb(49,151,255)";
    document.getElementById("btnSupTextOverlayN").style.color = "white";
    
    document.getElementById("supplierAddTextY").value = false;
    document.getElementById("supplierAddTextN").value = true;

    document.getElementById("supplier-promo-title").style.display = "none";
    document.getElementById("supplier-promo-label").style.display = "none";
  }
}

function supplierToggleTextColor(color) {
  //document.getElementById("btnSupTextColorWhite").style.backgroundColor = color == "white" ? "rgb(49,151,255)" : "white";
  //document.getElementById("btnSupTextColorWhite").style.color = color == "white" ? "white" : "#555";
  document.getElementById("btnSupTextColorWhiteCheck").style.display = color == "white" ? "block" : "none";

  //document.getElementById("btnSupTextColorBlack").style.backgroundColor = color == "black" ? "rgb(49,151,255)" : "white";
  //document.getElementById("btnSupTextColorBlack").style.color = color == "black" ? "white" : "#555";
  document.getElementById("btnSupTextColorBlackCheck").style.display = color == "black" ? "block" : "none";

  document.getElementById("supplierTextColorWhite").value = color == "white" ? true : false;
  document.getElementById("supplierTextColorBlack").value = color == "black" ? true : false;

  document.getElementById("supplier-promo-title").style.backgroundColor = color == "white" ? "black" : "white";
  document.getElementById("supplier-promo-title").style.color = color;
  document.getElementById("supplier-promo-label").style.backgroundColor = color == "white" ? "black" : "white";
  document.getElementById("supplier-promo-label").style.color = color;
}

function supplierToggleTextAlign(align) {
  document.getElementById("btnSupTextAlignLeft").style.backgroundColor = align == "left" ? "rgb(49,151,255)" : "white";
  document.getElementById("btnSupTextAlignLeft").style.color = align == "left" ? "white" : "black";
  document.getElementById("btnSupTextAlignCenter").style.backgroundColor = align == "center" ? "rgb(49,151,255)" : "white";
  document.getElementById("btnSupTextAlignCenter").style.color = align == "center" ? "white" : "black";
  document.getElementById("btnSupTextAlignRight").style.backgroundColor = align == "right" ? "rgb(49,151,255)" : "white";
  document.getElementById("btnSupTextAlignRight").style.color = align == "right" ? "white" : "black";

  document.getElementById("supplierTextAlignLeft").value = align == "left" ? true : false;
  document.getElementById("supplierTextAlignCenter").value = align == "center" ? true : false;
  document.getElementById("supplierTextAlignRight").value = align == "right" ? true : false;

  if(align != "center") {
    document.getElementById("supplier-promo-title").style.left = align == "left" ? "24px" : "";
    document.getElementById("supplier-promo-title").style.right = align == "right" ? "24px" : "";
    document.getElementById("supplier-promo-label").style.left = align == "left" ? "24px" : "";
    document.getElementById("supplier-promo-label").style.right = align == "right" ? "24px" : "";
  }
  else {
    document.getElementById("supplier-promo-title").style.left = 0;
    document.getElementById("supplier-promo-title").style.right = 0;
    document.getElementById("supplier-promo-title").style.margin = "auto";
    document.getElementById("supplier-promo-title").style.width = "fit-content";
    document.getElementById("supplier-promo-label").style.left = 0;
    document.getElementById("supplier-promo-label").style.right = 0;
    document.getElementById("supplier-promo-label").style.margin = "auto";
    document.getElementById("supplier-promo-label").style.width = "fit-content";
  }
}

function supplierToggleTextAlignVert(align) {
  document.getElementById("btnSupTextAlignTop").style.backgroundColor = align == "top" ? "rgb(49,151,255)" : "white";
  document.getElementById("btnSupTextAlignTop").style.color = align == "top" ? "white" : "#555";
  document.getElementById("btnSupTextAlignMiddle").style.backgroundColor = align == "center" ? "rgb(49,151,255)" : "white";
  document.getElementById("btnSupTextAlignMiddle").style.color = align == "center" ? "white" : "#555";
  document.getElementById("btnSupTextAlignBottom").style.backgroundColor = align == "bottom" ? "rgb(49,151,255)" : "white";
  document.getElementById("btnSupTextAlignBottom").style.color = align == "bottom" ? "white" : "#555";

  document.getElementById("imgAlignTop").style.display = align == "top" ? "none" : "inline-block";
  document.getElementById("imgAlignTopWhite").style.display = align == "top" ? "inline-block" : "none";
  document.getElementById("imgAlignMiddle").style.display = align == "center" ? "none" : "inline-block";
  document.getElementById("imgAlignMiddleWhite").style.display = align == "center" ? "inline-block" : "none";
  document.getElementById("imgAlignBottom").style.display = align == "bottom" ? "none" : "inline-block";
  document.getElementById("imgAlignBottomWhite").style.display = align == "bottom" ? "inline-block" : "none";

  document.getElementById("supplierTextAlignTop").value = align == "top" ? true : false;
  document.getElementById("supplierTextAlignMiddle").value = align == "center" ? true : false;
  document.getElementById("supplierTextAlignBottom").value = align == "bottom" ? true : false;

  if(align == "top") {
    document.getElementById("supplier-promo-title").style.top = "38px";
    document.getElementById("supplier-promo-label").style.top = "78px";
  }
  else if(align == "center") {
    document.getElementById("supplier-promo-title").style.top = "75px";
    document.getElementById("supplier-promo-label").style.top = "115px";
  }
  else if(align == "bottom") {
    document.getElementById("supplier-promo-title").style.top = "112px";
    document.getElementById("supplier-promo-label").style.top = "152px";
  }
}

/* add - design details */

var persistCampaignDetailsImg;

function supplierUseSameImage(checkbox) {
  if(checkbox.checked) {
    persistCampaignDetailsImg = persistCampaignImg;
    document.getElementById("supplier-promo-details-bg").style.backgroundImage = "url('"+persistCampaignDetailsImg+"')";
    document.getElementById("supplier-promo-details-bg").style.backgroundPosition = "center";
    document.getElementById("supplier-promo-details-bg").style.backgroundSize = "cover";
    document.getElementById("supplier-promo-details-preview").style.display = "none";
    document.getElementById("supplier-promoDetailsImg").style.display = "none";
  }
  else {
    persistCampaignDetailsImg = null;
    document.getElementById("supplier-promo-details-bg").style.backgroundImage = "none";
    document.getElementById("supplier-promo-details-preview").style.display = "block";
    document.getElementById("supplier-promoDetailsImg").style.display = "block";
  }
}

function supplierPreviewPromoDetailsImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    persistCampaignDetailsImg = reader.result;
    document.getElementById("supplier-promo-details-bg").style.backgroundImage = "url('"+reader.result+"')";
    document.getElementById("supplier-promo-details-bg").style.backgroundPosition = "center";
    document.getElementById("supplier-promo-details-bg").style.backgroundSize = "cover";
    document.getElementById("btnSupplierRemovePromoDetailsImg").style.display = "inline-block";
    document.getElementById("supplier-promo-details-preview").style.display = "none";
  }
  reader.readAsDataURL(event.target.files[0]);
}

function supplierRemovePromoDetailsImage(event) {
  event.preventDefault();

  persistCampaignDetailsImg = "";

  document.getElementById("supplier-promo-details-bg").style.backgroundImage = "none";
  document.getElementById("supplier-promo-details-preview").style.display = "block";

  document.getElementById("supplier-promoDetailsImg").value = null;
  document.getElementById("btnSupplierRemovePromoDetailsImg").style.display = "none";
}

function supplierToggleDetailsTextOverlay(yn) {
  if(yn == "Y") {
    document.getElementById("supplier-details-text-div").style.display = "block";
    document.getElementById("supplier-promo-details-preview-p").style.display = "none";

    document.getElementById("btnSupDetailsTextOverlayY").style.backgroundColor = "rgb(49,151,255)";
    document.getElementById("btnSupDetailsTextOverlayY").style.color = "white";
    document.getElementById("btnSupDetailsTextOverlayN").style.backgroundColor = "white";
    document.getElementById("btnSupDetailsTextOverlayN").style.color = "#555";

    document.getElementById("supplierDetailsAddTextY").value = true;
    document.getElementById("supplierDetailsAddTextN").value = false;

    document.getElementById("supplier-promo-description").style.display = "block";

    if(document.getElementById("supplier-description").value == "") {
      document.getElementById("supplier-promo-description").innerHTML = "Add text here";
    }

    supplierToggleDetailsTextColor("white");
  }
  else {
    document.getElementById("supplier-details-text-div").style.display = "none";
    document.getElementById("supplier-promo-details-preview-p").style.display = "block";

    document.getElementById("btnSupDetailsTextOverlayY").style.backgroundColor = "white";
    document.getElementById("btnSupDetailsTextOverlayY").style.color = "#555";
    document.getElementById("btnSupDetailsTextOverlayN").style.backgroundColor = "rgb(49,151,255)";
    document.getElementById("btnSupDetailsTextOverlayN").style.color = "white";
    
    document.getElementById("supplierDetailsAddTextY").value = false;
    document.getElementById("supplierDetailsAddTextN").value = true;

    document.getElementById("supplier-promo-description").style.display = "none";
  }
}

function supplierToggleDetailsTextColor(color) {
  document.getElementById("btnSupDetailsTextColorWhiteCheck").style.display = color == "white" ? "block" : "none";
  document.getElementById("btnSupDetailsTextColorBlackCheck").style.display = color == "black" ? "block" : "none";

  document.getElementById("supplierDetailsTextColorWhite").value = color == "white" ? true : false;
  document.getElementById("supplierDetailsTextColorBlack").value = color == "black" ? true : false;

  document.getElementById("supplier-promo-description").style.backgroundColor = color == "white" ? "black" : "white";
  document.getElementById("supplier-promo-description").style.color = color;
}

function fill(el) {
  var elId = el.id.replace("supplier-", "");

  if(elId == "title" && el.value != "")
    document.getElementById("supplier-promo-"+elId).innerHTML = el.value;
  else if(elId == "title" && el.value == "")
    document.getElementById("supplier-promo-"+elId).innerHTML = "Title";
  else if(elId == "label" && el.value != "")
    document.getElementById("supplier-promo-"+elId).innerHTML = el.value;
  else if(elId == "label" && el.value == "")
    document.getElementById("supplier-promo-"+elId).innerHTML = "Label";
  else if(elId == "description" && el.value != "")
    document.getElementById("supplier-promo-"+elId).innerHTML = el.value;
  else if(elId == "description" && el.value == "")
    document.getElementById("supplier-promo-"+elId).innerHTML = "Add text here";
  else if(elId == "promo-ctaLabel" && el.value != "")
    document.getElementById("supplier-promo-cta-txt").innerHTML = el.value;
  else if(elId == "promo-ctaLabel" && el.value == "")
    document.getElementById("supplier-promo-cta-txt").innerHTML = "Click this button to do something awesome!";
  else if(elId == "promo-ctaLabelTextColor" && el.value != "") {
    document.getElementById("supplier-promo-cta-txt").style.color = el.value;
    document.getElementById("supplier-promo-ctaLabelTextColorBtn").style.color = el.value;
  }
  else if(elId == "promo-ctaLabelBgColor" && el.value != "")
    document.getElementById("supplier-promo-action-btn-div").style.backgroundColor = el.value;
  else if(elId == "promo-ctaBtnText" && el.value != "")
    document.getElementById("supplier-promo-action-btn-txt").innerHTML = el.value;
  else if(elId == "promo-ctaBtnText" && el.value == "")
    document.getElementById("supplier-promo-action-btn-txt").innerHTML = "Click Me";
  else if(elId == "promo-ctaBtnTextColor" && el.value != "") {
    document.getElementById("supplier-promo-action-btn-txt").style.color = el.value;
    document.getElementById("supplier-promo-ctaBtnTextColorBtn").style.color = el.value;
  }
  else if(elId == "promo-ctaBtnBgColor" && el.value != "")
    document.getElementById("supplier-promo-action-btn").style.backgroundColor = el.value;

  if(elId == "title")
    document.getElementById("supplier-title-char-remain").innerHTML = (TITLE_MAX_CHAR - el.value.length) + " / " + TITLE_MAX_CHAR;
  else if(elId == "label")
    document.getElementById("supplier-label-char-remain").innerHTML = (LABEL_MAX_CHAR - el.value.length) + " / " + LABEL_MAX_CHAR;
  else if(elId == "description")
    document.getElementById("supplier-description-char-remain").innerHTML = (DESC_MAX_CHAR - el.value.length) + " / " + DESC_MAX_CHAR;
  else if(elId == "promo-ctaLabel")
    document.getElementById("supplier-ctaLabel-char-remain").innerHTML = (CTA_LABEL_MAX_CHAR - el.value.length) + " / " + CTA_LABEL_MAX_CHAR;
  else if(elId == "promo-ctaBtnText")
    document.getElementById("supplier-ctaBtnText-char-remain").innerHTML = (CTA_BTN_MAX_CHAR - el.value.length) + " / " + CTA_BTN_MAX_CHAR;
}

/* add - cta */

function supplierToggleCtaDiv(yn) {
  document.getElementById("supplier-cta-div").style.display = yn == "Y" ? "block" : "none";
  document.getElementById("supplier-promo-action-btn-div").style.display = yn == "Y" ? "block" : "none";
  
  document.getElementById("btnSupCtaDivY").style.backgroundColor = yn == "Y" ? "rgb(49,151,255)" : "white";
  document.getElementById("btnSupCtaDivY").style.color = yn == "Y" ? "white" : "#555";
  document.getElementById("btnSupCtaDivN").style.backgroundColor = yn == "Y" ? "white" : "rgb(49,151,255)";
  document.getElementById("btnSupCtaDivN").style.color = yn == "Y" ? "#555" : "white";

  document.getElementById("supplierCtaY").value = yn == "Y" ? true : false;
  document.getElementById("supplierCtaN").value = yn == "Y" ? false : true;

  // label
  if(document.getElementById("supplier-promo-ctaLabel").value == "") {
    document.getElementById("supplier-promo-cta-txt").innerHTML = "Click this button to do something awesome!";
  }
  document.getElementById("supplier-promo-cta-txt").style.color = "#335fdc";
  document.getElementById("supplier-promo-ctaLabelTextColor").value = "#335fdc";
  document.getElementById("supplier-promo-ctaLabelTextColorBtn").style.color = "#335fdc";
  document.getElementById("supplier-promo-action-btn-div").style.backgroundColor = "#f5f5f5";
  document.getElementById("supplier-promo-ctaLabelBgColor").value = "#f5f5f5";

  // button
  if(document.getElementById("supplier-promo-ctaBtnText").value == "") {
    document.getElementById("supplier-promo-action-btn-txt").innerHTML = "Click Me";
  }
  document.getElementById("supplier-promo-action-btn-txt").style.color = "#ffffff";
  document.getElementById("supplier-promo-ctaBtnTextColor").value = "#ffffff";
  document.getElementById("supplier-promo-ctaBtnTextColorBtn").style.color = "#ffffff";
  document.getElementById("supplier-promo-action-btn").style.backgroundColor ="#335fdc";
  document.getElementById("supplier-promo-ctaBtnBgColor").value = "#335fdc";
}

/* add - logistics/design validation */

function supplierValidatePromoLogistics() {
  document.getElementById("addCampaignLogisticsErr").innerHTML = "";
  document.getElementById("addCampaignLogisticsErr").style.display = "none";

  var err = [];
  var campaignType = document.getElementById("supCampaignType").value;
  var dineinTakeout = document.getElementById("supDineinTakeout").value;
  var patio = document.getElementById("supPatio").value;
  var rooftop = document.getElementById("supRooftop").value;
  var dog = document.getElementById("supDog").value;
  var brunch = document.getElementById("supBrunch").value;
  var delivery = document.getElementById("supDelivery").value;
  var takeout = document.getElementById("supTakeout").value;
  var keywords = document.getElementById("supKeywords").value;
  var everyday = document.getElementById("supplier-promoDayEveryday").value;
  var mon = document.getElementById("supplier-promoDayMon").value;
  var tue = document.getElementById("supplier-promoDayTue").value;
  var wed = document.getElementById("supplier-promoDayWed").value;
  var thu = document.getElementById("supplier-promoDayThu").value;
  var fri = document.getElementById("supplier-promoDayFri").value;
  var sat = document.getElementById("supplier-promoDaySat").value;
  var sun = document.getElementById("supplier-promoDaySun").value;
  var location = document.getElementById("supplier-location").value;

  if(campaignType == null || campaignType == "") {
    err.push("Select a <b>Campaign Type</b>");
  }
  else {
    if(campaignType == "Specials") {
      if(dineinTakeout == null || dineinTakeout == "") {
        err.push("Decide if you want to display <b>Dine-in and/or Takeout</b> specials");
      }
    }

    if(campaignType == "Specials" || campaignType == "Events") {
      if(keywords == null || keywords == "") {
        err.push("Enter at least one <b>Keyword</b>");
      }
    }
    else if(campaignType == "Ests") {
      if((patio == null || patio == "") && (rooftop == null || rooftop == "") &&
         (dog == null || dog == "") && (brunch == null || brunch == "") &&
         (delivery == null || delivery == "") && (takeout == null || takeout == "")) {
        err.push("Select at least one establishment <b>Feature</b>")
      }
    } 
  }

  if(everyday == "false" && mon == "false" && tue == "false" && wed == "false" && thu == "false" && fri == "false" && sat == "false" && sun == "false") {
    err.push("Select which <b>Day(s)</b> you want the campaign to run");
  }

  if(location == null || location == "") {
    err.push("Select the <b>Location(s)</b> where the campaign should run");
  }

  if(dineinTakeout == "Dinein")
    dineinTakeout = "Dine-in";
  else if(dineinTakeout == "Both")
    dineinTakeout = "Dine-in & Takeout";
  
  if(err.length == 0) {
    var params = {
      campaignType: campaignType,
      dineinTakeout: dineinTakeout,
      patio: patio,
      rooftop: rooftop,
      dog: dog,
      brunch: brunch,
      delivery: delivery,
      takeout: takeout,
      keywords: keywords,
      everyday: everyday,
      mon: mon,
      tue: tue,
      wed: wed,
      thu: thu,
      fri: fri,
      sat: sat,
      sun: sun,
      location: location
    }
    $.post('/supplierpersistlogistics', params, function(res) {
      showSlide(++slideIndex);
    });
    
  }
  else {
    var errStr = "<p style='font-size:16px; color:red; font-weight:700;'><i class='fa fa-exclamation-triangle'></i> Please correct the following issues:</p>";
    for(var e in err) {
      errStr += "<p style='font-size:14px; color:red;'>- " + err[e] + "</p>";
    }
    document.getElementById("addCampaignLogisticsErr").innerHTML = errStr;
    document.getElementById("addCampaignLogisticsErr").style.display = "block";
  }
}

function supplierValidatePromoDesign() {
  document.getElementById("addCampaignDesignErr").innerHTML = "";
  document.getElementById("addCampaignDesignErr").style.display = "none";

  var err = [];
  // deal feed
  var img = document.getElementById("supplier-promoImg").value;
  var addTextY = document.getElementById("supplierAddTextY").value;
  var addTextN = document.getElementById("supplierAddTextN").value;
  var title = document.getElementById("supplier-title").value;
  var label = document.getElementById("supplier-label").value;
  var textWhite = document.getElementById("supplierTextColorWhite").value;
  var textBlack = document.getElementById("supplierTextColorBlack").value;
  var alignLeft = document.getElementById("supplierTextAlignLeft").value;
  var alignCenter = document.getElementById("supplierTextAlignCenter").value;
  var alignRight = document.getElementById("supplierTextAlignRight").value;
  var alignTop = document.getElementById("supplierTextAlignTop").value;
  var alignMiddle = document.getElementById("supplierTextAlignMiddle").value;
  var alignBottom = document.getElementById("supplierTextAlignBottom").value;

  if(img == null || img == "") {
    err.push("Select an <b>Image</b> to display as the background of the campaign");
  }

  if(addTextY == "false" && addTextN == "false") {
    err.push("Decide if you would like to have <b>Text Overlay</b> the campaign background image");
  }
  else if(addTextY == "true") {
    if(title == null || title == "") {
      err.push("<b>Title</b> is required for text overlay");
    }
    if(textWhite == "false" && textBlack == "false") {
      err.push("Select a <b>Text Color</b>");
    }
    if(alignLeft == "false" && alignCenter == "false" && alignRight == "false") {
      err.push("Select a <b>Horizontal Text Alignment</b>");
    }
    if(alignTop == "false" && alignMiddle == "false" && alignBottom == "false") {
      err.push("Select a <b>Vertical Text Alignment</b>");
    }
  }

  // details view
  var useSameImg = document.getElementById("supplier-useSameImg").checked;
  var imgDetails = document.getElementById("supplier-promoDetailsImg").value;
  var addTextYDetails = document.getElementById("supplierDetailsAddTextY").value;
  var addTextNDetails = document.getElementById("supplierDetailsAddTextN").value;
  var descriptionDetails = document.getElementById("supplier-description").value;
  var textWhiteDetails = document.getElementById("supplierDetailsTextColorWhite").value;
  var textBlackDetails = document.getElementById("supplierDetailsTextColorBlack").value;

  if(!useSameImg) {
    if(imgDetails == null || imgDetails == "") {
      err.push("Select a second <b>Image</b> to display as the background of the campaign details view");
    }
  }
  else {
    imgDetails = img;
  }

  if(addTextYDetails == "false" && addTextNDetails == "false") {
    err.push("Decide if you would like to have <b>Text Overlay</b> the campaign details view background image");
  }
  else if(addTextYDetails == "true") {
    if(descriptionDetails == null || descriptionDetails == "") {
      err.push("<b>Text</b> is required for text overlay on the campaign details view");
    }
    if(textWhiteDetails == "false" && textBlackDetails == "false") {
      err.push("Select a <b>Text Color</b> for the campaign details veiw text overlay");
    }
  }

  // cta
  var ctaY = document.getElementById("supplierCtaY").value;
  if(ctaY == "true") {
    var labelText = document.getElementById("supplier-promo-ctaLabel").value;
    var labelTextColor = document.getElementById("supplier-promo-ctaLabelTextColor").value;
    var labelBgColor = document.getElementById("supplier-promo-ctaLabelBgColor").value;
    var btnText = document.getElementById("supplier-promo-ctaBtnText").value;
    var btnTextColor = document.getElementById("supplier-promo-ctaBtnTextColor").value;
    var btnBgColor = document.getElementById("supplier-promo-ctaBtnBgColor").value;
    var link = document.getElementById("supplier-cta-link").value;

    if(labelText == null || labelText == "") {
      err.push("<b>Label Text</b> is required for displaying a call-to-action");
    }
    if(labelTextColor == null || labelTextColor == "") {
      err.push("<b>Label Text Color</b> is required for displaying a call-to-action");
    }
    if(labelBgColor == null || labelBgColor == "") {
      err.push("<b>Label Background Color</b> is required for displaying a call-to-action");
    }
    if(btnText == null || btnText == "") {
      err.push("<b>Button Text</b> is required for displaying a call-to-action");
    }
    if(btnTextColor == null || btnTextColor == "") {
      err.push("<b>Button Text Color</b> is required for displaying a call-to-action");
    }
    if(btnBgColor == null || btnBgColor == "") {
      err.push("<b>Button Background Color</b> is required for displaying a call-to-action");
    }
    if(link == null || link == "") {
      err.push("<b>Link/URL</b> is required for displaying a call-to-action");
    }
  }

  if(err.length == 0) {

      var params = {
        img: img,
        addTextY: addTextY,
        addTextN: addTextN,
        title: title,
        label: label,
        textWhite: textWhite,
        textBlack: textBlack,
        alignLeft: alignLeft,
        alignCenter: alignCenter,
        alignRight: alignRight,
        alignTop: alignTop,
        alignMiddle: alignMiddle,
        alignBottom: alignBottom,
        imgDetails: imgDetails,
        addTextYDetails: addTextYDetails,
        addTextNDetails: addTextNDetails,
        descriptionDetails: descriptionDetails,
        textWhiteDetails: textWhiteDetails,
        textBlackDetails: textBlackDetails,
        ctaY: ctaY,
        labelText: labelText,
        labelTextColor: labelTextColor,
        labelBgColor: labelBgColor,
        btnText: btnText,
        btnTextColor: btnTextColor,
        btnBgColor: btnBgColor,
        link: link
      }

      var promoImg = $('#supplier-promoImg').get(0).files[0];
      var promoDetailsImg;
      if(useSameImg)
        promoDetailsImg = promoImg;
      else
        promoDetailsImg = $('#supplier-promoDetailsImg').get(0).files[0];

      var formData = new FormData();
      formData.append("promoImg", promoImg);
      formData.append("promoDetailsImg", promoDetailsImg);
      formData.append("json", JSON.stringify(params));

      $.ajax({
        url: '/supplierpersistdesign',
        type: 'POST',
        dataType: 'json',
        //data: JSON.stringify(params),
        data: formData,
        //contentType: 'application/json',
        contentType: false,
        processData: false,
        cache: false,
        success: function(res) {
            // LOGISTICS
            var campaignType = res.campaignType != "Ests" ? res.campaignType : "Establishments";

            document.getElementById("reviewLogisticsFeaturesRow").style.display = campaignType == "Specials" || campaignType == "Establishments" ? "block" : "none";
            document.getElementById("reviewLogisticsKeywordsRow").style.display = campaignType == "Specials" || campaignType == "Events" ? "block" : "none";

            document.getElementById("reviewCampaignType").innerHTML = campaignType;
            document.getElementById("reviewCampaignTypeHidden").value = campaignType.toLowerCase();

            document.getElementById("reviewKeywords").innerHTML = res.keywords.replaceAll(",", ", ");
            document.getElementById("reviewKeywordsHidden").value = res.keywords.replaceAll(",", ";").toLowerCase();

            document.getElementById("reviewFeatures").innerHTML = res.features;
            var featuresHidden = res.features.replaceAll(", ", ";");
            featuresHidden = featuresHidden.replace("Dine-in & Takeout", "");
            featuresHidden = featuresHidden.replace("Dine-in", "dinein");
            featuresHidden = featuresHidden.replace("Takeout", "takeout");
            featuresHidden = featuresHidden.replace("Patio", "patio");
            featuresHidden = featuresHidden.replace("Rooftop", "rooftop");
            featuresHidden = featuresHidden.replace("Dog-friendly", "dog_friendly");
            featuresHidden = featuresHidden.replace("Brunch", "brunch");
            featuresHidden = featuresHidden.replace("Delivery", "delivery");
            document.getElementById("reviewFeaturesHidden").value = featuresHidden;

            document.getElementById("reviewDay").innerHTML = res.dayStr;
            document.getElementById("reviewDayHidden").value = res.day;

            document.getElementById("reviewLocation").innerHTML = res.locationStr;
            document.getElementById("reviewLatitudeHidden").value = res.latitude;
            document.getElementById("reviewLongitudeHidden").value = res.longitude;

            // PREVIEW - DEAL FEED
            document.getElementById("reviewTitleHidden").value = res.title;
            document.getElementById("reviewLabelHidden").value = res.label;
            document.getElementById("reviewBackgroundHidden").value = res.campaignImgPath;
            document.getElementById("reviewBackgroundNameHidden").value = promoImg.name;
            document.getElementById("reviewFontColorHidden").value = res.textWhite == "true" ? "white" : "black";
            
            var textAlignHidden = "center";
            if(res.alignCenter != "true")
              textAlignHidden = res.alignLeft == "true" ? "left" : "right";
            document.getElementById("reviewTextAlignHidden").value = textAlignHidden;

            var verticalAlignHidden = "middle";
            if(res.alignMiddle != "true")
              verticalAlignHidden = res.alignTop == "true" ? "top" : "bottom";
            document.getElementById("reviewVerticalAlignHidden").value = verticalAlignHidden;

            document.getElementById("reviewDetailsBackgroundHidden").value = res.campaignDetailsImgPath;
            document.getElementById("reviewDetailsBackgroundNameHidden").value = promoDetailsImg.name;
            document.getElementById("reviewDescriptionHidden").value = res.descriptionDetails;
            document.getElementById("reviewDetailsFontColorHidden").value = res.textWhiteDetails == "true" ? "white" : "black";
            
            document.getElementById("reviewCtaLabelHidden").value = res.labelText;
            document.getElementById("reviewCtaLabelColorHidden").value = res.labelTextColor;
            document.getElementById("reviewCtaBgColorHidden").value = res.labelBgColor;
            document.getElementById("reviewCtaButtonTextHidden").value = res.btnText;
            document.getElementById("reviewCtaButtonTextColorHidden").value = res.btnTextColor;
            document.getElementById("reviewCtaButtonBgColorHidden").value = res.btnBgColor;
            document.getElementById("reviewCtaLinkHidden").value = res.link;

            if(res.link != null && res.link != "") {
              document.getElementById("review-campaign-action-btn-div").style.display = "block";
              document.getElementById("review-campaign-action-btn-div").style.backgroundColor = res.labelBgColor;
              document.getElementById("review-campaign-cta-txt").innerHTML = res.labelText;
              document.getElementById("review-campaign-cta-txt").style.color = res.labelTextColor;
              document.getElementById("review-campaign-action-btn-txt").innerHTML = res.btnText;
              document.getElementById("review-campaign-action-btn-txt").style.color = res.btnTextColor;
              document.getElementById("review-campaign-action-btn").style.backgroundColor = res.btnBgColor;
            }

            var reviewTitle = document.getElementById("supplier-promo-review-title");
            var reviewLabel = document.getElementById("supplier-promo-review-label");

            document.getElementById("supplier-promo-review-bg").style.backgroundImage = "url('"+persistCampaignImg+"')";
            
            if(res.addTextY == "true") {
              reviewTitle.style.display = "block";
              reviewTitle.innerHTML = res.title;
              reviewTitle.style.color = res.textWhite == "true" ? "white" : "black";
              reviewTitle.style.backgroundColor = res.textWhite == "true" ? "black" : "white";
              
              if(res.label != "") {
                reviewLabel.style.display = "block";
                reviewLabel.innerHTML = res.label;
                reviewLabel.style.color = res.textWhite == "true" ? "white" : "black";
                reviewLabel.style.backgroundColor = res.textWhite == "true" ? "black" : "white";
              }
              else {
                reviewLabel.style.display = "none";
              }

              if(res.alignCenter != "true") {
                reviewTitle.style.left = res.alignLeft == "true" ? "24px" : "";
                reviewTitle.style.right = res.alignRight == "true" ? "24px" : "";
                reviewLabel.style.left = res.alignLeft == "true" ? "24px" : "";
                reviewLabel.style.right = res.alignRight == "true" ? "24px" : "";
              }
              else {
                reviewTitle.style.left = 0;
                reviewTitle.style.right = 0;
                reviewTitle.style.margin = "auto";
                reviewTitle.style.width = "fit-content";
                reviewLabel.style.left = 0;
                reviewLabel.style.right = 0;
                reviewLabel.style.margin = "auto";
                reviewLabel.style.width = "fit-content";
              }

              if(res.alignTop == "true") {
                reviewTitle.style.top = "42px";
                reviewLabel.style.top = "82px";
              }
              else if(res.alignMiddle == "true") {
                reviewTitle.style.top = "75px";
                reviewLabel.style.top = "115px";
              }
              else if(res.alignBottom == "true") {
                reviewTitle.style.top = "118px";
                reviewLabel.style.top = "158px";
              }
            }
            else {
              reviewTitle.style.display = "none";
              reviewLabel.style.display = "none";
            }
            
            // PREVIEW - DETAILS VIEW
            var reviewDescription = document.getElementById("supplier-promo-review-description");

            document.getElementById("supplier-promo-details-review-bg").style.backgroundImage = "url('"+persistCampaignDetailsImg+"')";
            
            if(res.addTextYDetails == "true") {
              reviewDescription.style.display = "block";
              reviewDescription.innerHTML = res.descriptionDetails;
              reviewDescription.style.color = res.textWhiteDetails == "true" ? "white" : "black";
              reviewDescription.style.backgroundColor = res.textWhiteDetails == "true" ? "black" : "white";
            }
            else {
              reviewDescription.style.display = "none";
            }

            showSlide(++slideIndex);
        },
        error: function(err) {
          console.log(err);
        }
      });
  }
  else {
    var errStr = "<p style='font-size:16px; color:red; font-weight:700;'><i class='fa fa-exclamation-triangle'></i> Please correct the following issues:</p>";
    for(var e in err) {
      errStr += "<p style='font-size:14px; color:red;'>- " + err[e] + "</p>";
    }
    document.getElementById("addCampaignDesignErr").innerHTML = errStr;
    document.getElementById("addCampaignDesignErr").style.display = "block";
  }
}



/* EDIT CAMPAIGN */

function supplierEditCampaign(promoId) {
  $("#supplier-promo-modal").modal({backdrop: 'static', keyboard: false}); 
  document.getElementById("btnEditCampaignSave").disabled = false;
  document.getElementById("btnEditCampaignSave").innerHTML = "Save";
  $("body").css("cursor", "default");
  toggleEditCampaignTabs(0);

  var img = document.getElementById("supplier-image"+promoId).innerHTML;
  var title = document.getElementById("supplier-title"+promoId).innerHTML;
  var label = document.getElementById("supplier-label"+promoId).innerHTML;
  var fontColor = document.getElementById("supplier-fontColor"+promoId).innerHTML;
  var textAlign = document.getElementById("supplier-textAlign"+promoId).innerHTML;
  var vertAlign = document.getElementById("supplier-vertAlign"+promoId).innerHTML;
  var day = document.getElementById("supplier-day"+promoId).innerHTML;
  var location = document.getElementById("supplier-location"+promoId).innerHTML;
  var promoType = document.getElementById("supplier-promo_type"+promoId).innerHTML;
  var keywords = document.getElementById("supplier-keywords"+promoId).innerHTML;
  var features = document.getElementById("supplier-features"+promoId).innerHTML;

  document.getElementById("supplier-promo_id").value = promoId;
  document.getElementById("supplier-edit-campaign-original-img").value = img;
  document.getElementById("supplier-edit-campaign-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+img+"')";
  document.getElementById("supplier-edit-campaign-bg").style.backgroundPosition = "center";
  document.getElementById("supplier-edit-campaign-bg").style.backgroundSize = "cover";

  if(title != "") {
    document.getElementById("supplier-edit-campaign-bg-title").style.display = "block";
    document.getElementById("supplier-edit-campaign-bg-title").innerHTML = title;
    document.getElementById("supplier-edit-campaign-bg-title").style.backgroundColor = fontColor == "white" ? "black" : "white";
    document.getElementById("supplier-edit-campaign-bg-title").style.color = fontColor;
    if(textAlign == "left") {
      document.getElementById("supplier-edit-campaign-bg-title").style.left = "24px";
      document.getElementById("supplier-edit-campaign-bg-title").style.right = "";
    }
    else if(textAlign == "center") {
      document.getElementById("supplier-edit-campaign-bg-title").style.left = "0";
      document.getElementById("supplier-edit-campaign-bg-title").style.right = "0";
      document.getElementById("supplier-edit-campaign-bg-title").style.margin = "auto";
    }
    else {
      document.getElementById("supplier-edit-campaign-bg-title").style.right = "24px";
      document.getElementById("supplier-edit-campaign-bg-title").style.left = "";
    }
  }
  else {
    document.getElementById("supplier-edit-campaign-bg-title").style.display = "none";
  }

  if(label != "") {
    document.getElementById("supplier-edit-campaign-bg-label").style.display = "block";
    document.getElementById("supplier-edit-campaign-bg-label").innerHTML = label;
    document.getElementById("supplier-edit-campaign-bg-label").style.backgroundColor = fontColor == "white" ? "black" : "white";
    document.getElementById("supplier-edit-campaign-bg-label").style.color = fontColor;
    if(textAlign == "left") {
      document.getElementById("supplier-edit-campaign-bg-label").style.left = "24px";
      document.getElementById("supplier-edit-campaign-bg-label").style.right = "";
    }
    else if(textAlign == "center") {
      document.getElementById("supplier-edit-campaign-bg-label").style.left = "0";
      document.getElementById("supplier-edit-campaign-bg-label").style.right = "0";
      document.getElementById("supplier-edit-campaign-bg-label").style.margin = "auto";
    }
    else {
      document.getElementById("supplier-edit-campaign-bg-label").style.right = "24px";
      document.getElementById("supplier-edit-campaign-bg-label").style.left = "";
    }
  }
  else {
    document.getElementById("supplier-edit-campaign-bg-label").style.display = "none";
  }

  document.getElementById("supplier-edit-campaign-features-row").style.display = promoType == "establishments" ? "block" : "none";
  document.getElementById("supplier-edit-campaign-special-features-row").style.display = promoType == "specials" ? "block" : "none";
  document.getElementById("supplier-edit-campaign-keywords-row").style.display = promoType != "establishments" ? "block" : "none";

  /* INPUTS */

  document.getElementById("supplier-edit-campaign-add-textY").checked = title != "" ? true : false;
  document.getElementById("supplier-edit-campaign-add-textN").checked = title != "" ? false : true;
  document.getElementById("supplierEditCampaignAddTextY").value = title != "" ? true : false;
  document.getElementById("supplierEditCampaignAddTextN").value = title != "" ? false : true;
  document.getElementById("supplier-edit-campaign-text-div").style.display = title != "" ? "block" : "none";

  document.getElementById("supplier-edit-campaign-title").value = title;
  document.getElementById("supplier-edit-campaign-label").value = label;

  document.getElementById("supplier-edit-title-char-remain").innerHTML = (TITLE_MAX_CHAR - title.length) + " / " + TITLE_MAX_CHAR + " characters remaining";
  document.getElementById("supplier-edit-label-char-remain").innerHTML = (LABEL_MAX_CHAR - label.length) + " / " + LABEL_MAX_CHAR + " characters remaining";

  document.getElementById("supplier-edit-campaign-location").value = location;
  document.getElementById("supplier-edit-campaign-campaignType").value = promoType;

  if(promoType == 'specials' || promoType == 'events') {
    // don't allow special characters (except spaces) for keywords
    $('.bootstrap-tagsinput input').on('keypress', function(e) {
      var regex = new RegExp("^[0-9a-zA-Z \b]+$");
      var key = String.fromCharCode(e.charCode ? e.charCode : !e.which);
      if (!regex.test(key)) {
         e.preventDefault();
      }
    });
    $('#supplier-edit-campaign-keywords').on('beforeItemAdd', function(event) {
      var tags = $('#supplier-edit-campaign-keywords').val();
      tags = tags.split(",");
      if(tags.length + 1 > 10)
        event.cancel = true;
    });

    keywords = keywords.split(";");

    for(var k in keywords)
      $('#supplier-edit-campaign-keywords').tagsinput('add', keywords[k]);
  }

  document.getElementById("supplier-edit-campaign-patio").checked = false;
  document.getElementById("supplier-edit-campaign-rooftop").checked = false;
  document.getElementById("supplier-edit-campaign-dogs").checked = false;
  document.getElementById("supplier-edit-campaign-brunch").checked = false;
  document.getElementById("supplier-edit-campaign-delivery").checked = false;
  //document.getElementById("supplier-edit-campaign-blackowned").checked = false;
  //document.getElementById("supplier-edit-campaign-naoptions").checked = false;
  //document.getElementById("supplier-edit-campaign-paying").checked = false;
  document.getElementById("supplier-edit-campaign-takeout").checked = false;
  document.getElementById("supplier-edit-campaign-carryout").checked = false;

  if(features != "") {
    features = features.split(";");
    
    for(var f in features) {
      if(features[f] == "patio") {
        document.getElementById("supplier-edit-campaign-patio").checked = true;
      }
      else if(features[f] == "rooftop") {
        document.getElementById("supplier-edit-campaign-rooftop").checked = true;
      }
      else if(features[f] == "dog_friendly") {
        document.getElementById("supplier-edit-campaign-dogs").checked = true;
      }
      else if(features[f] == "brunch") {
        document.getElementById("supplier-edit-campaign-brunch").checked = true;
      }
      else if(features[f] == "delivery") {
        document.getElementById("supplier-edit-campaign-delivery").checked = true;
      }
      else if(features[f] == "black_owned") {
        //document.getElementById("supplier-edit-campaign-blackowned").checked = true;
      }
      else if(features[f] == "na_options") {
        //document.getElementById("supplier-edit-campaign-naoptions").checked = true;
      }
      else if(features[f] == "paying") {
        //document.getElementById("supplier-edit-campaign-paying").checked = true;
      }
      else if(features[f] == "takeout" && promoType == "establishments") {
        document.getElementById("supplier-edit-campaign-takeout").checked = true;
      }
      else if(features[f] == "takeout" && promoType == "specials") {
        document.getElementById("supplier-edit-campaign-carryout").checked = true;
      }
    }
  }

  var dayArr = day.split(",");
  for(var d in dayArr)
    supplierEditCampaignTogglePromoDay(parseInt(dayArr[d]));


  /* design - deal feed */

  if(fontColor == "white") {
    supplierEditCampaignToggleTextColor("white");
    document.getElementById("supplier-edit-campaign-textColorWhite").checked = true;
  }
  else {
    supplierEditCampaignToggleTextColor("black");
    document.getElementById("supplier-edit-campaign-textColorBlack").checked = true;
  }

  if(textAlign == "left") {
    supplierEditCampaignToggleTextAlign("left");
    document.getElementById("supplier-edit-campaign-textAlignLeft").checked = true;
  }
  else if(textAlign == "center") {
    supplierEditCampaignToggleTextAlign("center");
    document.getElementById("supplier-edit-campaign-textAlignCenter").checked = true;
  }
  else {
    supplierEditCampaignToggleTextAlign("right");
    document.getElementById("supplier-edit-campaign-textAlignRight").checked = true;
  }

  if(vertAlign == "top") {
    supplierEditCampaignToggleVertAlign("top");
    document.getElementById("supplier-edit-campaign-vertAlignTop").checked = true;
  }
  else if(vertAlign == "middle") {
    supplierEditCampaignToggleVertAlign("middle");
    document.getElementById("supplier-edit-campaign-vertAlignMiddle").checked = true;
  }
  else {
    supplierEditCampaignToggleVertAlign("bottom");
    document.getElementById("supplier-edit-campaign-vertAlignBottom").checked = true;
  }


  /* design - details view */

  var imgDetails = document.getElementById("supplier-details-img"+promoId).innerHTML;
  var textDetails = document.getElementById("supplier-details-text"+promoId).innerHTML;
  var fontColorDetails = document.getElementById("supplier-details-fontColor"+promoId).innerHTML;

  document.getElementById("supplier-edit-campaign-details-original-img").value = imgDetails;
  document.getElementById("supplier-edit-campaign-details-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+imgDetails+"')";
  document.getElementById("supplier-edit-campaign-details-bg").style.backgroundPosition = "center";
  document.getElementById("supplier-edit-campaign-details-bg").style.backgroundSize = "cover";

  if(textDetails != "") {
    document.getElementById("supplier-edit-campaign-bg-description").style.display = "block";
    document.getElementById("supplier-edit-campaign-bg-description").innerHTML = textDetails;
    document.getElementById("supplier-edit-campaign-bg-description").style.backgroundColor = fontColorDetails == "white" ? "black" : "white";
    document.getElementById("supplier-edit-campaign-bg-description").style.color = fontColorDetails == "white" ? "white" : "black";
    document.getElementById("supplier-edit-campaign-bg-description").style.left = "0";
    document.getElementById("supplier-edit-campaign-bg-description").style.right = "0";
    document.getElementById("supplier-edit-campaign-bg-description").style.margin = "auto";
  }
  else {
    document.getElementById("supplier-edit-campaign-bg-description").style.display = "none";
  }

  document.getElementById("supplier-edit-campaign-details-add-textY").checked = textDetails != "" ? true : false;
  document.getElementById("supplier-edit-campaign-details-add-textN").checked = textDetails != "" ? false : true;
  document.getElementById("supplierEditCampaignDetailsAddTextY").value = textDetails != "" ? true : false;
  document.getElementById("supplierEditCampaignDetailsAddTextN").value = textDetails != "" ? false : true;
  document.getElementById("supplier-edit-campaign-details-text-div").style.display = textDetails != "" ? "block" : "none";

  document.getElementById("supplier-edit-campaign-description").value = textDetails;

  document.getElementById("supplier-edit-description-char-remain").innerHTML = (DESC_MAX_CHAR - textDetails.length) + " / " + DESC_MAX_CHAR + " characters remaining";


  if(fontColorDetails == "white") {
    supplierEditCampaignDetailsToggleTextColor("white");
    document.getElementById("supplier-edit-campaign-details-textColorWhite").checked = true;
  }
  else {
    supplierEditCampaignDetailsToggleTextColor("black");
    document.getElementById("supplier-edit-campaign-details-textColorBlack").checked = true;
  }


  /* cta */

  var link = document.getElementById("supplier-link"+promoId).innerHTML.trim();
  var linkText = document.getElementById("supplier-ctaLabel"+promoId).innerHTML.trim();
  var linkTextColor = document.getElementById("supplier-ctaLabelTextColor"+promoId).innerHTML.trim();
  var linkBgColor = document.getElementById("supplier-ctaLabelBgColor"+promoId).innerHTML.trim();
  var linkBtnText = document.getElementById("supplier-linkText"+promoId).innerHTML.trim();
  var linkBtnTextColor = document.getElementById("supplier-linkTextColor"+promoId).innerHTML.trim();
  var linkBtnBgColor = document.getElementById("supplier-linkBgColor"+promoId).innerHTML.trim();

  toggleDetailsActionBtn(link!= "" ? "Y" : "N");

  // cta preview
  document.getElementById("campaign-action-btn-txt").innerHTML = linkBtnText != "" ? linkBtnText : "Click Me";
  document.getElementById("campaign-action-btn-txt").style.color = linkBtnTextColor != "" ? linkBtnTextColor : "#ffffff";
  document.getElementById("campaign-action-btn").style.backgroundColor = linkBtnBgColor != "" ? linkBtnBgColor : "#335fdc";
  document.getElementById("campaign-cta-txt").innerHTML = linkText != "" ? linkText : "Click this button to do something awesome!";
  document.getElementById("campaign-cta-txt").style.color = linkTextColor != "" ? linkTextColor : "#335fdc";
  document.getElementById("campaign-action-btn-div").style.backgroundColor = linkBgColor != "" ? linkBgColor : "#f5f5f5";

  document.getElementById("campaign-link").value = link != "" ? link : "";
  document.getElementById("campaign-linkText").value = linkBtnText != "" ? linkBtnText : "";
  document.getElementById("campaign-linkTextColor").value = linkBtnTextColor != "" ? linkBtnTextColor : "#ffffff";
  document.getElementById("campaign-linkTextColorBtn").style.color = linkBtnTextColor != "" ? linkBtnTextColor : "#ffffff";
  document.getElementById("campaign-linkBgColor").value = linkBtnBgColor != "" ? linkBtnBgColor : "#335fdc";

  document.getElementById("campaign-ctaLabel").value = linkText != "" ? linkText : "";
  document.getElementById("campaign-ctaLabelTextColor").value = linkTextColor != "" ? linkTextColor : "#335fdc";
  document.getElementById("campaign-ctaLabelTextColorBtn").style.color = linkTextColor != "" ? linkTextColor : "#335fdc";
  document.getElementById("campaign-ctaLabelBgColor").value = linkBgColor != "" ? linkBgColor : "#f5f5f5";
}

function toggleEditCampaignTabs(tab) {
  document.getElementById("nav-link-0").className = tab == 0 ? "nav-link active" : "nav-link";
  document.getElementById("nav-link-1").className = tab == 1 ? "nav-link active" : "nav-link";
  document.getElementById("nav-link-2").className = tab == 2 ? "nav-link active" : "nav-link";
  //document.getElementById("nav-link-3").className = tab == 3 ? "nav-link active" : "nav-link";

  document.getElementById("logistics-div").style.display = tab == 0 ? "block" : "none";
  document.getElementById("design-deal-feed-div").style.display = tab == 1 ? "block" : "none";
  document.getElementById("design-details-view-div").style.display = tab == 2 ? "block" : "none";
  //document.getElementById("cta-div").style.display = tab == 3 ? "block" : "none";
}

/* edit - logistics */

function supplierEditCampaignToggleCampaignType(type) {
  document.getElementById("supplier-edit-campaign-special-features-row").style.display = type == "specials" ? "block" : "none";
  document.getElementById("supplier-edit-campaign-features-row").style.display = type == "establishments" ? "block" : "none";
  document.getElementById("supplier-edit-campaign-keywords-row").style.display = type != "establishments" ? "block" : "none";

  if(type == 'specials' || type == 'events') {
    // don't allow special characters (except spaces) for keywords
    $('.bootstrap-tagsinput input').on('keypress', function(e) {
      var regex = new RegExp("^[0-9a-zA-Z \b]+$");
      var key = String.fromCharCode(e.charCode ? e.charCode : !e.which);
      if (!regex.test(key)) {
         e.preventDefault();
      }
    });
    //$('.bootstrap-tagsinput').tagsinput({
    //  maxTags: 10
    //});
    $('#supplier-edit-campaign-keywords').on('beforeItemAdd', function(event) {
      var tags = $('#supplier-edit-campaign-keywords').val();
      tags = tags.split(",");
      if(tags.length + 1 > 10)
        event.cancel = true;
    });
  }
}

function supplierEditCampaignTogglePromoDay(day) {
  if(day == -1) {
    document.getElementById("supplier-editCampaignEverydayToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDayEveryday").value == "false" ? "rgb(49,151,255)" : "white";
    document.getElementById("supplier-editCampaignEverydayToggleButton").style.color = document.getElementById("supplier-editCampaignDayEveryday").value == "false" ? "white" : "#555";
    document.getElementById("supplier-editCampaignDayEveryday").value = document.getElementById("supplier-editCampaignDayEveryday").value == "false" ? "true" : "false";

    document.getElementById("supplier-editCampaignMonToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignMonToggleButton").style.color = "#555";
    document.getElementById("supplier-editCampaignTueToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignTueToggleButton").style.color = "#555";
    document.getElementById("supplier-editCampaignWedToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignWedToggleButton").style.color = "#555";
    document.getElementById("supplier-editCampaignThuToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignThuToggleButton").style.color = "#555";
    document.getElementById("supplier-editCampaignFriToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignFriToggleButton").style.color = "#555";
    document.getElementById("supplier-editCampaignSatToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignSatToggleButton").style.color = "#555";
    document.getElementById("supplier-editCampaignSunToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignSunToggleButton").style.color = "#555";
    
    document.getElementById("supplier-editCampaignDayMon").value = "false";
    document.getElementById("supplier-editCampaignDayTue").value = "false";
    document.getElementById("supplier-editCampaignDayWed").value = "false";
    document.getElementById("supplier-editCampaignDayThu").value = "false";
    document.getElementById("supplier-editCampaignDayFri").value = "false";
    document.getElementById("supplier-editCampaignDaySat").value = "false";
    document.getElementById("supplier-editCampaignDaySun").value = "false";
  }
  else {
    document.getElementById("supplier-editCampaignEverydayToggleButton").style.backgroundColor = "white";
    document.getElementById("supplier-editCampaignEverydayToggleButton").style.color = "#555";
    document.getElementById("supplier-editCampaignDayEveryday").value = "false";

    if(day == 0) {
      document.getElementById("supplier-editCampaignSunToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDaySun").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-editCampaignSunToggleButton").style.color = document.getElementById("supplier-editCampaignDaySun").value == "false" ? "white" : "#555";
      document.getElementById("supplier-editCampaignDaySun").value = document.getElementById("supplier-editCampaignDaySun").value == "false" ? "true" : "false";
    }
    else if(day == 1) {
      document.getElementById("supplier-editCampaignMonToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDayMon").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-editCampaignMonToggleButton").style.color = document.getElementById("supplier-editCampaignDayMon").value == "false" ? "white" : "#555";
      document.getElementById("supplier-editCampaignDayMon").value = document.getElementById("supplier-editCampaignDayMon").value == "false" ? "true" : "false";
    }
    else if(day == 2) {
      document.getElementById("supplier-editCampaignTueToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDayTue").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-editCampaignTueToggleButton").style.color = document.getElementById("supplier-editCampaignDayTue").value == "false" ? "white" : "#555";
      document.getElementById("supplier-editCampaignDayTue").value = document.getElementById("supplier-editCampaignDayTue").value == "false" ? "true" : "false";
    }
    else if(day == 3) {
      document.getElementById("supplier-editCampaignWedToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDayWed").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-editCampaignWedToggleButton").style.color = document.getElementById("supplier-editCampaignDayWed").value == "false" ? "white" : "#555";
      document.getElementById("supplier-editCampaignDayWed").value = document.getElementById("supplier-editCampaignDayWed").value == "false" ? "true" : "false";
    }
    else if(day == 4) {
      document.getElementById("supplier-editCampaignThuToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDayThu").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-editCampaignThuToggleButton").style.color = document.getElementById("supplier-editCampaignDayThu").value == "false" ? "white" : "#555";
      document.getElementById("supplier-editCampaignDayThu").value = document.getElementById("supplier-editCampaignDayThu").value == "false" ? "true" : "false";
    }
    else if(day == 5) {
      document.getElementById("supplier-editCampaignFriToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDayFri").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-editCampaignFriToggleButton").style.color = document.getElementById("supplier-editCampaignDayFri").value == "false" ? "white" : "#555";
      document.getElementById("supplier-editCampaignDayFri").value = document.getElementById("supplier-editCampaignDayFri").value == "false" ? "true" : "false";
    }
    else if(day == 6) {
      document.getElementById("supplier-editCampaignSatToggleButton").style.backgroundColor = document.getElementById("supplier-editCampaignDaySat").value == "false" ? "rgb(49,151,255)" : "white";
      document.getElementById("supplier-editCampaignSatToggleButton").style.color = document.getElementById("supplier-editCampaignDaySat").value == "false" ? "white" : "#555";
      document.getElementById("supplier-editCampaignDaySat").value = document.getElementById("supplier-editCampaignDaySat").value == "false" ? "true" : "false";
    }
  }
}

/* edit - design deal feed view */

function supplierEditCampaignPreviewImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById("supplier-edit-campaign-bg").style.backgroundImage = "url('"+reader.result+"')";
    document.getElementById("supplier-edit-campaign-bg").style.backgroundPosition = "center";
    document.getElementById("supplier-edit-campaign-bg").style.backgroundSize = "cover";
    document.getElementById("btnSupplierEditCampaignRemoveImg").style.display = "inline-block";
  }
  reader.readAsDataURL(event.target.files[0]);
}

function supplierEditCampaignRemoveImage(event) {
  event.preventDefault();
  var original = document.getElementById("supplier-edit-campaign-original-img").value;
  document.getElementById("supplier-edit-campaign-bg").style.backgroundImage = "url('https://appy-promos.s3.us-east-2.amazonaws.com/"+original+"')";
  document.getElementById("btnSupplierEditCampaignRemoveImg").style.display = "none";
  document.getElementById("supplier-edit-campaign-img").value = null;
}

function supplierEditCampaignAddText(yn) {
  document.getElementById("supplier-edit-campaign-text-div").style.display = yn == "Y" ? "block" : "none";

  document.getElementById("supplier-edit-campaign-add-textY").checked = yn == "Y" ? true : false;
  document.getElementById("supplier-edit-campaign-add-textN").checked = yn == "N" ? true : false;

  document.getElementById("supplierEditCampaignAddTextY").value = yn == "Y" ? true : false;
  document.getElementById("supplierEditCampaignAddTextN").value = yn == "Y" ? false : true;

  document.getElementById("supplier-edit-campaign-bg-title").style.display = yn == "Y" ? "block" : "none";
  document.getElementById("supplier-edit-campaign-bg-label").style.display = yn == "Y" ? "block" : "none";

  if(document.getElementById("supplier-edit-campaign-title").value == "") {
    document.getElementById("supplier-edit-campaign-bg-title").innerHTML = "Title";
  }
  if(document.getElementById("supplier-edit-campaign-label").value == "") {
    document.getElementById("supplier-edit-campaign-bg-label").innerHTML = "Label";
  }
}

function supplierEditCampaignToggleTextColor(fontColor) {
  document.getElementById("supplier-edit-campaign-bg-title").style.backgroundColor = fontColor == "white" ? "black" : "white";
  document.getElementById("supplier-edit-campaign-bg-title").style.color = fontColor;
  document.getElementById("supplier-edit-campaign-bg-label").style.backgroundColor = fontColor == "white" ? "black" : "white";
  document.getElementById("supplier-edit-campaign-bg-label").style.color = fontColor;
}

function supplierEditCampaignToggleTextAlign(textAlign) {
  if(textAlign == "left") {
    document.getElementById("supplier-edit-campaign-bg-title").style.left = "10px";
    document.getElementById("supplier-edit-campaign-bg-title").style.right = "";
    document.getElementById("supplier-edit-campaign-bg-label").style.left = "10px";
    document.getElementById("supplier-edit-campaign-bg-label").style.right = "";
    document.getElementById("supplier-edit-campaign-bg-title").style.margin = "unset";
    document.getElementById("supplier-edit-campaign-bg-label").style.margin = "unset";
  }
  else if(textAlign == "center") {
    document.getElementById("supplier-edit-campaign-bg-title").style.left = "0";
    document.getElementById("supplier-edit-campaign-bg-title").style.right = "0";
    document.getElementById("supplier-edit-campaign-bg-label").style.left = "0";
    document.getElementById("supplier-edit-campaign-bg-label").style.right = "0";
    document.getElementById("supplier-edit-campaign-bg-title").style.margin = "auto";
    document.getElementById("supplier-edit-campaign-bg-label").style.margin = "auto";
  }
  else {
    document.getElementById("supplier-edit-campaign-bg-title").style.right = "10px";
    document.getElementById("supplier-edit-campaign-bg-title").style.left = "";
    document.getElementById("supplier-edit-campaign-bg-label").style.right = "10px";
    document.getElementById("supplier-edit-campaign-bg-label").style.left = "";
    document.getElementById("supplier-edit-campaign-bg-title").style.marginLeft = "auto";
    document.getElementById("supplier-edit-campaign-bg-title").style.marginRight = "0";
    document.getElementById("supplier-edit-campaign-bg-label").style.marginLeft = "auto";
    document.getElementById("supplier-edit-campaign-bg-label").style.marginRight = "0";
  }
}

function supplierEditCampaignToggleVertAlign(align) {
  if(align == "top") {
    document.getElementById("supplier-edit-campaign-bg-title").style.top = "10px";
    document.getElementById("supplier-edit-campaign-bg-label").style.top = "10px";
  }
  else if(align == "middle") {
    document.getElementById("supplier-edit-campaign-bg-title").style.top = "58px";
    document.getElementById("supplier-edit-campaign-bg-label").style.top = "58px";
  }
  else if(align == "bottom") {
    document.getElementById("supplier-edit-campaign-bg-title").style.top = "96px";
    document.getElementById("supplier-edit-campaign-bg-label").style.top = "96px";
  }
}

/* edit - design details view */

function supplierEditCampaignDetailsPreviewImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById("supplier-edit-campaign-details-bg").style.backgroundImage = "url('"+reader.result+"')";
    document.getElementById("supplier-edit-campaign-details-bg").style.backgroundPosition = "center";
    document.getElementById("supplier-edit-campaign-details-bg").style.backgroundSize = "cover";
    document.getElementById("btnSupplierEditCampaignDetailsRemoveImg").style.display = "inline-block";
  }
  reader.readAsDataURL(event.target.files[0]);
}

function supplierEditCampaignDetailsAddText(yn) {
  document.getElementById("supplier-edit-campaign-details-text-div").style.display = yn == "Y" ? "block" : "none";

  document.getElementById("supplier-edit-campaign-details-add-textY").checked = yn == "Y" ? true : false;
  document.getElementById("supplier-edit-campaign-details-add-textN").checked = yn == "N" ? true : false;

  document.getElementById("supplierEditCampaignDetailsAddTextY").value = yn == "Y" ? true : false;
  document.getElementById("supplierEditCampaignDetailsAddTextN").value = yn == "Y" ? false : true;

  document.getElementById("supplier-edit-campaign-bg-description").style.display = yn == "Y" ? "block" : "none";

  if(document.getElementById("supplier-edit-campaign-description").value == "") {
    document.getElementById("supplier-edit-campaign-bg-description").innerHTML = "Add text here";
  }
}

function supplierEditCampaignDetailsToggleTextColor(fontColor) {
  document.getElementById("supplier-edit-campaign-bg-description").style.backgroundColor = fontColor == "white" ? "black" : "white";
  document.getElementById("supplier-edit-campaign-bg-description").style.color = fontColor;
}

function supplierEditCampaignFill(el) {
  var elId = el.id.replace("supplier-edit-campaign-", "");

  if(elId == "title" && el.value != "")
    document.getElementById("supplier-edit-campaign-bg-"+elId).innerHTML = el.value;
  else if(elId == "title" && el.value == "")
    document.getElementById("supplier-edit-campaign-bg-"+elId).innerHTML = "Title";
  else if(elId == "label" && el.value != "")
    document.getElementById("supplier-edit-campaign-bg-"+elId).innerHTML = el.value;
  else if(elId == "label" && el.value == "")
    document.getElementById("supplier-edit-campaign-bg-"+elId).innerHTML = "Label";
  else if(elId == "description" && el.value != "")
    document.getElementById("supplier-edit-campaign-bg-"+elId).innerHTML = el.value;
  else if(elId == "description" && el.value == "")
    document.getElementById("supplier-edit-campaign-bg-"+elId).innerHTML = "Add text here";

  if(elId == "title")
    document.getElementById("supplier-edit-title-char-remain").innerHTML = (TITLE_MAX_CHAR - el.value.length) + " / " + TITLE_MAX_CHAR + " characters remaining";
  else if(elId == "label")
    document.getElementById("supplier-edit-label-char-remain").innerHTML = (LABEL_MAX_CHAR - el.value.length) + " / " + LABEL_MAX_CHAR + " characters remaining";
  else if(elId == "description")
    document.getElementById("supplier-edit-description-char-remain").innerHTML = (DESC_MAX_CHAR - el.value.length) + " / " + DESC_MAX_CHAR + " characters remaining";
}

/* edit - cta */

function toggleDetailsActionBtn(yn) {
  document.getElementById("campaign-details-action-btn-div").style.display = yn == "Y" ? "block" : "none";
  document.getElementById("campaign-details-actionBtnY").checked = yn == "Y" ? true : false;
  document.getElementById("campaign-details-actionBtnN").checked = yn == "N" ? true : false;
  document.getElementById("campaign-action-btn-div").style.display = yn == "Y" ? "block" : "none";
}

function campaignFill(el) {
  var id = el.id.replace("campaign-", "");
  if(id != "description")
    document.getElementById("campaign-bg-"+id).innerHTML = el.value;
  else
    document.getElementById("campaign-details-bg-text").innerHTML = el.value;
}

function campaignFillLink(el) {
  if(el.name == "linkText") {
    document.getElementById("campaign-action-btn-txt").innerHTML = el.value;
  }
  else if(el.name == "linkTextColor") {
    document.getElementById("campaign-action-btn-txt").style.color = el.value;
    document.getElementById("campaign-linkTextColorBtn").style.color = el.value;
  }
  else if(el.name == "linkBgColor") {
    document.getElementById("campaign-action-btn").style.backgroundColor = el.value;
  }
  else if(el.name == "ctaLabel") {
    document.getElementById("campaign-cta-txt").innerHTML = el.value;
  }
  else if(el.name == "ctaLabelTextColor") {
    document.getElementById("campaign-cta-txt").style.color = el.value;
    document.getElementById("campaign-ctaLabelTextColorBtn").style.color = el.value;
  }
  else if(el.name == "ctaLabelBgColor") {
    document.getElementById("campaign-action-btn-div").style.backgroundColor = el.value;
  }
}

/* edit - validation */

function supplierEditCampaignSave(e) {
  e.preventDefault();

  document.getElementById("editCampaignErr").innerHTML = "";
  document.getElementById("editCampaignErr").style.display = "none";

  /* LOGISTICS */

  var err = [];
  var campaignType = document.getElementById("supplier-edit-campaign-campaignType").value;
  var carryout = document.getElementById("supplier-edit-campaign-carryout").checked;
  var patio = document.getElementById("supplier-edit-campaign-patio").value;
  var rooftop = document.getElementById("supplier-edit-campaign-rooftop").value;
  var dog = document.getElementById("supplier-edit-campaign-dogs").value;
  var brunch = document.getElementById("supplier-edit-campaign-brunch").value;
  var delivery = document.getElementById("supplier-edit-campaign-delivery").value;
  var takeout = document.getElementById("supplier-edit-campaign-takeout").value;
  var keywords = document.getElementById("supplier-edit-campaign-keywords").value;
  var everyday = document.getElementById("supplier-editCampaignDayEveryday").value;
  var mon = document.getElementById("supplier-editCampaignDayMon").value;
  var tue = document.getElementById("supplier-editCampaignDayTue").value;
  var wed = document.getElementById("supplier-editCampaignDayWed").value;
  var thu = document.getElementById("supplier-editCampaignDayThu").value;
  var fri = document.getElementById("supplier-editCampaignDayFri").value;
  var sat = document.getElementById("supplier-editCampaignDaySat").value;
  var sun = document.getElementById("supplier-editCampaignDaySun").value;
  var location = document.getElementById("supplier-edit-campaign-location").value;

  if(campaignType == null || campaignType == "") {
    err.push("Select a <b>Campaign Type</b>");
  }
  else {
    if(campaignType == "specials" || campaignType == "events") {
      if(keywords == null || keywords == "") {
        err.push("Enter at least one <b>Keyword</b>");
      }
    }
    else if(campaignType == "establishments") {
      if((patio == null || patio == "") && (rooftop == null || rooftop == "") &&
         (dog == null || dog == "") && (brunch == null || brunch == "") &&
         (delivery == null || delivery == "") && (takeout == null || takeout == "")) {
        err.push("Select at least one establishment <b>Feature</b>")
      }
    } 
  }

  if(everyday == "false" && mon == "false" && tue == "false" && wed == "false" && thu == "false" && fri == "false" && sat == "false" && sun == "false") {
    err.push("Select which <b>Day(s)</b> you want the campaign to run");
  }

  if(location == null || location == "") {
    err.push("Select the <b>Location(s)</b> where the campaign should run");
  }

  /* DESIGN */

  // deal feed
  var img = document.getElementById("supplier-edit-campaign-img").value;
  var imgOriginal = document.getElementById("supplier-edit-campaign-original-img").value;
  var addTextY = document.getElementById("supplierEditCampaignAddTextY").value;
  var addTextN = document.getElementById("supplierEditCampaignAddTextN").value;
  var title = document.getElementById("supplier-edit-campaign-title").value;
  var label = document.getElementById("supplier-edit-campaign-label").value;
  var textWhite = document.getElementById("supplier-edit-campaign-textColorWhite").value;
  var textBlack = document.getElementById("supplier-edit-campaign-textColorBlack").value;
  var alignLeft = document.getElementById("supplier-edit-campaign-textAlignLeft").value;
  var alignCenter = document.getElementById("supplier-edit-campaign-textAlignCenter").value;
  var alignRight = document.getElementById("supplier-edit-campaign-textAlignRight").value;
  var alignTop = document.getElementById("supplier-edit-campaign-vertAlignTop").value;
  var alignMiddle = document.getElementById("supplier-edit-campaign-vertAlignMiddle").value;
  var alignBottom = document.getElementById("supplier-edit-campaign-vertAlignBottom").value;

  if((img == null || img == "") && (imgOriginal == null || imgOriginal == "")) {
    err.push("Select an <b>Image</b> to display as the background of the campaign");
  }

  if(addTextY == "false" && addTextN == "false") {
    err.push("Decide if you would like to have <b>Text Overlay</b> the campaign background image");
  }
  else if(addTextY == "true") {
    if(title == null || title == "") {
      err.push("<b>Title</b> is required for text overlay");
    }
    if(textWhite == "false" && textBlack == "false") {
      err.push("Select a <b>Text Color</b>");
    }
    if(alignLeft == "false" && alignCenter == "false" && alignRight == "false") {
      err.push("Select a <b>Horizontal Text Alignment</b>");
    }
    if(alignTop == "false" && alignMiddle == "false" && alignBottom == "false") {
      err.push("Select a <b>Vertical Text Alignment</b>");
    }
  }

  // details view
  var imgDetails = document.getElementById("supplier-edit-campaign-details-img").value;
  var imgDetailsOriginal = document.getElementById("supplier-edit-campaign-details-original-img").value;
  var addTextYDetails = document.getElementById("supplierEditCampaignDetailsAddTextY").value;
  var addTextNDetails = document.getElementById("supplierEditCampaignDetailsAddTextN").value;
  var descriptionDetails = document.getElementById("supplier-edit-campaign-description").value;
  var textWhiteDetails = document.getElementById("supplier-edit-campaign-details-textColorWhite").value;
  var textBlackDetails = document.getElementById("supplier-edit-campaign-details-textColorBlack").value;

  if((imgDetails == null || imgDetails == "") && (imgDetailsOriginal == null || imgDetailsOriginal == "")) {
    err.push("Select a second <b>Image</b> to display as the background of the campaign details view");
  }

  if(addTextYDetails == "false" && addTextNDetails == "false") {
    err.push("Decide if you would like to have <b>Text Overlay</b> the campaign details view background image");
  }
  else if(addTextYDetails == "true") {
    if(descriptionDetails == null || descriptionDetails == "") {
      err.push("<b>Text</b> is required for text overlay on the campaign details view");
    }
    if(textWhiteDetails == "false" && textBlackDetails == "false") {
      err.push("Select a <b>Text Color</b> for the campaign details veiw text overlay");
    }
  }

  // cta
  if(document.getElementById("campaign-details-actionBtnY").checked) {
    var linkText = document.getElementById("campaign-ctaLabel").value;
    var linkTextColor = document.getElementById("campaign-ctaLabelTextColor").value;
    var linkBgColor = document.getElementById("campaign-ctaLabelBgColor").value;
    var linkBtnText = document.getElementById("campaign-linkText").value;
    var linkBtnTextColor = document.getElementById("campaign-linkTextColor").value;
    var linkBtnBgColor = document.getElementById("campaign-linkBgColor").value;
    var link = document.getElementById("campaign-link").value;

    if(linkText == null || linkText == "") {
      err.push("<b>Label Text</b> is required for displaying a call-to-action");
    }
    if(linkTextColor == null || linkTextColor == "") {
      err.push("<b>Label Text Color</b> is required for displaying a call-to-action");
    }
    if(linkBgColor == null || linkBgColor == "") {
      err.push("<b>Label Background Color</b> is required for displaying a call-to-action");
    }
    if(linkBtnText == null || linkBtnText == "") {
      err.push("<b>Button Text</b> is required for displaying a call-to-action");
    }
    if(linkBtnTextColor == null || linkBtnTextColor == "") {
      err.push("<b>Button Text Color</b> is required for displaying a call-to-action");
    }
    if(linkBtnBgColor == null || linkBtnBgColor == "") {
      err.push("<b>Button Background Color</b> is required for displaying a call-to-action");
    }
    if(link == null || link == "") {
      err.push("<b>Link/URL</b> is required for displaying a call-to-action");
    }
  }


  if(err.length == 0) {
    document.getElementById("btnEditCampaignSave").disabled = true;
    document.getElementById("btnEditCampaignSave").innerHTML = "Saving...";
    $("body").css("cursor", "progress");
    document.getElementById("supplier-edit-campaign-form").submit();
  }
  else {
    var errStr = "<p style='font-size:16px; color:red; font-weight:700;'><i class='fa fa-exclamation-triangle'></i> Please correct the following issues:</p>";
    for(var e in err) {
      errStr += "<p style='font-size:14px; color:red;'>- " + err[e] + "</p>";
    }
    document.getElementById("editCampaignErr").innerHTML = errStr;
    document.getElementById("editCampaignErr").style.display = "block";
  }
}



/* PROMO MORE INFO */

$(function() {
  var labels = [];
  var dataArr = [];
  var interactionArr = [];

  var pressTable = document.getElementById("supplierPromoTable");
  if(pressTable != null) {
    var rows = pressTable.rows;
    for(var r = rows.length-1; r >= 0; r--) {
      var cells = rows[r].getElementsByTagName("td");
      if(cells.length == 3) {
        labels.push(cells[0].innerHTML);
        dataArr.push(cells[1].innerHTML);
        interactionArr.push(cells[2].innerHTML);
      }
    }
  }

  if(document.getElementById("supplierPromoChart") != null) {
    var ctx = document.getElementById('supplierPromoChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: 'Impressions',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                pointRadius: 0,
                data: dataArr
            },
            {
                label: 'Interactions',
                backgroundColor: 'rgba(50, 200, 60, 0.2)',
                borderColor: 'rgb(50, 200, 60)',
                borderWidth: 2,
                pointRadius: 0,
                data: interactionArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: true
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: true
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
  }

  allowLocation(true);
});

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function supplierPromoItemsCity(e, city) {
  e.preventDefault();

  if(document.getElementById("promoItemsCityLouisville")) {
    document.getElementById("promoItemsCityLouisville").className = "";
    document.getElementById("promoItemsCityLouisville").children[0].style.color = "rgb(49,151,255)";
  }

  if(document.getElementById("promoItemsCityMinneapolis")) {
    document.getElementById("promoItemsCityMinneapolis").className = "";
    document.getElementById("promoItemsCityMinneapolis").children[0].style.color = "rgb(49,151,255)";
  }

  if(document.getElementById("promoItemsCityIowacity")) {
    document.getElementById("promoItemsCityIowacity").className = "";
    document.getElementById("promoItemsCityIowacity").children[0].style.color = "rgb(49,151,255)";
  }

  if(document.getElementById("promoItemsCityDenver")) {
    document.getElementById("promoItemsCityDenver").className = "";
    document.getElementById("promoItemsCityDenver").children[0].style.color = "rgb(49,151,255)";
  }

  if(document.getElementById("promoItemsCityTampabay")) {
    document.getElementById("promoItemsCityTampabay").className = "";
    document.getElementById("promoItemsCityTampabay").children[0].style.color = "rgb(49,151,255)";
  }

  document.getElementById("promoItemsCity"+city).className = "active";
  document.getElementById("promoItemsCity"+city).children[0].style.color = "black";

  var items = document.getElementsByClassName("LouisvilleItem");
  for(var i in items) {
    if(items[i].tagName == "DIV")
      items[i].style.display = "none";
  }

  items = document.getElementsByClassName("MinneapolisItem");
  for(var i in items) {
    if(items[i].tagName == "DIV")
      items[i].style.display = "none";
  }

  items = document.getElementsByClassName("IowacityItem");
  for(var i in items) {
    if(items[i].tagName == "DIV")
      items[i].style.display = "none";
  }

  items = document.getElementsByClassName("DenverItem");
  for(var i in items) {
    if(items[i].tagName == "DIV")
      items[i].style.display = "none";
  }

  items = document.getElementsByClassName("TampabayItem");
  for(var i in items) {
    if(items[i].tagName == "DIV")
      items[i].style.display = "none";
  }

  items = document.getElementsByClassName(city+"Item");
  for(var i in items) {
    if(items[i].tagName == "DIV")
      items[i].style.display = "block";
  }
}

function allowLocation(displayWaiting) {
  var tz = getCookie("tz");

  if((tz === undefined || tz == '') && location.pathname == '/supplierdashboard') {
    //if(displayWaiting)
    //  document.getElementById("supplier-body-waiting").style.display = "block";

    //document.getElementById("supplier-body").className = "supplier-body-waiting";

    window.navigator.geolocation.getCurrentPosition(function(position) {
      if(position) {
        var params = {
          lat: position.coords.latitude, 
          lon: position.coords.longitude,
          userLocationRequested: true
        };

        $.post('/suppliersaveuserlocation', params, function(tz) {
          console.log("user allowed location - setting to " + tz);
          document.cookie = "tz="+tz;
          location.reload();
        });
      }
      else {
        var params = {
          lat: "", 
          lon: "",
          userLocationRequested: false
        };

        $.post('/suppliersaveuserlocation', params, function(tz) {
          console.log("location not found - setting to " + tz);
          document.cookie = "tz="+tz;
          location.reload();
        });
      }
    },
    function(err) {
      var params = {
        lat: "", 
        lon: "",
        userLocationRequested: false
      };

      $.post('/suppliersaveuserlocation', params, function(tz) {
        console.log("user denied location - setting to " + tz);
        document.cookie = "tz="+tz;
        location.reload();
      });
    });
  }
  else if(tz !== undefined && tz != '' && location.pathname == '/supplierdashboard'){
    var params = {
      lat: "", 
      lon: "",
      userLocationRequested: false
    };

    $.post('/suppliersaveuserlocation', params, function(tz) {
      console.log("location set in cookie");
      document.cookie = "tz="+tz;
    });
  }
}



/* ACCOUNT INFO */

function supplierShowEditInfo() {
	var info = document.getElementById("supplierShowInfoDiv");
	var edit = document.getElementById("supplierEditInfoDiv");
	var btn = document.getElementById("sup-btn-edit-info");

	if(edit.style.display == "none") {
		info.style.display = "none";
		edit.style.display = "block";
		btn.innerHTML = "Cancel";
		$("#sup-edit-phone").inputmask({"mask": "(999) 999-9999"});
	}
	else {
		info.style.display = "block";
		edit.style.display = "none";
		btn.innerHTML = "Edit";
	}
}

function supplierPreviewImage(event) {
  var reader = new FileReader();
  reader.onload = function() {
    document.getElementById('sup-edit-profile-picture').src = reader.result;
  }
  reader.readAsDataURL(event.target.files[0]);
}

function supplierOpenPasswordModal(e) {
	e.preventDefault();
	$("#supUpdatePasswordModal").modal({backdrop: 'static', keyboard: false});
}

function supplierUpdatePassword(e) {
	e.preventDefault();

	document.getElementById("supUpdatePasswordErr").innerHTML = "";
	document.getElementById("supUpdatePasswordErr").style.display = "none";

	var currPw = document.getElementById("sup-current-pw").value;
	var newPw = document.getElementById("sup-new-pw").value;
	var newPwConfirm = document.getElementById("sup-new-pw-confirm").value;

	var error = "";

	if(currPw == "") {
		error = error + "<br/>Error: Please enter your current password.";
	}

	if(newPw == "") {
		error = error + "<br/>Error: Please enter your new password.";
	}

	if(newPwConfirm == "") {
		error = error + "<br/>Error: Please enter your new password confirmation.";
	}

	if(newPw != "" && newPw.length < 6) {
		error = error + "<br/>Error: Password must be at least 6 characters.";
	}

	if(newPw != "" && newPwConfirm != "" && newPw != newPwConfirm) {
		error = error + "<br/>Error: New Password and New Password Confirm do not match.";
	}

	if(currPw != "" && newPw != "" && currPw == newPw) {
		error = error + "<br/>Error: New Password can't be your current password.";
	}

	if(error == "") {
		var params = {
			currPw: currPw,
			newPw: newPw
		};

		$.post('/supplierupdatepassword', params, function(data) {
			if(data == "true") {
				location.reload();
			}
			else {
				document.getElementById("supUpdatePasswordErr").innerHTML = "Error: " + data;
				document.getElementById("supUpdatePasswordErr").style.display = "block";
			}
		});
	}
	else {
		document.getElementById("supUpdatePasswordErr").innerHTML = error;
		document.getElementById("supUpdatePasswordErr").style.display = "block";
	}
}

function supplierCancelUpdatePassword() {
	document.getElementById("supUpdatePasswordErr").innerHTML = "";
	document.getElementById("supUpdatePasswordErr").style.display = "none";
	document.getElementById("supUpdatePasswordForm").reset();
}

function supplierSaveInfo(e) {
	e.preventDefault();

	document.getElementById("sup-info-err").innerHTML = "";
	document.getElementById("sup-info-err").style.display = "none";

	var err = [];
  var name = null;
  if(document.getElementById("sup-edit-name"))
	   name = document.getElementById("sup-edit-name").value;
	var username = document.getElementById("sup-edit-username").value;
	var email = document.getElementById("sup-edit-email").value;
	var phone = document.getElementById("sup-edit-phone").value;

	if(name == '')
		err.push("Please enter a name.")

	if(username == '')
		err.push("Please enter a username.");

	if(email == '')
		err.push("Please enter an email address.");
	
	var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
	if(!email.match(mailformat))
		err.push("Please enter a valid email address (example@email.com).");

	if(phone.indexOf("_") > -1) 
		err.push("Error: Invalid phone number.")

	if(err.length == 0) {
		var params = {	
			name: name,
			username: username,
			email: email
		}

		$.post('/suppliervalidateinfo', params, function(data) {
			if(data.length == 0) {
				document.getElementById("supSaveInfoForm").submit();
			}
		});	
	}
	else {
		supplierInfoError(err);
	}
}

function supplierInfoError(err) {
	var infoErr = "";

	for(var e in err) {
		infoErr += "<p style='color:red;'>" + err[e] + "</p>";
	}

	document.getElementById("sup-info-err").innerHTML = infoErr;
	document.getElementById("sup-info-err").style.display = "block";
}




/* EDIT MANAGERS */

function supplierShowAddManager() {
	$("#sup-manager-phone").inputmask({"mask": "(999) 999-9999"});
	$("#supAddManagerModal").modal({backdrop: 'static', keyboard: false}); 
}

function supplierAddManager(e) {
	e.preventDefault();

	document.getElementById("supAddManagerErr").innerHTML = "";
	document.getElementById("supAddManagerErr").style.display = "none";

	document.getElementById("supBtnAddManager").disabled = true;
	$("body").css("cursor", "progress");

	var error = [];
	var username = document.getElementById("sup-manager-username").value;
	var email = document.getElementById("sup-manager-email").value;
	var phone = document.getElementById("sup-manager-phone").value;

	phone = phone.replace("(", "").replace(")", "").replace(" ", "").replace("-", "");
	
	if(username == "")
		error.push("Error: Please enter a username.");

	if(!/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))
		error.push("Error: Invalid email address.");
	
	if(phone.length > 0 && phone.length != 10)
		error.push("Error: Please enter a valid phone number.");
	
	if(error.length == 0) {
		var params = {
			username: username,
			email: email,
			phone: phone
		};

		$.post('/supplieraddmanager', params, function(data) {
			if(data == "true") {
				location.reload();
			}
			else {
				document.getElementById("supBtnAddManager").disabled = false;
				$("body").css("cursor", "default");
				document.getElementById("supAddManagerErr").innerHTML = "Error: " + data;
				document.getElementById("supAddManagerErr").style.display = "block";
			}
		});
	}
	else {
		document.getElementById("supBtnAddManager").disabled = false;
		$("body").css("cursor", "default");

		errStr = "";
		for(var e in error) 
			errStr += "<p>" + error[e] + "</p>";
		
		document.getElementById("supAddManagerErr").innerHTML = errStr;
		document.getElementById("supAddManagerErr").style.display = "block";
	}
}

function supplierDeleteManager(e, managerId) {
	e.preventDefault();

	swal({
	  title: "Are you sure you want to delete access for this manager?",
	  buttons: ["Cancel", "Delete"],
	  dangerMode: true,
	  icon: "warning"
	})
	.then((willDelete) => {
		if(willDelete) {
			var params = { managerId: managerId };

			$.post('/supplierdeletemanager', params, function(data) {
				location.reload();
			});
		}
	});
}

function supplierResendClaimEmail(supplierId, email) {
  swal({
    title: "Are you sure?",
    text: "Send another email to " + email + " with login instructions?",
    buttons: ["Cancel", "Send"],
    dangerMode: false,
    icon: "warning"
  })
  .then((willSend) => {
    if(willSend) {
      var params = { 
        supplierId: supplierId,
        email: email
      };

      $.post('/supplierresendclaimemail', params, function(data) {
        location.reload();
      });
    }
  });
}


