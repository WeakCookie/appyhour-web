var metricsCharts = [];

if(window.location.href.indexOf("/metricslogout") > -1 || window.location.href.indexOf("/metricsauth") > -1)
    window.history.pushState("", "", '/metrics');

function loadMetricsCharts() {
    if(document.getElementById('totalEstChart')) {
        var labels = [];
        var dataArr = [];
        var payingArr = [];
        var revenueArr = [];
        var iosInstallArr = [];
        var androidInstallArr = [];
        var installLabels = [];

        var curr = newDate();
        curr.setHours(0,0,0,0);
        var first = curr.getDate();

        for(var i = 0; i <= 30; i++) {
            var d = new Date(curr.setDate(first));
            var dStr = intMonthToStringAbbr(d.getMonth()) + " " + d.getDate();
            labels.push(dStr);

            var estCount = 0;
            var payingEstCount = 0;
            var price99ests = [];
    		var price89ests = [];

            var allEstSpan = document.getElementsByTagName("span");
    	    for(var aes in allEstSpan) {
    	        if(allEstSpan[aes].id !== undefined && allEstSpan[aes].id.indexOf("allestspan") > -1) {
    	        	var created = parseInt(allEstSpan[aes].children[0].value); 
    	        	var userId = allEstSpan[aes].children[1].value;
    	        	var paying = allEstSpan[aes].children[2].value;

    	        	if(d.getTime() >= created && userId != 1458) {
    	        		estCount += 1;
    	        		if(paying == "true") {
    	        			payingEstCount += 1;

    	        			if(price99ests.length > 0 && price99ests.some(e => e === userId)) {
    							price89ests.push(userId);
    						}
    						else {
    							price99ests.push(userId);
    						}
    	        		}
    	        	}
    	        }
    	    }

    	    dataArr.push(estCount);
    	    payingArr.push(payingEstCount);
    	    revenueArr.push((price99ests.length * 99.99 + price89ests.length * 89.99).toFixed(2));

            first -= 1;
            curr = newDate();
            curr.setHours(0,0,0,0);
        }

        var allIosInstallsSpan = document.getElementsByTagName("span");
        for(var aiis in allIosInstallsSpan) {
            if(allIosInstallsSpan[aiis].id !== undefined && allIosInstallsSpan[aiis].id.indexOf("alliosinstalls") > -1) {
            	var count = parseInt(allIosInstallsSpan[aiis].children[0].value); 
            	iosInstallArr.push(count);

            	var start = new Date(parseInt(allIosInstallsSpan[aiis].children[1].value)); 
            	var end = new Date(parseInt(allIosInstallsSpan[aiis].children[2].value)); 

            	start.addHoursDST();
            	end.addHoursDST();

            	var dateStr = (start.getMonth()+1) + "/" + start.getDate() + " - " + (end.getMonth()+1) + "/" + end.getDate();
            	installLabels.push(dateStr);
            }
        }

        var allAndroidInstallsSpan = document.getElementsByTagName("span");
        for(var aais in allAndroidInstallsSpan) {
            if(allAndroidInstallsSpan[aais].id !== undefined && allAndroidInstallsSpan[aais].id.indexOf("allandroidinstalls") > -1) {
            	var count = parseInt(allAndroidInstallsSpan[aais].children[0].value); 
            	androidInstallArr.push(count);
            }
        }

        labels = labels.reverse();
        dataArr = dataArr.reverse();
        payingArr = payingArr.reverse();
        revenueArr = revenueArr.reverse();

        installLabels = installLabels.reverse();
        iosInstallArr = iosInstallArr.reverse();
        androidInstallArr = androidInstallArr.reverse();

    	var ctx = document.getElementById('totalEstChart').getContext('2d');
        var chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: labels,
                datasets: [{
                    label: '',
                    backgroundColor: 'rgba(0, 90, 160, 0.2)',
                    borderColor: 'rgb(0, 90, 160)',
                    borderWidth: 2,
                    pointRadius: 0,
                    data: dataArr
                }]
            },

            // Configuration options go here
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    intersect: false,
                    mode: 'index',
                    axis: 'x',
                    displayColors: false,
                    callbacks: {
                        title: function(tooltipItem) {
                            return tooltipItem[0].xLabel;
                        },
                        label: function(tooltipItem) {
                            if(parseInt(tooltipItem.yLabel) != 1)
                                return tooltipItem.yLabel;
                            else
                                return tooltipItem.yLabel;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        display: true
                    }],
                    xAxes: [{
                        display: true
                    }]
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                }
            }
        });
        metricsCharts.push(chart);

        ctx = document.getElementById('payingEstChart').getContext('2d');
        chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: labels,
                datasets: [{
                    label: '',
                    backgroundColor: 'rgba(0, 90, 160, 0.2)',
                    borderColor: 'rgb(0, 90, 160)',
                    borderWidth: 2,
                    pointRadius: 0,
                    data: payingArr
                }]
            },

            // Configuration options go here
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    intersect: false,
                    mode: 'index',
                    axis: 'x',
                    displayColors: false,
                    callbacks: {
                        title: function(tooltipItem) {
                            return tooltipItem[0].xLabel;
                        },
                        label: function(tooltipItem) {
                            if(parseInt(tooltipItem.yLabel) != 1)
                                return tooltipItem.yLabel;
                            else
                                return tooltipItem.yLabel;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        display: true
                    }],
                    xAxes: [{
                        display: true
                    }]
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                }
            }
        });
        metricsCharts.push(chart);

        ctx = document.getElementById('revenueChart').getContext('2d');
        chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: labels,
                datasets: [{
                    label: '',
                    backgroundColor: 'rgba(0, 90, 160, 0.2)',
                    borderColor: 'rgb(0, 90, 160)',
                    borderWidth: 2,
                    pointRadius: 0,
                    data: revenueArr
                }]
            },

            // Configuration options go here
            options: {
                legend: {
                    display: false
                },
                tooltips: {
                    intersect: false,
                    mode: 'index',
                    axis: 'x',
                    displayColors: false,
                    callbacks: {
                        title: function(tooltipItem) {
                            return tooltipItem[0].xLabel;
                        },
                        label: function(tooltipItem) {
                            return "$" + formatMoney(tooltipItem.yLabel, 2);
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        display: true
                    }],
                    xAxes: [{
                        display: true
                    }]
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                }
            }
        });
        metricsCharts.push(chart);

        ctx = document.getElementById('installsChart').getContext('2d');
        chart = new Chart(ctx, {
            // The type of chart we want to create
            type: 'line',

            // The data for our dataset
            data: {
                labels: installLabels,
                datasets: [{
                    label: 'iOS',
                    backgroundColor: 'rgba(0, 90, 160, 0.2)',
                    borderColor: 'rgb(0, 90, 160)',
                    borderWidth: 2,
                    pointRadius: 0,
                    data: iosInstallArr
                },
                {
                    label: 'Android',
                    backgroundColor: 'rgba(140, 20, 50, 0.2)',
                    borderColor: 'rgb(140, 20, 50)',
                    borderWidth: 2,
                    pointRadius: 0,
                    data: androidInstallArr
                }]
            },

            // Configuration options go here
            options: {
                legend: {
                    display: true
                },
                tooltips: {
                    intersect: false,
                    mode: 'index',
                    axis: 'x',
                    displayColors: false,
                    callbacks: {
                        title: function(tooltipItem) {
                            return tooltipItem[0].xLabel;
                        }
                    }
                },
                scales: {
                    yAxes: [{
                        display: true
                    }],
                    xAxes: [{
                        display: true
                    }]
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 10,
                        top: 10,
                        bottom: 10
                    }
                }
            }
        });
        metricsCharts.push(chart);
    }
}

function formatMoney(number, decPlaces, decSep, thouSep) {
	decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces,
	decSep = typeof decSep === "undefined" ? "." : decSep;
	thouSep = typeof thouSep === "undefined" ? "," : thouSep;
	var sign = number < 0 ? "-" : "";
	var i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
	var j = (j = i.length) > 3 ? j % 3 : 0;

	return sign +
		(j ? i.substr(0, j) + thouSep : "") +
		i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
		(decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
}


