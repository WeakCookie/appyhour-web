

function determineLogin(e, sup) {
	if(sup)
		showSupplierLogin(e, true);
	else
		showSupplierLogin(e, false);
}

function showSupplierLogin(e, show) {
	e.preventDefault();

	var estlogin = document.getElementById("loginform");
	var suplogin = document.getElementById("supplierloginform");

	if(show) {
		estlogin.style.display = 'none';
		suplogin.style.display = 'block';
	    document.getElementById("btn-establishment-login").className = "btn login-link";
	    document.getElementById("btn-supplier-login").className = "btn login-link-active";
	}
	else {
		estlogin.style.display = 'block';
		suplogin.style.display = 'none';
	    document.getElementById("btn-establishment-login").className = "btn login-link-active";
	    document.getElementById("btn-supplier-login").className = "btn login-link";
	}

	/*
	var estlogin = document.getElementById("est-login-div");
	var suplogin = document.getElementById("supplier-login-div");

	if(show) {
		suplogin.style.display = 'block';
	    window.setTimeout(function(){
	      suplogin.style.opacity = 1;
	      suplogin.style.transform = 'scale(1)';
	    },100);

	    estlogin.style.opacity = 0;
	    estlogin.style.transform = 'scale(0)';
	    window.setTimeout(function(){
	      estlogin.style.display = 'none';
	    },100); // timed to match animation-duration
	}
	else {
		estlogin.style.display = 'block';
	    window.setTimeout(function(){
	      estlogin.style.opacity = 1;
	      estlogin.style.transform = 'scale(1)';
	    },100);

	    suplogin.style.opacity = 0;
	    suplogin.style.transform = 'scale(0)';
	    window.setTimeout(function(){
	      suplogin.style.display = 'none';
	    },100); // timed to match animation-duration
	}
	*/
}


// Establishment login

$(function() {
	$('#btn-estlogin-enter').on('click', function(e) {
		estloginEnterClick(e);
 	});

	$('#btn-estlogin-continue').on('click', function(e) {
		estloginContinueClick(e);
	});

	$('#btn-estlogin-save-and-login').on('click', function(e) {
		estloginSaveAndLoginClick(e);
	});

 	$('#btn-estlogin-login').on('click', function(e) {
 		estloginLoginClick(e);
	});

	$('#btn-estlogin-forgot-password-cancel').on('click', function(e) {
		e.preventDefault();
		$('#loginDiv').show();
		$('#userForgotPasswordDiv').hide();
	});
});

function estloginEnterClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();

	var parameters = { username: $('#username').val() };
  	$.get('/userexists', parameters, function(data) {
  		if(data == true) {
  			$.get('/userhaschangedpassword', parameters, function(data2) {
		     	$('#btn-estlogin-enter').hide();

  				if(data2 == true) {
  					$('#btn-estlogin-login').show();
  					$('#passwordDiv').show();
		     		$('#password').focus();
  				}
  				else {
  					$('#btn-estlogin-continue').show();
  					$('#username').prop("readonly", "readonly");
  					$('#secretPasswordDiv').show();
  					$('#secretpw').focus();
  					$('#login-warning-message').html('If you haven\'t received a Secret Password, please contact the AppyHour team (<a href="mailto:appy-support@appyhourmobile.com">appy-support@appyhourmobile.com</a>) and we will respond promptly in order to assist with your onboarding process!');
  					$('#login-warning-message').show();
  				}
  			});
  		}
  		else {
  			$('#login-error-messages').html('We couldn\'t find that username. Please try again.');
     		$('#login-error-messages').show();
  		}
   	});
}

function estloginContinueClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();
	
	var secretpw = $('#secretpw').val();

	if (secretpw != undefined && secretpw != null && secretpw != "") {
		var parameters = { secretpw: secretpw };
		$.get('/secretpasswordmatches', parameters, function(match) {
  			if (match == true) {
  				parameters = { username: $('#username').val() };
  				$.get('/getemailforforgotpassword', parameters, function(email) {
	  				$('#btn-estlogin-continue').hide();
					$('#btn-estlogin-save-and-login').show();
	  				$('#secretpw').prop("readonly", "readonly");
	  				$('#newPasswordDiv').show();
	  				$('#email').val(email);
	  				$('#email').focus();
	  				$('#login-warning-message').html('Welcome to AppyHour! Please take a moment to update your account information.');
	  				$('#login-warning-message').show();
	  			});
  			}
  			else {
  				$('#login-error-messages').html('Incorrect password. Please try again.');
  				$('#login-error-messages').show();
  			}
		});
	}
}

function estloginSaveAndLoginClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();

	var username = $('#username').val();
  	var email = $('#email').val();
  	var pw = $('#newPassword').val();
  	var pwConfirm = $('#newPasswordConfirm').val();

	if (email != "" && pw != "" && pwConfirm != "") {
  		if (pw.length >= 6) {
    		if (pw == pwConfirm) {
      			//update email and password
				$('#loginform').attr('action', "/updateuser").submit();
				/*
  				$.post('/updateuser', parameters, function(result) {
  					swal({
			            title: "Account updated successfully!",
			            timer: 2000,
			            type: "success",
			            showConfirmButton: false
			        });
			        
  					
  				});
				*/
      		}
    		else {
    			$('#login-error-messages').html('Passwords do not match. Please try again.');
  				$('#login-error-messages').show();
	        }
  		} 
	    else {
	    	$('#login-error-messages').html('Password must be at least 6 characters. Please try again.');
  			$('#login-error-messages').show();
	    }
	} 
	else {
		$('#login-error-messages').html('Please fill out all fields to save.');
  		$('#login-error-messages').show();
	}
}

function estloginLoginClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();

	var parameters = { 
		username: $('#username').val(), 
		password: $('#password').val() 
	};
  	$.get('/passwordmatches', parameters, function(data) {
    	if(data == true) {
    		$('#loginform').submit();
    	} else {
     		$('#login-error-messages').html('Password incorrect. Please try again.');
     		$('#login-error-messages').show();
     	}
   	});
}

function getEmailForUserForgotPassword(e) {
	e.preventDefault();
	$("#userResetPasswordEmail").load("emails/reset_password_email.html");

	var parameters = { username: $('#username').val() };
	
	$.get('/getemailforforgotpassword', parameters, function(data) {
		if(data != "") {
			$('#userForgotPasswordEmail').html(data);
			$('#loginDiv').hide();
			$('#userForgotPasswordDiv').show();
		}
	});
}

function userForgotPassword(e) {
	e.preventDefault();

	var parameters = { email: $('#userForgotPasswordEmail').html(), resetPasswordEmail: $('#userResetPasswordEmail').html() };

	$.get('/forgotpassword', parameters, function(data) {
		if(data) {
			$('#forgot-password-messages').html("Successfully sent message to " + $('#userForgotPasswordEmail').html() + " with password reset instructions!");
		} else {
			$('#forgot-password-messages').html("Error sending message to " + $('#userForgotPasswordEmail').html() + " with password reset instructions.");
		}
		$('#forgot-password-messages').show();
		$('#loginDiv').show();
		$('#userForgotPasswordDiv').hide();
	});
}



// Supplier login

$(function() {
	$('#btn-supplierlogin-enter').on('click', function(e) {
		supplierloginEnterClick(e);
 	});

	$('#btn-supplierlogin-continue').on('click', function(e) {
		supplierloginContinueClick(e);
	});

	$('#btn-supplierlogin-save-and-login').on('click', function(e) {
		supplierloginSaveAndLoginClick(e);
	});

 	$('#btn-supplierlogin-login').on('click', function(e) {
 		supplierloginLoginClick(e);
	});

	$('#btn-supplierlogin-forgot-password-cancel').on('click', function(e) {
		e.preventDefault();
		$('#supplierLoginDiv').show();
		$('#supplierForgotPasswordDiv').hide();
	});
});

function supplierloginEnterClick(e) {
	e.preventDefault();
	$('#supplier-login-error-messages').hide();

	var parameters = { username: $('#supplier_username').val() };
  	$.get('/supplierexists', parameters, function(data) {
  		if(data == true) {
  			$.get('/supplierhaschangedpassword', parameters, function(data2) {
		     	$('#btn-supplierlogin-enter').hide();

  				if(data2 == true) {
  					$('#btn-supplierlogin-login').show();
  					$('#supplierPasswordDiv').show();
		     		$('#supplier_password').focus();
  				}
  				else {
  					$('#btn-supplierlogin-continue').show();
  					$('#supplier_username').prop("readonly", "readonly");
  					$('#supplierSecretPasswordDiv').show();
  					$('#supplier_secretpw').focus();
  					$('#supplier-login-warning-message').html('If you haven\'t received a Secret Password, please contact the AppyHour team (<a href="mailto:appy-support@appyhourmobile.com">appy-support@appyhourmobile.com</a>) and we will respond promptly in order to assist with your onboarding process!');
  					$('#supplier-login-warning-message').show();
  				}
  			});
  		}
  		else {
  			$('#supplier-login-error-messages').html('We couldn\'t find that username. Please try again.');
     		$('#supplier-login-error-messages').show();
  		}
   	});
}

function supplierloginLoginClick(e) {
	e.preventDefault();
	$('#supplier-login-error-messages').hide();

	var parameters = { 
		username: $('#supplier_username').val(), 
		password: $('#supplier_password').val() 
	};
  	$.get('/supplierpasswordmatches', parameters, function(data) {
    	if(data == true) {
    		$('#supplierloginform').submit();
    	} else {
     		$('#supplier-login-error-messages').html('Password incorrect. Please try again.');
     		$('#supplier-login-error-messages').show();
     	}
   	});
}

function supplierloginSaveAndLoginClick(e) {
	e.preventDefault();
	$('#supplier-login-error-messages').hide();

	var username = $('#supplier_username').val();
  	var email = $('#supplier_email').val();
  	var pw = $('#supplier_newPassword').val();
  	var pwConfirm = $('#supplier_newPasswordConfirm').val();

	if (email != "" && pw != "" && pwConfirm != "") {
  		if (pw.length >= 6) {
    		if (pw == pwConfirm) {
      			//update email and password
				$('#supplierloginform').attr('action', "/updatesupplier").submit();
      		}
    		else {
    			$('#supplier-login-error-messages').html('Passwords do not match. Please try again.');
  				$('#supplier-login-error-messages').show();
	        }
  		} 
	    else {
	    	$('#supplier-login-error-messages').html('Password must be at least 6 characters. Please try again.');
  			$('#supplier-login-error-messages').show();
	    }
	} 
	else {
		$('#supplier-login-error-messages').html('Please fill out all fields to save.');
  		$('#supplier-login-error-messages').show();
	}
}



// Common functions 

function enterSubmit(evt) {
	if(evt.keyCode == 13 && !isHidden("loginform")) {
	    if(evt.keyCode == 13 && !isHidden("btn-estlogin-enter")) {
	      	estloginEnterClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-estlogin-continue")) {
	      	estloginContinueClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-estlogin-save-and-login")) {
	      	estloginSaveAndLoginClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-estlogin-login")) {
	      	estloginLoginClick(evt);
	    } 
	}
	else if(evt.keyCode == 13 && !isHidden("supplierloginform")) {
		if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-enter")) {
	      	supplierloginEnterClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-continue")) {
	      	supplierloginContinueClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-save-and-login")) {
	      	supplierloginSaveAndLoginClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-login")) {
	      	supplierloginLoginClick(evt);
	    }
   	}

	/*
	if(evt.keyCode == 13 && !isHidden("est-login-div")) {
	    if(evt.keyCode == 13 && !isHidden("btn-estlogin-enter")) {
	      	estloginEnterClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-estlogin-continue")) {
	      	estloginContinueClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-estlogin-save-and-login")) {
	      	estloginSaveAndLoginClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-estlogin-login")) {
	      	estloginLoginClick(evt);
	    } 
	}
	else if(evt.keyCode == 13 && !isHidden("supplier-login-div")) {
		if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-enter")) {
	      	supplierloginEnterClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-continue")) {
	      	supplierloginContinueClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-save-and-login")) {
	      	supplierloginSaveAndLoginClick(evt);
	    } else if(evt.keyCode == 13 && !isHidden("btn-supplierlogin-login")) {
	      	supplierloginLoginClick(evt);
	    }
   	}
   	*/
}

function isHidden(elementId) {
    var style = window.getComputedStyle(document.getElementById(elementId));
    return (style.display === 'none')
}




