$(function() {
	// INDEX
	$('#btn-modal-enter').on('click', function(e) {
		modalEnterClick(e);
 	});

	$('#btn-modal-continue').on('click', function(e) {
		modalContinueClick(e);
	});

	$('#btn-modal-save-and-login').on('click', function(e) {
		modalSaveAndLoginClick(e);
	});

 	$('#btn-modal-login').on('click', function(e) {
 		modalLoginClick(e);
	});

	$('#btn-modal-cancel').on('click', function(e) {
		$('#login-error-messages').hide();
		$('#login-warning-message').hide();
		$('#forgot-password-messages').hide();
		$('#username').val('');
		$('#username').prop("readonly", false);
		$('#passwordDiv').hide();
		$('#password').val('');
		$('#secretPasswordDiv').hide();
		$('#secretpw').val('');
		$('#secretpw').prop("readonly", false);
		$('#newPasswordDiv').hide();
		$('#email').val('');
		$('#newPassword').val('');
		$('#newPasswordConfirm').val('');
		$('#btn-modal-login').hide();
		$('#btn-modal-continue').hide();
		$('#btn-modal-save-and-login').hide();
    	$('#btn-modal-enter').show();
	});

	$('#btn-modal-forgot-password-cancel').on('click', function(e) {
		e.preventDefault();
		$('#loginDiv').show();
		$('#forgotPasswordDiv').hide();
	});

	// DASHBOARD
	var selectedEstHidden = document.getElementById("selectedEstHidden");
  	if (selectedEstHidden && selectedEstHidden.value != "") {
    	document.getElementById("selectedEst").value = selectedEstHidden.value;
  	}

	var page = getUrlParameter("page");

	if(page == "")
		$('#page').val("home");
	else
		$('#page').val(page);

	if(page == "dashboard") {
		$('#dashboard-div').show();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();
		$('#menu-dashboard').addClass("active");
		$('#menu-home').removeClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').removeClass("active");
		window.history.replaceState(null, null, "/dashboard?page=dashboard");
	}
	else if(page == "home") {
		$('#dashboard-div').hide();
		$('#home-div').show();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();
		$('#menu-dashboard').removeClass("active");
		$('#menu-home').addClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').removeClass("active");
		window.history.replaceState(null, null, "/dashboard?page=home");
	} 
	else if(page == "events") {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').show();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();
		$('#menu-dashboard').removeClass("active");
		$('#menu-home').removeClass("active");
		$('#menu-events').addClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').removeClass("active");
		window.history.replaceState(null, null, "/dashboard?page=events");
	} 
	else if(page == "analytics") {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').show();
		$('#partners-div').hide();
		$('#upgrade-div').hide();
		$('#menu-dashboard').removeClass("active");
		$('#menu-home').removeClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').addClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').removeClass("active");
		window.history.replaceState(null, null, "/dashboard?page=analytics");
	} 
	else if(page == "info") {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').show();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();
		$('#menu-dashboard').removeClass("active");
		$('#menu-home').removeClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').addClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').removeClass("active");
		window.history.replaceState(null, null, "/dashboard?page=info");
	} 
	else if(page == "settings") {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').show();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();
		$('#menu-dashboard').removeClass("active");
		$('#menu-home').removeClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').addClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').removeClass("active");
		window.history.replaceState(null, null, "/dashboard?page=settings");
	} 
	else if(page == "partners") {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').show();
		$('#upgrade-div').hide();
		$('#menu-dashboard').removeClass("active");
		$('#menu-home').removeClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').addClass("active");
		$('#menu-upgrade').removeClass("active");
		window.history.replaceState(null, null, "/dashboard?page=partners");
	} 
	else if(page == "upgrade") {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').show();
		$('#menu-dashboard').removeClass("active");
		$('#menu-home').removeClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').addClass("active");
		window.history.replaceState(null, null, "/dashboard?page=upgrade");
	} 
	else {
		// default to home page
		$('#home-div').show();
		$('#menu-home').addClass("active");

		$('#dashboard-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();
		$('#menu-dashboard').removeClass("active");
		$('#menu-events').removeClass("active");
		$('#menu-info').removeClass("active");
		$('#menu-settings').removeClass("active");
		$('#menu-analytics').removeClass("active");
		$('#menu-partners').removeClass("active");
		$('#menu-upgrade').removeClass("active");
		//window.history.replaceState(null, null, "/");
	}

	$('#menu-dashboard').on('click', function(e) {
		$('#dashboard-div').show();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();

		$('#page').val("dashboard");
		window.history.replaceState(null, null, "/dashboard?page=dashboard");
	});

	$('#menu-home').on('click', function(e) {
		$('#dashboard-div').hide();
		$('#home-div').show();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();

		if(!$('#todaysSpecialsForm').is(':visible')) {
			document.getElementById("loader").style.display = "block";
    		document.getElementById("loaderText").style.display = "block";
    		removeLoading();
		}
		
		$('#page').val("home");
		window.history.replaceState(null, null, "/dashboard?page=home");
	});

	$('#menu-events').on('click', function(e) {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').show();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();

		if(!$('#todaysEventsForm').is(':visible')) {
			document.getElementById("loader").style.display = "block";
    		document.getElementById("loaderText").style.display = "block";
    		removeLoading();
		}

		$('#page').val("events");
		window.history.replaceState(null, null, "/dashboard?page=events");
	});

	$('#menu-info').on('click', function(e) {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').show();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();

		if(!$('#displayEstablishmentInfo').is(':visible')) {
			document.getElementById("loader").style.display = "block";
    		document.getElementById("loaderText").style.display = "block";
    		removeLoading();
		}

		$('#page').val("info");
		window.history.replaceState(null, null, "/dashboard?page=info");
	});

	$('#menu-settings').on('click', function(e) {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').show();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').hide();

		if(!$('#settingsHeader').is(':visible')) {
			document.getElementById("loader").style.display = "block";
    		document.getElementById("loaderText").style.display = "block";
    		removeLoading();
		}

		$('#page').val("settings");
		window.history.replaceState(null, null, "/dashboard?page=settings");
	});

	$('#menu-partners').on('click', function(e) {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').show();
		$('#upgrade-div').hide();

		if(!$('#partnersHeader').is(':visible')) {
			document.getElementById("loader").style.display = "block";
    		document.getElementById("loaderText").style.display = "block";
    		removeLoading();
		}

		$('#page').val("partners");
		window.history.replaceState(null, null, "/dashboard?page=partners");
	});

	$('#menu-analytics').on('click', function(e) {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').show();
		$('#partners-div').hide();
		$('#upgrade-div').hide();

		if(!$('#week').is(':visible') && !$('#mobile-day').is(':visible')) {
			document.getElementById("loader").style.display = "block";
    		document.getElementById("loaderText").style.display = "block";
    		removeLoading();
		}

		$('#page').val("analytics");
		window.history.replaceState(null, null, "/dashboard?page=analytics");
	});

	$('#menu-upgrade').on('click', function(e) {
		$('#dashboard-div').hide();
		$('#home-div').hide();
		$('#events-div').hide();
		$('#info-div').hide();
		$('#settings-div').hide();
		$('#analytics-div').hide();
		$('#partners-div').hide();
		$('#upgrade-div').show();

		if(!$('#upgradeHeader').is(':visible')) {
			document.getElementById("loader").style.display = "block";
    		document.getElementById("loaderText").style.display = "block";
    		removeLoading();
		}

		$('#page').val("upgrade");
		window.history.replaceState(null, null, "/dashboard?page=upgrade");
	});

	$('#logout').on('click', function(e) {
		logoutConfirm();
	});

	$('.sidebar-menu a').on('click', function() {
		if ($( window ).width() <= 768) {
	    	$('.sidebar-toggle-box').click(); //bootstrap 2.x
	    	$('.navbar-toggle').click() //bootstrap 3.x by Richard
		}
	});
});

function premium(e) {
	e.preventDefault();

	$('#dashboard-div').hide();
	$('#home-div').hide();
	$('#events-div').hide();
	$('#info-div').hide();
	$('#settings-div').hide();
	$('#analytics-div').hide();
	$('#partners-div').hide();
	$('#upgrade-div').show();

	$('#menu-dashboard').removeClass("active");
	$('#menu-home').removeClass("active");
	$('#menu-events').removeClass("active");
	$('#menu-info').removeClass("active");
	$('#menu-settings').removeClass("active");
	$('#menu-analytics').removeClass("active");
	$('#menu-partners').removeClass("active");
	$('#menu-upgrade').addClass("active");

	$('#page').val("upgrade");

	window.history.replaceState(null, null, "/dashboard?page=upgrade");
}

function logoutConfirm() {
	swal({
	  title: "Are you sure you want to logout?",
	  buttons: ["Cancel", "Logout"],
	  dangerMode: true,
	  icon: "warning"
	})
	.then((willLogout) => {
		if(willLogout) {
			window.location.href = "/logout";
		}
		else {
			document.getElementById("logout").classList.remove("active");
			var page = getUrlParameter("page");
			if(page != undefined && page != "")
				document.getElementById("menu-"+page).classList.add("active");
			else
				document.getElementById("menu-home").classList.add("active");

		}
	});
}

function getEmailForForgotPassword(e) {
	e.preventDefault();
	$("#resetPasswordEmail").load("emails/reset_password_email.html");

	var parameters = { username: $('#username').val() };
	
	$.get('/getemailforforgotpassword', parameters, function(data) {
		if(data != "") {
			$('#forgotPasswordEmail').html(data);
			$('#loginDiv').hide();
			$('#forgotPasswordDiv').show();
		}
	});
}

function forgotPassword(e) {
	e.preventDefault();

	var parameters = { email: $('#forgotPasswordEmail').html(), resetPasswordEmail: $('#resetPasswordEmail').html() };

	$.get('/forgotpassword', parameters, function(data) {
		if(data) {
			$('#forgot-password-messages').html("Successfully sent message to " + $('#forgotPasswordEmail').html() + " with password reset instructions!");
		} else {
			$('#forgot-password-messages').html("Error sending message to " + $('#forgotPasswordEmail').html() + " with password reset instructions.");
		}
		$('#forgot-password-messages').show();
		$('#loginDiv').show();
		$('#forgotPasswordDiv').hide();
	});
}

function modalEnterClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();

	var parameters = { username: $('#username').val() };
  	$.get('/userexists', parameters, function(data) {
  		if(data == true) {
  			$.get('/userhaschangedpassword', parameters, function(data2) {
		     	$('#btn-modal-enter').hide();

  				if(data2 == true) {
  					$('#btn-modal-login').show();
  					$('#passwordDiv').show();
		     		$('#password').focus();
  				}
  				else {
  					$('#btn-modal-continue').show();
  					$('#username').prop("readonly", "readonly");
  					$('#secretPasswordDiv').show();
  					$('#secretpw').focus();
  					$('#login-warning-message').html('If you haven\'t received a Secret Password, please contact the AppyHour team (<a href="mailto:appy-support@appyhourmobile.com">appy-support@appyhourmobile.com</a>) and we will respond promptly in order to assist with your onboarding process!');
  					$('#login-warning-message').show();
  				}
  			});
  		}
  		else {
  			$('#login-error-messages').html('We couldn\'t find that username. Please try again.');
     		$('#login-error-messages').show();
  		}
   	});
}

function modalContinueClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();
	
	var secretpw = $('#secretpw').val();

	if (secretpw != undefined && secretpw != null && secretpw != "") {
		var parameters = { secretpw: secretpw };
		$.get('/secretpasswordmatches', parameters, function(match) {
  			if (match == true) {
  				parameters = { username: $('#username').val() };
  				$.get('/getemailforforgotpassword', parameters, function(email) {
	  				$('#btn-modal-continue').hide();
					$('#btn-modal-save-and-login').show();
	  				$('#secretpw').prop("readonly", "readonly");
	  				$('#newPasswordDiv').show();
	  				$('#email').val(email);
	  				$('#email').focus();
	  				$('#login-warning-message').html('Welcome to AppyHour! Please take a moment to update your account information.');
	  				$('#login-warning-message').show();
	  			});
  			}
  			else {
  				$('#login-error-messages').html('Incorrect password. Please try again.');
  				$('#login-error-messages').show();
  			}
		});
	}
}

function modalSaveAndLoginClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();

	var username = $('#username').val();
  	var email = $('#email').val();
  	var pw = $('#newPassword').val();
  	var pwConfirm = $('#newPasswordConfirm').val();

	if (email != "" && pw != "" && pwConfirm != "") {
  		if (pw.length >= 6) {
    		if (pw == pwConfirm) {
      			//update email and password
				$('#loginform').attr('action', "/updateuser").submit();
				/*
  				$.post('/updateuser', parameters, function(result) {
  					swal({
			            title: "Account updated successfully!",
			            timer: 2000,
			            type: "success",
			            showConfirmButton: false
			        });
			        
  					
  				});
				*/
      		}
    		else {
    			$('#login-error-messages').html('Passwords do not match. Please try again.');
  				$('#login-error-messages').show();
	        }
  		} 
	    else {
	    	$('#login-error-messages').html('Password must be at least 6 characters. Please try again.');
  			$('#login-error-messages').show();
	    }
	} 
	else {
		$('#login-error-messages').html('Please fill out all fields to save.');
  		$('#login-error-messages').show();
	}
}

function modalLoginClick(e) {
	e.preventDefault();
	$('#login-error-messages').hide();

	var parameters = { 
		username: $('#username').val(), 
		password: $('#password').val() 
	};
  	$.get('/passwordmatches', parameters, function(data) {
    	if(data == true) {
    		$('#loginform').submit();
    	} else {
     		$('#login-error-messages').html('Password incorrect. Please try again.');
     		$('#login-error-messages').show();
     	}
   	});
}

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

function contactUs(e) {
	e.preventDefault();

	var parameters = { 
		first: $('#form_name').val(), 
		last: $('#form_lastname').val(),
		email: $('#form_email').val(),
		phone: $('#form_phone').val(),
		bizName: $('#form_bizName').val(),
		message: $('#form_message').val()
	};

	$.get('/contactus', parameters, function(data) {
		var message = "";
		if(data == "") {
			message = "<div style='background-color:rgba(0,255,0,0.5); padding:10px; border-radius:5px;'>Thank you for contacting us! We will respond to your message asap!</div>"
			$('#form_name').val(''); 
			$('#form_lastname').val('');
			$('#form_email').val('');
			$('#form_phone').val('');
			$('#form_bizName').val('');
			$('#form_message').val('');
		} else {
			message = "<div style='background-color:rgba(255,0,0,0.5); padding:10px; border-radius:5px; color:white;'>"+data+"</div>";
		}
		$('#contact-us-messages').html(message);
     	$('#contact-us-messages').show();
	});
}







