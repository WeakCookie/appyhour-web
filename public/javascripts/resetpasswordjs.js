if(location.pathname.indexOf("resetpassword") > -1) {
	jQuery(document).ready(initResetPwPage);
}

function initResetPwPage() {
	var email = getUrlParameter("email");
	var fpKey = getUrlParameter("fpKey");
	var success = getUrlParameter("success");

	document.getElementById("reset-pw-email").innerHTML = email != undefined ? email : "";

	if(fpKey == undefined && success == "true") {
		document.getElementById("pw-updated-successfully").style.display = "block";
		document.getElementById("pw-not-updated").style.display = "none";
		document.getElementById("invalidFPKey").style.display = "none";
	}
	else if(fpKey != undefined && email != undefined) {
		var parameters = {
			email: email,
			fpKey: fpKey
		}

		$.get('/verifyfpkey', parameters, function(data) {
			if(data == "true") {
				document.getElementById("pw-not-updated").style.display = "block";
				document.getElementById("invalidFPKey").style.display = "none";
			}
			else {
				document.getElementById("pw-not-updated").style.display = "none";
				document.getElementById("invalidFPKey").style.display = "block";
			}
		});
	}
	else {
		document.getElementById("pw-not-updated").style.display = "none";
		document.getElementById("invalidFPKey").style.display = "block";
	}
}

function resetPassword(e) {
	e.preventDefault();

	var email = getUrlParameter("email");
	var fpKey = getUrlParameter("fpKey");
	var pw = document.getElementById("reset-pw").value;
	var pwConfirm = document.getElementById("reset-pw-confirm").value;

	var error = "";

	if(pw == "") {
		error = error == "" ? "Please enter a new password" : error+"<br/>Please enter a new password";
	}
	if(pwConfirm == "") {
		error = error == "" ? "Please enter your password again in the New Password Confirm field" : error+"<br/>Please enter your password again in the New Password Confirm field";
	}
	if(pw != "" && pwConfirm != "" && pw != pwConfirm) {
		error = error == "" ? "Passwords do not match" : error+"<br/>Passwords do not match";
	}
	if(pw.length > 0 && pw.length < 6) {
		error = error == "" ? "Password must be at least 6 characters" : error+"<br/>Password must be at least 6 characters";
	}

	if(error == "") {
		document.getElementById("resetPasswordError").innerHTML = "";
		document.getElementById("resetPasswordError").style.display = "none";

		var parameters = {
			email: email,
			fpKey: fpKey,
			pw: pw
		};

		$.post('/resetpasswordsubmit', parameters, function(data) {
			if(data && data == "true") {
				window.location.href = "/resetpassword?email="+email+"&success=true";
			}
			else {
				document.getElementById("resetPasswordError").innerHTML = data;
				document.getElementById("resetPasswordError").style.display = "block";
			}
		});
	}
	else {
		document.getElementById("resetPasswordError").innerHTML = error;
		document.getElementById("resetPasswordError").style.display = "block";
	}

	
}