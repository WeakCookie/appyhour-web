var data = []; // special impression
var eventData = [];
var favData = [];
var profViewData = [];

var backgroundColors = [];
var labels = [];
var myCharts = [];
var week = -1;
var currentDay = 0;
var showDayPicker = true;


jQuery(document).ready(checkContainerAnalytics);

/*$(document).ready(function() {
    $.fn.dataTable.moment('MM/DD/YYYY');
});*/

function checkContainerAnalytics() {
    if($('#impCount').is(':visible')) { //if the container is visible on the page
        initAnalytics();
    } else {
        setTimeout(checkContainerAnalytics, 50); //wait 50 ms, then try again
    }
}

function initAnalytics() {
    loadData();
    //loadImpressionsChart();
    loadTable("specials", false);
    //loadCalTable();
    //loadCalTableMobile();

    var curr = newDate(); // get current date
    var first = curr.getDate() - 7;
    var last = first + 7; // last day is yesterday

    var weekStart = new Date(curr.setDate(first));
    weekStart.setHours(0,0,0,0);

    var currNew = newDate();
    var weekEnd = new Date(currNew.setDate(last));
    weekEnd.setHours(0,0,0,0);

    loadCharts(weekStart, weekEnd);

    document.getElementById("impChangeRange").innerHTML = "previous 7 days";
    document.getElementById("eventImpChangeRange").innerHTML = "previous 7 days";
    document.getElementById("favChangeRange").innerHTML = "previous 7 days";
    document.getElementById("profViewsChangeRange").innerHTML = "previous 7 days";
}

function loadData() {
    var impSpan = document.getElementsByTagName("span");
    for(var is in impSpan) {
        if(impSpan[is].id !== undefined && impSpan[is].id.indexOf("impressionspan") > -1) {
            var dataArr = [];
            for(var x = 0; x < 12; x++) {
                if(x == 3)
                    dataArr.push(parseInt(impSpan[is].children[x].innerHTML));
                else
                    dataArr.push(impSpan[is].children[x].innerHTML);

            }
            data.push(dataArr);
        }
    }

    var eimpSpan = document.getElementsByTagName("span");
    for(var eis in eimpSpan) {
        if(eimpSpan[eis].id !== undefined && eimpSpan[eis].id.indexOf("eventimpspan") > -1) {
            var dataArr = [];
            for(var x = 0; x < 9; x++) {
                 dataArr.push(eimpSpan[eis].children[x].innerHTML);
            }

            eventData.push(dataArr);
        }
    }

    var favSpan = document.getElementsByTagName("span");
    for(var fav in favSpan) {
        if(favSpan[fav].id !== undefined && favSpan[fav].id.indexOf("favspan") > -1) {
            var dataArr = [];
            for(var x = 0; x < 1; x++) {
                dataArr.push(favSpan[fav].children[x].innerHTML);
            }

            favData.push(dataArr);
        }
    }

    var profViewsSpan = document.getElementsByTagName("span");
    for(var pv in profViewsSpan) {
        if(profViewsSpan[pv].id !== undefined && profViewsSpan[pv].id.indexOf("profviewspan") > -1) {
            var dataArr = [];
            for(var x = 0; x < 2; x++) {
                dataArr.push(profViewsSpan[pv].children[x].innerHTML);
            }

            profViewData.push(dataArr);
        }
    }

    /*
    data = [
        ['$5 Burger Baskets', '04/24/2019', '4:30pm - 8:00pm', 37, 3],
        ['$3 Big Beers', '04/24/2019', '7:00pm - 11:00pm', 97, -1],
        ['BOGO Free Drafts', '04/25/2019', '7:00pm - 11:00pm', 46, 4],
        ['50% off Apps', '04/26/2019', '11:00pm - 2:00am', 81, -1],
        ['$1 Apple Pie Shots', '04/26/2019', '9:00am - 11:00am', 44, 5],
        ['Free Beer w/ Burger', '04/26/2019', '4:00pm - 8:00pm', 93, 5],
        ['$1 U-Call-It', '04/27/2019', '7:00pm - 10:00pm', 120, -1],
        ['BOGO Free PBR', '04/28/2019', '3:00pm - 10:00pm', 74, 0],
        ['$5 Burger Baskets', '04/28/2019', '11:00am - 8:00pm', 40, 0],
        ['50% off Drafts', '04/29/2019', '3:00pm - 10:00pm', 67, 1],
        ['$2 Wells', '04/29/2019', '7:00pm - 2:00am', 29, 1],
        ['50% off Pizza', '04/30/2019', '11:00am - 8:00pm', 58, 2],
        ['BOGO Free Drafts', '04/30/2019', '4:30pm - 6:00pm', 96, -1],
        ['$1.99 Miller Lite Bottles', '04/30/2019', '4:00pm - 2:00am', 78, 2],
        ['50% off Appetizers', '05/01/2019', '4:00pm - 8:00pm', 55, 3],
        ['$3 Margs', '05/01/2019', '8:00pm - 2:00am', 87, -1],
        ['$5 off Large Pizzas', '05/02/2019', '4:00pm - 10:00pm', 0, 4]
    ];*/
}

function loadCharts() {
    week = -1;
    labels = getChartWeekLabels();

    // data
    var dataset = [];
    var eventDataset = [];
    var favDataset = [];
    var profViewDataset = [];

    var curr = newDate(); // get current date
    var first = curr.getDate() + (week * 7);
    var last = first + 7; // last day is yesterday

    var weekStart = new Date(curr.setDate(first));
    weekStart.setHours(0,0,0,0);

    var currNew = newDate();
    var weekEnd = new Date(currNew.setDate(last));
    weekEnd.setHours(0,0,0,0);

    dataset = getImpForWeek(weekStart, weekEnd);
    eventDataset = getEventImpForWeek(weekStart, weekEnd);
    favDataset = getFavForWeek(weekStart, weekEnd);
    profViewDataset = getProfViewsForWeek(weekStart, weekEnd);

    var dataArr = [];
    var eventArr = [];
    var favArr = [];
    var profViewArr = [];

    for(var i = weekStart.getDay(); i < weekStart.getDay()+7; i++) {
        var iDay = i;
        if(iDay > 6)
            iDay -= 7;

        var dayImps = 0;
        var dayEventImps = 0;
        var dayFavs = 0;
        var dayProfViews = 0;

        for(var d = 0; d < dataset.length; d++) {
            var day = parseInt(dataset[d][5]);

            if(day == -1) {
                var ahDate = new Date(Date.parse(dataset[d][0]));
                day = ahDate.getDay();
            }
            //day -= 1;
            //if(day == -1)
            //    day = 6;

            if(day == iDay) {
                dayImps += dataset[d][3];
            }
        }
        dataArr.push(dayImps);

        for(var d = 0; d < eventDataset.length; d++) {
            var day = parseInt(eventDataset[d][3]);

            if(day == -1) {
                var ahDate = new Date(Date.parse(eventDataset[d][0]));
                day = ahDate.getDay();
            }

            //day -= 1;
            //if(day == -1)
            //    day = 6;

            if(day == iDay) {
                dayEventImps += parseInt(eventDataset[d][0]);
            }
        }
        eventArr.push(dayEventImps);

        for(var f = 0; f < favDataset.length; f++) {
            var day = parseInt(favDataset[f][1]);

            //day -= 1;
            //if(day == -1)
            //    day = 6;

            if(day == iDay) {
                dayFavs += 1;
            }
        }
        favArr.push(dayFavs);

        for(var p = 0; p < profViewDataset.length; p++) {
            var day = parseInt(profViewDataset[p][1]);

            //day -= 1;
            //if(day == -1)
            //    day = 6;

            if(day == iDay) {
                dayProfViews += parseInt(profViewDataset[p][2]);
            }
        }
        profViewArr.push(dayProfViews);
    }

    var ctx = document.getElementById('specialImpChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                data: dataArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Impressions";
                        else
                            return tooltipItem.yLabel + " Impression";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);

    var ctx = document.getElementById('eventImpChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                data: eventArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Impressions";
                        else
                            return tooltipItem.yLabel + " Impression";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);

    var ctx = document.getElementById('favoritesChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                data: favArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Favorites";
                        else
                            return tooltipItem.yLabel + " Favorite";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);

    var ctx = document.getElementById('profViewsChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                data: profViewArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Views";
                        else
                            return tooltipItem.yLabel + " View";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);
}

function loadCharts(start, end) {
    labels = getLabels(start, end);

    if(labels.length > 1) {
        document.getElementById("resultsRange").innerHTML = labels[0] + "<sup>" + getDateSuperscript(labels[0]) + "</sup> - " + labels[labels.length-1] + "<sup>" + getDateSuperscript(labels[labels.length-1]) + "</sup>";
        document.getElementById("specialImpChart").style.display = "block";
        document.getElementById("eventImpChart").style.display = "block";
        document.getElementById("favoritesChart").style.display = "block";
        document.getElementById("profViewsChart").style.display = "block";
    }
    else {
        document.getElementById("resultsRange").innerHTML = labels[0] + "<sup>" + getDateSuperscript(labels[0]) + "</sup>";
        document.getElementById("specialImpChart").style.display = "none";
        document.getElementById("eventImpChart").style.display = "none";
        document.getElementById("favoritesChart").style.display = "none";
        document.getElementById("profViewsChart").style.display = "none";
    }

    // data
    var dataset = [];
    var eventDataset = [];
    var favDataset = [];
    var profViewDataset = [];

    var weekStart = start;
    var weekEnd = end;

    dataset = getImpForWeek(weekStart, weekEnd);
    eventDataset = getEventImpForWeek(weekStart, weekEnd);
    favDataset = getFavForWeek(weekStart, weekEnd);
    profViewDataset = getProfViewsForWeek(weekStart, weekEnd);

    var dataArr = [];
    var eventArr = [];
    var favArr = [];
    var profViewArr = [];

    var allImps = 0;
    var allEventImps = 0;
    var allFavs = 0;
    var allProfViews = 0;

    var range = labels.length;

    for(var i = 0; i < range; i++) {
        var compareDt = new Date(weekStart);
        compareDt.setDate(compareDt.getDate()+i);
        compareDt.setHours(0,0,0,0);

        var dayImps = 0;
        var dayEventImps = 0;
        var dayFavs = 0;
        var dayProfViews = 0;



        for(var d = 0; d < dataset.length; d++) {
            var dt = new Date(Date.parse(dataset[d][0]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                dayImps += parseInt(dataset[d][3]);
                allImps += parseInt(dataset[d][3]);
            }
        }
        dataArr.push(dayImps);

        for(var d = 0; d < eventDataset.length; d++) {
            var dt = new Date(Date.parse(eventDataset[d][4]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                dayEventImps += parseInt(eventDataset[d][0]);
                allEventImps += parseInt(eventDataset[d][0]);
            }
        }
        eventArr.push(dayEventImps);

        for(var f = 0; f < favDataset.length; f++) {
            var dt = new Date(Date.parse(favDataset[f][0]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                dayFavs += 1;
                allFavs += 1;
            }
        }
        favArr.push(dayFavs);

        for(var p = 0; p < profViewDataset.length; p++) {
            var dt = new Date(Date.parse(profViewDataset[p][0]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                dayProfViews += parseInt(profViewDataset[p][2]);
                allProfViews += parseInt(profViewDataset[p][2]);
            }
        }
        profViewArr.push(dayProfViews);
    }

    var datasetPrev = [];
    var eventDatasetPrev = [];
    var favDatasetPrev = [];
    var profViewDatasetPrev = [];

    weekStart.setDate(weekStart.getDate()-range);
    weekEnd.setDate(weekEnd.getDate()-range);

    datasetPrev = getImpForWeek(weekStart, weekEnd);
    eventDatasetPrev = getEventImpForWeek(weekStart, weekEnd);
    favDatasetPrev = getFavForWeek(weekStart, weekEnd);
    profViewDatasetPrev = getProfViewsForWeek(weekStart, weekEnd);

    var allImpsPrev = 0;
    var allEventImpsPrev = 0;
    var allFavsPrev = 0;
    var allProfViewsPrev = 0;

    for(var i = 0; i < range; i++) {
        var compareDt = new Date(weekStart);
        compareDt.setDate(compareDt.getDate()+i);
        compareDt.setHours(0,0,0,0);

        for(var d = 0; d < datasetPrev.length; d++) {
            var dt = new Date(Date.parse(datasetPrev[d][0]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                allImpsPrev += parseInt(datasetPrev[d][3]);
            }
        }

        for(var d = 0; d < eventDatasetPrev.length; d++) {
            var dt = new Date(Date.parse(eventDatasetPrev[d][4]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                allEventImpsPrev += parseInt(eventDatasetPrev[d][0]);
            }
        }

        for(var f = 0; f < favDatasetPrev.length; f++) {
            var dt = new Date(Date.parse(favDatasetPrev[f][0]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                allFavsPrev += 1;
            }
        }

        for(var p = 0; p < profViewDatasetPrev.length; p++) {
            var dt = new Date(Date.parse(profViewDatasetPrev[p][0]));
            dt.setHours(0,0,0,0);

            if(compareDt.getTime() === dt.getTime()) {
                allProfViewsPrev += parseInt(profViewDatasetPrev[p][2]);
            }
        }
    }

    var impChange = 0;
    var eventImpChange = 0;
    var favChange = 0;
    var profViewsChange = 0;

    if(allImps != allImpsPrev && allImpsPrev > 0) {
        impChange = ((allImps - allImpsPrev) / allImpsPrev) * 100;
        impChange = impChange.toFixed(0);
    }
    else if(allImps > allImpsPrev && allImpsPrev == 0) {
        impChange = 100;
    }

    if(allEventImps != allEventImpsPrev && allEventImpsPrev > 0) {
        eventImpChange = ((allEventImps - allEventImpsPrev) / allEventImpsPrev) * 100;
        eventImpChange = eventImpChange.toFixed(0);
    }
    else if(allEventImps > allEventImpsPrev && allEventImpsPrev == 0) {
        eventImpChange = 100;
    }

    if(allFavs != allFavsPrev && allFavsPrev > 0) {
        favChange = ((allFavs - allFavsPrev) / allFavsPrev) * 100;
        favChange = favChange.toFixed(0);
    }
    else if(allFavs > allFavsPrev && allFavsPrev == 0) {
        favChange = 100;
    }

    if(allProfViews != allProfViewsPrev && allProfViewsPrev > 0) {
        profViewsChange = ((allProfViews - allProfViewsPrev) / allProfViewsPrev) * 100;
        profViewsChange = profViewsChange.toFixed(0);
    }
    else if(allProfViews > allProfViewsPrev && allProfViewsPrev == 0) {
        profViewsChange = 100;
    }

    document.getElementById("impCount").innerHTML = allImps;
    document.getElementById("eventImpCount").innerHTML = allEventImps;
    document.getElementById("favCount").innerHTML = allFavs;
    document.getElementById("profViewsCount").innerHTML = allProfViews;

    if(impChange > 0) {
        document.getElementById("impChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + impChange + "%";
        document.getElementById("impChange").style.color = "green";
    }
    else if(impChange < 0) {
        document.getElementById("impChange").innerHTML = "<i class='fa fa-caret-down' style='margin-right:2px;'></i>" + impChange + "%";
        document.getElementById("impChange").style.color = "red";
    }
    else {
        document.getElementById("impChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + impChange + "%";
        document.getElementById("impChange").style.color = "grey";
    }

    if(eventImpChange > 0) {
        document.getElementById("eventImpChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + eventImpChange + "%";
        document.getElementById("eventImpChange").style.color = "green";
    }
    else if(eventImpChange < 0) {
        document.getElementById("eventImpChange").innerHTML = "<i class='fa fa-caret-down' style='margin-right:2px;'></i>" + eventImpChange + "%";
        document.getElementById("eventImpChange").style.color = "red";
    }
    else {
        document.getElementById("eventImpChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + eventImpChange + "%";
        document.getElementById("eventImpChange").style.color = "grey";
    }

    if(favChange > 0) {
        document.getElementById("favChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + favChange + "%";
        document.getElementById("favChange").style.color = "green";
    }
    else if(favChange < 0) {
        document.getElementById("favChange").innerHTML = "<i class='fa fa-caret-down' style='margin-right:2px;'></i>" + favChange + "%";
        document.getElementById("favChange").style.color = "red";
    }
    else {
        document.getElementById("favChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + favChange + "%";
        document.getElementById("favChange").style.color = "grey";
    }

    if(profViewsChange > 0) {
        document.getElementById("profViewsChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + profViewsChange + "%";
        document.getElementById("profViewsChange").style.color = "green";
    }
    else if(profViewsChange < 0) {
        document.getElementById("profViewsChange").innerHTML = "<i class='fa fa-caret-down' style='margin-right:2px;'></i>" + profViewsChange + "%";
        document.getElementById("profViewsChange").style.color = "red";
    }
    else {
        document.getElementById("profViewsChange").innerHTML = "<i class='fa fa-caret-up' style='margin-right:2px;'></i>" + profViewsChange + "%";
        document.getElementById("profViewsChange").style.color = "grey";
    }


    var ctx = document.getElementById('specialImpChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                pointRadius: 0,
                data: dataArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Impressions";
                        else
                            return tooltipItem.yLabel + " Impression";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);

    var ctx = document.getElementById('eventImpChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                pointRadius: 0,
                data: eventArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Impressions";
                        else
                            return tooltipItem.yLabel + " Impression";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);

    var ctx = document.getElementById('favoritesChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                pointRadius: 0,
                data: favArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Favorites";
                        else
                            return tooltipItem.yLabel + " Favorite";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);

    var ctx = document.getElementById('profViewsChart').getContext('2d');
    var chart = new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: labels,
            datasets: [{
                label: '',
                backgroundColor: 'rgba(0, 90, 160, 0.2)',
                borderColor: 'rgb(0, 90, 160)',
                borderWidth: 2,
                pointRadius: 0,
                data: profViewArr
            }]
        },

        // Configuration options go here
        options: {
            legend: {
                display: false
            },
            tooltips: {
                intersect: false,
                mode: 'index',
                axis: 'x',
                displayColors: false,
                callbacks: {
                    title: function(tooltipItem) {
                        return tooltipItem[0].xLabel;
                    },
                    label: function(tooltipItem) {
                        if(parseInt(tooltipItem.yLabel) != 1)
                            return tooltipItem.yLabel + " Views";
                        else
                            return tooltipItem.yLabel + " View";
                    }
                }
            },
            scales: {
                yAxes: [{
                    display: false
                }],
                xAxes: [{
                    display: false
                }]
            },
            layout: {
                padding: {
                    left: 10,
                    right: 10,
                    top: 10,
                    bottom: 10
                }
            }
        }
    });
    myCharts.push(chart);
}

function loadImpressionsChart() {
    backgroundColors = getBackgroundColors();
    labels = getChartWeekLabels();

    document.getElementById("week").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);

    var dateArr = getWeekArr();
    var mon = [];
    var tue = [];
    var wed = [];
    var thu = [];
    var fri = [];
    var sat = [];
    var sun = [];
    var monL = [];
    var tueL = [];
    var wedL = [];
    var thuL = [];
    var friL = [];
    var satL = [];
    var sunL = [];
    var monB = [];
    var tueB = [];
    var wedB = [];
    var thuB = [];
    var friB = [];
    var satB = [];
    var sunB = [];

    for(var i = 0; i < data.length; i++) {
        if(data[i][3] == 0)
            data[i][3] = 0.5;

        if(data[i][1] == dateArr[0]) {
            mon.push(data[i][3]);
            monL.push([data[i][0], data[i][2]]);
            monB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[1]) {
            tue.push(data[i][3]);
            tueL.push([data[i][0], data[i][2]]);
            tueB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[2]) {
            wed.push(data[i][3]);
            wedL.push([data[i][0], data[i][2]]);
            wedB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[3]) {
            thu.push(data[i][3]);
            thuL.push([data[i][0], data[i][2]]);
            thuB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[4]) {
            fri.push(data[i][3]);
            friL.push([data[i][0], data[i][2]]);
            friB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[5]) {
            sat.push(data[i][3]);
            satL.push([data[i][0], data[i][2]]);
            satB.push(backgroundColors[i]);
        }
        else if(data[i][1] == dateArr[6]) {
            sun.push(data[i][3]);
            sunL.push([data[i][0], data[i][2]]);
            sunB.push(backgroundColors[i]);
        }
    }

    var datasets = [];
    datasets.push(mon);
    datasets.push(tue);
    datasets.push(wed);
    datasets.push(thu);
    datasets.push(fri);
    datasets.push(sat);
    datasets.push(sun);

    var labelsArr = [];
    labelsArr.push(monL);
    labelsArr.push(tueL);
    labelsArr.push(wedL);
    labelsArr.push(thuL);
    labelsArr.push(friL);
    labelsArr.push(satL);
    labelsArr.push(sunL);

    var bgArr = [];
    bgArr.push(monB);
    bgArr.push(tueB);
    bgArr.push(wedB);
    bgArr.push(thuB);
    bgArr.push(friB);
    bgArr.push(satB);
    bgArr.push(sunB);

    var weekArr = [];
    weekArr[0] = "mon";
    weekArr[1] = "tue";
    weekArr[2] = "wed";
    weekArr[3] = "thu";
    weekArr[4] = "fri";
    weekArr[5] = "sat";
    weekArr[6] = "sun";

    clearCharts();

    var curr = newDate();
    var day = curr.getDay();
    if(day == 0)
        day = 7;
    day = day - 1;

    for(var j = 0; j < 7; j++) {
        var arr = {};
        arr.data = datasets[j];
        arr.backgroundColor = bgArr[j];
        //arr.borderColor = "rgba(54, 162, 235, 1)";
        arr.borderColor = "black";
        arr.borderWidth = 1;

        var impressionsData = {};
        if(j <= day || week != 0) {
            impressionsData = {
                labels: labelsArr[j],
                datasets: [arr]
            }
        }
        else {
            impressionsData = {
                labels: [],
                datasets: []
            }
        }

        var showYAxisLabels = true;
        if(j > 0)
            showYAxisLabels = false;

        var ctx = document.getElementById(weekArr[j]+'-impressions');
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: impressionsData,
            options: {
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                            stepSize: getStepSize(),
                            max: getMaxY(),
                            display: false
                        }
                    }],
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: labels[j]
                        },
                        ticks: {
                            display: false
                        },
                        gridLines: {
                            color: "rgba(0, 0, 0, 0)",
                        },
                        barThickness: 20
                    }]
                },
                legend: {
                    display: false
                },
                tooltips: {
                    displayColors: false,
                    callbacks: {
                        title: function(tooltipItem) {
                            return tooltipItem[0].xLabel[0];
                        },
                        label: function(tooltipItem) {
                            return tooltipItem.xLabel[1];
                        }
                    }
                },
                hover: {
                    animationDuration: 1
                },
                animation: {
                    duration: 1,
                    onComplete: function () {
                        var chartInstance = this.chart,
                        ctx = chartInstance.ctx;
                        ctx.textAlign = 'center';
                        ctx.fillStyle = "rgba(0, 0, 0, 1)";
                        ctx.textBaseline = 'bottom';

                        this.data.datasets.forEach(function (dataset, i) {
                            var meta = chartInstance.controller.getDatasetMeta(i);
                            meta.data.forEach(function (bar, index) {
                                var data = dataset.data[index];
                                if(data == 0.5)
                                    data = 0;
                                ctx.fillText(data, bar._model.x, bar._model.y - 5);
                            });
                        });
                    }
                }
            }
        });
        myCharts.push(myChart);

        if(j == day) {
            document.getElementById("mobile-day").innerHTML = labels[j] + "<sup>" + getDateSuperscript(labels[j]) + "</sup>";

            ctx = document.getElementById('impressions-mobile');
            var myChart = new Chart(ctx, {
                type: 'bar',
                data: impressionsData,
                options: {
                    maintainAspectRatio: false,
                    scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero: true,
                                stepSize: getStepSize(),
                                max: getMaxY(),
                                display: false
                            }
                        }],
                        xAxes: [{
                            ticks: {
                                display: true,
                                fontSize: 7
                            },
                            gridLines: {
                                color: "rgba(0, 0, 0, 0)",
                            }
                        }]
                    },
                    legend: {
                        display: false
                    },
                    tooltips: {
                        enabled: false
                    },
                    hover: {
                        animationDuration: 1
                    },
                    animation: {
                        duration: 1,
                        onComplete: function () {
                            var chartInstance = this.chart,
                            ctx = chartInstance.ctx;
                            ctx.textAlign = 'center';
                            ctx.fillStyle = "rgba(0, 0, 0, 1)";
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    ctx.fillText(data, bar._model.x, bar._model.y - 5);
                                });
                            });
                        }
                    }
                }
            });
            myCharts.push(myChart);
        }
    }

    document.getElementById("week").disabled = false;
    document.getElementById("prevWeek").disabled = false;

    if(week == 0)
        document.getElementById("nextWeek").disabled = true;
    else
        document.getElementById("nextWeek").disabled = false;
}

function loadImpressionsChartMobile() {
    clearCharts();

    var curr = newDate();
    curr.setDate(curr.getDate()+currentDay);
    curr.setHours(0,0,0,0);
    var d = intMonthToStringAbbr(curr.getMonth()) + " " + curr.getDate();
    document.getElementById("mobile-day").innerHTML = d + "<sup>" + getDateSuperscript(d) + "</sup>";

    var dataArr = [];
    var labelArr = [];
    var bgArr = [];

    for(var i in data) {
        var dDate = new Date(data[i][1]);
        dDate.setHours(0,0,0,0);
        if(dDate.getDate() == curr.getDate() && dDate.getMonth() == curr.getMonth() && dDate.getYear() == curr.getYear()) {
            dataArr.push(data[i][3]);
            labelArr.push([data[i][0], data[i][2]]);
            bgArr.push(backgroundColors[i]);
        }
    }

    var arr = {};
    arr.data = dataArr;
    arr.backgroundColor = bgArr;
    arr.borderColor = "black";
    arr.borderWidth = 1;

    var impressionsData = {
        labels: labelArr,
        datasets: [arr]
    }

    ctx = document.getElementById('impressions-mobile');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: impressionsData,
        options: {
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: getStepSize(),
                        max: getMaxY(),
                        display: false
                    }
                }],
                xAxes: [{
                    ticks: {
                        display: true,
                        fontSize: 7
                    },
                    gridLines: {
                        color: "rgba(0, 0, 0, 0)",
                    }
                }]
            },
            legend: {
                display: false
            },
            tooltips: {
                enabled: false
            },
            hover: {
                animationDuration: 1
            },
            animation: {
                duration: 1,
                onComplete: function () {
                    var chartInstance = this.chart,
                    ctx = chartInstance.ctx;
                    ctx.textAlign = 'center';
                    ctx.fillStyle = "rgba(0, 0, 0, 1)";
                    ctx.textBaseline = 'bottom';

                    this.data.datasets.forEach(function (dataset, i) {
                        var meta = chartInstance.controller.getDatasetMeta(i);
                        meta.data.forEach(function (bar, index) {
                            var data = dataset.data[index];
                            if(data == 0.5)
                                data = 0;
                            ctx.fillText(data, bar._model.x, bar._model.y - 5);
                        });
                    });
                }
            }
        }
    });
    myCharts.push(myChart);

    document.getElementById("mobile-day").disabled = false;
    document.getElementById("prevDay").disabled = false;

    if(currentDay == 0)
        document.getElementById("nextDay").disabled = true;
    else
        document.getElementById("nextDay").disabled = false;
}

function filterWeeklyImp() {
    var specialOrEvent = document.getElementById("filterImpSelect").value;
    var btnShowArchived = document.getElementById("btnShowArchived");
    var showArchived = btnShowArchived.innerHTML.indexOf("Hide Archived") > -1;
    loadTable(specialOrEvent, showArchived);
}

function showArchived() {
    var btnShowArchived = document.getElementById("btnShowArchived");
    if(btnShowArchived.innerHTML.indexOf("Show Archived") > -1) {
        btnShowArchived.innerHTML = "Hide Archived";
        btnShowArchived.className = "form-control btn-primary";
    }
    else {
        btnShowArchived.innerHTML = "Show Archived";
        btnShowArchived.className = "form-control btn-default";
    }
    filterWeeklyImp();
}

function loadTable(specialOrEvent, showArchived) {
    var html = "";
    var htmlMobile = "";

    var curr = newDate(); // get current date
    var first;
    if(curr.getDay() > 0)
        first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    else
        first = curr.getDate() - 7;
    var last = first + 7;

    var weekStart = new Date(curr.setDate(first));
    weekStart.setDate(weekStart.getDate()+1);
    weekStart.setHours(0,0,0,0);

    var currNew = newDate();
    var weekEnd = new Date(currNew.setDate(last));
    weekEnd.setHours(0,0,0,0);

    var tw = intMonthToStringAbbr(weekStart.getMonth()) + " " + weekStart.getDate() + " - " + intMonthToStringAbbr(weekEnd.getMonth()) + " " + weekEnd.getDate();
    weekStart.setDate(weekStart.getDate()-7);
    weekEnd.setDate(weekEnd.getDate()-7);
    var lw = intMonthToStringAbbr(weekStart.getMonth()) + " " + weekStart.getDate() + " - " + intMonthToStringAbbr(weekEnd.getMonth()) + " " + weekEnd.getDate();

    html += "<thead><tr>";
    html += "<th style='padding:10px 18px; border-bottom: 1px solid #111; color:black; font-weight:bolder;'>Published</th>";
    if(showArchived)
        html += "<th style='padding:10px 18px; border-bottom: 1px solid #111; color:black; font-weight:bolder;'>Archived</th>";
    html += "<th style='padding:10px 18px; border-bottom: 1px solid #111; color:black; font-weight:bolder;'>Details</th>";
    html += "<th style='padding:10px 18px; border-bottom: 1px solid #111; color:black; font-weight:bolder;'>Date/Time</th>";
    html += "<th style='padding:10px 0px 10px 18px; border-bottom: 1px solid #111; color:black; font-weight:bolder; min-width:100px;'><p style='border-bottom:1px solid; margin-bottom:5px;'>Impressions</p><p style='font-size:0.8em;'>Last Week</p><p style='color:grey; font-size:0.8em;'>"+lw+"</p></th>";
    html += "<th style='padding:10px 0px; border-bottom: 1px solid #111; color:black; font-weight:bolder; min-width:100px;'><p style='border-bottom:1px solid; margin-bottom:5px;'>&nbsp;</p><p style='font-size:0.8em;'>This Week</p><p style='color:grey; font-size:0.8em;'>"+tw+"</p></th>";
    html += "<th style='padding:10px 0px; border-bottom: 1px solid #111; color:black; font-weight:bolder; min-width:100px;'><p style='border-bottom:1px solid; margin-bottom:5px;'>&nbsp;</p><p style='font-size:0.8em;'>Avg/Week</p><p style='color:grey; font-size:0.8em;'>Since published</p></th>";
    html += "</tr></thead><tbody>";

    htmlMobile += "<thead><tr>";
    htmlMobile += "<th style='padding:10px 15px; border-bottom: 1px solid #111; color:black; font-weight:bolder; font-size:0.8em;'>Details</th>";
    htmlMobile += "<th style='padding:10px 5px; border-bottom: 1px solid #111; color:black; font-weight:bolder; font-size:0.8em;'>Last Week</th>";
    htmlMobile += "<th style='padding:10px 5px; border-bottom: 1px solid #111; color:black; font-weight:bolder; font-size:0.8em;'>This Week</th>";
    htmlMobile += "<th style='padding:10px 5px; border-bottom: 1px solid #111; color:black; font-weight:bolder; font-size:0.8em;'>Average</th>";
    htmlMobile += "</tr></thead><tbody>";

    if(specialOrEvent == "specials") {
        var newData = [];

        for(var i = 0; i < data.length; i++) {
            var inArr = false;

            for(var j = 0; j < newData.length; j++) {
                if((data[i][0] == newData[j][0] && data[i][2] == newData[j][2] && data[i][4] == newData[j][4] &&
                  data[i][8] == newData[j][8] && data[i][9] == newData[j][9]) ||
                  data[i][10] == newData[j][10]) {
                    inArr = true;

                    var curr = new Date(); // get current date
                    var first;
                    if(curr.getDay() > 0)
                        first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                    else
                        first = curr.getDate() - 7;
                    var last = first + 7;

                    var weekStart = new Date(curr.setDate(first));
                    weekStart.setDate(weekStart.getDate()+1);
                    weekStart.setHours(0,0,0,0);

                    var currNew = newDate();
                    var weekEnd = new Date(currNew.setDate(last));
                    weekEnd.setHours(23,59,59,59);

                    var d = new Date(data[i][11]);

                    if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                        newData[j][12] += parseInt(data[i][3]);
                    }

                    weekStart.setDate(weekStart.getDate()-7);
                    weekEnd.setDate(weekEnd.getDate()-7);

                    if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                        newData[j][13] += parseInt(data[i][3]);
                    }

                    weekStart.setDate(weekStart.getDate()-7);
                    weekEnd.setDate(weekEnd.getDate()-7);

                    if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                        newData[j][14] += parseInt(data[i][3]);
                    }

                    newData[j][16]++;
                    newData[j][17] += parseInt(data[i][3]);
                    newData[j][15] = newData[j][17] / newData[j][16];

                    break;
                }
            }

            if(!inArr) {
                var curr = new Date(); // get current date
                var first;
                if(curr.getDay() > 0)
                    first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                else
                    first = curr.getDate() - 7;
                var last = first + 7;

                var weekStart = new Date(curr.setDate(first));
                weekStart.setDate(weekStart.getDate()+1);
                weekStart.setHours(0,0,0,0);

                var currNew = newDate();
                var weekEnd = new Date(currNew.setDate(last));
                weekEnd.setHours(23,59,59,59);

                data[i][12] = 0; //imp this week
                data[i][13] = 0; //imp last week
                data[i][14] = 0; //imp prev week

                var d = new Date(data[i][11]);

                if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                    data[i][12] = parseInt(data[i][3]);
                }

                weekStart.setDate(weekStart.getDate()-7);
                weekEnd.setDate(weekEnd.getDate()-7);

                if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                    data[i][13] = parseInt(data[i][3]);
                }

                weekStart.setDate(weekStart.getDate()-7);
                weekEnd.setDate(weekEnd.getDate()-7);

                if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                    data[i][14] = parseInt(data[i][3]);
                }

                data[i][16] = 1; // count
                data[i][17] = parseInt(data[i][3]); // total
                data[i][15] = data[i][17] / data[i][16]; // average

                newData.push(data[i]);
            }
        }

        // sort by day
        var sorted = false;
        while(!sorted) {
            sorted = true;
            for(var i = 0; i < newData.length-1; i++) {
                var d1 = newData[i][4];
                var d2 = newData[i+1][4];
                if(d1 == 0)
                    d1 = 7;
                if(d2 == 0)
                    d2 = 7;

                if(d1 > d2) {
                    var temp = newData[i];
                    newData[i] = newData[i+1];
                    newData[i+1] = temp;
                    sorted = false;
                }
            }
        }

        var currentDate = "";

        for(var i = 0; i < newData.length; i++) {
            var curr = newDate();
            var d = new Date(newData[i][1]);

            var archived;
            if(newData[i][5] == "Deleted")
                archived = new Date(newData[i][6]);

            if((newData[i][5].indexOf("Active") > -1) ||
               (d <= curr && archived > curr.setDate(curr.getDate()-14) && showArchived)) {

                var dayDate = "";
                if(newData[i][4] == -1)
                    dayDate = newData[i][1];
                else
                    dayDate = intDayToStr(newData[i][4]);

                if(currentDate != dayDate) {
                    html += "<tr style='border-top:1px solid;'>";
                    htmlMobile += "<tr style='border-top:1px solid;'>";
                }
                else {
                    html += "<tr>";
                    htmlMobile += "<tr>";
                }

                // Publish date
                var dtArr = newData[i][9].split(" ");
                html += "<td style='padding: 10px 18px;'><p>" + dtArr[0] + "</p><p style='font-size:0.8em;'>" + dtArr[1] + " " + dtArr[2] + "</p></td>";

                // Archive date
                if(showArchived) {
                    if(newData[i][6] != "") {
                        var arcDtArr = newData[i][6].split(" ");
                        if(arcDtArr.length > 0)
                            html += "<td style='padding: 10px 18px;'><p>" + arcDtArr[0] + "</p><p style='font-size:0.8em;'>" + arcDtArr[1] + " " + arcDtArr[2] + "</p></td>";
                        else
                            html += "<td style='padding: 10px 18px;'></td>";
                    }
                    else {
                        html += "<td style='padding: 10px 18px;'></td>";
                    }
                }

                // Details + event name
                if(newData[i][7] != "")
                    html += "<td style='padding: 10px 18px;'><img class='impIcon' src='assets/img/" + getDealTypeImageByRefId(parseInt(newData[i][8])) + "' style='height:34px; display:inline-flex;' /><p style='display:inline-flex; vertical-align:middle;'>" + newData[i][0] + "</p><p style='font-size:0.8em; margin-left:20px;'><i class='fa fa-calendar fa-fw'></i> " + newData[i][7] + "</p></td>";
                else
                    html += "<td style='padding: 10px 18px;'><img class='impIcon' src='assets/img/" + getDealTypeImageByRefId(parseInt(newData[i][8])) + "' style='height:34px; display:inline-flex;' /><p style='display:inline-flex; vertical-align:middle;'>" + newData[i][0] + "</p></td>";

                // Date
                //if(newData[i][4].split(", ").length != 7)
                //    html += "<td style='padding: 10px 18px;'>" + intDayToStr(newData[i][4]) + "</td>";
                //else
                //    html += "<td style='padding: 10px 18px;'>Everyday</td>";

                html += "<td style='padding: 10px 18px;'><p>" + dayDate + "</p><p style='font-size:0.8em;'>" + newData[i][2] + "</p></td>";

                if(newData[i][4] == -1)
                    currentDate = newData[i][1];
                else
                    currentDate = intDayToStr(newData[i][4]);

                //if(newData[i][4] != -1)
                //   html += "<td style='padding: 10px 18px;'>" + newData[i][4] + "</td>";
                //else
                //    html += "<td style='padding: 10px 18px;'>" + newData[i][1] + "</td>";

                // Time
                //html += "<td style='padding: 10px 18px;'>" + newData[i][2] + "</td>";


                htmlMobile += "<td style='padding: 10px 5px;'>";
                htmlMobile += "<div style='display:inline-block; vertical-align:middle;'><img class='impIcon' src='assets/img/" + getDealTypeImageByRefId(parseInt(newData[i][8])) + "' style='height:34px;' /></div>";
                htmlMobile += "<div style='display:inline-block; vertical-align:middle;'><p style='color:rgb(50,50,50);'>" + newData[i][0] + "</p>";
                if(newData[i][7] != "")
                    htmlMobile += "<p style='font-size:0.8em;'><i class='fa fa-calendar fa-fw'></i> " + newData[i][7] + "</p>";
                htmlMobile += "<p style ='font-size:0.8em;'>" + dayDate + " &middot; " + newData[i][2] + "</p>";

                if(newData[i][6] != "") {
                    var arcDtArr = newData[i][6].split(" ");
                    if(arcDtArr.length > 0) {
                        htmlMobile += "<p style ='font-size:0.7em; color:rgb(235, 52, 52);'>Archived " + arcDtArr[0] + " &middot; " + arcDtArr[1] + " " + arcDtArr[2] + "</p>";
                    }
                }

                htmlMobile += "</div></td>";


                // Impressions Last Week
                var lastweek = parseInt(newData[i][13]);
                var prevweek = parseInt(newData[i][14]);

                var change = 0;
                if(lastweek != prevweek && prevweek != 0)
                    change = ((lastweek - prevweek) / prevweek) * 100;
                else if(lastweek > prevweek && prevweek == 0)
                    change = 100;

                change = change.toFixed(0);

                html += "<td style='padding: 10px 0px 10px 18px;'><span style='color:black; padding-right:5px;'>" + newData[i][13] + "</span>";
                htmlMobile += "<td style='padding: 10px 5px;'><span style='color:black; padding-right:5px;'>" + newData[i][13] + "</span>";

                if(change > 0) {
                    html += "<span style='color:green; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:green;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }
                else if(change < 0) {
                    html += "<span style='color:red; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:red;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                }
                else {
                    html += "<span style='color:grey; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:grey;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }

                // Impressions This Week
                var thisweek = parseInt(newData[i][12]);

                change = 0;
                if(thisweek != lastweek && lastweek != 0)
                    change = ((thisweek - lastweek) / lastweek) * 100;
                else if(thisweek > lastweek && lastweek == 0)
                    change = 100;

                change = change.toFixed(0);

                html += "<td style='padding: 10px 0px;'><span style='color:black; padding-right:5px;'>" + newData[i][12] + "</span>";
                htmlMobile += "<td style='padding: 10px 5px;'><span style='color:black; padding-right:5px;'>" + newData[i][12] + "</span>";

                if(change > 0) {
                    html += "<span style='color:green; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:green;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }
                else if(change < 0) {
                    html += "<span style='color:red; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:red;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                }
                else {
                    html += "<span style='color:grey; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:grey;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }

                // Average Impressions per Week
                var avg = parseFloat(newData[i][15]);
                avg = avg.toFixed(1);
                html += "<td style='padding: 10px 18px 10px 0px; color:black;'>" + avg + "</td>";
                htmlMobile += "<td style='padding: 10px 5px; color:black;'>" + avg + "</td>";

                html += "</tr>";
                htmlMobile += "</tr>";
            }
        }
    }
    else {
        var newEventData = [];

        for(var i = 0; i < eventData.length; i++) {
            var inArr = false;

            for(var j = 0; j < newEventData.length; j++) {
                if(eventData[i][7] == newEventData[j][7]) {
                    inArr = true;

                    var curr = new Date(); // get current date
                    var first;
                    if(curr.getDay() > 0)
                        first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                    else
                        first = curr.getDate() - 7;
                    var last = first + 7;

                    var weekStart = new Date(curr.setDate(first));
                    weekStart.setDate(weekStart.getDate()+1);
                    weekStart.setHours(0,0,0,0);

                    var currNew = newDate();
                    var weekEnd = new Date(currNew.setDate(last));
                    weekEnd.setHours(23,59,59,59);

                    var d = new Date(eventData[i][4]);

                    if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                        newEventData[j][9] += parseInt(eventData[i][0]);
                    }

                    weekStart.setDate(weekStart.getDate()-7);
                    weekEnd.setDate(weekEnd.getDate()-7);

                    if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                        newEventData[j][10] += parseInt(eventData[i][0]);
                    }

                    weekStart.setDate(weekStart.getDate()-7);
                    weekEnd.setDate(weekEnd.getDate()-7);

                    if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                        newEventData[j][11] += parseInt(eventData[i][0]);
                    }

                    newEventData[j][12] += parseInt(eventData[i][0]);

                    break;
                }
            }

            if(!inArr) {
                var curr = new Date(); // get current date
                var first;
                if(curr.getDay() > 0)
                    first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                else
                    first = curr.getDate() - 7;
                var last = first + 7;

                var weekStart = new Date(curr.setDate(first));
                weekStart.setDate(weekStart.getDate()+1);
                weekStart.setHours(0,0,0,0);

                var currNew = newDate();
                var weekEnd = new Date(currNew.setDate(last));
                weekEnd.setHours(23,59,59,59);

                eventData[i][9] = 0; //imp this week
                eventData[i][10] = 0; //imp last week
                eventData[i][11] = 0; //imp prev week
                eventData[i][12] = parseInt(eventData[i][0]); //imp all

                var d = new Date(eventData[i][4]);

                if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                    eventData[i][9] = parseInt(eventData[i][0]);
                }

                weekStart.setDate(weekStart.getDate()-7);
                weekEnd.setDate(weekEnd.getDate()-7);

                if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                    eventData[i][10] = parseInt(eventData[i][0]);
                }

                weekStart.setDate(weekStart.getDate()-7);
                weekEnd.setDate(weekEnd.getDate()-7);

                if(d.getTime() >= weekStart.getTime() && d.getTime() <= weekEnd.getTime()) {
                    eventData[i][11] = parseInt(eventData[i][0]);
                }

                newEventData.push(eventData[i]);
            }
        }

        // sort by day
        var sorted = false;
        while(!sorted) {
            sorted = true;
            for(var i = 0; i < newEventData.length-1; i++) {
                var d1 = newEventData[i][3];
                var d2 = newEventData[i+1][3];
                if(d1 == 0)
                    d1 = 7;
                if(d2 == 0)
                    d2 = 7;

                if(d1 > d2) {
                    var temp = newEventData[i];
                    newEventData[i] = newEventData[i+1];
                    newEventData[i+1] = temp;
                    sorted = false;
                }
            }
        }

        var currentDate = "";

        for(var i = 0; i < newEventData.length; i++) {
            var curr = newDate();
            var d = new Date(newEventData[i][2]);
            var multiday = false;
            var startDate, startTime, endDate, endTime;

            var dayDate = "";
            if(newEventData[i][3] == -1) {
                var s = newEventData[i][1].split(" ");
                var e = newEventData[i][2].split(" ");
                startDate = s[0];
                startTime = s[1] + " " + s[2];
                endDate = e[0];
                endTime = e[1] + " " + e[2];

                if(s[0] != e[0]) {
                    dayDate = s[0] + " - " + e[0];
                    multiday = true;
                }
                else {
                    dayDate = s[0];
                }
            }
            else {
                dayDate = intDayToStr(newEventData[i][3]);
            }

            if(currentDate != dayDate) {
                html += "<tr style='border-top:1px solid;'>";
                htmlMobile += "<tr style='border-top:1px solid;'>";
            }
            else {
                html += "<tr>";
                htmlMobile += "<tr>";
            }

            var archived;
            if(newEventData[i][8] != "")
                archived = new Date(newEventData[i][8]);

            var ended;
            if(archived == undefined && d.getTime() < curr.getTime())
                ended = new Date(d.getTime());

            //if((archived == undefined) ||
            //   ((archived > curr.setDate(curr.getDate()-14) || d < curr) && archived != undefined && showArchived)) {

            //if((archived == undefined) ||
            //   (((archived != undefined || (archived == undefined && d < curr)) && showArchived))) {

            if((archived == undefined && ended == undefined) || ((archived != undefined || ended != undefined) && showArchived)) {


                // Publish date
                var dtArr = newEventData[i][5].split(" ");
                html += "<td style='padding: 10px 18px;'><p>" + dtArr[0] + "</p><p style='font-size:0.8em;'>" + dtArr[1] + " " + dtArr[2] + "</p></td>";

                // Archive date
                if(showArchived) {
                    if(newEventData[i][8] != "") {
                        var arcDtArr = newEventData[i][8].split(" ");
                        if(arcDtArr.length > 0)
                            html += "<td style='padding: 10px 18px;'><p>" + arcDtArr[0] + "</p><p style='font-size:0.8em;'>" + arcDtArr[1] + " " + arcDtArr[2] + "</p></td>";
                        else
                            html += "<td style='padding: 10px 18px;'></td>";
                    }
                    else {
                        html += "<td style='padding: 10px 18px;'></td>";
                    }
                }

                // Details
                html += "<td style='padding: 10px 18px;'><i class='fa fa-calendar fa-fw fa-lg impIcon' style='display:inline-flex; margin-left:10px; margin-right:5px; color:rgb(0,90,160);'></i><p style='display:inline-flex; vertical-align:middle;'>" + newEventData[i][7] + "</p></td>";

                // Date
                if(!multiday)
                    html += "<td style='padding: 10px 18px;'><p>" + dayDate + "</p><p style='font-size:0.8em;'>" + newEventData[i][6] + "</p></td>";
                else
                    html += "<td style='padding: 10px 18px;'><div style='display:inline-block'><p>" + startDate + "</p><p style='font-size:0.8em;'>" + startTime + "</p></div><div style='display:inline-block; padding:10px;'><p>-</p></div><div style='display:inline-block'><p>" + endDate + "</p><p style='font-size:0.8em;'>" + endTime + "</p></div></td>";

                if(newEventData[i][3] == -1)
                    currentDate = newEventData[i][1];
                else
                    currentDate = intDayToStr(newEventData[i][3]);

                // Time
                //html += "<td style='padding: 10px 18px;'>" +  + "</td>";


                htmlMobile += "<td style='padding: 10px 5px;'>";
                htmlMobile += "<div style='display:inline-block; vertical-align:middle;'><i class='fa fa-calendar fa-fw fa-lg impIcon' style='display:inline-flex; margin-left:10px; margin-right:5px; color:rgb(0,90,160);'></i></div>";
                htmlMobile += "<div style='display:inline-block; vertical-align:middle;'><p style='color:rgb(50,50,50)'>" + newEventData[i][7] + "</p>";
                if(!multiday)
                    htmlMobile +="<p style='font-size:0.8em; line-height:1;'>" + dayDate + "</p><p style='font-size:0.7em; line-height:1;'>" + newEventData[i][6] + "</p>";
                else
                    htmlMobile +="<div style='display:inline-block'><p style ='font-size:0.8em; line-height:1;'>" + startDate + "</p><p style ='font-size:0.7em; line-height:1;'>" + startTime + "</p></div><div style='display:inline-block; padding:0 10px;'><p style ='font-size:0.8em; line-height:1;'>-</p><p style ='font-size:0.7em; line-height:1;'>&nbsp;</p></div><div style='display:inline-block; line-height:1;'><p style ='font-size:0.8em; line-height:1;'>" + endDate + "</p><p style ='font-size:0.7em;'>" + endTime + "</p></div>";

                if(newEventData[i][8] != "") {
                    var arcDtArr = newEventData[i][8].split(" ");
                    if(arcDtArr.length > 0) {
                        htmlMobile += "<p style ='font-size:0.7em; color:rgb(235, 52, 52); line-height:1;'>Archived " + arcDtArr[0] + " &middot; " + arcDtArr[1] + " " + arcDtArr[2] + "</p>";
                    }
                }
                else if(archived == undefined && d < curr) {
                    if(!multiday)
                        htmlMobile += "<p style ='font-size:0.7em; color:rgb(235, 52, 52); line-height:1; margin-top:2px;'>Ended</p>";
                    else
                        htmlMobile += "<p style ='font-size:0.7em; color:rgb(235, 52, 52); line-height:1; margin-top:-2px;'>Ended</p>";
                }

                htmlMobile += "</div></td>";

                // Impressions Last Week
                var lastweek = parseInt(newEventData[i][10]);
                var prevweek = parseInt(newEventData[i][11]);

                var change = 0;
                if(lastweek != prevweek && prevweek != 0)
                    change = ((lastweek - prevweek) / prevweek) * 100;
                else if(lastweek > prevweek && prevweek == 0)
                    change = 100;

                change = change.toFixed(0);

                html += "<td style='padding: 10px 0px 10px 18px;'><span style='color:black; padding-right:5px;'>" + lastweek + "</span>";
                htmlMobile += "<td style='padding: 10px 5px;'><span style='color:black; padding-right:5px;'>" + lastweek + "</span>";

                if(change > 0) {
                    html += "<span style='color:green; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:green;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }
                else if(change < 0) {
                    html += "<span style='color:red; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:red;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                }
                else {
                    html += "<span style='color:grey; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:grey;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }

                // Impressions This Week
                var thisweek = parseInt(newEventData[i][9]);

                change = 0;
                if(thisweek != lastweek && lastweek != 0)
                    change = ((thisweek - lastweek) / lastweek) * 100;
                else if(thisweek > lastweek && lastweek == 0)
                    change = 100;

                change = change.toFixed(0);

                html += "<td style='padding: 10px 0px;'><span style='color:black; padding-right:5px;'>" + thisweek + "</span>";
                htmlMobile += "<td style='padding: 10px 5px;'><span style='color:black; padding-right:5px;'>" + thisweek + "</span>";

                if(change > 0) {
                    html += "<span style='color:green; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:green;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }
                else if(change < 0) {
                    html += "<span style='color:red; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:red;'><i class='fa fa-caret-down' style='margin-right:2px;'></i>" + Math.abs(change) + "%</span></td>";
                }
                else {
                    html += "<span style='color:grey; font-size:0.8em; padding-left:5px; margin-top:3px;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                    htmlMobile += "<span class='mobileChange' style='color:grey;'><i class='fa fa-caret-up' style='margin-right:2px;'></i>" + change + "%</span></td>";
                }

                // Average Impressions per Week
                var totalImp = parseInt(newEventData[i][12]);
                var weeks = weeksBetween(new Date(dtArr[0]), curr);

                var avg = totalImp / weeks;
                avg = avg.toFixed(1);
                html += "<td style='padding: 10px 18px 10px 0px; color:black;'>" + avg + "</td>";
                htmlMobile += "<td style='padding: 10px 5px; color:black;'>" + avg + "</td>";

                html += "</tr>";
                htmlMobile += "</tr>";
            }
        }
    }

    html += "</tbody>";
    htmlMobile += "</tbody>";

    document.getElementById("specialsAnalyticsTable").innerHTML = html;
    document.getElementById("specialsAnalyticsTableMobile").innerHTML = htmlMobile;
}

function loadCalTable() {
    labels = getChartWeekLabels();

    document.getElementById("week").innerHTML = labels[0] + getDateSuperscript(labels[0]) + " - " + labels[6] + getDateSuperscript(labels[6]);

    var table = document.getElementById("calTable");

    var tLen = table.rows.length;
    for(var t = 0; t < tLen; t++) {
        table.deleteRow(0);
    }

    // data
    var dataSet = [];

    var curr = newDate(); // get current date
    var first;
    if(curr.getDay() > 0)
        first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
    else
        first = curr.getDate() - 7;
    var last = first + 7; // last day is the first day + 6

    var weekStart = new Date(curr.setDate(first));
    weekStart.setDate(weekStart.getDate()+1);
    weekStart.setHours(0,0,0,0);

    var currNew = newDate();
    var weekEnd = new Date(currNew.setDate(last));
    weekEnd.setHours(0,0,0,0);

    if(week != 0) {
        weekStart.setDate(weekStart.getDate() + (week * 7));
        weekEnd.setDate(weekEnd.getDate() + (week * 7));
    }
    else {
        curr = newDate();
        curr.setHours(0,0,0,0);
        if(weekEnd > curr) {
           weekEnd = curr;
        }
    }

    var dataset = getImpForWeek(weekStart, weekEnd);

    dataset = dataset.sort(DatasetComparator);

    var sorted = false;
    while(!sorted) {
        sorted = true;
        for(var d = 0; d < dataset.length - 1; d++) {
            var dur1 = dataset[d][1];
            var s1 = getDateFromDuration(dur1, 0);
            var e1 = getDateFromDuration(dur1, 1);
            var dur2 = dataset[d+1][1];
            var s2 = getDateFromDuration(dur2, 0);
            var e2 = getDateFromDuration(dur2, 1);

            if(s1.getTime() > s2.getTime() || (s1.getTime() == s2.getTime() && e1.getTime() > e2.getTime())) {
                var temp = dataset[d];
                dataset[d] = dataset[d+1];
                dataset[d+1] = temp;
                sorted = false;
            }
        }
    }

    var r = table.insertRow(0);
    //r.style.borderLeft = "1px solid gainsboro";
    //r.style.borderRight = "1px solid gainsboro";

    for(var i = 0; i < 7; i++) {
        var cell = r.insertCell(i);
        cell.style.verticalAlign = "bottom";
        cell.style.border = 0;

        curr = newDate();
        curr.setHours(0,0,0.0,0);
        var today = new Date(weekStart);
        today.setDate(today.getDate()+i);
        today.setHours(0,0,0,0,0);

        if(curr.getTime() == today.getTime()) {
            cell.style.backgroundColor = "rgba(0,200,0,0.2)";
        }
        else if(today.getTime() > curr.getTime()) {
            cell.style.backgroundColor = "rgba(192,192,192,0.2)";
        }

        var dayHtml = "";
        var dayImps = 0;

        for(var d = 0; d < dataset.length; d++) {
            var exclusive = false;
            var day = parseInt(dataset[d][5]);

            if(day == -1) {
                var ahDate = new Date(Date.parse(dataset[d][0]));
                day = ahDate.getDay();
                if(dataset[d][2].indexOf("fa-calendar") == -1)
                    exclusive = true;
            }
            day -= 1;
            if(day == -1)
                day = 6;

            if(day == i) {
                dayImps += dataset[d][3];
                if(dayHtml == "") {
                    if(exclusive) {
                        dayHtml = "<div class='calBubble'><div class='calExclusive'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                    else {
                        dayHtml = "<div class='calBubble'><div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                }
                else {
                    if(exclusive) {
                        dayHtml = dayHtml + "<div class='calExclusive'>" + dataset[d][2] + "</span><br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                    else {
                        dayHtml = dayHtml + "<div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
                    }
                }
            }
            if(d == dataset.length - 1 && dayHtml != "") {
                dayHtml = dayHtml + "</div>";
                cell.innerHTML = "<p class='calImps'>"+dayImps+"</p>"+dayHtml;
            }
        }
    }

    // headers
    var row = table.insertRow(1);
    row.style.textAlign = "center";

    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);

    cell1.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Monday</p><p style='font-size:0.8em'>" + labels[0] + getDateSuperscript(labels[0]) + "</p></div>";
    cell2.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Tuesday</p><p style='font-size:0.8em'>" + labels[1] + getDateSuperscript(labels[1]) + "</p></div>";
    cell3.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Wednesday</p><p style='font-size:0.8em'>" + labels[2] + getDateSuperscript(labels[2]) + "</p></div>";
    cell4.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Thursday</p><p style='font-size:0.8em'>" + labels[3] + getDateSuperscript(labels[3]) + "</p></div>";
    cell5.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Friday</p><p style='font-size:0.8em'>" + labels[4] + getDateSuperscript(labels[4]) + "</p></div>";
    cell6.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Saturday</p><p style='font-size:0.8em'>" + labels[5] + getDateSuperscript(labels[5]) + "</p></div>";
    cell7.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>Sunday</p><p style='font-size:0.8em'>" + labels[6] + getDateSuperscript(labels[6]) + "</p></div>";

    cell1.style.width = "14.28%";
    cell2.style.width = "14.28%";
    cell3.style.width = "14.28%";
    cell4.style.width = "14.28%";
    cell5.style.width = "14.28%";
    cell6.style.width = "14.28%";
    cell7.style.width = "14.28%";

    document.getElementById("week").disabled = false;
    document.getElementById("prevWeek").disabled = false;

    if(week == 0)
        document.getElementById("nextWeek").disabled = true;
    else
        document.getElementById("nextWeek").disabled = false;
}

function loadCalTableMobile() {
    labels = getChartWeekLabels();

    var curr = newDate();
    curr.setDate(curr.getDate()+currentDay);
    curr.setHours(0,0,0,0);
    var d = intMonthToStringAbbr(curr.getMonth()) + " " + curr.getDate();
    document.getElementById("mobile-day").innerHTML = d + "<sup>" + getDateSuperscript(d) + "</sup>";

    var table = document.getElementById("calTableMobile");

    var tLen = table.rows.length;
    for(var t = 0; t < tLen; t++) {
        table.deleteRow(0);
    }

    // data
    var dataset = getImpForWeek(curr, curr);

    dataset = dataset.sort(DatasetComparator);

    var sorted = false;
    while(!sorted) {
        sorted = true;
        for(var d = 0; d < dataset.length - 1; d++) {
            var dur1 = dataset[d][1];
            var s1 = getDateFromDuration(dur1, 0);
            var e1 = getDateFromDuration(dur1, 1);
            var dur2 = dataset[d+1][1];
            var s2 = getDateFromDuration(dur2, 0);
            var e2 = getDateFromDuration(dur2, 1);

            if(s1.getTime() > s2.getTime() || (s1.getTime() == s2.getTime() && e1.getTime() > e2.getTime())) {
                var temp = dataset[d];
                dataset[d] = dataset[d+1];
                dataset[d+1] = temp;
                sorted = false;
            }
        }
    }

    var r = table.insertRow(0);
    //r.style.borderLeft = "1px solid gainsboro";
    //r.style.borderRight = "1px solid gainsboro";

    var cell = r.insertCell(0);
    cell.style.verticalAlign = "bottom";
    cell.style.border = 0;

    var dayHtml = "";
    var dayImps = 0;

    for(var d = 0; d < dataset.length; d++) {
        var exclusive = false;
        var day = parseInt(dataset[d][5]);

        if(day == -1) {
            var ahDate = new Date(Date.parse(dataset[d][0]));
            day = ahDate.getDay();
            exclusive = true;
        }
        day -= 1;
        if(day == -1)
            day = 6;

        dayImps += dataset[d][3];
        if(dayHtml == "") {
            if(exclusive) {
                dayHtml = "<div class='calBubble'><div class='calExclusive'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
            else {
                dayHtml = "<div class='calBubble'><div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
        }
        else {
            if(exclusive) {
                dayHtml = dayHtml + "<div class='calExclusive'>" + dataset[d][2] + "</span><br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
            else {
                dayHtml = dayHtml + "<div class='calDaily'>" + dataset[d][2] + "<br/><p class='calTime'>" + dataset[d][1] + "</p></div>";
            }
        }
        if(d == dataset.length - 1 && dayHtml != "") {
            dayHtml = dayHtml + "</div>";
            cell.innerHTML = "<p class='calImps'>"+dayImps+"</p>"+dayHtml;
        }
    }

    if(dataset.length == 0) {
        cell.innerHTML = "<p style='text-align:center;'>No Specials</p>";
    }


    // headers
    var row = table.insertRow(1);
    row.style.textAlign = "center";

    var cell1 = row.insertCell(0);

    cell1.innerHTML = "<div class='calDayLabel'><p style='font-weight:900'>" + intDayToStr(curr.getDay()) + "</p></div>";

    cell1.style.width = "14.28%";

    document.getElementById("mobile-day").disabled = false;
    document.getElementById("prevDay").disabled = false;

    if(currentDay == 0)
        document.getElementById("nextDay").disabled = true;
    else
        document.getElementById("nextDay").disabled = false;
}

function DatasetComparator(a, b) {
   if (a[2] < b[2]) return -1;
   if (a[2] > b[2]) return 1;
   return 0;
}

function getDateFromDuration(dur, startEnd) {
    var s1;

    if(startEnd == 0) {
        s1 = dur.substring(0, dur.indexOf(" - "));
    }
    else if(startEnd == 1) {
        s1 = dur.substring(dur.indexOf(" - ")+3);
    }

    var h1 = parseInt(s1.substring(0, s1.indexOf(":")));
    var m1 = s1.substring(s1.indexOf(":")+1);
    m1 = m1.replace("AM", "").replace("PM", "");
    if(s1.indexOf("AM") > -1 && h1 == 12) {
        h1 = 0;
    }
    else if(s1.indexOf("PM") > -1 && h1 != 12) {
        h1 = h1 + 12;
    }
    if(h1 < 10) {
        h1 = "0" + h1;
    }
    s1 = new Date("01-01-2020 " + h1 + ":" + m1 + ":00");
    return s1;
}

function getImpForWeek(s, e) {
    var dataSet = [];
    for(var i = 0; i < data.length; i++) {
        var ds = [];
        var d = new Date(data[i][1]);

        if(d.getTime() >= s.getTime() && d.getTime() <= e.getTime()) {
            ds.push(data[i][1]);
            ds.push(data[i][2]);

            if(data[i][7] != "")
                ds.push(data[i][0] + "<p style='font-size:0.8em;'><i class='fa fa-calendar fa-fw'></i> " + data[i][7]) + "</p>";
            else
                ds.push(data[i][0]);

            if(data[i][3] == 0.5)
                ds.push(0)
            else
                ds.push(data[i][3]);

            if(data[i][5] == "Deleted")
                ds.push("<p style='line-height:1.3; font-size:0.6em'>" + data[i][5] + "<br/><span style='font-size:1.7em'>" + data[i][6]) + "</span></p>";
            else
                ds.push(data[i][5]);

            ds.push(data[i][4]);
            dataSet.push(ds);
        }
    }

    return dataSet;
}

function getEventImpForWeek(s, e) {
    var dataSet = [];
    for(var i = 0; i < eventData.length; i++) {
        var ds = [];
        var d = new Date(eventData[i][1]);

        if(parseInt(eventData[i][3]) > -1 || (d >= s && d <= e)) {
            ds.push(eventData[i][0]);
            ds.push(eventData[i][1]);
            ds.push(eventData[i][2]);
            ds.push(eventData[i][3]);
            ds.push(eventData[i][4]);
            dataSet.push(ds);
        }
    }
    return dataSet;
}

function getFavForWeek(s, e) {
    var dataSet = [];

    for(var i = 0; i < favData.length; i++) {
        var ds = [];
        var d = new Date(favData[i][0]);

        if(d >= s && d <= e) {
            ds.push(favData[i][0]);
            ds.push(d.getDay());
            dataSet.push(ds);
        }
    }
    return dataSet;
}

function getProfViewsForWeek(s, e) {
    var dataSet = [];

    for(var i = 0; i < profViewData.length; i++) {
        var ds = [];
        var d = new Date(profViewData[i][0]);

        if(d >= s && d <= e) {
            ds.push(profViewData[i][0]);
            ds.push(d.getDay());
            ds.push(profViewData[i][1]);
            dataSet.push(ds);
        }
    }
    return dataSet;
}

function changeDateRange(range) {
    clearCharts();

    var curr = newDate(); // get current date

    if(range == "today") {
        var first = curr.getDate();
        var last = first + 1;
        document.getElementById("impChangeRange").innerHTML = "yesterday";
        document.getElementById("eventImpChangeRange").innerHTML = "yesterday";
        document.getElementById("favChangeRange").innerHTML = "yesterday";
        document.getElementById("profViewsChangeRange").innerHTML = "yesterday";
        document.getElementById("todayNote").style.display = "inline-block";
    }
    else if(range == "yesterday") {
        var first = curr.getDate() - 1;
        var last = first + 1;
        document.getElementById("impChangeRange").innerHTML = "previous day";
        document.getElementById("eventImpChangeRange").innerHTML = "previous day";
        document.getElementById("favChangeRange").innerHTML = "previous day";
        document.getElementById("profViewsChangeRange").innerHTML = "previous day";
        document.getElementById("todayNote").style.display = "none";
    }
    else if(range == "last7Days") {
        var first = curr.getDate() - 7;
        var last = first + 7;
        document.getElementById("impChangeRange").innerHTML = "previous 7 days";
        document.getElementById("eventImpChangeRange").innerHTML = "previous 7 days";
        document.getElementById("favChangeRange").innerHTML = "previous 7 days";
        document.getElementById("profViewsChangeRange").innerHTML = "previous 7 days";
        document.getElementById("todayNote").style.display = "none";
    }
    else if(range == "last28Days") {
        var first = curr.getDate() - 28;
        var last = first + 28;
        document.getElementById("impChangeRange").innerHTML = "previous 28 days";
        document.getElementById("eventImpChangeRange").innerHTML = "previous 28 days";
        document.getElementById("favChangeRange").innerHTML = "previous 28 days";
        document.getElementById("profViewsChangeRange").innerHTML = "previous 28 days";
        document.getElementById("todayNote").style.display = "none";
    }

    var weekStart = new Date(curr.setDate(first));
    weekStart.setHours(0,0,0,0);

    var currNew = newDate();
    var weekEnd = new Date(currNew.setDate(last));
    weekEnd.setHours(0,0,0,0);

    loadCharts(weekStart, weekEnd);
}

function weekPicker(e) {
    e.preventDefault();
    $('#week').datepicker('show');
}

function dayPicker(e) {
    e.preventDefault();
    $('#mobile-day').datepicker('show');
}

function nextWeek(e) {
    e.preventDefault();
    week = week + 1;

    //loadImpressionsChart();
    //loadCalTable();

    var d = newDate();
    d.setDate(d.getDate() + (week * 7));
    var startDate;
    if(d.getDay() > 0)
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay()+1);
    else
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 6);
    $('#week').data({date: getYYYYMMDD(startDate)});
    $('#week').datepicker('update');

    if(week == 0)
        document.getElementById("nextWeek").disabled = true;
}

function prevWeek(e) {
    e.preventDefault();
    week = week - 1;

    //loadImpressionsChart();
    //loadCalTable();

    var d = newDate();
    d.setDate(d.getDate() + (week * 7));
    var startDate;
    if(d.getDay() > 0)
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - d.getDay()+1);
    else
        startDate = new Date(d.getFullYear(), d.getMonth(), d.getDate() - 6);
    $('#week').data({date: getYYYYMMDD(startDate)});
    $('#week').datepicker('update');

    document.getElementById("nextWeek").disabled = false;
}

function nextDay(e) {
    e.preventDefault();
    currentDay = currentDay + 1;

    //loadImpressionsChartMobile();

    var d = newDate();
    d.setDate(d.getDate()+currentDay);
    $('#mobile-day').data({date: getYYYYMMDD(d)});
    $('#mobile-day').datepicker('update');

    if(currentDay == 0)
        document.getElementById("nextDay").disabled = true;
}

function prevDay(e) {
    e.preventDefault();
    currentDay = currentDay - 1;

    //loadImpressionsChartMobile();

    var d = newDate();
    d.setDate(d.getDate()+currentDay);
    $('#mobile-day').data({date: getYYYYMMDD(d)});
    $('#mobile-day').datepicker('update');

    document.getElementById("nextDay").disabled = false;
}

function clearCharts() {
    for(var c in myCharts) {
        myCharts[c].destroy();
    }
}

function getBackgroundColors() {
    var backgroundColor = [];

    if(data.length > 0) {
        var low = data[0][3];
        var high = data[0][3];
        var len = data.length;

        for(var i = 1; i < len; i++) {
            if(data[i][3] < low)
                low = data[i][3];
            if(data[i][3] > high)
                high = data[i][3];
        }

        var diff = high - low;
        if(diff > 0) {
            for(var j = 0; j < data.length; j++) {
                if(data[j][4] == "-1") {
                    backgroundColor.push('rgba(244, 149, 60, 0.9)');
                }
                else {
                    var opacity = (data[j][3] - low) / diff;
                    if(opacity < 0.1)
                        opacity = 0.1;
                    else if(opacity > 0.9)
                        opacity = 0.9;
                    backgroundColor.push('rgba(54, 162, 235, ' + opacity + ')');
                }
            }
        }
        else {
            for(var k = 0; k < data.length; k++) {
                if(data[k][4] == "-1")
                    backgroundColor.push('rgba(244, 149, 60, 0.9)');
                else
                    backgroundColor.push('rgba(54, 162, 235, 0.6)');
            }
        }
    }

    return backgroundColor;
}

function getChartWeekLabels() {
    var weekLabels = [];

    // var addDays = 1;
    // if(week != 0)
    //     addDays = week * 7 + 1;

    var curr = newDate();
    curr.setHours(0,0,0,0);
    var first = 0;

    first = curr.getDate() + (week * 7);

    //if(curr.getDay() != 0)
    //    first = curr.getDate() - curr.getDay() + addDays;
    //else
    //    first = curr.getDate() - 7 + addDays;

    for(var i = 0; i <= 6; i++) {
        var d = new Date(curr.setDate(first));
        var dStr = intMonthToStringAbbr(d.getMonth()) + " " + d.getDate();
        weekLabels.push(dStr);
        first = first + 1;
        curr = newDate();
        curr.setHours(0,0,0,0);
    }

    return weekLabels;
}

function getLabels(start, end) {
    var weekLabels = [];

    var s = new Date(start);
    var e = new Date(end);

    if(s < e) {
        while(s < e) {
            var dStr = intMonthToStringAbbr(s.getMonth()) + " " + s.getDate();
            weekLabels.push(dStr);
            s.setDate(s.getDate()+1);
        }
    }
    else if(e < s) {
        while(e < s) {
            var dStr = intMonthToStringAbbr(e.getMonth()) + " " + e.getDate();
            weekLabels.push(dStr);
            e.setDate(e.getDate()+1);
        }
    }
    else if(e == s) {
        var dStr = intMonthToStringAbbr(s.getMonth()) + " " + s.getDate();
        weekLabels.push(dStr);
    }

    return weekLabels;
}

function getWeekArr() {
    var weekArr = [];

    var addDays = 1;
    if(week != 0)
        addDays = week * 7 + 1;

    var curr = newDate();
    curr.setHours(0,0,0,0);
    var first = curr.getDate() - curr.getDay() + addDays;
    for(var i = 0; i <= 6; i++) {
        var d = new Date(curr.setDate(first));
        var month = d.getMonth()+1;
        if(month < 10)
            month = "0" + month;
        var date = d.getDate();
        if(date < 10)
            date = "0"+date;
        var year = d.getYear()+1900;
        weekArr.push(month+"/"+date+"/"+year);
        first = first + 1;
        curr = newDate();
        curr.setHours(0,0,0,0);
    }

    return weekArr;
}

function getStepSize() {
    var max = getMax();
    var stepSize = max / 6;
    var mod;
    if(stepSize < 1)
        return 1;
    else if(stepSize <= 2)
        return 2;
    else if(stepSize < 5)
        mod = 5;
    else
        mod = 10;
    var m = stepSize % mod;
    return Math.round(stepSize + mod - m);
}

function getMax() {
    var max = 1;
    for(var i = 0; i < data.length; i++)
        if(data[i][3] > max)
            max = data[i][3];
    return max;
}

function getMaxY() {
    var step = getStepSize();
    var max = getMax();
    var maxY = max + (max / 10);
    var mod = maxY % step;
    if(mod != 0)
        return Math.round(maxY + (step - mod));
    else
        return maxY;
}

function intMonthToStringAbbr(month) {
    var m = new Array(7);
    m[0] = "Jan";
    m[1] = "Feb";
    m[2] = "Mar";
    m[3] = "Apr";
    m[4] = "May";
    m[5] = "Jun";
    m[6] = "Jul";
    m[7] = "Aug";
    m[8] = "Sep";
    m[9] = "Oct";
    m[10] = "Nov";
    m[11] = "Dec";
    return m[month];
}

function intDayToStr(day) {
    var m = new Array(7);
    m[1] = "Monday";
    m[2] = "Tuesday";
    m[3] = "Wednesday";
    m[4] = "Thursday";
    m[5] = "Friday";
    m[6] = "Saturday";
    m[0] = "Sunday";
    return m[day];
}

function getYYYYMMDD(d) {
    var year = d.getYear()+1900;
    var month = d.getMonth()+1;
    if(month < 10)
        month = "0" + month;
    var day = d.getDate();
    if(day < 10)
        day = "0" + day;
   return year + "-" + month + "-" + day;
}

// Returns the ISO week of the date.
Date.prototype.getWeek = function() {
  var date = new Date(this.getTime());
  date.setHours(0, 0, 0, 0);
  // Thursday in current week decides the year.
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  // January 4 is always in week 1.
  var week1 = new Date(date.getFullYear(), 0, 4);
  // Adjust to Thursday in week 1 and count number of weeks from date to week1.
  return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
                        - 3 + (week1.getDay() + 6) % 7) / 7);
}

// Returns the four-digit year corresponding to the ISO week of the date.
Date.prototype.getWeekYear = function() {
  var date = new Date(this.getTime());
  date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
  return date.getFullYear();
}

function hideMessage() {
    document.getElementById("msg02042020").style.display = "none";
    localStorage.setItem("hideMsg", "true");
}

function getDealTypeImageByRefId(refId) {
    switch(refId) {
        case 200: return 'beer-icon.png';
        case 202: return 'shot-icon.png';
        case 204: return 'wine-icon.png';
        case 206: return 'mixed-drink-icon.png';
        case 208: return 'margarita-icon.png';
        case 210: return 'martini-icon.png';
        case 212: return 'tumbler-icon.png';
        case 214: return 'beerbottle.png';
        case 216: return 'beercan.png';
        case 251: return 'burger-icon.png';
        case 253: return 'appetizer-icon.png';
        case 255: return 'pizza-icon.png';
        case 257: return 'taco-icon.png';
        case 259: return 'sushi.png';
        case 261: return 'bowl.png';
        case 263: return 'chickenwing.png';
        default: return '';
    }
}

function weeksBetween(a, b) {
    var diff =(b.getTime() - a.getTime()) / 1000;
    diff /= (60 * 60 * 24 * 7);
    diff = Math.abs(Math.round(diff));
    if(diff > 0)
        return diff;
    else
        return 1;
}
