var mysql = require('mysql')
const util = require('util')
// var async = require('async');

var PRODUCTION_DB = 'appyhour';
var TEST_DB = 'appyhour_dev';

var state = {
  pool: null,
  mode: null,
};

exports.connect = function(done) {
  /* LOCAL */
  /*
  state.pool = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password: 'Chip1993',
    database: PRODUCTION_DB
  });
  */

  /* AWS */
  // state.pool = mysql.createPool({
  //   host: 'appyhour.cy6klgwavms2.us-east-2.rds.amazonaws.com',
  //   user: 'appyhourdevteam',
  //   password: 'Appyhour2017dev',
  //   database: PRODUCTION_DB
  // });

  // PUBLIC
  state.pool = mysql.createPool({
    host: '45.77.47.190',
    user: 'appyhour',
    password: 'appyhour',
    database: PRODUCTION_DB,
    port: 3307
  });

  state.mode = 'mode_production';
  done();
}

exports.get = function() {
  return state.pool;
}

exports.getQueryAsync = function() {
  return util.promisify(state.pool.query).bind(state.pool)
}

exports.insert = function(data, done) {
  var pool = state.pool;
  if (!pool) return done(new Error('Missing database connection.'));

  var table = Object.keys(data)[0];
  var obj = data[table];

  if(!obj.created_at)
    obj.created_at = new Date();
  if(!obj.updated_at)
    obj.updated_at = new Date();
  //obj.created_by = pull the logged in user;

  pool.query('INSERT INTO ' + table + ' SET ?', obj, function (err, res) {
    if(err) done(err);
    done(null, res);
  });
}

exports.update = function(data, done) {
  var pool = state.pool;
  if (!pool) return done(new Error('Missing database connection.'));

  var table = Object.keys(data)[0];
  var obj = data[table];

  var keys = Object.keys(obj);
  var values = keys.map(function(key) { return obj[key] });

  var query = 'UPDATE ' + table + ' SET ' + keys.join(' = ?, ') + ' = ?, updated_at = sysdate() WHERE ' + keys[0] + ' = ' + values[0];
  pool.query(query, values, function (err, res) {
    if(err) done(err);
    done(null, res);
  });
}

exports.upsert = function(data, done) {
  var pool = state.pool;
  if (!pool) return done(new Error('Missing database connection.'));

  var table = Object.keys(data)[0];
  var obj = data[table];

  if(!obj.created_at)
    obj.created_at = new Date();
  if(!obj.updated_at)
    obj.updated_at = new Date();

  pool.query('REPLACE INTO ' + table + ' SET ?', obj, function (err, res) {
    if(err) done(err);
    done(null, res);
  });
}

/*
exports.update = function(data, whereCol, whereVal, done) {
  var pool = state.pool;
  if (!pool) return done(new Error('Missing database connection.'));

  var table = Object.keys(data)[0];
  var obj = data[table];

  var keys = Object.keys(obj);
  var values = keys.map(function(key) { return obj[key] });

  var query = 'UPDATE ' + table + ' SET ' + keys.join(' = ?, ') + ' = ?, updated_at = sysdate() WHERE ' + whereCol + ' = ' + whereVal;
  pool.query(query, values, function (err, res) {
    if(err) done(err);
    done(null, res);
  });
}*/