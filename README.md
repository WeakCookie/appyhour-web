# appyhourweb
AppyHour Website

*After cloning this repo, be sure to run command: npm install*

_______________________________________________________________________________

# Run App Locally

	1. In app.js, comment out enforce.https code

	2. Run command: heroku local web

	3. App runs on localhost:5000/

_______________________________________________________________________________

# Applications
	
## app.appyhourmobile.com
	* Establishment/Supplier login page

## app.appyhourmobile.com/dashboard
	* Dashboard for establishments, tiered features depending on access level (i.e. Freemium or $99.99/mo Premium)
	    + Manage specials/events
	    + View analytics
        + Update business info
        + Update personal (login) info
        + Manage subscription/payment options
        + Add/update managers to control account

## app.appyhourmobile.com/supplierdashboard
	* Dashboard for suppliers/advertisers
        + Manage campaigns to feature products/events
        + Update account info
        + Add/update managers to control account

## app.appyhourmobile.com/admin
	* Internal application for content management
	    + Manage owners, establishments, specials, events
	    + Manage suppliers and campaigns
	    + Import establishments based on zip code
	    + Audit establishments

## app.appyhourmobile.com/metrics
	* Internal metrics page for AppyHour KPIs
	    + Total establishments per city
	    + Total paying establishments per city
	    + Monthly income
	    + Weekly downloads
	    + Active users (In-Progress)

_______________________________________________________________________________

# Deploy to Production

	1. Update Stripe keys in:
	  * app.js
	  * controllers/admin.js
	  * controllers/analytics.js
	  * controllers/home.js
	  * controllers/info.js
	  * public/javascripts/infojs.js

	2. Uncomment enforce.https code in app.js

	3. Run command: git push heroku master

_______________________________________________________________________________