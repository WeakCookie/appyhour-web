const { login, supplierLogin, userExists, supplierExists, secretPasswordMatches, userHasChangedPassword, supplierHasChangedPassword } = require('../controllers/v2/signIn')
const { updateUser } = require('../controllers/v2/user')
const { updateSupplier } = require('../controllers/v2/supplier')
const express = require("express")
const validate = require('../middlewares/validate');
const authValidation = require('../validations/auth.validation');
const auth = require('../middlewares/auth');
const router = express.Router()
const { loadSpecial } = require('../controllers/home');
const { getChartData, cacheMiddleware } = require('../controllers/v2/analytics')

function pingWithAuth(_, res) {
  res.end('Success')
}


router.get('/loadSpecial', auth(), loadSpecial);
router.get('/ping-with-auth', auth(), pingWithAuth);
router.post('/login', validate(authValidation.login), login);
router.post('/supplierLogin', validate(authValidation.login), supplierLogin);
router.post('/userExists', validate(authValidation.hasUserName), userExists)
router.post('/supplierExists', validate(authValidation.hasUserName), supplierExists)
router.post('/userHasChangedPassword', validate(authValidation.hasUserName), userHasChangedPassword)
router.post('/supplierHasChangedPassword', validate(authValidation.hasUserName), supplierHasChangedPassword)
router.post('/secretPasswordMatches', validate(authValidation.checkSecretPassword), secretPasswordMatches)
router.post('/updateSupplier', validate(authValidation.updateUser), updateSupplier)
router.post('/updateUser', validate(authValidation.updateUser), updateUser)

router.post('/analytics', cacheMiddleware, getChartData)

module.exports = router
