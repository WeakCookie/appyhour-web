const db = require('../db.js')

exports.getAllProfiles = function (done) {
    var queryStr = 'select * from appyhour.PROFILE';
    db.get().query(queryStr, function (err, rows) {
        if (err) return done(err);
        done(null, rows);
    });
}

exports.getAllAsync = async function () {
    const queryStr = 'SELECT * FROM PROFILE'
    const query = db.getQueryAsync()
    const rows = await query(queryStr)
    return rows
}
