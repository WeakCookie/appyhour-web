var db = require('../db.js');
const util = require('util')

exports.getAll = function (done) {
  db.get().query('select a.*, a.created_at as est_created_at, a.updated_at as est_updated_at, f.*, c.user_id, c.stripe_id, c.claimed_at, ' +
    'c.claim_email_sent_at, e.image_location as imageUrl, g.ref_code as state, h.* ' +
    'from appyhour.ESTABLISHMENT a ' +
    'left join appyhour.USER_ESTABLISHMENT b on a.establishment_id = b.establishment_id ' +
    'left join appyhour.USER c on b.user_id = c.user_id ' +
    'left join appyhour.ESTABLISHMENT_IMAGE d on a.establishment_id = d.establishment_id ' +
    'left join appyhour.IMAGE e on d.image_id = e.image_id ' +
    'left join appyhour.ADDRESS f on a.address_id = f.address_id ' +
    'left join appyhour.REF g on f.state_code_ref_id = g.ref_id ' +
    'left join appyhour.HOURS h on a.hours_id = h.hours_id ' +
    'order by a.name', function (err, rows) {
      if (err) return done(err);
      done(null, rows);
    })
}

exports.getAllByEstablishmentId = function (establishmentId, done) {
  db.get().query('SELECT * FROM ESTABLISHMENT WHERE establishment_id = ?', establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllEstablishments = function (done) {
  db.get().query('SELECT a.establishment_id, b.topic ' +
    'FROM appyhour.ESTABLISHMENT a ' +
    'JOIN appyhour.ESTABLISHMENT_QUEUE b ON a.establishment_id = b.establishment_id', function (err, rows) {
      if (err) return done(err);
      done(null, rows);
    });
}

exports.getAllEstablishmentsAsync = async function (pagination) {
  let baseQuery = 'SELECT a.establishment_id, b.topic ' +
    'FROM appyhour.ESTABLISHMENT a ' +
    'JOIN appyhour.ESTABLISHMENT_QUEUE b ON a.establishment_id = b.establishment_id'
  const pool = db.get()
  const query = util.promisify(pool.query).bind(pool)
  if (pagination) {
    baseQuery = `${baseQuery} ORDER BY a.establishment_id ASC LIMIT ?, ?`
  }

  const rows = await query(baseQuery, pagination)
  return rows
}

exports.countAllEstablishmentsAsync = async function () {
  const pool = db.get()
  const query = util.promisify(pool.query).bind(pool)
  const count = await query('SELECT COUNT(a.establishment_id) ' +
    'FROM appyhour.ESTABLISHMENT a ' +
    'JOIN appyhour.ESTABLISHMENT_QUEUE b ON a.establishment_id = b.establishment_id')
  return +count[0]['COUNT(a.establishment_id)']
}

exports.getAllByUserId = function (userId, done) {
  var query = 'select a.* ' +
    'from ESTABLISHMENT a ' +
    'join USER_ESTABLISHMENT b on a.establishment_id = b.establishment_id ' +
    'join USER c on b.user_id = c.user_id ' +
    'where c.user_id = ?';
  db.get().query(query, userId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByEventId = function (eventId, done) {
  var query = 'select a.* ' +
    'from ESTABLISHMENT a ' +
    'join ESTABLISHMENT_EVENT b on a.establishment_id = b.establishment_id ' +
    'join EVENT c on b.event_id = c.event_id ' +
    'where c.event_id = ?';
  db.get().query(query, eventId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}


exports.getAllByOwnerId = function (userId, done) {
  var query = 'select a.* ' +
    'from ESTABLISHMENT a ' +
    'join USER_ESTABLISHMENT b on a.establishment_id = b.establishment_id ' +
    'join USER c on b.user_id = c.user_id ' +
    'where c.user_id = ?';
  db.get().query(query, userId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByOwnerIdAsync = async function (userId) {
  const queryString = 'select a.* ' +
    'from ESTABLISHMENT a ' +
    'join USER_ESTABLISHMENT b on a.establishment_id = b.establishment_id ' +
    'join USER c on b.user_id = c.user_id ' +
    'where c.user_id = ?';
  const query = db.getQueryAsync()
  const rows = await query(queryString, userId)
  return rows[0]
}

exports.getEverythingByEstablishmentId = function (userId, done) {
  var query = 'select * ' +
    'from ESTABLISHMENT a ' +
    'join ADDRESS d on a.address_id = d.address_id ' +
    'where a.establishment_id = ?';
  db.get().query(query, userId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getByIdAsync = async function (userId) {
  const queryString = 'select * ' +
              'from ESTABLISHMENT a ' +
              'join ADDRESS d on a.address_id = d.address_id ' +
              'where a.establishment_id = ?'
  const query = db.getQueryAsync()
  const rows = await query(queryString, userId)
  return rows[0]
}

exports.getEverythingByUserId = function (userId, done) {
  var query = 'select a.*, d.* ' +
    'from ESTABLISHMENT a ' +
    'join USER_ESTABLISHMENT b on a.establishment_id = b.establishment_id ' +
    'join USER c on b.user_id = c.user_id ' +
    'join ADDRESS d on a.address_id = d.address_id ' +
    'where c.user_id = ?';
  db.get().query(query, userId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByUsername = function (username, done) {
  var query = 'select a.* ' +
    'from ESTABLISHMENT a ' +
    'join USER_ESTABLISHMENT b on a.establishment_id = b.establishment_id ' +
    'join USER c on b.user_id = c.user_id ' +
    'where c.username = ?';
  db.get().query(query, username, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function (establishment, done) {
  db.insert(establishment, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function (establishment, done) {
  db.update(establishment, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function (establishmentId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT WHERE establishment_id = ?', establishmentId, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
