exports.newDate = function(timezone) {
	const today = new Date()
	if(timezone == "PST")
		today.setHours(today.getHours() - 7)
	else if(timezone == "MST" || timezone == "PDT")
		today.setHours(today.getHours() - 6)
	else if(timezone == "CST" || timezone == "MDT")
		today.setHours(today.getHours() - 5)
	else if(timezone == "EST" || timezone == "CDT")
		today.setHours(today.getHours() - 4)
	else if(timezone == "EDT")
		today.setHours(today.getHours() - 3)
	else
		today.setHours(today.getHours() - 5)
	return today
}

Date.prototype.addHoursDST = function(timezone) {
	if(timezone == "PST")
		this.setHours(this.getHours() + 8);
	else if(timezone == "MST" || timezone == "PDT")
		this.setHours(this.getHours() + 7);
	else if(timezone == "CST" || timezone == "MDT")
		this.setHours(this.getHours() + 6);
	else if(timezone == "EST" || timezone == "CDT")
		this.setHours(this.getHours() + 5);
	else if(timezone == "EDT")
		this.setHours(this.getHours() + 4);
	else
		this.setHours(this.getHours() + 6);
}

Date.prototype.subtractHoursDST = function(timezone) {
	if(timezone == "PST")
		this.setHours(this.getHours() - 8);
	else if(timezone == "MST" || timezone == "PDT")
		this.setHours(this.getHours() - 7);
	else if(timezone == "CST" || timezone == "MDT")
		this.setHours(this.getHours() - 6);
	else if(timezone == "EST" || timezone == "CDT")
		this.setHours(this.getHours() - 5);
	else if(timezone == "EDT")
		this.setHours(this.getHours() - 4);
	else
		this.setHours(this.getHours() - 6);
}

/*
exports.newDate = function(timezone) {
	var today = new Date();
	if(timezone == "PST")
		today.setHours(today.getHours() - 8);
	else if(timezone == "MST" || timezone == "PDT")
		today.setHours(today.getHours() - 7);
	else if(timezone == "CST" || timezone == "MDT")
		today.setHours(today.getHours() - 6);
	else if(timezone == "EST" || timezone == "CDT")
		today.setHours(today.getHours() - 5);
	else if(timezone == "EDT")
		today.setHours(today.getHours() - 4);
	else
		today.setHours(today.getHours() - 6);
	return today;
}

Date.prototype.addHoursDST = function(timezone) {
	if(timezone == "PST")
		this.setHours(this.getHours() + 8);
	else if(timezone == "MST" || timezone == "PDT")
		this.setHours(this.getHours() + 7);
	else if(timezone == "CST" || timezone == "MDT")
		this.setHours(this.getHours() + 6);
	else if(timezone == "EST" || timezone == "CDT")
		this.setHours(this.getHours() + 5);
	else if(timezone == "EDT")
		this.setHours(this.getHours() + 4);
	else
		this.setHours(this.getHours() + 6);
}

Date.prototype.subtractHoursDST = function(timezone) {
	if(timezone == "PST")
		this.setHours(this.getHours() - 8);
	else if(timezone == "MST" || timezone == "PDT")
		this.setHours(this.getHours() - 7);
	else if(timezone == "CST" || timezone == "MDT")
		this.setHours(this.getHours() - 6);
	else if(timezone == "EST" || timezone == "CDT")
		this.setHours(this.getHours() - 5);
	else if(timezone == "EDT")
		this.setHours(this.getHours() - 4);
	else
		this.setHours(this.getHours() - 6);
}
*/




exports.dateEST = function() {
	var today = new Date();
	today.setHours(today.getHours() - 5);
	return today;
}
