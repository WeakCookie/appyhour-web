var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM IMAGE', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByImageId = function(imageId, done) {
  db.get().query('SELECT * FROM IMAGE WHERE image_id = ?', imageId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getProfilePicForEstablishmentId = function(establishmentId, done) {
  var query = 'select a.* '+
              'from appyhour.IMAGE a '+
              'join appyhour.ESTABLISHMENT_IMAGE b on a.image_id = b.image_id '+
              'join appyhour.ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'where a.image_type_ref_id = (select ref_id from appyhour.REF where ref_code = "Profile Pic") '+
              'and c.establishment_id = ?';

  db.get().query(query, establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(image, done) {
  db.insert(image, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(image, done) {
  db.update(image, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(imageId, done) {
  db.get().query('DELETE FROM IMAGE WHERE image_id = ?', imageId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}