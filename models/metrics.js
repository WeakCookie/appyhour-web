var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM METRICS ORDER BY start_date desc', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllInstalls = function(done) {
  db.get().query('SELECT * FROM METRICS WHERE metric = "installations" ORDER BY start_date desc', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByMetricsId = function(metricsId, done) {
  db.get().query('SELECT * FROM REF WHERE metrics_id = ?', metricsId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(ref, done) {
  db.insert(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(ref, done) {
  db.update(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(metricsId, done) {
  db.get().query('DELETE FROM REF WHERE metrics_id = ?', metricsId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}