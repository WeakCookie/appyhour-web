var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM USER_ESTABLISHMENT', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByUserEstablishmentId = function(userEstablishmentId, done) {
  db.get().query('SELECT * FROM USER_ESTABLISHMENT WHERE user_establishment_id = ?', userEstablishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByEstablishmentId = function(establishmentId, done) {
  db.get().query('SELECT * FROM USER_ESTABLISHMENT WHERE establishment_id = ?', establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(userEstablishment, done) {
  db.insert(userEstablishment, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(userEstablishment, done) {
  db.update(userEstablishment, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(userEstablishmentId, done) {
  db.get().query('DELETE FROM USER_ESTABLISHMENT WHERE user_establishment_id = ?', userEstablishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByUserId = function(userId, done) {
  db.get().query('DELETE FROM USER_ESTABLISHMENT WHERE user_id = ?', userId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEstablishmentId = function(establishmentId, done) {
  db.get().query('DELETE FROM USER_ESTABLISHMENT WHERE establishment_id = ?', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}