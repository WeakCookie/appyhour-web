var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM SUPPLIER_MANAGER', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.insert = function(suppliermanager, done) {
  db.insert(suppliermanager, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(suppliermanager, done) {
  db.update(suppliermanager, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(supplier_manager_id, done) {
  db.get().query('DELETE FROM SUPPLIER_MANAGER WHERE supplier_manager_id = ?', supplier_manager_id, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByManagerId = function(manager_id, done) {
  db.get().query('DELETE FROM SUPPLIER_MANAGER WHERE manager_id = ?', manager_id, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}