var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM ESTABLISHMENT_IMAGE', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByEstablishmentImageId = function(establishmentImageId, done) {
  db.get().query('SELECT * FROM ESTABLISHMENT_IMAGE WHERE establishment_image_id = ?', establishmentImageId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByEstablishmentId = function(establishmentId, done) {
  db.get().query('SELECT * FROM ESTABLISHMENT_IMAGE WHERE establishment_id = ?', establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(establishmentImage, done) {
  db.insert(establishmentImage, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(establishmentImage, done) {
  db.update(establishmentImage, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(establishmentImageId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_IMAGE WHERE establishment_image_id = ?', establishmentImageId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEstablishmentId = function(establishmentId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_IMAGE WHERE establishment_id = ?', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}