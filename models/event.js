var db = require('../db.js');

exports.getAll = function (done) {
  db.get().query('SELECT * FROM EVENT', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByEventId = function (eventId, done) {
  var query = 'select * from EVENT where event_id = ?';
  db.get().query(query, eventId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllGrouped = function (done) {
  var query = 'select a.*, c.establishment_id, group_concat(a.event_id order by a.event_id SEPARATOR ",") AS event_ids, ' +
    'group_concat(a.day order by a.day SEPARATOR ",") AS days ' +
    'from appyhour.EVENT a ' +
    'left join appyhour.ESTABLISHMENT_EVENT b on a.event_id = b.event_id ' +
    'left join appyhour.ESTABLISHMENT c on b.establishment_id = c.establishment_id ' +
    'group by a.title, a.details, a.start_time, a.end_time';

  db.get().query(query, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentId = function (establishmentId, done) {
  var query = 'select c.event_id, c.title, c.details as event_details, c.day, c.start_time, c.end_time, e.establishment_id, ' +
    'e.name as establishment, f.latitude, f.longitude, c.created_at, ' +
    '(select h.image_location from appyhour.ESTABLISHMENT_IMAGE g ' +
    'join appyhour.IMAGE h on g.image_id = h.image_id ' +
    'where g.establishment_id = e.establishment_id and h.image_type_ref_id = 303) as imageUrl ' +
    'from appyhour.EVENT c ' +
    'join appyhour.ESTABLISHMENT_EVENT d on c.event_id = d.event_id ' +
    'join appyhour.ESTABLISHMENT e on d.establishment_id = e.establishment_id ' +
    'join appyhour.ADDRESS f on e.address_id = f.address_id ' +
    'where e.establishment_id = ? ' +
    'order by c.end_time, c.event_id';

  db.get().query(query, establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdGroupRecurring = function (establishmentId, done) {
  var query = 'select c.event_id, c.title, c.details as event_details, group_concat(c.day order by c.day SEPARATOR ",") AS days, c.start_time, c.end_time, e.establishment_id, ' +
    'e.name as establishment, f.latitude, f.longitude, ' +
    '(select h.image_location from appyhour.ESTABLISHMENT_IMAGE g ' +
    'join appyhour.IMAGE h on g.image_id = h.image_id ' +
    'where g.establishment_id = e.establishment_id and h.image_type_ref_id = 303) as imageUrl ' +
    'from appyhour.EVENT c ' +
    'join appyhour.ESTABLISHMENT_EVENT d on c.event_id = d.event_id ' +
    'join appyhour.ESTABLISHMENT e on d.establishment_id = e.establishment_id ' +
    'join appyhour.ADDRESS f on e.address_id = f.address_id ' +
    'where e.establishment_id = ? ' +
    'group by title, event_details, start_time, end_time ' +
    'order by c.end_time, c.event_id';

  db.get().query(query, establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllForRecurring = function (parameters, done) {
  var query = 'select * from appyhour.EVENT where title = ? and day = ?;';

  db.get().query(query, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getEventIdForRecurring = function (parameters, done) {
  var query = 'select event_id from appyhour.EVENT where title = ? and details = ? and day = ?;';

  db.get().query(query, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getOneTimeEventsForWeek = function (parameters, done) {
  var query = 'select * from appyhour.EVENT where day = -1 and start_time > date(?) and start_time < date(?)';

  db.get().query(query, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdForYear = function (parameters, done) {
  var queryStr = 'SELECT e.* ' +
    'FROM EVENT e ' +
    'JOIN ESTABLISHMENT_EVENT ee ON e.event_id = ee.event_id ' +
    'JOIN ESTABLISHMENT es ON ee.establishment_id = es.establishment_id ' +
    'WHERE es.establishment_id = ? AND ' +
    '(e.day > -1 OR (e.day = -1 AND e.created_at >= ?)) ' +
    'ORDER BY e.created_at desc';

  db.get().query(queryStr, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

// parameters = [establishmentId, date that special create after (YYYY-MM-DD)]
exports.getAllByEstablishmentIdForYearAsync = async function (parameters) {
  var queryStr = 'SELECT e.* ' +
    'FROM EVENT e ' +
    'JOIN ESTABLISHMENT_EVENT ee ON e.event_id = ee.event_id ' +
    'JOIN ESTABLISHMENT es ON ee.establishment_id = es.establishment_id ' +
    'WHERE es.establishment_id = ? AND ' +
    '(e.day > -1 OR (e.day = -1 AND e.created_at >= ?)) ' +
    'ORDER BY e.created_at desc';

  const query = db.getQueryAsync()
  const rows = await query(queryStr, parameters)
  return rows
}

exports.getRecurringEventsForEstId = function (estId, done) {
  var query = 'select c.event_id, c.title, c.details as event_details, c.day, c.start_time, c.end_time, e.establishment_id, ' +
    'e.name as establishment, f.latitude, f.longitude, ' +
    '(select h.image_location from appyhour.ESTABLISHMENT_IMAGE g ' +
    'join appyhour.IMAGE h on g.image_id = h.image_id ' +
    'where g.establishment_id = e.establishment_id and h.image_type_ref_id = 303) as imageUrl ' +
    'from appyhour.EVENT c ' +
    'join appyhour.ESTABLISHMENT_EVENT d on c.event_id = d.event_id ' +
    'join appyhour.ESTABLISHMENT e on d.establishment_id = e.establishment_id ' +
    'join appyhour.ADDRESS f on e.address_id = f.address_id ' +
    'where c.day != -1 and e.establishment_id = ? ' +
    'order by c.end_time, c.event_id';

  db.get().query(query, estId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.search = function (search, done) {
  var query = 'select distinct b.special_id, b.price, b.price_type_ref_id, b.details as special_details, b.start_time as special_start_time, b.end_time as special_end_time, c.event_id, c.title, ' +
    'c.details as event_details, c.day, c.start_time, c.end_time, e.establishment_id, e.name as establishment, ' +
    'f.latitude, f.longitude, i.ref_desc as iconUrl, ' +
    '(select h.image_location from appyhour.ESTABLISHMENT_IMAGE g ' +
    'join appyhour.IMAGE h on g.image_id = h.image_id ' +
    'where g.establishment_id = e.establishment_id and h.image_type_ref_id = 303) as imageUrl ' +
    'from appyhour.EVENT c ' +
    'join appyhour.ESTABLISHMENT_EVENT d on c.event_id = d.event_id ' +
    'join appyhour.ESTABLISHMENT e on d.establishment_id = e.establishment_id ' +
    'join appyhour.ADDRESS f on e.address_id = f.address_id ' +
    'join appyhour.USER_ESTABLISHMENT j on e.establishment_id = j.establishment_id ' +
    'join appyhour.USER k on j.user_id = k.user_id ' +
    'left join appyhour.EVENT_SPECIAL a on a.event_id = c.event_id ' +
    'left join appyhour.SPECIAL b on a.special_id = b.special_id ' +
    'left join appyhour.REF i on b.deal_type_ref_id = i.ref_id ' +
    'where k.show_specials = 1 and ' +
    'e.show_in_app = 1 and ' +
    '( ' +
    'match(c.title, c.details) against ("?") ' +
    'or ' +
    'c.title like "%?%" ' +
    'or ' +
    'c.details like "%?%" ' +
    ') ' +
    'and ' +
    '( ' +
    '  ( ' +
    '    c.day = -1 and ' +
    '    c.end_time >= date_sub(now(), INTERVAL 5 HOUR) and ' +
    '    DATE(c.start_time) = CURDATE() ' +
    '  ) ' +
    '  or ' +
    '  c.day > -1 ' +
    ') ' +
    'order by TIME(c.end_time), c.event_id, b.details';

  db.get().query(query, [search, search, search], function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}




exports.insert = function (ev, done) {
  db.insert(ev, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function (ev, done) {
  db.update(ev, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function (eventId, done) {
  db.get().query('DELETE FROM EVENT WHERE event_id = ?', eventId, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
