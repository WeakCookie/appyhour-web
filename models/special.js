var db = require('../db.js');

exports.getAll = function(done) {
  var query = 'select distinct a.*, c.name as establishment, d.ref_desc as iconUrl, e.latitude, e.longitude, i.title as eventName, c.* '+
              'from appyhour.SPECIAL a '+
              'join appyhour.ESTABLISHMENT_SPECIAL b on a.special_id = b.special_id '+
              'join appyhour.ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'join appyhour.REF d on a.deal_type_ref_id = d.ref_id '+
              'join appyhour.ADDRESS e on c.address_id = e.address_id '+
              'join appyhour.USER_ESTABLISHMENT f on c.establishment_id = f.establishment_id '+
              'join appyhour.USER g on f.user_id = g.user_id '+
              'left join appyhour.EVENT_SPECIAL h on a.special_id = h.special_id '+
              'left join appyhour.EVENT i on h.event_id = i.event_id '+
              'where ( '+
              '  ( '+
              '    a.day = -1 and '+
              '    ( '+
              '       a.end_time >= date_sub(now(), INTERVAL 5 HOUR) or '+
              '       DATE(a.start_time) = CURDATE() '+
              '    ) '+
              '  ) '+
              '  or '+
              '  a.day > -1 '+
              ') '+
              'order by a.day, a.start_time';

  db.get().query(query, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllGrouped = function(done) {
  var query = 'select distinct a.*, c.name as establishment, d.ref_desc as iconUrl, e.latitude, e.longitude, i.event_id, i.title as eventName, c.*, '+
              'group_concat(a.special_id order by a.special_id SEPARATOR ",") AS special_ids, group_concat(a.day order by a.special_id SEPARATOR ",") AS days '+
              'from appyhour.SPECIAL a '+
              'join appyhour.ESTABLISHMENT_SPECIAL b on a.special_id = b.special_id '+
              'join appyhour.ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'join appyhour.REF d on a.deal_type_ref_id = d.ref_id '+
              'join appyhour.ADDRESS e on c.address_id = e.address_id '+
              'join appyhour.USER_ESTABLISHMENT f on c.establishment_id = f.establishment_id '+
              'join appyhour.USER g on f.user_id = g.user_id '+
              'left join appyhour.EVENT_SPECIAL h on a.special_id = h.special_id '+
              'left join appyhour.EVENT i on h.event_id = i.event_id '+
              'where ( '+
              '  ( '+
              '    a.day = -1 and '+
              '    ( '+
              '       a.end_time >= date_sub(now(), INTERVAL 5 HOUR) or '+
              '       DATE(a.start_time) = CURDATE() '+
              '    ) '+
              '  ) '+
              '  or '+
              '  a.day > -1 '+
              ') '+
              'group by c.establishment_id, a.price, a.price_type_ref_id, a.details, a.deal_type_ref_id, a.start_time, a.end_time, a.dinein, a.carryout';

  db.get().query(query, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllFromDate = function(d, done) {
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.*, e.name as establishment ';
  queryStr = queryStr + 'FROM SPECIAL s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'WHERE s.created_at > date(?) ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  db.get().query(queryStr, d, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentId = function(establishment_id, done) {
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.*, evs.event_id, ev.title ';
  queryStr = queryStr + 'FROM SPECIAL s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_SPECIAL evs ON s.special_id = evs.special_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT ev ON evs.event_id = ev.event_id '
  queryStr = queryStr + 'WHERE e.establishment_id = ? ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  db.get().query(queryStr, establishment_id, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdForWeek = function(parameters,  done) {
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.*, evs.event_id ';
  queryStr = queryStr + 'FROM SPECIAL s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_SPECIAL evs ON s.special_id = evs.special_id ';
  queryStr = queryStr + 'WHERE e.establishment_id = ? AND evs.event_id is null AND ';
  queryStr = queryStr + '((s.start_time >= ? AND s.end_time <= ?) OR ';
  queryStr = queryStr + 's.day > -1) ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  db.get().query(queryStr, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdForYear = function(parameters,  done) {
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.*, ev.title as event, ea.title as eventArc ';
  queryStr = queryStr + 'FROM SPECIAL s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_SPECIAL evs ON s.special_id = evs.special_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT ev ON evs.event_id = ev.event_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_ARCHIVE ea ON evs.event_id = ea.event_id ';
  queryStr = queryStr + 'WHERE e.establishment_id = ? AND ';
  queryStr = queryStr + '(s.day > -1 OR (s.day = -1 AND s.created_at >= ?)) ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  db.get().query(queryStr, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

// parameters = [establishmentId, date that special create after (YYYY-MM-DD)]
exports.getAllByEstablishmentIdForYearAsync = async function(parameters) {
  let queryStr = ""
  queryStr = queryStr + 'SELECT s.*, ev.title as event, ea.title as eventArc ';
  queryStr = queryStr + 'FROM SPECIAL s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_SPECIAL evs ON s.special_id = evs.special_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT ev ON evs.event_id = ev.event_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_ARCHIVE ea ON evs.event_id = ea.event_id ';
  queryStr = queryStr + 'WHERE e.establishment_id = ? AND ';
  queryStr = queryStr + '(s.day > -1 OR (s.day = -1 AND s.created_at >= ?)) ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  const query = db.getQueryAsync()
  const rows = await query(queryStr, parameters)
  return rows
}

exports.getTodaysSpecialsByEstablishmentId = function(establishment_id, done) {
  var today = new Date();
  var n = today.getDay();
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.* ';
  queryStr = queryStr + 'FROM SPECIAL s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'WHERE s.day in (-1, ' + n + ')';
  queryStr = queryStr + 'AND e.establishment_id = ?';
  // queryStr = queryStr + 'ORDER BY s.end_time desc';

  db.get().query(queryStr, establishment_id, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getDailySpecialsByEstablishmentId = function(establishment_id, done) {
  var today = new Date();
  var n = today.getDay();
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.* ';
  queryStr = queryStr + 'FROM SPECIAL s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'WHERE s.day != -1';
  queryStr = queryStr + 'AND e.establishment_id = ?';
  // queryStr = queryStr + 'ORDER BY s.end_time desc';

  db.get().query(queryStr, establishment_id, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getTodaysEventsByEventIds = function(event_ids, done) {
  var queryStr = '';
  queryStr = 'select b.special_id, b.price, b.price_type_ref_id, b.details as special_details, b.deal_type_ref_id, b.start_time as special_start_time, b.end_time as special_end_time, '+
             'c.event_id, c.title, c.details as event_details, c.start_time, c.end_time, e.name as establishment, '+
             'f.latitude, f.longitude, i.ref_desc as iconUrl, '+
             '(select h.image_location from appyhour.ESTABLISHMENT_IMAGE g '+
             'join appyhour.IMAGE h on g.image_id = h.image_id '+
             'where g.establishment_id = e.establishment_id and h.image_type_ref_id = 303) as imageUrl '+
             'from appyhour.EVENT_SPECIAL a '+
             'join appyhour.SPECIAL b on a.special_id = b.special_id '+
             'join appyhour.EVENT c on a.event_id = c.event_id '+
             'join appyhour.ESTABLISHMENT_EVENT d on c.event_id = d.event_id '+
             'join appyhour.ESTABLISHMENT e on d.establishment_id = e.establishment_id '+
             'join appyhour.ADDRESS f on e.address_id = f.address_id '+
             'join appyhour.REF i on b.deal_type_ref_id = i.ref_id '+
             'where c.event_id in ' + event_ids +
             ' order by c.end_time, c.event_id';
  db.get().query(queryStr, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllBySpecialId = function(specialId, done) {
  db.get().query('SELECT * FROM SPECIAL WHERE special_id = ?', specialId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllBySpecialIdGrouped = function(specialId, done) {
  var query = 'select *, group_concat(special_id order by special_id SEPARATOR ",") AS special_ids, group_concat(day order by special_id SEPARATOR ",") AS days '+
              'from appyhour.SPECIAL '+
              'where price = (select price from appyhour.SPECIAL where special_id = ?) and '+
              'price_type_ref_id = (select price_type_ref_id from appyhour.SPECIAL where special_id = ?) and '+
              'details = (select details from appyhour.SPECIAL where special_id = ?) and '+
              'deal_type_ref_id = (select deal_type_ref_id from appyhour.SPECIAL where special_id = ?) and '+
              'start_time = (select start_time from appyhour.SPECIAL where special_id = ?) and '+
              'end_time = (select end_time from appyhour.SPECIAL where special_id = ?) and '+
              'dinein = (select dinein from appyhour.SPECIAL where special_id = ?) and '+
              'carryout = (select carryout from appyhour.SPECIAL where special_id = ?) '+
              'group by price, price_type_ref_id, details, deal_type_ref_id, start_time, end_time, dinein, carryout';
  db.get().query(query, [specialId, specialId, specialId, specialId, specialId, specialId, specialId, specialId, specialId], function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}



exports.getAllByEventId = function(eventId, done) {
  var queryStr = 'select s.* '+
                 'from appyhour.SPECIAL s '+
                 'join appyhour.EVENT_SPECIAL es on s.special_id = es.special_id '+
                 'join appyhour.EVENT e on es.event_id = e.event_id '+
                 'where e.event_id = ?';
  db.get().query(queryStr, eventId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEventIdGrouped = function(eventId, done) {
  var queryStr = 'select s.*, group_concat(DATE(s.start_time) order by s.start_time SEPARATOR ",") AS special_dates '+
                 'from appyhour.SPECIAL s '+
                 'join appyhour.EVENT_SPECIAL es on s.special_id = es.special_id '+
                 'join appyhour.EVENT e on es.event_id = e.event_id '+
                 'where e.event_id = ? '+
                 'group by s.price, s.price_type_ref_id, s.details, s.deal_type_ref_id, s.day, TIME(s.start_time), TIME(s.end_time) '+
                 'order by special_dates;';
  db.get().query(queryStr, eventId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getEventSpecialsBySpecialId = function(specialId, done) {
  var queryStr = 'select s.* ' +
                 'from appyhour.SPECIAL s '+
                 'join appyhour.EVENT_SPECIAL es on s.special_id = es.special_id '+
                 'join appyhour.EVENT e on es.event_id = e.event_id '+
                 'where e.event_id = (select event_id from appyhour.EVENT_SPECIAL where special_id = ?);';
  db.get().query(queryStr, specialId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllRecurringSpecials = function(parameters, done) {
  var query = 'SELECT * FROM SPECIAL where price = ? and price_type_ref_id = ? and details = ? and deal_type_ref_id = ? and start_time = ? and end_time = ?;';

  db.get().query(query, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.search = function(search, done) {
  var query = 'select distinct a.*, c.name as establishment, d.ref_desc as iconUrl, e.latitude, e.longitude, i.title as eventName, c.* '+
              'from appyhour.SPECIAL a '+
              'join appyhour.ESTABLISHMENT_SPECIAL b on a.special_id = b.special_id '+
              'join appyhour.ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'join appyhour.REF d on a.deal_type_ref_id = d.ref_id '+
              'join appyhour.ADDRESS e on c.address_id = e.address_id '+
              'join appyhour.USER_ESTABLISHMENT f on c.establishment_id = f.establishment_id '+
              'join appyhour.USER g on f.user_id = g.user_id '+
              'left join appyhour.EVENT_SPECIAL h on a.special_id = h.special_id '+
              'left join appyhour.EVENT i on h.event_id = i.event_id '+
              'where g.show_specials = 1 and '+
              'c.show_in_app = 1 and '+
              '( '+
              '  match(a.details) against ("?") '+
              '  or '+
              '  a.details like "%?%" '+
              ') '+
              'and '+
              '( '+
              '  ( '+
              '    a.day = -1 and '+
              '    a.end_time >= date_sub(now(), INTERVAL 5 HOUR) and '+
              '    DATE(a.start_time) = CURDATE() '+
              '  ) '+
              '  or '+
              '  a.day > -1 '+
              ') '+
              'order by a.day, a.start_time';

  db.get().query(query, [search, search], function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.insert = function(special, done) {
  db.insert(special, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(special, done) {
  db.update(special, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(specialId, done) {
  db.get().query('DELETE FROM SPECIAL WHERE special_id = ?', specialId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEstablishmentId = function(establishmentId, done) {
  db.get().query('delete from SPECIAL where special_id in (select special_id from ESTABLISHMENT_SPECIAL where establishment_id = ?)', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEventId = function(eventId, done) {
  db.get().query('delete from SPECIAL where special_id in (select special_id from EVENT_SPECIAL where event_id = ?)', eventId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}