var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM SUPPLIER', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getOwners = function(done) {
  db.get().query('SELECT * FROM SUPPLIER WHERE user_type_ref_id = 305', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllBySupplierId = function(supplier_id, done) {
  db.get().query('SELECT * FROM SUPPLIER WHERE supplier_id = ?', supplier_id, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByName = function(name, done) {
  db.get().query('SELECT * FROM SUPPLIER WHERE name = ?', name, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByUsername = function(username, done) {
  db.get().query('SELECT * FROM SUPPLIER WHERE username = ?', username, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByEmail = function(email, done) {
  db.get().query('SELECT * FROM SUPPLIER WHERE email = ?', email, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getManagers = function(supplier_id, done) {
  var query = 'select c.* '+
              'from appyhour.SUPPLIER a '+
              'join appyhour.SUPPLIER_MANAGER b on a.supplier_id = b.supplier_id '+
              'join appyhour.SUPPLIER c on b.manager_id = c.supplier_id '+
              'where a.supplier_id = ?';

  db.get().query(query, supplier_id, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getOwner = function(supplier_id, done) {
  var query = 'select a.* '+
              'from appyhour.SUPPLIER a '+
              'join appyhour.SUPPLIER_MANAGER b on a.supplier_id = b.supplier_id '+
              'join appyhour.SUPPLIER c on b.manager_id = c.supplier_id '+
              'where b.manager_id = ?';

  db.get().query(query, supplier_id, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(supplier, done) {
  db.insert(supplier, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(supplier, done) {
  db.update(supplier, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(supplier_id, done) {
  db.get().query('DELETE FROM SUPPLIER WHERE supplier_id = ?', supplier_id, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}