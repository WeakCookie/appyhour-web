var db = require('../db.js');

exports.getAllByEstablishmentId = function(estId, done) {
  var queryStr = 'select * from appyhour.PROFILE_FAVORITES where establishment_id = ?';
  db.get().query(queryStr, estId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdAsync = async function(establishmentId) {
  const queryStr = 'SELECT * FROM PROFILE_FAVORITES WHERE establishment_id = ?'
  const query = db.getQueryAsync()
  const rows = await query(queryStr, establishmentId)
  return rows
}

exports.getCountByEstablishmentId = function(estId, done) {
  var queryStr = 'select count(*) as cnt from appyhour.PROFILE_FAVORITES where establishment_id = ?';
  db.get().query(queryStr, estId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllProfiles = function(done) {
  var queryStr = 'select * from appyhour.PROFILE';
  db.get().query(queryStr, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}
