var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM SPECIAL_ARCHIVE', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllFromDate = function(d, done) {
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.*, e.name as establishment ';
  queryStr = queryStr + 'FROM SPECIAL_ARCHIVE s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'WHERE s.created_at > date(?) ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  db.get().query(queryStr, d, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentId = function(establishment_id, done) {
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.* ';
  queryStr = queryStr + 'FROM SPECIAL_ARCHIVE s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'WHERE e.establishment_id = ?';

  db.get().query(queryStr, establishment_id, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdForWeek = function(parameters,  done) {
  var queryStr = "";
  queryStr = queryStr + 'SELECT s.*, evs.event_id ';
  queryStr = queryStr + 'FROM SPECIAL_ARCHIVE s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_SPECIAL evs ON s.special_id = evs.special_id ';
  queryStr = queryStr + 'WHERE e.establishment_id = ? AND evs.event_id is null AND ';
  queryStr = queryStr + 's.created_at >= ? AND s.deleted_at >= ? ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  db.get().query(queryStr, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdForYear = function(parameters,  done) {
  var queryStr = ""
  queryStr = queryStr + 'SELECT s.*, evs.event_id, ev.title as event, ea.title as eventArc ';
  queryStr = queryStr + 'FROM SPECIAL_ARCHIVE s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_SPECIAL evs ON s.special_id = evs.special_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT ev ON evs.event_id = ev.event_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_ARCHIVE ea ON evs.event_id = ea.event_id ';
  queryStr = queryStr + 'WHERE e.establishment_id = ? AND ';
  queryStr = queryStr + 's.created_at >= ? AND s.deleted_at >= ? ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  db.get().query(queryStr, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

/**
  * @param establishmentId
  * @param yearAgo date that special create after (YYYY-MM-DD)
  * @param yearAgo date that archive delete after (YYYY-MM-DD)
  */
exports.getAllByEstablishmentIdForYearAsync = async function(parameters) {
  var queryStr = ""
  queryStr = queryStr + 'SELECT s.*, evs.event_id, ev.title as event, ea.title as eventArc ';
  queryStr = queryStr + 'FROM SPECIAL_ARCHIVE s ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT_SPECIAL es ON s.special_id = es.special_id ';
  queryStr = queryStr + 'JOIN ESTABLISHMENT e ON es.establishment_id = e.establishment_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_SPECIAL evs ON s.special_id = evs.special_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT ev ON evs.event_id = ev.event_id ';
  queryStr = queryStr + 'LEFT JOIN EVENT_ARCHIVE ea ON evs.event_id = ea.event_id ';
  queryStr = queryStr + 'WHERE e.establishment_id = ? AND ';
  queryStr = queryStr + 's.created_at >= ? AND s.deleted_at >= ? ';
  queryStr = queryStr + 'ORDER BY s.created_at desc';

  const query = db.getQueryAsync()
  const rows = await query(queryStr, parameters)
  return rows
}

exports.getAllBySpecialId = function(specialId, done) {
  db.get().query('SELECT * FROM SPECIAL_ARCHIVE WHERE special_id = ?', specialId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(special, done) {
  db.insert(special, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(special, done) {
  db.update(special, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(specialId, done) {
  db.get().query('DELETE FROM SPECIAL_ARCHIVE WHERE special_id = ?', specialId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEstablishmentId = function(establishmentId, done) {
  db.get().query('delete FROM SPECIAL_ARCHIVE where special_id in (select special_id from ESTABLISHMENT_SPECIAL where establishment_id = ?)', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.archiveByEventId = function(eventId, done) {
  var query = 'insert into SPECIAL_ARCHIVE (special_id, price, price_type_ref_id, details, deal_type_ref_id, day, ' +
              'start_time, end_time, deleted_at, created_at, updated_at) ' +
              'select s.special_id, s.price, s.price_type_ref_id, s.details, s.deal_type_ref_id, s.day, ' +
              's.start_time, s.end_time, now(), s.created_at, now() from SPECIAL s ' +
              'join EVENT_SPECIAL es on s.special_id = es.special_id ' +
              'where es.event_id = ?';
  db.get().query(query, eventId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}