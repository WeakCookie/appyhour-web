var db = require('../db.js');

exports.getAll = function(done) {
  var query = 'select a.*, a.created_at as eq_created_at, a.updated_at as eq_updated_at, f.*, h.*, e.image_id, e.image_location as imageUrl, g.ref_code as state, '+
                'ifnull(j.user_id, k.user_id) as owner_id, ifnull(j.username, k.username) as owner_username, ifnull(j.email, k.email) as owner_email, '+
                'ifnull(j.phone_num, k.phone_num) as owner_phone_num, ifnull(j.needs_to_pay, k.needs_to_pay) as owner_needs_to_pay '+
              'from appyhour.ESTABLISHMENT_QUEUE a '+
              'left join appyhour.ESTABLISHMENT_QUEUE_IMAGE d on a.establishment_id = d.establishment_id '+
              'left join appyhour.IMAGE e on d.image_id = e.image_id '+
              'join appyhour.ADDRESS f on a.address_id = f.address_id '+
              'join appyhour.REF g on f.state_code_ref_id = g.ref_id '+
              'join appyhour.HOURS h on a.hours_id = h.hours_id '+
              'left join appyhour.USER_ESTABLISHMENT_QUEUE i on a.establishment_id = i.establishment_id '+
              'left join appyhour.USER_QUEUE j on i.user_id = j.user_id '+
              'left join appyhour.USER k on i.user_id = k.user_id '+
              'order by a.name';

  db.get().query(query, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByEstablishmentId = function(establishmentId, done) {
  var query = 'select a.*, f.*, h.*, e.image_id, e.image_location as imageUrl, g.ref_code as state '+
              'from appyhour.ESTABLISHMENT_QUEUE a '+
              'left join appyhour.ESTABLISHMENT_QUEUE_IMAGE d on a.establishment_id = d.establishment_id '+
              'left join appyhour.IMAGE e on d.image_id = e.image_id '+
              'join appyhour.ADDRESS f on a.address_id = f.address_id '+
              'join appyhour.REF g on f.state_code_ref_id = g.ref_id '+
              'join appyhour.HOURS h on a.hours_id = h.hours_id '+
              'where a.establishment_id = ?'

  db.get().query(query, establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(establishmentQueue, done) {
  db.insert(establishmentQueue, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(establishmentQueue, done) {
  db.update(establishmentQueue, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(establishmentId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_QUEUE WHERE establishment_special_id = ?', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEstablishmentId = function(establishmentId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_QUEUE WHERE establishment_id = ?', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
