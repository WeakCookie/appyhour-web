var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM ESTABLISHMENT_EVENT', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByEstablishmentEventId = function(establishmentSpecialId, done) {
  db.get().query('SELECT * FROM ESTABLISHMENT_EVENT WHERE establishment_event_id = ?', establishmentEventId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(establishmentEvent, done) {
  db.insert(establishmentEvent, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(establishmentEvent, done) {
  db.update(establishmentEvent, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(establishmentEventId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_EVENT WHERE establishment_event_id = ?', establishmentEventId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEventId = function(eventId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_EVENT WHERE event_id = ?', eventId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEstablishmentId = function(establishmentId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_EVENT WHERE establishment_id = ?', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
