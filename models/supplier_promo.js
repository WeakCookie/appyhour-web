var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM SUPPLIER_PROMO', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByPromoId = function(promoId, done) {
  db.get().query('SELECT * FROM SUPPLIER_PROMO where promo_id = ?', promoId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  })
}

exports.insert = function(supplierpromo, done) {
  db.insert(supplierpromo, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(supplierpromo, done) {
  db.update(supplierpromo, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(supplier_promo_id, done) {
  db.get().query('DELETE FROM SUPPLIER_PROMO WHERE supplier_promo_id = ?', supplier_promo_id, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}