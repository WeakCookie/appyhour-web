var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM REF', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByRefId = function(refId, done) {
  db.get().query('SELECT * FROM REF WHERE ref_id = ?', refId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByRefTypeId = function(refTypeId, done) {
  db.get().query('SELECT * FROM REF WHERE ref_type_id = ?', refTypeId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByRefCode = function(refCode, done) {
  db.get().query('SELECT * FROM REF WHERE ref_code = ?', refCode, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByRefDesc = function(refDesc, done) {
  db.get().query('SELECT * FROM REF WHERE ref_desc = ?', refDesc, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(ref, done) {
  db.insert(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(ref, done) {
  db.update(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(refId, done) {
  db.get().query('DELETE FROM REF WHERE ref_id = ?', refId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}