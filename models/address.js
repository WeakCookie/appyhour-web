var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM ADDRESS', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByAddressId = function(addressId, done) {
  var query = 'select a.*, b.ref_code as state '+
              'from ADDRESS a '+
              'join REF b on a.state_code_ref_id = b.ref_id '+
              'where a.address_id = ?';
  db.get().query(query, addressId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

/*
exports.getAllByUserId = function(userId, done) {
  var query = 'select a.*, e.ref_code as state '+
              'from ADDRESS a '+
              'join ESTABLISHMENT b on a.address_id = b.address_id '+
              'join USER_ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'join USER d on c.user_id = d.user_id '+
              'join REF e on a.state_code_ref_id = e.ref_id '+
              'where d.user_id = ?';
  db.get().query(query, userId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}*/

exports.insert = function(address, done) {
  db.insert(address, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(address, done) {
  db.update(address, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(addressId, done) {
  db.get().query('DELETE FROM ADDRESS WHERE address_id = ?', addressId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}