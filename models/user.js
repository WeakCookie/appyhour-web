var db = require('../db.js');
const util = require('util')

exports.getAll = function(done) {
  db.get().query('SELECT * FROM USER', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getOwners = function(done) {
  db.get().query('SELECT * FROM USER WHERE user_type_ref_id = 305', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByUserId = function(userId, done) {
  db.get().query('SELECT * FROM USER WHERE user_id = ?', userId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByUsername = function(username, done) {
  db.get().query('SELECT * FROM USER WHERE username = ?', username, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByUsernameAsync = async function(username) {
  const queryString = 'SELECT * FROM USER WHERE username = ?'
  const query = db.getQueryAsync()
  const rows = await query(queryString, username)
  return rows[0]
}

exports.getAllByEmail = function(email, done) {
  db.get().query('SELECT * FROM USER WHERE email = ?', email, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByEstablishmentId = function(establishmentId, done) {
  var query = 'select a.* '+
              'from USER a '+
              'join USER_ESTABLISHMENT b on a.user_id = b.user_id '+
              'join ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'where c.establishment_id = ?';
  db.get().query(query, establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllByEstablishmentIdAsync = async function(establishmentId) {
  const queryString = 'select a.* '+
                      'from USER a '+
                      'join USER_ESTABLISHMENT b on a.user_id = b.user_id '+
                      'join ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
                      'where c.establishment_id = ?';
  const pool = db.get()
  const query = util.promisify(pool.query).bind(pool)
  const rows = await query(queryString, establishmentId)
  return rows[0]
}

exports.getManagersForOwnerId = function(ownerId, done) {
  var parameters = [ownerId, ownerId];
  var query = 'select a.*, c.name as establishment '+
              'from appyhour.USER a '+
              'join appyhour.USER_ESTABLISHMENT b on a.user_id = b.user_id '+
              'join appyhour.ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'where a.user_id != ? and '+
              'a.user_id in ( '+
                'select distinct(user_id) '+
                'from appyhour.USER_ESTABLISHMENT '+
                'where establishment_id in ( '+
                  'select establishment_id '+
                  'from appyhour.USER_ESTABLISHMENT '+
                  'where user_id = ? '+
                '))';

  db.get().query(query, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getManagersForEstablishmentId = function(estId, done) {
  var query = 'select user_id '+
              'from appyhour.USER '+
              'where user_type_ref_id = 306 and '+
              'user_id in '+
                '(select user_id '+
                'from appyhour.USER_ESTABLISHMENT '+
                'where establishment_id = ?)';

  db.get().query(query, estId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getPayingUsers = function(done) {
  var query = 'select a.*, b.*, c.topic '+
              'from appyhour.USER a '+
              'join appyhour.USER_ESTABLISHMENT b on a.user_id = b.user_id '+
              'join appyhour.ESTABLISHMENT c on b.establishment_id = c.establishment_id '+
              'where stripe_id is not null '+
              'order by a.user_id;'
  db.get().query(query, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.insert = function(user, done) {
  db.insert(user, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(user, done) {
  db.update(user, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(userId, done) {
  db.get().query('DELETE FROM USER WHERE user_id = ?', userId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteAll = function(userIds, done) {
  db.get().query('DELETE FROM USER WHERE user_id in (?)', userIds, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
