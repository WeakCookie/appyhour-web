var db = require('../db.js');
const util = require('util')

exports.getAll = function(done) {
  db.get().query('SELECT * FROM ANALYTICS_CACHE', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllAsync = async function() {
  const pool = db.get()
  const query = util.promisify(pool.query).bind(pool)
  const rows = await query('SELECT * FROM ANALYTICS_CACHE')
  return rows
}

exports.upsert = function(analyticsCache, done = () => {}) {
  db.upsert(analyticsCache, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.getAllByEstablishmentId = function(parameters, done) {
  db.get().query('SELECT * FROM ANALYTICS_CACHE WHERE establishment_id = ? AND timezone = ?', parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}
