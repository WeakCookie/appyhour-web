var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM LOCATION ORDER BY location', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByLocationId = function(locationId, done) {
  db.get().query('SELECT * FROM LOCATION WHERE location_id = ?', locationId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(ref, done) {
  db.insert(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(ref, done) {
  db.update(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(locationId, done) {
  db.get().query('DELETE FROM LOCATION WHERE location_id = ?', locationId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}