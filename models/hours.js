var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM HOURS', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByHoursId = function(hoursId, done) {
  db.get().query('SELECT * FROM HOURS WHERE hours_id = ?', hoursId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(hours, done) {
  db.insert(hours, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(hours, done) {
  db.update(hours, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(hoursId, done) {
  db.get().query('DELETE FROM HOURS WHERE hours_id = ?', hoursId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}