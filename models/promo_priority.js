var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM PROMO_PRIORITY', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByPromoId = function(promoId, done) {
  db.get().query('SELECT * FROM PROMO_PRIORITY WHERE promo_id = ?', promoId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByLocationId = function(locationId, done) {
  db.get().query('SELECT * FROM PROMO_PRIORITY a JOIN appyhour.PROMO b on a.promo_id = b.promo_id WHERE location_id = ? ORDER BY priority', locationId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getNextPriorityForLocationId = function(locationId,  done) {
  db.get().query('SELECT max(priority)+1 as max FROM PROMO_PRIORITY WHERE location_id = ?', locationId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(ref, done) {
  db.insert(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(ref, done) {
  db.update(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(promoPriorityId, done) {
  db.get().query('DELETE FROM PROMO_PRIORITY WHERE promo_priority_id = ?', promoPriorityId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteAllByLocationId = function(locationId, done) {
  db.get().query('DELETE FROM PROMO_PRIORITY WHERE location_id = ?', locationId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteAllByPromoId = function(promoId, done) {
  db.get().query('DELETE FROM PROMO_PRIORITY WHERE promo_id = ?', promoId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}