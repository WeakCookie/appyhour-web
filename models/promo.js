var db = require('../db.js');

exports.getAll = function(done) {
  var query = 'select a.*, b.supplier_promo_id, b.approved_at, b.approved_by, b.denied_at, b.denied_by, b.denied_reason, IFNULL(c.name, d.name) as supplier_name '+
              'from PROMO a '+
              'left join SUPPLIER_PROMO b on a.promo_id = b.promo_id '+
              'left join SUPPLIER c on b.supplier_id = c.supplier_id '+
              'left join SUPPLIER_ARCHIVE d on b.supplier_id = d.supplier_id';
  db.get().query(query, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByPromoId = function(promoId, done) {
  var query = 'select a.*, b.supplier_promo_id, b.approved_at, b.approved_by, b.denied_at, b.denied_by, b.denied_reason, IFNULL(c.name, d.name) as supplier_name '+
              'from PROMO a '+
              'left join SUPPLIER_PROMO b on a.promo_id = b.promo_id '+
              'left join SUPPLIER c on b.supplier_id = c.supplier_id '+
              'left join SUPPLIER_ARCHIVE d on b.supplier_id = d.supplier_id '+
              'where a.promo_id = ?';

  db.get().query(query, promoId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllBySupplierId = function(supplierId, done) {
  var query = 'select a.*, b.approved_at, b.approved_by, b.denied_at, b.denied_by, b.denied_reason '+
              'from PROMO a '+
              'join SUPPLIER_PROMO b on a.promo_id = b.promo_id '+
              'join SUPPLIER c on b.supplier_id = c.supplier_id '+
              'where c.supplier_id = ?';
  db.get().query(query, supplierId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.insert = function(ref, done) {
  db.insert(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(ref, done) {
  db.update(ref, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(promoId, done) {
  db.get().query('DELETE FROM PROMO WHERE promo_id = ?', promoId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}