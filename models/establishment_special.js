var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM ESTABLISHMENT_SPECIAL', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByEstablishmentSpecialId = function(establishmentSpecialId, done) {
  db.get().query('SELECT * FROM ESTABLISHMENT_SPECIAL WHERE establishment_special_id = ?', establishmentSpecialId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(establishmentSpecial, done) {
  db.insert(establishmentSpecial, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(establishmentSpecial, done) {
  db.update(establishmentSpecial, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(establishmentSpecialId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_SPECIAL WHERE establishment_special_id = ?', establishmentSpecialId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteBySpecialId = function(specialId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_SPECIAL WHERE special_id = ?', specialId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEventId = function(eventId, done) {
  db.get().query('DELETE from ESTABLISHMENT_SPECIAL where special_id in (select special_id from EVENT_SPECIAL where event_id = ?)', eventId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEstablishmentId = function(establishmentId, done) {
  db.get().query('DELETE FROM ESTABLISHMENT_SPECIAL WHERE establishment_id = ?', establishmentId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
