var db = require('../db.js')
const isEmpty = require('lodash/isEmpty')

const TABLE = 'ANALYTICS_CACHE_V2'

exports.getAllAsync = async function () {
  const query = db.getQueryAsync()
  const rows = await query(`SELECT * FROM ${TABLE}`)
  if (!isEmpty(rows)) {
    return rows.map(row => convertDbObjectToAnalyticsCacheJson(row))
  } else {
    return []
  }
}

exports.upsert = function (analyticsCache, done = () => { }) {
  const dbObject = convertAnalyticsCacheJsonToDbObject(analyticsCache)
  db.upsert(dbObject, function (err, res) {
    if (err) return done(err)
    done(null, res)
  })
}

exports.getAllByEstablishmentIdAsync = async function (parameters) {
  const query = db.getQueryAsync()
  const rows = await query(`SELECT * FROM ${TABLE} WHERE establishment_id = ? AND timezone = ?`, parameters)
  if (!isEmpty(rows)) {
    return convertDbObjectToAnalyticsCacheJson(rows[0])
  } else {
    return {}
  }
}

// vendorData = { special: values, event: values, profileView: values }, all values are 56 nearest days
function convertAnalyticsCacheJsonToDbObject({ establishmentId, timezone, averageMapping, vendorData, weeklyImpressions, createdAt, updatedAt }) {
  return {
    [TABLE]: {
      timezone,
      establishment_id: establishmentId,
      vendor_data: JSON.stringify(vendorData),
      weekly_impressions: JSON.stringify(weeklyImpressions),
      average_views_since_published_mapping: JSON.stringify(averageMapping),
      created_at: createdAt,
      updated_at: updatedAt
    }
  }
}

function convertDbObjectToAnalyticsCacheJson({ establishment_id, timezone, average_views_since_published_mapping, vendor_data, weekly_impressions, created_at, updated_at }) {
  return {
    timezone,
    establishmentId: establishment_id,
    vendorData: JSON.parse(vendor_data),
    weeklyImpressions: JSON.parse(weekly_impressions),
    averageMapping: JSON.parse(average_views_since_published_mapping),
    createdAt: created_at,
    updatedAt: updated_at
  }
}