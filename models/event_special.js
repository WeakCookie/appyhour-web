var db = require('../db.js');

exports.getAll = function(done) {
  db.get().query('SELECT * FROM EVENT_SPECIAL', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByEventSpecialId = function(eventSpecialId, done) {
  db.get().query('SELECT * FROM EVENT_SPECIAL WHERE event_special_id = ?', eventSpecialId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.insert = function(eventSpecial, done) {
  db.insert(eventSpecial, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function(eventSpecial, done) {
  db.update(eventSpecial, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function(eventSpecialId, done) {
  db.get().query('DELETE FROM EVENT_SPECIAL WHERE event_special_id = ?', eventSpecialId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteBySpecialId = function(specialId, done) {
  db.get().query('DELETE FROM EVENT_SPECIAL WHERE special_id = ?', specialId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.deleteByEventId = function(eventId, done) {
  db.get().query('DELETE FROM EVENT_SPECIAL WHERE event_id = ?', eventId, function(err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
