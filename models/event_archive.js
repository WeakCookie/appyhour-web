var db = require('../db.js');

exports.getAll = function (done) {
  db.get().query('SELECT * FROM EVENT_ARCHIVE', function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  })
}

exports.getAllByEventId = function (eventId, done) {
  var query = 'select * from EVENT_ARCHIVE where event_id = ?';
  db.get().query(query, eventId, function (err, rows) {
    if (err) return done(err);
    done(null, rows[0]);
  });
}

exports.getAllGrouped = function (done) {
  var query = 'select *, group_concat(event_id order by event_id SEPARATOR ",") AS event_ids, ' +
    'group_concat(day order by day SEPARATOR ",") AS days ' +
    'from EVENT_ARCHIVE ' +
    'group by title, details, start_time, end_time';

  db.get().query(query, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentId = function (establishmentId, done) {
  var query = 'select c.event_id, c.title, c.details as event_details, c.day, c.start_time, c.end_time, e.establishment_id, ' +
    'e.name as establishment, f.latitude, f.longitude, ' +
    '(select h.image_location from appyhour.ESTABLISHMENT_IMAGE g ' +
    'join appyhour.IMAGE h on g.image_id = h.image_id ' +
    'where g.establishment_id = e.establishment_id and h.image_type_ref_id = 303) as imageUrl ' +
    'from appyhour.EVENT_ARCHIVE c ' +
    'join appyhour.ESTABLISHMENT_EVENT d on c.event_id = d.event_id ' +
    'join appyhour.ESTABLISHMENT e on d.establishment_id = e.establishment_id ' +
    'join appyhour.ADDRESS f on e.address_id = f.address_id ' +
    'where e.establishment_id = ? ' +
    'order by c.end_time, c.event_id';

  db.get().query(query, establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getAllByEstablishmentIdForYear = function (parameters, done) {
  var queryStr = 'SELECT e.* ' +
    'FROM EVENT_ARCHIVE e ' +
    'JOIN ESTABLISHMENT_EVENT ee ON e.event_id = ee.event_id ' +
    'JOIN ESTABLISHMENT es ON ee.establishment_id = es.establishment_id ' +
    'WHERE es.establishment_id = ? AND ' +
    '(e.day > -1 OR (e.day = -1 AND e.created_at >= ?)) ' +
    'ORDER BY e.created_at desc';

  db.get().query(queryStr, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

/**
  * @param establishmentId
  * @param yearAgo date that special create after (YYYY-MM-DD)
  */
exports.getAllByEstablishmentIdForYearAsync = async function (parameters) {
  const queryStr = 'SELECT e.* ' +
    'FROM EVENT_ARCHIVE e ' +
    'JOIN ESTABLISHMENT_EVENT ee ON e.event_id = ee.event_id ' +
    'JOIN ESTABLISHMENT es ON ee.establishment_id = es.establishment_id ' +
    'WHERE es.establishment_id = ? AND ' +
    '(e.day > -1 OR (e.day = -1 AND e.created_at >= ?)) ' +
    'ORDER BY e.created_at desc'

  const query = db.getQueryAsync()
  const rows = await query(queryStr, parameters)
  return rows
}

exports.getAllByEstablishmentIdGroupRecurring = function (establishmentId, done) {
  var query = 'select c.event_id, c.title, c.details as event_details, group_concat(c.day order by c.day SEPARATOR ",") AS days, c.start_time, c.end_time, e.establishment_id, ' +
    'e.name as establishment, f.latitude, f.longitude, ' +
    '(select h.image_location from appyhour.ESTABLISHMENT_IMAGE g ' +
    'join appyhour.IMAGE h on g.image_id = h.image_id ' +
    'where g.establishment_id = e.establishment_id and h.image_type_ref_id = 303) as imageUrl ' +
    'from appyhour.EVENT_ARCHIVE c ' +
    'join appyhour.ESTABLISHMENT_EVENT d on c.event_id = d.event_id ' +
    'join appyhour.ESTABLISHMENT e on d.establishment_id = e.establishment_id ' +
    'join appyhour.ADDRESS f on e.address_id = f.address_id ' +
    'where e.establishment_id = ? ' +
    'group by title, event_details, start_time, end_time ' +
    'order by c.end_time, c.event_id';

  db.get().query(query, establishmentId, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.getEventIdForRecurring = function (parameters, done) {
  var query = 'select event_id from appyhour.EVENT_ARCHIVE where title = ? and details = ? and day = ?;';

  db.get().query(query, parameters, function (err, rows) {
    if (err) return done(err);
    done(null, rows);
  });
}

exports.insert = function (ev, done) {
  db.insert(ev, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.update = function (ev, done) {
  db.update(ev, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}

exports.delete = function (eventId, done) {
  db.get().query('DELETE FROM EVENT WHERE event_id = ?', eventId, function (err, res) {
    if (err) return done(err);
    done(null, res);
  });
}
