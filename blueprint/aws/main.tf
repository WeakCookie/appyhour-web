# Configure the AWS Provider
provider "aws" {
  region = "us-east-2"
  access_key = var.iam_key.access_key
  secret_key = var.iam_key.secret_key
}

resource "aws_key_pair" "id_rsa" {
  key_name = "id_rsa"
  public_key = file("id_rsa.pub")
}

resource "aws_security_group" "ah_api_dev" {
  name        = "ah_api_dev_security_group"
  description = "Allow HTTP, HTTPS and SSH traffic"

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "HTTP"
    from_port   = 3001
    to_port     = 3001
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("id_rsa")
    host        = self.public_ip
  }

  tags = {
    Name = "ah_api_dev"
  }
}

# Create a VPC
resource "aws_vpc" "appy_hour_vpc_dev" {
  cidr_block = "10.0.0.0/16"
}

resource "aws_instance" "appy_hour_dev" {
  ami           = "ami-00399ec92321828f5"
  instance_type = "t2.medium"
  key_name      = aws_key_pair.id_rsa.key_name

  vpc_security_group_ids = [
    aws_security_group.ah_api_dev.id
  ]

  tags = {
    Name = "AppyHourBackendDev"
  }
}

resource "aws_s3_bucket" "developappyhour" {  
  bucket = "developappyhour"
  acl    = "public-read"     
  
  website {    
    index_document = "index.html"
    error_document = "error.html"
  }
}

resource "aws_s3_bucket_policy" "develop_appy_hour" {  
  bucket = aws_s3_bucket.developappyhour.id   
  policy = <<POLICY
{    
    "Version": "2012-10-17",    
    "Statement": [        
      {            
          "Sid": "PublicReadGetObject",            
          "Effect": "Allow",            
          "Principal": "*",            
          "Action": [                
             "s3:GetObject"            
          ],            
          "Resource": [
             "arn:aws:s3:::${aws_s3_bucket.developappyhour.id}/*"            
          ]        
      }    
    ]
}
POLICY
}

resource "aws_cloudfront_distribution" "develop_appy_hour" {
  origin {
    domain_name = aws_s3_bucket.developappyhour.bucket_regional_domain_name
    origin_id   = "developappyhour"
  }

  enabled             = true
  is_ipv6_enabled     = true
  comment             = "CDN for develop appy hour"
  default_root_object = "index.html"

  default_cache_behavior {
    allowed_methods  = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "developappyhour"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    viewer_protocol_policy = "allow-all"
    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  ordered_cache_behavior {
    path_pattern     = "*"
    allowed_methods  = ["GET", "HEAD", "OPTIONS"]
    cached_methods   = ["GET", "HEAD"]
    target_origin_id = "developappyhour"

    forwarded_values {
      query_string = false

      cookies {
        forward = "none"
      }
    }

    min_ttl                = 0
    default_ttl            = 3600
    max_ttl                = 86400
    compress               = true
    viewer_protocol_policy = "redirect-to-https"
  }

  price_class = "PriceClass_200"

  tags = {
    Environment = "develop"
  }

  viewer_certificate {
    cloudfront_default_certificate = true
  }
}